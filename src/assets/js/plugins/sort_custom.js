$(document).ready(function(){

    var updateOutput0 = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    var updateOutput1 = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    var updateOutput2 = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    $('#nestable-menu-level-0').nestable({
        group: 1
    }).on('change', updateOutput0);
    // output initial serialised data
    updateOutput0($('#nestable-menu-level-0').data('output', $('#nestable-output-menu-level-0')));


    $('#nestable-menu-level-1').nestable({
        group: 2
    }).on('change', updateOutput1);
    // output initial serialised data
    updateOutput1($('#nestable-menu-level-1').data('output', $('#nestable-output-menu-level-1')));

    $('#nestable-menu-level-2').nestable({
        group: 3
    }).on('change', updateOutput2);
    // output initial serialised data
    updateOutput2($('#nestable-menu-level-2').data('output', $('#nestable-output-menu-level-2')));

});
