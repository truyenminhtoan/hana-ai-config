chat = {
    khoiTao: function(){
        // CHAT MESSAGE
    $('.chat-main-box').css({
      'height': (($(window).height())) + 'px'
    });

    $('.info-right-aside').css({
      'height': (($(window).height())) + 'px'
    });
    require(['converse'], function (converse) {
      converse.initialize({
        auto_reconnect: true,
        bosh_service_url: 'https://test3.mideasvn.com:8280/http-bind', // Please use this connection manager only for testing purposes
        keepalive: true,
        sky_ai_name: 'Trợ lý ảo Hana',
        fb_name: 'Facebook User',
        message_carbons: true,
        message_archiving: 'always',
        roster_groups: true,
        show_controlbox_by_default: false,
        strict_plugin_dependencies: false,
        chatstate_notification_blacklist: ['mulles@movim.eu'],
        xhr_user_search: false,
        debug: true,
        sky_host: '@10min',
        play_sounds: false,
        show_notify: false,
        sky_room: '@muc.10min',
        hana_agent_id: 'hanaagenthana_378',
        hana_queue_name: '10m_hana_input_chung',
        sky_hanaprefix: 'hanaagenthana_',
        sky_apiserver: 'https://test3.mideasvn.com:8200/ReengBackendBiz/',
        hana_web_gateway: 'https://test.mideasvn.com:9201/',
        file_path: 'https://test3.mideasvn.com:8000/10mindev/file',
        partner_minhthu: '2d70832e-4d1c-4399-8bd7-395a4ef81216',
        jid: '10min',
        sky_agent: '21788885-c717-41b9-8fa2-e01a2d05c505',
        sky_partner: '7ea6f744-064a-4ee1-b9b7-ea1ae2957416',
        mideas_partner_id: '481',
        access_token: 'xxx',
        sky_pass: '5483cd1a-5f7a-4e93-be14-de20919ee61c',
        sky_myname: 'Tôi',
        sky_resource: 'webclient',
        is_connecting: false
      });
    });
    $(window).resize(function () {
      console.log('resize');
      // CONTACT
      $('.chat-left-inner').css({
        'height': (($(window).height())) + 'px'
      });
      $('.chatonline').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      $('.chat-left-inner .slimScrollDiv').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      $('.chat-left-inner .slimScrollBar').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      // CHATBOX
      $('.chat-main-box').css({
        'height': (($(window).height())) + 'px'
      });

      $('.chat-box .slimScrollDiv').css({
        'height': (($(window).height()) - 125) + 'px'
      });

      $('.chat-box .slimScrollBar').css({
        'height': (($(window).height()) - 125) + 'px'
      });

      $('.chatonline').slimScroll({
        height: (($(window).height()) - 70) + 'px',
        start: 'top',
        allowPageScroll: false,
        disableFadeOut: false
      });
    });
    }
}
