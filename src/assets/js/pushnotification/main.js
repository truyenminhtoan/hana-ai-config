/* global AppController */
/* eslint-env browser */

(function() {
    $(document).ready(function() {
        
    	//console.log("starting register pushnotification");
      const appController = new AppController();
      appController.ready
      .then(() => {
        //console.log("starting registerServiceWorker pushnotification");
        document.body.dataset.simplePushDemoLoaded = true;
        const host = 'mideasvn.com';
        if (
          window.location.host === host &&
          window.location.protocol !== 'https:') {
          window.location.protocol = 'https';
        }
        //console.log("starting registerServiceWorker pushnotification");
        appController.registerServiceWorker();
      });
      
    });
}).call(this); 
// window.onload = function() {
//   // tao doi tuong app controller
//   console.log("starting register pushnotification");
//   const appController = new AppController();
//   appController.ready
//   .then(() => {
//     document.body.dataset.simplePushDemoLoaded = true;

//     const host = 'gauntface.github.io';
//     if (
//       window.location.host === host &&
//       window.location.protocol !== 'https:') {
//       window.location.protocol = 'https';
//     }
//     console.log("starting registerServiceWorker pushnotification");
//     appController.registerServiceWorker();
//   });
// };
