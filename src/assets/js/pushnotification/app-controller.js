/* global PushClient, EncryptionHelperFactory, MaterialComponentsSnippets */
/* eslint-env browser */

class AppController {
    constructor() {
        // Define a different server URL here if desire.
        this._PUSH_SERVER_URL = '';
        this._API_KEY = 'AIzaSyBBh4ddPa96rQQNxqiq_qQj7sq1JdsNQUQ';
        // //testkey   google
        // this._applicationKeys = {
        //     publicKey: window.base64UrlToUint8Array(
        //         'BFMfcE1mObqBPoKZAq7I4edt-8L3XpjzCVmNhVayyXmb' +
        //         'YWz2noBCKQLD79Hzi1d335tgcWTbH1eAKjnPfjONZa8')
        // };
        // //testkey   cua minh
        // this._applicationKeys = {
        //     publicKey: window.base64UrlToUint8Array(
        //         'BI8VRNbXvwcu37Tc2_0XqqX8DKZsx2T8sT4OP1TFfpy' +
        //         '7OOtuDg_FlpEpnrLjoDp74mAw0Wf8_IavoZyFukBI5Us')
        // };
        //hotfix
        //  this._applicationKeys = {
        //   publicKey: window.base64UrlToUint8Array(
        //     'BDaLg_wxoGNr3-btFm-Sn2_Q2D8rcDmkofbUW2Vdrf2M' +
        //     'dt5WYLF9IKnqHtb23J6LQcjxAWkBwdQsg_W5E113-O4')
        // }; 
        // trien khai
        this._applicationKeys = {
            publicKey: window.base64UrlToUint8Array(
                'BK9HmIYcn4ViyBgemDDitqR8zqF4xxC2d15gj29hQCSf' +
                'ORsAqRlYI5A4h4mBWL3aC4PZyA0-VssFH4QEO-Ot52w')
        };
        this.ready = Promise.resolve();
        this._uiInitialised();
        //// console_bk.log("end init");
    }

    _uiInitialised() {
        //// console_bk.log("_uiInitialised");
        this._stateChangeListener = this._stateChangeListener.bind(this);
        this._subscriptionUpdate = this._subscriptionUpdate.bind(this);

        this._pushClient = new PushClient(
            this._stateChangeListener,
            this._subscriptionUpdate,
            this._applicationKeys.publicKey
        );
        // this._pushClient.subscribeDevice();
    }

    registerServiceWorker() {
        // Check that service workers are supported
        if ('serviceWorker' in navigator) {
            //// console_bk.log("registerServiceWorker");
            navigator.serviceWorker.register('./service-worker.js')
                .then((registration) => {
                    // console_bk.log("registration");
                    registration.update();
                })
                .catch((err) => {
                    this.showErrorMessage(
                        'Unable to Register SW',
                        'Sorry this demo requires a service worker to work and it ' +
                        'failed to install - sorry :('
                    );
                    console.error(err);
                });
            // Handler for messages coming from the service worker
            navigator.serviceWorker.addEventListener('message', function (event) {
                // console_bk.log("Client 1 Received Message: " + event.data);
                setTimeout(function () {
                    $('#contact_' + event.data).click();
                }, 1000);

                // event.ports[0].postMessage("Client 1 Says 'Hello back!'");
            });
        } else {
            this.showErrorMessage(
                'Service Worker Not Supported',
                'Sorry this demo requires service worker support in your browser. ' +
                'Please try this demo in Chrome or Firefox Nightly.'
            );
        }
    }

    _stateChangeListener(state, data, that) {

        if (state.id === 'ASKED') {
            return;
        }

        //// console_bk.log("state "+ JSON.stringify(state) + " data "+ data)
        if (typeof state.interactive !== 'undefined') {
            if (state.interactive) {
                //chua hoi 
            } else {
                return;
            }
        }

        if (typeof state.pushEnabled !== 'undefined') {
            if (state.pushEnabled) {
                //push is enabled
            } else {
                // this._toggleSwitch.off();
                //push is disable

                that.subscribeDevice();
            }
        }

        switch (state.id) {
            case 'UNSUPPORTED':
                this.showErrorMessage(
                    'Push Not Supported',
                    data
                );
                break;
            case 'PERMISSION_PROMPT':
                that.subscribeDevice();
                break;
            case 'ERROR':
                this.showErrorMessage(
                    'Ooops a Problem Occurred',
                    data
                );
                break;
            default:
                break;
        }
    }

    _subscriptionUpdate(subscription) {
        this._currentSubscription = subscription;
        // console_bk.log('[DOCAOHUYNH] _currentSubscription: '+ JSON.stringify(subscription));
        if (!subscription) {
            // Remove any subscription from your servers if you have
            // set it up.
            // this._sendPushOptions.style.opacity = 0;
            return;
        }
        // This is too handle old versions of Firefox where keys would exist
        // but auth wouldn't
        const subscriptionObject = JSON.parse(JSON.stringify(subscription));
        //// console_bk.log("Subscription : " + JSON.stringify(subscription));
        this.saveToServer(subscriptionObject);

    }

    showErrorMessage(title, message) {
        //// console_bk.log(title + " message: " + message);
    }

    saveToServer(subscriptionObject) {
        // console_bk.log('SAVEPUSHTOSERVER');
        my.namespace.publicFunc(subscriptionObject)
        /*$.ajax({
            type: "POST",
            url: "/notification/update-subscription",
            data: (subscriptionObject),
            dataType: "json",
            success: function(data) {

            },
            error: function(XMLHttpRequest, textStatus, errorDivThrown) {

            }
        });*/
    }
}

if (window) {
    if (window.AppController) {

    } else {
        window.AppController = AppController;
    }

}