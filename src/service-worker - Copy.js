'use strict';

/* eslint-env browser, serviceworker */

// importScripts('./dist/js/pushnotification/encryption/base64.js');
// importScripts('./dist/js/pushnotification/encryption/jquery.js');
importScripts('./assets/js/pushnotification/encryption/base64.jquery.js');
// importScripts('./plugins/jQuery/jquery.base64.min.js');

// self.analytics.trackingId = 'UA-77119321-2';
function send_message_to_client(client, msg) {
    return new Promise(function (resolve, reject) {
        var msg_chan = new MessageChannel();

        msg_chan.port1.onmessage = function (event) {
            if (event.data.error) {
                reject(event.data.error);
            } else {
                resolve(event.data);
            }
        };

        // client.postMessage("SW Says: '" + msg + "'", [msg_chan.port2]);
        client.postMessage(msg, [msg_chan.port2]);
    });
}

function send_message_to_all_clients(msg) {
    clients.matchAll().then((clients) => {
        clients.forEach((client) => {
            send_message_to_client(client, msg).then(m => console.log("SW Received Message: " + m));
        })
    })
}

self.addEventListener('push', function (event) {

    //console.log("Data teexxt1 1");

    // Outputs: "SGVsbG8gV29ybGQh"


    let notificationTitle = 'Hello';
    const notificationOptions = {
        body: 'Bạn có tin nhắn mới.',
        icon: './assets/img/chat/logo.png',
        badge: './assets/img/chat/logo.png',
        tag: 'hana-notification',
        data: {
            // url: 'https://test.mideasvn.com:9809/#/chat',
            url: 'https://chat.hana.ai/#/chat',
            // url: 'http://localhost:8001/chat',
            // url: 'https://google.mideasvn.com:9001/chat',
            groupid: ''

        },
    };

    // event.waitUntil(
    //         self.registration.pushManager.getSubscription()
    //         .then(function(subscription) {
    //             return fetch('url')
    //                 .then(function(response) {
    //                     console.log(response);
    //                     console.log("Data teexxt 2 " + event.data.text());
    //                     return self.registration.showNotification('title', notificationOptions);
    //                 });
    //         })
    //     )
    event.waitUntil(
        self.registration.pushManager.getSubscription()
            .then(function (subscription) {
                //nhan notifi cua cac event
                //console.log("Data teexxt1 ");
                console.log("Data teexxt 2 " + event.data.text());
                var dataTextTem = event.data.text();
                dataTextTem = JSON.parse(dataTextTem);
                //kiem tra xem client co focus vao tab hay khong
                function isClientFocused(datat) {
                    let baseData = Base64.decode(datat);
                    //console.log("Data teexxt isClientFocused " + baseData);
                    let jsonData = JSON.parse(baseData);
                    let type = jsonData.type;
                    //console.log("Data teexxt isClientFocused " + type + " FROM " + dataTextTem.from);
                    if (type == 'event') {
                        notificationOptions.data.groupid = dataTextTem.from;
                        // notificationOptions.data.url = notificationOptions.data.url + '?uid=' + dataTextTem.from;
                        notificationOptions.data.url = notificationOptions.data.url;
                        return clients.matchAll({
                            type: 'window',
                            includeUncontrolled: true
                        })
                            .then((windowClients) => {
                                let clientIsFocused = false;
                                return clientIsFocused;
                            });
                    } else {
                        notificationOptions.data.groupid = dataTextTem.from;
                        notificationOptions.data.url = notificationOptions.data.url;
                        // notificationOptions.data.url = notificationOptions.data.url + '?uid=' + dataTextTem.from;
                        const urlToOpen = new URL(notificationOptions.data.url, self.location.origin).href;
                        return clients.matchAll({
                            type: 'window',
                            includeUncontrolled: true
                        })
                            .then((windowClients) => {
                                let clientIsFocused = false;

                                for (let i = 0; i < windowClients.length; i++) {
                                    const windowClient = windowClients[i];
                                    // if (windowClient.url === urlToOpen && windowClient.focused) {
                                    if (windowClient.url === urlToOpen) {
                                        clientIsFocused = true;
                                        break;
                                    }
                                }
                                //console.log("Data teexxt clients.matchAll " + clientIsFocused);
                                return clientIsFocused;
                            });
                    }

                }

                // const promiseChain = isClientFocused(dataTextTem.content)
                return isClientFocused(dataTextTem.content)
                    .then((clientIsFocused) => {
                        let baseData = Base64.decode(dataTextTem.content);
                        //console.log("Data teexxt isClientFocused " + baseData);
                        let jsonData = JSON.parse(baseData);
                        let type = jsonData.type;
                        let from = jsonData.from;
                        let subtype = dataTextTem.subtype;
                        var content = 'Bạn có tin nhắn mới...';
                        //console.log("DATA FROM SERVER " + baseData);
                        if (subtype === 'fbcomment') {
                            if ((jsonData.is_customer === true) || (jsonData.is_customer === 'true')) {
                                if (clientIsFocused) {
                                    return;
                                } else {
                                    notificationTitle = 'Bình luận mới trên facebook';
                                    notificationOptions.body = jsonData.message;
                                }
                                return self.registration.showNotification(
                                    notificationTitle, notificationOptions).then((event) => {
                                        //console.log("Event close");
                                        if (event && event.notification) {
                                            setTimeout(function () { event.notification.close(); }, 5000);
                                        }
                                    })
                            }

                        } else {
                            //console.log("DATA FROM EVENT  " + type);
                            if (type == 'event') {
                                switch (jsonData.name) {
                                    case "new_conversation":
                                        content = "Bạn có cuộc chat mới!";
                                        break;
                                    case "ai_customer_info":
                                        content = "Trợ lý thông minh đã lấy được thông tin khách hàng.";
                                        break;
                                    case "ai_dont_understand":
                                        content = "Trợ lý thông minh cần sự trợ giúp của bạn";
                                        break;
                                    case "ai_demand_request":
                                        content = "Bạn có nhu cầu mới từ khách hàng";
                                        break;
                                    default:
                                        content = "Bạn có tin nhắn mới...";
                                }
                                //console.log(content)
                                notificationTitle = 'THÔNG BÁO';
                                notificationOptions.body = content;
                                // event.waitUntil(
                                // return Promise.all([
                                //         self.registration.showNotification(
                                //             notificationTitle, notificationOptions),
                                //     ])
                                return self.registration.showNotification(
                                    notificationTitle, notificationOptions).then((event) => {
                                        //console.log("Event close");
                                        if (event && event.notification) {
                                            setTimeout(function () { event.notification.close(); }, 5000);
                                        }
                                    })
                                // );
                            } else { //type body
                                if (clientIsFocused) {
                                    return;
                                } else {
                                    notificationTitle = 'Bạn có tin nhắn mới';
                                    // console.log('DAY LA DU LIEU GI ' + JSON.stringify(jsonData.body));
                                    if (typeof(jsonData.body) === 'string') {
                                        notificationOptions.body = jsonData.body;
                                    } else {
                                        notificationOptions.body = 'Tin nhắn';
                                    }
                                    
                                }
                                return self.registration.showNotification(
                                    notificationTitle, notificationOptions).then((event) => {
                                        //console.log("Event close");
                                        if (event && event.notification) {
                                            setTimeout(function () { event.notification.close(); }, 5000);
                                        }
                                    })
                            }
                        }
                    });
            })
    )


});

// self.addEventListener('notificationclick', function(event) {
//   event.notification.close();

//   let clickResponsePromise = Promise.resolve();
//   if (event.notification.data && event.notification.data.url) {
//     clickResponsePromise = clients.openWindow(event.notification.data.url);
//   }

//   event.waitUntil(
//     Promise.all([
//       clickResponsePromise,
//       // self.analytics.trackEvent('notification-click'),
//     ])
//   );
// });

self.addEventListener('notificationclick', function (event) {
    //kiem tra xem client co focus vao tab hay khong
    event.notification.close();
    //console.log(event.notification.data.url);
    const urlToOpen = new URL(event.notification.data.url, self.location.origin).href;
    const promiseChain = clients.matchAll({
        type: 'window',
        includeUncontrolled: true
    })
        .then((windowClients) => {
            let matchingClient = null;

            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                if (windowClient.url === urlToOpen) {
                    matchingClient = windowClient;
                    break;
                }
            }

            if (matchingClient) {
                send_message_to_all_clients(event.notification.data.groupid);
                return matchingClient.focus();
            } else {
                return clients.openWindow(urlToOpen);
            }
        });
    return event.waitUntil(promiseChain);
});

self.addEventListener('notificationclose', function (event) {
    return event.waitUntil(
        Promise.all([
            // self.analytics.trackEvent('notification-close'),
        ])
    );
});

self.addEventListener('message', function (event) {
    //console.log("SW Received Message: " + event.data);
});
