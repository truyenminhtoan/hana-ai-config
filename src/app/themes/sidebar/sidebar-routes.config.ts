import { MenuType, RouteInfo } from './sidebar.metadata';

export const PAGE_MENU = [
  {
    title: 'Trợ lí',
    menuType: MenuType.LEFT,
    icon: 'face',
    path: 'agents/agentList',
    header_title: 'DANH SÁCH TRỢ LÍ',
    show: false
  },
  {
    title: 'Khái niệm',
    menuType: MenuType.LEFT,
    icon: 'timeline',
    path: 'entities/entityList',
    header_title: 'DANH SÁCH KHÁI NIỆM',
    show: true
  },
  {
    title: 'Thêm khái niệm',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'entities/entityNew',
    header_title: 'THÊM KHÁI NIỆM',
    show: false
  },
  {
    title: 'Cập nhật khái niệm',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'entities/entityEdit',
    header_title: 'CẬP NHẬT KHÁI NIỆM',
    show: false
  },
  {
    title: 'Sản phẩm',
    menuType: MenuType.LEFT,
    icon: 'featured_play_list',
    path: 'products/product-list',
    header_title: 'DANH SÁCH SẢN PHẨM',
    show: true
  },
  {
    title: 'Thêm sản phẩm',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'products/product-new',
    header_title: 'THÊM SẢN PHẨM',
    show: false
  },
  {
    title: 'Hành động',
    menuType: MenuType.LEFT,
    icon: 'developer_board',
    path: 'actions/actionList',
    header_title: 'DANH SÁCH HÀNH ĐỘNG',
    show: true
  },
  {
    title: 'Thêm hành động',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'actions/actionNew',
    header_title: 'THÊM HÀNH ĐỘNG',
    show: false
  },
  {
    title: 'Cập nhật hành động',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'actions/actionEdit',
    header_title: 'CẬP NHẬT HÀNH ĐỘNG',
    show: false
  },
  {
    title: 'Cập nhật product',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'products/product-detail',
    header_title: 'CẬP NHẬT SẢN PHẨM',
    show: false
  },
  {
    title: 'Ý định',
    menuType: MenuType.LEFT,
    icon: 'flight_takeoff',
    path: 'intents/intentList',
    header_title: 'DANH SÁCH Ý ĐỊNH',
    show: true
  },
  {
    title: 'Thêm ý định',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'intents/intentNew',
    header_title: 'THÊM Ý ĐỊNH',
    show: false
  },
  {
    title: 'Cập nhật ý định',
    menuType: MenuType.LEFT,
    icon: 'playlist_add',
    path: 'intents/intentEdit',
    header_title: 'CẬP NHẬT Ý ĐỊNH',
    show: false
  }
];

export const ROUTES: RouteInfo[] = [

];
