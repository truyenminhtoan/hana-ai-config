import {Component, OnInit, ViewChild} from '@angular/core';
import { PAGE_MENU } from './sidebar-routes.config';
import { UserService } from '../../_services/user.service';
import { AgentModel, UserModel } from '../../_models/user.model';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from '../../_services/compoent-interaction-service';
import {AgentListComponent} from '../../pages/agents/agent-list/agent-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import {NotificationService} from "../../_services/notification.service";
import {ApplicationService} from "../../_services/application.service";

declare var $: any;
@Component({
  selector: 'sidebar-cmp',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.scss'],
})
export class SideBarComponent implements OnInit {

  private eventSubscription: Subscription;

  public menuItems: any[];
  private agents: AgentModel[] = [];
  private currentUser: any;
  private currentAgent: AgentModel = new AgentModel();

  @ViewChild(AgentListComponent) agentList: AgentListComponent;

  constructor(
      private userService: UserService,
      private _componentService: ComponentInteractionService,
      private route: ActivatedRoute,
      private router: Router,
      private notificationService: NotificationService,
      private applicationService: ApplicationService){
  }

  public ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.menuItems = PAGE_MENU;
    if (this.currentUser !== undefined) {
      this.getListAgentByPartnerId(this.currentUser.partner_id);
      //Event listener
      this.eventSubscription = this._componentService.eventReceiver$.subscribe(
          event => {
            if (event == 'load-sidebar') {
              // update agent list
              if (this.currentUser.partner_id != undefined) {
                this.getListAgentByPartnerId(this.currentUser.partner_id);
              }
            }
          }
      );
    }
  }

  public updateCurrentAgent(id) {
    if (this.agents.length > 0) {
      for (var i =0 ; i< this.agents.length; i++) {
        if (this.agents[i].id == id) {
          this.currentAgent = this.agents[i];
          localStorage.setItem('currentAgent_Id', this.currentAgent.id);
          // Detect url, redirect page curent to url list
          if (this.router != undefined && this.router.url != null) {
            var list_tructs_url = this.router.url.split('/');
            var url = '/pages/agents/agentList';

            //console.log('list_tructs_url', list_tructs_url);
            if (list_tructs_url.length >= 3) {
              switch (list_tructs_url[2]) {
                case 'products':
                      url = '/pages/products/product-list';
                      this._componentService.eventPublisher$.next('load-products');
                      break;
                case 'actions':
                      url = '/pages/actions/actionList';
                      this._componentService.eventPublisher$.next('load-actions');
                  break;
                case 'entities':
                     url = '/pages/entities/entityList';
                     this._componentService.eventPublisher$.next('load-entities');
                  break;
                case 'intents':
                     url = '/pages/intents/intentList';
                     this._componentService.eventPublisher$.next('load-intents');
                  break;
              }
            }
            this.router.navigate([url], { relativeTo: this.route });
          }
        }
      }
    }
  }

  public getListAgentByPartnerId(partnerId: string) {
    let actionOperate: Observable<AgentModel[]>;
    actionOperate = this.userService.getListAgentByPartnerId(partnerId);
    // Subscribe to observable
    actionOperate.subscribe(
      (data) => {
        this.agents = data;
        if (this.agents.length > 0) {
          for (var i = 0; i < this.agents.length; i++) {
            this.agents[i].tmp_name = this.agents[i].name;
            if (this.agents[i].name != null) {
              this.agents[i].name = this.agents[i].name.slice(0, 17);
              if ( this.agents[i].name.length >= 17) {
                this.agents[i].name += '...';
              }
            }
          }
          this.agents.sort(function(a, b) {
            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            // names must be equal
            return 0;
          });
        }
        console.log('this.currentUser', this.currentUser);
        //this.currentAgent = this.agents[0];
        if (this.currentUser.app_using != undefined) {
          console.log("localStorage.getItem('currentAgent_Id')", localStorage.getItem('currentAgent_Id'));
          if (localStorage.getItem('currentAgent_Id') == 'NULL' || !localStorage.getItem('currentAgent_Id')) {
            // Trợ lý nào đang được sử dụng cho app đó thì mặc định chọn trợ lý đó
            this.applicationService.getAgentActiveByPartnerId(this.currentUser.partner_id)
                .then((agent_active) => {
                  console.log('agent_active', agent_active);
                  localStorage.setItem('currentAgent_Id', agent_active.agent_id);
                  if (this.agents.length > 0) {
                    this.currentAgent = this.agents[0];
                  }
                  for (var i =0 ; i< this.agents.length; i++) {
                    if (this.agents[i].id == agent_active.agent_id) {
                      this.currentAgent = this.agents[i];
                      break;
                    }
                  }
                });
          } else {
            if (this.agents.length > 0) {
                  this.currentAgent = this.agents[0];
            }
            for (var i =0 ; i< this.agents.length; i++) {
              if (this.agents[i].id == localStorage.getItem('currentAgent_Id')) {
                this.currentAgent = this.agents[i];
                break;
              }
            }
          }
        } else {
          this.notificationService.showWarning('Mời bạn chọn App');
          this.router.navigate(['/application-hana/application-list'], { relativeTo: this.route });
        }
      },
      (err) => {});
  }

  public emitLoadInfo() {
    if (this.router != undefined && this.router.url != null) {
      var list_tructs_url = this.router.url.split('/');
      if (list_tructs_url.length >= 3) {
        switch (list_tructs_url[2]) {
          case 'products':
            this._componentService.eventPublisher$.next('load-products');
            break;
          case 'actions':
            this._componentService.eventPublisher$.next('load-actions');
            break;
          case 'entities':
            this._componentService.eventPublisher$.next('load-entities');
            break;
          case 'intents':
            this._componentService.eventPublisher$.next('load-intents');
            break;
          default:
                
            break;    
        }
      }
    }
  }


  getLinkStyle(path:string):boolean {
    var check = false;
    switch (path) {
      case 'Sản phẩm':
          //console.log('this.router.url san pham', this.router.url);
          if (this.router.url == '/pages/products/product-new' || this.router.url == '/pages/products/product-list' || this.router.url.indexOf('products/product-detail') != -1) {
            check = true;
          }
          break;
      case 'Ý định':
        if (this.router.url == '/pages/intents/intentList' || this.router.url == '/pages/intents/intentNew' || this.router.url == '/pages/intents/intentNew/callback' || this.router.url.indexOf('pages/intents/intentEdit') != -1) {
          check = true;
        }
        break;
      case 'Hành động':
        if (this.router.url == '/pages/actions/actionList' || this.router.url == '/pages/actions/actionNew' || this.router.url.indexOf('pages/actions/actionEdit') != -1) {
          check = true;
        }
        break;
      case 'Trợ lí':
        if (this.router.url == '/pages/agents/agentList' || this.router.url == '/pages/agents/agentNew' || this.router.url.indexOf('pages/agents/agentEdit') != -1) {
          check = true;
        }
        break;
      case 'Khái niệm':
        if (this.router.url == '/pages/entities/entityList' || this.router.url == '/pages/entities/entityNew' || this.router.url.indexOf('pages/entities/entityEdit') != -1) {
          check = true;
        }
        break;
    }
    
    return check;

  }



}
