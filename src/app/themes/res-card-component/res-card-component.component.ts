import { Component, Input, Output, EventEmitter, ElementRef, ViewChild} from '@angular/core';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {EmitterService} from "../../../../../../../../_services/emitter.service";

@Component({
    selector: 'res-card-component',
    templateUrl: 'res-card-component.component.html',
    styleUrls: ['res-card-component.component.scss']
})

export class ResCardComponent {
    @Input() res: any;
    @Input() index: any;
    private is_block = false;
    private is_block_nutbam = false;
    @ViewChild('title_input') title_input: ElementRef;
    @Output() res_output = new EventEmitter<any>();

    @Input() index_nam: any;

    private is_block_80_title = false;
    @Input() is_block_80_subtitle = false;
    
    constructor(myElement: ElementRef) {

    }

    public addNutBam() {
        if (this.res.buttons.length < 3) {
            this.res.buttons.push({ type: 'postback', title: '', payload: '' });
            this.checkBlock();
        }
        this.emitSave();
    }

    public emitSave() {
        EmitterService.get('SAVE_LIST_CARD').emit([]);
    }

    public deleteButtonCard(button) {
        this.res.buttons = this.res.buttons.filter((row) => {
            return row !== button
        });
        this.checkBlock();
        this.emitSave();
    }
    
    public checkBlock() {
        if (this.res.buttons.length >= 3) {
            this.is_block_nutbam = true;
        } else {
            this.is_block_nutbam = false;
        }
    }
    public checkValidate() {
        let that = this;
        setTimeout(function () {
            that.checkTitle();
        }, 500);
        if (this.res != undefined) {
            this.res_output.emit(this.res);
        }

        //neu het du lieu khi backspace ti xoa luon red

    }

    public checkValidateAndCheckDeleteButton(event, button, title) {
        let that = this;
        setTimeout(function () {
            that.checkTitle();
        }, 500);
        if (this.res != undefined) {
            this.res_output.emit(this.res);
        }

        //neu het du lieu khi backspace ti xoa luon red
        console.log('event', event);
        if (event.key != undefined && event.key == 'Backspace' && title.trim().length == 0) {
            this.res.buttons = this.res.buttons.filter((row) => {
                return row !== button
            });
            this.res_output.emit(this.res);
            this.checkBlock();
        }
        this.emitSave();

    }
    public checkTitle() {
        var check_button = false;
        if (this.res.buttons.length > 0) {
            for (var k = 0; k < this.res.buttons.length; k++) {
                if (
                    (this.res.buttons[k].item_url != undefined && this.res.buttons[k].item_url.trim().length > 0) ||
                    (this.res.buttons[k].payload != undefined && this.res.buttons[k].payload.trim().length > 0) ||
                    (this.res.buttons[k].title != undefined && this.res.buttons[k].title.trim().length > 0)
                ) {
                    check_button = true;
                }
            }
        }
        if ((this.res.buttons.length > 0 && check_button)
        || (this.res.image_url != null && this.res.image_url.trim().length > 0)
        || (this.res.subtitle != null && this.res.subtitle.trim().length >0)) {
            if (this.res.title == null || (this.res.title != null && this.res.title.trim().length == 0)) {
                this.is_block = true;
            } else {
                this.is_block = false;

                if (this.res.title.trim().length > 80) {
                    this.is_block_80_title = true;
                } else {
                    this.is_block_80_title = false;
                }
                if (this.res.subtitle.trim().length > 80) {
                    this.is_block_80_subtitle = true;
                } else {
                    this.is_block_80_subtitle = false;;
                }
            }
        } else {
            this.is_block = false;
            this.is_block_80_title = false;
        }
        this.res_output.emit(this.res);
    }
}