import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ROUTES, PAGE_MENU } from '.././sidebar/sidebar-routes.config';
import { MenuType } from '.././sidebar/sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { UserModel } from '../../_models/user.model';
declare var $: any;

@Component({
  selector: 'navbar-cmp',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit {
  private listTitles: any[];
  private location: Location;
  private currentUser: any;
  private title_page: string;

  constructor(location: Location, private router: Router, private activatedRoute: ActivatedRoute) {
    this.location = location;
    router.events.subscribe((url: any) => url);
  }
  // tslint:disable-next-line:one-line
  public ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.listTitles = PAGE_MENU.filter((listTitle) => listTitle.menuType !== MenuType.BRAND);
    this.title_page = this.getTitle();
  }
  public ngAfterViewInit(): void {
    
  }
  // tslint:disable-next-line:one-line
  public getTitle() {
    let title;
    let titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(8);
    }
    this.listTitles.filter((item) => {
      if (item.children) {
        let name = item.children.filter((it) => it.path === titlee)[0];
        if (name) {
          title = name.header_title;
          return;
        }
      } else {
        if (item.path === titlee || titlee.indexOf(item.path) != -1) {
          title = item.header_title;
          return;
        }
      }
    });

    return title;
  }
  public getPath() {
    return this.location.prepareExternalUrl(this.location.path());
  }
  public logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
  }
  public lock() {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    user.is_lock = true;
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.router.navigate(['/lock'], { queryParams: { returnUrl: this.router.url } });
  }
  // tslint:disable-next-line:eofline
  

}
