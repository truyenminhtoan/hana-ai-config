import { AbstractControl } from '@angular/forms';

export class Validator {
    // public static URL_REGEXP = /(((http|https|ftp)\:\/\/)|((www)\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\.\_\?\,\'\/\\\+&amp;%\$\#\=\~])*/i;
    public static URL_REGEXP = /((http|https|ftp)\:\/\/)?(?:\S+(?:\S*)?@)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\.\_\?\,\'\/\\\+&amp;%\$\#\=\~])*/i;
    public static  EMAIL_REGEXP = /[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*/i;
    public static isURL(value) {
        return this.URL_REGEXP.test(value);
    }

    public static isEmail(value) {
        return this.EMAIL_REGEXP.test(value);
    }
}
