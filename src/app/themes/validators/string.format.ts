import { latinMap } from './latin.map';
import date from '../../../../node_modules/date-and-time/date-and-time.min.js';
import _ from 'underscore';
export class StringFormat {
  // tslint:disable-next-line:member-access
  static latinMap: any = latinMap;

  public static isNullOrEmpty(text: string) {
    return (text === null || typeof (text) === 'undefined' || !text);
  }
  public static clearTextVi(text: string) {
    if (this.isNullOrEmpty(text)) {
      return '';
    }
    text = text.trim();
    text = text.toLowerCase();
    text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    text = text.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    text = text.replace(/đ/g, 'd');
    return text;
  }
  public static formatText(text: string) {
    if (this.isNullOrEmpty(text)) {
      return '';
    }
    text = text.trim();
    text = text.toLowerCase();
    text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    text = text.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    text = text.replace(/đ/g, 'd');
    text = text.replace(/[^a-zA-Z0-9-_]/g, '_');
    text = text.replace(/_+_/g, '_'); // thay thế 2_ thành 1_
    return text;
  }

  public static formatTextUserSays(text: string) {
    if (this.isNullOrEmpty(text)) {
      return '';
    }
    text = text.trim();
    text = text.toLowerCase();
    text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    text = text.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    text = text.replace(/đ/g, 'd');

    return text;
  }

  public static formatTextProduct(text: string) {
    if (this.isNullOrEmpty(text)) {
      return '';
    }
    text = text.trim();
    text = text.toLowerCase();
    text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    text = text.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    text = text.replace(/đ/g, 'd');
    text = text.replace(/[^a-zA-Z0-9-_]/g, '_');
    text = text.replace(/-/g, '');
    text = text.replace(/_+_/g, '_'); // thay thế 2_ thành 1_
    return text;
  }

  // private alphabet = '[àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-z0-9_]';

  public static checkSpecialChar(content) {
    let alphabet = /[^àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-z0-9_]/g;
    return alphabet.test(content);
  }

  public static hashTagRegex(text) {
    if (this.isNullOrEmpty(text)) {
      return '';
    }
    text = text.trim();
    let regexp = /#[1-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+/gi;
    return text.match(regexp);
  }

  // LOAI BO ALL KY TU DAT BIET
  public static latinize(str: string) {
    // tslint:disable-next-line:only-arrow-functions
    return str.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
      return StringFormat.latinMap[a] || a;
    });
  }
  // ESCAP KY TU DAC BIET
  public static escapeRegexp(queryToEscape: string) {
    // tslint:disable-next-line:max-line-length
    // Regex: capture the whole query string and replace it with the string that will be used to match
    // the results, for example if the capture is 'a' the result will be \a
    return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
  }

  // Kiem tra neu chua ki tu dac biet => true, else false
  public static checkRegexp(queryToEscape: string, regex: any) {
    return regex.test(queryToEscape);
  }

  public static tokenize(str: string, wordRegexDelimiters: string = ' ',
    phraseRegexDelimiters: string = ''): string[] {
    let regexStr: string = '(?:[' + phraseRegexDelimiters + '])([^' +
      // tslint:disable-next-line:max-line-length
      phraseRegexDelimiters + ']+)(?:[' + phraseRegexDelimiters + '])|([^' + wordRegexDelimiters + ']+)';
    let preTokenized: string[] = str.split(new RegExp(regexStr, 'g'));
    let result: string[] = [];
    let preTokenizedLength: number = preTokenized.length;
    let token: string;
    let replacePhraseDelimiters = new RegExp('[' + phraseRegexDelimiters + ']+', 'g');

    for (let i = 0; i < preTokenizedLength; i += 1) {
      token = preTokenized[i];
      if (token && token.length && token !== wordRegexDelimiters) {
        result.push(token.replace(replacePhraseDelimiters, ''));
      }
    }
    return result;
  }
  // SORT OBJECT
  public static compareValues(key, order = 'asc') {

    // const bands = [
    // { genre: 'Rap', band: 'Migos', albums: 2},
    // { genre: 'Pop', band: 'Coldplay', albums: 4, awards: 13},
    // { genre: 'Rock', band: 'Breaking Benjamins', albums: 1}
    // ];
    // // array được sắp xếp theo band nhạc, mặc định theo thứ tự tăng dần
    // bands.sort(StringFormat.compareValues('band'));
    // // array được sắp xếp theo band nhạc, thứ tự giảm dần
    // bands.sort(StringFormat.compareValues('band', 'desc'));
    // // array được sắp xếp theo album, thứ tự tăng dần
    // bands.sort(StringFormat.compareValues('albums'));

    // tslint:disable-next-line:only-arrow-functions
    return function (a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // không tồn tại tính chất trên cả hai object
        return 0;
      }
      const varA = (typeof a[key] === 'string') ?
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ?
        b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  public static dateToString(date, format) {
    return date.format(date, format);
  }
  public static stringToDate(dateString, format) {
    return date.parse(dateString, format);
  }

  public static generateName(name, object, param) {
    let nameArr = _.pluck(object, param);
    let nameStr = nameArr.join(',');
    let next = true;
    let nameTemp = '';
    let i = 1;
    while (next) {
      if (nameStr.indexOf(nameTemp) > -1) {
        nameTemp = name + ' ' + i++;
      } else {
        next = false;
      }
    }
    return nameTemp;
  }

  public static generateTagName(name, object, param) {
    let nameArr = _.pluck(object, param);
    let nameStr = nameArr.join(',');
    let next = true;
    let nameTemp = '';
    let i = 1;
    while (next) {
      if (nameStr.indexOf(nameTemp) > -1) {
        nameTemp = name + '_' + i++;
      } else {
        next = false;
      }
    }
    return nameTemp;
  }

  public static zipJson(data) {
    let _keys = _.allKeys(data[0]);
    let _values = [];
    _.filter(data, (item) => {
      let arr = [];
      _.filter(_keys, (key) => {
        let temp = _.propertyOf(item)(key) || '';
        arr.push(temp);
      });
      _values.push(arr);
    });
    return { keys: _keys, values: _values };
  }

  public static unzipJson(data) {
    let json = [];
    let keys = data.keys;
    _.filter(data.values, (item) => {
      let obj: any = {};
      _.filter(item, (it, idx, b) => {
        obj[keys[idx]] = it;
      });
      json.push(obj);
    });
    return json;
  }

}
