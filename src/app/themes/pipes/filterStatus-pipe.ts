import { PipeTransform } from '@angular/core';
import { Pipe } from '@angular/core';

// tslint:disable-next-line:pipe-naming
// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({ name: 'filterStatus' })
// tslint:disable-next-line:one-line
export class FilterStatusPipe {
    // tslint:disable-next-line:member-access
    transform(data: any[]) {
        return data.filter((item) => item.status === 1);
    }
}
