import { PipeTransform } from '@angular/core';
import { Pipe } from '@angular/core';

// tslint:disable-next-line:pipe-naming
// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({ name: 'truncate' })
// tslint:disable-next-line:one-line
export class TruncatePipe {
  // tslint:disable-next-line:member-access
  transform(value: string, limit: number = 40, trail: String = '…'): string {
    if (!value) {
      return '';
    }
    if (limit < 0) {
      limit *= -1;
      return value.length > limit ? trail + value.substring(value.length - limit, value.length) : value;
    } else {
      return value.length > limit ? value.substring(0, limit) + trail : value;
    }
  }
}
