import { PipeTransform } from '@angular/core';
import { Pipe } from '@angular/core';

// tslint:disable-next-line:pipe-naming
// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({ name: 'fmGender' })
// tslint:disable-next-line:one-line
export class FormatGender {
  // tslint:disable-next-line:member-access
  transform(value: string): string {
    if (value === 'male') {
      return 'Nam';
    } else if (value === 'female') {
      return 'Nữ';
    } else {
      return '';
    }
  }
}
