import { BaResponseEditComponent } from './components/ba-faq-v2/components/ba-response-edit/ba-response-edit.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// import { ChipsModule } from 'primeng/primeng';
import { ChipsModule } from '../../assets/plugins/primeng/primeng.js';
import { MatAutocompleteModule, MatMenuModule, MatSliderModule, MatDatepickerModule, MatSelectModule, MatChipsModule, MatDialogModule, MatPaginatorModule, MatButtonToggleModule, MatButtonModule, MatSlideToggleModule, MatRadioModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import {
    BaContainerComponent,
    BaMainComponent,
    CheckBoxComponent,
    BaCardComponent,
    BaAutocompleteComponent,
    BaMenuComponent,
    BaModalComponent,
    BaModalSuggestionComponent,
    BaAutocompleteActionComponent,
    BaAutocompleteIntentComponent,
    BaAutocompleteProductComponent,
    NavbarComponent,
    BaTrashPopupComponent,
    BaFaqComponent,
    BaFaqV2Component,
    BaFallbackComponent,
    BaChipsComponent,
    BaSidebarRemarketingComponent,
    BaQueryBuilderComponent,
    BaQueryBuilderItemComponent,
    GuideComponent,
    BaSwitchComponent,
    BaSwitch2Component,
    BaStructuredComponent,
    ResCardComponent,
    BaFilteredComponent,
    BaPictureUploadComponent,
    AgentDeleteComponent,
    BaDrapDateComponent
} from './components';
import { BaAutocompleteActionBasicComponent } from './components/ba-autocomplete-action-basic/ba-autocomplete.component';
import { BaAutocompleteActionSearchTypeComponent } from './components/ba-autocomplete-action-search-type/ba-autocomplete.component';
import { BaAutocompleteIntentInnerComponent } from './components/ba-autocomplete-intent-inner_action/ba-autocomplete.component';
import { ConfigService } from '../_services/config.service';
import { TruncatePipe } from './pipes/truncate-pipe';
import { TagInputModule } from 'ng2-tag-input';
import { AutoCompleteModule } from 'primeng/primeng';
import { Ng2DropdownModule } from 'ng2-material-dropdown';
// import { ClipboardModule } from 'ngx-clipboard';
import { ClipboardModule } from 'ng2-clipboard';
import { InputTextareaModule } from 'primeng/primeng';
import { JsonDialogComponent } from './components/ba-structured/components/json-dialog/json-dialog.component';
import { BaFaqEditComponent } from './components/ba-faq-v2/components/ba-faq-edit/ba-faq-edit.component';
import { bootloader } from '@angularclass/hmr';
import { NotificationService } from '../_services/notification.service';
import { AddEntityProductComponent } from './components/add-entity-product/add-entity-product';
import { ValidationService } from '../_services/ValidationService';
import { IntentDeleteConponent } from './components/intent-delete/intent-delete.component';
import { EntityDeleteComponent } from './components/entity-delete/entity-delete.component';
import { ProductDeleteComponent } from './components/delete-product/delete-product.component';
import { ActionDeleteComponent } from './components/action-delete/action-delete.component';
import { UserDontunderstandDeleteComponent } from './components/user-dontunderstand-delete/user-dontunderstand-delete.component';
import { BaFileUploadComponent } from './components/ba-file-upload/ba-file-upload.component';
import { FormatGender } from './pipes/formatGender-pipe';
import { Autosize } from 'angular2-autosize';
import { ApplicationService } from '../_services/application.service';
import { BaStructuredIntentComponent } from './components/ba-structured-intent/ba-structured.component';
import { ResCardSingleComponent } from './components/ba-res-card-single/res-card.component';
import { BaContextComponent } from './components/ba-context/ba-contenxt.component';
import { DragDropModule, DataTableModule, OrderListModule, SharedModule, ConfirmDialogModule } from 'primeng/primeng';
import { BaButtonBlockComponent } from './components/ba-button-block/ba-button-block.component';
import { DetailButtonDialogComponent } from './components/ba-button-block/components/detail-button-dialog/detail-button-dialog.component';
import { BaButtonGalleryComponent } from './components/ba-button-gallery/ba-button-block.component';
import { DetailButtonGalleryDialogComponent } from './components/ba-button-gallery/components/detail-button-dialog/detail-button-dialog.component';
import { BaButtonQuickReplyComponent } from './components/ba-button-quick-reply/ba-button-block.component';
import { DetailButtonQuickReplyDialogComponent } from './components/ba-button-quick-reply/components/detail-button-dialog/detail-button-dialog.component';
import { BaSortListComponent } from './components/ba-sort-list/ba-sort-list.component';
import { LengthLimitCapitalizePipe } from '../_services/custompipes/lengthlimit.pipe';
import { MysqlTimeToStringPipe } from '../_services/custompipes/mysqltimetostring.pipe';
import { AddHyperlinksPipe } from '../_services/custompipes/addHyperlinks.pipe';
import { AddEmoticonsPipe } from '../_services/custompipes/addEmoticons.pipe';
import { DragulaModule } from 'ng2-dragula/components/dragular.module';
import { FbPopupComponent } from './components/ba-facebook-popup/fbpopup.component';
import { BaIntentActionComponent } from './components/ba-intent-action';
import { DetailIntentActionDialogComponent } from "./components/ba-intent-action/components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import { DetailIntentActionDialogComponentV2 } from "./components/ba-intent-action-v2/components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import { BaIntentActionComponentV2 } from "./components/ba-intent-action-v2/ba-intent-action.component";
import { DetailIntentActionDialogComponentV3 } from "./components/ba-intent-action-v3/components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import { BaIntentActionComponentV3 } from "./components/ba-intent-action-v3/ba-intent-action.component";
import { FilterStatusPipe } from './pipes/filterStatus-pipe';
import { Log } from '../_services/log.service';
import {DetailCommentInboxDialogComponent} from "./components/ba-comment-inbox/components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import {BaCommentInboxComponent} from "./components/ba-comment-inbox/ba-intent-action.component";

const NGA_COMPONENTS = [
    BaContainerComponent,
    BaMainComponent,
    CheckBoxComponent,
    BaCardComponent,
    BaAutocompleteComponent,
    BaMenuComponent,
    BaModalComponent,
    BaModalSuggestionComponent,
    BaAutocompleteActionComponent,
    BaAutocompleteActionBasicComponent,
    BaAutocompleteIntentComponent,
    BaAutocompleteActionSearchTypeComponent,
    BaAutocompleteIntentInnerComponent,
    BaAutocompleteProductComponent,
    NavbarComponent,
    BaTrashPopupComponent,
    BaFaqComponent,
    BaFaqV2Component,
    BaFallbackComponent,
    BaChipsComponent,
    BaSidebarRemarketingComponent,
    BaQueryBuilderComponent,
    BaQueryBuilderItemComponent,
    TruncatePipe,
    FormatGender,
    GuideComponent,
    BaSwitchComponent,
    BaSwitch2Component,
    BaStructuredComponent,
    ResCardComponent,
    BaFilteredComponent,
    JsonDialogComponent,
    BaPictureUploadComponent,
    AgentDeleteComponent,
    AddEntityProductComponent,
    IntentDeleteConponent,
    EntityDeleteComponent,
    ProductDeleteComponent,
    ActionDeleteComponent,
    UserDontunderstandDeleteComponent,
    BaFileUploadComponent,
    BaStructuredIntentComponent,
    ResCardSingleComponent,
    BaContextComponent,
    Autosize,
    BaFaqEditComponent,
    BaResponseEditComponent,
    BaDrapDateComponent,
    BaButtonBlockComponent,
    DetailButtonDialogComponent,
    BaButtonGalleryComponent,
    DetailButtonGalleryDialogComponent,
    BaButtonQuickReplyComponent,
    DetailButtonQuickReplyDialogComponent,
    BaSortListComponent,
    FbPopupComponent,
    LengthLimitCapitalizePipe,
    FilterStatusPipe,
    MysqlTimeToStringPipe,
    AddHyperlinksPipe,
    AddEmoticonsPipe,
    BaIntentActionComponent,
    DetailIntentActionDialogComponent,
    BaIntentActionComponentV3,
    DetailIntentActionDialogComponentV3,
    BaIntentActionComponentV2,
    DetailIntentActionDialogComponentV2,
    BaCommentInboxComponent,
    DetailCommentInboxDialogComponent
];

@NgModule({
    declarations: [
        ...NGA_COMPONENTS
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        ChipsModule,
        TagInputModule,
        AutoCompleteModule,
        Ng2DropdownModule,
        ClipboardModule,
        InputTextareaModule,
        MatAutocompleteModule,
        MatMenuModule,
        MatSliderModule,
        MatDatepickerModule,
        MatSelectModule,
        MatChipsModule,
        MatDialogModule,
        MatPaginatorModule,
        MatButtonToggleModule,
        MatButtonModule,
        MatSlideToggleModule,
        DataTableModule,
        SharedModule,
        MatIconModule,
        DragulaModule,
        MatRadioModule
    ],
    providers: [
        ConfigService,
        Log
    ],
    exports: [
        ...NGA_COMPONENTS
    ],
    bootstrap: [
        JsonDialogComponent,
        BaFaqEditComponent,
        BaResponseEditComponent,
        DetailButtonDialogComponent,
        DetailButtonGalleryDialogComponent,
        DetailButtonQuickReplyDialogComponent,
        BaSortListComponent,
        BaIntentActionComponent,
        DetailIntentActionDialogComponent,
        BaIntentActionComponentV3,
        DetailIntentActionDialogComponentV3,
        BaIntentActionComponentV2,
        DetailIntentActionDialogComponentV2,
        BaCommentInboxComponent,
        DetailCommentInboxDialogComponent
    ]
})

export class NgaModule {
    // tslint:disable-next-line:member-access
    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
            ngModule: NgaModule,
            providers: [NotificationService, ApplicationService],
        };
    }
    // tslint:disable-next-line:eofline
}