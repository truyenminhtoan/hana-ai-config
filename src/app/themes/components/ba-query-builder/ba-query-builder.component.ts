import { OnChanges } from 'angular2-color-picker/node_modules/@angular/core/src/metadata/lifecycle_hooks';
import {
    NotificationService
} from './../../../_services/notification.service';
import {
    Observable
} from 'rxjs/Observable';
import {
    EmitterService
} from './../../../_services/emitter.service';
import {
    Component,
    OnInit,
    ViewEncapsulation,
    Input,
    AfterViewInit,
    NgZone,
    AfterContentInit,
    Output,
    EventEmitter
} from '@angular/core';
import * as moment from 'moment';
import { MdDialog } from '@angular/material';
import { FilterDialogComponent } from "./filter-dialog/filter-dialog.component";
declare var jQuery: any;
declare var $: any;
@Component({
    selector: 'ba-query-builder',
    templateUrl: 'ba-query-builder.component.html',
    styleUrls: ['ba-query-builder.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class BaQueryBuilderComponent implements OnInit, OnChanges {
    @Input() private searchTypeData;
    @Input() private conditionsSaved;
    @Input() private condition_default;
    @Output() private outputFilter: any = new EventEmitter<any>();

    private conditions;
    private fields;
    private condition: any;
    private is_close = true;

    private condition_pre: any;

z
    constructor(private notificationService: NotificationService, private dialog: MdDialog) {

    }

    public ngOnInit(): void {
        EmitterService.get('DELETE_QUERY_ITEM').subscribe((data: any) => {
            this.conditions.splice(data, 1);
            this.is_close = this.checkValidate();
        });
        if (!this.conditions) {
            this.conditions = [];
        }
        if (this.condition_default !== undefined) {
            this.condition = this.condition_default;
        } else {
            this.condition = {
                "syntax": "VÀ",
                "field": {
                    "value": "cus.full_name",
                    "label": "Tên khách hàng"
                },
                "operator": {
                    "value": "ct",
                    "label": "chứa"
                },
                "value": {
                    "value": "",
                    "label": ""
                }
            };
        }

        /** ham lay du lieu condition da luu truoc do neu co */
        this.getConditionSaved();
    }

    public ngOnChanges(): void {
        this.conditions = [];
        // if (!this.conditions) {
        //     this.conditions = [];
        // }
        // if (this.condition_default !== undefined) {
        //     this.condition = this.condition_default;
        // } else {
        //     this.condition = {
        //         "syntax": "VÀ",
        //         "field": {
        //             "value": "cus.full_name",
        //             "label": "Tên khách hàng"
        //         },
        //         "operator": {
        //             "value": "ct",
        //             "label": "chứa"
        //         },
        //         "value": {
        //             "value": "",
        //             "label": ""
        //         }
        //     };
        // }

        /** ham lay du lieu condition da luu truoc do neu co */
        this.getConditionSaved();
    }

    public addCondition() {
        this.getSearchType();

        this.is_close = this.checkValidate();
    }

    public getSearchType() {
        if (this.searchTypeData !== undefined) {
            let searchType = [];
            this.searchTypeData.filter((item) => {
                let search: any = {
                    id: item.param,
                    type: item.value_type,
                    label: item.name
                };
                if (item.value_type === 'list') {
                    if (item.page_values.length > 0) {
                        search.list = item.page_values;
                        searchType.push(search);
                    }
                } else {
                    searchType.push(search);
                }
            });
            this.fields = searchType;
            this.conditions.push({
                fields: this.fields,
                condition: this.condition,
            });
        }
    }

    public getConditionSaved() {
        if (this.searchTypeData != undefined) {

            for (let i = 0; i < this.searchTypeData.length; i++) {
                if (this.searchTypeData[i].syntax == undefined || (this.searchTypeData[i].syntax != undefined && this.searchTypeData[i].syntax.trim().length == 0)) {
                    this.searchTypeData[i].syntax = 'and';
                    this.searchTypeData[i].syntax_display = 'VÀ';
                }
            }

            let searchType = [];
            this.searchTypeData.filter((item) => {
                let search: any = {
                    id: item.param,
                    type: item.value_type,
                    label: item.name
                };
                if (item.value_type === 'list') {
                    if (item.page_values.length > 0) {
                        search.list = item.page_values;
                        searchType.push(search);
                    }
                } else {
                    searchType.push(search);
                }
            });
            this.fields = searchType;

            for (let i = 0; i < this.conditionsSaved.length; i++) {
                this.conditions.push(
                    {
                        condition: this.conditionsSaved[i],
                        fields: this.fields,
                    }
                );
            }

            this.conditions.filter((item) => {
                if (item.condition.field.value === 'cus.gender') {
                    item.condition.operator.value = 'in';
                } else if (item.condition.field.value === 'pcg1.app_group_id' && item.condition.operator.value === '=') {
                    item.condition.operator.value = 'in';
                }
            });

            let tmp = JSON.stringify(this.conditions);
            this.condition_pre = JSON.parse(tmp);
        }

        // if (this.conditions.length == 0) {
        //     this.addCondition();
        // }
    }

    public output(i, data) {
        this.conditions[i].condition = data;

        this.is_close = this.checkValidate();
    }

    public checkValidate() {
        let validated = true;
        if (this.conditions.length > 0) {
            this.conditions.filter((item) => {
                if (!item.condition) {
                    return false;
                }
                if (!item.condition.operator) {
                    return false;
                }

                if (item.condition.operator.value !== 'null' && item.condition.operator.value !== 'nn') {
                    if (item.condition.operator.value == 'nbw' || item.condition.operator.value == 'bw') {
                        if (item.condition.value != undefined) {
                            if (!item.condition.value || !item.condition.value.value) {
                                validated = false;
                            }
                            if (!item.condition.value || !item.condition.value.value2) {
                                validated = false;
                            }
                            let from = moment(item.condition.value.value).format('YYYY-MM-DD');
                            let to = moment(item.condition.value.value2).format('YYYY-MM-DD');
                            if (from > to) {
                                validated = false;
                            }
                        } else {
                            validated = false;
                        }
                    } else {
                        if (!item.condition.value || !item.condition.value.value) {
                            validated = false;
                        }
                    }
                }
            });
        }

        return validated;
    }

    public filter() {
        let validated = true;
        for (let i = 0; i < this.conditions.length; i++) {
            let item = this.conditions[i];
            if (!item.condition) {
                this.notificationService.showDanger('Bạn chưa chọn đầy đủ thông tin cho điều kiện lọc!');
                validated = false;
                return;
            }
            if (!item.condition.operator) {
                this.notificationService.showDanger('Bạn chưa chọn đầy đủ thông tin cho điều kiện lọc!');
                validated = false;
                return;
            }
            if (item.condition.operator.value !== 'null' && item.condition.operator.value !== 'nn') {
                if (item.condition.operator.value == 'nbw' || item.condition.operator.value == 'bw') {
                    if (!item.condition.value || !item.condition.value.value) {
                        this.notificationService.showDanger('Bạn chưa chọn đầy đủ thông tin cho điều kiện lọc!');
                        validated = false;
                        return;
                    }
                    if (!item.condition.value || !item.condition.value.value2) {
                        this.notificationService.showDanger('Bạn chưa chọn đầy đủ thông tin cho điều kiện lọc!');
                        validated = false;
                        return;
                    }
                    let from = moment(item.condition.value.value).format('YYYY-MM-DD');
                    let to = moment(item.condition.value.value2).format('YYYY-MM-DD');
                    if (from > to) {
                        this.notificationService.showDanger('Thời gian bắt đầu không được lớn hơn thời gian kết thúc!');
                        validated = false;
                        return;
                    }
                } else {
                    if (!item.condition.value || (!item.condition.value.value && item.condition.value.value !== 0)) {
                        this.notificationService.showDanger('Bạn chưa chọn đầy đủ thông tin cho điều kiện lọc!');
                        validated = false;
                        return;
                    }
                }
            }
        }
        // this.conditions.filter((item) => {
        //     if (!item.condition) {
        //         this.notificationService.showDanger('Bạn chưa chọn tham số');
        //         validated = false;
        //         return;
        //     }
        //     if (!item.condition.operator) {
        //         this.notificationService.showDanger('Bạn chưa chọn loại tìm kiếm');
        //         validated = false;
        //         return;
        //     }
        //     if (item.condition.operator.value !== 'null' && item.condition.operator.value !== 'nn') {
        //         if (item.condition.operator.value == 'nbw' || item.condition.operator.value == 'bw') {
        //             if (!item.condition.value || !item.condition.value.value) {
        //                 this.notificationService.showDanger('Bạn chưa chọn tham số tìm kiếm');
        //                 validated = false;
        //                 return;
        //             }
        //             if (!item.condition.value || !item.condition.value.value2) {
        //                 this.notificationService.showDanger('Bạn chưa chọn tham số tìm kiếm');
        //                 validated = false;
        //                 return;
        //             }
        //             let from = moment(item.condition.value.value).format('YYYY-MM-DD');
        //             let to = moment(item.condition.value.value2).format('YYYY-MM-DD');
        //             if (from > to) {
        //                 this.notificationService.showDanger('Thời gian bắt đầu không được lớn hơn thời gian kết thúc');
        //                 validated = false;
        //                 return;
        //             }
        //         } else {
        //             if (!item.condition.value || (!item.condition.value.value && item.condition.value.value !== 0)) {
        //                 this.notificationService.showDanger('Bạn chưa chọn tham số tìm kiếm');
        //                 validated = false;
        //                 return;
        //             }
        //         }
        //     }
        // });

        if (validated) {
            $("#btn_close").click();
            this.out();
        }
    }

    public deleteCondition(condition) {
        // this.conditions = this.conditions.filter((item) => {
        //     return item !== condition;
        // });
        this.conditions.splice(condition, 1);
        this.condition_pre.splice(condition, 1);
        this.is_close = this.checkValidate();
        this.filter();
    }

    private out() {
        let tmp = JSON.stringify(this.conditions);
        this.condition_pre = JSON.parse(tmp);
        /** dinh dang lai du lieu cho dau ra trc khi emit*/
        let outputEmits = [];
        this.conditions.filter((it) => {
            let syntax = '';
            let syntax_display = '';
            if (it.condition.syntax != undefined) {
                syntax = it.condition.syntax;
            }
            if (it.condition.syntax != undefined) {
                if (it.condition.syntax == 'or') {
                    syntax_display = 'HOẶC';
                } else {
                    syntax_display = 'VÀ';
                }
            }
            if (syntax == 'VÀ') {
                syntax = 'and';
            }
            let obj = {
                "syntax": syntax,
                "syntax_display": syntax_display,
                "field": { "value": it.condition.field.value, "label": it.condition.field.label },
                "operator": { "value": it.condition.operator.value, "label": it.condition.operator.label },
                "value": it.condition.value
            };
            outputEmits.push(obj);
        });

        let dataOutPut = {
            'query_filter': outputEmits,
            'conditions': this.conditions,
        };
        this.outputFilter.emit(dataOutPut);
    }

    formatValue(value) {

        // if (Number.isInteger(value)) {
        //     return value;
        // } else {
        //     let check = moment(value).format('DD-MM-YYYY');
        //     if (check != 'Invalid date') {
        //         return check;
        //     }
        // }

        return value;
    }

    resestFilterAndClose() {
        //this.conditions = [];
        //this.is_close = true;
        //$("#myModal").hide();

        let tmp = JSON.stringify(this.condition_pre);
        this.conditions = JSON.parse(tmp);

        // console.log('condition_pre', this.condition_pre);
        // console.log('conditions', this.conditions);

        /** dinh dang lai du lieu cho dau ra trc khi emit*/
        let outputEmits = [];
        this.condition_pre.filter((it) => {
            let syntax = '';
            let syntax_display = '';
            if (it.condition.syntax != undefined) {
                syntax = it.condition.syntax;
            }
            if (it.condition.syntax != undefined) {
                if (it.condition.syntax == 'or') {
                    syntax_display = 'HOẶC';
                } else {
                    syntax_display = 'VÀ';
                }
            }
            if (syntax == 'VÀ') {
                syntax = 'and';
            }

            if (it.condition !== undefined) {
                let obj = {
                    "syntax": syntax,
                    "syntax_display": syntax_display,
                    "field": { "value": it.condition.field.value, "label": it.condition.field.label },
                    "operator": { "value": it.condition.operator.value, "label": it.condition.operator.label },
                    "value": it.condition.value
                };
                outputEmits.push(obj);
            }

        });
        let dataOutPut = {
            'query_filter': outputEmits,
            'conditions': this.condition_pre,
        };
        this.outputFilter.emit(dataOutPut);


    }

    private showFilter() {
        let dialogRef = this.dialog.open(FilterDialogComponent, {
            width: '30%',
            data: {
                // customers: this.selectedCustomers,
            }
        });

    }

    private showPopupFilter() {
        if (this.conditions.length == 0) {
            this.condition_pre = [];
            this.addCondition();
        }
        // console.log('condition_pre', this.condition_pre);
        // console.log('conditions', this.conditions);

    }

}