import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import {NotificationService} from "../../../../_services/notification.service";

@Component({
    selector: 'filter-dialog',
    templateUrl: 'filter-dialog.component.html',
    styleUrls: ['filter-dialog.component.scss']
})
export class FilterDialogComponent {
    private fromDate;
    private toDate;
    constructor( @Inject(MD_DIALOG_DATA) public data: any, private notificationService: NotificationService, public dialogRef: MdDialogRef<FilterDialogComponent>) {

    }

    public search() {
        if (!this.fromDate) {
            return this.notificationService.showDanger('Bạn vui lòng nhập ngày bắt đầu');
        }
        if (!this.fromDate) {
            return this.notificationService.showDanger('Bạn vui lòng nhập ngày kết thúc');
        }
        this.dialogRef.close({ from_date: this.fromDate, to_date: this.toDate });
    }

}
