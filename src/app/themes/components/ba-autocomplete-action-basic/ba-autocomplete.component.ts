
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ActionBasic} from "../../../_models/actions.model";

@Component({
  selector: 'ba-autocomplete-action-basic',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteActionBasicComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Output() output = new EventEmitter<any>();
  @Input() actionBasics: ActionBasic[] = [];
  @Input() selected: any;
  @Input() set_disabled: any;
  private disabled = false;
  private selectDefault = 'search';
  
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;

  constructor() {
    var that = this;
    setTimeout(() => {
      if (that.set_disabled != undefined) {
        that.disabled = true;
      }
    }, 500);
  }

  private change(event: any) {
    this.output.emit(event.value);
  }
  
}

