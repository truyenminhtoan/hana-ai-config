import { TagModel } from 'ng2-tag-input/dist/modules/core';
import { BaResponseEditComponent } from './../ba-faq-v2/components/ba-response-edit/ba-response-edit.component';
import { MdDialog } from '@angular/material';
import { Component, Input, AfterViewInit, EventEmitter, Output, OnInit, SimpleChanges } from '@angular/core';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Observable';
import { IntentService } from '../../../_services/intents.service';
import { OnChanges } from 'angular2-color-picker/node_modules/@angular/core/src/metadata/lifecycle_hooks';
import { NotificationService } from '../../../_services/notification.service';
declare var Taggle: any;
declare var $: any;
@Component({
  selector: 'ba-fallback',
  templateUrl: 'ba-fallback.component.html',
  styleUrls: ['ba-fallback.component.scss']
})
export class BaFallbackComponent implements OnInit, OnChanges {
  @Input() private currentIntent: any = {};
  @Input() private blocks: any;
  @Input() private typeFaq: any;
  @Output() private output = new EventEmitter<any>();
  // tslint:disable-next-line:variable-name
  private isToInbox = false;
  private textToInbox = '';
  private responseTextType = true;

  // response
  private responseMessage = [];

  // block
  private blockSelected: any[] = [];

  constructor(private intentService: IntentService, private dialog: MdDialog, private notificationService: NotificationService) { }

  public init() {
    this.textToInbox = '';
    this.isToInbox = false;
    this.blockSelected = [];
    this.responseMessage = [];
  }
  public ngOnInit(): void {
    this.init();
    // alert(JSON.stringify(this.currentIntent));
    if (!this.currentIntent) {
      return;
    }
    // alert(JSON.stringify(this.currentIntent));
    // if (_.size(this.currentIntent.response_messages) > 0) {
    //   let obj = JSON.parse(this.currentIntent.response_messages[0].content);
    //   this.responseMessage = obj[0].message.posibleTexts;
    // }

    if (this.currentIntent.type_answer) {
      this.responseTextType = this.currentIntent.type_answer === 'text' ? true : false;
    }

    if (_.size(this.currentIntent.response_messages) > 0) {
      try {
        let obj = JSON.parse(this.currentIntent.response_messages[0].content);
        if (obj.length > 0 && obj[0].message && obj[0].message.posibleTexts) {
          this.responseMessage = obj[0].message.posibleTexts;
        } else {
          this.responseMessage = [];
        }
      } catch (error) {
        this.responseMessage = [];
      }
    }

    if (this.currentIntent.response_message_blocks) {
      this.blockSelected = [];
      this.currentIntent.response_message_blocks.filter((item) => {
        let bl = this.blocks.filter((it) => it.value === item.block_id)[0];
        if (bl) {
          this.blockSelected.push(bl);
        }
      });
    }

    if (this.currentIntent.inner_action && this.currentIntent.inner_action === 'createInboxFromComment') {
      //  alert(JSON.stringify(this.currentIntent));
      this.isToInbox = true;
    }
    // if (this.currentIntent.intent_entities && this.currentIntent.intent_entities[0]) {
    //   //  alert(JSON.stringify(this.currentIntent));
    //   this.textToInbox = this.currentIntent.intent_entities[0].default_value;
    //   // alert(this.textToInbox);
    // }
    if (this.currentIntent.intent_entities) {
      //  alert(JSON.stringify(this.currentIntent));
      this.currentIntent.intent_entities.filter((item) => {
        if (item.param === 'noi_dung_inbox') {
          this.textToInbox = item.default_value;
        }
      });
      // alert(this.textToInbox);
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.init();
    // alert(JSON.stringify(this.currentIntent));
    if (!this.currentIntent) {
      return;
    }
    // alert(JSON.stringify(this.currentIntent));
    // if (_.size(this.currentIntent.response_messages) > 0) {
    //   let obj = JSON.parse(this.currentIntent.response_messages[0].content);
    //   this.responseMessage = obj[0].message.posibleTexts;
    // }

    if (this.currentIntent.type_answer) {
      this.responseTextType = this.currentIntent.type_answer === 'text' ? true : false;
    }

    if (_.size(this.currentIntent.response_messages) > 0) {
      try {
        let obj = JSON.parse(this.currentIntent.response_messages[0].content);
        if (obj.length > 0 && obj[0].message && obj[0].message.posibleTexts) {
          this.responseMessage = obj[0].message.posibleTexts;
        } else {
          this.responseMessage = [];
        }
      } catch (error) {
        this.responseMessage = [];
      }
    }

    if (this.currentIntent.response_message_blocks) {
      this.blockSelected = [];
      this.currentIntent.response_message_blocks.filter((item) => {
        let bl = this.blocks.filter((it) => it.value === item.block_id)[0];
        if (bl) {
          this.blockSelected.push(bl);
        }
      });
    }

    if (this.currentIntent.inner_action && this.currentIntent.inner_action === 'createInboxFromComment') {
      //  alert(JSON.stringify(this.currentIntent));
      this.isToInbox = true;
    }
    // if (this.currentIntent.intent_entities && this.currentIntent.intent_entities[0]) {
    //   //  alert(JSON.stringify(this.currentIntent));
    //   this.textToInbox = this.currentIntent.intent_entities[0].default_value;
    //   // alert(this.textToInbox);
    // }
    if (this.currentIntent.intent_entities) {
      //  alert(JSON.stringify(this.currentIntent));
      this.currentIntent.intent_entities.filter((item) => {
        if (item.param === 'noi_dung_inbox') {
          this.textToInbox = item.default_value;
        }
      });
      // alert(this.textToInbox);
    }
  }

  public changeSwitch(event) {
    if (!this.currentIntent) {
      return;
    }
    if (event.checked) {
      this.currentIntent.inner_action = 'createInboxFromComment';
    } else {
      this.currentIntent.inner_action = null;
    }
    this.updateIntent();
  }

  // response text
  public updateResponse() {
    if (!this.currentIntent) {
      return;
    }
    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.name = this.currentIntent.name;
    intent.type_answer = 'text';
    mes.posibleTexts = this.responseMessage;

    if (this.typeFaq === 'inbox') {
      intent.faq_type = 1;
    } else if (this.typeFaq === 'comment') {
      intent.faq_type = 2;
    }

    if (this.currentIntent.response_messages) {
      intent.response_messages = this.currentIntent.response_messages[0];
    }
    if (!intent.response_messages) {
      intent.response_messages = { content: '' };
    }
    if (this.currentIntent.intent_contexts) {
      intent.intent_contexts = this.currentIntent.intent_contexts;
    }
    intent.response_messages.content = JSON.stringify([{ message: mes }]);
    let actionOperate: Observable<any>;
    if (this.currentIntent.is_new) {
      this.addFAQ(intent);
    } else {
      intent.faq = 1;
      intent.type = 2;
      if (this.typeFaq === 'comment') {
        intent.is_just_understand_in_context = true;
        if (!intent.intent_contexts) {
          intent.intent_contexts = [{
            contexts: {
              name: 'context_comment',
              name_text: 'context_comment',
              agent_id: intent.agent_id,
              status: 1
            },
            type: 0,
            status: 1
          }];
        }
      }
      actionOperate = this.intentService.updateFAQ(intent);
      actionOperate.subscribe(
        (usay) => {
          // console.log('usersay', usay);
        },
        (err) => {
          console.log(err);
        });
    }
  }

  public updateIntent() {
    if (!this.currentIntent) {
      return;
    }
    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.type_answer = 'text';
    intent.inner_action = this.currentIntent.inner_action;
    if (this.currentIntent.intent_contexts) {
      intent.intent_contexts = this.currentIntent.intent_contexts;
    }

    if (this.typeFaq === 'inbox') {
      intent.faq_type = 1;
    } else if (this.typeFaq === 'comment') {
      intent.faq_type = 2;
    }

    if (this.currentIntent.is_new) {
      this.addFAQ(intent);
    } else {
      intent.faq = 1;
      intent.type = 2;
      if (this.typeFaq === 'comment') {
        intent.is_just_understand_in_context = true;
        if (!intent.intent_contexts) {
          intent.intent_contexts = [{
            contexts: {
              name: 'context_comment',
              name_text: 'context_comment',
              agent_id: intent.agent_id,
              status: 1
            },
            type: 0,
            status: 1
          }];
        }
      }
      let actionOperate: Observable<any>;
      actionOperate = this.intentService.updateFAQ(intent);
      actionOperate.subscribe(
        (usay) => {
          // console.log('usersay', usay);
          // this.responseTextType = !this.responseTextType;
          this.currentIntent.type_answer = intent.type_answer;
        },
        (err) => {
          console.log(err);
        });
    }
  }

  public updateContentSendInbox() {
    if (!this.currentIntent) {
      return;
    }
    // console.log(this.textToInbox);
    let entity: any = {};
    entity.entity_name = 'sys.any';
    entity.param = 'noi_dung_inbox';
    entity.default_value = this.textToInbox ? this.textToInbox : '';
    entity.lifespan = 2;
    entity.priority = 1;
    entity.status = 1;

    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.inner_action = this.currentIntent.inner_action;
    intent.intent_entities = [entity];
    if (this.currentIntent.intent_contexts) {
      intent.intent_contexts = this.currentIntent.intent_contexts;
    }
    // if (_.size(this.usersaysTemp) > 0) {
    // }
    if (this.currentIntent.is_new) {
      this.addFAQ(intent);
    } else {
      intent.faq = 1;
      intent.type = 2;
      if (this.typeFaq === 'comment') {
        intent.is_just_understand_in_context = true;
        if (!intent.intent_contexts) {
          intent.intent_contexts = [{
            contexts: {
              name: 'context_comment',
              name_text: 'context_comment',
              agent_id: intent.agent_id,
              status: 1
            },
            type: 0,
            status: 1
          }];
        }
      }
      let actionOperate: Observable<any>;
      actionOperate = this.intentService.updateFAQ(intent);
      actionOperate.subscribe(
        (usay) => {
        },
        (err) => {
          console.log(err);
        });
    }
  }

  public addFAQ(body) {
    if (!this.currentIntent) {
      return;
    }
    body.faq = 1;
    body.type = 2;

    if (this.typeFaq === 'comment') {
      body.is_just_understand_in_context = true;
      if (!body.intent_contexts) {
        body.intent_contexts = [{
          contexts: {
            name: 'context_comment',
            name_text: 'context_comment',
            agent_id: body.agent_id,
            status: 1
          },
          type: 0,
          status: 1
        }];
      }
    }

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(body);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          delete this.currentIntent.is_new;
          return;
        }
        // this.intents = intents;
        if (intent !== 202) {
          delete this.currentIntent.is_new;
        }
      },
      (err) => {
        console.log(err);
      });
  }

  public onTagResponseEdited($event: TagModel) {
    this.openDialogResponseEdit($event);
  }

  public openDialogResponseEdit($event) {
    this.currentIntent.response_messages = [{ content: '' }];
    this.currentIntent.response_messages[0].content = JSON.stringify([{ message: { posibleTexts: this.responseMessage } }]);
    let dialogRef = this.dialog.open(BaResponseEditComponent, {
      width: '50%',
      data: {
        currentIntent: this.currentIntent,
        content: $event
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (_.size(this.currentIntent.response_messages) > 0) {
        let obj = JSON.parse(this.currentIntent.response_messages[0].content);
        this.responseMessage = obj[0].message.posibleTexts;
      }
    });
  }

  public updateIntentToBlock() {
    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.type_answer = this.responseTextType ? 'text' : 'block';
    intent.inner_action = this.currentIntent.inner_action;

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(intent);
    actionOperate.subscribe(
      (usay) => {
        this.currentIntent.type_answer = intent.type_answer;
      },
      (err) => {
        this.notificationService.showDanger('Cập nhật ' + this.responseTextType ? 'text' : 'block' + ' thất bại. Vui lòng thử lại');
        this.responseTextType = !this.responseTextType;
        // console_bk.log(err);
      });
  }

  public RemoveResponseBlock(event) {
    // // console_bk.log('remove block:', event);
    let intentBlock = { block_id: event.value, intent_id: this.currentIntent.id, status: -1 };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addOrUpdateIntentBlock([intentBlock]);
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public updateResponseBlock(event) {
    let intentBlock = { block_id: event.value, intent_id: this.currentIntent.id, status: 1 };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addOrUpdateIntentBlock([intentBlock]);
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
        this.sortIntentBlock();
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public sortIntentBlock() {
    let sortArr = [];
    let _ids = _.pluck(this.blockSelected, 'value');
    _ids.filter((item) => {
      sortArr.push({ intent_id: this.currentIntent.id, block_id: item, status: 1 });
    });
    // // console_bk.log('ids:', _ids);
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.sortIntentBlock({ ids: sortArr });
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
      },
      (err) => {
        // console_bk.log(err);
      });
  }
}
