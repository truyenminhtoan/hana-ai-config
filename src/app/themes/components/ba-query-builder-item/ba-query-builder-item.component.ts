import { EmitterService } from './../../../_services/emitter.service';
import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit, NgZone, AfterContentInit, Output, EventEmitter } from '@angular/core';
import { DateAdapter, NativeDateAdapter } from '@angular/material';
import * as moment from 'moment';
declare var jQuery: any;
declare var $: any;
import _ from 'underscore';
import * as _moment from 'moment';
@Component({
    selector: 'ba-query-builder-item',
    templateUrl: 'ba-query-builder-item.component.html',
    styleUrls: ['ba-query-builder-item.component.scss']
})
export class BaQueryBuilderItemComponent implements OnInit {
    @Input() private index;
    @Input() private condition;
    @Input() private fields;

    @Output() private output: any = new EventEmitter<any>();
    private outputQuery: any = {};

    private conditionOperator = ['VÀ', 'HOẶC'];
    private conditionOperatorSelected = 'VÀ';
    // param
    private paramSelected;
    private paramModelSelected;
    // operator
    private operatorSelected;
    private operatorModelSelected;
    // editor
    private inputValue;
    private dateBefore;
    private dateAfter;
    private mutiSelected;
    private showInput;
    private showSelect;
    private showSelectDateBefore;
    private showSelectDateAfter;
    private showSelectMultiple;
    private fTypes = {
        text: 'text',
        bool: 'boolean',
        number: 'number',
        date: 'date',
        time: 'time',
        list: 'list',
        listOpts: 'list-options',
        listDropdown: 'list-dropdown',
        list2: 'list2'
    };
    private radioSelected;

    private i18n = {
        sEqual: 'bằng',
        sNotEqual: 'không bằng',
        sStart: 'bắt đầu với',
        sContain: 'chứa',
        sNotContain: 'không chứa',
        sFinish: 'kết thúc với',
        sInList: 'là',
        sNotInList: 'không là',
        sIsNull: 'không có dữ liệu',
        sIsNotNull: 'có dữ liệu',
        sBefore: 'trước ngày',
        sAfter: 'sau ngày',
        sNumEqual: '=',
        sNumNotEqual: '!=',
        sGreater: '>=',
        sSmaller: '<=',
        sOn: 'trong ngày',
        sNotOn: 'khác ngày',
        sAt: 'tại',
        sNotAt: 'không tại',
        sBetween: 'trong khoảng',
        sNotBetween: 'không nằm trong khoảng',
        opAnd: 'và',
        // opOr:'or',
        yes: 'Đồng ý',
        no: 'Không',
        bNewCond: 'Thêm điều kiện lọc',
        bAddCond: '',
        bUpdateCond: 'Cập nhật điều kiện lọc',
        bSubmit: '',
        bCancel: ''
    };

    // - list of operators (for conditions)
    private evoAPI = {
        sEqual: '=',
        sNotEqual: '!=',
        sStart: 'sw',
        sContain: 'ct',
        sNotContain: 'nct',
        sFinish: 'fw',
        sInList: 'in',
        sNotInList: 'nin',
        sIsNull: 'null',
        sIsNotNull: 'nn',
        sGreater: '>=',
        sSmaller: '<=',
        sBetween: 'bw',
        sNotBetween: 'nbw'
    };

    private searchParam: any = [];
    private searchOperator: any = [];
    private cities = [];

    public ngOnInit(): void {
        if (this.fields) {
            this.loadSearchParam();
            this.loadQuery();
        }
    }

    constructor(dateAdapter: DateAdapter<NativeDateAdapter>) {
        dateAdapter.setLocale('vi-VN');
    }

    private loadSearchParam() {
        if (!this.fields) {
            this.fields = [];
        }
        this.searchParam = this.fields;
    }

    private loadOperator(type) {
        this.optReset();
        this.resetEditor();
        // set param selected
        this.paramSelected = type;
        switch (type) {
            case this.fTypes.number:
                this.opt(this.evoAPI.sEqual, this.i18n.sNumEqual);
                this.opt(this.evoAPI.sNotEqual, this.i18n.sNumNotEqual);
                this.opt(this.evoAPI.sGreater, this.i18n.sGreater);
                this.opt(this.evoAPI.sSmaller, this.i18n.sSmaller);
                break;
            case this.fTypes.text:
                // this.opt(this.evoAPI.sEqual, this.i18n.sEqual);
                // this.opt(this.evoAPI.sNotEqual, this.i18n.sNotEqual);
                // this.opt(this.evoAPI.sStart, this.i18n.sStart);
                // this.opt(this.evoAPI.sFinish, this.i18n.sFinish);
                this.opt(this.evoAPI.sContain, this.i18n.sContain);
                this.opt(this.evoAPI.sNotContain, this.i18n.sNotContain);
                break;
            case this.fTypes.list:
                this.opt(this.evoAPI.sInList, this.i18n.sInList);
                // this.opt(this.evoAPI.sNotInList, this.i18n.sNotInList);
                break;
            case this.fTypes.date:
                this.opt(this.evoAPI.sEqual, this.i18n.sOn);
                this.opt(this.evoAPI.sGreater, this.i18n.sAfter);
                this.opt(this.evoAPI.sSmaller, this.i18n.sBefore);
                this.opt(this.evoAPI.sBetween, this.i18n.sBetween);
                this.opt(this.evoAPI.sNotBetween, this.i18n.sNotBetween);
                break;
            case this.fTypes.bool:
                this.opt(this.evoAPI.sIsNotNull, this.i18n.sIsNotNull);
                this.opt(this.evoAPI.sIsNull, this.i18n.sIsNull);
                break;
            default:
                break;
        }
        this.buildField();
    }

    private optReset() {
        this.searchOperator = [];
    }

    private opt(_value, _label) {
        this.searchOperator.push({ value: _value, label: _label });
    }

    private resetEditor() {
        this.showInput = false;
        this.showSelect = false;
        this.showSelectMultiple = false;
        this.showSelectDateBefore = false;
        this.showSelectDateAfter = false;

        this.inputValue = '';
        this.mutiSelected = [];
    }
    private setEditor(type) {
        this.resetEditor();
        this.operatorSelected = type;
        switch (type) {
            case this.evoAPI.sEqual:
            case this.evoAPI.sNotEqual:
            case this.evoAPI.sStart:
            case this.evoAPI.sFinish:
            case this.evoAPI.sContain:
            case this.evoAPI.sNotContain:
                // this.showInput = true;
                if (this.paramSelected === 'date' && type === this.evoAPI.sEqual) {
                    this.showSelectDateBefore = true;
                } else if (true) {
                    this.showInput = true;
                }
                break;
            case this.evoAPI.sGreater:
            case this.evoAPI.sSmaller:
                if (this.paramSelected === 'date') {
                    this.showSelectDateBefore = true;
                } else if (true) {
                    this.showInput = true;
                }
                break;
            case this.evoAPI.sBetween:
            case this.evoAPI.sNotBetween:
                if (this.paramSelected === 'date') {
                    this.showSelectDateBefore = true;
                    this.showSelectDateAfter = true;
                }
                break;
            case this.evoAPI.sInList:
            case this.evoAPI.sNotInList:
                this.showSelectMultiple = true;
                break;
            case this.evoAPI.sIsNull:
            case this.evoAPI.sIsNotNull:
                break;
            default:
                this.showSelectMultiple = true;
                break;
        }

        this.buildOperator();

        if (this.paramModelSelected.type === 'list') {
            this.fields.filter((item) => {
                if (item.type === 'list') {
                    if (item.id === this.paramModelSelected.id) {
                        this.cities = item.list;
                        this.cities.filter((it) => {
                            if (this.condition.value != undefined && this.showSelectMultiple && this.condition.value.value.indexOf(it.id) > -1) {
                                if (!this.mutiSelected) {
                                    this.mutiSelected = [];
                                }
                                this.mutiSelected.push(it);
                            }
                        });
                        this.buildSelectMulti();
                    }
                }
            });

        }

    }

    // private loadEditor(type) {
    //     this.resetEditor();
    //     this.operatorSelected = type;
    //     this.buildOperator();
    //     switch (type) {
    //         case this.evoAPI.sEqual:
    //         case this.evoAPI.sNotEqual:
    //         case this.evoAPI.sStart:
    //         case this.evoAPI.sFinish:
    //         case this.evoAPI.sContain:
    //         case this.evoAPI.sNotContain:
    //             if (this.paramSelected === 'date') {
    //                 this.showSelectDateBefore = true;
    //                 this.dateBefore = this.condition.value.value;
    //             } else {
    //                 this.showInput = true;
    //                 if (this.condition && this.condition.value) {
    //                     this.inputValue = this.condition.value.value;
    //                     this.buildInputValue();
    //                 }
    //             }
    //             break;
    //         case this.evoAPI.sGreater:
    //         case this.evoAPI.sSmaller:
    //             if (this.paramSelected === 'date') {
    //                 this.showSelectDateBefore = true;
    //                 this.dateBefore = this.condition.value.value;
    //             } else if (true) {
    //                 this.showInput = true;
    //                 if (this.condition && this.condition.value) {
    //                     this.inputValue = this.condition.value.value;
    //                     this.buildInputValue();
    //                 }
    //             }
    //             break;
    //         case this.evoAPI.sBetween:
    //         case this.evoAPI.sNotBetween:
    //             if (this.paramSelected === 'date') {
    //                 this.showSelectDateBefore = true;
    //                 this.showSelectDateAfter = true;
    //                 this.dateBefore = this.condition.value.value;
    //                 this.dateAfter = this.condition.value.value2;
    //             }
    //             break;
    //         case this.evoAPI.sInList:
    //         case this.evoAPI.sNotInList:
    //             this.showSelectMultiple = true;
    //             this.buildSelectMulti();
    //             break;
    //         case this.evoAPI.sIsNull:
    //         case this.evoAPI.sIsNotNull:
    //             break;
    //         default:
    //             this.showSelectMultiple = true;
    //             this.buildSelectMulti();
    //             break;
    //     }
    //
    //     if (this.paramModelSelected.type === 'list') {
    //         this.fields.filter((item) => {
    //             if (item.type === 'list') {
    //                 if (item.id === this.paramModelSelected.id) {
    //                     this.cities = item.list;
    //
    //                     let aValues = this.condition.value.value.split(',');
    //
    //                     this.cities.filter((it) => {
    //                         if (this.showSelectMultiple && aValues.indexOf(it.id) !== -1) {
    //                             if (!this.mutiSelected) {
    //                                 this.mutiSelected = [];
    //                             }
    //                             this.mutiSelected.push(it);
    //                         }
    //                     });
    //                 }
    //             }
    //         });
    //
    //     }
    // }

    private loadEditor(type) {
        this.resetEditor();
        this.operatorSelected = type;
        this.buildOperator();
        switch (type) {
            case this.evoAPI.sEqual:
            case this.evoAPI.sNotEqual:
            case this.evoAPI.sStart:
            case this.evoAPI.sFinish:
            case this.evoAPI.sContain:
            case this.evoAPI.sNotContain:
                if (this.paramSelected === 'date') {
                    this.showSelectDateBefore = true;
                    this.dateBefore = this.condition.value.value;
                } else {
                    this.showInput = true;
                    if (this.condition && this.condition.value) {
                        this.inputValue = this.condition.value.value;
                        this.buildInputValue();
                    }
                }
                break;
            case this.evoAPI.sGreater:
            case this.evoAPI.sSmaller:
                if (this.paramSelected === 'date') {
                    this.showSelectDateBefore = true;
                    this.dateBefore = this.condition.value.value;
                } else if (true) {
                    this.showInput = true;
                    if (this.condition && this.condition.value) {
                        this.inputValue = this.condition.value.value;
                        this.buildInputValue();
                    }
                }
                break;
            case this.evoAPI.sBetween:
            case this.evoAPI.sNotBetween:
                if (this.paramSelected === 'date') {
                    this.showSelectDateBefore = true;
                    this.showSelectDateAfter = true;
                    this.dateBefore = this.condition.value.value;
                    this.dateAfter = this.condition.value.value2;
                }
                break;
            case this.evoAPI.sInList:
            case this.evoAPI.sNotInList:
                this.showSelectMultiple = true;
                this.buildSelectMulti();
                break;
            case this.evoAPI.sIsNull:
            case this.evoAPI.sIsNotNull:
                break;
            default:
                this.showSelectMultiple = true;
                this.buildSelectMulti();
                break;
        }

        if (this.paramModelSelected.type === 'list') {
            this.fields.filter((item) => {
                if (item.type === 'list') {
                    if (item.id === this.paramModelSelected.id) {
                        this.cities = item.list;

                        let aValues = this.condition.value.value.split(',');

                        this.cities.filter((it) => {
                            if (this.showSelectMultiple && aValues.indexOf(it.id) !== -1) {
                                if (!this.mutiSelected) {
                                    this.mutiSelected = [];
                                }
                                this.mutiSelected.push(it);
                            }
                        });
                    }
                }
            });

        }
    }

    private loadQuery() {
        if (this.condition) {
            this.conditionOperatorSelected = this.condition.syntax_display || 'VÀ';
            this.paramModelSelected = this.searchParam.filter((item) => item.id === this.condition.field.value)[0];
            if (this.paramModelSelected) {
                this.loadOperator(this.paramModelSelected.type);
                this.operatorModelSelected = this.searchOperator.filter((item) => item.value === this.condition.operator.value)[0];
                if (this.operatorModelSelected) {
                    this.paramSelected = this.paramModelSelected.type;
                    if (this.paramSelected === 'boolean') {
                        this.radioSelected = this.operatorModelSelected;
                    } else {
                        this.loadEditor(this.operatorModelSelected.value);
                    }
                }
            }
        }
    }

    private buildField() {
        this.outputQuery.field = { value: this.paramModelSelected.id, label: this.paramModelSelected.label };
        delete this.outputQuery.operator;
        this.out();
    }

    private buildOperator() {
        this.outputQuery.operator = this.operatorModelSelected;
        delete this.outputQuery.value;
        if (this.paramSelected === 'date') {
            this.dateAfter = '';
            this.dateBefore = '';
        }
        this.out();
    }

    // private buildOperator() {
    //     this.outputQuery.operator = this.operatorModelSelected;
    //     delete this.outputQuery.value;
    //     this.out();
    // }

    private buildInputValue() {
        this.outputQuery.value = { value: this.inputValue, label: this.inputValue };
        this.out();
    }

    private buildSelectMulti() {
        let name = _.pluck(this.mutiSelected, 'label');
        let vl = _.pluck(this.mutiSelected, 'id');
        this.outputQuery.value = { value: vl.join(), label: name.join() };
        if (vl.join().trim().length === 0 && name.join().trim().length === 0) {
            this.outputQuery.value = { value: this.condition.value.value, label: this.condition.value.label };
        }
        this.out();
    }

    private buildDateBefore() {
        if (!this.outputQuery.value) {
            this.outputQuery.value = {};
        }
        // this.outputQuery.value.value = this.dateBefore;
        // this.outputQuery.value.label = this.dateBefore;

        this.outputQuery.value.value = _moment(this.dateBefore).format('YYYY-MM-DD');
        this.outputQuery.value.label = _moment(this.dateBefore).format('DD/MM/YYYY');

        this.out();
    }

    private buildDateAfter() {
        if (!this.outputQuery.value) {
            this.outputQuery.value = {};
        }
        // this.outputQuery.value.value2 = this.dateAfter;
        // this.outputQuery.value.label2 = this.dateAfter;


        this.outputQuery.value.value2 = _moment(this.dateAfter).format('YYYY-MM-DD');
        this.outputQuery.value.label2 = _moment(this.dateAfter).format('DD/MM/YYYY');

        this.out();
    }

    private buildRadio() {
        this.outputQuery.operator = this.radioSelected;
        this.out();
    }

    private buildSyntax() {
        if (this.conditionOperatorSelected === 'VÀ') {
            this.outputQuery.syntax = 'and';
        } else {
            this.outputQuery.syntax = 'or';
        }

        this.out();
    }

    private out() {
        if (this.conditionOperatorSelected === 'VÀ') {
            this.outputQuery.syntax = 'and';
        } else {
            this.outputQuery.syntax = 'or';
        }
        /** fix loi cho truong hop load date trong khoang khong hien thi dc du lieu**/
        if (this.outputQuery.value == undefined) {
            this.outputQuery.value = this.condition.value;
        }
        this.output.emit(this.outputQuery);
    }

    private deleteQuery() {
        EmitterService.get('DELETE_QUERY_ITEM').emit(this.index);
    }
}