import {
    Component,
    Inject
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
declare var $: any;
declare var swal: any;
import {Observable} from "rxjs/Observable";
import {BlocksService} from "../../../../../_services/blocks.service";
import {IntentService} from "../../../../../_services/intents.service";
import {NotificationService} from "../../../../../_services/notification.service";
import {
    IntentContextModel, ContextModel, IntentContextsSuggestionsModel,
    SuggestionsIntentsModel, IntentModel
} from "../../../../../_models/intents.model";
import {EmitterService} from "../../../../../_services/emitter.service";

@Component({
    selector: 'detail-intent-action-dialog',
    templateUrl: 'detail-intent-action-dialog.component.html',
    styleUrls: ['detail-intent-action-dialog.component.scss']
})
export class DetailCommentInboxDialogComponent {

    private currentIntent;
    private currentIntent2;

    /** hanh dong */
    private createInboxFromComment = [];
    private removeCustomerFromGroups = [];
    private is_load = true;
    private is_load_update = false;
    private intent_actions = [];
    private tmp_intent_entities = [];
    private dropdownListBlocks = [];
    private textToInbox = '';

    private paramModelSelected = [
        {
            value: 'createInboxFromComment',
            lable: 'Comment thành inbox'
        }
    ];

    constructor(private blocksService: BlocksService, private intentService: IntentService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < DetailCommentInboxDialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    public okClick() {
        this.save2();
    }

    ngOnInit() {
        this.is_load = true;
        let that = this;
        if (this.data != undefined) {
            if (this.data.currentIntent != undefined) {
                this.currentIntent = this.data.currentIntent;
                if (this.currentIntent.intent_actions == undefined) {
                    this.currentIntent['intent_actions'] = [];
                } else {
                    if (Object.keys(this.currentIntent.intent_actions).length == 0) {
                        this.currentIntent.intent_actions = [];
                    }
                }
            }
            let tmp = JSON.stringify(this.currentIntent.intent_actions);
            this.intent_actions = JSON.parse(tmp);

            let listBlocks = {};
            Object.keys(this.data.list_blocks).forEach(function(k) {
                that.dropdownListBlocks.push({
                    "id": that.data.list_blocks[k].value,
                    "name": that.data.list_blocks[k].display
                });
                listBlocks[that.data.list_blocks[k].value] = {
                    "id": that.data.list_blocks[k].value,
                    "name": that.data.list_blocks[k].display
                };
            });
            let tmp_intent_entities = JSON.stringify(this.currentIntent.intent_entities);
            this.tmp_intent_entities = JSON.parse(tmp_intent_entities);

            /** cap nhat lai cau truc intent_contexts */
            this.currentIntent.intent_contexts.filter((row) => {
                row.contexts = row.contexts.id;
            });
            /** load data actions */
            this.currentIntent.intent_entities.filter((row) => {
                if (row.param ===  'noi_dung_inbox') {
                    if (this.checkAction('createInboxFromComment')) {
                        this.textToInbox = row.default_value;
                    } else {
                        this.textToInbox = '';
                    }
                }
            });
            let body = {
                "context_name": "comment_to_inbox_"+ this.currentIntent.id,
                "agent_id": this.currentIntent.agent_id
            };
            let actionOperate: Observable<IntentModel>;
            actionOperate = this.intentService.getIntentByContexName(body);
            actionOperate.subscribe(
                (data) => {
                    this.currentIntent2 = data;
                    //response_message_blocks
                    if (data['response_message_blocks'] !== undefined) {
                        Object.keys(data['response_message_blocks']).forEach(function(k) {
                            let name = 'NULL';
                            //console.log('listBlocks', listBlocks);
                            if (listBlocks[data['response_message_blocks'][k].block_id]['name'] !== undefined) {
                                name = listBlocks[data['response_message_blocks'][k].block_id]['name'];
                            }
                            that.createInboxFromComment.push({
                                "value": data['response_message_blocks'][k].block_id,
                                "display": name,
                                "id": data['response_message_blocks'][k].block_id,
                                "name": name
                            });
                            //that.createInboxFromComment.push(data['response_message_blocks'][k].block_id);
                        });
                    }
                    this.is_load = false;
                },
                (err) => {
                    this.is_load = false;
                });
        }
    }

    private checkArray(value, lists) {
        for (let i = 0; i < lists.length; i++) {
            if (lists[i].id == value) {
                return lists[i];
            }
        }

        return false;
    }

    private onAddOnRemove() {

    }

    private checkIntentEntities(param, intent_entities) {
        for (let i = 0; i < intent_entities.length; i++) {
            if (intent_entities[i].param.trim() == param.trim()) {
                return i;
            }
        }

        return -1;
    }

    private save() {
        if (this.tmp_intent_entities == undefined) {
            this.tmp_intent_entities = [];
        }
        let intent_entities = this.tmp_intent_entities;
        if (this.checkAction('createInboxFromComment') === true) {
            if (this.textToInbox.trim().length == 0) {
                this.notificationService.showDanger("Nội dung inbox không được bỏ trống!");
                return;
            }
            /** kiem tra noi_dung_inbox*/
            let index_check_inbox = this.checkIntentEntities('noi_dung_inbox', intent_entities);
            if (index_check_inbox != -1) {
                intent_entities[index_check_inbox]['default_value'] = this.textToInbox.trim();
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'noi_dung_inbox',
                    "default_value": this.textToInbox.trim()
                };
                intent_entities.push(item);
            }
            let index_check_contexts = this.checkIntentEntities('contexts', intent_entities);
            let contexts_values = [];
            if (index_check_contexts != -1) {
                intent_entities[index_check_contexts]['default_value'] = JSON.stringify(contexts_values);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'contexts',
                    "default_value": JSON.stringify(contexts_values)
                };
                intent_entities.push(item);
            }
        }

        for (let i = 0; i < intent_entities.length; i++) {
            intent_entities[i]['priority'] = i + 1;
        }

        this.currentIntent.intent_entities = intent_entities;

        this.updateFQA(intent_entities);
    }

    private save2() {
        if (this.tmp_intent_entities == undefined) {
            this.tmp_intent_entities = [];
        }
        let intent_entities = this.tmp_intent_entities;
        if (this.checkAction('createInboxFromComment') === true) {
            if (this.textToInbox.trim().length == 0) {
                this.notificationService.showDanger("Nội dung inbox không được bỏ trống!");
                return;
            }
            /** kiem tra noi_dung_inbox */
            let index_check_inbox = this.checkIntentEntities('noi_dung_inbox', intent_entities);
            if (index_check_inbox != -1) {
                intent_entities[index_check_inbox]['default_value'] = this.textToInbox.trim();
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'noi_dung_inbox',
                    "default_value": this.textToInbox.trim()
                };
                intent_entities.push(item);
            }
            let index_check_contexts = this.checkIntentEntities('contexts', intent_entities);
            let contexts_values = [];
            contexts_values.push("comment_to_inbox_"+ this.currentIntent.id);
            if (index_check_contexts != -1) {
                intent_entities[index_check_contexts]['default_value'] = JSON.stringify(contexts_values);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'contexts',
                    "default_value": JSON.stringify(contexts_values)
                };
                intent_entities.push(item);
            }
        }

        for (let i = 0; i < intent_entities.length; i++) {
            intent_entities[i]['priority'] = i + 1;
        }

        this.currentIntent.intent_entities = intent_entities;

        this.updateFQA2(intent_entities);
    }

    private updateFQA2(intent_entities) {
        let intent: any = {};
        intent.id = this.currentIntent.id;
        intent.agent_id = this.currentIntent.agent_id;
        intent.intent_entities = this.currentIntent.intent_entities;
        intent.intent_actions = this.intent_actions;
        intent.intent_actions.filter((item) => {
            item.type = "inner";
        });
        this.is_load_update = true;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFAQ(intent);
        actionOperate.subscribe(
            (usay) => {
                /** Tao intent 2 */
                this.createIntent2(intent_entities);
                // this.is_load_update = false;
                // this.currentIntent.intent_actions = this.intent_actions;
                // this.notificationService.showSuccess("Lưu thành công!");
                // this.dialogRef.close(this.currentIntent);
            },
            (err) => {
                console.log(err);
            });
    }
    
    private createIntent2(intent_entities) {
        if (this.checkAction('createInboxFromComment') === true) {
            let ids = _.pluck(this.createInboxFromComment, 'id');
            let index_check = this.checkIntentEntities('contexts', intent_entities);
            if (ids.length > 0 && index_check != -1) {
                let block_id = [];
                for (let i = 0; i < ids.length; i++) {
                    block_id.push({
                        "block_id": ids[i],
                        "priority":  i + 1
                    });
                }
              
                /** tao intent 2 */
                if (this.currentIntent2 != undefined && this.currentIntent2.id != undefined) {
                    delete this.currentIntent2.id;
                    this.currentIntent2['response_message_blocks'] = block_id;
                    this.currentIntent2['context_name'] = "comment_to_inbox_" + this.currentIntent.id;
                    this.currentIntent2['type_answer'] = "block";
                    let actionOperate: Observable<IntentModel>;
                    actionOperate = this.intentService.addntentByContextName(this.currentIntent2);
                    actionOperate.subscribe(
                        (data) => {
                            this.currentIntent.intent_actions = this.intent_actions;
                            this.is_load_update = false;
                            this.notificationService.showSuccess("Lưu thành công!");
                            this.dialogRef.close(this.currentIntent);
                        },
                        (err) => {
                            this.is_load_update = false;
                            this.notificationService.showSuccess("Lưu thất bại!");
                        });
                } else {
                    let intent2 =  {
                        "name":"default_fallback_" + this.currentIntent.id,
                        "name_text":"default_fallback_" + this.currentIntent.id,
                        "prioriy":1,
                        "is_system":false,
                        "agent_id": this.currentIntent.agent_id,
                        "inner_action":null,
                        "outler_action":null,
                        "type":2,
                        "type_answer":"block",
                        "faq":1,
                        "status":1,
                        "is_enable_send_action":true,
                        "is_just_understand_in_context":true,
                        "is_machine_learning":true,
                        "is_suggestion_predict":false,
                        "intent_contexts":[
                            {
                                "contexts":{
                                    "name":"comment_to_inbox_" + this.currentIntent.id,
                                },
                                "type" : 0
                            }
                        ],
                        "response_message_blocks": block_id,
                        "context_name":"comment_to_inbox_" + this.currentIntent.id
                    };
                    let actionOperate: Observable<IntentModel>;
                    actionOperate = this.intentService.addntentByContextName(intent2);
                    actionOperate.subscribe(
                        (data) => {
                            this.currentIntent.intent_actions = this.intent_actions;
                            this.is_load_update = false;
                            this.notificationService.showSuccess("Lưu thành công!");
                            this.dialogRef.close(this.currentIntent);
                        },
                        (err) => {
                            this.is_load_update = false;
                            this.notificationService.showSuccess("Lưu thất bại!");
                        });
                }
            } else {
                /** xoa intent 2 */
                this.deleteIntent2();
            }
        } else {
            /** xoa intent 2 */
            this.deleteIntent2();
        }
    }

    private deleteIntent2() {
        /** xoa intent 2 */
        if (this.currentIntent2 != undefined && this.currentIntent2.id != undefined) {
            this.intentService.deleteIntent(this.currentIntent2.id)
                .then((data) => {
                    this.notificationService.showSuccess("Lưu thành công!");
                    this.currentIntent.intent_actions = this.intent_actions;
                    this.is_load_update = false;
                    this.dialogRef.close(this.currentIntent);
                });
        } else {
            this.dialogRef.close(this.currentIntent);
        }
    }

    private validateContent() {
        /** check remove cac action = [] hoac cac action cu*/
        if (this.createInboxFromComment.length === 0 || this.checkAction('createInboxFromComment') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != 'addSequences';
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'createInboxFromComment' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'createInboxFromComment';
            });
        }
    }

    private updateFQA(intent_entities) {
        let intent: any = {};
        intent.id = this.currentIntent.id;
        intent.agent_id = this.currentIntent.agent_id;
        intent.intent_entities = this.currentIntent.intent_entities;
        intent.intent_actions = this.intent_actions;
        intent.intent_actions.filter((item) => {
            item.type = "inner";
        });
        this.is_load_update = true;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFAQ(intent);
        actionOperate.subscribe(
            (usay) => {
                /** Tao intent 2 */
                this.createIntent2(intent_entities);
                // this.currentIntent.intent_actions = this.intent_actions;
                // this.is_load_update = false;
                // this.notificationService.showSuccess("Lưu thành công!");
            },
            (err) => {
                console.log(err);
            });
    }
    
    private addAction(type) {
        let check = false;
        this.intent_actions.filter((item) => {
            if (item.action_name === type.value) {
                check = true;
            }
        });
        if (check === false) {
            this.intent_actions.push({action_name: type.value});
        } else {
            //this.notificationService.showDanger("Hành động này đã tồn tại");
        }
    }

    private deleteAction(item) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.intent_actions = this.intent_actions.filter((row) => {
                return row.action_name !== item;
            });
        }, (dismiss) => {
        });
    }

    private checkAction(type) {
        if (this.currentIntent === undefined) {
            return false;
        }
        if (this.currentIntent.intent_actions === undefined) {
            return false;
        }

        let check = false;
        this.intent_actions.filter((item) => {
            if (item.action_name === type) {
                check = true;
            }
        });

        return check;
    }

    public getStyleText(n) {
        if (n == 0) {
            return "rgba(183, 28, 28, 0.39)";
        } else {
            return "";
        }
    }
}