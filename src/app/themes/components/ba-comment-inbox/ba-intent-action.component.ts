import {Component, Input, EventEmitter, Output, ChangeDetectorRef, Inject, OnInit, AfterViewInit} from '@angular/core';
import { ApplicationService } from '../../../_services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../../../_services/config.service';
import { DOCUMENT } from '@angular/platform-browser';
import { Location } from '@angular/common';
import {DetailCommentInboxDialogComponent} from "./components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import {MdDialog} from "@angular/material";
import {IntentService} from "../../../_services/intents.service";
import {Observable} from "rxjs/Observable";
import * as _ from 'underscore';
import {UserModel} from "../../../_models/user.model";
import {BlocksService} from "../../../_services/blocks.service";
import {EmitterService} from "../../../_services/emitter.service";
import {NotificationService} from "../../../_services/notification.service";

@Component({
    selector: 'ba-comment-inbox',
    templateUrl: 'ba-intent-action.component.html',
    styleUrls: ['ba-intent-action.component.scss']
})
export class BaCommentInboxComponent implements OnInit {
    @Input() private currentIntent: any = {};
    @Output() output = new EventEmitter<any>();


    /** biens */
    private list_context_full = [];
    private autocomplete_context = [];
    private intent_sugest_auto_completes = [];
    private list_sequences = [];
    private list_groups = [];
    private list_blocks = [];

    private is_click = false;
    private count_actions = 0;

    constructor(private notificationService: NotificationService, private blockService: BlocksService, private intentService: IntentService, private dialog: MdDialog, private router: Router, private configService: ConfigService) {
        if (this.currentIntent.intent_actions != undefined) {
            this.count_actions = this.currentIntent.intent_actions.length;
        }
    }

    public ngOnInit(): void {
        EmitterService.get('LOAD_LIST_BLOCKS_ADDED').subscribe((data: any) => {
            this.getData();
        });
        this.getData();

        if (this.currentIntent.intent_actions != undefined) {
            this.count_actions = this.currentIntent.intent_actions.length;
        }
    }

    private countActions() {
        if (this.currentIntent != undefined) {
            if (this.currentIntent.intent_actions == undefined) {
                let actionOperate = this.intentService.getDetailIntentV2(this.currentIntent.id);
                actionOperate.subscribe(
                    (data) => {
                        if (data != undefined) {
                            if (data.intent_actions !== undefined) {
                                this.count_actions = data.intent_actions.length;
                            } else {
                                this.count_actions = 0;
                            }
                        }
                    },
                    (err) => {

                    });
            } else {
                this.count_actions = this.currentIntent.intent_actions.length;
            }
        } else {
            this.count_actions = 0;
        }
    }

    private getData() {
        /** Load data */
        let that = this;
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let app_id = currentUser.app_using.applications[0].app_id;
        let actionOperate: Observable<any>;
        actionOperate = this.blockService.getListGroupBlocks(currentUser.partner_id);
        actionOperate.subscribe(
            (data_groups) => {
                if (data_groups != undefined && data_groups.length > 0) {
                    that.list_blocks = [];
                    Object.keys(data_groups).forEach(function (k) {
                        Object.keys(data_groups[k].blocks).forEach(function (i) {
                            that.list_blocks.push({
                                value: data_groups[k].blocks[i].id,
                                display: data_groups[k].blocks[i].name
                            });
                        });
                    });
                }
            },
            (err) => {
            });
        let actionOperateIt: Observable<any>;
        actionOperateIt = this.intentService.initIntentView(localStorage.getItem('currentAgent_Id'), true);
        actionOperateIt.subscribe(
            (data) => {
                this.list_context_full = data.contexts;
                let context = _.pluck(data.contexts, 'name');
                data.contexts.filter((row) => {
                    this.autocomplete_context.push({
                        value: row.id,
                        display: row.name
                    });
                });
            },
            (err) => {
            });
    }

    private showDetail() {
        if (this.is_click == false) {
            this.is_click = true;
        } else {
            //return;
        }
        if (this.currentIntent == undefined) {
            return;
        }
        let that = this;
        let actionOperate = this.intentService.getDetailIntent(this.currentIntent.id);
        actionOperate.subscribe(
            (data) => {
                if (data != undefined) {
                    this.currentIntent = data;
                    if (!this.currentIntent) {
                        this.notificationService.showDanger("Không định nghĩa được Intent!");
                    } else {
                        this.is_click = false;
                        let dialogRef = this.dialog.open(DetailCommentInboxDialogComponent, {
                            width: '50%',
                            data: {
                                currentIntent: this.currentIntent,
                                list_blocks: this.list_blocks
                            }
                        });
                        dialogRef.afterClosed().subscribe(data_close => {
                            // if (data_close != undefined && data_close != null && data_close.intent_actions !== undefined) {
                            //     that.count_actions = data_close.intent_actions.length;
                            // }
                        });
                    }
                }
            },
            (err) => {

            });
    }

    public getAutoCompleteContent() {
        let actionOperateIt: Observable<any>;
        actionOperateIt = this.intentService.initIntentView(localStorage.getItem('currentAgent_Id'), true);
        actionOperateIt.subscribe(
            (data) => {
                this.list_context_full = data.contexts;
                this.autocomplete_context = data.contexts;
            },
            (err) => {
            });
    }
}
