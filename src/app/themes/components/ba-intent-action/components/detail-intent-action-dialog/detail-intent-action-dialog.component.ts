import {
    Component,
    Inject
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
declare var $: any;
declare var swal: any;
import {Observable} from "rxjs/Observable";
import {BlocksService} from "../../../../../_services/blocks.service";
import {IntentService} from "../../../../../_services/intents.service";
import {NotificationService} from "../../../../../_services/notification.service";
import {
    IntentContextModel, ContextModel, IntentContextsSuggestionsModel,
    SuggestionsIntentsModel
} from "../../../../../_models/intents.model";
import {EmitterService} from "../../../../../_services/emitter.service";

@Component({
    selector: 'detail-intent-action-dialog',
    templateUrl: 'detail-intent-action-dialog.component.html',
    styleUrls: ['detail-intent-action-dialog.component.scss']
})
export class DetailIntentActionDialogComponent {

    private currentIntent;
    private autocomplete_context = [];
    private intent_contexts_in = [];
    private intent_contexts_out = [];
    private suggestion_intents = [];
    private intent_sugest_auto_completes = [];
    private list_context_full = [];

    /** hanh dong */
    private addCustomerToSequences = [];
    private removeCustomerFromGroups = [];
    private removeCustomerFromSequences = [];
    private addCustomerToGroups = [];
    private dropdownListGroups = [];
    private dropdownListSequnces = [];
    private list_actions = [];

    private is_load = true;
    private is_load_update = false;
    private intent_actions = [];

    private tmp_intent_entities = [];

    //'removeCustomerFromGroups', 'addCustomerToGroups', 'removeCustomerFromSequences', 'createOrder'
    private paramModelSelected = [
        {
            value: 'addCustomerToGroups',
            lable: 'Thêm vào nhóm'
        },
        {
            value: 'removeCustomerFromGroups',
            lable: 'Bỏ ra khỏi nhóm'
        },
        {
            value: 'addCustomerToSequences',
            lable: 'Thêm vào quy trình'
        },
        {
            value: 'removeCustomerFromSequences',
            lable: 'Bỏ ra khỏi quy trình'
        },
        {
            value: 'createOrder',
            lable: 'Lên đơn hàng'
        }
    ];

    private content = '';

    constructor(private blocksService: BlocksService, private intentService: IntentService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < DetailIntentActionDialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    public okClick() {
        this.save2();
    }

    ngOnInit() {
        let that = this;
        if (this.data != undefined) {
            if (this.data.currentIntent != undefined) {
                this.currentIntent = this.data.currentIntent;
                if (this.currentIntent.intent_actions == undefined) {
                    this.currentIntent.intent_actions = [];
                } else {
                    if (Object.keys(this.currentIntent.intent_actions).length == 0) {
                        this.currentIntent.intent_actions = [];
                    }
                }
            }
            let tmp = JSON.stringify(this.currentIntent.intent_actions);
            this.intent_actions = JSON.parse(tmp);
            if (this.data.autocomplete_context != undefined) {
                this.autocomplete_context = this.data.autocomplete_context;
            }
            if (this.data.list_context_full != undefined) {
                this.list_context_full = this.data.list_context_full;
            }
            if (this.data.intent_sugest_auto_completes != undefined) {
                this.intent_sugest_auto_completes = this.data.intent_sugest_auto_completes;
            }
            Object.keys(this.data.groups).forEach(function(k) {
                that.dropdownListGroups.push({
                    "id": that.data.groups[k].id,
                    "name": that.data.groups[k].name
                });
            });
            Object.keys(this.data.sequences).forEach(function(k) {
                that.dropdownListSequnces.push({
                    "id": that.data.sequences[k].id,
                    "name": that.data.sequences[k].name
                });
            });

            let tmp_intent_entities = JSON.stringify(this.currentIntent.intent_entities);
            this.tmp_intent_entities = JSON.parse(tmp_intent_entities);
            console.log('let tmp_intent_entities', tmp_intent_entities);

            /** lay du lieu ngu canh dau vao & ngu canh dau ra */
            let intent_context_in_ids = [];
            let intent_context_out_ids = [];
            this.currentIntent.intent_contexts.filter((row) => {
                if (row.is_hidden == 0) {
                    if (row.type == 0) {
                        intent_context_in_ids.push(row.contexts.id);
                    } else {
                        intent_context_out_ids.push(row.contexts.id);
                    }
                }
            });
            this.autocomplete_context.filter((row) => {
                if (intent_context_in_ids.indexOf(row.value) != -1) {
                    that.intent_contexts_in.push(row);
                }
                if (intent_context_out_ids.indexOf(row.value) != -1) {
                    that.intent_contexts_out.push(row);
                }
            });

            /** lay du lieu goi y intent */
            if (this.currentIntent.contexts_suggestions != undefined) {
                this.currentIntent.contexts_suggestions.filter((row) => {
                    that.suggestion_intents.push({
                        name: row.suggestion_intents.name,
                        id: row.suggestion_intents.id
                    });
                });
            }

            /** cap nhat lai cau truc intent_contexts */
            this.currentIntent.intent_contexts.filter((row) => {
                row.contexts = row.contexts.id;
            });

            /** load data actions */
            this.currentIntent.intent_entities.filter((row) => {
                if (row.param ===  'removeGroups') {
                    if (this.checkAction('removeCustomerFromGroups')) {
                        let ids = JSON.parse(row.default_value);
                        for (let i = 0; i < ids.length; i++) {
                            let data = this.checkArray(ids[i], this.dropdownListGroups);
                            if (data !== false) {
                                data.display = data.name;
                                data.value = data.id;
                                this.removeCustomerFromGroups.push(data);
                            }
                        }
                    } else {
                        this.removeCustomerFromGroups = [];
                    }
                }
                if (row.param ===  'addGroups') {
                    if (this.checkAction('addCustomerToGroups')) {
                        let ids = JSON.parse(row.default_value);
                        for (let i = 0; i < ids.length; i++) {
                            let data = this.checkArray(ids[i], this.dropdownListGroups);
                            if (data !== false) {
                                data.display = data.name;
                                data.value = data.id;
                                this.addCustomerToGroups.push(data);
                            }
                        }
                    } else {
                        this.addCustomerToGroups = [];
                    }
                }
                if (row.param ===  'addSequences') {
                    if (this.checkAction('addCustomerToSequences')) {
                        let ids = JSON.parse(row.default_value);
                        for (let i = 0; i < ids.length; i++) {
                            let data = this.checkArray(ids[i], this.dropdownListSequnces);
                            if (data !== false) {
                                data.display = data.name;
                                data.value = data.id;
                                this.addCustomerToSequences.push(data);
                            }
                        }
                    } else {
                        this.addCustomerToSequences = [];
                    }
                }
                if (row.param ===  'removeSequences') {
                    if (this.checkAction('removeCustomerFromSequences')) {
                        let ids = JSON.parse(row.default_value);
                        for (let i = 0; i < ids.length; i++) {
                            let data = this.checkArray(ids[i], this.dropdownListSequnces);
                            if (data !== false) {
                                data.display = data.name;
                                data.value = data.id;
                                this.removeCustomerFromSequences.push(data);
                            }
                        }
                    } else {
                        this.removeCustomerFromSequences = [];
                    }
                }
                if (row.param ===  'phonenumber') {
                    if (this.checkAction('createOrder')) {
                        if (row.response_messages[0] !== undefined && row.response_messages[0].content !== undefined) {
                            this.content = row.response_messages[0].content;
                        }
                    }
                }
            });

            this.is_load = false;
        }
    }

    private checkArray(value, lists) {
        for (let i = 0; i < lists.length; i++) {
            if (lists[i].id == value) {
                return lists[i];
            }
        }

        return false;
    }

    private onAddOnRemove() {

    }

    private reloadSuggestion() {
        let ids = [];
        this.intent_contexts_out.filter((row) => {
            ids.push(row.value);
        });
        if (ids.length > 0) {
            let actionOperate: Observable<any>;
            actionOperate = this.intentService.getListIntentByContentIds(ids);
            actionOperate.subscribe(
                (data_suggestion) => {
                    this.intent_sugest_auto_completes = [];
                    data_suggestion.filter((item) => {
                        this.intent_sugest_auto_completes.push({value: item.id, display: item.name});
                    });
                    if (this.intent_sugest_auto_completes.length == 0) {
                        this.suggestion_intents = [];
                    }

                    //this.save();
                },
                (err_tmp) => {

                });
        }
    }

    private checkIntentEntities(param, intent_entities) {
        for (let i = 0; i < intent_entities.length; i++) {
            if (intent_entities[i].param.trim() == param.trim()) {
                return i;
            }
        }

        return -1;
    }

    private save() {
        if (this.tmp_intent_entities == undefined) {
            this.tmp_intent_entities = [];
        }
        let intent_entities = this.tmp_intent_entities;
        if (this.checkAction('addCustomerToGroups') === true) {
            let ids = _.pluck(this.addCustomerToGroups, 'id');
            let index_check = this.checkIntentEntities('addGroups', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('addCustomerToSequences') === true) {
            let ids = _.pluck(this.addCustomerToSequences, 'id');
            let index_check = this.checkIntentEntities('addSequences', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('removeCustomerFromGroups') === true) {
            let ids = _.pluck(this.removeCustomerFromGroups, 'id');
            let index_check = this.checkIntentEntities('removeGroups', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('removeCustomerFromSequences') === true) {
            let ids = _.pluck(this.removeCustomerFromSequences, 'id');
            let index_check = this.checkIntentEntities('removeSequences', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('createOrder') === true) {
            if (this.content.trim().length == 0) {
                this.notificationService.showDanger("Câu hỏi thu thập số điện thoại không được bỏ trống!");
                return;
            }
            let index_check = this.checkIntentEntities('phonenumber', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = "";
                if (intent_entities[index_check]['response_messages'][0] != undefined && intent_entities[index_check]['response_messages'][0]['content'] !== undefined) {
                    intent_entities[index_check]['response_messages'][0]['content'] = this.content.trim();
                } else {
                    intent_entities[index_check]['response_messages'] = [
                        {
                            "content" : this.content.trim()
                        }
                    ];
                }
                intent_entities[index_check]['is_required'] = true;

            } else {
                let item_1 = {
                    "entity_name" : "ht.so_dien_thoai",
                    "param" : 'phonenumber',
                    "default_value": "",
                    "response_messages" : [
                        {
                            "content" : this.content.trim()
                        }
                    ],
                    "is_required": true
                };
                intent_entities.push(item_1);
            }

            let index_check2 = this.checkIntentEntities('note', intent_entities);
            if (index_check2 != -1) {
                intent_entities[index_check2]['default_value'] = "$usersay";
            } else {
                let item_2 = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'note',
                    "default_value": "$usersay"
                };
                intent_entities.push(item_2);
            }
        }

        this.currentIntent.intent_contexts = this.currentIntent.intent_contexts.filter((row) => {
            return row.is_hidden === 1;
        });
        // Tao du lieu Context In
        if (this.intent_contexts_in.length > 0) {
            for (let i = 0; i < this.intent_contexts_in.length; i++) {
                this.handleAddContext(this.intent_contexts_in[i].value, 0);
            }
        }
        // Tao du lieu Context Out
        if (this.intent_contexts_out.length > 0) {
            for (let i = 0; i < this.intent_contexts_out.length; i++) {
                this.handleAddContext(this.intent_contexts_out[i].value, 1);
            }
        }
        this.currentIntent.intent_entities = intent_entities;
        if (this.currentIntent.contexts_suggestions == undefined) {
            this.currentIntent.contexts_suggestions = [];
        }
        let contexts_suggestions = [];
        let i = 1;
        this.suggestion_intents.filter((item) => {
            if (item.id !== item.name) {
                let context = new IntentContextsSuggestionsModel();
                context.status = 1;
                context.priority = i++;
                let suggestionIntent = new SuggestionsIntentsModel();
                suggestionIntent.id = item.id;
                suggestionIntent.name = item.name;
                context.suggestion_intents = suggestionIntent;
                contexts_suggestions.push(context);
            }
        });
        if (this.currentIntent.is_suggestion_predict === false) {
            contexts_suggestions = [];
        }
        this.currentIntent.contexts_suggestions = contexts_suggestions;

        this.updateFQA();
    }

    private save2() {
        if (this.tmp_intent_entities == undefined) {
            this.tmp_intent_entities = [];
        }
        let intent_entities = this.tmp_intent_entities;
        if (this.checkAction('addCustomerToGroups') === true) {
            let ids = _.pluck(this.addCustomerToGroups, 'id');
            let index_check = this.checkIntentEntities('addGroups', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('addCustomerToSequences') === true) {
            let ids = _.pluck(this.addCustomerToSequences, 'id');
            let index_check = this.checkIntentEntities('addSequences', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('removeCustomerFromGroups') === true) {
            let ids = _.pluck(this.removeCustomerFromGroups, 'id');
            let index_check = this.checkIntentEntities('removeGroups', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('removeCustomerFromSequences') === true) {
            let ids = _.pluck(this.removeCustomerFromSequences, 'id');
            let index_check = this.checkIntentEntities('removeSequences', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = JSON.stringify(ids);
            } else {
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
        }
        if (this.checkAction('createOrder') === true) {
            if (this.content.trim().length == 0) {
                this.notificationService.showDanger("Câu hỏi thu thập số điện thoại không được bỏ trống!");
                return;
            }
            let index_check = this.checkIntentEntities('phonenumber', intent_entities);
            if (index_check != -1) {
                intent_entities[index_check]['default_value'] = "";
                intent_entities[index_check]['is_required'] = true;
                if (intent_entities[index_check]['response_messages'][0] != undefined && intent_entities[index_check]['response_messages'][0]['content'] !== undefined) {
                    intent_entities[index_check]['response_messages'][0]['content'] = this.content.trim();
                } else {
                    intent_entities[index_check]['response_messages'] = [
                        {
                            "content" : this.content.trim()
                        }
                    ];
                }
            } else {
                let item_1 = {
                    "entity_name" : "ht.so_dien_thoai",
                    "param" : 'phonenumber',
                    "default_value": "",
                    "response_messages" : [
                        {
                            "content" : this.content.trim()
                        }
                    ],
                    "is_required": true
                };
                intent_entities.push(item_1);
            }

            let index_check2 = this.checkIntentEntities('note', intent_entities);
            if (index_check2 != -1) {
                intent_entities[index_check2]['default_value'] = "$usersay";
            } else {
                let item_2 = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'note',
                    "default_value": "$usersay"
                };
                intent_entities.push(item_2);
            }
        }

        this.currentIntent.intent_contexts = this.currentIntent.intent_contexts.filter((row) => {
            return row.is_hidden === 1;
        });
        // Tao du lieu Context In
        if (this.intent_contexts_in.length > 0) {
            for (let i = 0; i < this.intent_contexts_in.length; i++) {
                this.handleAddContext(this.intent_contexts_in[i].value, 0);
            }
        }
        // Tao du lieu Context Out
        if (this.intent_contexts_out.length > 0) {
            for (let i = 0; i < this.intent_contexts_out.length; i++) {
                this.handleAddContext(this.intent_contexts_out[i].value, 1);
            }
        }
        this.currentIntent.intent_entities = intent_entities;
        if (this.currentIntent.contexts_suggestions == undefined) {
            this.currentIntent.contexts_suggestions = [];
        }
        let contexts_suggestions = [];
        let i = 1;
        this.suggestion_intents.filter((item) => {
            if (item.id !== item.name) {
                let context = new IntentContextsSuggestionsModel();
                context.status = 1;
                context.priority = i++;
                let suggestionIntent = new SuggestionsIntentsModel();
                suggestionIntent.id = item.id;
                suggestionIntent.name = item.name;
                context.suggestion_intents = suggestionIntent;
                contexts_suggestions.push(context);
            }
        });
        if (this.currentIntent.is_suggestion_predict === false) {
            contexts_suggestions = [];
        }
        this.currentIntent.contexts_suggestions = contexts_suggestions;

        this.updateFQA2();
    }

    private updateFQA2() {

        //this.validateContent();

        let intent: any = {};
        intent.id = this.currentIntent.id;
        intent.agent_id = this.currentIntent.agent_id;
        //intent.contexts_suggestions = this.currentIntent.contexts_suggestions;
        intent.intent_entities = this.currentIntent.intent_entities;
        //intent.is_suggestion_predict = this.currentIntent.is_suggestion_predict;
        //intent.intent_contexts = this.currentIntent.intent_contexts;
        //intent.is_just_understand_in_context = this.currentIntent.is_just_understand_in_context;
        intent.intent_actions = this.intent_actions;
        intent.intent_actions.filter((item) => {
            item.type = "inner";
        });

        this.is_load_update = true;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFAQ(intent);
        actionOperate.subscribe(
            (usay) => {
                this.is_load_update = false;
                this.currentIntent.intent_actions = this.intent_actions;
                this.notificationService.showSuccess("Lưu thành công!");
                this.dialogRef.close(this.currentIntent);
            },
            (err) => {
                console.log(err);
            });
    }

    private validateContent() {
        /** check remove cac action = [] hoac cac action cu*/
        if (this.addCustomerToGroups.length === 0 || this.checkAction('addCustomerToGroups') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != 'addGroups';
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'addCustomerToSequences' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'addCustomerToGroups';
            });
        }
        if (this.removeCustomerFromGroups.length === 0 || this.checkAction('removeCustomerFromGroups') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != 'removeGroups';
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'addCustomerToSequences' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'removeCustomerFromGroups';
            });
        }
        if (this.addCustomerToSequences.length === 0 || this.checkAction('addCustomerToSequences') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != 'addSequences';
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'addCustomerToSequences' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'addCustomerToSequences';
            });
        }
        if (this.removeCustomerFromSequences.length === 0 || this.checkAction('removeCustomerFromSequences') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != 'removeSequences';
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'addCustomerToSequences' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'removeCustomerFromSequences';
            });
        }
        if (this.checkAction('createOrder') === false) {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') ||  (row.param != 'phonenumber' && row.param != 'note') ;
            });
            this.intent_actions = this.intent_actions.filter((item) => {
                return (item.action_name != 'addCustomerToGroups' && item.action_name != 'removeCustomerFromGroups' && item.action_name != 'addCustomerToSequences' && item.action_name != 'removeCustomerFromSequences') || item.action_name != 'createOrder';
            });
        }
    }

    private updateFQA() {

        //this.validateContent();

        let intent: any = {};
        intent.id = this.currentIntent.id;
        intent.agent_id = this.currentIntent.agent_id;
        //intent.contexts_suggestions = this.currentIntent.contexts_suggestions;
        intent.intent_entities = this.currentIntent.intent_entities;
        //intent.is_suggestion_predict = this.currentIntent.is_suggestion_predict;
        //intent.intent_contexts = this.currentIntent.intent_contexts;
        //intent.is_just_understand_in_context = this.currentIntent.is_just_understand_in_context;
        intent.intent_actions = this.intent_actions;
        intent.intent_actions.filter((item) => {
            item.type = "inner";
        });

        this.is_load_update = true;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFAQ(intent);
        actionOperate.subscribe(
            (usay) => {
                this.currentIntent.intent_actions = this.intent_actions;
                this.is_load_update = false;
                this.notificationService.showSuccess("Lưu thành công!");
            },
            (err) => {
                console.log(err);
            });
    }

    public handleAddContext(id, type) {
        let context: any = {};
        context.contexts = id
        context.status = 1;
        context.type = type;
        context.intent_id = this.currentIntent.id;
        if (!this.currentIntent.intent_contexts || this.currentIntent.intent_contexts.length === 0) {
            this.currentIntent.intent_contexts = [];
            this.currentIntent.intent_contexts.push(context);
        } else {
            this.currentIntent.intent_contexts.push(context);
        }
    }

    private addAction(type) {
        let check = false;
        this.intent_actions.filter((item) => {
            if (item.action_name === type.value) {
                check = true;
            }
        });
        if (check === false) {
            this.intent_actions.push({action_name: type.value});
        } else {
            //this.notificationService.showDanger("Hành động này đã tồn tại");
        }
    }

    private deleteAction(item) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.intent_actions = this.intent_actions.filter((row) => {
                return row.action_name !== item;
            });
            //this.checkRemoveActionTrash(item);
        }, (dismiss) => {
        });
    }

    private checkRemoveActionTrash(item) {
        let type = '';
        switch (item) {
            case 'removeCustomerFromGroups':
                type = 'removeGroups';
                break;
            case 'addCustomerToGroups':
                type = 'addGroups';
                break;
            case 'addCustomerToSequences':
                type = 'addSequences';
                break;
            case 'removeCustomerFromSequences':
                type = 'removeSequences';
                break;
        }
        if (type != 'createOrder') {
            if (type.trim().length > 0) {
                this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                    return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') || row.param != type;
                });
            }
        } else {
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return (row.param != 'addGroups' && row.param != 'removeGroups' && row.param != 'addSequences' && row.param != 'removeSequences') ||  row.param != 'phonenumber' && row.param != 'note' ;
            });
        }
    }

    private checkAction(type) {
        if (this.currentIntent === undefined) {
            return false;
        }
        if (this.currentIntent.intent_actions === undefined) {
            return false;
        }

        let check = false;
        this.intent_actions.filter((item) => {
            if (item.action_name === type) {
                check = true;
            }
        });

        return check;
    }

    private changeSuggestion_Predict() {
        this.currentIntent.is_suggestion_predict =! this.currentIntent.is_suggestion_predict
        this.reloadSuggestion();

    }

    onAddGroup(event) {

        let that = this;
        if (event.display.trim() == event.value.trim()) {
            swal({
                title: 'Thông báo',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display.trim()};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            console.log('event', event);
                            console.log(' that.addCustomerToGroups',  this.addCustomerToGroups);
                            for (let i = 0 ; i < that.addCustomerToGroups.length; i++) {
                                if (that.addCustomerToGroups[i].display == event.display) {
                                    that.addCustomerToGroups[i].value = result_add_group.id;
                                    that.addCustomerToGroups[i].id =  result_add_group.id;
                                    that.addCustomerToGroups[i].name =  result_add_group.name;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            EmitterService.get('LOAD_LIST_BLOCKS_ADDED').emit([]);
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.addCustomerToGroups = this.addCustomerToGroups.filter((row) => {
                    return (row.value != event.value)
                });
            });
        }
    }
}