import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ba-switch-2',
    templateUrl: 'ba-switch-2.component.html',
    styleUrls: ['ba-switch-2.component.scss']
})
export class BaSwitch2Component {
  @Input() private checked: boolean = true;
  @Output() private output = new EventEmitter<any>();

  public change(event) {
    this.output.emit(!event.checked);
  }
}
