import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { ComponentInteractionService } from '../../../_services/compoent-interaction-service';
import {NotificationService} from "../../../_services/notification.service";
import {ValidationService} from "../../../_services/ValidationService";
import {StringFormat} from "../../validators/string.format";
import {EntitieModel} from "../../../_models/entities.model";
import {EntityService} from "../../../_services/entity.service";
declare var $: any;
import * as _ from 'underscore';
import {Subscription} from 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import {ProductsService} from "../../../_services/products.service";

@Component({
    selector: 'add-entity-product',
    templateUrl: 'add-entity-product.component.html',
    styleUrls: ['add-entity-product.component.css']
})
export class AddEntityProductComponent implements OnInit {

    private entity_name_value = '';
    private entities: EntitieModel[];
    @ViewChild('btnCloseAddEntity') btnCloseEdit: ElementRef;
    @ViewChild('entity_name') entity_name: ElementRef;
    private eventSubscription: Subscription;
    private currentEntity = new EntitieModel();

    constructor(
        private productService: ProductsService,
        private entityService: EntityService,
        private _componentService: ComponentInteractionService,
        private notificationService: NotificationService) {
    }

    public initEntity() {
        this.currentEntity.name = '';
        this.currentEntity.type = 0;
        this.currentEntity.is_system = 0;
        this.currentEntity.status = 1;
        this.currentEntity.agent_id = localStorage.getItem('currentAgent_Id');
        this.currentEntity.type = 1;
    }

    ngOnInit() {
        this.loadEnties();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'close-btnCloseEdit') {
                    this.btnCloseEdit.nativeElement.click();
                    this.entity_name_value = '';
                }
            }
        );
    }

    public validateAlphabet(value) {
        if (StringFormat.checkRegexp(value, /^[a-zA-Z0-9_]+$/)) {
            return true;
        }
        return false;
    }

    private saveEntity() {
        if (this.entity_name_value.trim().length == 0) {
            this.notificationService.showWarning('Tên khái niệm không được rỗng ');
            this.entity_name.nativeElement.focus();
        } else {
            if (!this.validateAlphabet(this.entity_name_value)) {
                this.entity_name.nativeElement.focus();
                this.notificationService.showWarning('Tên khái niệm chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
            } else {
                this.entityService.getListEntityPromise(localStorage.getItem('currentAgent_Id'), 1)
                    .then((data) => {
                        this.entities = this.setValueText(data);
                        if (this.checkNameEntity(this.entity_name_value)) {
                            this.initEntity();
                            localStorage.setItem('current_entity_add', this.entity_name_value);
                            this.pushNewEntities((cb) => {
                                if (cb) {
                                    this.loadEnties();
                                    this._componentService.eventPublisher$.next('add-entity_product');
                                    this.notificationService.showSuccess('Thêm khái niệm thành công');
                                } else {
                                    this.notificationService.showDanger('Thêm khái niệm thất bại');
                                }
                            });
                        } else {
                            this.entity_name.nativeElement.focus();
                            this.notificationService.showWarning("Tên khái niệm đã tồn tại");
                        }

                    });
            }
        }
    }

    public pushNewEntities(callback) {
        this.currentEntity.name = localStorage.getItem('current_entity_add');
        let actionOperate: Observable<EntitieModel>;
        actionOperate = this.entityService.addEntity(this.currentEntity);
        actionOperate.subscribe(
            (data) => {
                callback(true);
            },
            (err) => {
                callback(false);
            });
    }

    private setValueText(enties) {
        _.each(enties, function (value) {
            value.text = value.name;
        });
        return enties;
    }

    public checkNameEntity(name) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i].name == name.trim()) {
                return false;
            }
        }
        return true;
    }

    private loadEnties() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entityService.getListEntityPromise(agentId, 1)
                .then((data) => {
                    this.entities = this.setValueText(data);
                })
        }
    }
   
}
