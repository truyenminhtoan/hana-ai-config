import { Component, Input } from '@angular/core';

@Component({
    selector: 'ba-container',
    templateUrl: 'ba-container.component.html',
    styleUrls: ['ba-container.component.scss']
})
export class BaContainerComponent {
  // tslint:disable-next-line:member-access
  @Input() title: string;
}
