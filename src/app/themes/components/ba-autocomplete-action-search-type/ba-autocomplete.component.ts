
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ActionBasic} from "../../../_models/actions.model";
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";

@Component({
  selector: 'ba-autocomplete-action-search-type',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteActionSearchTypeComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Input() selected: any;
  @Output() output = new EventEmitter<any>();
  
  private selectDefault = 'proximate';
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;
  private eventSubscription: Subscription;

  constructor(private _componentService: ComponentInteractionService) {
    this.output.emit('proximate');
    this.eventSubscription = this._componentService.eventReceiver$.subscribe(
        event => {
          if (event == 'auto-search') {
            this.selected = 'Câu nói của người dùng';
          }
        }
    );
  }

  private change(event: any) {
    this.output.emit(event.value);
  }
  
}

