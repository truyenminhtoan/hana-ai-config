import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ba-chips',
  templateUrl: 'ba-chips.component.html',
  styleUrls: ['ba-chips.component.scss']
})
export class BaChipsComponent {
  @Input() private values: any[] = [];
  @Output() private output = new EventEmitter<any>();

  private onRemove(item) {
    this.output.emit(item);
  }
}
