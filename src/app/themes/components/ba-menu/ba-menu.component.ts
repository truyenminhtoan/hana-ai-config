import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'ba-menu',
    templateUrl: 'ba-menu.component.html',
    styleUrls: ['ba-menu.component.scss']
})
export class BaMenuComponent {
  @Input() data: any;
  @Output() output = new EventEmitter<any>();

  public choiceData(dt) {
    this.output.emit(dt);
  }
}
