import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ResponseMessageModel } from '../../../_models/intents.model';
declare var $: any;
@Component({
  selector: 'ba-modal',
  templateUrl: 'ba-modal.component.html',
  styleUrls: ['ba-modal.component.scss']
})
export class BaModalComponent implements OnInit {
  @Input() title: any;
  @Input() vitri: any;
  @Input() response: ResponseMessageModel[] = [];
  @Output() output = new EventEmitter<any>();
  @ViewChild('content') content: ElementRef;
  @ViewChild('link_click') link_click: ElementRef;

  public respon : ResponseMessageModel[] = [];

  constructor(){
    //content
   // console.log('this.content.nativeElement', this.content.nativeElement);
    let that = this;
    setTimeout(function () {
      that.content.nativeElement.focus();
    }, 1000);
  }

  public autoFocus() {
    $('#noticeModal-' + this.vitri).modal('show');
    let that = this;
    setTimeout(function () {
      that.content.nativeElement.focus();
    }, 1000);
  }

  public ngOnInit() {
    $('.modal').appendTo('body');
    this.respon = this.response.slice();
    if (!this.respon || this.respon.length === 0) {
      let res: ResponseMessageModel = new ResponseMessageModel();
      res.content = '';
      res.status = 1;
      this.respon.push(res);
    }
    this.autoAdd();



  }

  public handleAddInput(event, index) {


    if (this.respon[this.respon.length - 1].content != null && this.respon[this.respon.length - 1].content.trim().length > 0) {
      this.autoAdd();
    }

    // if (this.respon.length > 1) {
    //   var list_delete = [];
    //   for (var i = 1; i < this.respon.length; i++) {
    //     if (this.respon[i].content != null && this.respon[i].content.length > 0) {
    //       list_delete.push(this.respon[i]);
    //     }
    //   }
    //   for (var i = 0; i < list_delete.length; i++) {
    //     this.respon = this.respon.filter((row) => {
    //       return (row != list_delete[i])
    //     });
    //   }
    // }

  }

  public autoAdd() {
    if (this.respon.length > 0 && this.respon[0].content.length > 0) {
      let res: ResponseMessageModel = new ResponseMessageModel();
      res.content = '';
      res.status = 1;
      this.respon.push(res);
    }
  }

  public handleDeleteInput(obj) {
    if (this.respon.length > 1) {
      this.respon = this.respon.filter((row) => {
        return row !== obj
      });
    }
  }

  public saveInput() {
    var respon = this.respon.filter((row) => {
      return (row.content != null && row.content.length > 0)
    });

    this.output.emit(respon);

  }

  public checkName(arr, key) {
    for (var i =0 ;i < arr.length; i++) {
      if (arr[i] == key) {
        return i;
      }
    }
    return -1;
  }


}
