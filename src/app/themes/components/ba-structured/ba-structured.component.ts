import {
    Component,
    OnInit,
    Input,
    EventEmitter,
    Output,
    ElementRef,
    ChangeDetectorRef
} from '@angular/core';
import * as _ from 'underscore';
import {
    Block
} from '../../../_models/blocks.model';
import {
    EmitterService
} from "../../../_services/emitter.service";
import {
    FormControl
} from "@angular/forms";
declare let swal: any;
declare let $: any;
import {
    Observable
} from 'rxjs/Rx';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import {
    IntentService
} from '../../../_services/intents.service';
import {
    NotificationService
} from '../../../_services/notification.service';
import {
    MdDialog
} from '@angular/material';
import {
    JsonDialogComponent
} from './components/json-dialog/json-dialog.component';
import {
    NgUploaderOptions
} from 'ngx-uploader';

import { DragulaService } from 'ng2-dragula';
import { ConfigService } from '../../../_services/config.service';

@Component({
    selector: 'ba-structured',
    templateUrl: 'ba-structured.component.html',
    styleUrls: ['ba-structured.component.scss', 'dragula.min.scss']
})
export class BaStructuredComponent implements OnInit {
    private blocks: Block;
    private blocksType;
    private width_value_list_card = 900;
    // danh sach cac phan hoi
    private responseMessage = [];
    // check show quick_reply
    private show_quick_reply = true;
    private is_load = false;
    private cs_width = 0;
    private is_focus = false;
    private json = '';

    @Input() id_component: any;

    @Input() private hideTitle: any = false;
    @Input() private blockInput: any;
    @Output() private blockOutput: any = new EventEmitter<any>();

    public outputBlock(body) {
        this.blockOutput.emit(body);
    }

    // tslint:disable-next-line:member-ordering
    constructor(private dialog: MdDialog, private element: ElementRef, private clipboard: ClipboardService, private intentService: IntentService,
        private notificationService: NotificationService, private dragulaService: DragulaService, private configService: ConfigService, private cdRef: ChangeDetectorRef) {
        dragulaService.dropModel.subscribe((value) => {
            this.onDropModel(value.slice(1));
        });

        $('.campaign-name').keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
        if (this.id_component === undefined) {
            this.id_component = '';
        }
        EmitterService.get('SAVE_LIST_CARD').subscribe((data: any) => {
            this.save();
        });
        EmitterService.get('SAVE_WHEN_UPLOAD_IMAGE').subscribe((data: any) => {
            this.save();
        });
        EmitterService.get('SAVE_BUTTON_BLOCK').subscribe((data: any) => {
            // console_bk.log('THIS ssss', JSON.stringify(this.responseMessage));
            this.save();
        });
        EmitterService.get('DELETE_BUTTON').subscribe((button: any) => {
            for (let i = 0; i < this.responseMessage.length; i++) {
                if (this.responseMessage[i].type == 1 && this.responseMessage[i].message.attachment != undefined && this.responseMessage[i].message.attachment.payload.buttons != undefined) {
                    this.responseMessage[i].message.attachment.payload.buttons = this.responseMessage[i].message.attachment.payload.buttons.filter((row) => {
                        return (row != button)
                    });
                }
                if (this.responseMessage[i].type === 2) {
                    for (let t = 0; t < this.responseMessage[i].message.attachment.payload.elements.length; t++) {
                        this.responseMessage[i].message.attachment.payload.elements[t].buttons = this.responseMessage[i].message.attachment.payload.elements[t].buttons.filter((row) => {
                            return (row != button)
                        });
                    }
                }
                if (this.responseMessage[i].type === 4) {
                    this.responseMessage[i].message.quick_replies = this.responseMessage[i].message.quick_replies.filter((row) => {
                        return (row != button)
                    });
                }

            }
            this.save();
        });
        EmitterService.get('SELECT_BLOCK').subscribe((data: any) => {
            this.json = '';
            if (data != null) {
                this.blockInput = data;
                this.blocks = this.blockInput;
                if (this.blocks.content === undefined || this.blocks.content === '[]') {
                    this.blocks.content = '';
                }
                this.blocksType = this.blockInput.type;
                this.is_load = true;
                this.loadBuild();
            } else {
                this.is_load = false;
            }
        });

    }

    public ngOnInit(): void {
        $('.modal').appendTo('body');
        $('.campaign-name').keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
        if (this.blockInput !== undefined) {
            this.blocks = this.blockInput;
            if (this.blocks.content === undefined) {
                this.blocks.content = '';
            }
            this.is_load = true;
            this.loadBuild();
            EmitterService.get('SAVE_LIST_CARD').subscribe((data: any) => {
                this.save();
            });
        }
    }

    public onDropModel(args) {
        this.configService.log('DRAGGGG ' + JSON.stringify(this.responseMessage));
        let quick = _.find(this.responseMessage, (res) => {
            return res.type === 4;
        });
        if (quick) {
            let last = _.last(this.responseMessage);
            if (last && last.type !== 4) {
                this.notificationService.showWarningTimer('Bạn không thể đổi vị trí của Quickreply', 3000);
            }
            this.responseMessage = _.filter(this.responseMessage, (res) => {
                return res.type !== 4;
            });
            this.responseMessage.push(quick);
            this.cdRef.detectChanges();
        };
        this.save();
        let [el, target, source] = args;

    }

    public checkButtonValidate(button) {
        let check = true;
        if (button.type == 'web_url') {
            if (button.url.trim().length == 0) {
                check = false;
            }
        } else {
            if (button.payload == undefined) {
                check = false;
            } else {
                let decodedString = atob(button.payload);
                decodedString = JSON.parse(decodedString);
                if (decodedString['title'].trim().length == 0) {
                    check = false;
                }
            }
        }

        return check;
    }

    public save() {
        let that = this;
        // console_bk.log('this.responseMessage sau kiem tra', JSON.stringify(this.responseMessage));
        let tmp_res = this.responseMessage;
        if (this.blocks !== undefined) {
            this.blocks.content = JSON.stringify(tmp_res);
            let res_content = JSON.parse(this.blocks.content);
            if (res_content) {
                for (let k = 0; k < res_content.length; k++) {
                    if (res_content[k].type === 1) {
                        if (res_content[k].message.text == undefined) {
                            res_content[k].message.attachment.type = 'template';
                            if (res_content[k].message.attachment.payload.buttons.length == 0) {
                                res_content[k] = {
                                    type: 1,
                                    message: {
                                        text: res_content[k].message.attachment.payload.text,
                                    }
                                }
                            } else {
                                // delete truong value
                                for (let z = 0; z < res_content[k].message.attachment.payload.buttons.length; z++) {
                                    delete res_content[k].message.attachment.payload.buttons[z].value;
                                    if (res_content[k].message.attachment.payload.buttons[z].button_type != undefined) {
                                        delete res_content[k].message.attachment.payload.buttons[z].button_type;
                                    }
                                }
                            }
                        }
                    }
                    if (res_content[k].type === 2) {
                        for (let t = 0; t < res_content[k].message.attachment.payload.elements.length; t++) {
                            for (let z = 0; z < res_content[k].message.attachment.payload.elements[t].buttons.length; z++) {
                                delete res_content[k].message.attachment.payload.elements[t].buttons[z].value;
                                if (res_content[k].message.attachment.payload.elements[t].buttons[z].button_type != undefined) {
                                    delete res_content[k].message.attachment.payload.elements[t].buttons[z].button_type;
                                }
                            }
                        }
                    }
                    if (res_content[k].type === 4) {
                        for (let z = 0; z < res_content[k].message.quick_replies.length; z++) {
                            delete res_content[k].message.quick_replies[z].value;
                            if (res_content[k].message.quick_replies[z].button_type != undefined) {
                                delete res_content[k].message.quick_replies[z].button_type;
                            }
                        }
                    }
                }
                this.blocks.content = JSON.stringify(res_content);
            }
        }
        this.output();
        this.outputBlock(this.blocks);
        EmitterService.get('LOAD_LIST_BLOCKS').emit([]);
    }

    public updateSaveBlockname() {
        this.output();
        this.outputBlock(this.blocks);
    }

    public removeResponseMessage(obj) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.responseMessage = this.responseMessage.filter((row) => {
                return row !== obj
            });
            this.checkShowQuickReply();
            this.save();
            swal({
                title: 'Xóa thành công!',
                text: '',
                type: 'success',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
            });
        }, (dismiss) => {
            // console_bk.log(dismiss);
        });
    }

    public removeCardItem(index, card) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.responseMessage[index].message.attachment.payload.elements = this.responseMessage[index].message.attachment.payload.elements.filter((row) => {
                return (row != card)
            });
            // kiem tra xoa luon nhung  group nao khong con elements nao
            this.responseMessage = this.responseMessage.filter((row) => {
                return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
            });
            this.save();
            this.updateWidthOfListCard();
            swal({
                title: 'Xóa thành công!',
                text: '',
                type: 'success',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
            });
        }, (dismiss) => {
            // console_bk.log(dismiss);
        });
    }

    public addCard(index) {
        this.responseMessage[index].message.attachment.payload.elements.push({
            image_url: '',
            title: '',
            subtitle: '',
            buttons: []
        });

        // width_value_list_card
        this.updateWidthOfListCard();
        this.save();
    }

    public updateWidthOfListCard() {
        let tmp = 0;
        for (let i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type == 2) {
                if (this.responseMessage[i].message.attachment.payload.elements.length > tmp) {
                    tmp = this.responseMessage[i].message.attachment.payload.elements.length;
                }
            }
        }
        if (tmp > 0) {
            this.width_value_list_card = (tmp * 500 + 300);
            this.cs_width = this.width_value_list_card;
        }
    }

    public addResponseMessageType(typeMes: number) {
        switch (typeMes) {
            case 1:
                // Phan hoi text
                this.responseMessage.push({
                    type: typeMes,
                    message: {
                        attachment: {
                            type: 'text',
                            payload: {
                                template_type: "button",
                                text: "",
                                buttons: []
                            }
                        }
                    }
                });
                this.setfocus();
                break;
            case 2:
                // Phan hoi card
                this.responseMessage.push({
                    type: typeMes,
                    message: {
                        attachment: {
                            type: 'template',
                            payload: {
                                template_type: 'generic',
                                elements: [{
                                    image_url: '',
                                    title: '',
                                    subtitle: '',
                                    buttons: []
                                }]
                            }
                        }
                    }
                });
                this.setfocus();
                break;
            case 3:
                // Phan hoi hinh anh
                this.responseMessage.push({
                    type: typeMes,
                    message: {
                        attachment: {
                            type: 'image',
                            payload: {
                                url: ''
                            }
                        }
                    }
                });
                this.setfocus();
                break;
            case 4:
                // Phan hoi quick_replies
                //this.responseMessage.push({ type: typeMes, message: { quick_replies: [{content_type: 'text', title: '', payload: ''}], text : ''}});
                this.responseMessage.push({
                    type: typeMes,
                    message: {
                        quick_replies: [],
                        text: ''
                    }
                });
                this.setfocusQuickReply();
                break;
        }
        //sort phan hoi
        //- Quickreply lúc nào cũng dưới cùng
        //- Các loại phản hồi khác thì hiển thị theo thứ tự người dùng đã thêm text - image - card - text - ... - quickreply
        let tmp_responseMessage = [];
        for (let i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type != 4) {
                tmp_responseMessage.push(this.responseMessage[i]);
            }
        }
        for (let i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type == 4) {
                tmp_responseMessage.push(this.responseMessage[i]);
                break;
            }
        }
        this.responseMessage = tmp_responseMessage;
        this.checkShowQuickReply();
        this.save();
    }

    public setfocusQuickReply() {
        // set focus
        let that = this;
        setTimeout(function () {
            $(that.element.nativeElement).find('.res-text-quick-reply').focus();
        }, 100);
    }

    public focus(i) {
        let that = this;
        setTimeout(function () {
            let element = '.res-text-' + i;
            $(that.element.nativeElement).find(element).focus();
        }, 500);
    }

    public setfocus() {
        // set focus
        let that = this;
        setTimeout(function () {
            let element = '.res-text-' + (that.responseMessage.length - 1);
            for (let i = 0; i < that.responseMessage.length; i++) {
                if (that.responseMessage[i].type == 4) {
                    element = '.res-text-' + (that.responseMessage.length - 2);
                    break;
                }
            }
            $(that.element.nativeElement).find(element).focus();
        }, 100);
    }

    public addResponseTextInput(event, i, j) {
        // this.responseMessage[i].message.posibleTexts[j].content = event.target.value;
        // let us = this.responseMessage[i].message.posibleTexts.filter((item) =>
        //     _.isEmpty(item.content))[0];
        // if (!us) {
        //   this.responseMessage[i].message.posibleTexts.push({content: ''});
        // }
    }

    public checkShowQuickReply() {
        let check = false;
        if (this.responseMessage.length > 0) {
            for (let i = 0; i < this.responseMessage.length; i++) {
                if (this.responseMessage[i].type == 4) {
                    this.show_quick_reply = false;
                    check = true;
                    break;
                }
            }
            if (!check) {
                this.show_quick_reply = true;
            }
        } else {
            this.show_quick_reply = true;
        }
    }

    public removeResponseTextInput(event, i, j) {
        if (this.responseMessage[i].message.posibleTexts.length > 1) {
            this.responseMessage[i].message.posibleTexts = this.responseMessage[i].message.posibleTexts.filter((row) => {
                return row !== this.responseMessage[i].message.posibleTexts[j]
            });
        }
    }

    public addResponseImagesInput(event, i) {
        this.responseMessage[i].message.attachment.payload.url = event.target.value;
        this.save();
    }

    public output() {
        // EmitterService.get(this.outputBlockId).emit(this.blocks);
    }

    public updateDataOld() {
        for (let i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type == 2) {
                for (let k = 0; k < this.responseMessage[i].message.attachment.payload.elements.length; k++) {
                    this.responseMessage[i].message.attachment.payload.elements[k].buttons = this.responseMessage[i].message.attachment.payload.elements[k].buttons.filter((bt) => {
                        return bt.title.trim().length > 0
                    });
                    for (let t = 0; t < this.responseMessage[i].message.attachment.payload.elements[k].buttons.length; t++) {
                        let button = this.responseMessage[i].message.attachment.payload.elements[k].buttons[t];

                        if (button.title.trim().length == 0) {

                            let obj = JSON.stringify({
                                "title": button.title,
                                "postback_type": "text",
                                "content": ""
                            });
                            let payload = new Buffer(obj).toString('base64');
                            this.responseMessage[i].message.attachment.payload.elements[k].buttons[t] = {
                                "type": "postback",
                                "title": button.title,
                                "payload": payload
                            };

                        } else {

                            if (button != undefined && button.type != undefined && button.type == 'postback') {
                                if (!this.isBase64(button.payload)) {
                                    let obj = JSON.stringify({
                                        "title": button.title,
                                        "postback_type": "text",
                                        "content": button.payload
                                    });
                                    let payload = new Buffer(obj).toString('base64');
                                    this.responseMessage[i].message.attachment.payload.elements[k].buttons[t] = {
                                        "type": "postback",
                                        "title": button.title,
                                        "payload": payload
                                    };

                                }

                            }
                        }


                    }
                }
            }

            if (this.responseMessage[i].type == 4) {
                this.responseMessage[i].message.quick_replies = this.responseMessage[i].message.quick_replies.filter((bt) => {
                    return bt.title.trim().length > 0
                });
                for (let t = 0; t < this.responseMessage[i].message.quick_replies.length; t++) {
                    let button = this.responseMessage[i].message.quick_replies[t];
                    if (!this.isBase64(button.payload)) {
                        if (button.title.trim().length > 0) {

                            if (!this.isBase64(button.payload)) {
                                let obj = JSON.stringify({
                                    "title": button.title,
                                    "postback_type": "text",
                                    "content": button.payload
                                });
                                let payload = new Buffer(obj).toString('base64');
                                this.responseMessage[i].message.quick_replies[t].payload = payload;
                            }

                        } else {
                            let obj = JSON.stringify({
                                "title": button.title,
                                "postback_type": "text",
                                "content": button.payload
                            });
                            let payload = new Buffer(obj).toString('base64');
                            this.responseMessage[i].message.quick_replies[t].payload = payload;
                        }
                    }

                }
            }
        }

    }

    public loadBuild() {
        // console_bk.log('this.blocks.content', this.blocks.content);
        this.responseMessage = [];
        if (this.blocks.content.trim().length > 0) {
            try {
                this.responseMessage = JSON.parse(this.blocks.content);
                this.updateDataOld();
                for (let k = 0; k < this.responseMessage.length; k++) {
                    if (this.responseMessage[k].type === 1) {
                        if (this.responseMessage[k].message.text == undefined) {
                            this.responseMessage[k].message.attachment.type = 'text';
                        }
                    }
                }


            } catch (error) {
                this.responseMessage = [];
            }
        }
        if (this.responseMessage.length === 0) {
            this.responseMessage.push({
                type: 1,
                message: {
                    attachment: {
                        type: 'text',
                        payload: {
                            template_type: "button",
                            text: "",
                            buttons: []
                        }
                    }
                }
            });
        }
        // kiem tra xoa luon nhung  group nao khong con elements nao
        this.responseMessage = this.responseMessage.filter((row) => {
            return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
        });
        // toan: bo sung xac dinh loai message
        this.responseMessage.filter((row) => {
            if (row.message.quick_replies) {
                row.type = 4;
            } else if (row.message.attachment && row.message.attachment.type === 'image') {
                row.type = 3;
            } else if (row.message.attachment && row.message.attachment.type === 'template') {
                row.type = 2;
            } else {
                row.type = 1;
            }
            for (let i = 0; i < this.responseMessage.length; i++) {
                if (this.responseMessage[i].type === 1) {
                    if (this.responseMessage[i].message.text != undefined) {
                        let res_text = {
                            type: 1,
                            message: {
                                attachment: {
                                    type: 'text',
                                    payload: {
                                        template_type: "button",
                                        text: this.responseMessage[i].message.text,
                                        buttons: []
                                    }
                                }
                            }
                        };
                        this.responseMessage[i] = res_text;
                    }
                }
            }
        });
        // width_value_list_card
        this.updateWidthOfListCard();
        this.checkShowQuickReply();
    }

    public isBase64(str) {
        try {
            return btoa(atob(str)) == str;
        } catch (err) {
            return false;
        }
    }

    private validatorsQuickReply = [this.validateTaginputGoiY];
    public errorMessagesQuickReply = {
        'mess_err_lenght@': ' Maxlength gợi ý ràng buộc <= 20 ký tự'
    };

    public validateTaginputGoiY(control: FormControl) {
        if (control.value.trim().length > 20) {
            return {
                'mess_err_lenght@': true
            };
        }
        return null;
    }

    public onTextChange(event) {
        // console_bk.log('event', event);
    }

    public getStyle(n) {
        if (n == 0) {
            return "rgba(183, 28, 28, 0.39)";
        } else {
            return "";
        }
    }

    // api format json facebook
    public copyToClipboard() {
        // this.clipboard.copy(this.blocks.content);
        // let actionOperate: Observable<any>;
        // actionOperate = this.intentService.getValidateFacebookJSON({ messages: this.blocks.content });
        // actionOperate.subscribe(
        //   (json) => {
        //     // console_bk.log('json:', json);
        //     if (json.code === 200) {
        //       this.clipboard.copy(json.data);
        //     }
        //   },
        //   (err) => {
        //     // console_bk.log(err);
        //   });
        this.clipboard.copy(this.json.trim());
    }

    public openDialogTraining() {
        if (this.json) {
            this.notificationService.showDanger('Vui lòng chọn khách hàng truyền thông');
            return;
        }
        let dialogRef = this.dialog.open(JsonDialogComponent, {
            // height: '310px',
            width: '50%',
            data: {
                message: this.json
            }
        });
    }

    public request() {
        if (!this.blocks.content) {
            this.notificationService.showDanger('Không có nội dung để copy');
            return;
        }
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getValidateFacebookJSON({
            messages: this.blocks.content
        });
        actionOperate.subscribe(
            (json) => {
                if (json.code === 200) {
                    try {
                        let js = JSON.parse(json.data);
                        if (js.length === 0) {
                            this.json = '';
                        } else {
                            this.json = json.data;
                        }
                        if (!this.json) {
                            this.notificationService.showDanger('Không có nội dung để copy');
                            return;
                        }
                        let dialogRef = this.dialog.open(JsonDialogComponent, {
                            width: '50%',
                            data: {
                                message: this.json
                            }
                        });
                    } catch (error) {
                        // console_bk.log(error);
                    }
                }
            },
            (err) => {
                // console_bk.log(err);
            });
    }


    public defaultPicture = 'assets/img/theme/no-photo.png';
    public profile: any = {
        picture: 'assets/img/app/profile/Nasta.png'
    };
    public uploaderOptions: NgUploaderOptions = {
        // url: 'http://website.com/upload'
        url: '',
    };

    public fileUploaderOptions: NgUploaderOptions = {
        // url: 'http://website.com/upload'
        url: '',
    };

    selectFile($event): void {
        let files = $event.target.files || $event.srcElement.files;
        let file = files[0];
        let formData = new FormData();
        formData.append("file", file);
        let req = new XMLHttpRequest();
        req.open("POST", "https://test.mideasvn.com:8206/api/upload");
        req.send(formData);
    }

    public deleteBlockItem() {
        let data = {
            blockId: this.blocks.id
        };
        EmitterService.get('DELETE_BLOCK_ITEM').emit(data);
    }

}