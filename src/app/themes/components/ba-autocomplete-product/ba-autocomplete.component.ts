
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ProductsService} from "../../../_services/products.service";
import {Inputs, Meta} from "../../../_models/actions.model";
import {EntityService} from "../../../_services/entity.service";

@Component({
  selector: 'ba-autocomplete-product',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteProductComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Input() entities: any;
  @Output() output = new EventEmitter<any>();
  
  @Input() products: ProductsModel[] = [];
  @Output() entity_id = new EventEmitter<any>();
  @Input() selected: any;
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;
  private name_text = '';

  constructor(private productService: ProductsService, private entityService: EntityService) {
  }

  public filterStates(val: string) {
    return val ? this.source.filter((s) => new RegExp(val, 'gi').test(s)) : this.source;
  }

  public keyChange(event) {
    event.stopPropagation();
    this.entity_id.emit(event.target.value);
  }

  public choiceData(data) {
    if (this.entities.length > 0) {
      this.emitTer(this.entities, data);
    }
  }
  
  public emitTer(inputs, data) {
      for (var i =0 ; i < inputs.length; i++) {
        if (inputs[i].name == data) {
          this.entity_id.emit(inputs[i].id);
        }
      }
  }

  public searchEntity(id) {
    for (var i = 0; i < this.entities.length; i++) {
      if (this.entities[i].id == id) {
        return this.entities[i].name;
      }
    }
    return '--';
  }


}

