
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ActionBasic} from "../../../_models/actions.model";

@Component({
  selector: 'ba-autocomplete-intent-inner',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteIntentInnerComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Output() output = new EventEmitter<any>();
  @Input() selected: any;
  
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;

  constructor() {

  }

  private change(event: any) {
    this.output.emit(event.value);
  }
  
}

