import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';
import { ProductsService } from '../../../_services/index';
import { NotificationService } from "../../../_services/notification.service";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
declare var $: any;
import {Subscription} from 'rxjs/Rx';

@Component({
    selector: 'product-delete',
    templateUrl: 'delete-product.component.html',
    styleUrls: ['delete-product.component.scss'],
    providers: [ProductsService]
})

export class ProductDeleteComponent implements OnInit {

    @ViewChild('inputSearch') input: ElementRef;
    @ViewChild('buttonSearch') buttonSearch: ElementRef;
    @ViewChild('cancel_product') cancel_product: ElementRef;

    private products = [];
    private eventSubscription: Subscription;

    constructor(private productService: ProductsService,
                private notificationService: NotificationService,
                private _componentService: ComponentInteractionService) {
    }

    ngOnInit() {
        this.loadListProducts();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-products') {
                    this.loadListProducts();
                }
            }
        );
    }

    public loadListProducts() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.productService.getListProductByAgentId(agentId)
                .then((products) => {
                    this.products = products;
                })
        }
    }


    public deleteProduct(productId) {
        this.productService.deleteProductById(productId)
            .then((product) => {
                this._componentService.eventPublisher$.next('load-products');
                this.notificationService.showSuccess('Xóa sản phẩm thành công');
            });
        this.cancel_product.nativeElement.click();
        $('body').removeClass("modal-open");
    }
}
