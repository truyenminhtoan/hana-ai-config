import { Component, OnInit, Input } from '@angular/core';
import { PAGE_MENU } from './sidebar-routes.config';
import { ConfigService } from '../../../_services/config.service';
import * as _ from 'underscore';
declare var $: any;
@Component({
  selector: 'ba-sidebar-remarketing',
  templateUrl: 'ba-sidebar-remarketing.component.html',
  styleUrls: ['ba-sidebar-remarketing.component.scss']
})
export class BaSidebarRemarketingComponent implements OnInit {
  private currentUser: any;
  private consoleHana: any;
  private menuItems: any[];
  @Input() menutype: any;

  public ngOnInit(): void {
    $.getScript('../../../../assets/js/sidebar-moving-tab.js');
    // $.getScript('../../../../assets/js/init/initTooltips');
    // $('[rel="tooltip"]').tooltip();

    this.consoleHana = this.configService.consoleHana;
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    if (this.currentUser) {
      $('.version-' + this.currentUser.version).show();
    }
    let temp = [];
    PAGE_MENU.filter((item) => {
      if (this.currentUser && this.currentUser.version === 1 && item.path === 'faq') {
        // haha
      } else {
        temp.push(item);
      }
    });

    let isIntegrationFacebook = false;
    let expiredDate = false;
    if (this.currentUser.app_using.integrations) {
      this.currentUser.app_using.integrations.filter((item) => {
        if (item.gatewayId === 1 && item.pageId && item.pageId.trim().length > 0) {
          isIntegrationFacebook = true;
          if (item.isExpire === 1) {
            expiredDate = true;
          }
        }
      });
    }

    // console.log('MEMNUUUU ' + this.menutype);
    // An growntool khi chua tich hop facebook
    if (!isIntegrationFacebook) {
      temp = _.reject(temp, (num) => { return num.path === 'growthtools' || num.path === 'customers' || num.path === 'campaigns' || num.path === 'setting-menu' || num.path === 'hashtags' || num.path === 'post'; });
    }
    if (expiredDate) {
      temp = _.reject(temp, (num) => { return num.path === 'growthtools' || num.path === 'campaigns'; });
    }

    if (this.menutype === 'traininghana') {
      temp = _.reject(temp, (num) => { return num.path === 'customers' || num.path === 'growthtools' || num.path === 'campaigns' || num.path === 'growthtools'; });
    } else if (this.menutype === 'menuremarketing') {
      temp = _.reject(temp, (num) => { return num.path === 'customers' || num.path === 'growthtools' || num.path === 'blocksv2' || num.path === 'faq' || num.path === 'hashtags' || num.path === 'post' || num.path === 'setting-menu'; });
    } else if (this.menutype === 'customerhana') {
      temp = _.reject(temp, (num) => { return num.path === 'blocksv2' || num.path === 'faq' || num.path === 'hashtags' || num.path === 'post' || num.path === 'setting-menu' || num.path === 'campaigns'; });
    }
    this.menuItems = temp;
  }
  // tslint:disable-next-line:member-ordering
  constructor(private configService: ConfigService) {

  }
}
