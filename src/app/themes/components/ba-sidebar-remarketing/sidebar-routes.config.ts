import { MenuType, RouteInfo } from './sidebar.metadata';

export const PAGE_MENU = [
  {
    title: 'ĐÃ INBOX',
    menuType: MenuType.LEFT,
    icon: 'people',
    path: 'customers',
    show: true
  },
  {
    title: 'CHƯA INBOX',
    menuType: MenuType.LEFT,
    icon: 'trending_up',
    path: 'growthtools',
    show: true
  },
  // {
  //   title: 'NỘI DUNG',
  //   menuType: MenuType.LEFT,
  //   icon: 'timeline',
  //   path: 'blocks',
  //   header_title: 'DANH SÁCH KHÁI NIỆM',
  //   show: true
  // },
  {
    title: 'NỘI DUNG MẪU',
    menuType: MenuType.LEFT,
    icon: 'filter_drama',
    path: 'blocksv2',
    header_title: 'NỘI DUNG V2',
    show: true
  },
  {
    title: 'CHIẾN DỊCH',
    menuType: MenuType.LEFT,
    icon: 'update',
    path: 'campaigns',
    show: true
  },
  {
    title: 'INBOX',
    menuType: MenuType.LEFT,
    icon: 'android',
    path: 'faq',
    show: true
  },
  {
    title: 'COMMENT',
    menuType: MenuType.LEFT,
    icon: 'hashtags',
    path: 'hashtags',
    show: true
 },
  {
    title: 'BÀI POST',
    menuType: MenuType.LEFT,
    icon: 'question_answer',
    path: 'post',
    show: true
  },
  // {
  //    title: 'COMMENT',
  //    menuType: MenuType.LEFT,
  //    icon: 'comment',
  //    path: 'comment',
  //    show: true
  // },
  {
    title: 'CÀI ĐẶT',
    menuType: MenuType.LEFT,
    icon: 'settings_applications',
    path: 'setting-menu',
    header_title: 'CÀI ĐẶT',
    show: true
  },

];
export const ROUTES: RouteInfo[] = [

];
