import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { AgentsModel } from '../../../_models/agents.model';
import { NotificationService } from '../../../_services/notification.service';
import { UserModel } from '../../../_models/user.model';
import { Observable} from "rxjs/Rx";
import { AgentService } from '../../../_services/agents.service';
import { ComponentInteractionService } from '../../../_services/compoent-interaction-service';
import { StringFormat } from '../../../themes/validators/string.format';
declare var $: any;
import {Subscription} from 'rxjs/Rx';
@Component({
    selector: 'agent-delete',
    templateUrl: 'agent-delete.component.html',
    styleUrls: ['agent-delete.component.css']
})
export class AgentDeleteComponent implements OnInit {

    private currentUser: UserModel;
    private agents = [];
    private currentAgent: AgentsModel = new AgentsModel();
    @ViewChild('btnCloseAdd') btnCloseAdd: ElementRef;
    @ViewChild('btnCloseEdit') btnCloseEdit: ElementRef;
    @ViewChild('name_new') name_new: ElementRef;
    @ViewChild('name_edit') name_edit: ElementRef;
    @ViewChild('description') description_input: ElementRef;
    
    private eventSubscription: Subscription;
    constructor(
        private agentService: AgentService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService) {
    }

    ngOnInit() {
        this.loadListAgent();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-agents') {
                    this.loadListAgent();
                }
                if (event == 'click-agent-edit') {
                    this.description_input.nativeElement.focus();
                }
            }
        );
    }
    public intAgent() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentAgent.name = '';
        this.currentAgent.partner_user_id = this.currentUser.id;
        this.currentAgent.partner_id = this.currentUser.partner_id;
        this.currentAgent.is_public = true;
        this.currentAgent.status = 1;
        this.currentAgent.time_zone = "GMT+7";
        this.currentAgent.language = "Việt Nam";
    }


    public loadListAgent() {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.getListAgentByPartnerId(currentUser.partner_id);
    }

    public getListAgentByPartnerId(partnerId) {
        try {
            this.agentService.getListAgentByPartnerId(partnerId)
                .then((data) => {
                    this.agents = data;
                    if (this.agents.length > 0) {
                        for (var i = 0; i < this.agents.length; i++) {
                            this.agents[i].tmp_name = this.agents[i].name;
                            this.agents[i].tmp_avatar_url = this.agents[i].avatar_url;
                            if (this.agents[i].name != null) {
                                this.agents[i].name = this.agents[i].name.slice(0, 20);
                                if ( this.agents[i].name.length >= 15) {
                                    this.agents[i].name += '...';
                                }
                            }
                            if (this.agents[i].description != null) {
                                this.agents[i].tmp_description = this.agents[i].description;
                                this.agents[i].description = this.agents[i].description.slice(0, 17);
                                this.agents[i].description_full = this.agents[i].description;
                                if ( this.agents[i].description.length >= 20) {
                                    this.agents[i].description += '...';
                                }
                            }
                        }
                        this.agents.sort(function(a, b) {
                            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            // names must be equal
                            return 0;
                        });
                    }
                });
        } catch (error) {

        }
    }
    
    public pushUpdate(callback) {
        let actionOperate: Observable<AgentsModel>;
        actionOperate = this.agentService.update(this.currentAgent);
        actionOperate.subscribe(
            (data) => {
                callback(true);
            },
            (err) => {
                callback(false);
            });
    }

    public checkName(name, id) {
        if (id != false) {
            for (var i = 0; i < this.agents.length; i++) {
                if (this.agents[i].id != id && this.agents[i].tmp_name.trim() == name.trim()) {

                    return false;
                }
            }
        } else {
            for (var i = 0; i < this.agents.length; i++) {
                if (this.agents[i].tmp_name.trim() == name.trim()) {
                    return false;
                }
            }
        }

        return true;
    }
    public updateAgent(fromValue: any) {
        this.currentAgent = new AgentsModel();
        this.intAgent();
        if (fromValue['id'] !== '') {
            if (fromValue['name'] != null && fromValue['name'].trim().length >= 1) {
                if (fromValue['name'].trim().length <= 50) {
                    if (StringFormat.checkRegexp(name, /([?*+!%`=^$@#[\]\\(){}|-])/)) {
                        this.notificationService.showWarning('Tên trợ lý không được chưa ký tự đặc biệt');
                        this.name_edit.nativeElement.focus();
                    } else {
                        this.agentService.getListAgentByPartnerId(this.currentUser.partner_id)
                            .then((data) => {
                                this.agents = data;
                                for (var i = 0; i < this.agents.length; i++) {
                                    this.agents[i].tmp_name = this.agents[i].name;
                                    this.agents[i].tmp_avatar_url = this.agents[i].avatar_url;
                                    if (this.agents[i].name != null) {
                                        this.agents[i].name = this.agents[i].name.slice(0, 20);
                                        if ( this.agents[i].name.length >= 15) {
                                            this.agents[i].name += '...';
                                        }
                                    }
                                    if (this.agents[i].description != null) {
                                        this.agents[i].tmp_description = this.agents[i].description;
                                        this.agents[i].description = this.agents[i].description.slice(0, 17);
                                        this.agents[i].description_full = this.agents[i].description;
                                        if ( this.agents[i].description.length >= 20) {
                                            this.agents[i].description += '...';
                                        }
                                    }
                                }
                                this.agents.sort(function(a, b) {
                                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                                    if (nameA < nameB) {
                                        return -1;
                                    }
                                    if (nameA > nameB) {
                                        return 1;
                                    }
                                    // names must be equal
                                    return 0;
                                });


                                // thuc hien cap nhat
                                if (this.checkName(fromValue['name'], fromValue['id'])) {
                                    this.currentAgent.name = fromValue['name'];
                                    this.currentAgent.id = fromValue['id'];
                                    this.currentAgent.avatar_url = fromValue['avatar_url'];
                                    this.currentAgent.description = fromValue.description;
                                    if (this.currentAgent.name != undefined && this.currentAgent.name != null) {
                                        this.currentAgent.name = this.currentAgent.name.trim();
                                    }
                                    if (this.currentAgent.avatar_url != undefined && this.currentAgent.avatar_url != null) {
                                        this.currentAgent.avatar_url = this.currentAgent.avatar_url.trim();
                                    }
                                    if (this.currentAgent.description != undefined && this.currentAgent.description != null) {
                                        this.currentAgent.description = this.currentAgent.description.trim();
                                    }
                                    this.pushUpdate((cb) => {
                                        this.btnCloseEdit.nativeElement.click();
                                        if (cb) {
                                            this.loadListAgent();
                                            this._componentService.eventPublisher$.next('load-sidebar');
                                            this._componentService.eventPublisher$.next('load-agents');

                                            $('body').removeClass("modal-open");
                                            this.notificationService.showSuccess('Cập nhật trợ lý thành công');
                                        } else {
                                            this.notificationService.showDanger('Cập nhật trợ lý thất bại');
                                        }

                                    });
                                } else {
                                    this.name_edit.nativeElement.focus();
                                    this.notificationService.showWarning('Tên trợ lý đã tồn tại');
                                }

                            });
                    }
                } else {
                    this.name_edit.nativeElement.focus();
                    this.notificationService.showWarning('Tên trợ lý tối đa 50 kí tự');
                }
            } else {
                this.name_edit.nativeElement.focus();
                this.notificationService.showWarning('Tên trợ lý không được rỗng');
            }
        }  else {
            this.notificationService.showWarning('Id trợ lý  không được rỗng');
        }
    }
    public checkEmptyUpdate(value) {
        if (value != '') {
        }
    }

    public updateStatus() {
        this.currentAgent = new AgentsModel();
        this.intAgent();
    }
}
