import {Component, OnInit} from '@angular/core';
import { NotificationService } from '../../../_services/notification.service';
import { EntitieModel } from '../../../_models/entities.model';
import { EntityService } from '../../../_services/entity.service';
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
declare var $: any;

@Component({
    selector: 'entity-delete',
    templateUrl: 'entity-delete.component.html',
    styleUrls: ['entity-delete.component.scss']
})
export class EntityDeleteComponent implements OnInit {

    private lists: EntitieModel[] = [];
    private eventSubscription: Subscription;

    constructor(
        private entityService: EntityService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService) {
    }

    ngOnInit() {
        this.loadListEntity();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-entities') {
                    this.loadListEntity();
                }
            }
        );
    }

    public loadListEntity() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entityService.getListEntityPromise(agentId, false)
                .then((data) => {
                    this.lists = data;
                })
        }
        setTimeout(function () {
        }, 1000);
    }

    public deleteEntity(id) {
        this.entityService.deleteEntity(id)
            .then((entity) => {
                this._componentService.eventPublisher$.next('load-entities');
                this.notificationService.showSuccess('Xóa khái niệm thành công');
            });
        $('body').removeClass("modal-open");
    }

}
