import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef  } from '@angular/core';
import { IntentEntitySuggestion } from '../../../_models/intents.model';
declare var $: any;
@Component({
    selector: 'ba-modal-suggestion',
    templateUrl: 'ba-modal-suggestion.html',
    styleUrls: ['ba-modal-suggestion.scss']
})
export class BaModalSuggestionComponent implements OnInit {
    @Input() title: any;
    @Input() vitri: any;
    @Input() response: IntentEntitySuggestion[] = [];
    @Output() output = new EventEmitter<any>();
    @ViewChild('content') content: ElementRef;

    public respon : IntentEntitySuggestion[] = [];

    // constructor(){}

    public ngOnInit() {
        $('.modal').appendTo('body');
        this.respon = this.response.slice();
        if (!this.respon || this.respon.length === 0) {
            let res: IntentEntitySuggestion = new IntentEntitySuggestion();
            res.value = '';
            res.status = 1;
            this.respon.push(res);
        }
        this.autoAdd();
    }

    public autoFocus() {
        $('#sug-noticeModal-' + this.vitri).modal('show');
        let that = this;
        setTimeout(function () {
            that.content.nativeElement.focus();
        }, 1000);
    }

    public handleAddInput(event, i) {
        // if (this.respon.length > 1) {
        //     this.respon = this.respon.filter((row) => {
        //         return (row.value != null && row.value.length > 0)
        //     });
        // }

        if (this.respon[this.respon.length - 1].value != null && this.respon[this.respon.length - 1].value.trim().length > 0) {
            this.autoAdd();
        }

        //this.autoAdd();
    }

    public handleAddInputUpdate(event, i) {
        this.respon[i].value = event.target.value;
    }

    public handleDeleteInput(obj) {
        if (this.respon.length > 1) {
            this.respon = this.respon.filter((row) => {
                return row !== obj
            });
        }
    }

    public saveInput() {
        var respon = this.respon.filter((row) => {
            return (row.value != null && row.value.length > 0)
        });
        
        this.output.emit(respon);
    }

    public checkName(arr, key) {
        for (var i =0 ;i < arr.length; i++) {
            if (arr[i] == key) {
                return i;
            }
        }
        return -1;
    }

    public autoAdd() {
        if (this.respon != undefined && this.respon.length > 0 && this.respon[0].value != undefined &&  this.respon[0].value.length > 0) {
            let res: IntentEntitySuggestion = new IntentEntitySuggestion();
            res.value = '';
            res.status = 1;
            this.respon.push(res);
        }
    }
}
