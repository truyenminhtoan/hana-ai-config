import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ba-switch',
  templateUrl: 'ba-switch.component.html',
  styleUrls: ['ba-switch.component.scss']
})
export class BaSwitchComponent {
  @Input() private checked: boolean = true;
  @Output() private output = new EventEmitter<any>();

  public change(event) {
    this.output.emit(!event.checked);
  }
}
