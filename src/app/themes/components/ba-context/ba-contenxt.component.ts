import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Observable';
import { IntentService } from '../../../_services/intents.service';
import { EmitterService } from "../../../_services/emitter.service";
import { IntentContextsSuggestionsModel, SuggestionsIntentsModel } from "../../../_models/intents.model";
import { ActivatedRoute } from '@angular/router';
declare var Taggle: any;
declare var $: any;
@Component({
  selector: 'ba-context',
  templateUrl: 'ba-contenxt.component.html',
  styleUrls: ['ba-contenxt.component.scss']
})
export class BaContextComponent implements OnInit {
  @Input() private currentIntent: any = {};
  @Input() private autocomplete_context: any;
  @Input() private intent_contexts: any;
  @Input() private is_suggestion_predict: boolean;
  @Output() private is_suggestion_predictOutPut: any = new EventEmitter<any>();
  @Output() private output = new EventEmitter<any>();
  @Output() private intent_contexts_outOut: any = new EventEmitter<any>();
  @Output() private contexts_suggestionsOutPut: any = new EventEmitter<any>();
  @Output() private context_outOut: any = new EventEmitter<any>();
  // response
  private contexts = [];
  private suggestion_intents = [];
  private list_context_full = [];
  private intentId;
  private sub: any;
  private list_contexts = [];
  private context_out = [];
  private contexts_suggestions_old = [];
  private is_load_content = true;
  private lists_usersays = [];

  // toantm
  private intentSugest: any = [];

  constructor(private route: ActivatedRoute, private intentService: IntentService) {
    this.getAutoCompleteContent();
    this.sub = this.route.params.subscribe((params) => {
      this.intentId = params['id'];
      this.is_load_content = false;
      let actionOperate = this.intentService.getDetailIntent(this.intentId);
      actionOperate.subscribe(
        (data) => {
          if (data.usersays != undefined) {
            for (var i = 0; i < data.usersays.length; i++) {
              this.lists_usersays.push(data.usersays[i].content);
            }
          }
          if (data.contexts_suggestions != undefined) {
            this.contexts_suggestions_old = data.contexts_suggestions;
          }
          var ids = [];
          if (data.intent_contexts != undefined) {
            if (this.intent_contexts == undefined) {
              this.intent_contexts = [];
            }
            if (this.intent_contexts.length == 0) {
              for (var i = 0; i < data.intent_contexts.length; i++) {
                if (data.intent_contexts[i].type == 1) {
                  this.intent_contexts.push({
                    value: data.intent_contexts[i].contexts.name,
                    display: data.intent_contexts[i].contexts.name,
                    intent_id: data.intent_contexts[i].intent_id,
                    id: data.intent_contexts[i].contexts.id,
                    status: data.intent_contexts[i].contexts.status,
                    created_date_context_item: data.intent_contexts[i].contexts.created_date,
                    created_date: data.intent_contexts[i].created_date
                  });
                }
              }
            }
            for (var i = 0; i < data.intent_contexts.length; i++) {
              if (data.intent_contexts[i].type == 1) {
                this.context_out.push(data.intent_contexts[i]);
                ids.push(data.intent_contexts[i].contexts.id);
              }
            }
          }
          // update lai cac intent moi them vao
          if (ids.length > 0) {
            let actionOperate: Observable<any>;
            actionOperate = this.intentService.getListIntentByContentIds(ids);
            actionOperate.subscribe(
              (data_suggestion) => {
                // toantm
                this.intentSugest = [];
                data_suggestion.filter((item) => {
                  this.intentSugest.push({ value: item.id, display: item.name });
                });
                // end toantm
                this.suggestion_intents = [];
                if (data.contexts_suggestions !== undefined) {
                  let count = 1;
                  for (var i = 0; i < data.contexts_suggestions.length && count < 11; i++) {
                    var status = false;
                    if (data.contexts_suggestions[i].status === 1) {
                      count++;
                      status = true;
                      this.suggestion_intents.push({
                        'id': data.contexts_suggestions[i].suggestion_intents.id,
                        'name': data.contexts_suggestions[i].suggestion_intents.name,
                        'status': status,
                        'priority': data.contexts_suggestions[i].priority
                      });
                    }
                  }
                  //them cac intent moi neu co
                  // toantm: khong tu dong them.
                  // for (var i = 0; i < data_suggestion.length; i++) {
                  //   if (this.checkExist(data_suggestion[i].name)) {
                  //     this.suggestion_intents.push({
                  //       'id': data_suggestion[i].id,
                  //       'name': data_suggestion[i].name,
                  //       'status': false,
                  //       'priority': (i + this.suggestion_intents.length)
                  //     });
                  //   }
                  // }
                  // 1. Vào màn hình intent A bất kỳ, chọn ngữ cảnh đầu ra (có intent suggestion) và lưu lại
                  // 2. Bỏ ngữ cảnh của một intent bất kỳ trong danh sách sugestion ở trên
                  // 3. Mở lại intent A lên
                  // KQ: Trong danh sách suggestion vẫn hiển thị intent đã bỏ đi ngữ cảnh
                  // MM: Không hiển thị intent đã bỏ ngữ cảnh ở trên
                  this.suggestion_intents = this.suggestion_intents.filter((row) => {
                    return this.removerIntent(data_suggestion, row.name)
                  });
                  //Sort
                  // toantm bo sort. Da sort tren server
                  // this.suggestion_intents.sort(function(a, b) {
                  //   var nameA = a.priority, nameB = b.priority
                  //   if (nameA < nameB) {
                  //     return -1;
                  //   }
                  //   if (nameA > nameB) {
                  //     return 1;
                  //   }
                  //   return 0;
                  // });

                  // Chỉnh lại Intent đang mở không hiển thị trong danh sách suggestion của chính intent đó
                  // 1. Mở intent A lên (có ngữ cảnh đầu vào 'chungcu' và có câu nói ng dùng là 'mua chung cư')
                  // 2. Cấu hình ngữ cảnh đầu ra cho intent A là 'chungcu'
                  // KQ: Hiển thị ds suggestion intent có cả intent A với nội dung 'mua chung cư'
                  // MM: Không dùng intent A để làm suggestion cho chính nó (k hiển thị câu 'mua chung cư')
                  this.suggestion_intents = this.suggestion_intents.filter((row) => {
                    return !this.removerUsersay(this.lists_usersays, row.name)
                  });

                  this.intentSugest = this.intentSugest.filter((item) => !(item.value === this.intentId));

                }
                this.is_load_content = true;
              },
              (err_tmp) => {
              });
          } else {
            this.is_load_content = true;
          }
          this.is_load_content = true;
        },
        (err) => {
        });
    });

    let that = this;
    setInterval(function () {
      that.save();
    }, 800);
  }

  public checkExist(name) {
    for (var i = 0; i < this.suggestion_intents.length; i++) {
      if (this.suggestion_intents[i].name.trim() == name.trim()) {
        return false;
      }
    }

    return true;
  }

  public ngOnInit(): void {
  }

  public emitSave() {
    EmitterService.get('SAVE_LIST_CARD').emit([]);
  }


  public onAddOnRemove() {
    this.getListIdContext();
  }

  public save() {
    //console.log('this.intent_contexts', this.intent_contexts);
    this.intent_contexts_outOut.emit(this.intent_contexts);
    // tao contexts_suggestions
    // tslint:disable-next-line:variable-name
    let contexts_suggestions = [];
    let i = 1;
    this.suggestion_intents.filter((item) => {
      if (item.id !== item.name) {
        let context = new IntentContextsSuggestionsModel();
        context.status = 1;
        context.priority = i++;
        let suggestionIntent = new SuggestionsIntentsModel();
        suggestionIntent.id = item.id;
        suggestionIntent.name = item.name;
        context.suggestion_intents = suggestionIntent;
        contexts_suggestions.push(context);
      }
    });

    if (this.is_suggestion_predict === false) {
      contexts_suggestions = [];
    }
    this.contexts_suggestionsOutPut.emit(contexts_suggestions);
    this.is_suggestion_predictOutPut.emit(this.is_suggestion_predict);
    this.emitSave();
  }

  public getAutoCompleteContent() {
    let actionOperateIt: Observable<any>;
    actionOperateIt = this.intentService.initIntentView(localStorage.getItem('currentAgent_Id'), true);
    actionOperateIt.subscribe(
      (data) => {
        this.list_context_full = data.contexts;
        let context = _.pluck(data.contexts, 'name');
        this.autocomplete_context = context;
      },
      (err) => {
      });
  }

  public getListIdContext() {
    let ids = [];
    for (var i = 0; i < this.list_context_full.length; i++) {
      for (var j = 0; j < this.intent_contexts.length; j++) {
        if (this.list_context_full[i].name.trim() == this.intent_contexts[j].value.trim()) {
          ids.push(this.list_context_full[i].id);
        }
      }
    }
    if (ids.length > 0) {
      let actionOperate: Observable<any>;
      actionOperate = this.intentService.getListIntentByContentIds(ids);
      actionOperate.subscribe(
        (data) => {
          this.intentSugest = [];
          // toantm
          data.filter((item) => {
            this.intentSugest.push({ value: item.id, display: item.name });
          });
          // end toantm
          // for (var i = 0; i < data.length; i++) {
          //   data[i].priority = false;
          //   if (this.checkExist(data[i].name)) {
          //     if (this.checkStatus() == false) {
          //       data[i].status = true;
          //     }
          //     this.suggestion_intents.push(data[i]);
          //   }
          // }
          // 1. Vào màn hình intent A bất kỳ, chọn ngữ cảnh đầu ra (có intent suggestion) và lưu lại
          // 2. Bỏ ngữ cảnh của một intent bất kỳ trong danh sách sugestion ở trên
          // 3. Mở lại intent A lên
          // KQ: Trong danh sách suggestion vẫn hiển thị intent đã bỏ đi ngữ cảnh
          // MM: Không hiển thị intent đã bỏ ngữ cảnh ở trên
          this.suggestion_intents = this.suggestion_intents.filter((row) => {
            return this.removerIntent(data, row.name)
          });

          // Chỉnh lại Intent đang mở không hiển thị trong danh sách suggestion của chính intent đó
          // 1. Mở intent A lên (có ngữ cảnh đầu vào 'chungcu' và có câu nói ng dùng là 'mua chung cư')
          // 2. Cấu hình ngữ cảnh đầu ra cho intent A là 'chungcu'
          // KQ: Hiển thị ds suggestion intent có cả intent A với nội dung 'mua chung cư'
          // MM: Không dùng intent A để làm suggestion cho chính nó (k hiển thị câu 'mua chung cư')
          this.suggestion_intents = this.suggestion_intents.filter((row) => {
            return !this.removerUsersay(this.lists_usersays, row.name)
          });

          this.intentSugest = this.intentSugest.filter((item) => !(item.value === this.intentId));
          this.save();
        },
        (err) => {
        });
    } else {
      this.intentSugest = [];
      this.save();
    }
  }

  public updatePriority(name) {
    for (var i = 0; i < this.suggestion_intents.length; i++) {
      if (this.suggestion_intents[i].name == name) {
        if (this.suggestion_intents[i].status == true || this.suggestion_intents[i].status == 1) {
          this.suggestion_intents[i].status = false;
        } else {
          this.suggestion_intents[i].status = true;
        }
      }
    }
    this.save();
  }

  public checkStatus() {
    var n = 0;
    for (var i = 0; i < this.suggestion_intents.length; i++) {
      if (this.suggestion_intents[i].status == 1 || this.suggestion_intents[i].status == true) {
        n++;
      }
    }
    if (n == 10) {
      return true;
    }
    return false;
  }

  public removerIntent(lists, name) {
    for (var i = 0; i < lists.length; i++) {
      if (lists[i].name.trim() == name.trim()) {
        return true;
      }
    }

    return false;
  }

  public removerUsersay(lists, name) {
    for (var i = 0; i < lists.length; i++) {
      if (lists[i].trim() == name.trim()) {
        return true;
      }
    }

    return false;
  }

  public changeSuggestionPredict() {
    this.is_suggestion_predict = !this.is_suggestion_predict;
    // 1. Vào màn hình chức năng intent
    // 2. Chọn ngữ cảnh đầu ra cho intent sao cho có dữ liệu intent suggestion
    // KQ: Danh sách suggestion mặc định không được check chọn intent nào hết
    // MM: Chỉnh lại mặc định check chọn tối đa 10 intent đầu tiên trong danh sách suggestion, còn lại thì không được chọn
    // @Xem xử lý chỗ nếu chọn nhiều ngữ cảnh thì cũng chỉ check chọn tối đa 10 intent thôi
    for (var i = 0; i < this.suggestion_intents.length; i++) {
      if (this.checkStatus() == false) {
        this.suggestion_intents[i].status = true;
      }
    }
    this.save();
  }

  public getStyleBackground(status) {
    if (status == true) {
      return '#3f51b5';
    }
    return '';
  }

  public getStyleColor(status) {
    if (status == true) {
      return '#fff';
    }
    return '';
  }
}
