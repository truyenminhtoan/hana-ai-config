import { Component, ViewEncapsulation, Input, OnInit, AfterViewInit, Output, EventEmitter, NgZone, AfterContentInit } from '@angular/core';
import * as _ from 'underscore';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { EmitterService } from '../../../_services/emitter.service';
import { NotificationService } from '../../../_services/notification.service';
import { ConfigService } from '../../../_services/config.service';
declare var $: any;
@Component({
  selector: 'ba-file-upload',
  // encapsulation: ViewEncapsulation.Native,
  templateUrl: 'ba-file-upload.component.html',
  styleUrls: ['ba-file-upload.component.scss']
})
export class BaFileUploadComponent implements OnInit {
  @Input() private id_element: string;
  @Input() private url: string;
  @Input() private type: string;
  @Output() private output = new EventEmitter<any>();
  
  private is_uploading = false;

  constructor(private notificationService: NotificationService, private configService: ConfigService) {

  }

  public ngOnInit(): void {
    if (this.url === undefined) {
      this.url = '';
    }
  }

  selectFile($event): void {
    var files = $event.target.files || $event.srcElement.files;
    this.is_uploading = true;
    this.makeFileRequest(this.configService.HOST_UPLOAD_IMAGE, files, this.type).subscribe((res) => {
      if (res != undefined) {
        this.url = this.configService.HOST_STATIC_FILE + res;
        this.output.emit(this.url);
        this.emitSave();
        this.is_uploading = false;
      }
    });
  }

  public emitSave() {
    this.output.emit(this.url);
    EmitterService.get('SAVE_WHEN_UPLOAD_IMAGE').emit([]);
  }

  private makeFileRequest(url: string, files: File[], typeRequest: string): Observable<any> {
    return Observable.create(observer => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      var kb = (files[0].size/1000);
      if (kb <= 5000 && (files[0].type == "image/png" || files[0].type == "image/jpg"
        || files[0].type == "image/gif" || files[0].type == "image/jpeg")) {
        formData.append("file", files[0]);
        formData.append("type", typeRequest);
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              observer.next(JSON.parse(xhr.response));
              observer.complete();
            } else {
              observer.error(xhr.response);
            }
          }
        };
        xhr.open('POST', url, true);
        xhr.send(formData);
      } else {
        this.is_uploading = false;
        this.notificationService.showDanger('Kích thước file <= 5M và chỉ chấp nhận các loại sau: jpg; gif; png; jpeg')
      }
    });
  }

}
