import { Component, ViewEncapsulation, Input, OnInit, AfterViewInit, Output, EventEmitter, NgZone, AfterContentInit } from '@angular/core';
import { Self } from '@angular/core';
import { ControlValueAccessor, NgModel } from '@angular/forms';
import * as _ from 'underscore';
declare var Taggle: any;
declare var $: any;
import { UUID } from 'angular2-uuid';
@Component({
  selector: 'ba-faq',
  // encapsulation: ViewEncapsulation.Native,
  templateUrl: 'ba-faq.component.html',
  styleUrls: ['ba-faq.component.scss']
})
export class BaFaqComponent implements OnInit {
  @Input() private intentId: string;
  @Input() private index: any;
  @Input() private typeAnswer: any;
  @Input() private typeFaq: any;
  @Input() private currentIntent: any;
  @Input() private usersays: any;
  @Input() private blocks: any;
  @Input() private responseBlocks: any[];
  @Input() private response: any;
  @Output() private output = new EventEmitter<any>();
  @Output() private outputDelete = new EventEmitter<any>();
  // tslint:disable-next-line:variable-name
  private response_message = [];
  private usersaysTemp: any = [];
  private responseTextType = true;
  private autoCompleteblocks = [{ value: 1, display: 'block1' }, { value: 2, display: 'block2' }];

  private blockSelected: any[] = [];
  private filteredDataMultiple: any[] = [{ id: '1', name: 'bloc 1' }, { id: '2', name: 'bloc 2' }];
  private isToInbox = false;
  private textToInbox = '';

  public ngOnInit(): void {
    try {
      if (this.currentIntent && this.currentIntent.inner_action && this.currentIntent.inner_action === 'createInboxFromComment') {
        //  alert(JSON.stringify(this.currentIntent));
        this.isToInbox = true;
      }
      if (this.currentIntent && this.currentIntent.intent_entities && this.currentIntent.intent_entities[0]) {
        //  alert(JSON.stringify(this.currentIntent));
        this.textToInbox = this.currentIntent.intent_entities[0].default_value;
        // alert(this.textToInbox);
      }

      if (this.typeAnswer) {
        this.responseTextType = this.typeAnswer === 'text' ? true : false;
      }
      this.usersaysTemp = _.clone(this.usersays.filter((item) => item.status === 1));
      if (_.size(this.response) > 0) {
        let obj = JSON.parse(this.response[0].content);
        this.response_message = obj[0].message.posibleTexts;
      }
      if (!this.responseTextType && this.responseBlocks && this.responseBlocks.length > 0) {
        // this.responseTextType = false;
        this.responseBlocks.filter((item) => {
          let bl = this.blocks.filter((it) => it.value === item.block_id)[0];
          // alert(JSON.stringify(bl));
          if (bl) {
            this.blockSelected.push(bl);
          }
        });
      }
      // alert(JSON.stringify(this.blockSelected));
      // tslint:disable-next-line:no-empty
    } catch (error) {

    }

  }

  public save() {
    let intent = { type_answer: '', index: this.index, usersays: [], response: '', response_message_blocks: [], intent_entities: [] };
    this.usersays.filter((item) => {
      if (item.is_new) {
        delete item.is_new;
        // delete item.id;
      }
    });
    intent.usersays = this.usersays;
    intent.type_answer = this.responseTextType ? 'text' : 'block';
    if (this.responseTextType) {
      intent.response = JSON.stringify([{ type: 1, message: { posibleTexts: this.response_message } }]);
    } else {
      this.blockSelected.filter((item) => {
        intent.response_message_blocks.push({ intent_id: this.intentId, block_id: item.value });
      });
      let i = 1;
      intent.response_message_blocks.filter((item) => {
        item.priority = i++;
      });
      // intent.response_message_blocks = [];
      // let block = _.pluck(this.blockSelected, 'value');
    }
    // set entity
    let entity: any = {};
    entity.entity_name = 'sys.any';
    entity.param = 'noi_dung_inbox';
    entity.default_value = this.textToInbox ? this.textToInbox : '';
    entity.lifespan = 2;
    entity.priority = 1;
    entity.status = 1;
    intent.intent_entities = [entity];

    // alert(JSON.stringify(intent));
    this.output.emit(intent);
  }

  public changeSwitch(event) {
    if (event.checked) {
      this.currentIntent.inner_action = 'createInboxFromComment';
    } else {
      this.currentIntent.inner_action = null;
    }
  }

  public delete() {
    this.outputDelete.emit(this.index);
  }

  public onAdd(tag) {
    this.usersays.push({ id: UUID.UUID(), content: tag.value, status: 1, is_new: true });
    this.usersaysTemp = _.clone(this.usersays.filter((item) => item.status === 1));
  }

  public onRemove(tag) {
    let data = tag.value;
    data.status = -1;
    if (tag.value.is_new) {
      this.usersays = _.reject(this.usersays, (item) => tag.value.id === item.id && item.status === -1);
    } else {
      this.usersaysTemp = this.usersays.filter((item) => item.status === 1);
    }
  }

  public filterDataMultiple(event) {
    this.filteredDataMultiple = [{ id: '1', name: 'bloc 1' }, { id: '2', name: 'bloc 2' }];
  }

  public onItemAdded(event) {
    if (!this.responseBlocks) {
      this.responseBlocks = [];
    }
    this.responseBlocks.filter((item) => {
      if (item.intent_id === event.value && item.status === -1) {
        item.status = -1;
        let block = { intent_id: this.intentId, block_id: event.value };
        this.responseBlocks.push(block);
      }
    });
  }
  public onItemRemoved(event) {
    // alert(JSON.stringify(event));
    // let block = {intent: this.intentId, block_id: event.value};
    if (this.responseBlocks && this.responseBlocks.length > 0) {
      this.responseBlocks.filter((item) => {
        if (item.intent_id === event.value) {
          item.status = -1;
        }
      });
    }
    // this.blockSelected.push(event);
  }
  public onSelected(event) {
    // alert(JSON.stringify(event));
    // this.blockSelected.push(event);
  }
}
