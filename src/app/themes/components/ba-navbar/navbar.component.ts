import { ConfigService } from './../../../_services/config.service';
import { ApplicationService } from './../../../_services/application.service';
import { Component, OnInit, AfterViewInit, Inject, ChangeDetectorRef, Input } from '@angular/core';
import { ROUTES, PAGE_MENU } from '../../sidebar/sidebar-routes.config';
import { MenuType } from '../../sidebar/sidebar.metadata';
import { Router, ActivatedRoute, Params, UrlSegment } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { saveAs } from 'file-saver';
import { DOCUMENT } from '@angular/platform-browser';
import { UUID } from 'angular2-uuid';
declare var gapi: any;
declare var $: any;
import _ from 'underscore';
@Component({
  selector: 'navbar-cmp',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css']
})
export class NavbarComponent implements AfterViewInit, OnInit {
  private listTitles: any[];


  private location: Location;
  // private auth2: any;
  private currentUser: any;
  // tslint:disable-next-line:variable-name
  private title_page: string;
  private consoleHana;
  private appName: string = '';
  private avatarUrl: string = '';
  private pageUrl: string = '';
  private menus: any = [];
  private menusAvatar: any = [];
  private token = '';
  private appId;
  private gwUrl;
  private isFacebookIntegrate = false;

  constructor( @Inject(DOCUMENT) private document, private cdRef: ChangeDetectorRef, private applicationService: ApplicationService, location: Location, private router: Router, private activatedRoute: ActivatedRoute, private configService: ConfigService) {
    this.location = location;
    // router.events.subscribe((url: any) => url);
    // if (!localStorage.getItem('token')) {
    //   this.token = UUID.UUID();
    //   localStorage.setItem('token', this.token);
    // }

  }
  // tslint:disable-next-line:one-line
  public ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser.app_using) {
        // $('#token').val(this.currentUser.app_using.id);
        // this.token = this.currentUser.app_using.id;
      }
    }

  }


  public getTryUsing() {
    let integrations = this.currentUser.app_using.integrations;
    let fbInte = _.find(integrations, (inte) => {
      return inte.gatewayId === 1;
    });
    if (fbInte) {
      if (fbInte.isExpire === 0) {
        let body = {
          page_id: fbInte.pageId,
          partner_id: this.currentUser.partner_id
        }
        this.applicationService.getTryUsing(body)
          .then((apps) => {

            // console.log('paypay ' + JSON.stringify(apps));
            this.currentUser.payment = apps.data;
            this.currentUser.paymentstatus = false;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          });
      }
    }
  }

  public ngAfterViewInit(): void {
    // this.loadListApps();
    this.getListApplication();
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

      if (this.currentUser.app_using) {
        this.appId = this.currentUser.app_using.applications[0].app_id;
        this.appName = this.currentUser.app_using.company;
        this.token = this.currentUser.app_using.id;
        this.currentUser.partner_id = this.currentUser.app_using.id;
        $('#token').val(this.currentUser.app_using.id);
        if (this.currentUser.app_using.integrations) {
          this.currentUser.app_using.integrations.filter((item) => {
            if (item.gatewayId === 1 && item.pageId) {
              this.avatarUrl = item.picture;
              this.pageUrl = 'https://facebook.com/' + item.pageId;
              this.isFacebookIntegrate = true;

            }
          });
        }
      }
      this.hideMenu();
    } else {
      this.hideMenu();
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    }
    this.consoleHana = this.configService.consoleHana;
    this.gwUrl = this.configService.HOST_MY;
    this.cdRef.detectChanges();
  }
  public hideMenu() {
    // hide/show menu
    let currentUrl = this.router.url;
    let lastIndex = currentUrl.indexOf('?');
    if (lastIndex > -1) {
      currentUrl = currentUrl.substr(1, lastIndex - 1);
    } else {
      currentUrl = currentUrl.substr(1);
    }
    currentUrl = currentUrl.replace(/\//g, '_');

    if (this.currentUser) {
      $('.version-' + this.currentUser.version).show();
      $('.version-' + this.currentUser.version + '_role-' + this.currentUser.role_id).show();
      $('.role-' + this.currentUser.role_id).show();
      $('.navbar-' + currentUrl).css('border-bottom', '2px solid #e9604a');
      $('.navbar-minimize').show();
      if (currentUrl.indexOf('application-hana') > -1) {
        $('.version-' + this.currentUser.version).hide();
        $('.version-' + this.currentUser.version + '_role-' + this.currentUser.role_id).hide();
        $('.role-' + this.currentUser.role_id).hide();
        $('.navbar-minimize').hide();
      }
    } else {
      if (currentUrl.indexOf('application-hana') > -1) {
        $('.navbar-minimize').hide();
      }
    }

    if (currentUrl.indexOf('application-hana') === -1) {
      // hide menu
      if (!this.isFacebookIntegrate) {
        $('.nav-remarketing').hide();
        $('.nav-customer').hide();
      } else {
        $('.nav-remarketing').show();
        $('.nav-customer').show();
      }
    }
  }
  // tslint:disable-next-line:one-line
  public getTitle() {
    let title = '';
    // let titlee = this.location.prepareExternalUrl(this.location.path());
    // if (titlee.charAt(0) === '#') {
    //   titlee = titlee.slice(8);
    // }
    // this.listTitles.filter((item) => {
    //   if (item.children) {
    //     let name = item.children.filter((it) => it.path === titlee)[0];
    //     if (name) {
    //       title = name.header_title;
    //       return;
    //     }
    //   } else {
    //     if (item.path === titlee || titlee.indexOf(item.path) != -1) {
    //       title = item.header_title;
    //       return;
    //     }
    //   }
    // });

    return title;
  }
  public getPath() {
    return this.location.prepareExternalUrl(this.location.path());
  }
  public logout() {

    // console.log('SIGGGGGGGOUT       ' + JSON.stringify(gapi.auth));
    // gapi.auth.signOut()
    //   let auth2 = gapi.auth2.getAuthInstance();
    //   auth2.signOut().then( () => {
    //     console.log('User signed out.');
    //   });
    localStorage.removeItem('currentUser');
    localStorage.removeItem('apphanausing');
    localStorage.removeItem('session_time');
    // this.router.navigate(['/login-hana'], { queryParams: { returnUrl: this.router.url } });
    // this.router.navigate(['/login-hana'], { queryParams: { returnUrl: this.router.url } });
    this.router.navigate(['/login-hana']);
  }
  public lock() {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    user.is_lock = true;
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.router.navigate(['/lock'], { queryParams: { returnUrl: this.router.url } });
  }
  // tslint:disable-next-line:eofline

  public configHana() {
    localStorage.setItem('remarketting', 'true');
  }

  public redirect(url) {
    window.location.href = this.consoleHana + '/' + url;
  }

  public redirectOrder() {
    window.location.href = "#/customers/orders";;
  }

  public redirectCustomerSearch() {
    window.location.href = "#/customer-search";;
  }

  public redirectTaskSearch() {
    window.location.href = "#/demand-search";;
  }

  public exportBill() {
    if (!this.appId) {
      return;
    }
    let url = this.gwUrl + '/backend_app_user/downLoadOrderExelByAppId?app_id=' + this.appId;
    // this.newTab(url);
    window.open(url);
    // window.location.href = this.gwUrl + '/backend_app_user/downLoadOrderExelByAppId?app_id=' + this.appId;
  }

  public getListApplication() {
    if (!this.currentUser || !this.currentUser.username) {
      return;
    }
    this.applicationService.getListPartnerByUsername(this.currentUser.username)
      .then((apps) => {
        if (apps) {
          if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.currentUser = currentUser;
            currentUser.apps = apps;
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
            this.menus = [];
            let i = 0;
            currentUser.apps.filter((it) => {
              this.menusAvatar.push('');
              this.menus.push(it);
              if (it.integrations) {
                it.integrations.filter((inter) => {
                  if (inter.picture) {
                    this.menusAvatar[i] = (inter.picture);
                  }
                });
              }
              i++;
            });
          }
        }
      });
  }

  public loadListApps() {
    if (localStorage.getItem('currentUser') && localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser.apps) {
        this.menus = [];
        let i = 0;
        currentUser.apps.filter((it) => {
          this.menusAvatar.push('');
          this.menus.push(it);
          if (it.integrations) {
            it.integrations.filter((inter) => {
              if (inter.picture) {
                this.menusAvatar[i] = (inter.picture);
              }
            });
          }
          i++;
        });
      }
    }
  }

  private usingApp(app) {
    // console_bk.log('APP: ' + JSON.stringify(app));
    // alert(JSON.stringify(app));
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      currentUser.app_using = app;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      localStorage.setItem('currentAgent_Id', 'NULL');
      this.router.navigate(['/' + this.currentUser.app_using.id + '/messages']);
    }
    /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
      .then((apps) => {
        let callbackUrl = this.configService.HOST_MY_WEB + '/#' + this.location.path();
        if (this.location.path().indexOf('connect-integration') > -1) {
          callbackUrl = this.configService.HOST_MY_WEB + '/#/chat';
        }
        this.newTab(this.configService.consoleHana + '/authen/' + app.id + '/' + apps.token + '/' + btoa(callbackUrl));
      }); 
      this.router.navigate(['/redirect-app']); */
      
  }

  private newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb' + url);
    let form = document.createElement('form');
    form.method = 'GET';
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }
}
