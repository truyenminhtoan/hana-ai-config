import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ROUTES, PAGE_MENU } from '../../sidebar/sidebar-routes.config';
import { MenuType } from '../../sidebar/sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ConfigService } from '../../../_services/config.service';
declare var gapi: any;
declare var $: any;
@Component({
  selector: 'guide-cpm',
  templateUrl: 'guide.component.html',
  styleUrls: ['guide.component.css']
})
export class GuideComponent {
  private listTitles: any[];
  private location: Location;
  // private auth2: any;
  private currentUser: any;
  // tslint:disable-next-line:variable-name
  private title_page: string;
  private consoleHana;
  private appName: string;
  private avatarUrl: string;
  private pageUrl: string;

  constructor(location: Location, private router: Router, private activatedRoute: ActivatedRoute, private configService: ConfigService) {
    this.location = location;
    router.events.subscribe((url: any) => url);
  }
}
