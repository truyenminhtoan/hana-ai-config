import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {ActionsService} from "../../../_services/actions.service";
import {NotificationService} from "../../../_services/notification.service";
import {Observable} from "rxjs/Rx";
import {ActionModel} from "../../../_models/actions.model";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
declare var $: any;

@Component({
    selector: 'action-delete',
    templateUrl: 'action-delete.component.html',
    styleUrls: ['action-delete.component.scss']
})
export class ActionDeleteComponent implements OnInit {
    private lists: ActionModel[] = [];
    private eventSubscription: Subscription;

    @ViewChild('cancel_action') cancel_action: ElementRef;

    constructor(
        private actionsService: ActionsService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService) {
    }
    ngOnInit() {
        this.loadListActions();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-actions') {
                    this.loadListActions();
                }
            }
        );
    }

    public loadListActions() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            let actionOperate: Observable<any>;
            actionOperate = this.actionsService.getListActionByAgentId(agentId);
            actionOperate.subscribe(
                (group_actions) => {
                    if (group_actions !== null) {
                        this.lists = [];
                        for (var i = 0; i< group_actions.length; i++) {
                            if (group_actions[i].data != undefined) {

                                for (var j = 0; j< group_actions[i].data.length; j++) {
                                    this.lists.push(group_actions[i].data[j]);
                                }
                            }
                        }
                    } else {
                        this.lists = [];
                    }
                },
                (err) => {
                });
        } else {
            this.notificationService.showNoAgentId();
        }
        this.notificationService.clearConsole();
    }

    public deleteAciton(actionId) {
        let actionOperate: Observable<ActionModel>;
        actionOperate = this.actionsService.delete(actionId);
        actionOperate.subscribe(
            (data) => {
                this._componentService.eventPublisher$.next('load-actions');
                this.notificationService.showSuccess("Xóa hành động thành công");
            },
            (err) => {
                // load event fail
                this.notificationService.showWarning("Xóa hành động không thành công");
            });
        this.cancel_action.nativeElement.click();

    }

}
