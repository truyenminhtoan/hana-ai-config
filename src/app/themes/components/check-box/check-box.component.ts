import { Component, Input , Output , EventEmitter } from '@angular/core';

@Component({
    selector: 'check-box',
    templateUrl: 'check-box.component.html',
    styleUrls: ['check-box.component.scss']
})
export class CheckBoxComponent {
    @Input() value: string;
    @Input() data: any;

    @Output() valueChange = new EventEmitter();

    private is_check = false;

    public ngOnInit() {
        if (this.data.true === this.value) {
            this.is_check = true;
        }
    }

    public checkBoxChange(event) {
        if (event.target.checked) {
            this.value = this.data.true;
        } else {
            this.value = this.data.false;
        }
        this.valueChange.emit(this.value);
    }
}
