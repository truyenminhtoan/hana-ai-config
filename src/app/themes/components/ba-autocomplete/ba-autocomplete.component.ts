import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ba-autocomplete',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Output() output = new EventEmitter<any>();

  @Input() selectedItem: any;
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;
  private sl = ['khai niem 66'];

  constructor() {

    this.filteredStates = this.stateCtrl.valueChanges
        .startWith(null)
        .map((name) => this.filterStates(name));
  }

  public filterStates(val: string) {
    return val ? this.source.filter((s) => new RegExp(val, 'gi').test(s)) : this.sl;
  }

  public keyChange(event) {
    event.stopPropagation();
    this.output.emit(event.target.value);
  }

  public choiceData(data) {
    this.output.emit(data);
  }

  displayFn(value): string {
    return (value != '') ? 'khai niem 66' : '';
  }


}
