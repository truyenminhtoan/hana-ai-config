import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, ViewChildren, ElementRef} from '@angular/core';
import { NotificationService}  from "../../../_services/notification.service";
import { EntityService } from "../../../_services/entity.service";
import { ObjectMemoryService } from "../../../_services/object_memory.service";
import { IntentService } from "../../../_services/intents.service";
import { EventsService } from "../../../_services/events.service";
import {IntentModel, ContextModel} from "../../../_models/intents.model";
import { Observable } from "rxjs/Rx";
import {IntentFilterModel} from "../../../_models/intents_filter";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
import {StringFormat} from "../../../themes/validators/string.format";
declare var $: any;
@Component({
    selector: 'intent-delete',
    templateUrl: 'intent-delete.component.html',
    styleUrls: ['intent-delete.component.css']
})
export class IntentDeleteConponent implements AfterViewInit, OnInit{
    private id_loading = true;
    private list_filter = [];
    private list_filter_fix = [];
    private list_filter_save = [];
    private currentAgentId = '';
    private eventSubscription: Subscription;
    @ViewChild('list_intents') private list_intents: ElementRef;

    constructor(private intentService: IntentService,
                private notificationService: NotificationService,
                private _componentService: ComponentInteractionService) {

        this.loadIntentLists();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-intents') {
                    //this.loadIntentLists();
                }
            }
        );
        this.notificationService.clearConsole();
    }

    public loadIntentLists() {
        this.currentAgentId = localStorage.getItem('currentAgent_Id');
        if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
            this.loadList();
        } else {
            this.notificationService.showNoAgentId();
        }
    }

    ngOnInit() {
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-intents') {
                    this.loadList();
                }
            }
        );
    }

    public getListIntentFilterByAgentId(agentId: string, callback) {
        let actionOperate: Observable<IntentFilterModel[]>;
        actionOperate = this.intentService.getListIntentFilterByAgentId(agentId);
        // Subscribe to observable
        actionOperate.subscribe(
            (data) => {
                if (data) {
                    callback(data);
                } else {
                    callback([]);
                }
            },
            (err) => {
                callback(false);
            });
    }

    public loadList() {
        this.getListIntentFilterByAgentId(localStorage.getItem('currentAgent_Id'), (data) => {
            if (data != undefined && data.length>0) {
                // console.log('data', JSON.stringify(data));
                for (var i = 0; i < data.length; i++) {
                    data[i].name_full = data[i].name;
                    data[i].usersay_full = data[i].usersay;
                    if (data[i].group_context_in != null) {
                        data[i].group_context_in = data[i].group_context_in.replace(',', ', ');
                        //console.log('data[i].group_context_in', data[i].group_context_in.split(','));

                        var tmp_list_context = data[i].group_context_in.split(',');
                        if (tmp_list_context.length > 1) {
                            var string_context = '';
                            for (var k = 0; k < tmp_list_context.length - 1; k++) {
                                string_context += tmp_list_context[k].trim() + ', ';
                            }
                            string_context += tmp_list_context[tmp_list_context.length-1].trim();
                            data[i].group_context_in_full = string_context;
                        } else {
                            data[i].group_context_in_full = data[i].group_context_in;
                        }

                        data[i].group_context_in = data[i].group_context_in.slice(0, 20);
                        if ( data[i].group_context_in.length >= 20) {
                            data[i].group_context_in += '...';
                        }
                    }
                    if (data[i].usersay != null) {
                        data[i].usersay = data[i].usersay.slice(0, 20);
                        if ( data[i].usersay.length >= 20) {
                            data[i].usersay += '...';
                        }
                    }
                    if (data[i].name != null) {
                        data[i].name = data[i].name.slice(0, 20);
                        if ( data[i].name.length >= 20) {
                            data[i].name += '...';
                        }
                    }

                }
            }
            this.list_filter = data;
            this.list_filter_save = data;
            this.list_filter_fix = data;

            this.id_loading = false;
        });
    }

    public deleteIntentById(id) {
        this.intentService.deleteIntent(id)
            .then((data) => {
                //this.loadList();
                this._componentService.eventPublisher$.next('load-intents');
                this.notificationService.showSuccess('Xóa ý định "' +data[0].name+ '" thành công');
            });
        $('body').removeClass("modal-open");
    }

    public ngAfterViewInit() {
    }

}
