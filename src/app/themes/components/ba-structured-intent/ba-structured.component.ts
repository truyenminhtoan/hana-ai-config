import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { Block } from '../../../_models/blocks.model';
import { EmitterService } from "../../../_services/emitter.service";
import { FormControl } from "@angular/forms";
declare var swal: any;
declare var $: any;
import * as _ from 'underscore';
import { ClipboardService } from 'ng2-clipboard/ng2-clipboard';
import { IntentService } from '../../../_services/intents.service';
import { NotificationService } from '../../../_services/notification.service';
import { MdDialog } from '@angular/material';
import { NgUploaderOptions } from 'ngx-uploader';
@Component({
  selector: 'ba-structured-intent',
  templateUrl: 'ba-structured.component.html',
  styleUrls: ['ba-structured.component.scss']
})
export class BaStructuredIntentComponent implements OnInit {
  private width_value_list_card = 250;
  // danh sach cac phan hoi
  private responseMessage = [];
  // check show quick_reply
  private show_quick_reply = true;
  private is_load = false;
  @Input() private responseMessageInput: any;
  @Output() private responseMessageOutput: any = new EventEmitter<any>();
  @Output() private intent_contexts_outOutput: any = new EventEmitter<any>();
  @Input() private intent_contexts: any;
  @Output() private contexts_suggestionsOutPut: any = new EventEmitter<any>();
  private is_show_res_sug = false;
  private resTexts = [];
  private intent_contexts_out = [];
  private contexts_suggestions = [];
  @Input() private is_suggestion_predict;
  @Output() private is_suggestion_predictOut: any = new EventEmitter<any>();

  // tslint:disable-next-line:member-ordering
  constructor(private element: ElementRef, private notificationService: NotificationService) {
    EmitterService.get('SAVE_LIST_CARD').subscribe((data: any) => {
      this.save();
    });
    EmitterService.get('SAVE_WHEN_UPLOAD_IMAGE').subscribe((data: any) => {
      this.save();
    });

    EmitterService.get('LOAD_INTENT').subscribe((data: any) => {
      this.responseMessageInput = [];
      this.responseMessageInput = data;
      this.loadBuild();
      this.is_load = true;
    });

    //console.log('this.intent_contexts', this.intent_contexts);
    let that = this;
    setTimeout(() => {
      if (that.intent_contexts != undefined && that.intent_contexts.length > 0) {
        that.is_show_res_sug = true;
      }
    }, 500);
  }

  public ngOnInit(): void {
    if (this.intent_contexts != undefined && this.intent_contexts.length > 0) {
      this.is_show_res_sug = true;
    }
    if (this.responseMessage.length == 0) {
      this.responseMessage.push({ type: 1, message: { posibleTexts: [{content : ''}] } });
      this.is_load = true;
    }
  }

  public save() {
    this.responseMessageOutput.emit(this.responseMessage);
    this.intent_contexts_outOutput.emit(this.intent_contexts_out);
    this.contexts_suggestionsOutPut.emit(this.contexts_suggestions);
    this.is_suggestion_predictOut.emit(this.is_suggestion_predict);
  }


  public removeTextResItem(index, item) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.responseMessage[index].message.posibleTexts = this.responseMessage[index].message.posibleTexts.filter((row) => {
        return (row != item)
      });
      this.save();
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      console.log(dismiss);
    });
  }

  public removeResponseMessage(obj) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.responseMessage = this.responseMessage.filter((row) => {
        return row !== obj
      });
      this.checkShowQuickReply();
      this.save();
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      console.log(dismiss);
    });
  }

  public removeCardItem(index, card) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.responseMessage[index].message.attachment.payload.elements = this.responseMessage[index].message.attachment.payload.elements.filter((row) => {
        return (row != card)
      });
      // kiem tra xoa luon nhung  group nao khong con elements nao
      this.responseMessage = this.responseMessage.filter((row) => {
        return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
      });
      this.save();
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      console.log(dismiss);
    });
  }

  public addCard(index) {
    this.responseMessage[index].message.attachment.payload.elements.push({ image_url: '', title: '', subtitle: '', buttons: [{ type: 'postback', title: '', payload: '' }] });
    this.save();
  }

  public addResponseMessageType(typeMes: number) {
    switch (typeMes) {
      case 1:
        // Phan hoi text
        this.responseMessage.push({ type: typeMes, message: { posibleTexts: [{content : ''}] } });
        break;
      case 2:
        // Phan hoi card
        this.responseMessage.push({ type: typeMes, message: { attachment: {type: 'template', payload: {template_type: 'generic', elements: [{image_url: '', title: '', subtitle: '', buttons: [{ type: 'postback', title: '', payload: ''}]}]}} } });

        break;
      case 3:
        // Phan hoi hinh anh
        this.responseMessage.push({ type: typeMes, message: { attachment: {type: 'image', payload: {url:''}} } });
        break;
      case 4:
        // Phan hoi quick_replies
        //this.responseMessage.push({ type: typeMes, message: { quick_replies: [{content_type: 'text', title: '', payload: ''}], text : ''}});
        this.responseMessage.push({ type: typeMes, message: { quick_replies: [], text : ''}});
        this.setfocusQuickReply();
        break;
    }
    //sort phan hoi
    //- Quickreply lúc nào cũng dưới cùng
    //- Các loại phản hồi khác thì hiển thị theo thứ tự người dùng đã thêm text - image - card - text - ... - quickreply
    var tmp_responseMessage = [];
    for (var i = 0; i < this.responseMessage.length; i++) {
      if (this.responseMessage[i].type != 4) {
        tmp_responseMessage.push(this.responseMessage[i]);
      }
    }
    for (var i = 0; i < this.responseMessage.length; i++) {
      if (this.responseMessage[i].type == 4) {
        tmp_responseMessage.push(this.responseMessage[i]);
        break;
      }
    }
    this.responseMessage = tmp_responseMessage;
    this.checkShowQuickReply();
    this.save();
  }

  public setfocusQuickReply() {
    // set focus
    let that = this;
    setTimeout(function () {
      $(that.element.nativeElement).find('.res-text-quick-reply').focus();
    }, 100);
  }

  public focus(i) {
    // let that = this;
    // setTimeout(function () {
    //   var element = '.res-text-' + i;
    //   $(that.element.nativeElement).find(element).focus();
    // }, 500);
  }


  public addResponseTextInput(i) {
    this.responseMessage[i].message.posibleTexts.push({content : ''});
  }

  public checkShowQuickReply() {
    var check = false;
    if (this.responseMessage.length > 0) {
      for (var i = 0; i < this.responseMessage.length; i++) {
        if (this.responseMessage[i].type == 4) {
          this.show_quick_reply = false;
          check = true;
          break;
        }
      }
      if (!check) {
        this.show_quick_reply = true;
      }
    } else {
      this.show_quick_reply = true;
    }
  }

  public removeResponseTextInput(event, i, j) {
    if (this.responseMessage[i].message.posibleTexts.length > 1) {
      this.responseMessage[i].message.posibleTexts = this.responseMessage[i].message.posibleTexts.filter((row) => {
        return row !== this.responseMessage[i].message.posibleTexts[j]
      });
    }
  }

  public addResponseImagesInput(event, i) {
    this.responseMessage[i].message.attachment.payload.url = event.target.value;
    this.save();
  }

  public output() {
    // EmitterService.get(this.outputBlockId).emit(this.blocks);
  }

  public loadBuild() {
    this.responseMessage = [];
    if (this.responseMessageInput != undefined) {
      try {
        this.responseMessage = this.responseMessageInput;
      } catch (error) {
        this.responseMessage = [];
      }
    }
    if (this.responseMessage.length === 0) {
      this.responseMessage.push({ type: 1, message: { posibleTexts: [{content : ''}] } });
    }
    // else {
    //   for (var i = 0; i < this.responseMessage.length; i++) {
    //     if (this.responseMessage[i].type == 1) {
    //       var tmp = [];
    //       if (this.responseMessage[i].message != undefined && this.responseMessage[i].message.posibleTexts != undefined) {
    //         for (var k = 0; k < this.responseMessage[i].message.posibleTexts.length; k ++) {
    //           if (this.responseMessage[i].message.posibleTexts[k].content != undefined) {
    //             tmp.push({'content': this.responseMessage[i].message.posibleTexts[k].content});
    //           } else {
    //             tmp.push({'content': this.responseMessage[i].message.posibleTexts[k]});
    //           }
    //         }
    //       }
    //       this.responseMessage[i].message.posibleTexts = tmp;
    //     }
    //   }
    // }
    // kiem tra xoa luon nhung  group nao khong con elements nao
    this.responseMessage = this.responseMessage.filter((row) => {
      return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
    });
    this.checkShowQuickReply();
  }

  private validatorsQuickReply = [this.validateTaginputGoiY];
  public errorMessagesQuickReply = {
    'mess_err_lenght@': ' Maxlength gợi ý ràng buộc <= 20 ký tự'
  };

  public validateTaginputGoiY(control: FormControl) {
    if (control.value.trim().length > 20) {
      return {
        'mess_err_lenght@': true
      };
    }
    return null;
  }

  public onTextChange(event) {
    
  }

  public getStyle(n) {
    if (n == 0) {
      return "rgba(183, 28, 28, 0.39)";
    } else {
      return "";
    }
  }

  public deleteContentSug() {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.is_show_res_sug = false;
      this.intent_contexts = [];
      this.intent_contexts_out = [];
      this.contexts_suggestions = [];
      this.is_suggestion_predict = false;
      this.save();
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      console.log(dismiss);
    });
  }
  
  public addResponseSug() {
    this.is_show_res_sug = true;
    //intent_contexts
    this.save();
  }

}
