import { Component, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { ClipboardService } from 'ng2-clipboard/ng2-clipboard';

@Component({
  selector: 'json-dialog',
  templateUrl: 'json-dialog.component.html',
  styleUrls: ['json-dialog.component.scss']
})
export class JsonDialogComponent {
  constructor( @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef<JsonDialogComponent>) { }

  public copyToClipboard() {
    this.clipboard.copy(this.data.message);
    this.dialogRef.close('');
  }
}
