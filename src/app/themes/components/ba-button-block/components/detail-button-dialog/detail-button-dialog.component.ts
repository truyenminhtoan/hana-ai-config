import {
    Component,
    Inject
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
declare var $: any;
declare var swal: any;
import {NotificationService} from "../../../../../_services/notification.service";
import {Observable} from "rxjs/Observable";
import {BlocksService} from "../../../../../_services/blocks.service";
@Component({
    selector: 'detail-button-dialog',
    templateUrl: 'detail-button-dialog.component.html',
    styleUrls: ['detail-button-dialog.component.scss']
})
export class DetailButtonDialogComponent {

    constructor(private blocksService: BlocksService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < DetailButtonDialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    private dropdownList = [];
    private selectedItems = [];
    private dropdownListGroups = [];
    private selectedItemsGroup = [];
    private dropdownSettings = {};
    private button: any;
    private text_value = '';
    private url_value = '';
    private title_value = '';
    private button_data = {};

    /** Group */
    private dropdownListSequnces = [];
    private selectedItemsSequnces = [];

    /** Text */
    private selectedItemsGroupText = [];
    private selectedItemsSequncesText = [];

    /** Group Delete */
    private selectedItemsGroupDelete = [];
    private selectedItemsSequncesDelete = [];

    /** Text Delete */
    private selectedItemsGroupTextDelete = [];
    private selectedItemsSequncesTextDelete = [];

    onNoClick(): void {
        //this.dialogRef.close();
    }

    public okClick() {
        this.returnData();
        if (!this.checkButtonValidate(this.button_data)) {
            this.notificationService.showDanger("Button phải nhập tiêu đề và nội dung của 1 trong 3 dạng text/ blocks/ url");
        } else {
            if (!this.checkUrl(this.button_data)) {
                this.notificationService.showDanger("Giá trị url không hợp lệ");
            } else {
                this.dialogRef.close(this.button_data);
            }
        } 
    }

    public onFocusTag(event) {
        //$('.ng2-dropdown-menu').hide();
    }

    public onFocusTagLiBlock(event) {
        //$('.ng2-tag-input__text-input').click();
        //$('.ng2-tag-input__text-input').click();
        //$('.ng2-dropdown-menu').hide();
    }

    public checkUrl(button) {
        let check = true;
        if (button.type == 'web_url') {
            if (!this.isValidURL(button.url.trim())) {
                check = false;
            }
        }

        return check;
    }

    isValidURL(url){
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if(RegExp.test(url)){
            return true;
        }else{
            return false;
        }
    }


    public checkButtonValidate(button) {
        var check = true;
        if (button.title.trim().length== 0) {
            check = false;
            return check;
        }
        if (button.type == 'web_url') {
            if (button.url.trim().length == 0) {
                check = false;
            }
        } else {
            if (button.payload == undefined) {
                check = false;
            } else {
                let decodedString = atob(button.payload);
                decodedString = JSON.parse(decodedString);
                if (decodedString['postback_type'] == 'text') {
                    if (decodedString['content'].trim().length == 0) {
                        check = false;
                    }    
                } else {
                    if (decodedString['block_ids'].length == 0) {
                        check = false;
                    } 
                }
            }
        }

        return check;
    }

    ngOnInit() {
        var that = this;
        // tao data cho select box block
        if (this.data.blocks != undefined) {
            that.button = this.data.button;
            // dung cho edit
            if (that.button.button_type == 'url') {
                that.url_value = that.button.url;
                that.title_value = that.button.title;
            } else {
                if (this.button.payload != undefined && this.button.payload.trim().length == 0) {
                    this.button.button_type = 'text';
                } else {
                    let decodedString = Buffer.from(this.button.payload, 'base64').toString('utf8');
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        that.text_value = decodedString['content'];
                        that.title_value = that.button.title;
                        //group
                        if (this.data.selected_groups_text != undefined && this.data.selected_groups_text.length > 0) {
                            this.selectedItemsGroupText = this.data.selected_groups_text;
                        }
                        //sequence
                        if (this.data.selected_sequences_text != undefined && this.data.selected_sequences_text.length > 0) {
                            this.selectedItemsSequncesText = this.data.selected_sequences_text;
                        }
                        //group delete
                        if (this.data.selected_groups_text_delete != undefined && this.data.selected_groups_text_delete.length > 0) {
                            this.selectedItemsGroupTextDelete = this.data.selected_groups_text_delete;
                        }
                        //sequence delete
                        if (this.data.selected_sequences_text_delete != undefined && this.data.selected_sequences_text_delete.length > 0) {
                            this.selectedItemsSequncesTextDelete = this.data.selected_sequences_text_delete;
                        }
                    } else {
                        // block
                        that.title_value = decodedString['title'];
                        if (this.data.selected_blocks != undefined && this.data.selected_blocks.length > 0) {
                            this.selectedItems = this.data.selected_blocks;
                        }
                        //group 
                        if (this.data.selected_groups != undefined && this.data.selected_groups.length > 0) {
                            this.selectedItemsGroup = this.data.selected_groups;
                        }
                        //sequence 
                        if (this.data.selected_sequences != undefined && this.data.selected_sequences.length > 0) {
                            this.selectedItemsSequnces = this.data.selected_sequences;
                        }
                        //group delete
                        if (this.data.selected_groups_delete != undefined && this.data.selected_groups_delete.length > 0) {
                            this.selectedItemsGroupDelete = this.data.selected_groups_delete;
                        }
                        //sequence delete
                        if (this.data.selected_sequences_delete != undefined && this.data.selected_sequences_delete.length > 0) {
                            this.selectedItemsSequncesDelete = this.data.selected_sequences_delete;
                        }
                    }
                }
            }
            Object.keys(this.data.blocks).forEach(function(k) {
                that.dropdownList.push({
                    "id": that.data.blocks[k].id,
                    "name": that.data.blocks[k].name
                }, );
            });
            Object.keys(this.data.groups).forEach(function(k) {
                that.dropdownListGroups.push({
                    "id": that.data.groups[k].id,
                    "name": that.data.groups[k].name
                }, );
            });
            Object.keys(this.data.sequences).forEach(function(k) {
                that.dropdownListSequnces.push({
                    "id": that.data.sequences[k].id,
                    "name": that.data.sequences[k].name
                }, );
            });
        }
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select block name",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            maxHeight: 50
        };
    }


    onSelectedGroup(event) {
        this.returnData();
    }

    onSelectedSequence(event) {
        this.returnData();
    }

    onSelectedBlock(event) {
        this.returnData();
    }

    onItemSelect(item: any) {
        this.returnData();
    }
    OnItemDeSelect(item: any) {
        this.returnData();
    }
    onSelectAll(items: any) {
        this.returnData();
    }
    onDeSelectAll(items: any) {
        this.returnData();
    }

    updateTypeButton(type) {
        this.data.button.button_type = type;
        this.returnData();
    }

    onAddGroup(event) {
        let that = this;
        if (event.display.trim() == event.value.trim()) {
            swal({
                title: 'Thông báo',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display.trim()};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroup.length; i++) {
                                if (that.selectedItemsGroup[i].value == event.display) {
                                    that.selectedItemsGroup[i].value = result_add_group.id;
                                    that.selectedItemsGroup[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroup = this.selectedItemsGroup.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    onAddGroupText(event) {
        let that = this;
        if (event.display.trim() == event.value.trim()) {
            swal({
                title: 'Thông báo',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display.trim()};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroupText.length; i++) {
                                if (that.selectedItemsGroupText[i].value == event.display) {
                                    that.selectedItemsGroupText[i].value = result_add_group.id;
                                    that.selectedItemsGroupText[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroupText = this.selectedItemsGroupText.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    onAddSequence(event) {
        this.returnData();
    }

    returnData() {
        // trim data
        this.title_value = this.title_value.trim();
        this.url_value = this.url_value.trim();
        this.text_value = this.text_value.trim();
        let payload;
        if (this.data.button.button_type != undefined) {
            this.data.button.button_type = this.data.button.button_type;
        } else {
            this.data.button.button_type = 'text';
        }
        switch (this.data.button.button_type) {
            case 'text':
                let group_ids_text = _.pluck(this.selectedItemsGroupText, 'id');
                let sequence_ids_text = _.pluck(this.selectedItemsSequncesText, 'id');
                let group_ids_delete = _.pluck(this.selectedItemsGroupTextDelete, 'id');
                let sequence_ids_delete = _.pluck(this.selectedItemsSequncesTextDelete, 'id');
                let obj = JSON.stringify({
                    "title": this.title_value,
                    "postback_type": "text",
                    "content": this.text_value,
                    "group_ids": group_ids_text,
                    "sequence_ids": sequence_ids_text,
                    "remove_group_ids": group_ids_delete,
                    "remove_sequence_ids": sequence_ids_delete,
                });
                payload = new Buffer(obj).toString('base64');
                this.button_data = {
                    "type": "postback",
                    "title": this.title_value,
                    "payload": payload
                };
                break;
            case 'block':
                let block_ids = _.pluck(this.selectedItems, 'id');
                let group_ids = _.pluck(this.selectedItemsGroup, 'id');
                let sequence_ids = _.pluck(this.selectedItemsSequnces, 'id');
                let group_ids_delete_block = _.pluck(this.selectedItemsGroupDelete, 'id');
                let sequence_ids_delete_block = _.pluck(this.selectedItemsSequncesDelete, 'id');
                //console.log('sequence_ids_delete', sequence_ids_delete);
                payload = new Buffer(JSON.stringify({
                    "title": this.title_value,
                    "postback_type": "link_blocks",
                    "block_ids": block_ids,
                    "group_ids": group_ids,
                    "sequence_ids": sequence_ids,
                    "remove_group_ids": group_ids_delete_block,
                    "remove_sequence_ids": sequence_ids_delete_block,
                })).toString('base64');
                this.button_data = {
                    "type": "postback",
                    "title": this.title_value,
                    "payload": payload
                };
                break;
            case 'url':
                this.button_data = {
                    "type": "web_url",
                    "url": this.url_value,
                    "title": this.title_value
                };
                break;
        }
    }

    public getStyleText(n) {
        if (n == 0) {
            return "rgba(183, 28, 28, 0.39)";
        } else {
            return "";
        }
    }
}