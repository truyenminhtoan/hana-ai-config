import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import * as _ from 'underscore';
import { FormControl } from "@angular/forms";
declare var swal: any;
declare var $: any;
import { Observable } from 'rxjs/Rx';
import { MdDialog } from '@angular/material';
import {DetailButtonDialogComponent} from "./components/detail-button-dialog/detail-button-dialog.component";
import {UserModel} from "../../../_models/user.model";
import {BlocksService} from "../../../_services/blocks.service";
import {NotificationService} from "../../../_services/notification.service";
import {ButtonsModel} from "../../../_models/buttons.model";
import {EmitterService} from "../../../_services/emitter.service";

@Component({
    selector: 'ba-button-block',
    templateUrl: 'ba-button-block.component.html',
    styleUrls: ['ba-button-block.component.scss']
})
export class BaButtonBlockComponent {

    private currentUser: UserModel;
    @Input() button: any;
    @Input() input_list_buttons: any;
    @Input() block_cur: any;
    
    @Output() private outPutButton: any = new EventEmitter<any>();
    private list_blocks = [];
    private list_sequences = [];
    private list_groups = [];
    private button_add: any;

    constructor(private dialog: MdDialog,
                private blockService: BlocksService,
                private notificationService: NotificationService) {
    }

    public ngOnInit(): void {
        EmitterService.get('LOAD_LIST_BLOCKS').subscribe((data: any) => {
            this.getDataShowContentList();
        });
        var that = this;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let actionOperate: Observable<any>;
        actionOperate = this.blockService.getListGroupBlocks(this.currentUser.partner_id);
        actionOperate.subscribe(
            (data_groups) => {
                if (data_groups != undefined && data_groups.length > 0) {
                    that.list_blocks = [];
                    //list_blocks
                    Object.keys(data_groups).forEach(function(k) {
                        Object.keys(data_groups[k].blocks).forEach(function(i) {
                            that.list_blocks.push(data_groups[k].blocks[i]);
                        });
                    });
                }
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let actionOperateGroup: Observable<any>;
                actionOperateGroup = this.blockService.listGroupByApp(app_id);
                actionOperateGroup.subscribe(
                    (data_groups) => {
                        if (data_groups != undefined && data_groups.length > 0) {
                            this.list_groups = data_groups;
                        } else {
                            this.list_groups = [];
                        }

                        let actionOperateSequence: Observable<any>;
                        actionOperateSequence = this.blockService.listSequencesByApp(app_id);
                        actionOperateSequence.subscribe(
                            (sequences) => {
                                if (sequences != undefined && sequences.length > 0) {
                                    this.list_sequences = sequences;
                                } else {
                                    this.list_sequences = [];
                                }

                            });
                    });


                that.getDataShowContentList();
            },
            (err) => {
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh block");
            });
    }

    public getDataShowContentList() {
        var that = this;
        if (this.input_list_buttons != undefined && this.input_list_buttons.length > 0) {
            Object.keys(this.input_list_buttons).forEach(function(k) {
                if (that.input_list_buttons[k].type != undefined && that.input_list_buttons[k].type != 'web_url') {
                    let decodedString = Buffer.from(that.input_list_buttons[k].payload, 'base64').toString('utf8');
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        that.input_list_buttons[k].value = decodedString['content'];
                    } else {
                        let list_block_names = [];
                        if (decodedString['block_ids'] != undefined && decodedString['block_ids'].length > 0) {
                            for (let z = 0; z < decodedString['block_ids'].length; z++) {
                                for (let i = 0; i < that.list_blocks.length; i++) {
                                    if (that.list_blocks[i].id == decodedString['block_ids'][z]) {
                                        list_block_names.push(that.list_blocks[i].name);
                                        break;
                                    }
                                }
                            }
                            if (list_block_names.length > 0) {
                                that.input_list_buttons[k].value = list_block_names.join(", ");
                            } else {
                                that.input_list_buttons[k].value = '';
                            }
                        } else {
                            that.input_list_buttons[k].value = '';
                        }

                    }
                }
            });
        }
    }

    showAddButton() {
        var that = this;
        if (that.button_add == undefined) {
            that.button_add = new ButtonsModel();
            that.button_add.type = 'postback';
            that.button_add.title = '';
            that.button_add.payload = '';
            // tu dinh nghia
            that.button_add.button_type = 'text';
        }

        that.button_add.title = '';
        that.button_add.payload = '';
        // tu dinh nghia
        that.button_add.button_type = 'text';
        let dialogRef = this.dialog.open(DetailButtonDialogComponent, {
            width: '50%',
            data: {
                'blocks': this.list_blocks,
                'groups' : this.list_groups,
                'sequences': this.list_sequences,
                'button': this.button_add
            }
        });
        dialogRef.afterClosed().subscribe(button_add => {
            if (button_add) {
                if (this.checkButtonValidate(button_add) == true) {
                    if (this.input_list_buttons == undefined) {
                        this.input_list_buttons = [];
                    }
                    this.input_list_buttons.push(button_add);
                    this.clearButton();
                    this.outputBlock(this.input_list_buttons);
                    EmitterService.get('SAVE_BUTTON_BLOCK').emit(this.input_list_buttons);
                    EmitterService.get('SELECT_BLOCK').emit(this.block_cur);
                } else {
                    this.notificationService.showDanger("Button phải nhập tiêu đề và nội dung của 1 trong 3 dạng text/ blocks/ url");
                }
            }
        });
    }

    public clearButton() {
        var that = this;
        this.input_list_buttons = this.input_list_buttons.filter((item) => {
            return that.checkButtonValidate(item);
        });
    }

    public checkButtonValidate(button) {
        var check = true;
        if (button.title.trim().length== 0) {
            check = false;
            return check;
        }
        if (button.type == 'web_url') {
            if (button.url.trim().length == 0) {
                check = false;
            }
        } else {
            if (button.payload == undefined) {
                check = false;
            } else {
                let decodedString = atob(button.payload);
                decodedString = JSON.parse(decodedString);
                if (decodedString['postback_type'] == 'text') {
                    if (decodedString['content'].trim().length == 0) {
                        check = false;
                    }    
                } else {
                    if (decodedString['block_ids'].length == 0) {
                        check = false;
                    } 
                }
            }
        }

        return check;
    }

    public showEditButton(button) {
        this.button = button;
        if (this.button.type == 'web_url') {
            // tu dinh nghia
            this.button.button_type = 'url';
        } else {
            let decodedString = JSON.parse(atob(this.button.payload));
            if (decodedString['postback_type'] == 'text') {
                this.button.button_type = 'text';
            } else {
                this.button.button_type = 'block';
            }
        }
        var that = this;
        if (that.button == undefined) {
            that.button = new ButtonsModel();
            that.button.type = 'postback';
            that.button.title = '';
            that.button.payload = '';
            // tu dinh nghia
            that.button.button_type = 'text';
        }
        // get danh sach blocSelected
        let blocSelected = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'link_blocks') {

                for (let z = 0; z < decodedString['block_ids'].length; z++) {
                    for (let i = 0; i < that.list_blocks.length; i++) {
                        if (that.list_blocks[i].id == decodedString['block_ids'][z]) {
                            that.list_blocks[i].value = that.list_blocks[i].id;
                            that.list_blocks[i].display = that.list_blocks[i].name;
                            blocSelected.push(that.list_blocks[i]);
                            break;
                        }
                    }
                }
            }
        }
        // get danh sach groupSelected
        let groupSelected = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'link_blocks') {
                if (decodedString['group_ids'] != undefined) {
                    for (let z = 0; z < decodedString['group_ids'].length; z++) {
                        for (let i = 0; i < that.list_groups.length; i++) {
                            if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                that.list_groups[i].value = that.list_groups[i].id;
                                that.list_groups[i].display = that.list_groups[i].name;
                                groupSelected.push(that.list_groups[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
        // get danh sach sequenceSelected
        let sequenceSelected = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'link_blocks') {
                if (decodedString['sequence_ids'] != undefined) {
                    for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                        for (let i = 0; i < that.list_sequences.length; i++) {
                            if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                that.list_sequences[i].value = that.list_sequences[i].id;
                                that.list_sequences[i].display = that.list_sequences[i].name;
                                sequenceSelected.push(that.list_sequences[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
        // get danh sach groupSelected
        let groupSelectedDelete = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'link_blocks') {
                if (decodedString['remove_group_ids'] != undefined) {
                    for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                        for (let i = 0; i < that.list_groups.length; i++) {
                            if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                that.list_groups[i].value = that.list_groups[i].id;
                                that.list_groups[i].display = that.list_groups[i].name;
                                groupSelectedDelete.push(that.list_groups[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
        // get danh sach sequenceSelected
        let sequenceSelectedDelete = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            console.log('decodedString', decodedString);
            if (decodedString['postback_type'] == 'link_blocks') {
                if (decodedString['remove_sequence_ids'] != undefined) {
                    for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                        for (let i = 0; i < that.list_sequences.length; i++) {
                            if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                that.list_sequences[i].value = that.list_sequences[i].id;
                                that.list_sequences[i].display = that.list_sequences[i].name;
                                sequenceSelectedDelete.push(that.list_sequences[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }


        // get danh sach groupSelected
        let groupSelectedText = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'text') {
                if (decodedString['group_ids'] != undefined) {
                    for (let z = 0; z < decodedString['group_ids'].length; z++) {
                        for (let i = 0; i < that.list_groups.length; i++) {
                            if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                that.list_groups[i].value = that.list_groups[i].id;
                                that.list_groups[i].display = that.list_groups[i].name;
                                groupSelectedText.push(that.list_groups[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
        // get danh sach sequenceSelected
        let sequenceSelectedText = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'text') {
                if (decodedString['sequence_ids'] != undefined) {
                    for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                        for (let i = 0; i < that.list_sequences.length; i++) {
                            if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                that.list_sequences[i].value = that.list_sequences[i].id;
                                that.list_sequences[i].display = that.list_sequences[i].name;
                                sequenceSelectedText.push(that.list_sequences[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        let groupSelectedTextDelete = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'text') {
                if (decodedString['remove_group_ids'] != undefined) {
                    for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                        for (let i = 0; i < that.list_groups.length; i++) {
                            if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                that.list_groups[i].value = that.list_groups[i].id;
                                that.list_groups[i].display = that.list_groups[i].name;
                                groupSelectedTextDelete.push(that.list_groups[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
        // get danh sach sequenceSelected
        let sequenceSelectedTextDelete = [];
        if (that.button.payload != undefined && that.button.payload.trim().length > 0) {
            let decodedString = atob(that.button.payload);
            decodedString = JSON.parse(decodedString);
            if (decodedString['postback_type'] == 'text') {
                if (decodedString['remove_sequence_ids'] != undefined) {
                    for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                        for (let i = 0; i < that.list_sequences.length; i++) {
                            if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                that.list_sequences[i].value = that.list_sequences[i].id;
                                that.list_sequences[i].display = that.list_sequences[i].name;
                                sequenceSelectedTextDelete.push(that.list_sequences[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        let dialogRef = this.dialog.open(DetailButtonDialogComponent, {
            width: '50%',
            data: {
                'blocks': that.list_blocks,
                'groups' : this.list_groups,
                'sequences': this.list_sequences,
                'button': that.button,
                //block
                'selected_blocks': blocSelected,
                'selected_groups': groupSelected,
                'selected_sequences': sequenceSelected,
                'selected_groups_delete': groupSelectedDelete,
                'selected_sequences_delete': sequenceSelectedDelete,
                // text
                'selected_groups_text': groupSelectedText,
                'selected_sequences_text': sequenceSelectedText,
                'selected_groups_text_delete': groupSelectedTextDelete,
                'selected_sequences_text_delete': sequenceSelectedTextDelete,
            }
        });
        dialogRef.afterClosed().subscribe(button_add => {
            if (button_add) {
                if (this.checkButtonValidate(button_add) == true) {
                    //outPutButton
                    if (this.input_list_buttons == undefined || (this.input_list_buttons != undefined && this.input_list_buttons.length == 1 &&  this.input_list_buttons[0].title.trim().length == 0)) {
                        this.input_list_buttons = [];
                    }
                    for (var i = 0; i < this.input_list_buttons.length; i++) {
                        if (this.input_list_buttons[i] == button) {
                            this.input_list_buttons[i] = button_add;
                        }
                    }
                    this.clearButton();
                    this.outputBlock(this.input_list_buttons);
                    EmitterService.get('SAVE_BUTTON_BLOCK').emit(this.input_list_buttons);
                    EmitterService.get('SELECT_BLOCK').emit(this.block_cur);
                } else {
                    this.notificationService.showDanger("Button phải nhập tiêu đề và nội dung của 1 trong 3 dạng text/ blocks/ url");
                }
            }
        });
    }

    public outputBlock(body) {
        this.outPutButton.emit(body);
    }

    public save() {
        this.outPutButton.emit(this.input_list_buttons);
        EmitterService.get('SAVE_BUTTON_BLOCK').emit(this.input_list_buttons);
    }

    public deleteButton(button) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.input_list_buttons = this.input_list_buttons.filter((row) => {
                return (row != button)
            });
            this.outputBlock(this.input_list_buttons);
            EmitterService.get('DELETE_BUTTON').emit(button);


            //EmitterService.get('SAVE_BUTTON_BLOCK').emit(this.input_list_buttons);
            //this.save();
        }, (dismiss) => {
            //console.log(dismiss);
        });
    }
}
