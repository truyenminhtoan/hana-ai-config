
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ProductsService} from "../../../_services/products.service";
import {Inputs, Meta} from "../../../_models/actions.model";
import {EntityService} from "../../../_services/entity.service";

@Component({
  selector: 'ba-autocomplete-action',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteActionComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Input() entities: any;
  @Output() output = new EventEmitter<any>();
  @Output() is_show = new EventEmitter<any>();
  
  @Input() products: ProductsModel[] = [];
  @Output() product_id = new EventEmitter<any>();

  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;
  private name_text = '';

  constructor(private productService: ProductsService, private entityService: EntityService) {
  }

  public filterStates(val: string) {
    return val ? this.source.filter((s) => new RegExp(val, 'gi').test(s)) : this.source;
  }

  public keyChange(event) {
    event.stopPropagation();
    this.output.emit(event.target.value);
  }

  public choiceData(data) {
    if (this.products.length > 0) {
      for (var i =0; i< this.products.length; i++) {
        if (this.products[i].name == data) {
          this.product_id.emit(this.products[i].id);
          this.getThongTinTieuChiHanhDong(this.products[i].id);
        }
      }
    }
  }

  public getThongTinTieuChiHanhDong(productId) {
    this.productService.getPromiseProductById(productId)
        .then((product) => {
          let input_actions: Inputs[] = [];
          if (product.product_criterias.length > 0) {
            for (var i = 0; i < product.product_criterias.length; i++) {

              var tmp_input = new Inputs();
              tmp_input.param = product.product_criterias[i].name;
              tmp_input.defaultValue = 'NULL';
              tmp_input.param = product.product_criterias[i].name;

              let tmp_meta: Meta[] = [];

              var meta_search_type = new Meta();
              meta_search_type.key = 'searchType';
              meta_search_type.value = 'proximate';
              tmp_meta.push(meta_search_type);

              var meta_required = new Meta();
              meta_required.key = 'required';
              meta_required.value = false;
              tmp_meta.push(meta_required);

              var meta_weight = new Meta();
              meta_weight.key = 'weight';
              meta_weight.value = '1';
              tmp_meta.push(meta_weight);

              var meta_entity = new Meta();
              meta_entity.key = 'entity_info';
              meta_entity.value = product.product_criterias[i].entity_id;
              tmp_meta.push(meta_entity);

              tmp_input.meta = tmp_meta;
              input_actions.push(tmp_input);

            }

            this.emitTer(input_actions);
          } else {
            this.emitTer([]);
          }

        });
  }

  public emitTer(inputs) {
      for (var i =0 ; i < inputs.length; i++) {
        for (var j = 0; j < inputs[i].meta.length; j++) {
          if (inputs[i].meta[j].key == 'entity_info') {
            inputs[i].meta[j].value = this.searchEntity(inputs[i].meta[j].value);
          }
        }
      }
      if (inputs.length > 0) {
        this.output.emit(inputs);
        this.is_show.emit(true);
      } else {
        this.is_show.emit(false);
      }

  }

  public searchEntity(id) {
    for (var i = 0; i < this.entities.length; i++) {
      if (this.entities[i].id == id) {
        return this.entities[i].name;
      }
    }
    return '--';
  }


}

