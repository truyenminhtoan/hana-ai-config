import { NotificationService } from './../../../../../_services/notification.service';
import { Observable } from 'rxjs/Rx';
import { IntentService } from './../../../../../_services/intents.service';
import { Content } from './../../../../../_models/blocks.model';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Component, Inject } from '@angular/core';
import * as _ from 'underscore';
@Component({
    selector: 'ba-response-edit',
    templateUrl: 'ba-response-edit.component.html',
    styleUrls: ['ba-response-edit.component.scss']
})
export class BaResponseEditComponent {
    private currentIntent;
    private content;
    private contentOld;
    private responseMessage;
    private isProcess = false;
    constructor( @Inject(MD_DIALOG_DATA) private data: any, private intentService: IntentService, private dialogRef: MdDialogRef<BaResponseEditComponent>, private notificationService: NotificationService) {
        this.currentIntent = data.currentIntent;
        this.content = data.content;
        this.contentOld = data.content;

        if (_.size(this.currentIntent.response_messages) > 0) {
            let obj = JSON.parse(this.currentIntent.response_messages[0].content);
            this.responseMessage = obj[0].message.posibleTexts;
        }
    }

    public updateResponse() {
        if (!this.content || this.content.trim().length === 0) {
            this.notificationService.showDanger('Vui lòng nhập nội dung phản hồi cho khách hàng');
            return;
        }
        this.isProcess = true;
        let intent: any = {};
        let mes: any = {};
        intent.id = this.currentIntent.id;
        intent.agent_id = this.currentIntent.agent_id;
        intent.name = this.currentIntent.name;

        if (this.currentIntent.response_messages) {
            intent.response_messages = this.currentIntent.response_messages[0];
        }
        if (!intent.response_messages) {
            intent.response_messages = { content: '' };
        }
        let i = 0;
        if (!this.responseMessage) {
            this.responseMessage = [];
        }
        this.responseMessage.filter((res) => {
            if (res === this.contentOld) {
                this.responseMessage[i] = this.content;
            }
            this.responseMessage[i] = this.responseMessage[i].trim();
            i++;
        });
        mes.posibleTexts = this.responseMessage;
        intent.response_messages.content = JSON.stringify([{ message: mes }]);
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFAQ(intent);
        actionOperate.subscribe(
            (usay) => {
                this.isProcess = false;
                this.dialogRef.close('');
            },
            (err) => {
                this.isProcess = true;
                this.notificationService.showDanger('Cập nhật thất bại');
                console.log(err);
            });
    }
}
