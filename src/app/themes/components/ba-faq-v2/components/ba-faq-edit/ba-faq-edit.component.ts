import { NotificationService } from './../../../../../_services/notification.service';
import { Observable } from 'rxjs/Rx';
import { IntentService } from './../../../../../_services/intents.service';
import { Content } from './../../../../../_models/blocks.model';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Component, Inject } from '@angular/core';

@Component({
    selector: 'ba-faq-edit',
    templateUrl: 'ba-faq-edit.component.html',
    styleUrls: ['ba-faq-edit.component.scss']
})
export class BaFaqEditComponent {
    private id;
    private content;
    private isProcess = false;
    constructor( @Inject(MD_DIALOG_DATA) private data: any, private notificationService: NotificationService, private intentService: IntentService, private dialogRef: MdDialogRef<BaFaqEditComponent>) {
        this.id = data.id;
        this.content = data.content;
    }

    public updateUsersay() {
        if (!this.content || this.content.trim().length === 0) {
            this.notificationService.showDanger('Vui lòng nhập câu nói của khách hàng');
            return;
        }
        this.isProcess = true;
        this.content = this.content.trim();
        let body = { id: this.id, content: this.content };
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateUsersay(body);
        actionOperate.subscribe(
            (usay) => {
                this.isProcess = false;
                this.dialogRef.close(body);
            },
            (err) => {
                console.log(err);
                this.notificationService.showDanger('Cập nhật thất bại');
            });
    }
}
