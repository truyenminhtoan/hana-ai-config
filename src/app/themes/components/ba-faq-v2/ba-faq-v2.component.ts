import { Log } from './../../../_services/log.service';
import { BaResponseEditComponent } from './components/ba-response-edit/ba-response-edit.component';
import { BaFaqComponent } from './../ba-faq/ba-faq.component';
import { MdDialog } from '@angular/material';
import { Component, Input, OnInit, EventEmitter, Output, HostListener, SimpleChanges } from '@angular/core';
import * as _ from 'underscore';
declare var Taggle: any;
declare var $: any;
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs/Observable';
import { TagModel } from 'ng2-tag-input/dist/modules/core';
import { IntentService } from '../../../_services/intents.service';
import { NotificationService } from '../../../_services/notification.service';
import { BaFaqEditComponent } from './components/ba-faq-edit/ba-faq-edit.component';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
@Component({
  selector: 'ba-faq-v2',
  templateUrl: 'ba-faq-v2.component.html',
  styleUrls: ['ba-faq-v2.component.scss']
})
export class BaFaqV2Component implements OnInit, OnChanges {
  @Input() private index: any;
  @Input() private typeFaq: any;
  @Input() private currentIntent: any = {};
  @Input() private blocks: any;
  @Input() private responseBlocks: any[];
  @Output() private outputDelete = new EventEmitter<any>();
  private isToInbox = false;
  private textToInbox = '';
  private responseTextType = true;

  private inputText;

  // usersay
  private usersaysTemp: any = [];

  // response
  private responseMessage = [];

  // block
  private blockSelected: any[] = [];

  constructor(private log: Log, private dialog: MdDialog, private intentService: IntentService, private notificationService: NotificationService) { }

  public init() {
    this.textToInbox = '';
    this.isToInbox = false;
    this.blockSelected = [];
    this.responseMessage = [];
  }
  // tslint:disable-next-line:member-access
  public ngOnInit(): void {
    try {
      this.init();
      if (!this.currentIntent) {
        return;
      }
      if (this.currentIntent.inner_action && this.currentIntent.inner_action === 'createInboxFromComment') {
        //  alert(JSON.stringify(this.currentIntent));
        this.isToInbox = true;
      }
      // if (this.currentIntent.intent_entities && this.currentIntent.intent_entities[0]) {
      //   //  alert(JSON.stringify(this.currentIntent));
      //   this.textToInbox = this.currentIntent.intent_entities[0].default_value;
      //   // alert(this.textToInbox);
      // }

      if (this.currentIntent.intent_entities) {
        //  alert(JSON.stringify(this.currentIntent));
        this.currentIntent.intent_entities.filter((item) => {
          if (item.param === 'noi_dung_inbox') {
            this.textToInbox = item.default_value;
          }
        });
        // alert(this.textToInbox);
      }

      if (this.currentIntent.type_answer) {
        this.responseTextType = this.currentIntent.type_answer === 'text' ? true : false;
      }

      if (this.currentIntent.usersays) {
        this.usersaysTemp = _.pluck(this.currentIntent.usersays, 'content');
      }
      // this.usersaysTemp = this.usersaysTemp.filter((item) => !item);
      this.usersaysTemp = _.reject(this.usersaysTemp, (item) => {
        return !item;
      });
      if (_.size(this.currentIntent.response_messages) > 0) {
        try {
          let obj = JSON.parse(this.currentIntent.response_messages[0].content);
          if (obj.length > 0 && obj[0].message && obj[0].message.posibleTexts) {
            this.responseMessage = obj[0].message.posibleTexts;
          } else {
            this.responseMessage = [];
          }
        } catch (error) {
          this.responseMessage = [];
        }
      }
      // this.log.info('current intent_id:' + this.currentIntent.id + '=>' + JSON.stringify(this.currentIntent));
      if (this.currentIntent.response_message_blocks) {
        // console_bk.log('block:', JSON.stringify(this.currentIntent.response_message_blocks));
        // this.responseTextType = false;
        this.currentIntent.response_message_blocks.filter((item) => {
          let bl = this.blocks.filter((it) => it.value === item.block_id)[0];
          // alert(JSON.stringify(bl));
          if (bl) {
            this.blockSelected.push(bl);
          }
        });
        // this.log.info('intent_id:' + this.currentIntent.id + '=>' + JSON.stringify(this.blockSelected));
      }
      $('.modal-hana').hide();
      $('.material-icons').click((event) => {
        $('.modal-hana').hide();
        $(event.target).closest('div.flex-1').find('div.tut-payment.modal-hana').show();
      });

      $('body').click((event) => {
        if (!$(event.target).is('.material-icons')) {
          $('.modal-hana').hide();
        }
      });
      // alert(JSON.stringify(this.blockSelected));
      // tslint:disable-next-line:no-empty
    } catch (error) {

    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    try {
      this.init();
      if (!this.currentIntent) {
        return;
      }
      if (this.currentIntent.inner_action && this.currentIntent.inner_action === 'createInboxFromComment') {
        //  alert(JSON.stringify(this.currentIntent));
        this.isToInbox = true;
      }
      if (this.currentIntent.intent_entities) {
        //  alert(JSON.stringify(this.currentIntent));
        this.currentIntent.intent_entities.filter((item) => {
          if (item.param === 'noi_dung_inbox') {
            this.textToInbox = item.default_value;
          }
        });
        // alert(this.textToInbox);
      }
      if (this.currentIntent.type_answer) {
        this.responseTextType = this.currentIntent.type_answer === 'text' ? true : false;
      }

      if (this.currentIntent.usersays) {
        this.usersaysTemp = _.pluck(this.currentIntent.usersays, 'content');
      }

      if (_.size(this.currentIntent.response_messages) > 0) {
        try {
          let obj = JSON.parse(this.currentIntent.response_messages[0].content);
          if (obj.length > 0 && obj[0].message && obj[0].message.posibleTexts) {
            this.responseMessage = obj[0].message.posibleTexts;
          } else {
            this.responseMessage = [];
          }
        } catch (error) {
          this.responseMessage = [];
        }
      }
      // if (this.currentIntent.id === '54279706-0e84-7e01-4e3c-5acaac48914e') {
      //   alert(JSON.stringify(this.blockSelected));
      // }
      // this.log.info('current intent_id:' + this.currentIntent.id + '=>' + JSON.stringify(this.currentIntent));
      if (this.currentIntent.response_message_blocks) {
        this.blockSelected = [];
        // this.responseTextType = false;
        // console_bk.log('block:', JSON.stringify(this.currentIntent.response_message_blocks));
        this.currentIntent.response_message_blocks.filter((item) => {
          let bl = this.blocks.filter((it) => it.value === item.block_id)[0];
          // alert(JSON.stringify(bl));
          if (bl) {
            this.blockSelected.push(bl);
          }
        });
        // if (this.currentIntent.id === '54279706-0e84-7e01-4e3c-5acaac48914e') {
        //   // alert(JSON.stringify(this.blocks));
        //   this.log.info('blocks:' + this.currentIntent.id + '=>' + JSON.stringify(this.blocks));
        //   this.log.info('blockSelected:' + this.currentIntent.id + '=>' + JSON.stringify(this.blockSelected));
        // }
        // this.log.info('intent_id:' + this.currentIntent.id + '=>' + JSON.stringify(this.blockSelected));
      }
      $('.modal-hana').hide();
      $('.material-icons').click((event) => {
        $('.modal-hana').hide();
        $(event.target).closest('div.flex-1').find('div.tut-payment.modal-hana').show();
      });

      $('body').click((event) => {
        if (!$(event.target).is('.material-icons')) {
          $('.modal-hana').hide();
        }
      });
      // alert(JSON.stringify(this.blockSelected));
      // tslint:disable-next-line:no-empty
    } catch (error) {

    }
  }

  public changeSwitch(event) {
    if (event.checked) {
      this.currentIntent.inner_action = 'createInboxFromComment';
    } else {
      this.currentIntent.inner_action = null;
    }
    this.updateIntentToBlock();
  }

  public updateMachineLearning(event) {
    let intent: any = {};
    intent.id = this.currentIntent.id;
    intent.is_machine_learning = event.checked;
    intent.agent_id = this.currentIntent.agent_id;

    if (this.usersaysTemp && this.usersaysTemp.length > 0) {
      intent.name = this.usersaysTemp[0];
      if (intent.name.length > 200) {
        intent.name = intent.name.substr(0, 200);
      }
    }

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(intent);
    actionOperate.subscribe(
      (it) => {
        // // console_bk.log('usersay', usay);
        // this.responseTextType = !this.responseTextType;
        // this.currentIntent.is_machine_learning = intent.is_machine_learning;
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public onAfterAddUsersay(tag) {
    this.log.info('intent:' + JSON.stringify(this.currentIntent));
    let usersayInsert: any = { id: UUID.UUID(), intent_id: this.currentIntent.id, content: tag.display, not_update_mongo: true };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addUsersayIntoIntent(usersayInsert);
    actionOperate.subscribe(
      (usay) => {
        if (!this.currentIntent.usersays) {
          this.currentIntent.usersays = [];
        }
        this.currentIntent.usersays.push(usay);
        let ids = _.pluck(this.currentIntent.usersays, 'id');
        this.updateFirstUsersay(ids);
        this.updateResponse();
      },
      (err) => {
        this.notificationService.showDanger('Có lỗi xảy ra khi thêm câu nói khách hàng');
        this.usersaysTemp = _.reject(this.usersaysTemp, (num) => { return num === tag.display; });
        // console_bk.log(err);
      });
  }

  public onRemoveUsersay(event) {
    let says = this.currentIntent.usersays.filter((item) => item.content === event)[0];
    if (says) {
      let actionOperate: Observable<any>;
      actionOperate = this.intentService.removeUsersayFromIntent(says.id);
      actionOperate.subscribe(
        (usay) => {
          this.currentIntent.usersays = _.reject(this.currentIntent.usersays, (num) => { return num.id === says.id; });
          // alert('remove');
          let ids = _.pluck(this.currentIntent.usersays, 'id');
          this.updateFirstUsersay(ids);
          this.updateResponse();
        },
        (err) => {
          // console_bk.log(err);
        });
    }
  }

  // response text
  public updateResponse() {
    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.name = this.currentIntent.name;
    // Lay usersay dau tien lam ten intent
    if (this.usersaysTemp && this.usersaysTemp.length > 0) {
      intent.name = this.usersaysTemp[0];
      if (intent.name.length > 200) {
        intent.name = intent.name.substr(0, 200);
      }
    }
    intent.type_answer = this.responseTextType ? 'text' : 'block';
    mes.posibleTexts = this.responseMessage;

    if (this.currentIntent.response_messages) {
      intent.response_messages = this.currentIntent.response_messages[0];
    }
    if (!intent.response_messages) {
      intent.response_messages = { content: '' };
    }
    intent.response_messages.content = JSON.stringify([{ message: mes }]);
    // update vao view
    this.currentIntent.response_messages = [];
    this.currentIntent.response_messages.push({ content: JSON.stringify([{ message: mes }]) });
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(intent);
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
      },
      (err) => {
        this.notificationService.showDanger('Có lỗi xảy ra khi thêm câu trả lời');
        // console_bk.log(err);
      });
  }

  public updateFirstUsersay(ids) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFirstUsersay(ids);
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  // response block
  public updateIntentToBlock() {
    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.type_answer = this.responseTextType ? 'text' : 'block';
    intent.inner_action = this.currentIntent.inner_action;

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(intent);
    actionOperate.subscribe(
      (usay) => {
        this.currentIntent.type_answer = intent.type_answer;
      },
      (err) => {
        this.notificationService.showDanger('Cập nhật ' + this.responseTextType ? 'text' : 'block' + ' thất bại. Vui lòng thử lại');
        this.responseTextType = !this.responseTextType;
        // console_bk.log(err);
      });
  }

  public updateResponseBlock(event) {
    let intentBlock = { block_id: event.value, intent_id: this.currentIntent.id, status: 1 };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addOrUpdateIntentBlock([intentBlock]);
    actionOperate.subscribe(
      (usay) => {
        if (!this.currentIntent.response_message_blocks) {
          this.currentIntent.response_message_blocks = [];
        }
        this.currentIntent.response_message_blocks.push(intentBlock);
        this.sortIntentBlock();
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public RemoveResponseBlock(event) {
    // // console_bk.log('remove block:', event);
    let intentBlock = { block_id: event.value, intent_id: this.currentIntent.id, status: -1 };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addOrUpdateIntentBlock([intentBlock]);
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
        if (this.currentIntent.response_message_blocks) {
          this.currentIntent.response_message_blocks = _.reject(this.currentIntent.response_message_blocks, (num) => { return num.block_id === intentBlock.block_id; });
        }
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public sortIntentBlock() {
    let sortArr = [];
    let _ids = _.pluck(this.blockSelected, 'value');
    _ids.filter((item) => {
      sortArr.push({ intent_id: this.currentIntent.id, block_id: item, status: 1 });
    });
    // // console_bk.log('ids:', _ids);
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.sortIntentBlock({ ids: sortArr });
    actionOperate.subscribe(
      (usay) => {
        // // console_bk.log('usersay', usay);
      },
      (err) => {
        // console_bk.log(err);
      });
  }

  public onItemAdded(event) {
    if (!this.responseBlocks) {
      this.responseBlocks = [];
    }
    this.responseBlocks.filter((item) => {
      if (item.intent_id === event.value && item.status === -1) {
        item.status = -1;
        let block = { intent_id: this.currentIntent.id, block_id: event.value };
        this.responseBlocks.push(block);
      }
    });
  }

  public onItemRemoved(event) {
    if (this.responseBlocks && this.responseBlocks.length > 0) {
      this.responseBlocks.filter((item) => {
        if (item.intent_id === event.value) {
          item.status = -1;
        }
      });
    }
    // this.blockSelected.push(event);
  }

  public updateContentSendInbox() {
    if (!this.currentIntent.intent_entities) {
      this.currentIntent.intent_entities = [];
    }
    let entity = this.currentIntent.intent_entities.filter((item) => item.param === 'noi_dung_inbox')[0];
    if (!entity) {
      entity = {};
      entity.entity_name = 'ht.bat_ky_gi'; // Nhung gui
      entity.param = 'noi_dung_inbox';
      entity.default_value = this.textToInbox ? this.textToInbox : '';
      entity.lifespan = 2;
      entity.intent_id = this.currentIntent.id;
      entity.priority = 1;
      entity.status = 1;
      this.currentIntent.intent_entities.push(entity);
    } else {
      this.currentIntent.intent_entities.filter((item) => {
        if (item.param === 'noi_dung_inbox') {
          item.default_value = this.textToInbox ? this.textToInbox : '';
          item.lifespan = 2;
          item.intent_id = this.currentIntent.id;
          item.priority = 1;
          item.status = 1;
        }
      });
    }

    let intent: any = {};
    let mes: any = {};
    intent.id = this.currentIntent.id;
    intent.agent_id = this.currentIntent.agent_id;
    intent.inner_action = this.currentIntent.inner_action;
    intent.intent_entities = this.currentIntent.intent_entities;
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(intent);
    actionOperate.subscribe(
      (usay) => {
        // success
      },
      (err) => {
        this.notificationService.showDanger('Cập nhật thất bại. Vui lòng thực hiện lại');
      });
  }

  public delete() {
    this.outputDelete.emit(this.index);
  }

  public onTagEdited($event: TagModel) {
    this.currentIntent.usersays.filter((us) => {
      // console_bk.log(JSON.stringify(us));
      if (us.content === $event) {
        this.openDialogEdit(us.id, us.content);
      }
    });
    // this.openDialogEdit();
  }

  public openDialogEdit(_id, _content) {
    let dialogRef = this.dialog.open(BaFaqEditComponent, {
      width: '50%',
      data: {
        id: _id,
        content: _content
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.currentIntent.usersays.filter((us) => {
        if (result && us.id === _id) {
          us.content = result.content;
        }
      });
      if (this.currentIntent.usersays) {
        this.usersaysTemp = _.pluck(this.currentIntent.usersays, 'content');
      }
    });
  }
  public onTagResponseEdited($event: TagModel) {
    this.openDialogResponseEdit($event);
  }

  public openDialogResponseEdit($event) {
    this.currentIntent.response_messages = [{ content: '' }];
    this.currentIntent.response_messages[0].content = JSON.stringify([{ message: { posibleTexts: this.responseMessage } }]);
    let dialogRef = this.dialog.open(BaResponseEditComponent, {
      width: '50%',
      data: {
        currentIntent: this.currentIntent,
        content: $event
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (_.size(this.currentIntent.response_messages) > 0) {
        let obj = JSON.parse(this.currentIntent.response_messages[0].content);
        this.responseMessage = obj[0].message.posibleTexts;
      }
    });
  }
}
