import { ConfigService } from './../../../_services/config.service';
import { ApplicationService } from './../../../_services/application.service';
import { Component, OnInit, AfterViewInit, Inject, ChangeDetectorRef } from '@angular/core';
import { ROUTES, PAGE_MENU } from '../../sidebar/sidebar-routes.config';
import { MenuType } from '../../sidebar/sidebar.metadata';
import { Router, ActivatedRoute, Params, UrlSegment } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { saveAs } from 'file-saver';
import { DOCUMENT } from '@angular/platform-browser';
import { UUID } from 'angular2-uuid';
declare var gapi: any;
declare var $: any;
import _ from 'underscore';
@Component({
  selector: 'fbpopup-cmp',
  templateUrl: 'fbpopup.component.html',
  styleUrls: ['fbpopup.component.css']
})
export class FbPopupComponent {
  private location: any;
  constructor( @Inject(DOCUMENT) private document, private cdRef: ChangeDetectorRef, private applicationService: ApplicationService, location: Location, private router: Router, private activatedRoute: ActivatedRoute, private configService: ConfigService) {
    this.location = location;
  }
}
