import { Component, Input , Output , EventEmitter } from '@angular/core';

declare var $: any;

@Component({
    selector: 'sort-list',
    templateUrl: 'ba-sort-list.component.html',
    styleUrls: ['ba-sort-list.component.scss']
})
export class BaSortListComponent {
    @Input() value: string;
    @Input() data: any;
    @Output() valueChange = new EventEmitter();
    public ngOnInit() {
        $.getScript('../../assets/js/plugins/jquery.nestable.js');
        $.getScript('../../assets/js/plugins/sort_custom.js');
    }
}
