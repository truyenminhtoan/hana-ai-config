
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {ProductsModel} from "../../../_models/products.model";
import {ActionBasic} from "../../../_models/actions.model";
import {IntentEntityModel, IntentModel} from "../../../_models/intents.model";
import {Observable} from "rxjs/Rx";
import {ObjectMemoryModel} from "../../../_models/object_memory.model";
import {ObjectMemoryService} from "../../../_services/object_memory.service";

@Component({
  selector: 'ba-autocomplete-intent',
  templateUrl: 'ba-autocomplete.component.html',
  styleUrls: ['ba-autocomplete.component.scss']
})
export class BaAutocompleteIntentComponent {
  // tslint:disable-next-line:member-access
  @Input() source: any;
  @Output() output = new EventEmitter<any>();
  @Output() memory_id = new EventEmitter<any>();
  @Input() objects: ActionBasic[] = [];
  private currentIntent: IntentModel= new IntentModel();
  @Input() selectDefault: any;
  private stateCtrl: FormControl = new FormControl();
  private filteredStates: any;

  @Input() user_say_intent_entities: any;
  @Output() output_user_say_intent_entitie = new EventEmitter<any>();
  @Input() listEntyties: any;
  @Output() memory_name = new EventEmitter<any>();

  @Output() list_param_name = new EventEmitter<any>();
  private active: any;

  constructor(private objectService: ObjectMemoryService) {
    let that = this;
    setTimeout(function () {
      that.active = that.selectDefault;
    }, 1000);
  }

  private change(event: any) {
    this.memory_id.emit(event.value);
    this.handleChangeObjectMem(event.value);
  }

  public handleChangeObjectMem(value) {
    this.getObjectMemoryDetail(value, (cb) => {
      if (cb) {
        this.currentIntent.intent_entities = [];
        this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((obj) => {
          return (obj.global_memory_param == '' || obj.global_memory_param == null)
        });

        cb.object_memory_params.filter((item) => {
       
          let intentEntity: IntentEntityModel = new IntentEntityModel();
          intentEntity.is_required = false;
          intentEntity.param = item.param;
          if (this.listEntyties !== undefined) {
            for (var i =0; i < this.listEntyties.length; i++) {
              if (this.listEntyties[i].id == item.entity_id) {
                intentEntity.entity_name = this.listEntyties[i].name.replace('@', '');
                break;
              }
            }
          } else {
            intentEntity.entity_name = '';
          }
          intentEntity.default_value = '';
          intentEntity.priority = 0;
          intentEntity.status =1 ;
          intentEntity.response_messages = [];
          intentEntity.intent_entities_suggestion = [];
          this.currentIntent.intent_entities.push(intentEntity);

          if (intentEntity.entity_name != undefined && intentEntity.entity_name != null) {
            this.user_say_intent_entities = this.user_say_intent_entities.filter((row) => {
              return (row.param.trim() != intentEntity.param.trim() || row.entity_name.trim() != intentEntity.entity_name.trim())
            });
          }

        });
        //4. Bên dưới bảng đã có tham số (kich_thuoc với entity là: kich_thuoc), sau đó chọn bộ nhớ có tham số (kich_thuoc với entity: kich_thuoc), do 2 tham số này trùng nhau cả tên và loại dữ liệu nên chỉ giữ lại tham số của bộ nhớ
        if (this.currentIntent.intent_entities !== undefined && this.user_say_intent_entities !== undefined) {
          for (var i =0; i < this.currentIntent.intent_entities.length; i++) {
            this.user_say_intent_entities = this.user_say_intent_entities.filter((row) => {
              return (row.param != this.currentIntent.intent_entities[i].param || row.entity_name != this.currentIntent.intent_entities[i].entity_name)
            });
          }
        }

        this.memory_name.emit(cb.name);
        this.output.emit(this.currentIntent.intent_entities);
        this.output_user_say_intent_entitie.emit(this.user_say_intent_entities);
        var list_param_name = [];
        for (var i = 0; i < cb.object_memory_params.length; i++) {
          list_param_name.push(cb.object_memory_params[i].name);
        }
        this.list_param_name.emit(list_param_name);
      }
    });
  }

  public getObjectMemoryDetail(id, callback) {
    let actionOperate: Observable<ObjectMemoryModel>;
    actionOperate = this.objectService.getObjectMemoryById(id);
    actionOperate.subscribe(
        (data) => {

          callback(data);
        },
        (err) => {
          // load event fail
          callback(false);
        });
  }
  
}

