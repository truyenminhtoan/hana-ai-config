import {
    Component,
    Inject,
    EventEmitter, Output
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
declare var $: any;
declare var swal: any;
import {Observable} from "rxjs/Observable";
import {BlocksService} from "../../../../../_services/blocks.service";
import {IntentService} from "../../../../../_services/intents.service";
import {NotificationService} from "../../../../../_services/notification.service";
import {EmitterService} from "../../../../../_services/emitter.service";

@Component({
    selector: 'detail-intent-action-dialog',
    templateUrl: 'detail-intent-action-dialog.component.html',
    styleUrls: ['detail-intent-action-dialog.component.scss']
})
export class DetailIntentActionDialogComponentV3 {

    @Output() output = new EventEmitter<any>();

    private currentIntent;

    /** hanh dong */
    private addCustomerToSequences = [];
    private removeCustomerFromGroups = [];
    private removeCustomerFromSequences = [];
    private addCustomerToGroups = [];
    private dropdownListGroups = [];
    private dropdownListSequnces = [];
    private list_actions = [];

    private is_load = true;
    private intent_actions = [];

    private paramModelSelected = [
        {
            value: 'addCustomerToGroups',
            lable: 'Thêm vào nhóm'
        },
        {
            value: 'removeCustomerFromGroups',
            lable: 'Bỏ ra khỏi nhóm'
        },
        {
            value: 'addCustomerToSequences',
            lable: 'Thêm vào quy trình'
        },
        {
            value: 'removeCustomerFromSequences',
            lable: 'Bỏ ra khỏi quy trình'
        },
        {
            value: 'createOrder',
            lable: 'Lên đơn hàng'
        }
    ];

    constructor(private blocksService: BlocksService, private intentService: IntentService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < DetailIntentActionDialogComponentV3 > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    public okClick() {
        this.save2();
    }

    ngOnInit() {
        let that = this;
        if (this.data != undefined) {
            if (this.data.currentIntent != undefined) {
                this.currentIntent = this.data.currentIntent;
                if (this.currentIntent.intent_actions == undefined) {
                    this.currentIntent.intent_actions = [];
                } else {
                    if (Object.keys(this.currentIntent.intent_actions).length == 0) {
                        this.currentIntent.intent_actions = [];
                    }
                }
            }
            let tmp = JSON.stringify(this.currentIntent.intent_actions);
            this.intent_actions = JSON.parse(tmp);

            Object.keys(this.data.groups).forEach(function(k) {
                that.dropdownListGroups.push({
                    "id": that.data.groups[k].id,
                    "name": that.data.groups[k].name
                }, );
            });
            Object.keys(this.data.sequences).forEach(function(k) {
                that.dropdownListSequnces.push({
                    "id": that.data.sequences[k].id,
                    "name": that.data.sequences[k].name
                }, );
            });

            this.is_load = false;
        }
    }

    private checkArray(value, lists) {
        for (let i = 0; i < lists.length; i++) {
            if (lists[i].id == value) {
                return lists[i];
            }
        }

        return false;
    }

    private onAddOnRemove() {

    }

    private save() {

            let intent_entities = [];
            if (this.checkAction('addCustomerToGroups') === true) {
                let ids = _.pluck(this.addCustomerToGroups, 'id');
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
            if (this.checkAction('addCustomerToSequences') === true) {
                let ids = _.pluck(this.addCustomerToSequences, 'id');
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'addSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
            if (this.checkAction('removeCustomerFromGroups') === true) {
                let ids = _.pluck(this.removeCustomerFromGroups, 'id');
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeGroups',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
            if (this.checkAction('removeCustomerFromSequences') === true) {
                let ids = _.pluck(this.removeCustomerFromSequences, 'id');
                let item = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'removeSequences',
                    "default_value": JSON.stringify(ids)
                };
                intent_entities.push(item);
            }
            if (this.checkAction('createOrder') === true) {
                let item_1 = {
                    "entity_name" : "ht.so_dien_thoai",
                    "param" : 'phonenumber',
                    "default_value": ""
                };
                intent_entities.push(item_1);
                let item_2 = {
                    "entity_name" : "ht.bat_ky_gi",
                    "param" : 'note',
                    "default_value": "$usersay"
                };
                intent_entities.push(item_2);
            }
            if (this.currentIntent.intent_entities == undefined) {
                this.currentIntent.intent_entities = [];
            }
            intent_entities.filter((row) => {
                this.currentIntent.intent_entities.push(row);
            });

            this.updateFQA();
    }

    private save2() {
        let intent_entities = [];
        if (this.checkAction('addCustomerToGroups') === true) {
            let ids = _.pluck(this.addCustomerToGroups, 'id');
            let item = {
                "entity_name" : "ht.bat_ky_gi",
                "param" : 'addGroups',
                "default_value": JSON.stringify(ids)
            };
            intent_entities.push(item);
        }
        if (this.checkAction('addCustomerToSequences') === true) {
            let ids = _.pluck(this.addCustomerToSequences, 'id');
            let item = {
                "entity_name" : "ht.bat_ky_gi",
                "param" : 'addSequences',
                "default_value": JSON.stringify(ids)
            };
            intent_entities.push(item);
        }
        if (this.checkAction('removeCustomerFromGroups') === true) {
            let ids = _.pluck(this.removeCustomerFromGroups, 'id');
            let item = {
                "entity_name" : "ht.bat_ky_gi",
                "param" : 'removeGroups',
                "default_value": JSON.stringify(ids)
            };
            intent_entities.push(item);
        }
        if (this.checkAction('removeCustomerFromSequences') === true) {
            let ids = _.pluck(this.removeCustomerFromSequences, 'id');
            let item = {
                "entity_name" : "ht.bat_ky_gi",
                "param" : 'removeSequences',
                "default_value": JSON.stringify(ids)
            };
            intent_entities.push(item);
        }
        if (this.checkAction('createOrder') === true) {
            let item_1 = {
                "entity_name" : "ht.so_dien_thoai",
                "param" : 'phonenumber',
                "default_value": ""
            };
            intent_entities.push(item_1);
            let item_2 = {
                "entity_name" : "ht.bat_ky_gi",
                "param" : 'note',
                "default_value": "$usersay"
            };
            intent_entities.push(item_2);
        }

        if (this.currentIntent.intent_entities == undefined) {
            this.currentIntent.intent_entities = [];
        }
        intent_entities.filter((row) => {
            this.currentIntent.intent_entities.push(row);
        });

        this.updateFQA();
    }

    private updateFQA() {
        // this.currentIntent.intent_actions = this.intent_actions;
        // this.currentIntent.intent_actions.filter((item) => {
        //     item.type = "inner";
        // });
        //console.log('intent', JSON.stringify(this.currentIntent));
        this.output.emit(this.currentIntent);
    }

    private addAction(type) {
            let check = false;
            this.intent_actions.filter((item) => {
                if (item.action_name === type.value) {
                    check = true;
                }
            });
            if (check === false) {
                this.intent_actions.push({action_name: type.value});
            } else {
                //this.notificationService.showDanger("Hành động này đã tồn tại");
            }
    }

    private deleteAction(item) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.intent_actions = this.intent_actions.filter((row) => {
                return row.action_name !== item;
            });
        }, (dismiss) => {
        });
    }

    private checkAction(type) {
        if (this.currentIntent === undefined) {
            return false;
        }
        if (this.currentIntent.intent_actions === undefined) {
            return false;
        }

        let check = false;
        this.intent_actions.filter((item) => {
            if (item.action_name === type) {
              check = true;
            }
        });

        return check;
    }

    onAddGroup(event) {
        let that = this;
        if (event.display.trim() == event.value.trim()) {
            swal({
                title: 'Thông báo',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display.trim()};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            console.log('event', event);
                            console.log(' that.addCustomerToGroups',  this.addCustomerToGroups);
                            for (let i = 0 ; i < that.addCustomerToGroups.length; i++) {
                                if (that.addCustomerToGroups[i].display == event.display) {
                                    that.addCustomerToGroups[i].value = result_add_group.id;
                                    that.addCustomerToGroups[i].id =  result_add_group.id;
                                    that.addCustomerToGroups[i].name =  result_add_group.name;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            EmitterService.get('LOAD_LIST_BLOCKS_ADDED').emit([]);
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.addCustomerToGroups = this.addCustomerToGroups.filter((row) => {
                    return (row.value != event.value)
                });
            });
        }
    }
}