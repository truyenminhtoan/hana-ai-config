import {Component, Input, EventEmitter, Output, ChangeDetectorRef, Inject, OnInit, AfterViewInit} from '@angular/core';
import { ApplicationService } from '../../../_services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../../../_services/config.service';
import { DOCUMENT } from '@angular/platform-browser';
import { Location } from '@angular/common';
import {DetailIntentActionDialogComponentV3} from "./components/detail-intent-action-dialog/detail-intent-action-dialog.component";
import {MdDialog} from "@angular/material";
import {IntentService} from "../../../_services/intents.service";
import {Observable} from "rxjs/Observable";
import * as _ from 'underscore';
import {UserModel} from "../../../_models/user.model";
import {BlocksService} from "../../../_services/blocks.service";
import {EmitterService} from "../../../_services/emitter.service";

@Component({
    selector: 'ba-intent-action-v3',
    templateUrl: 'ba-intent-action.component.html',
    styleUrls: ['ba-intent-action.component.scss']
})
export class BaIntentActionComponentV3 implements OnInit {
    @Input() private currentIntent: any = {};
    @Output() output = new EventEmitter<any>();


    /** biens */
    private list_context_full = [];
    private autocomplete_context = [];
    private intent_sugest_auto_completes = [];
    private list_sequences = [];
    private list_groups = [];

    private is_click = false;
    private count_actions = 0;

    constructor(private blockService: BlocksService, private intentService: IntentService, private dialog: MdDialog, private router: Router, private configService: ConfigService) {
        if (this.currentIntent.intent_actions == undefined) {
            this.currentIntent.intent_actions = [];
        }
    }

    public ngOnInit(): void {
        EmitterService.get('LOAD_LIST_BLOCKS_ADDED').subscribe((data: any) => {
            this.getData();
        });
        this.getData();
    }

    private getData() {
        /** Load data */
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let app_id = currentUser.app_using.applications[0].app_id;
        let actionOperateGroup: Observable<any>;
        actionOperateGroup = this.blockService.listGroupByApp(app_id);
        actionOperateGroup.subscribe(
            (data_groups) => {
                if (data_groups != undefined && data_groups.length > 0) {
                    this.list_groups = data_groups;
                } else {
                    this.list_groups = [];
                }
                let actionOperateSequence: Observable<any>;
                actionOperateSequence = this.blockService.listSequencesByApp(app_id);
                actionOperateSequence.subscribe(
                    (sequences) => {
                        if (sequences != undefined && sequences.length > 0) {
                            this.list_sequences = sequences;
                        } else {
                            this.list_sequences = [];
                        }
                    });
            });
    }

    private showDetail() {
        if (this.currentIntent == undefined) {
            return;
        }
        let dialogRef = this.dialog.open(DetailIntentActionDialogComponentV3, {
            width: '50%',
            data: {
                currentIntent: this.currentIntent,
                sequences: this.list_sequences,
                groups: this.list_groups
            }
        });
        dialogRef.afterClosed().subscribe(data_close => {
            // if (data_close != undefined && data_close != null && data_close.intent_actions !== undefined) {
            //     that.count_actions = data_close.intent_actions.length;
            // }
        });
    }

    public getAutoCompleteContent() {
        let actionOperateIt: Observable<any>;
        actionOperateIt = this.intentService.initIntentView(localStorage.getItem('currentAgent_Id'), true);
        actionOperateIt.subscribe(
            (data) => {
                this.list_context_full = data.contexts;
                this.autocomplete_context = data.contexts;
            },
            (err) => {
            });
    }
}
