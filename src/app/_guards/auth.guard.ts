import { Injectable, NgModule } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserModel } from '../_models/user.model';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ConfigService } from '../_services/config.service';
declare var $: any;
@Injectable()
export class AuthGuard implements CanActivate {
  private currentUser: UserModel;
  private urlAuthen = '';
  constructor(private router: Router, public http: Http, public configService: ConfigService) {
    this.urlAuthen = configService.HOST_MY;
    if (localStorage.getItem('currentUser')) {
      this.checkToken();
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (localStorage.getItem('session_time')) {
        let d = new Date();
        let now = d.getTime();
        let sessionStoreTime = JSON.parse(localStorage.getItem('session_time'));
        let timeBefor = sessionStoreTime.timeStart;
        let timeUnixSession = configService.SESSION_TIME * 60 * 1000;
        // console_bk.log(now + ' now and befor ' + timeBefor);
        let rangTime = now - parseInt(timeBefor, 10);
        // console_bk.log(rangTime + ' and ' + timeUnixSession);
        if (rangTime >= timeUnixSession) {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('session_time');
          localStorage.removeItem('apphanausing');
          this.router.navigate(['/login-hana']);
        } else {
          let sessionStoreTimeNew = {
            timeStart: now
          };
          localStorage.setItem('session_time', JSON.stringify(sessionStoreTimeNew));
        }
      } else {
        let d = new Date();
        let n = d.getTime();
        let sessionStoreTime = {
          timeStart: n
        };
        localStorage.setItem('session_time', JSON.stringify(sessionStoreTime));
      }
    } else {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      this.router.navigate(['/login-hana']);
    }
  }

  public checkToken() {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    let url = this.urlAuthen + '/backend_app_user/check_token?uid=' + Math.floor(Date.now());
    let bodyString = `{}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + user.token });
    let options = new RequestOptions({ headers: header });
    return this.http.get(url, options)
      .toPromise()
      .then((response) => {
        if (response.status === 403) {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
          this.router.navigate(['/login-hana']);
        } else {
          return response.json();
        }

      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    if (error.status === 403) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      this.router.navigate(['/login-hana'], {});
    }
    return;
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.checkCurrentAppToken();
    if (this.currentUser) {
      // logged in so return true
      return true;
    }
    if (this.currentUser && this.currentUser.is_lock) {
      this.router.navigate(['/lock'], { queryParams: { returnUrl: state.url } });
      return false;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login-hana'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  public checkCurrentAppToken() {
    if ($('#token') && $('#token').val()) {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if ($('#token').val().indexOf(currentUser.app_using.id) === -1) {
          location.reload();
        }
      }
    }
  }
}
