export class UserLoginModel {
    public id: string;
    public full_name: string;
    public email: string;
    public username: string;
    public access_token: string;
    public url_avatar: string;
    public password: string;
    public type_login: string;
    public register_from: string;
    public affiliate: string;

    constructor() {
    }
}
