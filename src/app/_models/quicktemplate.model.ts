export class QuickTemplate {
  public template_reply_id: number;
  public quick_search: string;
  // tslint:disable-next-line:variable-name
  public text_search: string;
  public partner_id: number;
  public status: number;
}
