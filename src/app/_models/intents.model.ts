import { EventsModel } from './events.model';
export class IntentModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public name_text: string;
  public prioriy: number;
  // tslint:disable-next-line:variable-name
  public is_system: boolean;
  // tslint:disable-next-line:variable-name
  public agent_id: string;
  // tslint:disable-next-line:variable-name
  public action_name: string;
  public type: number;
  public status: number;
  // tslint:disable-next-line:variable-name
  public is_just_understand_in_context: boolean = false;
  // tslint:disable-next-line:variable-name
  public is_enable_send_action: boolean = false;
  // tslint:disable-next-line:variable-name
  public is_machine_learning: boolean = false;
  // tslint:disable-next-line:variable-name
  public is_suggestion_predict: boolean = false;
  // tslint:disable-next-line:variable-name
  public created_date: string;
  // tslint:disable-next-line:variable-name
  public modified_date: string;
  // tslint:disable-next-line:variable-name
  public intent_contexts: IntentContextModel[];
  // tslint:disable-next-line:variable-name
  public intent_events: IntentEventModel[];
  public usersays: UserSayModel[];
  // tslint:disable-next-line:variable-name
  public response_messages: ResponseMessageModel;
  // tslint:disable-next-line:variable-name
  public intent_entities: IntentEntityModel[];
  public inner_action: string;
  public outler_action: string;
  public memory_id: string;

  public contexts_suggestions: IntentContextsSuggestionsModel[];
}
// tslint:disable-next-line:max-classes-per-file
export class IntentContextModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public intent_id: string;
  public type: number = 1;
  public status: number = 1;
  public contexts: ContextModel;
  public created_date: string;
}
// tslint:disable-next-line:max-classes-per-file
export class ContextModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public name_text: string;
  // tslint:disable-next-line:variable-name
  public agent_id: string;
  public status: number;
  public created_date: string;
}
// tslint:disable-next-line:max-classes-per-file
export class IntentEventModel {
  public id: string;
  public status: number = 1;
  // tslint:disable-next-line:variable-name
  public intent_id: string;
  public events: EventsModel;
}
// tslint:disable-next-line:max-classes-per-file
export class UserSayModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public intent_id: string;
  public content: string;
  // tslint:disable-next-line:variable-name
  public content_text: string;
  public status: number = 1;
  // tslint:disable-next-line:variable-name
  public is_first: number = 0;
  public custom_id: string;
  public usersay_entities: UsersayEntities[];
  public created_date: string;
}

// tslint:disable-next-line:max-classes-per-file
export class UsersayEntities {
  public agent_id: string;
  // tslint:disable-next-line:variable-name
  public entity_name: string;
  public param: string;
}

// tslint:disable-next-line:max-classes-per-file
export class ResponseMessageModel {
  public id: string;

  public respond_intent_id: string;

  public question_intent_entity_id: string;

  public content: string;

  public status: number = 1;

}

// tslint:disable-next-line:max-classes-per-file
export class IntentEntitySuggestion {
  public id: string;
  public intent_entity_id: string;
  public value: string;
  public status: number;
  public content: string;
}

// tslint:disable-next-line:max-classes-per-file
export class IntentEntityModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public intent_id: string;
  // tslint:disable-next-line:variable-name
  public is_required: boolean;
  // tslint:disable-next-line:variable-name
  public entity_name: string;
  public param: string;
  // tslint:disable-next-line:variable-name
  public default_value: string;
  public priority: number;
  public status: number;
  // tslint:disable-next-line:variable-name
  public response_messages: ResponseMessageModel[];
  // tslint:disable-next-line:variable-name
  public response_suggestion: IntentEntitySuggestion[];
  public intent_entities_suggestion: IntentEntitySuggestion[];
  //public global_memory_param: string;

  public global_memory_param: string;
  public global_memory: string;
  public custom_id: string;
  public is_usersay: boolean;
  public is_check_param: boolean;
}
// tslint:disable-next-line:max-classes-per-file
export class IntentFilterModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public group_context_in: string;
  // tslint:disable-next-line:variable-name
  public group_context_out: string;
  // tslint:disable-next-line:variable-name
  public group_event: string;
}






// tslint:disable-next-line:max-classes-per-file
export class IntentContextsSuggestionsModel {
  public id: string;
  public priority: number = 0;
  public status: number = 0;
  public suggestion_intents: SuggestionsIntentsModel;
}
// tslint:disable-next-line:max-classes-per-file
export class SuggestionsIntentsModel {
  public id: string;
  public name: string;
}