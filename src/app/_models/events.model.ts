export class EventsModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public name_text: string;
  public name_old: string;
  // tslint:disable-next-line:variable-name
  public is_system: boolean = false;
  // tslint:disable-next-line:variable-name
  public type: string;
  public agent_id: string;
  public status: number = 1;
  // tslint:disable-next-line:variable-name
  public event_params: EventParamsModel[];
}
// tslint:disable-next-line:max-classes-per-file
export class EventParamsModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public name_text: string;
  public param: string;
  public description: string;
  // tslint:disable-next-line:variable-name
  public event_id: string;
  public status: number;
}
