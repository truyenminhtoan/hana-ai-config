export class UserModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public full_name: string;
  public email: string;
  public username: string;
  public access_token: string;
  // tslint:disable-next-line:variable-name
  public avatar_url: string;
  public password: string;
  // tslint:disable-next-line:variable-name
  public is_lock: boolean;
  public partners: PartnerModel;
  public roles: RoleModel;
  public partner_id:string;
  constructor() {
    this.partners = new PartnerModel();
    this.partners.company = '';
    this.partners.website = '';

    this.roles = new RoleModel();
    this.roles.id = 'ADMIN';
  }
}
// tslint:disable-next-line:max-classes-per-file
export class AgentModel {
  public id: string;
  public name: string;
  // tslint:disable-next-line:variable-name
  public name_text: string;
  public tmp_name: string;
}
// tslint:disable-next-line:max-classes-per-file
export class PartnerModel {
  public id: string;
  public company: string;
  public website: string;
}
// tslint:disable-next-line:max-classes-per-file
export class RoleModel {
  public id: string;
  // tslint:disable-next-line:eofline
}
