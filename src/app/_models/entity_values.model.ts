
import { EntityValueSynonyms } from './entity_value_synonyms.model';

export interface EntityValues {
  id: number;
  main_word: string;
  status: number;
  entity_id: number;
  entity_value_synonyms: EntityValueSynonyms[];
}
