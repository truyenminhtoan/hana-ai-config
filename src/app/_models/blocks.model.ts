export class BlockGroup {
	public blocks: Block[];
	public id: string;
	public name: string;
	public partner_id: number;
	public priority: number;
	public type: string;
}
export class Block {
	public id: string;
	public name: string;
	public block_group_id: string;
	public content: string;
}

export class Button {
	public type: string;
	public url: string;
	public title: string;
	public webview_height_ratio: string;
	public payload: string;
}

export class Payload {
	public template_type: string;
	public text: string;
	public buttons: Button[];
	public url: string;
	public elements: Element[];
}

export class Attachment {
	public type: string;
	public payload: Payload;
}

export class Message {
	public attachment: Attachment;
	public text: string;
	public quick_replies: QuickReply[];
}

export class Content {
	public message: Message;
}

export class QuickReply {
	public content_type: string;
	public title: string;
	public payload: string;
}

export class Element {
	public title: string;
	public image_url: string;
	public subtitle: string;
	public buttons: Button[];
}
