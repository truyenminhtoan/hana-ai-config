import { EntityValues } from './entity_values.model';

export class EntitieModel {
    public id: string;
    public name: string;
    public name_text: string;
    public type: number;
    public is_system: number;
    public status: number;
    public agent_id: string;
    public entity_values: EntityValuesModel[];
}

export class EntityValuesModel {
    public id: string;
    public index: number;
    public main_word: string;
    public status: number;
    public created_date: string;
    public entity_value_synonyms: EntityValueSynonymsModel[];
}

export class EntityValueSynonymsModel {
    public id: string;
    public index: number;
    public word: string;
    public status: number;

    public entity_value_id: string;
    public created_date: string;
}


