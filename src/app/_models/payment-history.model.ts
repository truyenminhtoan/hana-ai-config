export interface PaymentHistoryModel {
    id: string;
    type: string;
    package: string;
    value: string;
    code: string;
    payment_date: Date;
}