export class ActionGroup {
  public id: string;
  public actionBaseName: string;
  // tslint:disable-next-line:variable-name
  public group_name: string;
  public actions: ActionModel[];
}

// tslint:disable-next-line:max-classes-per-file
export class ActionModel {
  public id: string;
  public actionId: string;
  public actionBaseName: string;
  public name: string;
  public nameDisplay: string = '';
  public agentId: string;
  public meta: Meta[];
  public inputs: Inputs[];
  public outputs: Outputs[];
}

// tslint:disable-next-line:max-classes-per-file
export class Meta {
  public id: string;
  public key: string;
  public value: any;
}
// tslint:disable-next-line:max-classes-per-file
export class Inputs {
  public id: string;
  public meta: Meta[];
  public param: string;
  public name: string;
  public defaultValue: string;
}

// tslint:disable-next-line:max-classes-per-file
export class Outputs {
  public id: string;
  public actionId: string;
  public eventId: string;
  public eventTypeName: string;
  public eventName: string;
  public eventBaseName: string;
}

// tslint:disable-next-line:max-classes-per-file
export class ActionBasic {
  public id: string;
  public name: string;
  public nameDisplay: string;
  public actionBaseName: string;
  public agentId: string;
}
