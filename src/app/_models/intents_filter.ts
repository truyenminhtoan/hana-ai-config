export class IntentFilterModel {
    public agent_id: string;
    public contexts: string[];
    public usersay: string;
}

export class FilterIntentModel {
    public id: string;
    public content: string;
}
export class ListFilterIntentModel {
    public item: FilterIntentModel[];
}