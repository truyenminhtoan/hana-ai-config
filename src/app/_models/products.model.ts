export class CategoryModel {
  public id: string;
  public name: string;
  public description: string;
  public products: ProductsModel[];
}

export class ProductsModel {
  public id: string;
  public name: string;
  public product_name_old: string;
  public partner_id: string;
  public agent_id: string;
  public description: string;
  public tags: string;
  public vendor: string;
  public status : number;

  public product_criterias: ProductCriteria[];
}

export class ProductItemsModel {
  public id: string;
  public name: string;
  public param: string;
  public type: string;
  public data_type: string;
  public search_type: string;
  public weight: string;
  public entity_id: string;
}


export class ProductCriteria {
  public product_id: string;
  public id: string;
  public name: string;
  public param: string;
  public entity_id: string;
  public entity_name: string;
  public type: string;
}
