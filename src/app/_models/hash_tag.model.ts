export class HashTag {
    public id: string;
    public name: string;
    public partner_id: string;

    public context_id: string;
    public status: string;
    public type: number;
    public created_date: string;
    public agent_id: string;
}