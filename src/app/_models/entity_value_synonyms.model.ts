export interface EntityValueSynonyms {
  id: number;
  word: string;
  status: number;
  entity_value_id: number;
}
