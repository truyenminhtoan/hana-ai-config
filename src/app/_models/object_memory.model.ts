export class ObjectMemoryModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public is_system: boolean = false;
  // tslint:disable-next-line:variable-name
  public agent_id: string;
  public name: string;
  public status: number = 1;
  // tslint:disable-next-line:variable-name
  public object_memory_params: ObjectMemoryParamsModel[];
}

// tslint:disable-next-line:max-classes-per-file
export class ObjectMemoryParamsModel {
  public id: string;
  public name: string;
  public param: string;
  // tslint:disable-next-line:variable-name
  public entity_id: string;
  public status: number;
}

// tslint:disable-next-line:max-classes-per-file
export class ObjectMemoryActionsModel {
  public id: string;
  // tslint:disable-next-line:variable-name
  public action_id: string;
  // tslint:disable-next-line:variable-name
  public object_memory_id: string;
  public status: number;
}
