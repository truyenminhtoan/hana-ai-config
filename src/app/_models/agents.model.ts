export class AgentsModel {
    public id:string;
    public name:string;
    public partner_id:string;
    public partner_user_id:string;
    public is_public:boolean;
    public status: number;
    public time_zone:string;
    public language:string;
    public description:string;
    public avatar_url:string;
    public tmp_name:string;
    public tmp_description:string;
    public tmp_avatar_url:string;
}