export class Customer {
  id;
  choice;
  user_app_fb_id;
  avatar;
  first_name;
  last_name;
  full_name;
  gender;
  locale;
  timezone;
}
