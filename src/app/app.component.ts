/*
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation, ViewChild
} from '@angular/core';
import { AppState } from './app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentInteractionService } from './_services/compoent-interaction-service';
import { Subscription } from 'rxjs/Rx';
import { SideBarComponent } from './themes/sidebar/sidebar.component';
/*
 * App Component
 * Top Level Component
 */
declare var $: any;

@Component({
  selector: 'my-app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

  private eventSubscription: Subscription;

  constructor(public appState: AppState, private _componentService: ComponentInteractionService) {
  }

  public ngOnInit() {
    
  }

  public ngAfterViewInit() {
    // console.log('APPENDDDDDDDDĐ');
  }
}
