import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CustomFormsModule } from 'ng2-validation';
// noinspection TypeScriptCheckImport
import {
  NgModule, CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';

import { PagesModule } from './pages/pages.module';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { NgaModule } from './themes/nga.module';
import { routing } from './app.routing';
import '../styles/styles.scss';
import { AuthGuard } from './_guards/auth.guard';
import { UserService } from './_services/user.service';
import { AgentService } from './_services/agents.service';
import { EntityService } from './_services/entity.service';
import { ComponentInteractionService } from './_services/compoent-interaction-service';
import { ActionsService } from './_services/actions.service';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { IntentService } from "./_services/intents.service";
import { ProductsService } from "./_services/products.service";
import {MenusService} from "./_services/menus.service";

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpModule,
    PagesModule,
    NgaModule.forRoot(),
    routing,
    CustomFormsModule,
    FormsModule,
    SlimLoadingBarModule.forRoot()
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AuthGuard,
    UserService,
    AgentService,
    EntityService,
    IntentService,
    ProductsService,
    ComponentInteractionService,
    ActionsService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    MenusService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {

}
