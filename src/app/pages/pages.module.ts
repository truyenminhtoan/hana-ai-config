import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { routing }       from './pages.routing';
import { PagesComponent } from './pages.component';
import { NgaModule } from '../themes/nga.module';
import { SideBarComponent } from '../themes/sidebar/sidebar.component';
// import { NavbarComponent } from '../themes/navbar/navbar.component';
import { FooterComponent } from '../themes/footer/footer.component';
import '../../styles/styles.scss';
@NgModule({
  imports: [CommonModule, NgaModule, routing],
  declarations: [
    PagesComponent,
    SideBarComponent,
    // NavbarComponent,
    FooterComponent,
  ]
})
export class PagesModule {
}
