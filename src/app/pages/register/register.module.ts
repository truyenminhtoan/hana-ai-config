// Angular Imports
import { NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './register.routing';
import { UserService } from '../../_services/user.service';
import { NotificationService } from '../../_services/notification.service';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      routing
    ],
    declarations: [
        RegisterComponent,
    ],
    providers: [UserService, NotificationService],
    exports: [
        RegisterComponent,
    ]
})
export class RegisterModule {

}
