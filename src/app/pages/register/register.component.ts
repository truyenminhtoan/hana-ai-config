import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserModel } from '../../_models/user.model';
import { NotificationService } from '../../_services/notification.service';
declare var $: any;
declare var gapi: any;
declare const FB: any;
@Component({
  selector: 'register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {
  // tslint:disable-next-line:max-line-length
  constructor(private userService: UserService, private router: Router, private notify: NotificationService) { }
  public ngAfterViewInit() {
    $.getScript('../../../assets/js/login.js');
  }

  public ngOnInit() {
    // FACEBOOK INIT
    FB.init({
      appId: '378516159201961',
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
  }

  /**
   * LOGIN GOOGLE
   */
  public sigin() {
    gapi.signin2.render('my-signin2', {
      onsuccess: (param) => this.onSignIn(param),
      scope: 'profile email',
      width: 37,
      height: 37
    });
  }

  public onSignIn(googleUser) {
    let profile = googleUser.getBasicProfile();
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());
    this.findOrAdd(profile.getName(), profile.getImageUrl(), profile.getEmail());
  }

  /**
   * FACEBOOK LOGIN
   */
  public onFacebookLoginClick() {
    FB.login((response) => {
      if (response.authResponse) {
        FB.api('/me', { fields: 'id,name,email' }, (res) => {
          console.log('Good to see you, ' + JSON.stringify(res) + '.');
          this.findOrAdd(res.name, '', res.email);
        });
      }
    }, { scope: 'email,public_profile', return_scopes: true });
  }

  // toantm: login or register social
  public findOrAdd(fname: string, img: string, uname: string) {
    let actionOperate: Observable<UserModel>;
    let user = new UserModel();
    user.full_name = fname;
    user.username = uname;
    user.avatar_url = img;
    actionOperate = this.userService.loginOrRegisterSocial(user);
    actionOperate.subscribe(
      (data) => {
        this.router.navigate(['/pages', 'intents', 'intentList']);
      },
      (err) => {
        console.log('');
      });
  }

  // dang ky local
  public register(fname: string, uname: string, pwd: string) {
    try {
      this.checkExisted(uname, (cb) => {
        if (cb === 0) {
          let actionOperate: Observable<UserModel>;
          let user = { full_name: fname, username: uname, password: pwd, status: 1, partners :{"company":"mideasvn","website":"https://mideasvn.com"},"roles":{"id":"ADMIN"} };
          actionOperate = this.userService.register(user);
          actionOperate.subscribe(
            (data) => {
              this.notify.showSuccess('Đăng ký thành công');
              this.router.navigate(['/login']);
            },
            (err) => {
              this.notify.showDanger('Lỗi khi đăng ký');
            });
        } else if (cb === 2) {
          this.notify.showDanger('Lỗi khi đăng ký');
        } else {
          this.notify.showDanger('Tài khoản đã tồn tại');
        }
      });
    } catch (error) {
      this.notify.showDanger('Lỗi khi đăng ký');
    }
  }

  public checkExisted(uname: string, callback) {
    let actionOperate: Observable<UserModel>;
    actionOperate = this.userService.getUserByEmail(uname);
    actionOperate.subscribe(
      (data) => {
        // true: existed
        if (data) {
          return callback(data);
        }
        callback(0);
      },
      (err) => {
        callback(2);
      });
  }
}
