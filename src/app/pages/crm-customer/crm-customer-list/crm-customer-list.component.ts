import { Component, Output, EventEmitter, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { GtConfig, GenericTableComponent } from '@angular-generic-table/core';
import { CrmCustomerService } from '../../../_services/crmcustomer.service';
import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';
import { TableData } from './table-data';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Router } from '@angular/router';

declare var $: any;
declare var _: any;

@Component({
    selector: 'crm-customer-list',
    templateUrl: 'crm-customer-list.component.html',
    styleUrls: ['crm-customer-list.component.scss']
})
export class CrmCustomerListComponent {
    public rows: Array<any> = [];
    public partnerId: any;
    public currentUser: any;
    public isDestroy: boolean;
    public columns: Array<any> = [
        { title: 'Tên', name: 'name', className: ['office-header', 'text-success'], filtering: { filterString: '', placeholder: 'Tên' } },
        {
            title: 'SĐT',
            name: 'phone',
            sort: false,
            className: ['office-header', 'text-success'],
            filtering: { filterString: '', placeholder: 'Số điện thoại' }
        },
        { title: 'Email', className: ['office-header', 'text-success'], name: 'email', filtering: { filterString: '', placeholder: 'Email' } },
        { title: 'Người tạo.', className: ['office-header', 'text-success'], name: 'backend_user_full_name', filtering: { filterString: '', placeholder: 'người tạo' } },
        {
            title: 'Ngày tạo',
            name: 'c_created_date',
            className: ['office-header', 'text-success'],
            filtering: { filterString: '', placeholder: 'Ngày tạo' }
        },
        // { title: 'Start date', className: 'text-warning', name: 'startDate' },
        // { title: 'Salary ($)', name: 'salary' }
    ];
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered']
    };

    // private data: Array<any> = TableData;
    private data: Array<any> = [];

    constructor(private crmCustomerService: CrmCustomerService, private router: Router, private cdRef: ChangeDetectorRef) {
        // this.length = this.data.length;
        this.checkLoged();
    }
    public checkLoged() {
        let current = localStorage.getItem('currentUser');
        if (current && current !== 'undefined') {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (this.currentUser.app_using) {
                this.partnerId = this.currentUser.app_using.id;
                this.getData();
            } else {
                localStorage.removeItem('currentUser');
                this.router.navigate(['/login-hana']);
            }
        } else {
            this.router.navigate(['/login-hana']);
        }
    }

    public getData() {

        let dataBody = {
            partner_id: this.partnerId
        }
        this.crmCustomerService.getAllCustomer(dataBody).then((apps) => {
            if (apps) {
                // console_bk.log('HELLO OO ' + JSON.stringify(apps));
                this.data = apps;
                this.length = apps.length;
                this.onChangeTable(this.config);
                this.callChange();
            }
        });
    };


    public ngOnDestroy() {
        this.isDestroy = true;
    }
    public ngOnInit(): void {
        this.onChangeTable(this.config);
        let h = $(window).height();
        $('.crm-customer-scroll').slimScroll({
            height: h - 60 + 'px'
        });
    }

    public changePage(page: any, data: Array<any> = this.data): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    if (!item[column.name]) {
                        if (column.name === 'backend_user_full_name') {
                            item[column.name] = 'Hana';
                        } else {
                            item[column.name] = '';
                        }

                    }
                    return item[column.name].match(column.filtering.filterString);

                });
            } else {
                filteredData = filteredData.filter((item: any) => {
                    if (!item[column.name]) {
                        item[column.name] = '';
                    }
                    return true;
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }
        // console_bk.log('filterdata1 ' + JSON.stringify(filteredData));
        if (config.filtering.columnName) {
            return filteredData.filter((item: any) => {
                if (!item[config.filtering.columnName]) {
                    item[config.filtering.columnName] = '';
                }
                item[config.filtering.columnName].match(this.config.filtering.filterString)
            }

            );
        }
        // console_bk.log('filterdata2 ' + JSON.stringify(filteredData));
        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                // console_bk.log('Tttttt ' + item[column.name]);
                if (item[column.name].toString().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.data, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }

    public onCellClick(data: any): any {
        // console_bk.log(data);
    }

    public exportData() {
        // console_bk.log('export data');
        let dataexport = [];
        dataexport.push({
            name: 'Tên',
            phone: 'SĐT',
            email: 'Email',
            backend_user_full_name: 'Người tạo',
            c_created_date: 'Ngày tạo'
        });
        _.each(this.data, (data) => {
            dataexport.push(_.pick(data, 'name', 'phone', 'email', 'backend_user_full_name', 'c_created_date'));
        });
        new Angular2Csv(dataexport, 'My Report');
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }

}
