import { Component, AfterViewInit } from '@angular/core';
declare var $: any;
@Component({
    selector: 'crm-customer',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['crm-customer.component.scss']
})
export class CrmCustomerComponent implements AfterViewInit {
    public ngAfterViewInit() {
        $('.dropdown-toggle').dropdown();
    }

}
