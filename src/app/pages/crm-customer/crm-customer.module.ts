// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// This Module's Components
import { ApplicationHanaComponent } from './application-hana.component';
import { NavbarComponent } from '../../themes/navbar/navbar.component';
import { ColorPickerModule } from 'angular2-color-picker';
import { MdSliderModule, MdSelectModule, MdProgressBarModule, MdProgressSpinnerModule, MdRadioModule } from '@angular/material';
import { routing } from './crm-customer.routing';
import { NgaModule } from '../../themes/nga.module';
import { NotificationService } from '../../_services/notification.service';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { FacebookService } from '../../_services/facebook.service';
import { ApplicationService } from '../../_services/application.service';
import { ValidationService } from '../../_services/ValidationService';
import { ApplicationPaymentComponent } from "./application-payment/application-payment.component";
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { GenericTableModule } from '@angular-generic-table/core';

import { CrmCustomerComponent } from './crm-customer.component';
import { CrmCustomerListComponent } from './crm-customer-list/crm-customer-list.component';
import { CrmCustomerService } from '../../_services/crmcustomer.service';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule, TabsModule } from 'ng2-bootstrap';

@NgModule({
    imports: [
        routing,
        NgaModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule,
        MdSliderModule,
        MdSelectModule,
        GenericTableModule,
        Ng2TableModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        SlimLoadingBarModule.forRoot(),
        MdRadioModule,
        PaginationModule.forRoot(),
        TabsModule,
        DataTableModule,
        SharedModule
    ],
    declarations: [
        CrmCustomerComponent,
        CrmCustomerListComponent
    ],
    exports: [
        CrmCustomerComponent,
        SlimLoadingBarModule,
        MdSelectModule,
        GenericTableModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        MdRadioModule
    ],
    providers: [
        FacebookService,
        ApplicationService,
        ValidationService,
        NotificationService,
        CrmCustomerService
    ],
})
export class CrmCustomerModule {

}
