import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { CrmCustomerComponent } from './crm-customer.component';
import { CrmCustomerListComponent } from './crm-customer-list/crm-customer-list.component';

const routes: Routes = [
  {
    path: '',
    component: CrmCustomerComponent,
    children: [
      { path: 'customer-list', component: CrmCustomerListComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
