import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { ApplicationService } from '../../../_services/application.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../_services/user.service';


@Component({
  selector: 'backend-confirm',
  templateUrl: 'backend-confirm.component.html',
  styleUrls: ['backend-confirm.component.scss']
})
export class BackendConfirmComponent implements OnInit, OnDestroy {
  private appName: any;
  private complexForm: FormGroup;
  private currentUser: any;
  private confirmCode: any;
  constructor(private applicationService: ApplicationService, private userService: UserService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private cdRef: ChangeDetectorRef) {
    this.complexForm = fb.group({
      appName: [null, Validators.required]
    });
  }

  public ngOnInit() {
    // console_bk.log('BackendConfirmComponent');
    this.route.params.subscribe((params) => {
      this.confirmCode = params['code'];
    });
    this.getUserConfirmCode();
    // this.checkLoged();
  }

  public ngOnDestroy(): void {
    location.reload();
  }

  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    this.cdRef.detectChanges();
  }

  public getUserConfirmCode() {
    let body = { confirm_code: this.confirmCode };
    // console_bk.log('code : ' + this.confirmCode);
    this.userService.getBackendUserByConfirmCode(body)
      .then((user) => {
        if (user) {
          // console_bk.log('cap nhat ' + JSON.stringify(user));
          this.router.navigate(['/backend-users/update-user-info', this.confirmCode]);
        } else {
          // console_bk.log('Login ssss ' );
          localStorage.removeItem('currentUser');
          localStorage.removeItem('apphanausing');
          localStorage.removeItem('session_time');
          this.router.navigate(['/login-hana']);
        }
      });
  }

  public submitForm(value: any) {
    this.applicationService.createBackendPartner(value.appName, this.currentUser.id)
      .then((integrate) => {
        // console_bk.log('Fanpage ' + JSON.stringify(integrate));
        this.router.navigate(['/application-hana/application-config', integrate.apllication.partner_id]);
      });
  }
}
