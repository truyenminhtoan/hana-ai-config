import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { BackendUsersComponent } from './backend-users.component';
import { BackendConfirmComponent } from './backend-confirm/backend-confirm.component';
import { BackendUpdatePasswordComponent } from './backend-update-password/backend-update-password.component';
import { BackendConfirmRegisterComponent } from './backend-confirm-register/backend-confirm-register.component';


const routes: Routes = [
  {
    path: '',
    component: BackendUsersComponent,
    children: [
      { path: 'confirm/:code', component: BackendConfirmComponent },
      { path: 'confirm-register/:code', component: BackendConfirmRegisterComponent },
      { path: 'update-user-info/:code', component: BackendUpdatePasswordComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
