import { Component, ViewEncapsulation } from '@angular/core';
import { AfterViewInit, OnInit, ElementRef, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';


@Component({
    selector: 'backend-users',
    template: '<router-outlet></router-outlet>',
    // encapsulation: ViewEncapsulation.Native,
})
export class BackendUsersComponent  {
    
}

// import { Component, ViewEncapsulation} from '@angular/core';

// // import { NavbarComponent } from '../../themes/navbar/navbar.component';

// @Component({
//     selector: 'application-hana',
//     templateUrl: 'application-hana.component.html',
//     styleUrls: ['application-hana.component.scss']
// })

// export class BackendUsersComponent {

// }


