// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// This Module's Components

// import { NgaModule } from 'nga.module';
// import { SideBarComponent } from '../../themes/sidebar/sidebar.component';
import { NavbarComponent } from '../../themes/navbar/navbar.component';

import { routing } from './backend-users.routing';
import { NgaModule } from '../../themes/nga.module';

import { FacebookService, ValidationService, ApplicationService } from '../../_services';
import { BackendUsersComponent } from './backend-users.component';
import { BackendConfirmComponent } from './backend-confirm/backend-confirm.component';
import { BackendUpdatePasswordComponent } from './backend-update-password/backend-update-password.component';
import { BackendConfirmRegisterComponent } from './backend-confirm-register/backend-confirm-register.component';

@NgModule({
    imports: [
        routing, NgaModule, CommonModule, FormsModule, ReactiveFormsModule
    ],
    declarations: [
        BackendUsersComponent,
        BackendConfirmComponent,
        BackendUpdatePasswordComponent,
        BackendConfirmRegisterComponent
        // NavbarComponent,
    ],
    exports: [
        BackendUsersComponent,
    ],
    providers: [
      FacebookService,
      ApplicationService,
      ValidationService
    ],
})
export class BackendUsersModule {

}



