import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { ApplicationService } from '../../../_services/application.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../_services/user.service';

declare var $: any;

@Component({
  selector: 'backend-update-password',
  templateUrl: 'backend-update-password.component.html',
  styleUrls: ['backend-update-password.component.scss']
})
export class BackendUpdatePasswordComponent implements OnInit, OnDestroy {
  private appName: any;
  private complexForm: FormGroup;
  private currentUser: any;
  private partnerId: any;
  private confirmCode: any;
  private validatePass: any;
  private validatePasaString: any;
  private user: any;
  constructor(private applicationService: ApplicationService, private userService: UserService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private cdRef: ChangeDetectorRef) {
    this.complexForm = fb.group({
      fullname: [null, Validators.required],
      password: [null, Validators.required],
      passwordconfirm: [null, Validators.required]
    });
  }

  public ngOnInit() {
    this.validatePass = false;
    this.route.params.subscribe((params) => {
      this.confirmCode = params['code'];
    });
    this.getUserConfirmCode();
    // console_bk.log('BackendUpdatePasswordComponent ' + this.confirmCode);
    // this.checkLoged();
  }

  public ngOnDestroy(): void {
    location.reload();
  }

  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    this.cdRef.detectChanges();
  }

  public getUserConfirmCode() {
    let body = { confirm_code: this.confirmCode };
    // console_bk.log('code : ' + this.confirmCode);
    this.userService.getBackendUserByConfirmCode(body)
      .then((user) => {
        if (user) {
          // console_bk.log('cap nhat ' + JSON.stringify(user));
          this.user = user;
          this.cdRef.detectChanges();
        } else {
          // console_bk.log('Login ssss ' );
          localStorage.removeItem('currentUser');
          localStorage.removeItem('apphanausing');
          localStorage.removeItem('session_time');
          this.router.navigate(['/login-hana']);
        }
      });
  }
  public setValidate() {
    this.validatePass = false;
  }
  public submitForm(value: any) {
    if (value.fullname.trim() === '') {
      this.validatePass = true;
      this.validatePasaString = 'Vui lòng nhập Tên';
      return;
    }

    if (value.password.trim() === '') {
      this.validatePass = true;
      this.validatePasaString = 'Vui lòng nhập mật khẩu';
      return;
    }

    if (value.passwordconfirm.trim() === '') {
      this.validatePass = true;
      this.validatePasaString = 'Vui lòng nhập xác nhận mật khẩu';
      return;
    }

    if (value.password.trim() !== value.passwordconfirm.trim()) {
      this.validatePass = true;
      this.validatePasaString = 'Mật khẩu không trùng khớp';
      return;
    }
    let body = { id: this.user.id, full_name: value.fullname, password: value.password, status: 1, confirm_code: this.confirmCode };
    // console_bk.log('full_name : ' + this.confirmCode);
    this.userService.updateBackendUser(body)
      .then((user) => {
        if (user) {
          $('#exampleModalLong').modal('show');
        } else {
          this.router.navigate(['/login-hana']);
        }
      });
  }
}
