import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { ApplicationService } from '../../../_services/application.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../_services/user.service';


@Component({
  selector: 'backend-confirm-register',
  templateUrl: 'backend-confirm-register.component.html',
  styleUrls: ['backend-confirm-register.component.scss']
})
export class BackendConfirmRegisterComponent implements OnInit, OnDestroy {
  private username: any;
  private complexForm: FormGroup;
  private currentUser: any;
  private confirmCode: any;
  constructor(private applicationService: ApplicationService, private userService: UserService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private cdRef: ChangeDetectorRef) {

  }

  public ngOnInit() {
    // console_bk.log('BackendConfirmComponent');
    this.route.params.subscribe((params) => {
      this.confirmCode = params['code'];
    });
    this.getUserConfirmCode();
    // this.checkLoged();
  }

  public ngOnDestroy(): void {
    location.reload();
  }

  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    this.cdRef.detectChanges();
  }

  public getUserConfirmCode() {
    let body = { confirm_code: this.confirmCode };
    // console_bk.log('code : ' + this.confirmCode);
    this.userService.getBackendUserByConfirmCode(body)
      .then((user) => {
        if (user) {
          // console_bk.log('cap nhat ' + JSON.stringify(user));
          this.username = user.full_name;
          let bodyUpdate = { id: user.id, status: 1, confirm_code: this.confirmCode };
          // console_bk.log('full_name : ' + this.confirmCode);
          this.userService.updateBackendUser(bodyUpdate)
            .then((users) => {
            });
          /* if (localStorage.getItem('affiliate')) {
            // goi api affiliate
            let bodyAff = {
              name: user.full_name,
              email: user.email,
              ref: localStorage.getItem('affiliate'),
              phone: user.phone
            };
            this.userService.createAffiliateUser(bodyAff)
              .then((usersRes) => {
              });
          } */
        } else {
          // console_bk.log('Login ssss ' );
          localStorage.removeItem('currentUser');
          localStorage.removeItem('apphanausing');
          localStorage.removeItem('session_time');
          // this.router.navigate(['/login-hana']);
        }
      });
  }

}
