// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { LoginComponent } from './login.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './login.routing';
import { UserService } from '../../_services/user.service';
import { NotificationService } from '../../_services/notification.service';
// import { ConfigService } from '../../_services/config.service';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { MdProgressBarModule } from '@angular/material';
import { ApplicationService } from '../../_services/application.service';
import { NgaModule } from '../../themes/nga.module';


@NgModule({
    imports: [
        CommonModule,
        routing,
        NgaModule,
        FormsModule,
        ReactiveFormsModule,
        SlimLoadingBarModule.forRoot(),
        MdProgressBarModule
    ],
    declarations: [
        LoginComponent,
    ],
    providers: [UserService, NotificationService, ApplicationService],
    exports: [
        LoginComponent,
        SlimLoadingBarModule,
        MdProgressBarModule
    ]
})
export class LoginModule {

}
