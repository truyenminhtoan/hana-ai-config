import { Component, AfterViewInit, OnInit, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { UserModel } from '../../_models/user.model';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../_services/notification.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserLoginModel } from '../../_models/userlogin.model';
import { ConfigService } from '../../_services/config.service';
import { element } from 'protractor';
import { ApplicationService } from '../../_services/application.service';

declare var $: any;
declare var _: any;
declare var gapi: any;
declare const FB: any;

@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  // tslint:disable-next-line:max-line-length
  private hasErrorMsg: boolean;
  private submit = false;
  private username: string;
  private password: string;
  private errorMsg: any;
  private currentUser: any;
  private auth2: any;
  private userInfo: any = {};
  private complexForm: FormGroup;
  private color = 'red';
  private mode = 'indeterminate';
  private value = 50;
  private bufferValue = 75;
  private loadingProcess: boolean = false;
  constructor(private applicationService: ApplicationService, private userService: UserService, private route: ActivatedRoute, private router: Router,
    private fb: FormBuilder, private cdRef: ChangeDetectorRef, private serviceConfig: ConfigService,
    private zone: NgZone) {
    this.checkLoged();
    this.complexForm = fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }
  public ngAfterViewInit() {
    $.getScript('../../../assets/js/login.js');
    $.getScript('../../../assets/js/popup-hana.js');
    // $.Backbone.history.start();
    this.sigin();
  }

  public ngOnInit() {
    this.hasErrorMsg = false;
    // $.getScript('../../../assets/js/core/jquery.validate.min.js');
    // $('#loginFormValidation').validate();
    // FACEBOOK INIT
    FB.init({
      appId: this.serviceConfig.facebookConfig.client_id,
      // appId: '108207942999174',
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });

    this.route
      .queryParams
      .subscribe((params) => {
        let paramT = params['t'];
        let affiliate = params['ref'];
        if (affiliate) {
          localStorage.setItem('affiliate', affiliate);
        }
        if (paramT === 'advance') {
          localStorage.setItem('registerFrom', paramT);
        } else if (paramT === 'basic') {
          localStorage.setItem('registerFrom', paramT);
        } else if (paramT === 'special') {
          localStorage.setItem('registerFrom', paramT);
        }
      });
  }
  public ngOnDestroy() {
    // console_bk.log('ngOnDestroy login');
  }
  public submitForm(value: any) {
    this.loadingProcess = true;
    if (this.submit) {
      this.loadingProcess = false;
      return;
    }
    this.submit = true;
    let that = this;
    let user = new UserModel();
    user.username = value.username;
    user.password = value.password;
    if (!user.username) {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập email';
      this.cdRef.detectChanges();
      this.loadingProcess = false;
      this.submit = false;
      return;
    }
    if (!user.password) {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập mật khẩu';
      this.cdRef.detectChanges();
      this.loadingProcess = false;
      this.submit = false;
      return;
    }
    this.userService.loginWithSUsername(user)
      .then((usersRes) => {
        this.loadingProcess = false;
        this.submit = false;
        if (usersRes) {
          // localStorage.setItem('currentUser', JSON.stringify(usersRes));
          // console_bk.log('LOGIN WITH SOCIAL ' + JSON.stringify(usersRes));
          /*setTimeout(function () {
            that.router.navigate(['/application-hana/application-redirect']);
          }, 1000);*/
          localStorage.removeItem('accessToken');
          usersRes.login_type = 'email';
          localStorage.setItem('currentUser', JSON.stringify(usersRes));
          this.applicationService.getListPartnerByUsername(user.username)
            .then((apps) => {
              if (apps) {
                // console_bk.log('LIST AP length ' + apps.length);
                if (apps.length === 0) {
                  this.router.navigate(['/application-hana/application-create']);
                } else if (apps.length === 1) {
                  this.usingApp(apps, user.username, user.username);
                } else {
                  this.router.navigate(['/application-hana/application-list']);
                }
              }
            });
        } else {
          this.errorMsg = 'Tài khoản đăng nhập không đúng';
          this.hasErrorMsg = true;
        }
      });

  }
  public clearError() {
    this.hasErrorMsg = false;
    this.cdRef.detectChanges();
  }

  public getLogin(uname: string, pwd: string) {
    let actionOperate: Observable<UserModel>;
    let user = { username: uname, password: pwd };
    actionOperate = this.userService.getLogin(user);
    actionOperate.subscribe(
      (data) => {
        // data.is_lock = false;
        // localStorage.setItem('currentUser', JSON.stringify(data));
        this.router.navigate(['/pages', 'intents', 'intentList']);
        //  this.router.navigate(['/pages','entities','edit'], {queryParams: {id: entity_id}});
      },
      (err) => {
        // load event fail
      });
  }

  /**
   * LOGIN GOOGLE
   */
  public sigin() {
    let that = this;
    gapi.load('auth2', () => {
      that.auth2 = gapi.auth2.init({
        client_id: this.serviceConfig.googleConfig.client_id,
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      that.attachSignin(document.getElementById('my-signin-hana'));
    });
  }

  public attachSignin(element) {
    let that = this;
    this.zone.run(() => {
      that.getUserInfoGoogle(element);
    });
  }
  public getUserInfoGoogle(element) {
    let that = this;
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        this.loadingProcess = true;
        let profile = googleUser.getBasicProfile();
        this.userInfo.token = googleUser.getAuthResponse().id_token;
        this.userInfo.id = profile.getId();
        this.userInfo.name = profile.getName();
        this.userInfo.img = profile.getImageUrl();
        // console_bk.log('Token || ' + googleUser.getAuthResponse().id_token);
        // console_bk.log('ID: ' + profile.getId());
        // console_bk.log('Name: ' + profile.getName());
        // console_bk.log('Image URL: ' + profile.getImageUrl());
        // console_bk.log('Email: ' + profile.getEmail());
        localStorage.removeItem('accessToken');
        this.findOrAdd(profile.getEmail(), profile.getName(), profile.getImageUrl(), profile.getEmail(), googleUser.getAuthResponse().id_token, 'email');
      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  public signout() {
    // gapi.auth.signOut();
    let auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(() => {
      // console_bk.log('User signed out.');
    });
  }

  /**
   * FACEBOOK LOGIN
   */
  public onFacebookLoginClick() {
    this.loadingProcess = true;
    FB.login((response) => {
      if (response.authResponse) {
        FB.api('/me', { fields: 'id,name,email' }, (res) => {
          // console_bk.log('TOKE nnnnnnnnnnnnnnn : ' + response.authResponse.accessToken); // Do not send to your backend! Use an ID token instead.
          // console_bk.log('ID: ' + res.id); // Do not send to your backend! Use an ID token instead.
          // console_bk.log('Name: ' + res.name);
          // console_bk.log('Image URL: ' + 'https://graph.facebook.com/' + res.id + '/picture?width=100');
          // console_bk.log('Email: ' + res.email); // This is null if the 'email' scope is not present.
          localStorage.setItem('accessToken', response.authResponse.accessToken);
          this.findOrAdd(res.id, res.name, 'https://graph.facebook.com/' + res.id + '/picture?width=150&height=150', res.email, response.authResponse.accessToken, 'facebook');
          return;
        });
      }
    }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
  }

  // toantm: login or register social
  public findOrAdd(username: string, fullName: string, img: string, email: string, accessToken: string, source: string) {
    this.zone.run(() => {
      let user = new UserLoginModel();
      user.full_name = fullName;
      user.username = username;
      user.url_avatar = img;
      user.email = email ? email : '';
      user.access_token = accessToken ? accessToken : '';
      user.type_login = source;
      if (localStorage.getItem('registerFrom')) {
        user.register_from = localStorage.getItem('registerFrom');
      }
      if (localStorage.getItem('affiliate')) {
        user.affiliate = localStorage.getItem('affiliate');
      }
      // console_bk.log('TOKE nnnnnnnnnnnnnnn : ' + JSON.stringify(user));
      let that = this;
      this.userService.loginWithSocial(user)
        .then((usersRes) => {
          // console_bk.log('USER RRRRRR ' + JSON.stringify(usersRes));
          if (usersRes) {
            usersRes.login_type = source;
            localStorage.setItem('currentUser', JSON.stringify(usersRes));
            // console_bk.log('SETITEMMMM  ' + localStorage.getItem('currentUser'));
            this.applicationService.getListPartnerByUsername(username)
              .then((apps) => {
                if (apps) {
                  // console_bk.log('LIST AP length ' + apps.length + ' source ' + source);
                  if (apps.length === 0) {
                    if (source === 'facebook') {
                      this.router.navigate(['/application-hana/application-list']);
                    } else {
                      this.router.navigate(['/application-hana/application-create']);
                    }
                  } else if (apps.length === 1) {
                    usersRes.apps = apps;
                    localStorage.setItem('currentUser', JSON.stringify(usersRes));
                    this.usingApp(apps, fullName, username);
                  } else {
                    this.router.navigate(['/application-hana/application-list']);
                  }
                }
              });
            // return this.router.navigate(['/application-hana/application-redirect']);

            /* if ( usersRes.status === 200 ) {
              if (localStorage.getItem('affiliate')) {
                let bodyAff = {
                  name: fullName,
                  email: email,
                  ref: localStorage.getItem('affiliate'),
                };
                this.userService.createAffiliateUser(bodyAff)
                  .then((usersRes) => {
                  });
              }
            } */
          } else {
            this.hasErrorMsg = true;
            this.errorMsg = 'Thông tin đăng nhập không chính xác';
          }
        });
    });
  }

  private usingApp(app: any, fullname: any, username: any) {
    this.zone.run(() => {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        currentUser.version = app.version;
        currentUser.partner_id = app.id;
        // // console_bk.log('app:', app);
        currentUser.app_using = app[0];
        currentUser.apps = app;
        if (currentUser.role_id !== 1) {
          let user = app[0].backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
          if (user) {
            currentUser.role_id = user.role_id;
          }
        }
        localStorage.setItem('currentUser', JSON.stringify(currentUser));
        this.currentUser = currentUser;
        // console.log('USSSSSSSSSSSSSSS ' + JSON.stringify(this.currentUser));
      }
      this.getTryUsing();

      /* this.applicationService.getTokenByPartnerId(fullname, username, app.id, app.version)
        .then((apps) => {
          window.location.href = this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token;
        }); */
        // this.router.navigate(['/chat']);
        this.router.navigate(['/' + this.currentUser.app_using.id + '/messages']);
    });
  }

  private getTryUsing() {
    let integrations = this.currentUser.app_using.integrations;
    let fbInte = _.find(integrations, (inte) => {
        return inte.gatewayId === 1;
    });
    if (fbInte) {
        if (fbInte.isExpire === 0) {
          let body = {
            page_id: fbInte.pageId,
            partner_id: this.currentUser.partner_id
          }
          this.applicationService.getTryUsing(body)
          .then((apps) => {

              // console.log('paypay ' + JSON.stringify(apps));
              this.currentUser.payment = apps.data;
              this.currentUser.paymentstatus = false;
              localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          });
        }
    }
  }

  public loginUsername() {
  }

  private checkLoged() {
    // console_bk.log('CURRRENT USER' + localStorage.getItem('currentUser'));
    if (localStorage.getItem('currentUser') && localStorage.getItem('currentUser') !== 'undefined') {
      // this.router.navigate(['/application-hana/application-redirect']);
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.applicationService.getListPartnerByUsername(currentUser.username)
        .then((apps) => {
          if (apps) {
            // console_bk.log('LIST AP length ' + apps.length);
            if (apps.length === 0) {
              if (currentUser.login_type === 'facebook') {
                this.router.navigate(['/application-hana/application-list']);
              } else {
                this.router.navigate(['/application-hana/application-create']);
              }

            } else if (apps.length === 1) {
              this.usingApp(apps, currentUser.full_name, currentUser.username);
            } else {
              this.router.navigate(['/application-hana/application-list']);
            }
          }
        });
    } else {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('session_time');
    }
  }
  private checkExisted(uname: string, callback) {
    let actionOperate: Observable<UserModel>;
    actionOperate = this.userService.getUserByEmail(uname);
    actionOperate.subscribe(
      (data) => {
        // true: existed
        if (data) {
          return callback(data);
        }
        callback(0);
      },
      (err) => {
        callback(2);
      });
  }

  private guildLogin() {
    // // console_bk.log('CHAO BAN ');
    $('#loginGuideModal').modal('show');

    setTimeout(() => {
      $('.images-guide-app-list').bxSlider({
        mode: 'horizontal',
        moveSlides: 1,
        slideMargin: 40,
        infiniteLoop: true,
        // slideWidth: 900,
        minSlides: 1,
        maxSlides: 1,
        speed: 800,
      });
    }, 300);
  }
}
