import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
declare var $: any;
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from "../_services/compoent-interaction-service";

@Component({
  selector: 'pages',
  templateUrl: './pages.component.html',
  styleUrls: ['pages.component.scss']
})
export class PagesComponent implements OnInit, AfterViewInit {
  private is_load = true;
  private eventSubscription: Subscription;

  public ngOnInit() {
    $.getScript('../assets/js/init/initMenu.js');
    // $.getScript('../assets/js/demo.js');
  }
  public ngAfterViewInit() {
    $('.dropdown-toggle').dropdown();
    // $('body').removeClass('sidebar-mini');
    // console.log('APPENDDDDDDDDĐ');
    $('.condition-item1').dropdown();
  }
  // tslint:disable-next-line:member-ordering
  constructor(private location: Location, private _componentService: ComponentInteractionService) {
    this.location = location;
    this.eventSubscription = this._componentService.eventReceiver$.subscribe(
        event => {
          if (event == 'load-entities' || event == 'load-actions' || event == 'load-products' || event == 'load-intents') {
            var that = this;
            this.is_load = false;
            setTimeout(function () {
              that.is_load = true;
            }, 700);
          }
        }
    );

  }
  public isMap() {
    if (this.location.prepareExternalUrl(this.location.path()) === '#/maps/fullscreen') {
      return true;
    }
    else {
      return false;
    }
  }

  public close() {
    // $('.modal-hana').css('display', 'none');
  }
}
