import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {NotificationService} from "../../../../../../_services/notification.service";
import {BlocksService} from "../../../../../../_services/blocks.service";
import {Observable} from "rxjs/Observable";
declare var $: any;
declare var swal: any;


@Component({
    selector: 'add-menu-level-2-dialog',
    templateUrl: 'add-menu-level-2.components.html',
    styleUrls: ['add-menu-level-2.components.scss']
})
export class AddMenuLevel2DialogComponent {

    @Input() menu_cur: any;

    private url_value = '';
    private title_value = '';
    private text_value = '';
    private  menu : any;

    private dropdownList = [];
    private selectedItems = [];

    private dropdownListGroups = [];
    private selectedItemsGroup = [];

    private dropdownListSequnces = [];
    private selectedItemsSequnces = [];

    private dropdownSettings = {};

    /** Menu dung de gan gia tri save */
    private  menu_save : any;

    /**text*/
    private selectedItemsGroupText = [];
    private selectedItemsSequncesText = [];

    /**text delete*/
    private selectedItemsGroupTextDelete = [];
    private selectedItemsSequncesTextDelete = [];

    /**group delete*/
    private selectedItemsGroupDelete = [];
    private selectedItemsSequncesDelete = [];

    constructor(private blocksService: BlocksService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < AddMenuLevel2DialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data != undefined && this.data.menu_add != undefined) {
            this.menu = this.data.menu_add;
        }
        this.loadConfigs();
    }

    public okClick() {
        this.returnData();
        if (!this.checkMenuValidate(this.menu_save.menuLevel2)) {
            this.notificationService.showDanger("Menu phải nhập tiêu đề và nội dung của 1 trong 3 dạng tetx/ blocks/ url");
        } else {
            if (!this.checkUrl(this.menu_save.menuLevel2)) {
                this.notificationService.showDanger("Giá trị url không hợp lệ");
            } else {
                this.dialogRef.close(this.menu_save);
            }
        }
    }

    public checkUrl(menu) {
        let check = true;
        if (menu.type == 'web_url') {
            if (!this.isValidURL(menu.value.trim())) {
                check = false;
            }
        }

        return check;
    }

    isValidURL(url){
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if(RegExp.test(url)){
            return true;
        }else{
            return false;
        }
    }


    public checkMenuValidate(menu) {
        var check = true;
        if (menu.title.trim().length== 0) {
            check = false;
            return check;
        }
        if (menu.type == 'web_url') {
            if (menu.value.trim().length == 0) {
                check = false;
            }
        } else {
            if (menu.type != 'nested') {
                let decodedString = atob(menu.value);
                decodedString = JSON.parse(decodedString);
                if (decodedString['postback_type'] == 'text') {
                    if (decodedString['content'].trim().length == 0) {
                        check = false;
                    }
                } else {
                    if (decodedString['block_ids'].length == 0) {
                        check = false;
                    }
                }
            }
        }

        return check;
    }

    checkExists(item, lists) {
        for (let i = 0; i < lists.length; i++) {
            if (lists[i].id == item.id) {
                return false;
            }
        }

        return true;
    }

    loadConfigs() {
        let that = this;
        if (this.data.blocks != undefined) {
            that.dropdownList = [];
            Object.keys(this.data.blocks).forEach(function(k) {
                let item_add = {
                    "id": that.data.blocks[k].id,
                    "name": that.data.blocks[k].name
                };
                if (that.checkExists(item_add, that.dropdownList) == true) {
                    that.dropdownList.push(item_add);
                }
            });
        }
        Object.keys(this.data.groups).forEach(function(k) {
            that.dropdownListGroups.push({
                "id": that.data.groups[k].id,
                "name": that.data.groups[k].name
            }, );
        });
        Object.keys(this.data.sequences).forEach(function(k) {
            that.dropdownListSequnces.push({
                "id": that.data.sequences[k].id,
                "name": that.data.sequences[k].name
            }, );
        });
        this.dropdownList.sort(function (a, b) {
            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select block name",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            maxHeight: 50
        };
    }

    updateTypeMenu(type) {
        this.menu.menuLevel2.type_custom = type;
        this.returnData();
    }


    onSelectedBlock(event) {
        this.returnData();
    }

    onAddGroup(event) {
        let that = this;
        if (event.display == event.value) {
            swal({
                title: 'Thêm nhóm',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroup.length; i++) {
                                if (that.selectedItemsGroup[i].value == event.display) {
                                    that.selectedItemsGroup[i].value = result_add_group.id;
                                    that.selectedItemsGroup[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroup = this.selectedItemsGroup.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    onAddGroupText(event) {
        let that = this;
        if (event.display == event.value) {
            swal({
                title: 'Thêm nhóm',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroupText.length; i++) {
                                if (that.selectedItemsGroupText[i].value == event.display) {
                                    that.selectedItemsGroupText[i].value = result_add_group.id;
                                    that.selectedItemsGroupText[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroupText = this.selectedItemsGroupText.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    returnData() {

        // trim data
        this.text_value = this.text_value.trim();
        this.title_value = this.title_value.trim();
        this.url_value = this.url_value.trim();

        let payload;
        switch (this.menu.menuLevel2.type_custom) {
            case 'text':
                let group_ids_text = _.pluck(this.selectedItemsGroupText, 'id');
                let sequence_ids_text = _.pluck(this.selectedItemsSequncesText, 'id');
                let group_ids_delete = _.pluck(this.selectedItemsGroupTextDelete, 'id');
                let sequence_ids_delete = _.pluck(this.selectedItemsSequncesTextDelete, 'id');
                payload = new Buffer(JSON.stringify({
                    "title": this.title_value,
                    "postback_type":"text",
                    "content": this.text_value,
                    "group_ids": group_ids_text,
                    "sequence_ids": sequence_ids_text,
                    "remove_group_ids": group_ids_delete,
                    "remove_sequence_ids": sequence_ids_delete,
                })).toString('base64');
                let menu_text = {
                    "app_id" : this.menu.app_id,
                    "menuLevel2" : {
                        "level1Id" : this.menu['menuLevel2'].level1Id,
                        "type" : "postback",
                        "title" : this.title_value,
                        "index" : this.menu.menuLevel2.index,
                        "value": payload
                    }
                };
                this.menu_save = menu_text;
                break;
            case 'block':
                let block_ids = _.pluck(this.selectedItems, 'id');
                let group_ids = _.pluck(this.selectedItemsGroup, 'id');
                let sequence_ids = _.pluck(this.selectedItemsSequnces, 'id');
                let group_ids_delete_block = _.pluck(this.selectedItemsGroupDelete, 'id');
                let sequence_ids_delete_block = _.pluck(this.selectedItemsSequncesDelete, 'id');
                payload = new Buffer(JSON.stringify({
                    "title": this.title_value,
                    "postback_type": "link_blocks",
                    "block_ids": block_ids,
                    "group_ids": group_ids,
                    "sequence_ids": sequence_ids,
                    "remove_group_ids": group_ids_delete_block,
                    "remove_sequence_ids": sequence_ids_delete_block,
                })).toString('base64');
                let menu_postback = {
                    "app_id" : this.menu.app_id,
                    "menuLevel2" : {
                        //"messengerProfileId" : this.menu.menuLevel2.messengerProfileId,
                        "level1Id" : this.menu['menuLevel2'].level1Id,
                        "type" : "postback",
                        "title" : this.title_value,
                        "index" : this.menu.menuLevel2.index,
                        "value": payload
                    }
                };
                this.menu_save = menu_postback;
                break;
            case 'web_url':
                let menu_web_url = {
                    "app_id" : this.menu.app_id,
                    "menuLevel2" : {
                        //"messengerProfileId" : this.menu.menuLevel2.messengerProfileId,
                        "level1Id" : this.menu['menuLevel2'].level1Id,
                        "type" : "web_url",
                        "title" : this.title_value,
                        "index" : this.menu.menuLevel2.index,
                        "value": this.url_value
                    }
                };
                this.menu_save = menu_web_url;
                break;
        }
    }

    public getStyleText(n) {
        if (n == 0) {
            return "rgba(183, 28, 28, 0.39)";
        } else {
            return "";
        }
    }

}