import { ApplicationService } from './../../../../_services/application.service';
import { AgentService } from './../../../../_services/agents.service';
import { Component, AfterViewInit, ViewEncapsulation, OnInit, ViewChildren, ViewChild, ElementRef, Inject, OnDestroy, HostListener } from '@angular/core';
import { IntentService } from '../../_services/intents.service';
import { Observable } from 'rxjs/Rx';
import { IntentModel, UserSayModel } from '../../_models/intents.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { MD_DIALOG_DATA, MdDialog } from '@angular/material';
declare var $: any;
declare var swal: any;
declare var Taggle: any; // https://sean.is/poppin/tags/
import * as _ from 'underscore';
import { ComfirmDeleteDialogComponent } from './components/comfirm-delete-dialog/comfirm-delete-dialog.component';
import { ConfigService } from '../../_services/config.service';
import { TrainingDontUnderstandDialogComponent } from './components/training-dont-understand-dialog/training-dont-understand-dialog.component';
import { NotificationService } from '../../../../_services/notification.service';
import { MenusService } from '../../../../_services/menus.service';
import { AddMenuDialogComponent } from "./components/add-menu/add-menu.components";
import { BlocksService } from "../../../../_services/blocks.service";
import { EditMenuDialogComponent } from "./components/edit-menu/edit-menu.components";
import { AddMenuLevel1DialogComponent } from "./components/add-menu-level-1/add-menu-level-1.components";
import { EditMenuLevel1DialogComponent } from "./components/edit-menu-level-1/edit-menu-level-1.components";
import { AddMenuLevel2DialogComponent } from "./components/add-menu-level-2/add-menu-level-2.components";
import { EditMenuLevel2DialogComponent } from "./components/edit-menu-level-2/edit-menu-level-2.components";
import {EmitterService} from "../../../../_services/emitter.service";

@Component({
    selector: 'setting-menus',
    templateUrl: 'setting-menus.component.html',
    styleUrls: ['setting-menus.component.scss', '../../../../../assets/plugins/taggle/taggle.min.css']
})

export class SettingMenusComponent implements OnInit, AfterViewInit {

    private app_id;
    private messengerProfileId;
    private menuRoots = [];
    private list_blocks = [];
    private list_sequences = [];
    private list_groups = [];

    private menuRootsisDefault = [];
    private menuRootNotDefault = [];

    private menuLevel1s = [];
    private menuLevel1sShowList = [];

    private menuLevel2s = [];
    private menuLevel2sShowList = [];

    private is_load_ding = false;

    private user_first_name = "{{user_first_name}}";
    private user_last_name = "{{user_last_name}}";
    private user_full_name = "{{user_full_name}}";
    private welcome_text = "";

    private is_user_setting = false;
    private is_loading_profile = false;

    constructor(
        private dialog: MdDialog,
        private elRef: ElementRef,
        private notificationService: NotificationService,
        private blockService: BlocksService,
        private menuService: MenusService,
        private route: ActivatedRoute,
        private router: Router,
        private applicationService: ApplicationService) {


        let that = this;
        setInterval(function () {
            /** Update sort menu root */
            let value_sort_new = $('#nestable-output-menu-level-0').val();
            if (that.list_sort_0 != value_sort_new) {
                that.list_sort_0 = $('#nestable-output-menu-level-0').val();
                let list_indexs = JSON.parse(value_sort_new);
                let list_sort = [];
                for (let i = 0; i < list_indexs.length; i++) {
                    //sectionvalue
                    list_sort.push(list_indexs[i].sectionvalue);
                }
                if (list_sort.length > 0) {
                    let body = { "app_id": that.app_id, "level": 0, "idItems": list_sort };
                    let actionOperate: Observable<any>;
                    actionOperate = that.menuService.updateIndexMenu(body);
                    actionOperate.subscribe(
                        (res) => {
                        },
                        (err) => {
                        });
                }
            }
            /** Update sort menu level 1 */
            let value_sort_new1 = $('#nestable-output-menu-level-1').val();
            if (that.list_sort_1 != value_sort_new1) {
                that.list_sort_1 = $('#nestable-output-menu-level-1').val();
                let list_indexs = JSON.parse(value_sort_new1);
                let list_sort = [];
                for (let i = 0; i < list_indexs.length; i++) {
                    //sectionvalue
                    list_sort.push(list_indexs[i].sectionvalue);
                }
                if (list_sort.length > 0) {
                    let body = { "app_id": that.app_id, "level": 1, "idItems": list_sort };
                    let actionOperate: Observable<any>;
                    actionOperate = that.menuService.updateIndexMenu(body);
                    actionOperate.subscribe(
                        (res) => {
                        },
                        (err) => {
                        });
                }
            }
            /** Update sort menu level 2 */
            let value_sort_new2 = $('#nestable-output-menu-level-2').val();
            if (that.list_sort_2 != value_sort_new2) {
                that.list_sort_2 = $('#nestable-output-menu-level-2').val();
                let list_indexs = JSON.parse(value_sort_new2);
                let list_sort = [];
                for (let i = 0; i < list_indexs.length; i++) {
                    //sectionvalue
                    list_sort.push(list_indexs[i].sectionvalue);
                }
                if (list_sort.length > 0) {
                    let body = { "app_id": that.app_id, "level": 2, "idItems": list_sort };
                    let actionOperate: Observable<any>;
                    actionOperate = that.menuService.updateIndexMenu(body);
                    actionOperate.subscribe(
                        (res) => {
                        },
                        (err) => {
                        });
                }
            }

        }, 2000);


        // EmitterService.get('RELOAD_DATA_BLOCK').subscribe((data: any) => {
        //     this.loadBlocks();
        //     this.getListMenuLevel0();
        //     console.log('xxxxxxxxxxxxxxxxxxxxxx');
        // });

    }


    public ngOnInit(): void {
        $.getScript('../../../../assets/js/plugins/jquery.nestable.js');
        $.getScript('../../../../assets/js/plugins/sort_custom.js');

        this.reqcheckFacebookIntegration();

        // get groups and sequence
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let app_id = currentUser.app_using.applications[0].app_id;
        let actionOperateGroup: Observable<any>;
        actionOperateGroup = this.blockService.listGroupByApp(app_id);
        actionOperateGroup.subscribe(
            (data_groups) => {
                if (data_groups != undefined && data_groups.length > 0) {
                    this.list_groups = data_groups;
                } else {
                    this.list_groups = [];
                }
                let actionOperateSequence: Observable<any>;
                actionOperateSequence = this.blockService.listSequencesByApp(app_id);
                actionOperateSequence.subscribe(
                    (sequences) => {
                        if (sequences != undefined && sequences.length > 0) {
                            this.list_sequences = sequences;
                        } else {
                            this.list_sequences = [];
                        }
                    });
            });
    }

    public getListGroup() {
        // get groups and sequence
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let app_id = currentUser.app_using.applications[0].app_id;
        let actionOperateGroup: Observable<any>;
        actionOperateGroup = this.blockService.listGroupByApp(app_id);
        actionOperateGroup.subscribe(
            (data_groups) => {
                if (data_groups != undefined && data_groups.length > 0) {
                    this.list_groups = data_groups;
                } else {
                    this.list_groups = [];
                }
                let actionOperateSequence: Observable<any>;
                actionOperateSequence = this.blockService.listSequencesByApp(app_id);
                actionOperateSequence.subscribe(
                    (sequences) => {
                        if (sequences != undefined && sequences.length > 0) {
                            this.list_sequences = sequences;
                        } else {
                            this.list_sequences = [];
                        }
                    });
            });
    }

    public loadBlocks() {
        // console.log('loadBlocks');
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.app_id = currentUser.app_using.applications[0].app_id;
            this.getListBlocks();
        }
    }

    // toantm1
    public reqcheckFacebookIntegration() {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            let appId = currentUser.app_using.applications[0].app_id;
            let isIntegrationFacebook = false;
            if (currentUser.app_using.integrations) {
                currentUser.app_using.integrations.filter((item) => {
                    if (item.gatewayId === 1 && item.pageId && item.pageId.trim().length > 0) {
                        isIntegrationFacebook = true;
                    }
                });
            }
            if (!isIntegrationFacebook) {
                this.router.navigate(['/remarketingv2/faq']);
                return;
            }
            this.checkIntegrateFacebook(appId);
        }
    }

    public checkIntegrateFacebook(appId: string) {
        this.applicationService.getOnlyIntegrationGateway(appId, 1)
            .then((integrate) => {
                if (integrate) {
                    let fanpageInfo = integrate;
                    if (fanpageInfo && fanpageInfo.pageId && (fanpageInfo.isExpire !== 1)) {
                        this.loadBlocks();
                    } else {
                        if (fanpageInfo.isExpire === 1 || fanpageInfo.isExpire === '1') {
                            this.router.navigate(['/remarketingv2/connect-integration'], { relativeTo: this.route });
                            return;
                        }
                    }
                }
            });
    }

    public getListBlocks() {
        this.is_load_ding = true;

        let that = this;
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let actionOperate: Observable<any>;
        actionOperate = this.blockService.getListGroupBlocks(currentUser.partner_id);
        actionOperate.subscribe(
            (data_groups) => {
                this.getMessengerProfile();
                this.getListMenuLevel0();
                if (data_groups != undefined && data_groups.length > 0) {
                    that.list_blocks = [];
                    //list_blocks
                    Object.keys(data_groups).forEach(function (k) {
                        Object.keys(data_groups[k].blocks).forEach(function (i) {
                            that.list_blocks.push(data_groups[k].blocks[i]);
                        });
                    });
                }
            },
            (err) => {
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh block");
            });
    }

    public ngAfterViewInit(): void {

    }

    getMessengerProfile() {
        this.is_loading_profile = true;
        let body: any = {};
        body.app_id = this.app_id;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.getMessengerProfile(body);
        actionOperate.subscribe(
            (result) => {
                if (result.messengerProfile != undefined) {
                    this.messengerProfileId = result.messengerProfile.id;
                    this.is_user_setting = true;
                } else {
                    this.is_user_setting = false;
                    this.is_loading_profile = false;
                    this.is_load_ding = false;
                    this.notificationService.showInfo("MessengerProfile is not found!");
                }
                if (result.messengerProfile != undefined) {
                    this.isOn = result.messengerProfile.isMenuAllowInput;
                    if (result.messengerProfile.greetingText != undefined) {
                        this.welcome_text = result.messengerProfile.greetingText;
                    } else {
                        //this.welcome_text = "Xin chào {{user_first_name}}";
                    }
                }
                this.is_loading_profile = false;
            },
            (err) => {
                this.is_loading_profile = false;
                console.log(err);
            });
    }

    public getListMenuLevel0() {
        let that = this;
        let body: any = {};
        body.app_id = this.app_id;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.getListMenuByAppId(body);
        actionOperate.subscribe(
            (result) => {
                if (result.code == 200 && result.menuRoots != undefined) {

                    this.is_load_ding = false;

                    this.menuRoots = [];
                    this.menuRootsisDefault = [];
                    this.menuRootNotDefault = [];
                    this.menuRoots = result.menuRoots;
                    for (let i = 0; i < this.menuRoots.length; i++) {
                        if (this.menuRoots[i].isDefault == true) {
                            let row = this.menuRoots[i];
                            row.display = row.value;
                            this.menuRootsisDefault.push(row);
                        } else {
                            let row = this.menuRoots[i];
                            if (this.menuRoots[i].value != undefined && this.isBase64(this.menuRoots[i].value) == true) {
                                let decodedString = Buffer.from(this.menuRoots[i].value, 'base64').toString('utf8');
                                decodedString = JSON.parse(decodedString);
                                let list_block_names = [];
                                if (decodedString['block_ids'] != undefined && decodedString['block_ids'].length > 0) {
                                    for (let z = 0; z < decodedString['block_ids'].length; z++) {
                                        for (let i = 0; i < this.list_blocks.length; i++) {
                                            if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                                list_block_names.push(this.list_blocks[i].name);
                                                break;
                                            }
                                        }
                                    }
                                    if (list_block_names.length > 0) {
                                        row.display = list_block_names.join(", ");
                                    } else {
                                        row.display = '';
                                    }
                                } else {
                                    row.display = row.display = decodedString['content'];
                                }
                                this.menuRootNotDefault.push(row);
                            } else {
                                row.display = row.value;
                                this.menuRootNotDefault.push(row);
                            }
                        }
                    }
                    this.updateMenuSortLevel0();

                } else {
                    this.menuRoots = [];
                }
            },
            (err) => {
                console.log(err);
            });
    }

    public isBase64(str) {
        try {
            return btoa(atob(str)) == str;
        } catch (err) {
            return false;
        }
    }


    showAddMenu() {
        if (this.app_id != undefined && this.app_id != null) {
            let menu_add = {
                "app_id": this.app_id,
                "menuRoot": {
                    "messengerProfileId": this.messengerProfileId,
                    "type": "postback",
                    "title": "",
                    "index": this.menuRootNotDefault.length,
                    "value": "",
                    "payload": "",
                    "type_custom": "text"
                }
            };
            let dialogRef = this.dialog.open(AddMenuDialogComponent, {
                width: '50%',
                data: {
                    'blocks': this.list_blocks,
                    'groups' : this.list_groups,
                    'sequences': this.list_sequences,
                    'list_menu_leve_0': this.menuRoots,
                    'menu_add': menu_add
                }
            });
            dialogRef.afterClosed().subscribe(menu_add => {
                if (menu_add) {
                    this.saveMenu(menu_add);
                }
            });
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    saveMenu(menu) {
        if (menu.menuRoot.messengerProfileId != undefined) {
            this.is_load_ding = true;
            let actionOperate: Observable<any>;
            actionOperate = this.menuService.add(menu);
            actionOperate.subscribe(
                (menu_add) => {
                    if (menu_add.menuRoot != undefined) {
                        this.getListMenuLevel0();
                        this.getListGroup();
                        this.notificationService.showSuccess("Thêm menu thành công!");
                    } else {
                        this.notificationService.showDanger("Thêm menu thất bại!");
                    }
                },
                (err) => {
                    this.notificationService.showDanger("Thêm menu thất bại:" + err);
                });
        } else {
            this.notificationService.showDanger("Thêm menu thất bại: messengerProfileId is not found!");
        }
    }

    updateMenu(menu) {
        this.is_load_ding = true;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.add(menu);
        actionOperate.subscribe(
            (menu_edit) => {
                if (menu_edit.menuRoot != undefined) {
                    this.getListMenuLevel0();
                    this.getListGroup();
                    this.notificationService.showSuccess("Cập nhật menu thành công!");
                } else {
                    this.notificationService.showDanger("Cập nhật menu thất bại!");
                }
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật menu thất bại:" + err);
            });
    }

    deleteMenu(level, id_menu) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.is_load_ding = true;
            let actionOperate: Observable<any>;
            actionOperate = this.menuService.delete({ "app_id": this.app_id, "level": level, "menuId": id_menu });
            actionOperate.subscribe(
                (menu_delete) => {
                    if (menu_delete.code != 200) {
                        this.notificationService.showDanger("Xóa menu thất bại:" + menu_delete.desc);
                    } else {
                        this.getListMenuLevel0();
                        if (this.menu_level_0_cur != undefined && this.menu_level_0_cur != null) {
                            this.getMenuLevel1ByRootId(this.menu_level_0_cur.id);
                        }
                        if (this.menu_level_1_cur != undefined && this.menu_level_1_cur != null) {
                            this.getMenuLevel2ByLevel1(this.menu_level_1_cur.id);
                        }
                        this.notificationService.showSuccess("Xóa menu thành công!");

                        if (id_menu == this.menu_level_0_cur.id) {
                            this.menu_level_0_cur = null;
                            this.menu_level_1_cur = null;
                        }
                        if (id_menu == this.menu_level_1_cur.id) {
                            this.menu_level_1_cur = null;
                        }
                    }

                },
                (err) => {
                    this.notificationService.showDanger("Xóa menu thất bại:" + err);
                });
        }, (dismiss) => {
            //console.log(dismiss);
        });
    }

    showEditMenu(id_menu) {
        let that = this;
        if (this.app_id != undefined && this.app_id != null) {
            let data_menu_edit = {
                "app_id": this.app_id,
                "menuRoot": {
                    "messengerProfileId": '',
                    "type": "",
                    "title": "",
                    "index": -1,
                    "id": "",
                    "value": "",
                    "payload": ""
                }
            };
            var is_check = 0;
            Object.keys(this.menuRoots).forEach(function (k) {
                if (that.menuRoots[k].id == id_menu) {
                    data_menu_edit['menuRoot'].messengerProfileId = that.menuRoots[k].messengerProfileId;
                    data_menu_edit['menuRoot'].type = that.menuRoots[k].type;
                    data_menu_edit['menuRoot'].title = that.menuRoots[k].title;
                    data_menu_edit['menuRoot'].index = that.menuRoots[k].index;
                    data_menu_edit['menuRoot'].id = that.menuRoots[k].id;
                    if (that.menuRoots[k].value != undefined) {
                        data_menu_edit['menuRoot']['value'] = that.menuRoots[k].value;
                    }
                    is_check = 1;
                }
            });
            if (is_check == 1) {
                data_menu_edit['menuRoot']['type_custom'] = data_menu_edit['menuRoot']['type'];
                // let danh sach blocSelected
                let blocSelected = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        for (let z = 0; z < decodedString['block_ids'].length; z++) {
                            for (let i = 0; i < this.list_blocks.length; i++) {
                                if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                    this.list_blocks[i].value = that.list_blocks[i].id;
                                    this.list_blocks[i].display = that.list_blocks[i].name;
                                    blocSelected.push(this.list_blocks[i]);
                                    break;
                                }
                            }
                        }
                        data_menu_edit['menuRoot']['type_custom'] = 'block';
                    } else {
                        data_menu_edit['menuRoot']['type_custom'] = 'text';
                    }
                }
                // get danh sach groupSelected
                let groupSelected = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelected.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelected = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelected.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // get danh sach groupSelected
                let groupSelectedDelete = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedDelete = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }


                // get danh sach groupSelected
                let groupSelectedText = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    console.log('decodedString', decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedText.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        console.log('groupSelectedText', groupSelectedText);
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedText = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedText.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // get danh sach groupSelected
                let groupSelectedTextDelete = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    console.log('decodedString', decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedTextDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        console.log('groupSelectedText', groupSelectedText);
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedTextDelete = [];
                if (data_menu_edit['menuRoot']['value'] != undefined && data_menu_edit['menuRoot']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuRoot']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedTextDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                let data_show = {
                    'blocks': this.list_blocks,
                    'groups' : this.list_groups,
                    'sequences': this.list_sequences,
                    'list_menu_leve_0': this.menuRoots,
                    'menu_edit': data_menu_edit,
                    'selected_blocks': blocSelected,
                    'selected_groups': groupSelected,
                    'selected_sequences': sequenceSelected,
                    'selected_groups_text': groupSelectedText,
                    'selected_sequences_text': sequenceSelectedText,
                    'selected_groups_text_delete': groupSelectedTextDelete,
                    'selected_sequences_text_delete': sequenceSelectedTextDelete,
                    'selected_groups_delete': groupSelectedDelete,
                    'selected_sequences_delete': sequenceSelectedDelete,
                };
                let dialogRef = this.dialog.open(EditMenuDialogComponent, {
                    width: '50%',
                    data: data_show
                });
                dialogRef.afterClosed().subscribe(menu_edit => {
                    if (menu_edit) {
                        this.updateMenu(menu_edit);
                    }
                });
            } else {
                this.notificationService.showDanger("Menu is not found !");
            }
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    getMenuLevel1ByRootId(rootId) {
        this.is_load_ding = true;
        let that = this;
        let body: any = { "app_id": this.app_id, "menu_root_id": rootId };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.getMenuLevel1ByRootId(body);
        actionOperate.subscribe(
            (result) => {
                this.menuLevel1sShowList = [];
                if (result.code == 200 && result.menuLevel1s != undefined) {
                    this.menuLevel1s = result.menuLevel1s;
                } else {
                    this.menuLevel1s = [];
                }
                for (let i = 0; i < this.menuLevel1s.length; i++) {
                    let row = this.menuLevel1s[i];
                    if (this.menuLevel1s[i]['type'] == 'web_url') {
                        row.display = row.value;
                    } else {
                        if (this.menuLevel1s[i]['type'] == 'nested') {
                            row.display = '';
                        } else {
                            let decodedString = Buffer.from(this.menuLevel1s[i].value, 'base64').toString('utf8');
                            decodedString = JSON.parse(decodedString);
                            let list_block_names = [];
                            if (decodedString['block_ids'] != undefined && decodedString['block_ids'].length > 0) {
                                for (let z = 0; z < decodedString['block_ids'].length; z++) {
                                    for (let i = 0; i < this.list_blocks.length; i++) {
                                        if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                            list_block_names.push(this.list_blocks[i].name);
                                            break;
                                        }
                                    }
                                }
                                if (list_block_names.length > 0) {
                                    row.display = list_block_names.join(", ");
                                } else {
                                    row.display = '';
                                }
                            } else {
                                row.display = row.display = decodedString['content'];
                            }
                        }
                    }

                    this.menuLevel1sShowList.push(row);
                }
                this.updateMenuSortLevel1();
                this.is_load_ding = false;
            },
            (err) => {
                this.is_load_ding = false;
                console.log(err);
            });
    }

    showAddMenuLevel1() {
        if (this.app_id != undefined && this.app_id != null) {
            if (this.menu_level_0_cur != undefined && this.menu_level_0_cur != null) {
                let menu_add = {
                    "app_id": this.app_id,
                    "menuLevel1": {
                        "rootId": this.menu_level_0_cur.id,
                        //"messengerProfileId" : this.messengerProfileId,
                        "type": "postback",
                        "title": "",
                        "index": this.menuLevel1s.length,
                        "value": "",
                        "payload": "",
                        "type_custom": "text"
                    }
                };
                let dialogRef = this.dialog.open(AddMenuLevel1DialogComponent, {
                    width: '50%',
                    data: {
                        'blocks': this.list_blocks,
                        'groups' : this.list_groups,
                        'sequences': this.list_sequences,
                        'list_menu_level_1': this.menuLevel1s,
                        'menu_add': menu_add
                    }
                });
                dialogRef.afterClosed().subscribe(menu_add => {
                    if (menu_add) {
                        this.saveMenuLevel1(menu_add);
                    }
                });
            } else {
                //this.notificationService.showDanger("Menu root is undefined !");
            }
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    showEditMenuLevel1(id_menu) {
        let that = this;
        if (this.app_id != undefined && this.app_id != null) {
            let data_menu_edit = {
                "app_id": this.app_id,
                "menuLevel1": {
                    //"messengerProfileId" : '',
                    "rootId": "",
                    "type": "",
                    "title": "",
                    "index": -1,
                    "id": "",
                    "value": "",
                    "payload": ""
                }
            };
            var is_check = 0;

            Object.keys(this.menuLevel1s).forEach(function (k) {
                if (that.menuLevel1s[k].id == id_menu) {
                    //data_menu_edit['menuLevel1'].messengerProfileId = that.menuRoots[k].messengerProfileId;
                    data_menu_edit['menuLevel1'].rootId = that.menuLevel1s[k].rootId;
                    data_menu_edit['menuLevel1'].type = that.menuLevel1s[k].type;
                    data_menu_edit['menuLevel1'].title = that.menuLevel1s[k].title;
                    data_menu_edit['menuLevel1'].index = that.menuLevel1s[k].index;
                    data_menu_edit['menuLevel1'].id = that.menuLevel1s[k].id;
                    if (that.menuLevel1s[k].value != undefined) {
                        data_menu_edit['menuLevel1']['value'] = that.menuLevel1s[k].value;
                    }
                    is_check = 1;
                }
            });
            if (is_check == 1) {
                data_menu_edit['menuLevel1']['type_custom'] = data_menu_edit['menuLevel1']['type'];
                // let danh sach blocSelected
                let blocSelected = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        for (let z = 0; z < decodedString['block_ids'].length; z++) {
                            for (let i = 0; i < this.list_blocks.length; i++) {
                                if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                    this.list_blocks[i].value = that.list_blocks[i].id;
                                    this.list_blocks[i].display = that.list_blocks[i].name;
                                    blocSelected.push(this.list_blocks[i]);
                                    break;
                                }
                            }
                        }
                        data_menu_edit['menuLevel1']['type_custom'] = 'block';
                    } else {
                        data_menu_edit['menuLevel1']['type_custom'] = 'text';
                    }
                }
                // get danh sach groupSelected
                let groupSelected = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelected.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelected = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelected.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }



                // get danh sach groupSelected
                let groupSelectedDelete = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedDelete = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // get danh sach groupSelected
                let groupSelectedText = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedText.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedText = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedText.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // get danh sach groupSelected
                let groupSelectedTextDelete = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedTextDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedTextDelete = [];
                if (data_menu_edit['menuLevel1']['value'] != undefined && data_menu_edit['menuLevel1']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel1']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedTextDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }



                let data_show = {
                    'blocks': this.list_blocks,
                    'groups' : this.list_groups,
                    'sequences': this.list_sequences,
                    'list_menu_leve_0': this.menuLevel1s,
                    'menu_edit': data_menu_edit,
                    'selected_blocks': blocSelected,
                    'selected_groups': groupSelected,
                    'selected_sequences': sequenceSelected,
                    'selected_groups_text': groupSelectedText,
                    'selected_sequences_text': sequenceSelectedText,
                    'selected_groups_delete': groupSelectedDelete,
                    'selected_sequences_delete': sequenceSelectedDelete,
                    'selected_groups_text_delete': groupSelectedTextDelete,
                    'selected_sequences_text_delete': sequenceSelectedTextDelete
                };
                let dialogRef = this.dialog.open(EditMenuLevel1DialogComponent, {
                    width: '50%',
                    data: data_show
                });
                dialogRef.afterClosed().subscribe(menu_edit => {
                    if (menu_edit) {
                        this.updateMenuLevel1(menu_edit);
                    }
                });
            } else {
                this.notificationService.showDanger("Menu is not found !");
            }
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    saveMenuLevel1(menu) {
        if (menu.menuLevel1.rootId != undefined) {
            this.is_load_ding = true;
            let actionOperate: Observable<any>;
            actionOperate = this.menuService.addMenuLevel1(menu);
            actionOperate.subscribe(
                (menu_add) => {
                    if (menu_add.menuLevel1 != undefined) {
                        this.getMenuLevel1ByRootId(menu.menuLevel1.rootId);
                        this.getListGroup();
                        this.notificationService.showSuccess("Thêm menu thành công!");
                    } else {
                        this.notificationService.showDanger("Thêm menu thất bại!");
                    }
                },
                (err) => {
                    this.notificationService.showDanger("Thêm menu thất bại:" + err);
                    this.is_load_ding = false;
                });
        } else {
            this.notificationService.showDanger("Thêm menu thất bại: rootId is not found!");
        }
    }

    updateMenuLevel1(menu) {
        this.is_load_ding = true;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.addMenuLevel1(menu);
        actionOperate.subscribe(
            (menu_edit) => {
                if (menu_edit.menuLevel1 != undefined) {
                    this.getMenuLevel1ByRootId(menu.menuLevel1.rootId);
                    this.getListGroup();
                    this.notificationService.showSuccess("Cập nhật menu thành công!");
                } else {
                    this.notificationService.showDanger("Cập nhật menu thất bại!");
                }
            },
            (err) => {
                this.is_load_ding = false;
                this.notificationService.showDanger("Cập nhật menu thất bại:" + err);
            });
    }

    private menu_level_0_cur: any;
    /** get edit menu level 1 from level 0 **/
    editSubmenuLevel1(id_menu_level_0) {
        let that = this;
        Object.keys(this.menuRoots).forEach(function (k) {
            if (that.menuRoots[k].id == id_menu_level_0) {
                that.menu_level_0_cur = that.menuRoots[k];
            }
        });
        this.getMenuLevel1ByRootId(id_menu_level_0);
    }

    hideEditLevel0() {
        this.menu_level_0_cur = null;
        this.menu_level_1_cur = null;
    }


    private menu_level_1_cur: any;
    /** get edit menu level 2 from level 1 **/
    editSubmenuLevel2(id_menu_level_1) {
        let that = this;
        Object.keys(this.menuLevel1s).forEach(function (k) {
            if (that.menuLevel1s[k].id == id_menu_level_1) {
                that.menu_level_1_cur = that.menuLevel1s[k];
            }
        });
        this.getMenuLevel2ByLevel1(id_menu_level_1);
    }

    getMenuLevel2ByLevel1(id_menu_level_1) {
        this.is_load_ding = true;
        let that = this;
        let body: any = { "app_id": this.app_id, "menu_level1_id": id_menu_level_1 };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.getMenuLevel2ByLevel1Id(body);
        actionOperate.subscribe(
            (result) => {
                this.menuLevel2sShowList = [];
                if (result.code == 200 && result.menuLevel2s != undefined) {
                    this.menuLevel2s = result.menuLevel2s;
                } else {
                    this.menuLevel2s = [];
                }
                for (let i = 0; i < this.menuLevel2s.length; i++) {
                    let row = this.menuLevel2s[i];
                    if (this.menuLevel2s[i]['type'] == 'web_url') {
                        row.display = row.value;
                    } else {
                        if (this.menuLevel2s[i]['type'] == 'nested') {
                            row.display = '';
                        } else {
                            let decodedString = Buffer.from(this.menuLevel2s[i].value, 'base64').toString('utf8');
                            decodedString = JSON.parse(decodedString);
                            let list_block_names = [];
                            if (decodedString['block_ids'] != undefined && decodedString['block_ids'].length > 0) {
                                for (let z = 0; z < decodedString['block_ids'].length; z++) {
                                    for (let i = 0; i < this.list_blocks.length; i++) {
                                        if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                            list_block_names.push(this.list_blocks[i].name);
                                            break;
                                        }
                                    }
                                }
                                if (list_block_names.length > 0) {
                                    row.display = list_block_names.join(", ");
                                } else {
                                    row.display = '';
                                }
                            } else {
                                row.display = row.display = decodedString['content'];
                            }
                        }
                    }
                    this.menuLevel2sShowList.push(row);
                }
                this.updateMenuSortLevel2();
                this.is_load_ding = false;
            },
            (err) => {
                console.log(err);
                this.is_load_ding = false;
            });
    }

    showAddMenuLevel2() {
        if (this.app_id != undefined && this.app_id != null) {
            if (this.menu_level_1_cur != undefined && this.menu_level_1_cur != null) {
                let menu_add = {
                    "app_id": this.app_id,
                    "menuLevel2": {
                        "level1Id": this.menu_level_1_cur.id,
                        //"messengerProfileId" : this.messengerProfileId,
                        "type": "postback",
                        "title": "",
                        "index": this.menuLevel2s.length,
                        "value": "",
                        "payload": "",
                        "type_custom": "text"
                    }
                };
                let dialogRef = this.dialog.open(AddMenuLevel2DialogComponent, {
                    width: '50%',
                    data: {
                        'blocks': this.list_blocks,
                        'groups' : this.list_groups,
                        'sequences': this.list_sequences,
                        'list_menu_level_2': this.menuLevel2s,
                        'menu_add': menu_add
                    }
                });
                dialogRef.afterClosed().subscribe(menu_add => {
                    if (menu_add) {
                        this.saveMenuLevel2(menu_add);
                    }
                });
            } else {
                //this.notificationService.showDanger("Menu level1 Id undefined !");
            }
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    showEditMenuLevel2(id_menu) {
        let that = this;
        if (this.app_id != undefined && this.app_id != null) {
            let data_menu_edit = {
                "app_id": this.app_id,
                "menuLevel2": {
                    //"messengerProfileId" : '',
                    "level1Id": "",
                    "type": "",
                    "title": "",
                    "index": -1,
                    "id": "",
                    "value": "",
                    "payload": ""
                }
            };
            var is_check = 0;
            Object.keys(this.menuLevel2s).forEach(function (k) {
                if (that.menuLevel2s[k].id == id_menu) {
                    //data_menu_edit['menuLevel2'].messengerProfileId = that.menuRoots[k].messengerProfileId;
                    data_menu_edit['menuLevel2'].level1Id = that.menu_level_1_cur.id;
                    data_menu_edit['menuLevel2'].type = that.menuLevel2s[k].type;
                    data_menu_edit['menuLevel2'].title = that.menuLevel2s[k].title;
                    data_menu_edit['menuLevel2'].index = that.menuLevel2s[k].index;
                    data_menu_edit['menuLevel2'].id = that.menuLevel2s[k].id;
                    if (that.menuLevel2s[k].value != undefined) {
                        data_menu_edit['menuLevel2']['value'] = that.menuLevel2s[k].value;
                    }
                    is_check = 1;
                }
            });
            if (is_check == 1) {
                data_menu_edit['menuLevel2']['type_custom'] = data_menu_edit['menuLevel2']['type'];
                // let danh sach blocSelected
                let blocSelected = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        for (let z = 0; z < decodedString['block_ids'].length; z++) {
                            for (let i = 0; i < this.list_blocks.length; i++) {
                                if (this.list_blocks[i].id == decodedString['block_ids'][z]) {
                                    this.list_blocks[i].value = that.list_blocks[i].id;
                                    this.list_blocks[i].display = that.list_blocks[i].name;
                                    blocSelected.push(this.list_blocks[i]);
                                    break;
                                }
                            }
                        }
                        data_menu_edit['menuLevel2']['type_custom'] = 'block';
                    } else {
                        data_menu_edit['menuLevel2']['type_custom'] = 'text';
                    }
                }
                // get danh sach groupSelected
                let groupSelected = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelected.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelected = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelected.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }


                // get danh sach groupSelected
                let groupSelectedDelete = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedDelete = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'link_blocks') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }



                // get danh sach groupSelected
                let groupSelectedText = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedText.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedText = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedText.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // get danh sach groupSelected
                let groupSelectedTextDelete = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_group_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_group_ids'].length; z++) {
                                for (let i = 0; i < that.list_groups.length; i++) {
                                    if (that.list_groups[i].id == decodedString['remove_group_ids'][z]) {
                                        that.list_groups[i].value = that.list_groups[i].id;
                                        that.list_groups[i].display = that.list_groups[i].name;
                                        groupSelectedTextDelete.push(that.list_groups[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                // get danh sach sequenceSelected
                let sequenceSelectedTextDelete = [];
                if (data_menu_edit['menuLevel2']['value'] != undefined && data_menu_edit['menuLevel2']['type'] == 'postback') {
                    let decodedString = atob(data_menu_edit['menuLevel2']['value']);
                    decodedString = JSON.parse(decodedString);
                    if (decodedString['postback_type'] == 'text') {
                        if (decodedString['remove_sequence_ids'] != undefined) {
                            for (let z = 0; z < decodedString['remove_sequence_ids'].length; z++) {
                                for (let i = 0; i < that.list_sequences.length; i++) {
                                    if (that.list_sequences[i].id == decodedString['remove_sequence_ids'][z]) {
                                        that.list_sequences[i].value = that.list_sequences[i].id;
                                        that.list_sequences[i].display = that.list_sequences[i].name;
                                        sequenceSelectedTextDelete.push(that.list_sequences[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }


                let data_show = {
                    'blocks': this.list_blocks,
                    'groups' : this.list_groups,
                    'sequences': this.list_sequences,
                    'list_menu_level_2': this.menuLevel2s,
                    'menu_edit': data_menu_edit,
                    'selected_blocks': blocSelected,
                    'selected_groups': groupSelected,
                    'selected_sequences': sequenceSelected,
                    'selected_groups_text': groupSelectedText,
                    'selected_sequences_text': sequenceSelectedText,

                    'selected_groups_delete': groupSelectedDelete,
                    'selected_sequences_delete': sequenceSelectedDelete,
                    'selected_groups_text_delete': groupSelectedTextDelete,
                    'selected_sequences_text_delete': sequenceSelectedTextDelete
                };
                let dialogRef = this.dialog.open(EditMenuLevel2DialogComponent, {
                    width: '50%',
                    data: data_show
                });
                dialogRef.afterClosed().subscribe(menu_edit => {
                    if (menu_edit) {
                        this.updateMenuLevel2(menu_edit);
                    }
                });
            } else {
                this.notificationService.showDanger("Menu is not found !");
            }
        } else {
            this.notificationService.showDanger("app_id is undefined !");
        }
    }

    saveMenuLevel2(menu) {
        if (menu.menuLevel2.level1Id != undefined) {
            this.is_load_ding = true;
            let actionOperate: Observable<any>;
            actionOperate = this.menuService.addMenuLevel2(menu);
            actionOperate.subscribe(
                (menu_add) => {
                    if (menu_add.menuLevel2 != undefined) {
                        this.getMenuLevel2ByLevel1(menu.menuLevel2.level1Id);
                        this.getListGroup();
                        this.notificationService.showSuccess("Thêm menu thành công!");
                    } else {
                        this.is_load_ding = false;
                        this.notificationService.showDanger("Thêm menu thất bại!");
                    }
                },
                (err) => {
                    this.notificationService.showDanger("Thêm menu thất bại:" + err);
                    this.is_load_ding = false;
                });
        } else {
            this.notificationService.showDanger("Thêm menu thất bại: Id menuLevel1 is not found!");
        }
    }

    updateMenuLevel2(menu) {
        this.is_load_ding = true;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.addMenuLevel2(menu);
        actionOperate.subscribe(
            (menu_edit) => {
                if (menu_edit.menuLevel2 != undefined) {
                    this.getMenuLevel2ByLevel1(menu.menuLevel2.level1Id);
                    this.getListGroup();
                    this.notificationService.showSuccess("Cập nhật menu thành công!");
                } else {
                    this.notificationService.showDanger("Cập nhật menu thất bại!");
                    this.is_load_ding = false;
                }
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật menu thất bại:" + err);
                this.is_load_ding = false;
            });
    }

    hideEditLevel1() {
        this.menu_level_1_cur = null;
    }


    /** Update sort menu **/
    updateMenuSortLevel0() {
        let list_sort = [];
        this.menuRootNotDefault = this.menuRootNotDefault.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuRootNotDefault.length; i++) {
            this.menuRootNotDefault[i].index = i;
        }
        for (let i = 0; i < this.menuRootNotDefault.length; i++) {
            list_sort.push(this.menuRootNotDefault[i].id);
        }
        let body = { "app_id": this.app_id, "level": 0, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
                // if (res.code == 200) {
                //     this.notificationService.showSuccess("Cập nhật index menu thành công!");
                // } else {
                //     this.notificationService.showSuccess("Cập nhật index menu thất bại:" + res.desc);
                // }
                // this.is_load_ding = false;
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật index menu thất bại:" + err);
                //this.is_load_ding = false;
            });
    }

    updateMenuSortLevel1() {
        let list_sort = [];
        this.menuLevel1sShowList = this.menuLevel1sShowList.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuLevel1sShowList.length; i++) {
            this.menuLevel1sShowList[i].index = i;
        }
        for (let i = 0; i < this.menuLevel1sShowList.length; i++) {
            list_sort.push(this.menuLevel1sShowList[i].id);
        }
        let body = { "app_id": this.app_id, "level": 1, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
                // if (res.code == 200) {
                //     this.notificationService.showSuccess("Cập nhật index menu thành công!");
                // } else {
                //     this.notificationService.showSuccess("Cập nhật index menu thất bại:" + res.desc);
                // }
                // this.is_load_ding = false;
            },
            (err) => {
                // this.notificationService.showDanger("Cập nhật index menu thất bại:" + err);
                // this.is_load_ding = false;
            });
    }

    updateMenuSortLevel2() {
        let list_sort = [];
        this.menuLevel2sShowList = this.menuLevel2sShowList.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuLevel2sShowList.length; i++) {
            this.menuLevel2sShowList[i].index = i;
        }
        for (let i = 0; i < this.menuLevel2sShowList.length; i++) {
            list_sort.push(this.menuLevel2sShowList[i].id);
        }
        let body = { "app_id": this.app_id, "level": 2, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
                // if (res.code == 200) {
                //     this.notificationService.showSuccess("Cập nhật index menu thành công!");
                // } else {
                //     this.notificationService.showSuccess("Cập nhật index menu thất bại:" + res.desc);
                // }
                // this.is_load_ding = false;
            },
            (err) => {
                // this.notificationService.showDanger("Cập nhật index menu thất bại:" + err);
                // this.is_load_ding = false;
            });
    }


    private isOn = false;
    changeIsOn(event) {
        this.isOn = !this.isOn;
        let body;
        if (this.isOn == true) {
            body = { "app_id": this.app_id, "is_allow": "true" };
        } else {
            body = { "app_id": this.app_id, "is_allow": "false" };
        }
        this.is_load_ding = true;
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateAllowUserInput(body);
        actionOperate.subscribe(
            (res) => {
                if (res.code == 200) {
                    this.notificationService.showSuccess("Cập nhật 'Cho phép người dùng nhập tin nhắn' thành công!");
                } else {
                    this.notificationService.showSuccess("Cập nhật 'Cho phép người dùng nhập tin nhắ' thất bại:" + res.desc);
                }
                this.is_load_ding = false;
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật 'Cho phép người dùng nhập tin nhắ' thất bại:" + err);
                this.is_load_ding = false;
            });
    }


    applyPersistentMenu() {
        this.is_load_ding = true;
        let body = { "app_id": this.app_id };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.applyPersistentMenu(body);
        actionOperate.subscribe(
            (res) => {
                if (res.code == 200) {
                    this.notificationService.showSuccess("Cập nhật 'Áp dụng Menu' thành công!");
                } else {
                    this.notificationService.showSuccess("Cập nhật 'Áp dụng Menu' thất bại:" + res.desc);
                }
                this.is_load_ding = false;
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật 'Áp dụng Menu' thất bại:" + err);
                this.is_load_ding = false;
            });
    }


    public createOrUpdateGreetingText() {
        // if (this.welcome_text.trim().length > 0) {
        this.is_load_ding = true;
        let body = { "app_id": this.app_id, "greeting": this.welcome_text.trim() };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.createOrUpdateGreetingText(body);
        actionOperate.subscribe(
            (res) => {
                if (res.code == 200) {
                    this.notificationService.showSuccess("Cập nhật 'Văn bản chào mừng' thành công!");
                } else {
                    this.notificationService.showSuccess("Cập nhật 'Văn bản chào mừng' thất bại:" + res.desc);
                }
                this.is_load_ding = false;
            },
            (err) => {
                this.notificationService.showDanger("Cập nhật 'Văn bản chào mừng' thất bại:" + err);
                this.is_load_ding = false;
            });
        // }
    }

    udateMenuSortLevel2Backgroud() {
        let list_sort = [];
        this.menuLevel2sShowList = this.menuLevel2sShowList.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuLevel2sShowList.length; i++) {
            this.menuLevel2sShowList[i].index = i;
        }
        for (let i = 0; i < this.menuLevel2sShowList.length; i++) {
            list_sort.push(this.menuLevel2sShowList[i].id);
        }
        let body = { "app_id": this.app_id, "level": 2, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
            },
            (err) => {
                this.is_load_ding = false;
            });
    }

    udateMenuSortLevel1Backgroud() {
        let list_sort = [];
        this.menuLevel1sShowList = this.menuLevel1sShowList.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuLevel1sShowList.length; i++) {
            this.menuLevel1sShowList[i].index = i;
        }
        for (let i = 0; i < this.menuLevel1sShowList.length; i++) {
            list_sort.push(this.menuLevel1sShowList[i].id);
        }
        let body = { "app_id": this.app_id, "level": 1, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
            },
            (err) => {
                this.is_load_ding = false;
            });
    }

    udateMenuSortLevel0Backgroud() {
        let list_sort = [];
        this.menuRootNotDefault = this.menuRootNotDefault.sort(function (a, b) {
            return parseInt(a.index) - parseInt(b.index);
        });
        for (let i = 0; i < this.menuRootNotDefault.length; i++) {
            this.menuRootNotDefault[i].index = i;
        }
        for (let i = 0; i < this.menuRootNotDefault.length; i++) {
            list_sort.push(this.menuRootNotDefault[i].id);
        }
        let body = { "app_id": this.app_id, "level": 0, "idItems": list_sort };
        let actionOperate: Observable<any>;
        actionOperate = this.menuService.updateIndexMenu(body);
        actionOperate.subscribe(
            (res) => {
            },
            (err) => {
                this.is_load_ding = false;
            });
    }

    updatMenuIndexAll() {
        // this.udateMenuSortLevel0Backgroud();
        // this.udateMenuSortLevel1Backgroud();
        // this.udateMenuSortLevel2Backgroud();
    }

    getDisplay(item_status) {
        if (item_status) {
            return 1;
        }

        return 0;
    }

    getDisplayBySize(lists) {
        if (lists.length > 0) {
            return 'block';
        }

        return 'none';
    }


    private list_sort_0 = [];
    private list_sort_1 = [];
    private list_sort_2 = [];


    sortMenus0(event) {
        alert(1);
        console.log('event', event);
        console.log('list_sort_0', this.list_sort_0);
    }

    showEditPoweredByMideas() {
        this.notificationService.showInfo('Bạn cần phải nâng cấp lên gói Cao Cấp hoặc Đặc Biệt để có thể Xoá "Tạo chatbot tại HANA.AI"');
    }

    getDisplaySize(lists) {
        if (lists.length > 0) {
            return 1;
        }

        return 0;
    }

    getNotDisplaySize(lists) {
        if (lists.length > 0) {
            return 0;
        }

        return 1;
    }
}
