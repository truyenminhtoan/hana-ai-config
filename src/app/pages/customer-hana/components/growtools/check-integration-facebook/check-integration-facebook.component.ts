import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'check-integration-facebook',
    templateUrl: 'check-integration-facebook.component.html',
    styleUrls: ['check-integration-facebook.component.scss']
})
export class CheckIntegrationFacebookComponent implements OnInit {
    private currentUser;
    public ngOnInit(): void {
        if (localStorage.getItem('currentUser')) {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
    }

}
