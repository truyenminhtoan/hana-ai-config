import { Log } from './../../../../../../_services/log.service';
import { Component, OnInit, Input } from '@angular/core';
import { EmitterService } from '../../../../../../_services/emitter.service';
import * as _ from 'underscore';
import {Block} from "../../../../../../_models/blocks.model";

@Component({
  selector: 'block-items',
  templateUrl: 'block-items.component.html',
  styleUrls: ['block-items.component.scss']
})
export class BlockItemsComponent implements OnInit {
  @Input() changeBlockId: string;
  @Input() outputBlockId: string;

  private blocks: Block;

  private width_value_list_card = 1400;

  // danh sach cac phan hoi
  private responseMessage = [];
  // check show quick_reply
  private show_quick_reply = true;
  private is_load = false;

  private cs_width = 0;

  constructor(private log: Log){}

  public ngOnInit(): void {
    EmitterService.get(this.changeBlockId).subscribe((data: any) => {
      this.log.info(data);
      this.is_load = true;
      this.blocks = data;
      if (this.blocks.content == undefined) {
        this.blocks.content = '';
      }
      // load build phan hoi
      this.loadBuild();
    });

    EmitterService.get('SAVE_LIST_CARD').subscribe((data: any) => {
      this.log.info('dang goi ham ave');
      this.save();
    });

  }

  public save() {
    // Update lai cau trc loai button web_url hoac postback
    var tmp_res = this.responseMessage;
    for (var k = 0; k < tmp_res.length; k++) {
      if (tmp_res[k].type == 4) {
        for (var z = 0; z < tmp_res[k].message.quick_replies.length; z++) {
          tmp_res[k].message.quick_replies[z].content_type = "text";
          tmp_res[k].message.quick_replies[z].payload = tmp_res[k].message.quick_replies[z].title;
        }
      }
    }
    for (var k = 0; k < tmp_res.length; k++) {
      // phan hoi text
      if (tmp_res[k].type == 2) {
        if (tmp_res[k].message.attachment.payload.elements[0].buttons.length > 0) {
          for (var z = 0; z < tmp_res[k].message.attachment.payload.elements[0].buttons.length; z ++) {
            if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].url != undefined && tmp_res[k].message.attachment.payload.elements[0].buttons[z].url.indexOf('http') != -1) {
              tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'web_url';
              delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload;
            } else {
              tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'postback';
              tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = tmp_res[k].message.attachment.payload.elements[0].buttons[z].title;
              delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].url;
            }
          }
        }
      }
    }
    this.log.info('this.blocks' + this.blocks);
    this.log.info('responseMessage' + JSON.stringify(tmp_res));
    if (this.blocks != undefined) {
      this.blocks.content = JSON.stringify(tmp_res);
    }
    this.output();

    // trim khoang trang trong value
    //
    // for (var k = 0; k < tmp_res.length; k++) {
    //     // phan hoi text
    //     if (tmp_res[k].type == 1) {
    //         tmp_res[k].message.text = tmp_res[k].message.text.trim();
    //     }
    //     if (tmp_res[k].type == 3) {
    //       tmp_res[k].message.attachment.payload.url = tmp_res[k].message.attachment.payload.url.trim();
    //     }
    //     if (tmp_res[k].type == 4) {
    //       if (tmp_res[k].message.quick_replies != undefined && tmp_res[k].message.quick_replies.length > 0) {
    //         for (var z = 0; z < tmp_res[k].message.quick_replies.length; z ++) {
    //           tmp_res[k].message.quick_replies[z].title = tmp_res[k].message.quick_replies[z].title.trim();
    //           tmp_res[k].message.quick_replies[z].payload = tmp_res[k].message.quick_replies[z].payload.trim();
    //         }
    //       }
    //       }
    //     }
    // }
  }

  public removeResponseMessage(obj) {
    this.responseMessage = this.responseMessage.filter((row) => {
      return row !== obj
    });
    this.checkShowQuickReply();
    this.save();
  }

  public removeCardItem(index, card) {
    this.responseMessage[index].message.attachment.payload.elements = this.responseMessage[index].message.attachment.payload.elements.filter((row) => {
      return (row != card)
    });
    // kiem tra xoa luon nhung  group nao khong con elements nao
    this.responseMessage = this.responseMessage.filter((row) => {
      return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
    });
    this.save();
  }

  public addCard(index) {
    this.responseMessage[index].message.attachment.payload.elements.push({image_url: '', title: '', subtitle: '', buttons: [{ type: 'postback', title: '', payload: '' }]});

    // width_value_list_card
    this.updateWidthOfListCard();
    this.save();
  }

  public updateWidthOfListCard() {
    var tmp = 0;
    for (var i = 0; i < this.responseMessage.length; i++) {
      if (this.responseMessage[i].type == 2) {
        if (this.responseMessage[i].message.attachment.payload.elements.length > tmp) {
          tmp = this.responseMessage[i].message.attachment.payload.elements.length;
        }
      }
    }
    if (tmp > 0) {
      this.width_value_list_card = (tmp*500 + 300);
      //this.log.info('this.width_value_list_card', this.width_value_list_card);
      this.cs_width = this.width_value_list_card;
    }

  }

  public addResponseMessageType(typeMes: number) {
    switch (typeMes) {
      case 1:
        // Phan hoi text
        this.responseMessage.push({ type: typeMes, message: { text: '' } });
        break;
      case 2:
        // Phan hoi card
        this.responseMessage.push({ type: typeMes, message: { attachment: {type: 'template', payload: {template_type: 'generic', elements: [{image_url: '', title: '', subtitle: '', buttons: [{ type: 'postback', title: '', payload: '' }]}]}} } });

        break;
      case 3:
        // Phan hoi hinh anh
        this.responseMessage.push({ type: typeMes, message: { attachment: {type: 'image', payload: {url:''}} } });
        break;
      case 4:
        // Phan hoi quick_replies
        //this.responseMessage.push({ type: typeMes, message: { quick_replies: [{content_type: 'text', title: '', payload: ''}], text : ''}});
        this.responseMessage.push({ type: typeMes, message: { quick_replies: [], text : ''}});

        break;
    }
    //sort phan hoi
    //- Quickreply lúc nào cũng dưới cùng
    //- Các loại phản hồi khác thì hiển thị theo thứ tự người dùng đã thêm text - image - card - text - ... - quickreply
    var tmp_responseMessage = [];
    for (var i = 0; i < this.responseMessage.length; i++) {
      if (this.responseMessage[i].type != 4) {
        tmp_responseMessage.push(this.responseMessage[i]);
      }
    }
    for (var i = 0; i < this.responseMessage.length; i++) {
      if (this.responseMessage[i].type == 4) {
        tmp_responseMessage.push(this.responseMessage[i]);
        break;
      }
    }
    this.responseMessage = tmp_responseMessage;
    this.checkShowQuickReply();
    this.save();
  }

  public addResponseTextInput(event, i, j) {
    // this.responseMessage[i].message.posibleTexts[j].content = event.target.value;
    // let us = this.responseMessage[i].message.posibleTexts.filter((item) =>
    //     _.isEmpty(item.content))[0];
    // if (!us) {
    //   this.responseMessage[i].message.posibleTexts.push({content: ''});
    // }
  }

  public checkShowQuickReply() {
    var check = false;
    if (this.responseMessage.length>0) {
      for (var i = 0; i < this.responseMessage.length; i++) {
        if (this.responseMessage[i].type == 4) {
          this.show_quick_reply = false;
          check = true;
          break;
        }
      }
      if (!check) {
        this.show_quick_reply = true;
      }
    } else {
      this.show_quick_reply = true;
    }
  }

  public removeResponseTextInput(event, i, j) {
    if (this.responseMessage[i].message.posibleTexts.length > 1) {
      this.responseMessage[i].message.posibleTexts = this.responseMessage[i].message.posibleTexts.filter((row) => {
        return row !==  this.responseMessage[i].message.posibleTexts[j]
      });
    }
  }

  public addResponseImagesInput(event, i) {
    this.responseMessage[i].message.attachment.payload.url = event.target.value;
    this.save();
  }

  public output() {
    EmitterService.get(this.outputBlockId).emit(this.blocks);
  }

  public loadBuild() {
    this.log.info('content' + this.blocks.content);
    this.responseMessage = [];
    this.responseMessage = JSON.parse(this.blocks.content);
    if (this.responseMessage.length === 0) {
      this.responseMessage.push({ type: 1, message: { text: '' } });
    }
    // kiem tra xoa luon nhung  group nao khong con elements nao
    this.responseMessage = this.responseMessage.filter((row) => {
      return row.type !== 2 || (row.type === 2 && row.message.attachment.payload.elements.length > 0)
    });
    // width_value_list_card
    this.updateWidthOfListCard();

    this.checkShowQuickReply();
  }

}
