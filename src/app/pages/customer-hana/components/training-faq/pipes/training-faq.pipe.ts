import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { StringFormat } from '../../../../../themes/validators/string.format';
@Pipe({
  name: 'pipeTraining',
  pure: false
})
export class TrainingFaqPipe implements PipeTransform {
  public transform(data: any[], searchTerm: string): any[] {
    searchTerm = StringFormat.formatText(searchTerm);
    if (searchTerm != null && searchTerm.length > 0) {
      return data.filter((item) => {
        let temp = item.usersays.filter((says) => {
          return StringFormat.formatText(says.content).indexOf(searchTerm) !== -1
        });
        let response = '';
        if (item.response_messages && item.response_messages[0]) {
          response = item.response_messages[0].content;
        }
        return (temp.length > 0 || StringFormat.formatText(response).indexOf(searchTerm) !== -1);
      });
    } else {
      return data;
    }
  }
}
