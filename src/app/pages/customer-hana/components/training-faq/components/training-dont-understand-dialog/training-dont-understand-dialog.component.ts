import { Log } from './../../../../../../_services/log.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Rx';
import { NotificationService } from '../../../../../../_services/notification.service';
import { IntentService } from '../../../../../../_services/intents.service';
import { UUID } from 'angular2-uuid';
import { BlocksService, EmitterService } from '../../../../../../_services/index';
declare var $: any;
declare var swal: any;
@Component({
  selector: 'training-dont-understand-dialog',
  templateUrl: 'training-dont-understand-dialog.component.html',
  styleUrls: ['training-dont-understand-dialog.component.scss']
})
export class TrainingDontUnderstandDialogComponent implements OnInit {

  private agentId: any;
  private intendId: any;
  private usersays: any[] = [];
  private responseMessage: any[] = [];
  private loadingProcess = false;
  private currentIntent: any = {};
  private userSayMongoId: any;
  private usersayIdDeleteted: any;
  private blocks: any = [];
  private intents: any = [];
  private intentsTemp: any = [];
  private currentUser: any;
  private partnerId: any;
  private emitTraining: any;

  constructor( @Inject(MD_DIALOG_DATA) public data: any,
    private log: Log, private blocksService: BlocksService,
    public dialogRef: MdDialogRef<TrainingDontUnderstandDialogComponent>, public notificationService: NotificationService, public intentService: IntentService) { }

  public ngOnInit(): void {
    if (this.data.usersays) {
      this.currentIntent.is_new = true;
      this.usersays = [this.data.usersays];
      this.userSayMongoId = this.data.usersays.id;
      this.emitTraining = this.data.emitTraining;
    }
    this.agentId = this.data.agentId;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser.app_using) {
      // // console_bk.log('CURRENTUSER ' + JSON.stringify(this.currentUser.app_using));
      this.partnerId = this.currentUser.app_using.id;
    }
    // console.log('CUSTOMER INF OBB ' + JSON.stringify(this.data.usersays.content));
    this.addNewItem((cb) => {
      this.getListBlock((cb1) => {
        this.onAfterAddUsersay(this.data.usersays.content, 0);
        this.updateTrainedUsersay(this.userSayMongoId, (cb2) => {
          EmitterService.get(this.emitTraining).emit(this.userSayMongoId);
        });
      });
    });
  }

  public addNewItem(callback) {
    // console_bk.log('AddNew Item');
    let intentNew = { id: UUID.UUID(), name: UUID.UUID(), faq: 1, faq_type: 1, agent_id: this.agentId };

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(intentNew);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          this.dialogRef.close();
          return;
        }
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          this.dialogRef.close();
        } else {
          this.intentsTemp.unshift(intent);
          return callback(true);
        }
      },
      (err) => {
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public getListBlock(callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.getListGroupBlocks(this.partnerId);
    actionOperate.subscribe(
      (blocks) => {
        if (!blocks) {
          this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
          callback(false);
          return;
        }
        let block = [];
        blocks.filter((item) => {
          if (item.blocks) {
            block = _.union(block, item.blocks);
          }
        });
        let temp = [];
        block.filter((item) => {
          temp.push({ value: item.id, display: item.name });
        });
        this.blocks = temp;
        callback(true);
      },
      (err) => {
        callback(false);
        this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
      });
  }

  public onAfterAddUsersay(tag, index) {

    let intentObject = this.intentsTemp[index];
    let usersayInsert: any = { id: UUID.UUID(), intent_id: intentObject.id, content: tag, not_update_mongo: true };
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addUsersayIntoIntent(usersayInsert);
    actionOperate.subscribe(
      (usay) => {
        if (!intentObject.usersays) {
          intentObject.usersays = [];
        }
        intentObject.usersays.push(usay);
        let ids = _.pluck(intentObject.usersays, 'id');
        this.updateFirstUsersay(ids);
        this.intents = this.intentsTemp;
      },
      (err) => {
        this.notificationService.showDanger('Có lỗi xảy ra khi thêm câu nói khách hàng');
      });
  }

  public update(intent) {
    if (this.loadingProcess) {
      return;
    }
    let intentNew: any = this.currentIntent;
    if (_.isEmpty(intent.usersays)) {
      this.notificationService.showDanger('Vui lòng nhập câu nói khách hàng...');
      return;
    }
    let checkResponse = JSON.parse(intent.response)[0].message.posibleTexts;
    if (_.isEmpty(checkResponse)) {
      this.notificationService.showDanger('Vui lòng nhập thông tin bot trả lời...');
      return;
    }

    intentNew.name = intent.usersays[0].content;
    intentNew.usersays = intent.usersays;
    intentNew.agent_id = this.agentId;
    intentNew.faq = 1;
    intentNew.faq_type = 1;
    intentNew.response_messages = [{ content: intent.response }];

    this.loadingProcess = true;
    if (intentNew.is_new) {
      delete intentNew.is_new;
      this.updateTrainedUsersay(this.userSayMongoId, (cb) => {
        if (cb) {
          this.addIntent(intentNew, (cb1) => {
            if (cb1) {
              this.currentIntent.id = cb1.id;
              this.usersayIdDeleteted = this.userSayMongoId;
              this.dialogRef.close(this.userSayMongoId);
            }
            this.loadingProcess = false;
            // window.location.reload();
          });
        }
      });
    } else {
      delete intentNew.is_new;
      this.updateIntent(intentNew, (cb) => {
        if (cb) {
          this.currentIntent.id = cb[0].id;
          this.usersayIdDeleteted = this.userSayMongoId;
          this.dialogRef.close(this.userSayMongoId);
        }
      });
    }
  }

  public close() {
    this.dialogRef.close(this.usersayIdDeleteted);
  }

  public deleteIntent1(body) {
    this.updateTrainedUsersay(this.userSayMongoId, (cb) => {
      this.dialogRef.close(this.userSayMongoId);
    });
  }

  public updateFirstUsersay(ids) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFirstUsersay(ids);
    actionOperate.subscribe(
      (usay) => {
      },
      (err) => {
      });
  }

  public deleteIntent(Id) {
    swal({
      title: 'Bạn chắc chắn muốn xóa câu nói này?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.updateIgnoreTrainUsersay(this.userSayMongoId, (cb) => {
        this.dialogRef.close(this.userSayMongoId);
      });
      // tslint:disable-next-line:no-empty
    }, (dismiss) => { });
  }

  public addIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(body);
    actionOperate.subscribe(
      (intent) => {
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          callback(false);
        } else {
          this.notificationService.showSuccess('Thêm thành công');
          callback(intent);
        }
      },
      (err) => {
        this.log.info(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public updateTrainedUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'trained' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        this.log.info(err);
        callback(false);
      });
  }

  public updateIgnoreTrainUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'ignoreTrain' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        this.log.info(err);
        callback(false);
      });
  }

  public updateIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(body);
    actionOperate.subscribe(
      (intent) => {
        this.loadingProcess = false;
        if (intent === 202) {
          this.notificationService.showWarning('Ý định đã tồn tại');
          callback(false);
          return;
        }
        this.notificationService.showSuccess('Cập nhật thành công');
        callback(intent);
      },
      (err) => {
        callback(false);
        this.log.info(err);
        this.notificationService.showDanger('Cập nhật thất bại');
        this.loadingProcess = false;
      });
  }
}
