import { LazyLoadEvent } from './../../../../../../../assets/plugins/primeng/components/common/lazyloadevent.d';
import { StringFormat } from './../../../../../../themes/validators/string.format';
import { Component, AfterViewInit, ViewEncapsulation, OnInit, ViewChildren, ViewChild, ElementRef, Inject, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { IntentModel, UserSayModel } from '../../../../_models/intents.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { MD_DIALOG_DATA, MdDialog, PageEvent } from '@angular/material';
declare var $: any;
declare var swal: any;
declare var Taggle: any; // https://sean.is/poppin/tags/
import * as _ from 'underscore';
import { ComfirmDeleteDialogComponent } from '../comfirm-delete-dialog/comfirm-delete-dialog.component';
import { ApplicationService } from '../../../../../../_services/application.service';
import { NotificationService } from '../../../../../../_services/notification.service';
import { IntentService } from '../../../../../../_services/intents.service';
import { ConfigService } from '../../../../../../_services/config.service';

@Component({
  selector: 'training-comment',
  templateUrl: 'training-comment.component.html',
  styleUrls: ['training-comment.component.scss']
})

export class TrainingCommentComponent implements OnInit, AfterViewInit {
  public intents: any = [];
  public intentCurrents: any = [];
  public intentFallback: any;
  @Input() public contextSelected: any;
  public currentUser;
  @Input() public agentId: any;
  public appId;
  public partnerId;
  public loadingProcess = false;
  private color = 'primary';
  private mode = 'indeterminate';
  private value = 50;
  private isSaving = false;
  // tslint:disable-next-line:member-ordering
  @ViewChild('inputSearch') public inputSearch: ElementRef;

  // MdPaginator Inputs
  private length = 0;
  private pageSize = 5;
  private pageIndex = 0;

  // docaohuynh
  /*  private getAppDetail() {
    }*/
  // tslint:disable-next-line:member-ordering
  // tslint:disable-next-line:max-line-length
  constructor(
    private dialog: MdDialog,
    private appService: ApplicationService,
    private configService: ConfigService,
    private notificationService: NotificationService,
    private intentService: IntentService,
    private route: ActivatedRoute,
    private router: Router) { }

  public paginate(event: LazyLoadEvent) {
    this.pageIndex = parseInt(event.first + '', 10);
    this.pageSize = parseInt(event.rows + '', 10);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    this.length = this.intents.length;
  }

  public paginateDel() {
    // this.loadCarsLazy2(pageIndex, event.rows);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    if (this.intentCurrents.length === 0) {
      this.pageIndex = this.pageIndex - this.pageSize;
      this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
      this.length = this.intents.length;
    }
    // if (this.intentCurrents.length === 0) {
    //   this.intentCurrents = this.intents.slice(this.pageIndex - this.pageSize, this.pageIndex);
    // }
  }

  public showSwal(type) {
    if (type === 'basic') {
      swal({
        title: "Here's a message!",
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      });
    } else if (type === 'warning-message-and-cancel') {
      swal({
        title: 'Bạn chắc chắn muốn xóa FAQ này?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Có',
        cancelButtonText: 'Không',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then(() => {
        swal({
          title: 'Xóa thành công!',
          text: '',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false
        });
      }, (dismiss) => {
      });
    }
  }

  public ngOnInit(): void {
    this.checkCurrentAppToken();
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser.app_using && this.currentUser.app_using.id) {
        this.partnerId = this.currentUser.app_using.id;
      }
    }
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (!this.agentId) {
      this.loadAgentAcive((agent) => {
        this.agentId = agent.agent_id;
        this.getListIntent(1, 'context_comment');
      });
    } else {
      this.getListIntent(1, 'context_comment');
    }
    if (this.inputSearch) {
      let eventInputObservable = Observable.fromEvent(this.inputSearch.nativeElement, 'keyup');
      eventInputObservable.subscribe();
    }
  }
  public ngAfterViewInit(): void {
    // this.loadPopup();

    // if (this.currentUser) {
    //   $('.version-' + this.currentUser.version).show();
    //   $('.role-' + this.currentUser.role_id).show();
    //   $('.navbar-daotao').css('border-bottom', '2px solid #e9604a');
    //   console.log(JSON.stringify(this.currentUser));
    // }
  }

  // docaohuynh
  public loadPopup() {
    this.appService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        this.appId = integrate.applications[0].app_id;
        let r = document.createElement('script');
        r.type = 'text/javascript';
        r.async = !0;
        r.src = this.configService.hostResource + '/hana/' + this.appId;
        let c = document.getElementsByTagName('script')[0];
        c.parentNode.insertBefore(r, c);
      });
  }

  public loadAgentAcive(callback) {
    this.appService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        callback(agent);
      });
  }

  public getListIntent(index, contextName) {
    if (this.loadingProcess) {
      return;
    }
    this.intents = [];
    this.intentFallback = undefined;
    this.loadingProcess = true;
    this.contextSelected = index;
    let body = { agent_id: this.agentId + '', context_name: '', faq_type: 2 };
    if (contextName) {
      body.context_name = contextName;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListFAQ(body);
    actionOperate.subscribe(
      (intents) => {
        // alert(JSON.stringify(intents));
        this.loadingProcess = false;
        intents.filter((item) => {
          if (item.type === 2) {
            this.intentFallback = item;
          }
        });
        if (!this.intentFallback) {
          let intentNew = { id: UUID.UUID(), is_new: true, name: 'default_comment_fallback', agent_id: this.agentId, type: 2, usersays: [], faq: 1, faq_type: 2, response_messages: [] };
          intentNew.response_messages = [{ content: JSON.stringify([{ type: 1, message: { posibleTexts: [] } }]) }];
          this.intentFallback = intentNew;
        }
        this.intents = _.reject(intents, (item) => { return item.type === 2 });
        if (this.intents) {
          this.intentCurrents = this.intents.slice(0, this.pageSize);
          this.length = this.intents.length;
        }
      },
      (err) => {
        this.loadingProcess = false;
        console.log(err);
        this.notificationService.showDanger('Tải dữ liệu thất bại, vui lòng tải lại...');
      });
  }

  public getListUsersayDontUnderstand(index, contextName) {
    if (this.loadingProcess) {
      return;
    }
    // this.loadingProcess = true;
    this.contextSelected = index;
  }

  public addIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(body);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          callback(false);
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          callback(false);
        } else {
          this.notificationService.showSuccess('Thêm thành công');
          callback(intent);
        }
      },
      (err) => {
        console.log(err);
        this.notificationService.showDanger('Thêm thất bại');
        callback(false);
      });
  }

  public updateTrainedUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'trained' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        console.log(err);
        callback(false);
      });
  }

  public deleteUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'ignoreTrain' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        console.log(err);
        callback(false);
      });
  }

  public updateIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(body);
    actionOperate.subscribe(
      (intent) => {
        this.loadingProcess = false;
        // alert(JSON.stringify(intents));
        // this.intents = intents;
        if (!intent) {
          this.notificationService.showDanger('Cập nhật thất bại');
          callback(false);
          return;
        }
        if (intent === 202) {
          this.notificationService.showWarning('Ý định đã tồn tại');
          callback(false);
          return;
        }
        this.notificationService.showSuccess('Cập nhật thành công');
        callback(intent);
      },
      (err) => {
        callback(false);
        console.log(err);
        this.notificationService.showDanger('Cập nhật thất bại');
        this.loadingProcess = false;
      });
  }

  public deleteIntent(index) {
    swal({
      title: 'Bạn chắc chắn muốn xóa FAQ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      if (this.contextSelected === 2) {
        let intent = this.intents[index];
        this.deleteUsersay(intent.mogo_id, (cb) => {
          this.intents.splice(index, 1);
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        });
      } else {
        // delete this.intents[index];
        let intent = this.intentCurrents[index];
        if (intent.is_new) {
          this.intents = this.intents.filter((it) => {
            return it.id !== intent.id;
          });
          this.length = this.intents.length;
          // this.intentCurrents.splice(index, 1);
          this.paginateDel();
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        } else {
          let actionOperate: Observable<any>;
          actionOperate = this.intentService.delete(intent.id);
          actionOperate.subscribe(
            (intents) => {
              // alert(JSON.stringify(intents));
              // this.intents = intents;
              let itent = this.intentCurrents[index];
              this.intents = this.intents.filter((it) => {
                return it.id !== itent.id;
              });
              this.length = this.intents.length;
              // this.intentCurrents.splice(index, 1);
              this.paginateDel();
              swal({
                title: 'Xóa thành công!',
                text: '',
                type: 'success',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
              });
            },
            (err) => {
              console.log(err);
            });
        }
      }
    }, (dismiss) => {
      // a
    });
  }

  public updateFallback(intent) {
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (this.loadingProcess) {
      return;
    }
    this.loadingProcess = true;
    intent.intent_contexts = [{
      contexts: {
        name: 'context_comment',
        name_text: 'context_comment',
        agent_id: this.agentId,
        status: 1
      },
      type: 0,
      status: 1,
      is_hidden: 1
    }];
    if (intent.is_new) {
      delete intent.is_new;
      this.addIntent(intent, (cb) => {
        if (cb) {
          this.intentFallback.id = cb.id;
        }
        this.loadingProcess = false;
      });
    } else {
      // delete intent.intent_contexts;
      this.updateIntent(intent, (cb) => {
        if (cb) {
          this.intentFallback.id = cb[0].id;
        }
        this.loadingProcess = false;
      });
    }
  }

  public update(intent) {
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (this.loadingProcess) {
      return;
    }
    let intentNew = this.intents[intent.index];
    if (_.isEmpty(intent.usersays)) {
      this.notificationService.showDanger('Vui lòng nhập câu nói khách hàng...');
      return;
    }
    let checkResponse = JSON.parse(intent.response)[0].message.posibleTexts;
    if (_.isEmpty(checkResponse)) {
      this.notificationService.showDanger('Vui lòng nhập thông tin bot trả lời...');
      return;
    }

    intentNew.name = intent.usersays[0].content;
    intentNew.usersays = intent.usersays;
    intentNew.agent_id = this.agentId;
    intentNew.is_just_understand_in_context = true;
    intentNew.response_messages = [{ content: intent.response }];
    intentNew.intent_entities = intent.intent_entities;
    if (this.contextSelected === 0 || this.contextSelected === 2) {
      intentNew.name = intentNew.name + '_inbox';
    }
    if (this.contextSelected === 1) {
      intentNew.intent_contexts = [{
        contexts: {
          name: 'context_comment',
          name_text: 'context_comment',
          agent_id: this.agentId,
          status: 1
        },
        type: 0,
        status: 1,
        is_hidden: 1
      }];
      intentNew.name = intentNew.name + '_comment';
    }
    this.loadingProcess = true;
    if (intentNew.is_new) {
      delete intentNew.is_new;
      if (intentNew.is_dont_understand) {
        this.updateTrainedUsersay(intentNew.mogo_id, (cb) => {
          if (cb) {
            delete intentNew.is_dont_understand;
            delete intentNew.mogo_id;
            this.addIntent(intentNew, (cb1) => {
              if (cb1) {
                this.intents[intent.index].id = cb1.id;
                delete this.intents[intent.index].is_dont_understand;
                delete this.intents[intent.index].mogo_id;
                delete this.intents[intent.index].is_new;
                this.intents.splice(intent.index, 1);
              }
              this.loadingProcess = false;
              // window.location.reload();
            });
          }
        });
      } else {
        this.addIntent(intentNew, (cb) => {
          if (cb) {
            this.intents[intent.index].id = cb.id;
            delete this.intents[intent.index].is_dont_understand;
            delete this.intents[intent.index].mogo_id;
            delete this.intents[intent.index].is_new;
          }
          this.loadingProcess = false;
        });
      }
    } else {
      delete intentNew.is_new;
      // delete intentNew.intent_contexts;
      this.updateIntent(intentNew, (cb) => {
        if (cb) {
          this.intents[intent.index].id = cb[0].id;
        }
      });
    }
  }

  // public addNewItem() {
  //   let intentNew = { id: UUID.UUID(), is_just_understand_in_context: true, is_new: true, usersays: [], faq: 1, response_messages: [] };
  //   this.intents.unshift(intentNew);
  // }

  public addNewItem() {
    this.isSaving = true;
    // let intentNew = { id: UUID.UUID(), is_new: true, usersays: [], faq: 1, response_messages: [] };
    // this.intents.unshift(intentNew);
    let intentNew = { id: UUID.UUID(), name: UUID.UUID(), is_just_understand_in_context: true, faq: 1, faq_type: 2, agent_id: this.agentId, intent_contexts: [] };

    intentNew.intent_contexts = [{
      contexts: {
        name: 'context_comment',
        name_text: 'context_comment',
        agent_id: this.agentId,
        status: 1
      },
      type: 0,
      status: 1,
      is_hidden: 1
    }];

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(intentNew);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          // callback(false);
          this.isSaving = false;
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          this.isSaving = false;
          // callback(false);
        } else {
          // this.notificationService.showSuccess('Thêm thành công');
          // callback(intent);
          this.intents.unshift(intent);
          this.isSaving = false;

          if (this.intents) {
            this.intentCurrents = this.intents.slice(0, this.pageSize);
            this.length = this.intents.length;
            this.pageIndex = 0;
          }
        }
      },
      (err) => {
        // callback(false);
        this.isSaving = false;
        console.log(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public transform(searchTerm: string) {
    searchTerm = StringFormat.formatText(searchTerm);
    if (searchTerm != null && searchTerm.length > 0) {
      let intentTemp = this.intents.filter((item) => {
        let temp = [];
        if (item.usersays) {
          temp = item.usersays.filter((says) => {
            return StringFormat.formatText(says.content).indexOf(searchTerm) !== -1
          });
        }
        let response = '';
        if (item.response_messages && item.response_messages[0]) {
          response = item.response_messages[0].content;
        }
        return (temp.length > 0 || StringFormat.formatText(response).indexOf(searchTerm) !== -1);
      });

      if (intentTemp) {
        this.intentCurrents = intentTemp.slice(0, this.pageSize);
        this.length = intentTemp.length;
        this.pageIndex = 0;
      }
    } else {
      if (this.intents) {
        this.intentCurrents = this.intents.slice(0, this.pageSize);
        this.length = this.intents.length;
        this.pageIndex = 0;
      }
    }
  }

  public checkCurrentAppToken() {
    if ($('#token') && $('#token').val()) {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if ($('#token').val().indexOf(currentUser.app_using.id) === -1) {
          location.reload();
        }
      }
    }
  }

}
