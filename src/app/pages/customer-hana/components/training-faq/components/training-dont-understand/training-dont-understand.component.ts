import { Log } from './../../../../../../_services/log.service';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { UUID } from 'angular2-uuid';
import * as _ from 'underscore';
import { MdDialog } from '@angular/material';
import { TrainingDontUnderstandDialogComponent } from '../training-dont-understand-dialog/training-dont-understand-dialog.component';
import { IntentService } from '../../../../../../_services/intents.service';
import { EmitterService } from '../../../../../../_services/index';
declare var $: any;
declare var swal: any;
@Component({
  selector: 'training-dont-understand',
  templateUrl: 'training-dont-understand.component.html',
  styleUrls: ['training-dont-understand.component.scss']
})
export class TrainingDontUnderstandComponent implements OnInit {
  // tslint:disable-next-line:member-access
  @Input() agentId: string;
  private loadingProcess = false;
  private errorMes = false;
  private msgSay = false;
  private intents: any[] = [];
  private availableCars: any[];
  private selectedCars: any[];
  private draggedCar: any;
  private searchKey: any = '';
  private emitTraining: 'EMIT_TRAINED_DONTKNOW';
  // @ViewChild('filterInput') inputSearch: ElementRef;
  // tslint:disable-next-line:member-access
  ngOnInit(): void {
    this.checkCurrentAppToken();
    // let eventInputObservable = Observable.fromEvent(this.inputSearch.nativeElement, 'keyup');
    // eventInputObservable.subscribe();
    this.selectedCars = [];
    this.availableCars = [];
    this.getListUsersayDontUnderstand();
    EmitterService.get(this.emitTraining).subscribe((data: any) => {
      this.removeTrained(data);
  });
  }

  // tslint:disable-next-line:member-ordering
  constructor(private intentService: IntentService, private dialog: MdDialog,
    private log: Log) { }

  public showSwal(type) {
    if (type === 'basic') {
      swal({
        title: "Here's a message!",
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      });
    } else if (type === 'warning-message-and-cancel') {
      swal({
        title: 'Bạn chắc chắn muốn xóa FAQ này?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Có',
        cancelButtonText: 'Không',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then(() => {
        swal({
          title: 'Xóa thành công!',
          text: '',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false
        });
      }, (dismiss) => {
      });
    }
  }

  private dragStart(event, car: any) {
    this.draggedCar = car;
  }

  private drop(event, index) {
    if (this.draggedCar) {
      this.addUsersay(this.draggedCar.id, this.intents[index].id, this.draggedCar.content);
      let draggedCarIndex = this.findIndex(this.draggedCar);
      this.intents[index].usersays = [...this.intents[index].usersays, this.draggedCar];
      this.availableCars = this.availableCars.filter((val, i) => i !== draggedCarIndex);
      this.draggedCar = null;
      // { id: item.id, content: item.content, status: 1, style: 'chip-red' };
    }
  }

  private dragEnd(event) {
    this.draggedCar = null;
  }

  private findIndex(car: any) {
    let index = -1;
    for (let i = 0; i < this.availableCars.length; i++) {
      if (car.id === this.availableCars[i].id) {
        index = i;
        break;
      }
    }
    return index;
  }

  private backToDontUnderstand(item, index) {
    this.removeUsersay(item.value.id, (cb) => {
      this.availableCars.push(item.value);
      let intent = this.intents[index];
      for (let i = 0; i < intent.usersays.length; i++) {
        if (intent.usersays[i].id === item.value.id) {
          this.intents[index].usersays.splice(i, 1);
          break;
        }
      }
    });
  }

  private getListIntentByUsersay(say) {
    this.loadingProcess = true;
    this.intents = [];
    this.errorMes = false;
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListIntentUsersay(say, this.agentId);
    actionOperate.subscribe(
      (intents) => {
        if (intents) {
          this.loadingProcess = false;
          if (_.isEmpty(intents)) {
            this.errorMes = true;
          } else {
            let intentList = intents;
            intentList.filter((item) => {
              if (item.usersays) {
                item.usersays.filter((saysItem) => {
                  saysItem.style = 'chip-none';
                });
              }
            });
            this.intents = intentList;
          }
        }
      },
      (err) => {
        this.loadingProcess = false;
        this.errorMes = true;
        this.log.info(err);
      });
  }

  private getListUsersayDontUnderstand() {
    this.msgSay = false;
    this.loadingProcess = true;
    let intentsTemp = [];
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListDontunderstand(this.agentId);
    actionOperate.subscribe(
      (says) => {
        this.loadingProcess = false;
        if (says) {
          let sayArr = [];
          says.filter((item) => {
            let say = { id: item.id, content: item.content, status: 1, style: 'chip-red' };
            sayArr.push(say);
          });
          this.availableCars = sayArr;
          if (_.isEmpty(says)) {
            this.msgSay = true;
          }
        }
      },
      (err) => {
        this.msgSay = true;
        this.loadingProcess = false;
        this.log.info(err);
      });
  }

  private addUsersay(Id, intentId, contentString) {
    this.loadingProcess = true;
    let options = { id: Id, intent_id: intentId, content: contentString };
    let intentsTemp = [];
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.addUsersayIntoIntent(options);
    actionOperate.subscribe(
      (says) => {
        this.loadingProcess = false;
        // callback(true);
      },
      (err) => {
        this.loadingProcess = false;
        this.log.info(err);
        // callback(false);
      });
  }

  private removeUsersay(Id, callback) {
    this.loadingProcess = true;
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.removeUsersayFromIntent(Id);
    actionOperate.subscribe(
      (says) => {
        this.loadingProcess = false;
        callback(true);
      },
      (err) => {
        this.loadingProcess = false;
        this.log.info(err);
        callback(false);
      });
  }

  private handleSearchUsersay(content) {
    this.searchKey = content;
    this.getListIntentByUsersay(this.searchKey);
  }

  private openDialogTraining(Id, data) {
    let dialogRef = this.dialog.open(TrainingDontUnderstandDialogComponent, {
      height: 'auto',
      width: '80%',
      data: {
        agentId: this.agentId,
        usersays: { id: Id, content: data, status: 1, is_new: true },
        emitTraining: this.emitTraining
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.availableCars = _.reject(this.availableCars, (item) => item.id === result);
      }
    });
  }

  private removeTrained(result) {
    this.availableCars = _.reject(this.availableCars, (item) => item.id === result);
  }

  private deleteIntent(Id, index) {
    swal({
      title: 'Bạn chắc chắn muốn xóa câu nói này?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.deleteUsersay(Id, (cb) => {
        this.availableCars.splice(index, 1);
        swal({
          title: 'Xóa thành công!',
          text: '',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false
        });
      });
    // tslint:disable-next-line:no-empty
    }, (dismiss) => {});
  }

  private deleteUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'deleted' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        this.log.info(err);
        callback(false);
      });
  }

  private checkCurrentAppToken() {
    if ($('#token') && $('#token').val()) {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if ($('#token').val().indexOf(currentUser.app_using.id) === -1) {
          location.reload();
        }
      }
    }
  }
}
