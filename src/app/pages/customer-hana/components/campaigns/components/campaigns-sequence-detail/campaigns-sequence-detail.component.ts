import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { NotificationService } from '../../../../../../_services/notification.service';
import { Observable } from 'rxjs/Rx';
import * as _ from 'underscore';
declare var $: any;
@Component({
  selector: 'campaigns-sequence-detail',
  templateUrl: 'campaigns-sequence-detail.component.html',
  styleUrls: ['campaigns-sequence-detail.component.scss']
})
export class CampaignsSequenceDetailComponent implements OnChanges{
  @Input() private campaignSequenceDetailId: string;
  @Input() private viewSelectedId: string;
  @Input() private appId: string;
  @Input() private campaign: any = { id: '', name: '' };
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  private templateBroadcast: any = {};
  private messageContent = '';
  private queries = [];
  private campaignName = 'hi';
  private searchTypeData;

  // block
  private changeBlockId: string = 'BLOCK_CHANGE_ID';
  private outputBlockId: string = 'BLOCK_OUTPUT_ID';
  private block = { block_group_id: 'default', id: 'default', name: 'BLOCK 1', content: '[{\"type\":1,\"message\":{\"text\":\"eeeeeeeee\"}}]', type: 'content' };
  private blockCurrent: any = {};

  private isTachTiep = false;

  constructor(private configService: ConfigService, private log: Log, private campaignService: CampaignService, private facebookService: FacebookService, private notificationService: NotificationService) { }

  public ngOnChanges() {
    $('.campaign-name').keydown(function (event) {
      if (event.keyCode === 13) {
        this.log.info('`Enter` key is not allowed.');
        event.preventDefault();
        return false;
      }
    });
    let temp = [];
    // EmitterService.get(this.campaignSequenceDetailId).subscribe((data: any) => {
    // alert(JSON.stringify(data))
    // this._clearCondition();
    // this.getSearchType(this.appId);
    // this.campaignName = data.name;
    // this.log.info('get emiit: ', JSON.stringify(data));
    this.block.content = '[]';
    this.blockCurrent = this.block;
    // this.campaign = data;
    // alert('new: ' + JSON.stringify(this.campaign));
    if (this.campaign.template_broadcast && this.campaign.template_broadcast.length === 0) {
      let body = { campaign_id: this.campaign.id, content: '' };
      EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
      this.addTemplateBroadcast(body);
    } else {
      if (this.campaign.template_broadcast) {
        this.templateBroadcast = this.campaign.template_broadcast[0];
        if (!this.templateBroadcast.content) {
          this.templateBroadcast.content = '';
        }

        // checkbox
        if (this.templateBroadcast.data_set_size && this.templateBroadcast.expand_days) {
          this.isTachTiep = true;
        }

        this.blockCurrent.content = this.templateBroadcast.content;
        EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
        // this._addCondition(data.customer_target);
        // alert('new: ' + JSON.stringify(this.templateBroadcast));
      }
    }
    // this.getTotalCustomer();
    // });
  }

  public campaignChange() {
    let body = { id: this.campaign.id, name: this.campaign.name };
    this.updateCampaign(body);
  }

  public _queryBuilder2() {
    let that = this;
    $('#myFilter').structFilter({
      highlight: true,
      buttonLabels: true,
      submitReady: true,
      fields: this.searchTypeData
    });

    this._addCondition(this.campaign.customer_target);

    $('#myFilter').on('change.search', function (event) {
      // do something
      let queries = [];
      let data = $('#myFilter').structFilter('val');
      // that.templateBroadcast.condition = JSON.stringify(data);
      // alert('update condition:' + JSON.stringify(that.templateBroadcast));
      // that.updateTemplateBroadcast();
      that.queries = data;

      // data.filter((item) => {
      //   item.value.label = StringFormat.latinize(item.value.label);
      //   this.log.info('item.value.label:', item.value.label);
      // });

      // alert(JSON.stringify(data));

      let body = { id: that.campaign.id, customer_target: JSON.stringify(data) };
      that.campaign.customer_target = JSON.stringify(data);
      that.updateCampaign(body);
      // alert('old:'+JSON.stringify(that.campaign));
    });
  }

  public _addCondition(condition) {
    let that = this;
    let arr = [];
    if (condition) {
      arr = JSON.parse(condition);
    }
    $('#myFilter').structFilter('val', arr);
  }

  public updateCampaign(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateCampaign(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public addTemplateBroadcast(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('them thanh cong');
        this.templateBroadcast = result;
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public updateTemplateBroadcast() {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateTemplateBroadcast(this.templateBroadcast);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat thanh cong');
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public send() {
    if (_.isEmpty(this.appId)) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng muốn gửi tin nhắn');
      return;
    }
    if (!this.templateBroadcast || !this.templateBroadcast.content_text || this.templateBroadcast.content_text.trim().length === 0) {
      this.notificationService.showDanger('Vui lòng nhập nội dung gửi tin nhắn');
      return;
    }
    if (this.totalRecords === 0) {
      this.notificationService.showDanger('Không có khách hàng thỏa điều kiện gửi');
      return;
    }
    let body: any = {};
    body.app_id = this.appId;
    body.template_broadcast_id = this.templateBroadcast.id;
    body.text_content = this.templateBroadcast.content_text;
    body.customer_filter = JSON.parse(this.campaign.customer_target);
    if (this.templateBroadcast.content) {
      body.structure_content = this.templateBroadcast.content;
    }
    //  else {
    //   body.structure_content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
    // }
    this.sendMessages(body);
    // alert(JSON.stringify(body));
  }

  public sendMessages(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.sendMessageStruct(body);
    actionOperate.subscribe(
      (cus) => {
        if (cus) {
          this.notificationService.showSuccess('Gửi tin nhắn đến khách hàng thành công');
        } else {
          this.notificationService.showDanger('Gửi tin nhắn đến khách hàng thất bại');
        }
      },
      (err) => {
        this.notificationService.showDanger('Gửi tin nhắn đến khách hàng thất bại');
        // this.log.info(err);
      });
  }

  public messageChange(event) {
    if (event.target.textContent.length >= 10) {
      event.preventDefault();
    }
  }

  public getTotalCustomer() {
    let options: any = {};
    options.app_id = this.appId;
    options.queries = [];
    options.queries.push({ field: { value: 'seq.campaign_id' }, operator: { value: '=' }, value: { value: this.campaign.id } });
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getTotalCustomer(options);
    actionOperate.subscribe(
      (result) => {
        this.totalRecords = result.count;
        this.totalNotFb = result.not_fb_count;
        this.totalFb = result.fb_count;
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public updateBlock(body) {
    try {
      // alert(JSON.stringify(body));
      let content = JSON.parse(body.content);
      if (this.templateBroadcast) {
        if (content.length === 0) {
          this.templateBroadcast.content = '';
        } else {
          this.templateBroadcast.content = body.content;
        }
        this.updateTemplateBroadcast();
      }
    } catch (error) {

    }
  }

  public clearTachtiep() {
    if (!this.isTachTiep) {
      this.templateBroadcast.data_set_size = null;
      this.templateBroadcast.expand_days = null;
      this.updateTemplateBroadcast();
    }
  }
}
