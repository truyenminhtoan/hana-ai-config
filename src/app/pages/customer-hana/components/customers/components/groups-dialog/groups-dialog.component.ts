import { Log } from './../../../../../../_services/log.service';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { NotificationService } from '../../../../../../_services/notification.service';
import * as _ from 'underscore';
declare var swal: any;
@Component({
  selector: 'groups-dialog',
  templateUrl: 'groups-dialog.component.html',
  styleUrls: ['groups-dialog.component.scss']
})
export class GroupsDialogComponent implements OnInit {

  private myControl = new FormControl();
  private options = [];
  private filteredOptions: Observable<any[]>;
  // private customers: any = [];
  private queries;
  private fullName;
  private actionType;
  private appId = '';
  private isCheckAll = false;
  private customerOutputs = [];
  private totalSelects = 0;

  public ngOnInit() {
    this.getListGroup('');
  }

  private filter(name: string): any[] {
    name = name.trim();
    name = name.toLowerCase();
    // return this.options.filter((option) => new RegExp(`^${name}`, 'gi').test(option.name.trim()));
    return this.options.filter((option) => option.name.toLowerCase().trim().indexOf(name) > -1);
  }

  private displayFn(user: any): string {
    return user ? user.name : user;
  }

  // tslint:disable-next-line:member-ordering
  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:member-ordering
  constructor( @Inject(MD_DIALOG_DATA) public data: any,
    private log: Log,
    public dialogRef: MdDialogRef<GroupsDialogComponent>, private facebookService: FacebookService, private notificationService: NotificationService) {
    // this.customers = this.data.customers;
    this.isCheckAll = this.data.is_check_all;
    this.customerOutputs = this.data.customer_outputs;
    this.queries = this.data.queries;
    this.fullName = this.data.full_name;
    this.actionType = this.data.action_type;
    this.appId = this.data.app_id;
    this.totalSelects = this.data.totalSelects;
  }

  private action() {
    let options: any = {};
    options.app_id = this.appId;
    // options.page_customer_ids = this.customers;
    options.queries = this.queries;
    if (this.isCheckAll && _.size(this.customerOutputs) === 0) {
      options.action = 'all';
    } else if
       (this.isCheckAll && _.size(this.customerOutputs) > 0) {
      options.action = 'unselected';
    } else if
         (!this.isCheckAll && _.size(this.customerOutputs) > 0) {
      options.action = 'selected';
    }
    options.customers = this.customerOutputs;
    options.full_name = this.fullName;
    options.group = { name: this.myControl.value.name ? this.myControl.value.name.trim() : this.myControl.value.trim() };
    if (this.actionType === 'add') {
      this.addListCustomerToGroup(options);
    }
    if (this.actionType === 'delete') {
      this.deleteListCustomerFromGroup(options);
    }
    this.dialogRef.close('');
  }

  private close() {
    this.log.info(this.myControl.value);
    this.dialogRef.close('');
  }

  private getListGroup(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getListGroup(this.appId);
    actionOperate.subscribe(
      (services) => {
        this.options = services;
        this.filteredOptions = this.myControl.valueChanges
          .startWith(null)
          .map((user) => user && typeof user === 'object' ? user.name : user)
          .map((name) => name ? this.filter(name) : this.options.slice());
      },
      (err) => {
        this.log.info(err);
      });
  }

  private addListCustomerToGroup(body) {
    let actionOperate: Observable<any>;
    body.has_fb_userchat = true;
    actionOperate = this.facebookService.addListCustomerToGroup(body);
    actionOperate.subscribe(
      (result) => {
        if (result) {
          this.showSwal('thêm', 'vào', body.group.name);
          // this.notificationService.showSuccess('Thêm khách hàng vào nhóm thành công');
        } else {
          this.notificationService.showDanger('Thêm khách hàng vào nhóm thất bại');
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

  private deleteListCustomerFromGroup(body) {
    let actionOperate: Observable<any>;
    body.has_fb_userchat = true;
    actionOperate = this.facebookService.deleteListCustomerFromGroup(body);
    actionOperate.subscribe(
      (result) => {
        if (result) {
          this.showSwal('xoá', 'từ', body.group.name);
          // this.notificationService.showSuccess('Xóa khách hàng khỏi nhóm thành công');
        } else {
          this.notificationService.showDanger('Xóa khách hàng khỏi nhóm thất bại');
        }
      },
      (err) => {
        this.log.info(err);
        this.notificationService.showDanger('Xóa khách hàng khỏi nhóm thất bại');
      });
  }

  private showSwal(text, ob, group) {
    let min = '1 phút';
    let max = '5 phút';
    if (this.totalSelects < 1000) {
      min = '1 phút';
      max = '5 phút';
    } else if (this.totalSelects < 10000) {
      min = '5 phút';
      max = '15 phút';
    } else if (this.totalSelects < 20000) {
      min = '5 phút';
      max = '1 giờ';
    } else {
      min = '15 phút';
      max = '3 giờ';
    }
    swal({
      title: 'Bạn vừa thực hiện ' + text + ' "' + this.totalSelects + '" khách hàng ' + ob + ' nhóm "' + group + '"',
      text: 'Thời gian đồng bộ nhanh nhất ' + min + ' và chậm nhất là ' + max,
      type: 'success',
      confirmButtonText: 'Đóng',
      confirmButtonClass: 'btn btn-success',
      buttonsStyling: false
    }).then(() => {
      //
    }, (dismiss) => {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        // console_bk.log('giữ nó');
      }
    });
  }
}
