import { IntentService } from './../../../../_services/intents.service';
import { Component, AfterViewInit, ViewEncapsulation, OnInit, ViewChildren, ViewChild, ElementRef, Inject, OnDestroy, HostListener } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { IntentModel, UserSayModel } from '../../_models/intents.model';
import { MD_DIALOG_DATA, MdDialog } from '@angular/material';
declare var $: any;
declare var swal: any;
declare var Taggle: any; // https://sean.is/poppin/tags/
import * as _ from 'underscore';
import { ConfigService } from '../../_services/config.service';
import { NotificationService } from "../../../../_services/notification.service";
import { CommentsService } from "../../../../_services/comment.service";
import { HashTag } from "../../../../_models/hash_tag.model";
import { UserModel } from "../../../../_models/user.model";
import { EmitterService } from "../../../../_services/emitter.service";
@Component({
    selector: 'comment',
    templateUrl: 'comments.component.html',
    styleUrls: ['comments.component.scss', '../../../../../assets/plugins/taggle/taggle.min.css']
})

export class CommentsComponent implements OnInit {

    /** danh sach comment */
    private listHashTags = [];
    private listHashTagDefaults = [];
    private currentUser: any;
    private tagCurrent;
    private value_filter;
    private id_loading = false;
    // toantm
    private partnerId;
    private intentCurrents = [];

    constructor(private dialog: MdDialog,
        private elRef: ElementRef,
        private commentService: CommentsService,
        private intentService: IntentService,
        private notificationService: NotificationService) {

        // setTimeout(function() {
        //     $('body').addClass('sidebar-mini');
        // }, 1000);


        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser && this.currentUser.app_using) {
            this.partnerId = this.currentUser.app_using.id;
        }
        this.getAllHashTag();
    }

    public ngOnInit(): void {

    }


    /** Add hagtag **/
    addHagtag() {
        try {
            if (this.currentUser.partner_id != undefined) {
                let name = 'Tag_' + (this.listHashTags.length + 1);
                let tag: HashTag = new HashTag();
                tag.name = name;
                tag.partner_id = this.currentUser.partner_id;
                this.addNewHashTag(tag);
            } else {
                this.notificationService.showDanger("partner_id undefined!");
            }
        } catch (error) {
            console.log(error);
        }
    }

    /** push new hashtag */
    public addNewHashTag(body) {
        let actionOperate: Observable<any>;
        actionOperate = this.commentService.addNew(body);
        actionOperate.subscribe(
            (result) => {
                if (result.status == 1) {
                    // reload list
                    this.getAllHashTag();
                }
            },
            (err) => {
                console.log(err);
            });
    }

    //** get all hash tag */
    public getAllHashTag() {
        let body = { "partner_id": this.currentUser.partner_id };
        this.getHashTagFallback(body);
    }


    /** get hashtag by object */
    public getHashTag(body) {
        let that = this;
        this.id_loading = true;
        let actionOperate: Observable<any>;
        actionOperate = this.commentService.find(body);
        actionOperate.subscribe(
            (result) => {
                if (result != undefined) {
                    Object.keys(result).forEach(function (k) {
                        that.listHashTags.push(result[k]);
                    });
                    // this.listHashTags = result;
                }
                this.id_loading = false;
            },
            (err) => {
                console.log(err);
                this.id_loading = false;
            });
    }

    /** get hashtag deault */
    public getHashTagFallback(body) {
        this.id_loading = true;
        let actionOperate: Observable<any>;
        actionOperate = this.commentService.getHashTagFallback(body);
        actionOperate.subscribe(
            (result) => {
                if (result != undefined) {
                    this.listHashTags = result;
                }
                this.getHashTag(body);
            },
            (err) => {
                console.log(err);
                this.id_loading = false;
            });
    }


    /** select tag */
    public tagSelected(tag_select) {
        this.tagCurrent = tag_select;
        // alert(JSON.stringify(tag_select))
        // EmitterService.get('SELECT_HASH_TAG').emit(this.tagCurrent);
        this.getListFAQByContextId(this.tagCurrent.context_id);
    }

    /** delete tag */
    public deleteTag(tag_delete) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.id_loading = true;
            let actionOperate: Observable<any>;
            actionOperate = this.commentService.delete(tag_delete.id, tag_delete.context_id);
            actionOperate.subscribe(
                (result) => {
                    this.listHashTags = this.listHashTags.filter((row) => {
                        return (
                            row.id != tag_delete.id
                        )
                    });
                    //this.notificationService.showSuccess("Xóa tag " + tag_delete.name + " thành công!");
                    this.id_loading = false;
                },
                (err) => {
                    this.id_loading = false;
                    console.log(err);
                });

        }, (dismiss) => {
            //console.log(dismiss);
        });
    }

    /** change name hash tag*/
    public tagNameChange() {
        if (this.tagCurrent.id && this.tagCurrent.name.trim().length > 0) {
            this.id_loading = true;
            let body = { "partner_id": 2095, "id": this.tagCurrent.id, "name": this.tagCurrent.name.trim() };
            let actionOperate: Observable<any>;
            actionOperate = this.commentService.update(body);
            actionOperate.subscribe(
                (result) => {
                    // update name
                    for (let i = 0; i < this.listHashTags.length; i++) {
                        if (this.listHashTags[i].id == this.tagCurrent.id) {
                            this.listHashTags[i].name == this.tagCurrent.name;
                        }
                    }
                    this.id_loading = false;
                },
                (err) => {
                    this.id_loading = false;
                    console.log(err);
                });
        }
    }

    /** get hashtag by object */
    public getListFAQByContextId(contextId) {
        let body: any = { partner_id: this.partnerId, context_id: contextId };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getListFAQByContextId(body);
        actionOperate.subscribe(
            (result) => {
                this.intentCurrents = result;
            },
            (err) => {
                console.log(err);
                this.id_loading = false;
            });
    }

    public deleteIntent(event){
        
    }

}
