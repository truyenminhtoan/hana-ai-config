import { Log } from './../../../../_services/log.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from './../../../../_services/application.service';
import { LazyLoadEvent } from './../../../../../assets/plugins/primeng/components/common/lazyloadevent.d';
import { NotificationService } from './../../../../_services/notification.service';
import { StringFormat } from './../../../../themes/validators/string.format';
import { Observable } from 'rxjs/Rx';
import { EmitterService } from './../../../../_services/emitter.service';
import { IntentService } from './../../../../_services/intents.service';
import { BlocksService } from './../../../../_services/blocks.service';
import { Block, BlockGroup } from './../../../../_models/blocks.model';
import { Component, OnInit, OnDestroy, ViewChildren, ElementRef, ChangeDetectorRef } from '@angular/core';
import { UUID } from 'angular2-uuid';
declare var $: any;
declare var swal: any;
import _ from 'underscore';
import { CommentsService } from '../../../../_services/index';
@Component({
  selector: 'hashtags',
  templateUrl: 'hashtags.component.html',
  styleUrls: ['hashtags.component.scss']
})
export class HashtagsComponent implements OnInit, OnDestroy {
  @ViewChildren('input') private inputs;
  private changeBlockId: string = 'BLOCK_CHANGE_ID';
  private outputBlockId: string = 'BLOCK_OUTPUT_ID';
  private blockCurrent: Block;
  private groupBlocks: BlockGroup[] = [];
  private partnerId;
  private agentId;

  // new
  private tagName = null;
  private tagSelected: any = { id: null, name: '' };
  private listHashTagData = [];
  private listHashTags = [];
  private listHashTagFallback = [];
  private intentCurrents = [];
  private intents = [];

  private isSaving = false;
  private loadingProcess = false;
  private intentFallback: any;

  // paging
  private length = 0;
  private pageSize = 5;
  private pageIndex = 0;
  private isDestroy: boolean;

  private commentContextSelected = false;

  constructor(
    private blocksService: BlocksService,
    private elRef: ElementRef,
    private commentService: CommentsService,
    private intentService: IntentService,
    private notificationService: NotificationService,
    private cdRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private log: Log,
    private applicationService: ApplicationService) {
    // hidden overflow of body
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
    this.reqcheckFacebookIntegration();
  }

  public ngOnInit(): void {
    // scroll left
    let h = $(window).height();
    $('.left-scroll').slimScroll({
      height: h - 60 + 'px'
    });
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser.app_using && currentUser.app_using.id) {
        this.partnerId = currentUser.app_using.id;
        this.getHashTagFallback();
        this.getHashTag();
        // load agent active;
        this.getAgentActive();
      }
    }
  }

  public callChange() {
    if (this.isDestroy) {
      return false;
    }
    this.cdRef.detectChanges();
  }
  public ngOnDestroy(): void {
    this.isDestroy = true;
    $('body').removeAttr('style');
  }

  public reqcheckFacebookIntegration() {
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      let appId = currentUser.app_using.applications[0].app_id;
      let isIntegrationFacebook = false;
      if (currentUser.app_using.integrations) {
        currentUser.app_using.integrations.filter((item) => {
          if (item.gatewayId === 1 && item.pageId && item.pageId.trim().length > 0) {
            isIntegrationFacebook = true;
          }
        });
      }
      if (!isIntegrationFacebook) {
        this.router.navigate(['/remarketingv2/faq']);
        return;
      }
      this.checkIntegrateFacebook(appId);
    }
  }
  public checkIntegrateFacebook(appId: string) {
    this.applicationService.getOnlyIntegrationGateway(appId, 1)
      .then((integrate) => {
        if (integrate) {
          let fanpageInfo = integrate;
          if (fanpageInfo && fanpageInfo.pageId && (fanpageInfo.isExpire !== 1)) {
            // this.loadBlocks();
          } else {
            if (fanpageInfo.isExpire === 1 || fanpageInfo.isExpire === '1') {
              this.router.navigate(['/remarketingv2/connect-integration'], { relativeTo: this.route });
              return;
            }
          }
        }
      });
  }

  // new
  public getHashTagFallback() {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.commentService.getHashTagFallback({ partner_id: this.partnerId });
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        this.listHashTagFallback = result;
      },
      (err) => {
        this.setUnLoading();
        this.log.info(err);
      });
  }

  public getHashTag() {
    this.setUnLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.commentService.find({ partner_id: this.partnerId });
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        this.listHashTagData = result;
        this.listHashTags = result;
      },
      (err) => {
        this.setUnLoading();
        this.log.info(err);
      });
  }

  /** get hashtag by object */
  public getListFAQByContextId(tag) {
    this.setLoading();
    let body: any = { partner_id: this.partnerId, context_id: tag.context_id };
    let that = this;
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListFAQByContextId(body);
    actionOperate.subscribe(
      (intents) => {
        this.tagSelected = tag;
        this.tagName = tag.name;
        this.intentCurrents = [];
        this.intents = _.reject(intents, (item) => { return item.type === 2; });
        if (this.intents) {
          this.intentCurrents = this.intents.slice(0, this.pageSize);
          this.length = this.intents.length;
        }
        this.setUnLoading();
      },
      (err) => {
        this.setUnLoading();
        this.log.info(err);
        this.intentCurrents = [];
      });
  }

  public getListFAQFallbackByContextId(tag) {
    this.setLoading();
    let body: any = { partner_id: this.partnerId, context_id: tag.context_id };
    let that = this;
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListFAQByContextId(body);
    actionOperate.subscribe(
      (intents) => {
        this.setUnLoading();
        this.intentFallback = null;
        this.tagSelected = tag;
        this.tagName = tag.name;
        intents.filter((item) => {
          if (item.type === 2) {
            this.intentFallback = item;
          }
        });
        if (!this.intentFallback) {
          // let intentNew: any = { id: UUID.UUID(), is_new: true, name: 'default_comment_fallback_' + new Date().getTime(), agent_id: this.agentId, type: 2, usersays: [], faq: 1, response_messages: [] };
          // intentNew.response_messages = [{ content: JSON.stringify([{ type: 1, message: { posibleTexts: [] } }]) }];
          // intentNew.intent_contexts = [{
          //   contexts: { id: this.tagSelected.context_id },
          //   type: 0,
          //   status: 1
          // }];
          // this.intentFallback = intentNew;
          this.addNewFallback();
        }
        // alert(JSON.stringify(this.intentFallback));
      },
      (err) => {
        this.setUnLoading();
        this.log.info(err);
      });
  }

  public changeHashTag() {
    if (!this.tagSelected.id) {
      this.notificationService.showDanger('Cập nhật thất bại. Vui lòng thực hiện lại.');
      return;
    }
    let name = this.tagName.toLowerCase();
    if (StringFormat.checkSpecialChar(name)) {
      this.notificationService.showDanger('Tên hashtag không chứa khoảng trắng, các ký tự đặc biệt');
      return;
    }

    this.loadingProcess = true;
    let actionOperate: Observable<any>;
    actionOperate = this.commentService.update({ partner_id: this.partnerId, id: this.tagSelected.id, name: this.tagName });
    actionOperate.subscribe(
      (result) => {
        if (result) {
          if (result === 202) {
            this.loadingProcess = false;
            this.notificationService.showDanger('Tên hashtag đã tồn tại. Vui lòng sử dụng tên khác.');
            return;
          }
          this.loadingProcess = false;
          if (this.tagSelected.type === 2) {
            let i = 0;
            this.listHashTagFallback.filter((item) => {
              if (item.id === this.tagSelected.id) {
                this.listHashTagFallback[i].name = this.tagName;
              }
              i++;
            });
          } else {
            let i = 0;
            this.listHashTags.filter((item) => {
              if (item.id === this.tagSelected.id) {
                this.listHashTags[i].name = this.tagName;
              }
              i++;
            });
          }
        } else {
          this.loadingProcess = false;
          this.notificationService.showDanger('Cập nhật thất bại. Vui lòng thực hiện lại.');
        }
      },
      (err) => {
        this.loadingProcess = false;
        this.log.info(err);
      });
  }

  public selectTag(tag) {
    if (this.loadingProcess || !tag) {
      return;
    }
    // this.tagSelected = tag;
    // this.tagName = tag.name;
    this.getListFAQFallbackByContextId(tag);
    this.getListFAQByContextId(tag);

    EmitterService.get('SELECT_TAG').emit([]);
  }
  public selectFallbackTag(tag) {
    this.getListFAQFallbackByContextId(tag);
  }

  public deleteTag(tag) {
    if (!tag.id || !tag.context_id) {
      this.notificationService.showDanger('Xoá thất bại. Vui lòng thực hiện lại.');
      return;
    }
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.loadingProcess = true;
      let actionOperate: Observable<any>;
      actionOperate = this.commentService.delete(tag.id, tag.context_id);
      actionOperate.subscribe(
        (result) => {
          if (result) {
            this.loadingProcess = false;
            let i = 0;
            this.listHashTags.filter((item) => {
              if (item.id === tag.id) {
                this.listHashTags.splice(i, 1);
              }
              i++;
            });
            this.tagSelected = { id: null, name: '' };
          } else {
            this.notificationService.showDanger('Xoá thất bại. Vui lòng thực hiện lại.');
          }
        },
        (err) => {
          this.loadingProcess = false;
          this.log.info(err);
        });

    }, (dismiss) => {
      // this.log.info(dismiss);
    });
  }

  public addNewItem() {
    if (!this.tagSelected.context_id) {
      this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
      return;
    }
    if (!this.agentId) {
      this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
      this.getAgentActive();
      return;
    }
    this.isSaving = true;
    // let intentNew = { id: UUID.UUID(), is_new: true, usersays: [], faq: 1, response_messages: [] };
    // this.intents.unshift(intentNew);
    let intentNew = { id: UUID.UUID(), name: UUID.UUID(), is_just_understand_in_context: true, faq: 1, agent_id: this.agentId, intent_contexts: [] };

    intentNew.intent_contexts = [{
      contexts: { id: this.tagSelected.context_id },
      type: 0,
      status: 1,
      is_hidden: 1
    }];

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.tagAddFaq(intentNew);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          // callback(false);
          this.isSaving = false;
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          this.isSaving = false;
          // callback(false);
        } else {
          // this.notificationService.showSuccess('Thêm thành công');
          // callback(intent);
          this.intents.unshift(intent);
          this.isSaving = false;

          if (this.intents) {
            this.intentCurrents = this.intents.slice(0, this.pageSize);
            this.length = this.intents.length;
            this.pageIndex = 0;
          }
        }
      },
      (err) => {
        // callback(false);
        this.isSaving = false;
        this.log.info(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public addNewFallback() {
    this.setLoading();
    if (!this.tagSelected.context_id) {
      this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
      this.setUnLoading();
      return;
    }
    if (!this.agentId) {
      this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
      this.setUnLoading();
      this.getAgentActive();
      return;
    }
    this.isSaving = true;
    // let intentNew = { id: UUID.UUID(), is_new: true, usersays: [], faq: 1, response_messages: [] };
    // this.intents.unshift(intentNew);
    let intentNew = { id: UUID.UUID(), name: this.tagSelected.id + '_hashtag_fallback', is_just_understand_in_context: true, type: 2, faq: 1, agent_id: this.agentId, intent_contexts: [] };

    intentNew.intent_contexts = [{
      contexts: { id: this.tagSelected.context_id },
      type: 0,
      status: 1,
      is_hidden: 1
    }];

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.tagAddFaq(intentNew);
    actionOperate.subscribe(
      (intent) => {
        this.setUnLoading();
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          // callback(false);
          this.isSaving = false;
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          this.isSaving = false;
          // callback(false);
        } else {
          // this.notificationService.showSuccess('Thêm thành công');
          // callback(intent);
          intent.intent_contexts = [{
            contexts: { id: this.tagSelected.context_id },
            type: 0,
            status: 1,
            is_hidden: 1
          }];
          this.intentFallback = intent;
        }
      },
      (err) => {
        this.setUnLoading();
        // callback(false);
        this.isSaving = false;
        this.log.info(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public transform(searchTerm: string) {
    searchTerm = StringFormat.formatText(searchTerm);
    if (searchTerm != null && searchTerm.length > 0) {
      let intentTemp = this.intents.filter((item) => {
        let temp = [];
        if (item.usersays) {
          temp = item.usersays.filter((says) => {
            return StringFormat.formatText(says.content).indexOf(searchTerm) !== -1
          });
        }
        let response = '';
        if (item.response_messages && item.response_messages[0]) {
          response = item.response_messages[0].content;
        }
        return (temp.length > 0 || StringFormat.formatText(response).indexOf(searchTerm) !== -1);
      });

      if (intentTemp) {
        this.intentCurrents = intentTemp.slice(0, this.pageSize);
        this.length = intentTemp.length;
        this.pageIndex = 0;
      }
    } else {
      if (this.intents) {
        this.intentCurrents = this.intents.slice(0, this.pageSize);
        this.length = this.intents.length;
        this.pageIndex = 0;
      }
    }
  }

  public filterTag(searchTerm: string): any[] {
    if (searchTerm && searchTerm != null && searchTerm.trim().length > 0) {
      searchTerm = StringFormat.formatText(searchTerm.trim());
      return this.listHashTags = this.listHashTagData.filter((item) => {
        return (StringFormat.formatText(item.name).indexOf(StringFormat.formatText(searchTerm)) !== -1)
      });
    } else {
      return this.listHashTags = this.listHashTagData;
    }
  }

  public paginate(event: LazyLoadEvent) {
    this.pageIndex = parseInt(event.first + '', 10);
    this.pageSize = parseInt(event.rows + '', 10);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    this.length = this.intents.length;
  }

  public paginateDel() {
    // this.loadCarsLazy2(pageIndex, event.rows);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    if (this.intentCurrents.length === 0) {
      this.pageIndex = this.pageIndex - this.pageSize;
      this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
      this.length = this.intents.length;
    }
    // if (this.intentCurrents.length === 0) {
    //   this.intentCurrents = this.intents.slice(this.pageIndex - this.pageSize, this.pageIndex);
    // }
  }

  public getAgentActive() {
    this.setLoading();
    this.applicationService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          this.setUnLoading();
          this.agentId = agent.agent_id;
        }
      });
  }

  public addNewHashTag() {
    this.setLoading();
    if (!this.partnerId) {
      this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
      this.setUnLoading();
      return;
    }
    let _name = StringFormat.generateTagName('hana', this.listHashTags, 'name');
    let actionOperate: Observable<any>;
    actionOperate = this.commentService.addNew({ partner_id: this.partnerId, name: _name });
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result) {
          this.listHashTags.unshift(result);
        }
      },
      (err) => {
        this.setUnLoading();
        this.log.info(err);
      });
  }
  // delete intent
  public deleteIntent(index) {
    swal({
      title: 'Bạn chắc chắn muốn xóa FAQ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      // delete this.intents[index];
      let intent = this.intentCurrents[index];
      if (intent.is_new) {
        this.intents = this.intents.filter((it) => {
          return it.id !== intent.id;
        });
        this.length = this.intents.length;
        this.paginateDel();
        swal({
          title: 'Xóa thành công!',
          text: '',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false
        });
      } else {
        this.setLoading();
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.delete(intent.id);
        actionOperate.subscribe(
          (intents) => {
            this.setUnLoading();
            // alert(JSON.stringify(intents));
            // this.intents = intents;
            let itent = this.intentCurrents[index];
            this.intents = this.intents.filter((it) => {
              return it.id !== itent.id;
            });
            this.length = this.intents.length;
            // this.intentCurrents.splice(index, 1);
            this.paginateDel();
            swal({
              title: 'Xóa thành công!',
              text: '',
              type: 'success',
              confirmButtonClass: 'btn btn-success',
              buttonsStyling: false
            });
          },
          (err) => {
            this.setUnLoading();
            this.log.info(err);
          });
      }
    }, (dismiss) => {
      // a
    });
  }
  public setLoading() {
    this.loadingProcess = true;
    this.callChange();
  }
  public setUnLoading() {
    this.loadingProcess = false;
    this.callChange();
  }
}
