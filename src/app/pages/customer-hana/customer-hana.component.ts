import { NotificationService } from './../../_services/notification.service';
import { Component, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import { EmitterService } from '../../_services/emitter.service';
declare var $: any;
@Component({
  selector: 'customer-hana',
  templateUrl: 'customer-hana.component.html'
})
export class CustomerHanaComponent implements AfterViewInit, OnInit {
  private menutype = 'customerhana';
  constructor(private notificationService: NotificationService) { }
  public ngAfterViewInit(): void {
    $('.dropdown-toggle').dropdown();
    $('.dropdown').dropdown();
    $('body').removeClass('sidebar-mini');
  }

  public ngOnInit() {
    localStorage.setItem('remarketting', 'true');
    this.notificationService.clearConsole();

  }

  public close() {
  }

}
