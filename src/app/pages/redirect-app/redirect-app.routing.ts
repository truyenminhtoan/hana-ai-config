import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { RedirectAppComponent } from './redirect-app.component';

const routes: Routes = [
  { path: '', component: RedirectAppComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
