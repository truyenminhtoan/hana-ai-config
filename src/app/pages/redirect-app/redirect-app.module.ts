// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { GuideAdvisoryComponent } from './guide-advisory.component';
import { CommonModule } from '@angular/common';
import { YoutubePlayerModule } from 'ng2-youtube-player';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../themes/nga.module';
import { routing } from './redirect-app.routing';
import { RedirectAppComponent } from './redirect-app.component';
import { Router } from '@angular/router';
import { UUID } from 'angular2-uuid';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        NgaModule,
        YoutubePlayerModule
    ],
    declarations: [
        RedirectAppComponent,
    ],
    exports: [
        RedirectAppComponent,
    ]
})
export class RedirectAppModule {
}
