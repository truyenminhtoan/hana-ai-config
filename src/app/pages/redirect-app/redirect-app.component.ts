import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'redirect-app',
    templateUrl: 'redirect-app.component.html',
    styleUrls: ['redirect-app.component.scss']
})
export class RedirectAppComponent {

    private partnerId: any;
    constructor(private router: Router, private route: ActivatedRoute) {
        this.route.params.subscribe((params) => {
            this.partnerId = params['id'];
        });
        // location.reload();
        this.router.navigate(['/' + this.partnerId + '/messages']);
    }

    public ngAfterViewInit() {
        // location.reload();
        // this.router.navigate(['/' + this.partnerId + '/messages']);
    }
}
