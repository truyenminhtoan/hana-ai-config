import { Component, ElementRef, NgZone, ChangeDetectorRef, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuickTemplateService } from '../../../_services/quicktemplate.service';
import { NotificationService } from '../../../_services/notification.service';

declare var $: any;
declare var swal: any;

@Component({
    selector: 'list-quick-template',
    templateUrl: 'list-quick-template.component.html',
    styleUrls: ['list-quick-template.component.scss']
})
export class ListQuickTemplateComponent implements OnInit, AfterViewInit, OnDestroy {
    private currentUser: any;
    private partnerId: number;
    private listQuickTemplate = [];
    private editValue: any;
    constructor(private ngZone: NgZone, private elementRef: ElementRef,  private notificationService: NotificationService,
        private router: Router, private route: ActivatedRoute, private cdRef: ChangeDetectorRef, private quickTemplateService: QuickTemplateService) {

        this.checkLoged();

    }
    public checkLoged() {
        if (localStorage.getItem('currentUser') === 'undefined') {
            this.router.navigate(['/login-hana']);
        } else {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }

        if (this.currentUser.app_using) {
            this.partnerId = this.currentUser.app_using.id;
            //console_bk.log('APPUSING ' + JSON.stringify(this.currentUser.app_using));
        } else {
            localStorage.removeItem('currentUser');
            window.location.href = '/';
        }
        this.getQuickTemplate();
    }

    public ngAfterViewInit() {
        $(".dropdown-toggle").dropdown();
        let h = $(window).height();
        $('.template-quick-class').slimScroll({
          height: h - 60 + 'px'
        });
    }

    public getQuickTemplate() {
        this.quickTemplateService.getListQuickPromise(this.partnerId)
            .then((integrate) => {
                if (integrate.errorCode === 200) {
                    this.listQuickTemplate = integrate.data;
                }
                //console_bk.log('SJON ' + JSON.stringify(integrate));
            });
    }

    public deleteAction(template: any) {
        //console_bk.log('this ' + JSON.stringify(template));
        this.quickTemplateService.deleteTemplateReply(this.partnerId, template.template_reply_id)
        .then((integrate) => {
            this.getQuickTemplate();
            this.notificationService.showSuccessTimer('Xóa thành công', 1000);
        });
    }
    public delete(template: any) {
        // console_bk.log('xóa id ' + id + ' APP ID ' + appid);
        swal({
          title: 'Bạn có muốn xóa nội dung này?',
          text: 'Bạn không thể khôi phục lại khi đã xóa!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Vâng, Tôi muốn xóa!',
          cancelButtonText: 'Không, Tôi muốn giữ ',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(() => {
          this.deleteAction(template);
        }, (dismiss) => {
          // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
          if (dismiss === 'cancel') {
            // console_bk.log('giữ nó');
          }
        })
      }
    public edit(template: any) {

        //console_bk.log('this ' + JSON.stringify(template));
        this.editValue = template;
        this.cdRef.detectChanges();
        $('#createQuickTemplateModel').modal('show');
    }

    public ngOnInit() {


    }

    public ngOnDestroy(): void {

    }


    private popupCreate() {
        //console_bk.log('CREATE');
        this.editValue = false;
        this.cdRef.detectChanges();
        this.showModel();
    }

    private showModel() {
        $('#createQuickTemplateModel').modal('show');
    }

    public counterUpdate(event: boolean) {
        //console_bk.log('CHANGE UNPAGE SUB');
        this.getQuickTemplate();
    }
}
