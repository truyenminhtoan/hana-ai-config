// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../themes/nga.module';
import { routing } from './quick-template.routing';
// This Module's Components
import { QuickTemplateComponent } from './quick-template.component';
import { ListQuickTemplateComponent } from './list-quick-template/list-quick-template.component';
import { CreateQuickTemplateComponent } from './create-quick-template/create-quick-template.component';
import { QuickTemplateService } from '../../_services/quicktemplate.service';
import { NotificationService } from '../../_services/notification.service';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        NgaModule,
    ],
    declarations: [
        QuickTemplateComponent,
        ListQuickTemplateComponent,
        CreateQuickTemplateComponent
    ],
    exports: [
        QuickTemplateComponent,
    ],
    providers: [
        QuickTemplateService,
        NotificationService
        // ConfigService
    ],
})
export class QuickTemplateModule {

}
