import { Component, AfterViewInit, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';



@Component({
    selector: 'quick-template',
    // templateUrl: 'quick-template.component.html',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['quick-template.component.scss']
})
export class QuickTemplateComponent {
    

}
