import { QuickTemplateService } from '../../../_services/quicktemplate.service';
import { NotificationService } from '../../../_services/notification.service';
import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef
} from '@angular/core';


declare var $: any;
declare var _: any;

@Component({
    selector: 'create-quick-template',
    templateUrl: 'create-quick-template.component.html',
    styleUrls: ['create-quick-template.component.scss']
})
export class CreateQuickTemplateComponent implements OnInit, OnChanges {
    private quicktext: string;
    private content: string;
    private ordernum: number;

    @Input() partnerId: string;
    @Input() editValue: any;

    @Output() isChange = new EventEmitter<any>();

    constructor(private cdRef: ChangeDetectorRef, private quickTemplateService: QuickTemplateService, private notificationService: NotificationService) {
    }
    public ngOnInit() {
        
    }

    public ngOnChanges() {
        //console_bk.log('PARNERIDDD ' + this.partnerId);
        //console_bk.log('PARNERIDDDeditValueeditValue ' + this.editValue);
        this.cdRef.detectChanges();
    }

    public save() {
        //console_bk.log('this ' + this.quicktext + ' ' + this.content);
        if (!this.content || this.content === '') {
            this.notificationService.showWarningTimer('Vui lòng nhập nội dung', 1000);
            return;
        }
        // return;
        this.quickTemplateService.createTemplateReply(this.partnerId, this.quicktext, this.content, this.ordernum)
            .then((integrate) => {
                //console_bk.log('SJON ' + JSON.stringify(integrate));
                $('#createQuickTemplateModel').modal('hide');
                this.notificationService.showSuccessTimer('Lưu thành công', 1000);
                this.editValue = false;
                this.quicktext = '';
                this.content = '';
                this.ordernum = 1;
                this.isChange.emit(this.partnerId);
            });
    }


    public saveEdit() {
        if (!this.editValue.text_search || this.editValue.text_search === '') {
            this.notificationService.showWarningTimer('Vui lòng nhập nội dung', 1000);
            return;
        }
        //console_bk.log('this ' + JSON.stringify(this.editValue));
        this.quickTemplateService.editTemplateReply(this.partnerId, this.editValue.quick_search, this.editValue.text_search, this.editValue.order_num, this.editValue.template_reply_id)
            .then((integrate) => {
                //console_bk.log('SJON ' + JSON.stringify(integrate));
                $('#createQuickTemplateModel').modal('hide');
                this.notificationService.showSuccessTimer('Lưu thành công', 1000);
                this.editValue = false;
                this.quicktext = '';
                this.content = '';
                this.ordernum = 1;
                this.isChange.emit(this.partnerId);
            });
    }

}
