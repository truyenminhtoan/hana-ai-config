import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { QuickTemplateComponent } from './quick-template.component';
import { ListQuickTemplateComponent } from './list-quick-template/list-quick-template.component';

const routes: Routes = [
  { path: '', component: QuickTemplateComponent,
  children: [
    { path: 'quick-list', component: ListQuickTemplateComponent }
  ] },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
