import { Component } from '@angular/core';

@Component({
    selector: 'intents',
    template: '<router-outlet></router-outlet>'
})
export class IntentsComponent {

}
