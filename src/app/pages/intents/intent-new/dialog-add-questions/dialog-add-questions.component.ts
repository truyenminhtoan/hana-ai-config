import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ResponseMessageModel } from '../../../../_models/intents.model';

@Component({
  selector: 'dialog-add-questions',
  templateUrl: 'dialog-add-questions.component.html',
  styleUrls: ['dialog-add-questions.component.scss']
})
export class DialogAddQuestionsComponent implements OnInit {
  private response: ResponseMessageModel[] = [];
  constructor(public dialogRef: MdDialogRef<DialogAddQuestionsComponent>) {

  }

  public handleAddInput(){
      let res: ResponseMessageModel = new ResponseMessageModel();
      res.content = '';
      res.status = 1;
      this.response.push(res);
  }

  public ngOnInit() {
  
  }

  public handleClose() {
    this.dialogRef.close('ok');
  }
}
