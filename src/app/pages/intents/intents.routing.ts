import { Routes, RouterModule }  from '@angular/router';

import { IntentsComponent } from './intents.component';
import { ModuleWithProviders } from '@angular/core';
import { IntentNewComponent } from './intent-new/intent-new.component';
import { IntentListComponent } from './intent-list/intent-list.component';
import {IntentEditComponent} from "./intent-edit/intent-edit.component";
import {IntentNewCallback} from "./intent-new-callback/intent-new-callback.component";
import {IntentEditCacllbackConponent} from "./intent-edit-callback/intent-edit-callback";

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: IntentsComponent,
    children: [
      { path: 'intentNew/callback', component: IntentNewCallback },
      { path: 'intentNew', component: IntentNewComponent },
      { path: 'intentEdit/:id', component: IntentEditComponent },
      { path: 'intentEdit/callback/:id', component: IntentEditCacllbackConponent },
      { path: 'intentList', component: IntentListComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
