import { NgModule, NgZone } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntentsComponent } from './intents.component';
import { routing } from './intents.routing';
import { NgaModule } from '../../themes/nga.module';
import { IntentListComponent } from './intent-list/intent-list.component';
import { IntentNewComponent } from './intent-new/intent-new.component';
import { SearchDataComponent } from './intent-list/components/search-data/search-data.component';
import { IntentService } from '../../_services/intents.service';
import { FilterDataPipe } from './intent-list/components/filter-data-pipe/filter-data.pipe';
import { EventsService } from '../../_services/events.service';
import { ObjectMemoryService } from '../../_services/object_memory.service';
import { EntityService } from '../../_services/entity.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogAddQuestionsComponent } from './intent-new/dialog-add-questions/dialog-add-questions.component';
import { NotificationService } from '../../_services/notification.service';
import { TagInputModule } from 'ng2-tag-input';
import { IntentEditComponent } from "./intent-edit/intent-edit.component";
import { DataTableModule } from "angular2-datatable/index";
import { IntentPipe } from "./pipes/actions.pipe";
import { MySelectAutocompleteComponent } from "./components/select-autocomplete/select-autocomplete.component";
import { MemoryAutocompleteComponent } from "./components/memory-autocomplete/select-autocomplete.component";
import { IntentInputAutoComplete } from "./components/input-autocomplete/select-autocomplete.component";
import { ValidationService } from "../../_services/ValidationService";
import { IntentActionAutoComponent } from "./components/intents_action_auto_complete/intents_action_auto_complete";
import { MdAutocompleteModule } from '@angular/material';
import { IntentUserSayAutoComponent } from "./components/intent-user-say-autocomplete/intent-user-say-autocomplete";
import { EntityValueSelectAutoComponent } from "./components/entity-select-autocomplete/entity-select-autocomplete";
import { ParamAutoComponent } from "./components/param-autocomplete/param-autocomplete";
import { ObjectAutoComponent } from "./components/object_auto_complete/object_auto_complete";
import { ResCardComponent } from "./components/res-card-component/res-card-component.component";
import { SearchDataTableListComponent } from "./intent-list/components/search-data-table-list/search-data.component";
import { UserSerSayAutoComponent } from "./intent-list/components/user-say-autocomplete/user-say-autocomplete.component";
import { IntentNewCallback } from "./intent-new-callback/intent-new-callback.component";
import { IntentEditCacllbackConponent } from "./intent-edit-callback/intent-edit-callback";
import {BlocksService} from "../../_services/blocks.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    routing,
    TagInputModule,
    DataTableModule,
    MdAutocompleteModule
  ],
  declarations: [
    IntentsComponent,
    IntentListComponent,
    IntentNewComponent,
    IntentNewCallback,
    IntentEditCacllbackConponent,
    SearchDataComponent,
    SearchDataTableListComponent,
    FilterDataPipe,
    DialogAddQuestionsComponent,
    IntentEditComponent,
    IntentPipe,
    MySelectAutocompleteComponent,
    MemoryAutocompleteComponent,
    IntentInputAutoComplete,
    IntentActionAutoComponent,
    IntentUserSayAutoComponent,
    EntityValueSelectAutoComponent,
    ParamAutoComponent,
    ObjectAutoComponent,
    ResCardComponent,
    UserSerSayAutoComponent
  ],
  providers: [
    IntentService,
    EventsService,
    ObjectMemoryService,
    EntityService,
    NotificationService,
    ValidationService,
    BlocksService
  ],
  exports: [
    FormsModule,
    SearchDataComponent,
    SearchDataTableListComponent,
    IntentNewComponent,
    IntentNewCallback,
    IntentEditCacllbackConponent,
    IntentEditComponent,
    FilterDataPipe,
    DialogAddQuestionsComponent,
    IntentPipe,
    MySelectAutocompleteComponent,
    MemoryAutocompleteComponent,
    IntentInputAutoComplete,
    IntentActionAutoComponent,
    IntentUserSayAutoComponent,
    EntityValueSelectAutoComponent,
    ParamAutoComponent,
    ObjectAutoComponent,
    ResCardComponent,
    UserSerSayAutoComponent
  ],
  bootstrap: [DialogAddQuestionsComponent]
})
export class IntentsModule { }
