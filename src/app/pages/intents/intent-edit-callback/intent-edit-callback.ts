import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, ViewChildren, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/startWith';
import * as _ from 'underscore';
import { EventsService, IntentService } from '../../../_services/index';
import { IntentModel } from '../../../_models/index';
import {
    ContextModel, IntentContextModel,
    IntentEntityModel, IntentFilterModel
} from '../../../_models/intents.model';
import { ObjectMemoryModel } from '../../../_models/object_memory.model';
import { ObjectMemoryService } from '../../../_services/object_memory.service';
import { EntityService } from '../../../_services/entity.service';
import { EntitiesComponent } from '../../entities/entities.component';
import { NotificationService } from '../../../_services/notification.service';
import { UserModel } from '../../../_models/user.model';
import { ResponseMessageModel } from '../../../_models/intents.model';
import { ActionsService } from "../../../_services/actions.service";
import { EntitieModel } from "../../../_models/entities.model";
import { ValidationService } from "../../../_services/ValidationService";
declare var $: any;
import { Router, ActivatedRoute } from '@angular/router';
import { StringFormat } from "../../../themes/validators/string.format";
import { ComponentInteractionService } from "../../../_services/compoent-interaction-service";
import { Subscription } from 'rxjs/Rx';
import { EmitterService } from "../../../_services/emitter.service";
declare var swal: any;
@Component({
    selector: 'intent-edit-callback',
    templateUrl: 'intent-edit-callback.html',
    styleUrls: ['intent-edit-callback.scss'],
})
export class IntentEditCacllbackConponent implements OnInit, OnChanges, AfterViewInit {
    private currentIntent: IntentModel = new IntentModel();
    private contexts: ContextModel[] = [];
    private objects: ObjectMemoryModel[] = [];
    private entities: EntitiesComponent[] = [];
    private stateCtrl: FormControl;
    private filteredStates: any;
    private objectMemory: ObjectMemoryModel;
    private responseMessage = [];
    private states = [];
    private currentUser: UserModel;
    private sub: any;
    private intentId: any;
    private intent_contexts_in = [];
    private tagsContextInAutoComplete = ['-', '-'];
    private intent_contexts_out = [];
    private tagsContextOutAutoComplete = ['-', '-'];
    private response_quickreply = [];
    private intent_inners = [{
        value: 'inner_action',
        name: 'Trong hệ thống'
    }, {
        value: 'outler_action',
        name: 'Ngoài hệ thống'
    }];
    private selected = 'inner_action';
    private memory_id: string;
    private lists: IntentModel[] = [];
    private is_inner = 'inner_action';
    private list_actions = [];
    private inner_action = '';
    private outler_action = '';
    private listEntyties: EntitieModel[] = [];
    private show_quick_reply = true;
    private err_name = '';
    private entities_name = [];
    private entities_name_memory = [];
    private auto_intent_entities: IntentEntityModel[] = [];
    private is_load_intent_edit = true;
    private is_load_list_entities = true;
    private list_intent_filter: IntentFilterModel[] = [];
    private list_object_name = [];
    // View
    @ViewChild('name') input_name: ElementRef;
    private currentAgentId = '';
    private is_check_table = '';
    private memory_name = '';
    private list_param_name = [];
    private is_show_ngucanh = false;
    private is_show_hanhdong = false;
    private status_update = false;
    @ViewChild('link_ngu_canh') link_ngu_canh: ElementRef;
    @ViewChild('link_su_kien_dau_vao') link_su_kien_dau_vao: ElementRef;
    @ViewChild('link_hanh_dong') link_hanh_dong: ElementRef;
    @ViewChild('quick_reply') quick_reply: ElementRef;
    private eventSubscription: Subscription;
    private load_action_status = false;
    private list_action_full = [];
    private auto_intent_entities_full = [];
    private is_load_content = false;
    constructor(
        private intentService: IntentService,
        private objectService: ObjectMemoryService,
        private entityService: EntityService,
        private notificationService: NotificationService,
        private route: ActivatedRoute,
        private router: Router,
        private actionsService: ActionsService,
        private validationService: ValidationService,
        private _componentService: ComponentInteractionService) {

        this.loadBuild();

        //auto select entity
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'auto-select-entity') {
                    var index_param = localStorage.getItem('index-param');
                    var type_param = localStorage.getItem('type-param');
                    if (index_param != null && index_param != undefined && type_param != null && type_param != undefined) {
                        if (type_param.toString() == '1') {
                            if (this.currentIntent.intent_entities != undefined) {
                                for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                                    if (this.currentIntent.intent_entities[i].param != null && this.currentIntent.intent_entities[i].param.trim().length > 0) {
                                        if (i == parseInt(index_param)) {
                                            var entity_name_new = this.filterEntityname(this.currentIntent.intent_entities[i].param);
                                            if (entity_name_new != null && entity_name_new.trim().length > 0) {
                                                this.currentIntent.intent_entities[i].entity_name = entity_name_new;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (this.auto_intent_entities != undefined) {
                                var param_check = localStorage.getItem('obj_param_cur');
                                let that = this;
                                setTimeout(() => {
                                    var entity_name_new = that.filterEntityname(param_check);
                                    that.auto_intent_entities[index_param].entity_name = entity_name_new;
                                    localStorage.setItem('obj_param_cur', '');
                                }, 200);
                            }
                        }
                    }
                }
            }
        );
    }

    public filterEntityname(param) {
        for (var i = 0; i < this.auto_intent_entities_full.length; i++) {
            if (this.auto_intent_entities_full[i].param.trim() == param.trim()) {
                return this.auto_intent_entities_full[i].entity_name;
            }
        }
        return '';
    }

    public loadBuild() {
        this.currentAgentId = localStorage.getItem('currentAgent_Id');
        if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
            if (this.responseMessage.length == 0) {
                this.responseMessage.push({ type: 1, message: { posibleTexts: [{ content: '' }] } });
            }
            this.getIntentInfo();
            this.stateCtrl = new FormControl();
            this.initIntentView();
            this.loadListEntity();
            this.loadList();
        } else {
            this.notificationService.showNoAgentId();
        }

    }

    public loadList() {
        this.getListIntentFilterByAgentId(this.currentAgentId, (data) => {
            this.list_intent_filter = data;
        });
    }

    public getListIntentFilterByAgentId(agentId: string, callback) {
        let actionOperate: Observable<IntentFilterModel[]>;
        actionOperate = this.intentService.getListIntentFilterByAgentId(agentId);
        // Subscribe to observable
        actionOperate.subscribe(
            (data) => {
                if (data) {
                    callback(data);
                } else {
                    callback([]);
                }
            },
            (err) => {
                callback(false);
            });
    }

    public loadListEntity() {
        this.entityService.getListEntityPromise(this.currentAgentId, 1)
            .then((data) => {
                if (data != undefined && data.length > 0) {
                    this.listEntyties = data;
                    if (this.listEntyties.length > 0) {
                        this.listEntyties.sort(function (a, b) {
                            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            // names must be equal
                            return 0;
                        });

                        for (var i = 0; i < this.listEntyties.length; i++) {
                            this.entities_name.push('@' + this.listEntyties[i].name);
                            this.entities_name_memory.push(this.listEntyties[i].name);
                        }
                    }
                }
                this.is_load_list_entities = false;
            })
    }


    public loadIntentEdit() {
        this.sub = this.route.params.subscribe((params) => {
            this.intentId = params['id'];
            let actionOperate = this.intentService.getDetailIntent(this.intentId);
            actionOperate.subscribe(
                (data) => {
                    this.buildIntentEdit(data);
                },
                (err) => {

                });
            this.is_load_intent_edit = false;
        });
    }


    public buildIntentEdit(data) {
        if (this.load_action_status == false && this.list_actions.length == 0) {
            let actionOperate: Observable<any>;
            actionOperate = this.actionsService.getListActionBasicNotAbstractByAgentId(this.currentAgentId);
            actionOperate.subscribe(
                (group_actions) => {
                    if (group_actions !== null) {
                        this.list_action_full = group_actions;
                        this.list_actions = [];
                        if (group_actions != undefined && group_actions.length > 0) {
                            for (var j = 0; j < group_actions.length; j++) {
                                this.list_actions.push(group_actions[j].nameDisplay);
                            }
                        }
                    }
                },
                (err) => {
                });
        }
        this.currentIntent = data;
        if (this.currentIntent.intent_entities.length > 0 ||
            (this.currentIntent.inner_action != null && this.currentIntent.inner_action != 'null' && this.currentIntent.inner_action.trim().length > 0) ||
            (this.currentIntent.outler_action != null && this.currentIntent.outler_action != 'null' && this.currentIntent.outler_action.trim().length > 0)) {
            this.link_hanh_dong.nativeElement.click();

        } else {
            this.is_show_hanhdong = false;
        }
        // set ngu canh dau vao
        if (this.currentIntent.intent_contexts != undefined && this.currentIntent.intent_contexts.length > 0) {
            this.is_show_ngucanh = true;
            this.link_ngu_canh.nativeElement.click();
            for (var i = 0; i < this.currentIntent.intent_contexts.length; i++) {
                if (this.currentIntent.intent_contexts[i].type == 0) {
                    //this.intent_contexts_in.push(this.currentIntent.intent_contexts[i]);
                    this.intent_contexts_in.push(
                        {
                            value: this.currentIntent.intent_contexts[i].contexts.name,
                            display: this.currentIntent.intent_contexts[i].contexts.name,
                            intent_id: this.currentIntent.intent_contexts[i].intent_id,
                            id: this.currentIntent.intent_contexts[i].contexts.id,
                            status: this.currentIntent.intent_contexts[i].contexts.status,
                            created_date_context_item: this.currentIntent.intent_contexts[i].contexts.created_date,
                            created_date: this.currentIntent.intent_contexts[i].created_date
                        }
                    );
                } else {
                    //this.intent_contexts_out.push(this.currentIntent.intent_contexts[i]);
                    this.intent_contexts_out.push({
                        value: this.currentIntent.intent_contexts[i].contexts.name,
                        display: this.currentIntent.intent_contexts[i].contexts.name,
                        intent_id: this.currentIntent.intent_contexts[i].intent_id,
                        id: this.currentIntent.intent_contexts[i].contexts.id,
                        status: this.currentIntent.intent_contexts[i].contexts.status,
                        created_date_context_item: this.currentIntent.intent_contexts[i].contexts.created_date,
                        created_date: this.currentIntent.intent_contexts[i].created_date
                    });
                }
            }
            this.intent_contexts_out.sort(function (a, b) {
                var nameA = a.value.toLowerCase(), nameB = b.value.toLowerCase()
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            });
        }

        // Set Entity
        if (this.currentIntent.intent_entities && this.currentIntent.intent_entities.length > 0) {
            for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                if (this.currentIntent.intent_entities[i].intent_entities_suggestion == undefined) {
                    this.currentIntent.intent_entities[i].intent_entities_suggestion = [];
                }
                if (this.currentIntent.intent_entities[i].response_messages == undefined) {
                    this.currentIntent.intent_entities[i].response_messages = [];
                }
            }
        }
        this.currentIntent.intent_entities.sort(function (a, b) {
            var nameA = a.priority, nameB = b.priority
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        // Set memory Id
        if (this.currentIntent.intent_entities.length > 0) {
            this.memory_name = this.currentIntent.intent_entities[0].global_memory;
            for (var z = 0; z < this.currentIntent.intent_entities.length; z++) {
                if (this.currentIntent.intent_entities[z].global_memory != null && this.currentIntent.intent_entities[z].global_memory.trim().length > 0) {
                    this.memory_name = this.currentIntent.intent_entities[z].global_memory.trim();
                    break;
                }
            }
            if (this.objects != undefined && this.objects.length > 0) {
                for (var k = 0; k < this.objects.length; k++) {
                    if (this.objects[k].name != undefined && this.memory_name != undefined) {
                        if (this.objects[k].name.trim() == this.memory_name.trim()) {
                            var memory_id = this.objects[k].id;
                            this.getObjectMemoryDetail(memory_id, (cb) => {
                                if (cb) {
                                    cb.object_memory_params.filter((item) => {
                                        for (var i = 0; i < cb.object_memory_params.length; i++) {
                                            this.list_param_name.push(cb.object_memory_params[i].param);
                                        }
                                    });
                                    this.auto_intent_entities_full = cb.object_memory_params;
                                }
                            });

                        }
                    }
                }
            }
        }
        // do du lieu qua intent_entities auto
        this.auto_intent_entities = this.currentIntent.intent_entities;
        if (this.auto_intent_entities.length == 0) {
            this.addIntentEntityRow();
        }
        this.currentIntent.intent_entities = [];
        // Set phan hoi
        if (this.currentIntent.response_messages !== undefined) {
            if (this.currentIntent.response_messages[0] != undefined && this.currentIntent.response_messages[0].content != undefined) {
                let response_messages = JSON.parse(this.currentIntent.response_messages[0].content);

                response_messages.filter((row) => {
                    if (row.message.posibleTexts) {
                        row.type = 1;
                    } else if (row.message.quick_replies) {
                        row.type = 4;
                    } else if (row.message.attachment && row.message.attachment.type === 'image') {
                        row.type = 3;
                    } else if (row.message.attachment && row.message.attachment.type === 'template') {
                        row.type = 2;
                    }
                });

                this.responseMessage = response_messages;
                if (this.response_quickreply.length == 0) {
                    for (var i = 0; i < response_messages.length; i++) {
                        if (response_messages[i].type == 4) {
                            this.show_quick_reply = false;
                        } else {
                            this.response_quickreply.push([]);
                        }
                    }
                }
                for (var i = 0; i < response_messages.length; i++) {
                    if (response_messages[i].type == 1) {
                        var tmp = [];
                        if (response_messages[i].message != undefined && response_messages[i].message.posibleTexts != undefined) {
                            for (var k = 0; k < response_messages[i].message.posibleTexts.length; k++) {
                                tmp.push({ 'content': response_messages[i].message.posibleTexts[k] });
                            }
                        }
                        this.responseMessage[i].message.posibleTexts = tmp;
                    }
                }
            }
            //this.responseMessage.sort(function(a, b){return a.type - b.type});
        }
        if (this.currentIntent.inner_action != null) {
            this.is_inner = 'inner_action';
            //cap nhat lai ten hien thi
            for (var k = 0; k < this.list_action_full.length; k++) {
                if (this.list_action_full[k].name.trim() == this.currentIntent.inner_action.trim()) {
                    this.currentIntent.inner_action = this.list_action_full[k].nameDisplay.trim();
                }
            }
        } else {
            this.is_inner = 'outler_action';
        }
        this.inner_action = this.currentIntent.inner_action;
        this.outler_action = this.currentIntent.outler_action;
        EmitterService.get('LOAD_INTENT').emit(this.responseMessage);

        console.log('this.responseMessage@@@@@@@@@@@', JSON.stringify(this.responseMessage));

        this.is_load_content = true;
        this.notificationService.clearConsole();
    }

    public ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.intEventModule();
    }

    public intEventModule() {
    }

    public filterStates(val: string) {
        return val ? this.states.filter((s) => new RegExp(val, 'gi').test(s)) : this.states;
    }

    public ngAfterViewInit() {
        this.getListContext();
    }

    public ngOnChanges(changes: any) {
    }

    /**
     * ADD CONTENXT IN INENT
     * @param {any} contextName
     *
     * @memberOf IntentNewComponent
     */
    public handleAddContext(contextName, type) {
        let context = new IntentContextModel();
        context.contexts = new ContextModel();
        context.contexts.name = contextName;
        context.contexts.name_text = contextName;
        context.contexts.agent_id = localStorage.getItem('currentAgent_Id');
        context.contexts.status = 1;
        context.status = 1;
        context.type = type;
        context.intent_id = this.currentIntent.id;
        if (!this.currentIntent.intent_contexts || this.currentIntent.intent_contexts.length === 0) {
            this.currentIntent.intent_contexts = [];
            this.currentIntent.intent_contexts.push(context);
        } else {
            this.currentIntent.intent_contexts.push(context);
        }
    }

    public removeIntentEntityRow(obj) {
        this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
            return (row != obj)
        });
    }

    public removeIntentEntityRowAutoLoad(obj) {
        this.auto_intent_entities = this.auto_intent_entities.filter((row) => {
            return (row != obj)
        });
    }

    public removeIntentEntityRowAuto(i) {
        let entity = this.auto_intent_entities[i];
        if (entity.id) {
            this.auto_intent_entities[i].status = -1;
        } else {
            this.auto_intent_entities.splice(i, 1);
        }
    }

    public addIntentEntityRow() {
        if (!this.currentIntent.intent_entities) {
            this.currentIntent.intent_entities = [];
        }
        let intentEntity: IntentEntityModel = new IntentEntityModel();
        intentEntity.is_required = false;
        intentEntity.param = '';
        intentEntity.entity_name = '';
        intentEntity.response_messages = [];
        intentEntity.intent_entities_suggestion = [];
        intentEntity.default_value = '';
        intentEntity.status = 1;

        this.auto_intent_entities.push(intentEntity);
    }

    public addResponseMessageType(typeMes: number) {
        switch (typeMes) {
            case 1:
                // Phan hoi text
                this.responseMessage.push({ type: typeMes, message: { posibleTexts: [{ content: '' }] } });
                break;
            case 2:
                // Phan hoi card
                this.responseMessage.push({ type: typeMes, message: { attachment: { type: 'template', payload: { template_type: 'generic', elements: [{ image_url: '', title: '', subtitle: '', buttons: [{ type: 'postback', title: '', payload: '' }] }] } } } });

                break;
            case 3:
                // Phan hoi hinh anh
                this.responseMessage.push({ type: typeMes, message: { attachment: { type: 'image', payload: { url: '' } } } });
                break;
            case 4:
                // Phan hoi quick_replies
                this.responseMessage.push({ type: typeMes, message: { quick_replies: [], text: '' } });
                break;
        }
        //sort phan hoi
        //- Quickreply lúc nào cũng dưới cùng
        //- Các loại phản hồi khác thì hiển thị theo thứ tự người dùng đã thêm text - image - card - text - ... - quickreply
        var tmp_responseMessage = [];
        for (var i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type != 4) {
                tmp_responseMessage.push(this.responseMessage[i]);
            }
        }
        for (var i = 0; i < this.responseMessage.length; i++) {
            if (this.responseMessage[i].type == 4) {
                tmp_responseMessage.push(this.responseMessage[i]);
                break;
            }
        }
        this.responseMessage = tmp_responseMessage;
        this.checkShowQuickReply();
    }

    public checkShowQuickReply() {
        var check = false;
        if (this.responseMessage.length > 0) {
            for (var i = 0; i < this.responseMessage.length; i++) {
                if (this.responseMessage[i].type == 4) {
                    this.show_quick_reply = false;
                    check = true;
                    break;
                }
            }
            if (!check) {
                this.show_quick_reply = true;
            }
        }
    }

    public removeResponseMessage(obj) {
        this.responseMessage = this.responseMessage.filter((row) => {
            return row !== obj
        });

        this.checkShowQuickReply();
    }

    public deleteButtonCard(index, button) {
        this.responseMessage[index].message.attachment.payload.elements[0].buttons = this.responseMessage[index].message.attachment.payload.elements[0].buttons.filter((row) => {
            return row !== button
        });
    }

    public addNutBam(index) {
        if (this.responseMessage[index].message.attachment.payload.elements[0].buttons.length < 3) {
            this.responseMessage[index].message.attachment.payload.elements[0].buttons.push({ type: 'postback', title: '', payload: '' });
        }
    }

    public addNoiDungNutBam(event, i, j) {
        if (this.responseMessage[i].message.attachment.payload.elements[0].buttons) {
            this.responseMessage[i].message.attachment.payload.elements[0].buttons[j].title = event.target.value;
        }
    }

    public addDuongDamNutBam(event, i, j) {
        if (this.responseMessage[i].message.attachment.payload.elements[0].buttons) {
            this.responseMessage[i].message.attachment.payload.elements[0].buttons[j].payload = event.target.value;
        }
    }

    // Set text phan hoi quick_replies
    public setTitleQuickReplies(eventName, index) {
        this.responseMessage[index].message.text = eventName.target.value;
    }

    // Add phan hoi quick_replies
    public onItemAdded1(index, eventName) {
        if (this.response_quickreply[index].length > 0) {
            this.responseMessage[index].message.quick_replies = [];
            for (var i = 0; i < this.response_quickreply[index].length; i++) {
                this.responseMessage[index].message.quick_replies.push({ content_type: 'text', title: this.response_quickreply[index][i], payload: this.response_quickreply[index][i] });
            }
        }
    }

    // Remove phan hoi quick_replies
    public onItemRemoved(index, eventName) {
        if (this.response_quickreply[index].length > 0) {
            this.responseMessage[index].message.quick_replies = [];
            for (var i = 0; i < this.response_quickreply[index].length; i++) {
                this.responseMessage[index].message.quick_replies.push({ content_type: 'text', title: this.response_quickreply[index][i], payload: this.response_quickreply[index][i] });
            }
        }
    }

    public addResponseImagesInput(event, i) {
        this.responseMessage[i].message.attachment.payload.url = event.target.value;
    }

    public addResponseCardInput_image_url(event, i) {
        this.responseMessage[i].message.attachment.payload.elements[0].image_url = event.target.value;
        this.responseMessage[i].message.attachment.payload.elements[0].image_url = event.target.value;
    }

    public addResponseCardInput_title(event, i) {
        this.responseMessage[i].message.attachment.payload.elements[0].title = event.target.value;
    }

    public addResponseCardInput_subtitle(event, i) {
        this.responseMessage[i].message.attachment.payload.elements[0].subtitle = event.target.value;
    }

    public addResponseTextInput(event, i, j) {
        this.responseMessage[i].message.posibleTexts[j].content = event.target.value;
        let us = this.responseMessage[i].message.posibleTexts.filter((item) =>
            _.isEmpty(item.content))[0];
        if (!us) {
            this.responseMessage[i].message.posibleTexts.push({ content: '' });
        }
    }

    public addResponseText(i) {
        this.responseMessage[i].message.posibleTexts.push({ content: '' });
    }

    public removeResponseTextInput(event, i, j) {
        if (this.responseMessage[i].message.posibleTexts.length > 1) {
            this.responseMessage[i].message.posibleTexts = this.responseMessage[i].message.posibleTexts.filter((row) => {
                return row !== this.responseMessage[i].message.posibleTexts[j]
            });
        }
    }

    public validated(callback) {
        var entity_names = [];
        //Kiem tra du lieu khai niem co ton tai hay khong
        this.entityService.getListEntityPromise(this.currentAgentId, 1)
            .then((data) => {
                entity_names = _.pluck(data, 'name');
                for (var i = 0; i < this.auto_intent_entities_full.length; i++) {
                    entity_names.push(this.auto_intent_entities_full[i].entity_name);
                }
                try {
                    if (_.isEmpty(this.currentIntent.name)) {
                        this.err_name = 'form-group has-error';
                        this.notificationService.showWarning('Tên ý định không được rỗng');
                        this.input_name.nativeElement.focus();
                        return callback(false);
                    }
                    // SET AGENT_ID
                    this.currentIntent.agent_id = localStorage.getItem('currentAgent_Id');
                    // check exist
                    if (!this.checkName(this.currentIntent.name)) {
                        this.input_name.nativeElement.focus();
                        this.notificationService.showWarning('Tên ý định đã tồn tại');
                        return callback(false);
                    }
                    if (this.currentIntent.intent_entities != undefined && this.currentIntent.intent_entities.length > 0) {
                        for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                            var check = 0;
                            for (var j = 0; j < this.currentIntent.intent_entities.length; j++) {
                                if (this.currentIntent.intent_entities[i].param == this.currentIntent.intent_entities[j].param) {
                                    check++;
                                    if (check == 2) {
                                        if (!this.is_show_hanhdong) {
                                            this.link_hanh_dong.nativeElement.click();
                                        }

                                        this.notificationService.showWarning('Thông tin tham số không được trùng nhau');
                                        return callback(false);
                                    }
                                }
                            }
                            if (this.auto_intent_entities != undefined && this.auto_intent_entities.length > 0) {
                                for (var j = 0; j < this.auto_intent_entities.length; j++) {
                                    if (this.currentIntent.intent_entities[i].param == this.auto_intent_entities[j].param) {
                                        if (!this.is_show_hanhdong) {
                                            this.link_hanh_dong.nativeElement.click();
                                        }
                                        this.notificationService.showWarning('Thông tin tham số không được trùng nhau');
                                        return callback(false);
                                    }
                                }
                            }
                            if ((this.currentIntent.intent_entities[i].param != null && this.currentIntent.intent_entities[i].param.trim().length > 0) ||
                                (this.currentIntent.intent_entities[i].entity_name != null && this.currentIntent.intent_entities[i].entity_name.trim().length > 0) ||
                                this.currentIntent.intent_entities[i].default_value != null && this.currentIntent.intent_entities[i].default_value.trim().length > 0 ||
                                this.currentIntent.intent_entities[i].response_messages.length > 0 || this.currentIntent.intent_entities[i].intent_entities_suggestion.length > 0 ||
                                this.currentIntent.intent_entities[i].is_required) {
                                if (this.currentIntent.intent_entities[i].param == '' || this.currentIntent.intent_entities[i].entity_name == '') {
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    this.notificationService.showWarning('Tham số & khái niệm không được để trống');
                                    return callback(false);
                                }
                                if (!this.validationService.validateAlphabet(this.currentIntent.intent_entities[i].param.trim())) {
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    this.notificationService.showWarning('Tên tham số chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
                                    return callback(false);
                                }
                                var check_entity_validate = false;
                                for (var j = 0; j < entity_names.length; j++) {
                                    if (entity_names[j] == this.currentIntent.intent_entities[i].entity_name) {
                                        check_entity_validate = true;
                                    }
                                }
                                if (!check_entity_validate) {
                                    this.notificationService.showWarning('Thông tin khái niệm không tồn tại');
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    return callback(false);
                                }
                            }
                            if (this.currentIntent.intent_entities[i].is_required && this.currentIntent.intent_entities[i].response_messages.length == 0) {
                                if (!this.is_show_hanhdong) {
                                    this.link_hanh_dong.nativeElement.click();
                                }
                                this.notificationService.showWarning('Câu hỏi thu thập không được để trống nếu đã check trường bắt buộc');
                                return callback(false);
                            }
                        }
                    }
                    if (this.auto_intent_entities != undefined && this.auto_intent_entities.length > 0) {
                        for (var i = 0; i < this.auto_intent_entities.length; i++) {
                            check = 0;
                            for (var j = 0; j < this.auto_intent_entities.length; j++) {
                                if (this.auto_intent_entities[i].param == this.auto_intent_entities[j].param) {
                                    check++;
                                    if (check == 2) {
                                        if (!this.is_show_hanhdong) {
                                            this.link_hanh_dong.nativeElement.click();
                                        }
                                        this.notificationService.showWarning('Thông tin tham số không được trùng nhau');
                                        return callback(false);
                                    }
                                }
                            }
                            if (this.currentIntent.intent_entities != undefined && this.currentIntent.intent_entities.length > 0) {
                                for (var j = 0; j < this.currentIntent.intent_entities.length; j++) {
                                    if (this.auto_intent_entities[i].param == this.currentIntent.intent_entities[j].param) {
                                        if (!this.is_show_hanhdong) {
                                            this.link_hanh_dong.nativeElement.click();
                                        }
                                        this.notificationService.showWarning('Thông tin tham số không được trùng nhau');
                                        return callback(false);
                                    }
                                }
                            }
                            if ((this.auto_intent_entities[i].param != null && this.auto_intent_entities[i].param.trim().length > 0) ||
                                (this.auto_intent_entities[i].entity_name != null && this.auto_intent_entities[i].entity_name.trim().length > 0) ||
                                this.auto_intent_entities[i].default_value != null && this.auto_intent_entities[i].default_value.trim().length > 0 ||
                                this.auto_intent_entities[i].response_messages.length > 0 || this.auto_intent_entities[i].intent_entities_suggestion.length > 0 ||
                                this.auto_intent_entities[i].is_required) {
                                if (this.auto_intent_entities[i].param.trim().length == 0 || this.auto_intent_entities[i].entity_name.trim().length == 0) {
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    this.notificationService.showWarning('Tham số & khái niệm không được để trống');
                                    return callback(false);
                                }
                                var check_entity_validate = false;
                                for (var j = 0; j < entity_names.length; j++) {
                                    if (entity_names[j].trim() == this.auto_intent_entities[i].entity_name.trim()) {
                                        check_entity_validate = true;
                                    }
                                }
                                if (!check_entity_validate) {
                                    this.notificationService.showWarning('Thông tin khái niệm không tồn tại');
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    return callback(false);
                                }
                                if (!this.validationService.validateAlphabet(this.auto_intent_entities[i].param.trim())) {
                                    if (!this.is_show_hanhdong) {
                                        this.link_hanh_dong.nativeElement.click();
                                    }
                                    this.notificationService.showWarning('Tên tham số chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
                                    return callback(false);
                                }
                            }
                            if (this.auto_intent_entities[i].is_required && this.auto_intent_entities[i].response_messages.length == 0) {
                                if (!this.is_show_hanhdong) {
                                    this.link_hanh_dong.nativeElement.click();
                                }
                                this.notificationService.showWarning('Câu hỏi thu thập không được để trống nếu đã check trường bắt buộc');
                                return callback(false);
                            }
                        }
                    }
                    //check phan hoi
                    if (this.responseMessage != undefined) {
                        // toan: bo sung xac dinh loai message
                        this.responseMessage.filter((row) => {
                            if (row.message.posibleTexts) {
                                row.type = 1;
                            } else if (row.message.quick_replies) {
                                row.type = 4;
                            } else if (row.message.attachment && row.message.attachment.type === 'image') {
                                row.type = 3;
                            } else if (row.message.attachment && row.message.attachment.type === 'template') {
                                row.type = 2;
                            }
                        });
                        for (var i = 0; i < this.responseMessage.length; i++) {
                            if (this.responseMessage[i].type == 2) {
                                var element = this.responseMessage[i].message.attachment.payload.elements[0];
                                if (element.subtitle != null && element.subtitle.trim().length > 0
                                    || element.subtitle != null && element.image_url.trim().length > 0) {
                                    if (element.title.trim().length == 0) {
                                        this.notificationService.showWarning('Nội dung tiêu đề card không được trống nếu bạn có nhập 1 trong các trường khác');
                                        return callback(false);
                                    } else {
                                        if (element.title.trim().length > 80) {
                                            this.notificationService.showWarning('Nội dung tiêu đề card chỉ cho phép tối đa 80 kí tự');
                                            return callback(false);
                                        }
                                        if (element.subtitle.trim().length > 80) {
                                            this.notificationService.showWarning('Nội dung mô tả ngắn của phản hồi card chỉ cho phép tối đa 80 kí tự');
                                            return callback(false);
                                        }
                                    }
                                }

                                if (element.title.trim().length > 0) {
                                    if (element.subtitle != null && element.subtitle.trim().length == 0
                                        && element.image_url != null && element.image_url.trim().length == 0
                                        && (element.buttons.length == 0 || (element.buttons.length > 0 && this.checkButtons(element.buttons) == false))) {
                                        //checkButtons
                                        this.notificationService.showWarning('Url hình ảnh, mô tả ngắn, nội dung nút bấm không được đồng thời để trống');
                                        return callback(false);
                                    }
                                }
                                if (element.buttons.length > 0) {
                                    for (var k = 0; k < element.buttons.length; k++) {
                                        if (element.buttons[k].title.trim().length > 0 || element.buttons[k].payload.trim().length > 0 || (element.buttons[k].url != undefined && element.buttons[k].url.trim().length > 0)) {
                                            if (element.title.trim().length == 0) {
                                                this.notificationService.showWarning('Nội dung tiêu đề card không được trống nếu bạn có nhập 1 trong các trường khác');
                                                return callback(false);
                                            }
                                        }
                                    }
                                }
                            }
                            if (this.responseMessage[i].type == 4) {
                                if (this.responseMessage[i].message.quick_replies.length > 0) {
                                    if (this.responseMessage[i].message.text.trim().length == 0) {
                                        this.quick_reply.nativeElement.focus();
                                        this.notificationService.showWarning('Tiêu đề phản hồi Quick reply không được rỗng');
                                        return callback(false);
                                    }
                                }
                            }
                        }
                    }
                    this.is_check_table = '';
                    return callback(true);
                } catch (error) {
                    return callback(false);
                }
            });
    }

    public checkButtons(lists) {
        var check = false;
        for (var i = 0; i < lists.length; i++) {
            if (lists[i].title != undefined && lists[i].title != null && lists[i].title.trim().length > 0) {
                check = true;
                break;
            }
        }
        return check;
    }


    public contains(lists, obj) {
        var n = 0;
        for (var i = 0; i < lists.length; i++) {
            if (lists[i].param === obj.param) {
                n++;
                if (n == 2) {
                    return false;
                }
            }
        }
        return true;
    }

    public checkName(name) {
        for (var i = 0; i < this.list_intent_filter.length; i++) {
            if (this.list_intent_filter[i].name == name.trim() && this.currentIntent.id != this.list_intent_filter[i].id) {
                return false;
            }
        }

        return true;
    }

    public checkParamOfMemory(param) {
        var check = 0;
        if (this.list_param_name != undefined && this.list_param_name.length > 0) {
            for (var i = 0; i < this.list_param_name.length; i++) {
                if (this.list_param_name[i].toLowerCase() == param.trim().toLowerCase()) {
                    check = 1;
                    break;
                }
            }
        }

        return check;
    }

    public saveIntent() {
        this.is_check_table = '';
        this.validated((cb) => {
            if (!cb) {
                return;
            }
            //Set outler_action, inner_action
            // if (this.is_inner == 'outler_action') {
            //     this.currentIntent.outler_action = this.outler_action;
            //     this.currentIntent.inner_action = null;
            // } else {
            //     this.currentIntent.inner_action = this.inner_action;
            //     this.currentIntent.outler_action = null;
            //     if (this.currentIntent.inner_action != null && this.currentIntent.inner_action.trim().length > 0) {
            //         var check_hanhdong = false;
            //         for (var z = 0; z < this.list_action_full.length; z++) {
            //             if (this.list_action_full[z].nameDisplay.trim() == this.currentIntent.inner_action.trim()) {
            //                 this.currentIntent.inner_action = this.list_action_full[z].name;
            //                 check_hanhdong = true;
            //             }
            //         }
            //         if (check_hanhdong == false) {
            //             this.notificationService.showWarning('Hành động bạn chọn không có trong hệ thống');
            //             return;
            //         }
            //     }
            // }

            this.currentIntent.inner_action = this.inner_action;
            this.currentIntent.outler_action = this.outler_action;
            if (this.currentIntent.inner_action != null && this.currentIntent.inner_action.trim().length > 0) {
                var check_hanhdong = false;
                for (var z = 0; z < this.list_action_full.length; z++) {
                    if (this.list_action_full[z].nameDisplay.trim() == this.currentIntent.inner_action.trim()) {
                        this.currentIntent.inner_action = this.list_action_full[z].name;
                        check_hanhdong = true;
                    }
                }
                if (check_hanhdong == false) {
                    this.notificationService.showWarning('Hành động bạn chọn không có trong hệ thống');
                    return;
                }
            }

            this.currentIntent.status = 1;
            this.currentIntent.intent_contexts = [];
            // Tao du lieu Context In
            if (this.intent_contexts_in.length > 0) {
                for (var i = 0; i < this.intent_contexts_in.length; i++) {
                    this.handleAddContext(this.intent_contexts_in[i].value, 0);
                }
            }
            // Tao du lieu Context Out
            if (this.intent_contexts_out.length > 0) {
                for (var i = 0; i < this.intent_contexts_out.length; i++) {
                    this.handleAddContext(this.intent_contexts_out[i].value, 1);
                }
            }
            this.currentIntent.intent_events = [];
            // Set phan hoi
            this.currentIntent.response_messages = new ResponseMessageModel();
            // Trim phan hoi
            var index_trim = [];
            if (this.responseMessage.length > 0) {
                for (var k = 0; k < this.responseMessage.length; k++) {
                    // phan hoi text
                    if (this.responseMessage[k].type == 1) {
                        var check = 0;
                        for (var z = 0; z < this.responseMessage[k].message.posibleTexts.length; z++) {
                            if (this.responseMessage[k].message.posibleTexts[z].content.trim().length > 0) {
                                check = 1;
                                break;
                            }
                        }
                        if (check == 0) {
                            index_trim.push(this.responseMessage[k]);
                        }
                    }
                    // phan hoi card
                    if (this.responseMessage[k].type == 2) {
                        var check = 0;
                        var element = this.responseMessage[k].message.attachment.payload.elements[0];
                        if (element.image_url.trim().length > 0 || element.title.trim().length > 0 || element.subtitle.trim().length > 0) {
                            check = 1;
                        }
                        if (element.buttons.length > 0) {
                            for (var z = 0; z < element.buttons.length; z++) {
                                if (element.buttons[z].title.trim().length > 0 || element.buttons[z].payload.trim().length > 0 || (element.buttons[z].url != undefined && element.buttons[z].url.trim().length > 0)) {
                                    check = 1;
                                    break;
                                }
                            }
                        }
                        if (check == 0) {
                            index_trim.push(this.responseMessage[k]);
                        }
                        //update lay payload button
                        // for (var z = 0; z < this.responseMessage[k].message.attachment.payload.elements[0].buttons.length; z ++) {
                        //     this.responseMessage[k].message.attachment.payload.elements[0].buttons[z].payload = this.responseMessage[k].message.attachment.payload.elements[0].buttons[z].title;
                        // }
                    }
                    // phan hoi images
                    if (this.responseMessage[k].type == 3) {
                        if (this.responseMessage[k].message.attachment.payload.url.trim().length == 0) {
                            index_trim.push(this.responseMessage[k]);
                        }
                    }
                    // phan hoi nhanh
                    if (this.responseMessage[k].type == 4) {
                        if (this.responseMessage[k].message.quick_replies.length == 0 && this.responseMessage[k].message.text.trim().length == 0) {
                            index_trim.push(this.responseMessage[k]);
                        } else {
                            if (this.responseMessage[k].message.quick_replies.length > 0) {
                                var check = 0;
                                if (check == 0) {
                                    //index_trim.push(this.responseMessage[k]);
                                }
                            }
                        }
                    }
                }
            }
            if (index_trim.length > 0) {
                for (var i = 0; i < index_trim.length; i++) {
                    this.responseMessage = this.responseMessage.filter((res) => {
                        return res != index_trim[i];
                    });
                }
            }
            // Xoa cac file null trong tung res
            if (this.responseMessage.length > 0) {
                for (var k = 0; k < this.responseMessage.length; k++) {
                    if (this.responseMessage[k].type == 1) {
                        this.responseMessage[k].message.posibleTexts = this.responseMessage[k].message.posibleTexts.filter((res) => {
                            return res.content.trim().length > 0;
                        });
                    }
                    if (this.responseMessage[k].type == 2) {
                        this.responseMessage[k].message.attachment.payload.elements[0].buttons = this.responseMessage[k].message.attachment.payload.elements[0].buttons.filter((button) => {
                            return ((button.title != undefined && button.title.trim().length > 0) || (button.payload != undefined && button.payload.trim().length > 0) || (button.url != undefined && button.url.trim().length > 0))
                        });
                    }
                }
            }
            // Update lai cau trc res text
            var tmp_res = this.responseMessage;
            for (var k = 0; k < tmp_res.length; k++) {
                // phan hoi text
                if (tmp_res[k].type == 1) {
                    var tmp_res_text = [];
                    for (var z = 0; z < tmp_res[k].message.posibleTexts.length; z++) {
                        tmp_res_text.push(tmp_res[k].message.posibleTexts[z].content);
                    }
                    tmp_res[k].message.posibleTexts = tmp_res_text;
                }
                if (tmp_res[k].type == 4) {
                    for (var z = 0; z < tmp_res[k].message.quick_replies.length; z++) {
                        tmp_res[k].message.quick_replies[z].content_type = "text";
                        tmp_res[k].message.quick_replies[z].payload = tmp_res[k].message.quick_replies[z].title;
                    }
                }
            }
            for (var k = 0; k < tmp_res.length; k++) {
                // phan hoi text
                if (tmp_res[k].type == 2) {
                    if (tmp_res[k].message.attachment.payload.elements[0].buttons.length > 0) {
                        for (var z = 0; z < tmp_res[k].message.attachment.payload.elements[0].buttons.length; z++) {

                            var content = '';
                            console.log('tmp_res[k].message.attachment.payload.elements[0].buttons[z]', tmp_res[k].message.attachment.payload.elements[0].buttons[z]);
                            if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].url != undefined) {
                                content = tmp_res[k].message.attachment.payload.elements[0].buttons[z].url;
                            }
                            if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload != undefined) {
                                content = tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload;
                            }
                            if (content.indexOf('http') != -1) {
                                tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'web_url';
                                tmp_res[k].message.attachment.payload.elements[0].buttons[z].url = content;
                                delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload;
                            } else {
                                tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'postback';
                                tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = content;

                                if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload.trim().length == 0) {
                                    tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = tmp_res[k].message.attachment.payload.elements[0].buttons[z].title.trim();
                                }
                                delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].url;
                            }
                            if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload == undefined && tmp_res[k].message.attachment.payload.elements[0].buttons[z].url == undefined) {
                                tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = '';
                                if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].title.trim().lenght > 0) {
                                    tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = tmp_res[k].message.attachment.payload.elements[0].buttons[z].title.trim();
                                }
                            }

                            // if (tmp_res[k].message.attachment.payload.elements[0].buttons[z].url != undefined && tmp_res[k].message.attachment.payload.elements[0].buttons[z].url.indexOf('http') != -1) {
                            //     tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'web_url';
                            //     delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload;
                            // } else {
                            //     tmp_res[k].message.attachment.payload.elements[0].buttons[z].type = 'postback';
                            //     tmp_res[k].message.attachment.payload.elements[0].buttons[z].payload = tmp_res[k].message.attachment.payload.elements[0].buttons[z].title;
                            //     delete tmp_res[k].message.attachment.payload.elements[0].buttons[z].url;
                            // }
                        }
                    }
                }
            }
            this.currentIntent.response_messages.content = JSON.stringify(tmp_res);
            // Set intent entities
            if (this.currentIntent.intent_entities != undefined) {
                if (this.currentIntent.intent_entities.length > 0) {
                    for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                        this.currentIntent.intent_entities[i].response_messages = this.currentIntent.intent_entities[i].response_messages.filter((response_messages) => {
                            return response_messages.content !== ''
                        });
                    }
                    for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                        this.currentIntent.intent_entities[i].intent_entities_suggestion = this.currentIntent.intent_entities[i].intent_entities_suggestion.filter((intent_entities_suggestion) => {
                            return intent_entities_suggestion.content !== ''
                        });
                    }
                    // push auto complete user say
                    if (this.auto_intent_entities != undefined) {
                        if (this.auto_intent_entities.length > 0) {
                            for (var i = 0; i < this.auto_intent_entities.length; i++) {
                                this.auto_intent_entities[i].intent_id = this.currentIntent.id;
                                this.currentIntent.intent_entities.push(this.auto_intent_entities[i]);
                            }
                        }
                    }
                } else {
                    // push auto complete user say
                    if (this.auto_intent_entities != undefined) {
                        if (this.auto_intent_entities.length > 0) {
                            for (var i = 0; i < this.auto_intent_entities.length; i++) {
                                this.auto_intent_entities[i].intent_id = this.currentIntent.id;
                                this.currentIntent.intent_entities.push(this.auto_intent_entities[i]);
                            }
                        }
                    }
                }
            }
            //Set id bo nho
            for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                this.currentIntent.intent_entities[i].priority = i + 1;
            }
            this.currentIntent.intent_entities.sort(function (a, b) {
                return (a.priority) - (b.priority);
            });
            // xoa intent entity khong nhap gi ca
            this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((row) => {
                return ((row.param != null && row.param.trim().length > 0) ||
                    (row.entity_name != null && row.entity_name.trim().length > 0) ||
                    row.default_value != null && row.default_value.trim().length > 0 ||
                    (row.response_messages != undefined && row.response_messages.length) > 0 ||
                    (row.intent_entities_suggestion != undefined && row.intent_entities_suggestion.length > 0) ||
                    row.is_required)
            });
            for (var i = 0; i < this.currentIntent.intent_entities.length; i++) {
                if (this.memory_name != undefined && this.memory_name != null && this.memory_name.trim().length > 0) {
                    if (this.checkParamOfMemory(this.currentIntent.intent_entities[i].param) == 1) {
                        this.currentIntent.intent_entities[i].global_memory_param = '&' + this.memory_name + '.' + this.currentIntent.intent_entities[i].param;
                        this.currentIntent.intent_entities[i].global_memory = this.memory_name;
                    }
                }
            }
            this.currentIntent.is_enable_send_action = true;
            this.currentIntent.is_machine_learning = true;
            this.auto_intent_entities = [];
            this.status_update = true;
            this.updateIntent((cb) => {
                // Update lai cau trc phan hoi text
                var is_updae_struct = false;
                ///check lai cau truc
                if (this.responseMessage.length > 0) {
                    for (var i = 0; i < this.responseMessage.length; i++) {
                        if (this.responseMessage[i].type == 1 && this.responseMessage[i].message.posibleTexts.length > 0) {
                            if (this.responseMessage[i].message.posibleTexts[0].content == undefined) {
                                is_updae_struct = true;
                            }
                        }
                    }
                }
                if (is_updae_struct) {
                    for (var i = 0; i < this.responseMessage.length; i++) {
                        if (this.responseMessage[i].type == 1) {
                            var tmp = [];
                            if (this.responseMessage[i].message != undefined && this.responseMessage[i].message.posibleTexts != undefined) {
                                for (var k = 0; k < this.responseMessage[i].message.posibleTexts.length; k++) {
                                    tmp.push({ 'content': this.responseMessage[i].message.posibleTexts[k] });
                                }
                            }
                            this.responseMessage[i].message.posibleTexts = tmp;
                        }
                    }
                }
                let that = this;
                if (!cb) {
                    setTimeout(function () {
                        that.router.navigate(['/pages/intents/intentList'], { relativeTo: this.route });
                    }, 1000);
                    this.notificationService.showDanger('Cập nhật ý định thất bại');
                }
                setTimeout(function () {
                    that.status_update = false;
                }, 1000);
            });
        });
    }

    public updateIntent(callback) {
        this.currentIntent.agent_id = localStorage.getItem('currentAgent_Id');
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.update(this.currentIntent);
        actionOperate.subscribe(
            (intent_update) => {
                this.notificationService.showSuccess('Cập nhật ý định "' + this.currentIntent.name + '" thành công');
                setTimeout(function () {
                    window.location.reload();
                }, 500);
                callback(true);
            },
            (err) => {
                this.notificationService.showWarning(err);
                // load event fail
                callback(false);
            });
    }

    public addIntent(callback) {
        let actionOperate: Observable<IntentModel>;
        actionOperate = this.intentService.add(this.currentIntent);
        actionOperate.subscribe(
            (data) => {
                callback(true);
            },
            (err) => {
                // load event fail
                callback(false);
            });
    }

    public getIntentInfo() {
        this.currentIntent.name = '';
        this.currentIntent.agent_id = localStorage.getItem('currentAgent_Id');
        this.currentIntent.type = 1;
        this.currentIntent.is_system = false;
        this.currentIntent.prioriy = 0;
    }

    /**
     * LAY DANH SACH CONTEXT CHO AUTOCOMPLAT
     * @memberOf IntentNewComponent
     */
    public getListContext() {
        let actionOperate: Observable<ContextModel[]>;
        actionOperate = this.intentService.getListContextByAgentId(this.currentAgentId);
        actionOperate.subscribe(
            (data) => {
                this.contexts = data;
                let names = _.pluck(data, 'name');
                $('.tagsContextIn').tagsinput({
                    typeahead: {
                        source: names
                    },
                    freeInput: true
                });
                $('.tagsContextOut').tagsinput({
                    typeahead: {
                        source: names
                    },
                    freeInput: true
                });
            },
            (err) => {
                // load event fail
            });
    }

    /**
     * DANH SACH OBJECT MEMORY
     *
     * @memberOf IntentNewComponent
     */
    public getListObjectMemory() {
        let actionOperate: Observable<ObjectMemoryModel[]>;
        actionOperate = this.objectService.getListObjectMemoryByAgent(this.currentAgentId);
        actionOperate.subscribe(
            (data) => {
                this.objects = data;
            },
            (err) => {
            });
    }

    public getListEntity() {
        this.entityService.getListEntityPromise(this.currentAgentId, true)
            .then((data) => {
                this.entities = data;
                this.states = _.pluck(data, 'name');
            })
    }


    public initIntentView() {
        $('.tagsContextIn').tagsinput({
            typeahead: {
                source: ['', '']
            },
            freeInput: true
        });
        let actionOperateIt: Observable<any>;
        actionOperateIt = this.intentService.initIntentView(this.currentAgentId, true);
        actionOperateIt.subscribe(
            (data) => {
                this.entities = data.entites;
                this.contexts = data.contexts;
                this.objects = data.objectMemories;
                if (this.objects.length > 0) {

                    this.objects.sort(function (a, b) {
                        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        // names must be equal
                        return 0;
                    });

                    this.list_object_name = [];
                    for (var i = 0; i < this.objects.length; i++) {
                        this.list_object_name.push(this.objects[i].name);
                    }
                }
                this.states = _.pluck(this.entities, 'name');
                let context = _.pluck(this.contexts, 'name');
                this.tagsContextInAutoComplete = context;
                this.tagsContextOutAutoComplete = context;
                $('.tagsContextIn').tagsinput({
                    typeahead: {
                        source: context
                    },
                    freeInput: true
                });
                $('.tagsContextOut').tagsinput({
                    typeahead: {
                        source: context
                    },
                    freeInput: true
                });
                this.loadIntentEdit();
            },
            (err) => {
                // load event fail
            });


        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListActionBasicNotAbstractByAgentId(this.currentAgentId);
        actionOperate.subscribe(
            (group_actions) => {
                this.load_action_status = true;
                if (group_actions !== null) {
                    this.list_action_full = group_actions;
                    this.list_actions = [];
                    if (group_actions != undefined && group_actions.length > 0) {
                        for (var j = 0; j < group_actions.length; j++) {
                            this.list_actions.push(group_actions[j].nameDisplay);
                        }
                        this.list_actions.sort(function (a, b) {
                            var nameA = a.toLowerCase(), nameB = b.toLowerCase()
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            // names must be equal
                            return 0;
                        });

                    }
                }
            },
            (err) => {
            });

    }

    public getIntentByName(name, agentId, callback) {
        try {
            let actionOperate: Observable<IntentModel>;
            actionOperate = this.intentService.getIntentByParams(name, agentId);
            actionOperate.subscribe(
                (data) => {
                    callback(true);
                },
                (err) => {
                    // load event fail
                    callback(false);
                });
        } catch (error) {
            callback(false);
        }
    }

    public getObjectMemoryDetail(id, callback) {
        let actionOperate: Observable<ObjectMemoryModel>;
        actionOperate = this.objectService.getObjectMemoryById(id);
        actionOperate.subscribe(
            (data) => {
                callback(data);
            },
            (err) => {
                // load event fail
                callback(false);
            });
    }

    public clickDivHD() {
        this.is_show_hanhdong = !this.is_show_hanhdong;
        console.log('this.is_show_hanhdonxxxg', this.is_show_hanhdong);
    }

    private validators = [this.validateTaginput];
    public errorMessages = {
        'mess_err@': 'Ngữ cảnh cho phép nhập [a-z], [A-Z], [0-9] và _',
        'mess_err_lenght@': 'Ngữ cảnh cho phép nhập tối đa 50 kí tự'
    };

    // Validate tag ngu canh
    public validateTaginput(control: FormControl) {
        if (control.value != undefined && control.value != null && control.value.trim().length > 50) {
            return {
                'mess_err_lenght@': true
            };
        }
        if (!StringFormat.checkRegexp(control.value, /^[a-zA-Z0-9_]+$/)) {
            return {
                'mess_err@': true
            };
        }

    }

    private validatorsQuickReply = [this.validateTaginputGoiY];
    public errorMessagesQuickReply = {
        'mess_err_lenght@': 'Gợi ý chỉ cho phép nhập tối đa 25 kí tự.'
    };

    public validateTaginputGoiY(control: FormControl) {
        if (control.value.trim().length > 50) {
            return {
                'mess_err_lenght@': true
            };
        }
    }

    private validators_event = [this.validateTaginputEvent];
    public errorMessages_event = {
        'mess_err@': 'Sự kiện cho phép nhập kí tự [a-z], [A-Z], [0-9] và _',
        'mess_err_lenght@': 'Sự kiện cho phép nhập tối đa 50 kí tự'
    };

    // Validate tag su kien
    public validateTaginputEvent(control: FormControl) {
        if (!StringFormat.checkRegexp(control.value, /^[a-zA-Z0-9_]+$/)) {
            return {
                'mess_err@': true
            };
        }
        if (control.value.trim().length > 50) {
            return {
                'mess_err_lenght@': true
            };
        }
    }

    private duplicateIntent(intentId) {
        swal({
          title: 'Bạn muốn duplicate ý định này?',
          text: '',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Có',
          cancelButtonText: 'Không',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(() => {
          this.intentService.duplicateIntentById(intentId)
            .then((data) => {
              this.router.navigated = false;
              if (data.type === 1) {
                this.router.navigateByUrl('/pages/intents/intentEdit/' + data.id);
              }
              if (data.type === 2) {
                this.router.navigateByUrl('/pages/intents/intentEdit/callback/' + data.id);
              }
              window.location.reload();
            });
    
        }, (dismiss) => {
        });
      }
}