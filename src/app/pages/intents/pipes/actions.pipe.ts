import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { StringFormat } from '../../../themes/validators';

@Pipe({
  name: 'intentPipe',
  pure: false
})
export class IntentPipe implements PipeTransform {
    public transform(data: any[], searchTerm: string, type_filter: string): any[] {
        if (type_filter == 'Tìm tên ý định, ngữ cảnh') {
            console.log('xxxxxxx');
            searchTerm = StringFormat.formatText(searchTerm);
            if (searchTerm != null && searchTerm.length > 0) {
                return data.filter(item => {
                    return (StringFormat.formatText(searchTerm).indexOf(item.name) !== -1 || StringFormat.formatText(searchTerm).indexOf(item.group_context_in) !== -1 || StringFormat.formatText(item.group_context_out).indexOf(searchTerm) !== -1)
                });
            } else {
                return data;
            }
        } else {
            return data;
        }
    }
}
