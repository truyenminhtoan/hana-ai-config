import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import * as _ from 'underscore';
import { StringFormat } from '../../../../../themes/validators/string.format';

@Pipe({
  name: 'filterIntentPipe',
  pure: false
})
export class FilterDataPipe implements PipeTransform {
  // tslint:disable-next-line:member-access
  public transform(data: any[], searchTerm: string): any[] {
    searchTerm = StringFormat.formatText(searchTerm);
    // return data.filter(item => {
    //   return StringFormat.formatText(item.name).indexOf(searchTerm) !== -1
    // });

    //console.log('data', JSON.stringify(data));

    return data.filter(item => {
      return (StringFormat.formatText(item.name_full).indexOf(StringFormat.formatText(searchTerm)) !== -1)
    });
  }
}
