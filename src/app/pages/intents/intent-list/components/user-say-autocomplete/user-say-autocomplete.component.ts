import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {StringFormat} from "../../../../../themes/validators/string.format";
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from "../../../../../_services/compoent-interaction-service";
import {Observable} from "rxjs/Observable";
import {IntentService} from "../../../../../_services/intents.service";
import {IntentFilterModel} from "../../../../../_models/intents_filter";

@Component({
    selector: 'user-say-autocomplete',
    templateUrl: 'user-say-autocomplete.component.html',
    styleUrls: ['user-say-autocomplete.component.scss']
})
export class UserSerSayAutoComponent {

    @Input() query_default: any;
    @Input() usersays: any;
    @Output() value_select = new EventEmitter<any>();

    @ViewChild('input_auto') input_auto: ElementRef;
    stateCtrl: FormControl;
    filteredStates: any;
    private eventSubscription: Subscription;

    constructor(private _componentService: ComponentInteractionService, private intentService: IntentService) {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.usersays = [];
        this.stateCtrl = new FormControl();
        var that = this;
        setTimeout(function () {
            this.usersays = [];
            if (that.usersays.length == 0) {
                that.loadUserSay();
            }
        }, 1000);
    }

    public loadUserSay () {

        this.getListIntentFilterByAgentId(localStorage.getItem('currentAgent_Id'), (data) => {
            if (data != undefined && data.length > 0) {
                this.usersays = [];
                for (var i = 0; i < data.length; i++) {
                    // this.usersays.push(StringFormat.clearTextVi(data[i].usersay));
                    this.usersays.push(data[i].usersay);
                    this.usersays = this.usersays.filter((row) => {
                        return (row != undefined && row != null && row.trim().length > 0)
                    });

                    console.log('this.usersays', this.usersays);
                }
            }
        });
    }

    public getListIntentFilterByAgentId(agentId: string, callback) {
        let actionOperate: Observable<IntentFilterModel[]>;
        actionOperate = this.intentService.getListIntentFilterByAgentId(agentId);
        // Subscribe to observable
        actionOperate.subscribe(
            (data) => {
                if (data) {
                    callback(data);
                } else {
                    callback([]);
                }
            },
            (err) => {
                callback(false);
            });
    }

    check() {
        this.filteredStates = [];
        //clear
        //this.query_default = '';
        //this.filteredStates = this.usersays;

        var tmp_list = [];
        tmp_list.push({'value': this.query_default, 'display': this.query_default});
        this.value_select.emit(tmp_list);
        // if (this.query_default != undefined && this.query_default.trim().length > 0) {
        //     this.filteredStates = [];
        // } else {
        //     this.filteredStates = this.usersays;
        // }

        //this._componentService.eventPublisher$.next('auto-search-intent-client');
    }

    check2() {
        this.filteredStates = [];
        if (this.query_default != undefined && this.query_default.trim().length > 0) {
            this.filteredStates = this.filterStates(this.query_default);
        } else {
            this.filteredStates = [];
        }

        var tmp_list = [];
        tmp_list.push({'value': this.query_default, 'display': this.query_default});
        this.value_select.emit(tmp_list);
        // this._componentService.eventPublisher$.next('auto-search-intent-client');
    }

    selectValue (val) {
        this.query_default = val;
        var tmp_list = [];
        tmp_list.push({'value': this.query_default, 'display': this.query_default});
        this.value_select.emit(tmp_list);
    }

    filterStates(val: string) {
        var val_select =  val ? this.usersays.filter(item => {
            return (StringFormat.formatText(item).indexOf(StringFormat.formatText(val.trim())) !== -1)
        }) : this.usersays;
        return val_select;
    }

    enterSelect(event, val) {

        //this._componentService.eventPublisher$.next('auto-search-intent-client');

        this.query_default = val;
        var tmp_list = [];
        tmp_list.push({'value': val, 'display': val});
        this.value_select.emit(tmp_list);
    }

    search() {
        //auto-search-intent-client
        // if (this.query_default != undefined && this.query_default != null && this.query_default.trim().length > 0) {
        //     this._componentService.eventPublisher$.next('auto-search-intent-client');
        // }
        this._componentService.eventPublisher$.next('auto-search-intent-client');
    }

}