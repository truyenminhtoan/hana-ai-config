import { Component, Input, OnInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { ViewChild, ViewChildren } from '@angular/core';
import 'rxjs/add/operator/startWith';
import { Observable } from 'rxjs/Observable';
import { IntentService } from '../../../../../_services/index';
import {IntentDontunderstandModel} from "../../../../../_models/intents_dontunderstand";
import {NotificationService} from "../../../../../_services/notification.service";
import {ListFilterIntentModel, FilterIntentModel} from "../../../../../_models/intents_filter";
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from "../../../../../_services/compoent-interaction-service";
declare var $: any;
declare var swal: any;

@Component({
  selector: 'search-data-table-list',
  templateUrl: 'search-data.component.html',
  styleUrls: ['search-data.component.scss']
})
export class SearchDataTableListComponent implements OnInit {
  // tslint:disable-next-line:member-access
  @Input() searchId: string;
  // tslint:disable-next-line:member-access
  @Input() listId: string;
  // tslint:disable-next-line:member-access
  @ViewChildren('inputSearch') inputSearch;

  private filterType = [];
  private currentFilterType;
  private intentDontunderstands: IntentDontunderstandModel[] = [];
  public filterQuery = "";
  public rowsOnPage = 3;
  public sortBy = "content";
  public sortOrder = "asc";

  private type_search='Câu nói của người dùng';
  private searchTypes = [{
      key: 'Câu nói của người dùng',
      value: 'Câu nói của người dùng'
    },{
      key: 'Tìm tên ý định, ngữ cảnh',
      value: 'Tìm tên ý định, ngữ cảnh'
    }
  ];

  @Input() query_old: any;
  @Output() value_filter = new EventEmitter<any>();
  @Output() type_filter = new EventEmitter<any>();
  @Output() intent_usersays_filter = new EventEmitter<any>();
  @Output() userSayDontunderstand = new EventEmitter<any>();
  private listUserSayDontunderstand = [];
  private eventSubscription: Subscription;


  constructor(private intentService: IntentService,  private notificationService: NotificationService, private _componentService: ComponentInteractionService) {

  }

  public ngOnInit() {
    this.loadData();
    this.eventSubscription = this._componentService.eventReceiver$.subscribe(
        event => {
          if (event == 'load-intents') {
            this.loadData();
          }
        }
    );
  }

  public loadData() {
    this.loadFilterType();
    this.currentFilterType = this.filterType[0];
    this.getIntentDontunderstand();
  }
  
  private setValueFilter(value) {
    this.intent_usersays_filter.emit([value]);
    this.value_filter.emit(value);
    this.type_filter.emit(this.type_search);

    // tu dong search
    this._componentService.eventPublisher$.next('auto-search');
  }

  private getIntentDontunderstand() {
    let agentId = localStorage.getItem('currentAgent_Id');
    if (agentId) {
      this.intentService.getDontunderstand(agentId)
          .then((data) => {
            this.intentDontunderstands = data;

            console.log('this.intentDontunderstands', JSON.stringify(this.intentDontunderstands));

            this.userSayDontunderstand.emit(this.listUserSayDontunderstand);
          });
    }
  }

  private loadFilterType() {
    this.filterType = [];
    this.filterType.push({ type: 1, name: 'Tìm tên ý định, sự kiện, ngữ cảnh' });
    this.filterType.push({ type: 2, name: 'Câu nói của người dùng' });
  }

  public delete(id: string) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.intentService.deleteIntentFiltertById(id)
          .then((data) => {
            this._componentService.eventPublisher$.next('load-intents');
            this.notificationService.showSuccess('Xóa Câu nói của người dùng thành công');
            this.getIntentDontunderstand();
          });
    }, (dismiss) => {
      console.log(dismiss);
    });
  }
  
}
