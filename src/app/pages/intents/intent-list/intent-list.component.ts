import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, ViewChildren, ElementRef } from '@angular/core';
import { NotificationService } from "../../../_services/notification.service";
import { EntityService } from "../../../_services/entity.service";
import { ObjectMemoryService } from "../../../_services/object_memory.service";
import { IntentService } from "../../../_services/intents.service";
import { EventsService } from "../../../_services/events.service";
import { IntentModel, ContextModel } from "../../../_models/intents.model";
import { Observable } from "rxjs/Rx";
import { IntentFilterModel } from "../../../_models/intents_filter";
import { ComponentInteractionService } from "../../../_services/compoent-interaction-service";
import { Subscription } from 'rxjs/Rx';
import { StringFormat } from "../../../themes/validators/string.format";
declare var $: any;
declare var swal: any;
@Component({
    selector: 'intent-list',
    templateUrl: 'intent-list.component.html',
    styleUrls: ['intent-list.component.scss']
})
export class IntentListComponent implements AfterViewInit, OnInit {
    private searchId = 'SEARCH_COMPONENT_LIST';
    private listId = 'INTENT_COMPONENT_LIST';
    private lists: IntentModel[] = [];
    private agentId = '1';
    private filterQuery = "";
    private rowsOnPage = 10;
    private sortBy = "name";
    private sortOrder = "asc";
    private id_loading = true;
    private value_filter = "";
    private type_filter = "";
    private list_filter = [];
    private list_filter_fix = [];
    private list_filter_save = [];
    private value_filter_search = '';
    private type_search = 'Câu nói của người dùng';
    private selected = 'Câu nói của người dùng';
    private searchTypes = [{
        key: 'Câu nói của người dùng',
        value: 'Câu nói của người dùng'
    }, {
        key: 'Tìm tên ý định, ngữ cảnh',
        value: 'Tìm tên ý định, ngữ cảnh'
    }
    ];
    private value_no_enter = '';
    private is_loading_search = false;

    private id_auto_content = true;
    private contexts: ContextModel[] = [];
    private tagsContextInAutoComplete = [];
    private intent_contexts_in_filter = [];
    private intent_usersays_filter = [];
    private currentAgentId = '';
    private eventSubscription: Subscription;
    private userSayDontunderstand = ['', ''];
    private userSayDontunderstand2 = ['', ''];
    private tmp_text_tag_ngucanh_no_enter = '';
    private value_select_user_say = '';

    @ViewChild('list_intents') private list_intents: ElementRef;

    constructor(private intentService: IntentService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService, private route: ActivatedRoute,
        public router: Router, ) {
        this.loadIntentLists();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-intents') {
                    this.loadIntentLists();
                }
                if (event == 'auto-search') {
                    this.type_search = 'Câu nói của người dùng';
                    this.selected = 'Câu nói của người dùng';
                    this.filterUserSayDontunderstand();
                }
                if (event == 'auto-search-intent-client') {
                    this.filterIntent();
                }
            }
        );
        this.notificationService.clearConsole();
    }

    public onAddAutoContent(event) {
        if (this.intent_contexts_in_filter.length > 0) {
            for (var i = 0; i < this.intent_contexts_in_filter.length; i++) {
                if (this.intent_contexts_in_filter[i].display != undefined && this.intent_contexts_in_filter[i].value != undefined) {
                    this.intent_contexts_in_filter[i].display = this.intent_contexts_in_filter[i].value;
                }
            }
        }
        var that = this;
        this.is_loading_search = true;
        setTimeout(function () {
            that.filterIntent();
            that.is_loading_search = false;
        }, 700);
    }

    public filterUserSayDontunderstand() {
        this.sortBy = "";
        this.is_loading_search = true;
        var intent_filter = new IntentFilterModel();
        intent_filter.agent_id = localStorage.getItem('currentAgent_Id');
        var list_search = [];
        for (var i = 0; i < this.intent_usersays_filter.length; i++) {
            if (this.intent_usersays_filter[i].display != undefined) {
                list_search.push(this.intent_usersays_filter[i].display);
            } else {
                list_search.push(this.intent_usersays_filter[i]);
            }
        }
        intent_filter.usersay = list_search[0];
        if (list_search.length > 0) {
            this.intentService.searchDontunderstand(intent_filter)
                .then((res) => {

                    var data = res.data;
                    for (var i = 0; i < data.length; i++) {
                        data[i].name_full = data[i].name;
                        data[i].usersay_full = data[i].usersay;
                        if (data[i].group_context_in != null) {
                            data[i].group_context_in = data[i].group_context_in.replace(',', ', ');
                            var tmp_list_context = data[i].group_context_in.split(',');
                            if (tmp_list_context.length > 1) {
                                var string_context = '';
                                for (var k = 0; k < tmp_list_context.length - 1; k++) {
                                    string_context += tmp_list_context[k].trim() + ', ';
                                }
                                string_context += tmp_list_context[tmp_list_context.length - 1].trim();
                                data[i].group_context_in_full = string_context;
                            } else {
                                data[i].group_context_in_full = data[i].group_context_in;
                            }
                            data[i].group_context_in = data[i].group_context_in.slice(0, 20);
                            if (data[i].group_context_in.length >= 20) {
                                data[i].group_context_in += '...';
                            }
                        }
                        if (data[i].usersay != null) {
                            data[i].usersay = data[i].usersay.slice(0, 20);
                            if (data[i].usersay.length >= 20) {
                                data[i].usersay += '...';
                            }
                        }
                        if (data[i].name != null) {
                            data[i].name = data[i].name.slice(0, 20);
                            if (data[i].name.length >= 20) {
                                data[i].name += '...';
                            }
                        }
                    }
                    this.list_filter = data;
                    this.is_loading_search = false;
                });
        } else {
            this.is_loading_search = false;
            this.list_filter = this.list_filter_fix;
        }
    }

    private getIntentDontunderstand() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.intentService.getDontunderstand(agentId)
                .then((data) => {
                    if (data != undefined) {
                        this.userSayDontunderstand = [];
                        this.userSayDontunderstand2 = [];
                        for (var i = 0; i < data.length; i++) {
                            this.userSayDontunderstand.push(data[i].content);
                            this.userSayDontunderstand2.push(data[i].content);
                        }
                    }
                });
        }
    }

    public loadIntentLists() {
        this.currentAgentId = localStorage.getItem('currentAgent_Id');
        if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
            this.loadList();
            this.getIntentDontunderstand();
            this.getAutoLoadContext();
        } else {
            this.notificationService.showNoAgentId();
        }
        $('body').scrollTop(800);
    }

    ngOnInit() {
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-intents') {
                    this.getAutoLoadContext();
                    //this.getIntentDontunderstand();
                    this.loadList();
                }
            }
        );
    }

    public getAutoLoadContext() {
        let actionOperateIt: Observable<any>;
        actionOperateIt = this.intentService.initIntentView(this.currentAgentId, true);
        actionOperateIt.subscribe(
            (data) => {
                if (data != undefined) {
                    for (var i = 0; i < data.contexts.length; i++) {
                        this.tagsContextInAutoComplete.push(data.contexts[i].name);
                    }
                    console.log('tagsContextInAutoComplete', this.tagsContextInAutoComplete);
                }
                this.id_auto_content = false;
            },
            (err) => {
                // load event fail
                this.notificationService.showDanger("Xảy ra lỗi load danh sách autocomplete ngữ cảnh & tên ý định");
            });
    }

    public getListIntentFilterByAgentId(agentId: string, callback) {
        let actionOperate: Observable<IntentFilterModel[]>;
        actionOperate = this.intentService.getListIntentFilterByAgentId(agentId);
        // Subscribe to observable
        actionOperate.subscribe(
            (data) => {
                if (data) {
                    callback(data);
                } else {
                    callback([]);
                }
            },
            (err) => {
                callback(false);
            });
    }

    public onRemove(event) {
        this.filterIntent();
    }

    public onTextChangeContext(value) {
        this.value_no_enter = value;
    }

    public filterIntent() {
        var intent_filter = new IntentFilterModel();
        intent_filter.agent_id = localStorage.getItem('currentAgent_Id');
        switch (this.type_search) {
            case 'Câu nói của người dùng':
                this.sortBy = "";
                this.is_loading_search = true;
                if (this.value_filter != null && this.value_filter.trim().length > 0) {
                    var intent_filter = new IntentFilterModel();
                    intent_filter.agent_id = localStorage.getItem('currentAgent_Id');
                    var list_search = [];
                    list_search.push(this.value_filter);
                    intent_filter.usersay = list_search[0];
                    if (list_search.length > 0) {
                        this.intentService.searchDontunderstand(intent_filter)
                            .then((res) => {
                                var data = res.data;
                                for (var i = 0; i < data.length; i++) {
                                    data[i].name_full = data[i].name;
                                    data[i].usersay_full = data[i].usersay;
                                    if (data[i].group_context_in != null) {
                                        data[i].group_context_in = data[i].group_context_in.replace(',', ', ');
                                        var tmp_list_context = data[i].group_context_in.split(',');
                                        if (tmp_list_context.length > 1) {
                                            var string_context = '';
                                            for (var k = 0; k < tmp_list_context.length - 1; k++) {
                                                string_context += tmp_list_context[k].trim() + ', ';
                                            }
                                            string_context += tmp_list_context[tmp_list_context.length - 1].trim();
                                            data[i].group_context_in_full = string_context;
                                        } else {
                                            data[i].group_context_in_full = data[i].group_context_in;
                                        }

                                        data[i].group_context_in = data[i].group_context_in.slice(0, 20);
                                        if (data[i].group_context_in.length >= 20) {
                                            data[i].group_context_in += '...';
                                        }
                                    }
                                    if (data[i].usersay != null) {
                                        data[i].usersay = data[i].usersay.slice(0, 20);
                                        if (data[i].usersay.length >= 20) {
                                            data[i].usersay += '...';
                                        }
                                    }
                                    if (data[i].name != null) {
                                        data[i].name = data[i].name.slice(0, 20);
                                        if (data[i].name.length >= 20) {
                                            data[i].name += '...';
                                        }
                                    }
                                }
                                this.is_loading_search = false;
                                this.list_filter = data;

                            });
                    } else {
                        this.is_loading_search = false;
                        this.list_filter = this.list_filter_fix;
                    }
                } else {
                    this.is_loading_search = false;
                    this.list_filter = this.list_filter_fix;
                }
                this.list_filter.sort(function (a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                //console.log('list_filter', this.list_filter);
                break;
            case 'Tìm tên ý định, ngữ cảnh':
                this.sortBy = "name";
                if (this.intent_contexts_in_filter != null && this.intent_contexts_in_filter.length > 0) {
                    var list_value_filter_tmp = [];
                    if (this.value_no_enter != undefined && this.value_no_enter != null && this.value_no_enter.trim().length > 0) {
                        list_value_filter_tmp.push({ 'value': this.value_no_enter, 'display': this.value_no_enter });
                    }
                    if (this.intent_contexts_in_filter.length > 0) {
                        for (var i = 0; i < this.intent_contexts_in_filter.length; i++) {
                            list_value_filter_tmp.push(this.intent_contexts_in_filter[i]);
                        }
                    }
                    //list_value_filter_tmp = this.intent_contexts_in_filter;
                    var intent_contexts_in_filter_tmp = [];
                    for (var i = 0; i < this.list_filter_fix.length; i++) {
                        if (this.list_filter_fix[i].name_full != undefined) {
                            if (this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_out.trim())
                                || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_in.trim())
                                || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].name_full.trim())
                            ) {
                                intent_contexts_in_filter_tmp.push(this.list_filter_fix[i]);
                            }
                        } else {
                            if (this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_out.trim())
                                || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_in.trim())
                                || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].name.trim())
                            ) {
                                intent_contexts_in_filter_tmp.push(this.list_filter_fix[i]);
                            }
                        }
                    }
                    this.list_filter = intent_contexts_in_filter_tmp;
                } else {
                    //xu ly tim khi chua tao tag
                    var list_value_filter_tmp = [];
                    if (this.value_no_enter != undefined && this.value_no_enter != null && this.value_no_enter.trim().length > 0) {
                        list_value_filter_tmp.push({ 'value': this.value_no_enter, 'display': this.value_no_enter });
                        var intent_contexts_in_filter_tmp = [];
                        for (var i = 0; i < this.list_filter_fix.length; i++) {
                            if (this.list_filter_fix[i].name_full != undefined) {
                                //group_context_out
                                if (this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_out.trim())
                                    || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_in.trim())
                                    || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].name_full.trim())
                                ) {
                                    intent_contexts_in_filter_tmp.push(this.list_filter_fix[i]);
                                }
                            } else {
                                if (this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_out.trim())
                                    || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].group_context_in.trim())
                                    || this.filterString(list_value_filter_tmp, this.list_filter_fix[i].name.trim())
                                ) {
                                    intent_contexts_in_filter_tmp.push(this.list_filter_fix[i]);
                                }
                            }
                        }
                        this.list_filter = intent_contexts_in_filter_tmp;


                    } else {
                        this.list_filter = this.list_filter_fix;
                    }
                }
                this.list_filter.sort(function (a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                break;
        }
    }

    public filterString(list, value) {
        if (list.length > 0 && value != null && value != undefined) {
            for (var i = 0; i < list.length; i++) {
                if (value.indexOf(list[i].value.trim()) != -1) {
                    return true;
                }
                // clearTextVi
                var tmp_list_item = StringFormat.clearTextVi(list[i].value);
                var value_tmp = StringFormat.clearTextVi(value);
                if (value_tmp.indexOf(tmp_list_item) != -1) {
                    return true;
                }
            }
        }
        return false;
    }

    public loadList() {
        this.getListIntentFilterByAgentId(localStorage.getItem('currentAgent_Id'), (data) => {
            if (data != undefined && data.length > 0) {
                this.userSayDontunderstand2 = [];
                this.userSayDontunderstand = [];
                this.tagsContextInAutoComplete = [];
                for (var i = 0; i < data.length; i++) {
                    data[i].name_full = data[i].name;
                    data[i].usersay_full = data[i].usersay;
                    this.userSayDontunderstand2.push(data[i].usersay_full);
                    this.userSayDontunderstand.push(StringFormat.clearTextVi(data[i].usersay_full));
                    if (data[i].group_context_in != null) {
                        data[i].group_context_in = data[i].group_context_in.replace(',', ', ');
                        var tmp_list_context = data[i].group_context_in.split(',');
                        if (tmp_list_context.length > 1) {
                            var string_context = '';
                            for (var k = 0; k < tmp_list_context.length - 1; k++) {
                                string_context += tmp_list_context[k].trim() + ', ';
                            }
                            string_context += tmp_list_context[tmp_list_context.length - 1].trim();
                            data[i].group_context_in_full = string_context;
                        } else {
                            data[i].group_context_in_full = data[i].group_context_in;
                        }

                        data[i].group_context_in = data[i].group_context_in.slice(0, 20);
                        if (data[i].group_context_in.length >= 20) {
                            data[i].group_context_in += '...';
                        }
                    }
                    if (data[i].usersay != null) {
                        data[i].usersay = data[i].usersay.slice(0, 20);
                        if (data[i].usersay.length >= 20) {
                            data[i].usersay += '...';
                        }
                    }
                    if (data[i].name != null) {
                        data[i].name = data[i].name.slice(0, 20);
                        if (data[i].name.length >= 20) {
                            data[i].name += '...';
                        }
                        this.tagsContextInAutoComplete.push({ 'display': StringFormat.clearTextVi(data[i].name_full) + ' ' + data[i].name_full, 'value': data[i].name_full });
                    }
                    /** @Tam them */
                    // 1. Vào màn hình chức năng danh sách ý định
                    // 2. Chuyển qua kiểu tìm kiếm theo tên ý định và ngữ cảnh
                    // 3. Gõ một tên ngữ cảnh đã tồn tại
                    // KQ: không suggestion tên ngữ cảnh đã tồn tại
                    // MM: Suggestion đc tên ngữ cảnh đã tồn tại
                    this.id_auto_content = true;
                    let actionOperateIt: Observable<any>;
                    actionOperateIt = this.intentService.initIntentView(this.currentAgentId, true);
                    actionOperateIt.subscribe(
                        (data1) => {
                            if (data1 != undefined && data1.contexts != undefined) {
                                for (let i = 0; i < data1.contexts.length; i++) {
                                    if (this.checkExitsContext(data1.contexts[i].name)) {
                                        this.tagsContextInAutoComplete.push({ 'display': data1.contexts[i].name, 'value': data1.contexts[i].name });
                                    }
                                }
                            }

                            let tmp = this.tagsContextInAutoComplete;
                            this.tagsContextInAutoComplete = [];
                            for (let i = 0; i < tmp.length; i++) {
                                if (this.checkExitsContext(tmp[i].value)) {
                                    this.tagsContextInAutoComplete.push(tmp[i]);
                                }
                            }

                            this.id_auto_content = false;
                        },
                        (err) => {
                            // load event fail
                            this.notificationService.showDanger("Xảy ra lỗi load danh sách autocomplete ngữ cảnh & tên ý định");
                        });
                }
                this.userSayDontunderstand2 = this.userSayDontunderstand2.filter((row) => {
                    return (row != undefined && row != null && row.trim().length > 0)
                });
            }
            this.list_filter = data;
            this.list_filter_save = data;
            this.list_filter_fix = data;
            this.id_loading = false;
        });
    }

    private checkExitsContext(value) {
        for (let i = 0; i < this.tagsContextInAutoComplete.length; i++) {
            if (this.tagsContextInAutoComplete[i].value === value) {
                return false;
            }
        }

        return true;

    }


    public requestAutocompleteItems = (text: string): any => {
        return this.tagsContextInAutoComplete;
    };

    public ngAfterViewInit() {
    }

    public search() {
        this.value_filter_search = this.value_filter;
    }

    public search_change() {
        if (this.value_filter != null && this.value_filter.trim().length == 0) {
            this.value_filter_search = '';
        }
    }

    private onTextChange($event) {
        this.tmp_text_tag_ngucanh_no_enter = $event;
    }

    public deleteIntentById(id) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.intentService.deleteIntent(id)
                .then((data) => {
                    this._componentService.eventPublisher$.next('load-intents');
                    this.notificationService.showSuccess('Xóa ý định "' + data[0].name + '" thành công');
                });
            $('body').removeClass("modal-open");
        }, (dismiss) => {
            console.log(dismiss);
        });
    }

    private duplicateIntent(intentId) {
        swal({
            title: 'Bạn muốn duplicate ý định này?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.intentService.duplicateIntentById(intentId)
                .then((data) => {
                    this.router.navigated = false;
                    if (data.type === 1) {
                        this.router.navigateByUrl('/pages/intents/intentEdit/' + data.id);
                    }
                    if (data.type === 2) {
                        this.router.navigateByUrl('/pages/intents/intentEdit/callback/' + data.id);
                    }
                });

        }, (dismiss) => {
        });
    }

}
