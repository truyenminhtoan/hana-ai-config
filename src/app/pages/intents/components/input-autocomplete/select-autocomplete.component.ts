
import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {IntentEntityModel} from "../../../../_models/intents.model";

@Component({
  selector: 'intent-input-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class IntentInputAutoComplete {

  public filteredList = [];
  public elementRef;
  selectedIdx: number;
  @Input() sources: any;
  @Input() intent_entities: IntentEntityModel[] = [];
  @Input() intent_entities_old: IntentEntityModel[] = [];

  @Output() auto_new_intent_entities = new EventEmitter<any>();
  @Input() query: any;
  @Output() output = new EventEmitter<any>();
  private query_string = '';

  constructor(myElement: ElementRef) {
    this.elementRef = myElement;
    this.selectedIdx = -1;
  }

  filter(event) {
    for (var i = 0; i < this.sources.length; i++) {
      if (this.sources[i].name.indexOf('@') == -1) {
        this.sources[i].name = '@' + this.sources[i].name;
      }
    }
    this.filteredList = this.sources.filter(function(el){
      return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
    }.bind(this));

    var tmp = event.target.value.split(" ");
    if (tmp[tmp.length - 1] == '@') {
      this.filteredList = this.sources;
    } else {
      if (this.query.indexOf('@') > -1) {
        this.filteredList = this.sources.filter(function(el){
          return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
        }.bind(this));
      }
    }

    this.output.emit(this.query);

  }

  setAutoComplete() {
    if (this.query[this.query.length-1] != ' ' && this.query[this.query.length-1] != '' && this.query[this.query.length-1]!=':' && this.query[this.query.length-1] != '@') {
      var res = this.query.split(" @");
      if (res.length > 0) {
        var tmp_value_query = res[0].trim().split(":");
        if (tmp_value_query.length > 1) {
          let intentEntity: IntentEntityModel = new IntentEntityModel();
          intentEntity.is_required = false;
          intentEntity.param = tmp_value_query[1];
          intentEntity.entity_name = tmp_value_query[0].replace('@', '');
          intentEntity.default_value = '1';
          intentEntity.priority = 0;
          intentEntity.status = 1 ;
          intentEntity.global_memory_param = null;
          intentEntity.response_messages = [];
          intentEntity.intent_entities_suggestion = [];
          if (!this.intent_entities || this.intent_entities.length == undefined) {
            var intent_entitiesl: IntentEntityModel[] = [];
            if (!this.containsObject(res[0].trim(), intent_entitiesl)) {
              intent_entitiesl.push(intentEntity);
              this.auto_new_intent_entities.emit(intent_entitiesl);
            }
          } else {
            if (this.intent_entities_old != undefined && this.intent_entities_old.length > 0) {
              if (!this.containsObject(res[0].trim(), this.intent_entities) && !this.containsObject(res[0].trim(), this.intent_entities_old)) {
                this.intent_entities.push(intentEntity);
                this.auto_new_intent_entities.emit(this.intent_entities);
              }
            } else {
              this.intent_entities.push(intentEntity);
              this.auto_new_intent_entities.emit(this.intent_entities);
            }
          }
        }
      }
    }
  }

  containsObject(obj, list) {
    if (list.length == 0) {
      return false;
    }
    var i;
    for (i = 0; i < list.length; i++) {
      if (list[i].param === obj) {
        return true;
      }
    }

    return false;
  }

  filter_click() {
    this.filteredList = this.sources;
  }

  select(item){
    this.query = this.query + ' ' + item.replace('@', '') + ':';
    this.query = this.query.replace('@ ', '@');
    var tmp = this.query;
    this.output.emit(tmp);
    this.filteredList = [];
  }

  handleBlur() {
    if (this.selectedIdx > -1) {
      this.query = this.filteredList[this.selectedIdx];
    }
    this.filteredList = [];
    this.selectedIdx = -1;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.selectedIdx = -1;
  }


}