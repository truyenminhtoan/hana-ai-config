import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {IntentEntityModel, UserSayModel} from "../../../../_models/intents.model";
import {StringFormat} from "../../../../themes/validators/string.format";
@Component({
    selector: 'intent-user-say-auto',
    templateUrl: 'intent-user-say-autocomplete.html',
    styleUrls: ['intent-user-say-autocomplete.scss']
})
export class IntentUserSayAutoComponent {
    
    @Input() query_default: any;
    @Input() intent_entities: any;
    @Output() value_select = new EventEmitter<any>();
    @Output() intent_entities_new = new EventEmitter<any>();
    @Input() index: any;
    @ViewChild('input_auto') input_auto_tmp: ElementRef;
    stateCtrl: FormControl;
    filteredStates: any;
    //old
    @Input() intent_entities_old: IntentEntityModel[] = [];
    @Input() intent_entities_auto_input: IntentEntityModel[] = [];
    @Input() intent_entities_memory: IntentEntityModel[] = [];
    @Input() intent_entities_tranning: IntentEntityModel[] = [];
    @Input() usersays: UserSayModel[] = [];
    @Input() list_param_name_memory = [];

    @Output() auto_new_intent_entities = new EventEmitter<any>();
    private value_pre = '';

    private index_end_select = '';
    private index_start = '';

    constructor() {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        //this.stateCtrl = new FormControl();
    }

    private index_auto = -1;
    private index_auto_key = -1;
    private valude_search = '';

    private start_string = '';
    private end_string = '';
    private enter_select = false;

    check(event) {
        this.getValueOld(event);
        let that = this;
        setTimeout(() => {
            that.onCheck(event);
        }, 10);
        setTimeout(() => {
            // xoa cac param trung lap nhau tren nhieu usersays, cai nay quan trong
            that.clearDoubleValueUserSayPre(this.getCustomId());
            // xoa cac param trung lap nhau tren 1 usersays, cai nay quan trong
            that.clearParamInOneCustomId(this.getCustomId());
            // xoa cac row khong ton tai cua usersay hien tai
            that.clearRowTrashByCustomId(this.getCustomId());
        }, 1500);
    }

    clearRowTrashByCustomId(custom_id) {
        var content = '';
        for (var i = 0; i < this.usersays.length; i++) {
            if (this.usersays[i].custom_id != undefined && this.usersays[i].custom_id == custom_id) {
                content = this.usersays[i].content;
            }
        }
        if (content.trim().length > 0) {
            var list_tmp = content.split('@');
            var list_rows = [];
            if (list_tmp.length > 0) {
                for (var j = 0; j < list_tmp.length; j++) {
                    list_rows.push(list_tmp[j].trim());
                }
            }
            var list_params = [];
            if (list_rows.length > 0) {
                for (var j = 0; j < list_rows.length; j++) {
                    var arr_param = list_rows[j].split(':');
                    if (arr_param[1] != undefined) {
                        var param = arr_param[1].split(' ')[0];
                        list_params.push(param);
                    }
                }
            }
            var row_deletes = [];
            if (list_params.length > 0) {
                for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
                    if (this.intent_entities_auto_input[i].custom_id != undefined && this.intent_entities_auto_input[i].custom_id == custom_id) {
                        if (!this._check_pram(list_params, this.intent_entities_auto_input[i].param)) {
                            row_deletes.push(this.intent_entities_auto_input[i]);
                        }
                    }
                }
            }
            for (var z = 0; z < row_deletes.length; z++) {
                this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                    return (row != row_deletes[z])
                });
            }
            this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
        } else {
            // neu content rong
            this.deleteAllRowByUserSay(custom_id);
        }
    }

    _check_pram(params, param) {
        for (var i = 0; i < params.length; i++) {
            if (params[i].trim() == param.trim()) {
                return true;
            }
        }
        return false;
    }

    onPaste(event) {
        // xoa cai rong
        this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
            return (row.custom_id == undefined || (row.custom_id != undefined && row.custom_id != this.getCustomId()))
        });
        this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
    }

    onCheck(event) {
        console.log('event.target.selectionEnd', event);
        if (event.key != 'Enter') {
            //this.deleteAllRowByUserSay(this.getCustomId());
            if (event.code == 'ControlLeft' || event.key == 'Backspace') {
                //cap nhat lai cac row can xoa cua custom_is hien tai
                //this.deleteAllRowByUserSay(this.getCustomId());
                this.updateDelete(this.getCustomId(), []);
            }
            var index_auto = -1;
            if (event.key == '@' || event.key == 'Shift') {
                this.index_auto_key = event.target.selectionStart;
            }
            index_auto = event.target.selectionEnd;
            // console.log('index_auto_key', this.index_auto_key);
            // console.log('index_auto',index_auto);
            this.valude_search = event.target.value.slice(this.index_auto_key - 1, index_auto);
            // console.log('value_search', this.valude_search);
            this.index_end_select = event.target.value.slice(index_auto, event.target.value.length)
            this.index_start =  event.target.value.slice(0, this.index_auto_key - 1);
            // console.log('gia tr trc:', this.index_start);
            // console.log('gia tr sau:', this.index_end_select);
            if ( this.valude_search.indexOf('@') != -1 &&  this.valude_search.trim().length > 0 &&  this.valude_search.indexOf(':') == -1) {
                this.filteredStates  =  this.intent_entities.filter(item => {
                    return (StringFormat.formatText(item).indexOf(StringFormat.formatText( this.valude_search.replace('@', '').trim())) !== -1)
                }) ;
            } else {
                this.filteredStates = [];
            }
        }
        this.query_default =  event.target.value;
        this.value_select.emit(this.query_default);
        let that = this;
        setTimeout(() => {
            that.autoRow();
            that.updateDelete(this.getCustomId(), []);
        }, 600);
    }

    deleteRowByUserSay(custom_id) {
        if (this.intent_entities_auto_input != undefined) {
            this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                return (
                    row.custom_id == undefined
                || (!this.kiem_tra_trun_param_bo_nho(row.param.trim()))        // giu lai param bo nho
                || (row.custom_id != undefined && row.custom_id != custom_id && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                )
            });
            // update cau noi user_say
            localStorage.setItem('user_say_'+ custom_id, '');
            this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
        }
    }

    deleteAllRowByUserSay(custom_id) {
        if (this.intent_entities_auto_input != undefined) {
            this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                return (
                    row.param.trim().length > 0 &&
                    (row.custom_id == undefined
                        || (!this.kiem_tra_trun_param_bo_nho(row.param.trim()))        // giu lai param bo nho
                        || (row.custom_id != undefined && row.custom_id != custom_id && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                    )
                )
            });
            this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
        }
    }

    autoRow() {
        if (this.query_default != undefined) {
            // xoa entity_name rong
            var tmp_intent_entities = this.intent_entities.filter((entity) => {
                return ((entity.param != undefined && entity.param != null && entity.param.trim().length > 0) ||
                (entity.entity_name != undefined && entity.entity_name != null && entity.entity_name.trim().length > 0))
            });
            if (tmp_intent_entities != undefined) {
                this.intent_entities_new.emit(tmp_intent_entities);
            }
            // xoa cai rong
            this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                return (row.param.trim().length > 0)
            });

            if (this.intent_entities_auto_input.length == 0) {
                //this.addRow();
            }

            //co 2 cach lay custom_id
            if (this.index == undefined) {
                var custom_id = this.findCustomID(this.query_default);
                var list_user_says = [];
                if (this.usersays != undefined && this.usersays.length > 0) {
                    for (var i = 0; i < this.usersays.length; i++) {
                        if (this.usersays[i].custom_id == custom_id) {
                            var tmp_user_say_string = this.usersays[i].content;
                            var list_tmp = tmp_user_say_string.split('@');
                            if (list_tmp.length > 0) {
                                for (var j = 0; j < list_tmp.length; j++) {
                                    list_user_says.push(list_tmp[j].trim());
                                }
                            }
                        }
                    }
                }
            } else {
                var custom_id = this.usersays[this.index].custom_id;
                var list_user_says = [];
                var tmp_user_say_string = this.usersays[this.index].content;
                var list_tmp = tmp_user_say_string.split('@');
                if (list_tmp.length > 0) {
                    for (var j = 0; j < list_tmp.length; j++) {
                        list_user_says.push(list_tmp[j].trim());
                    }
                }
            }
            this.clearTrash(custom_id);
            list_user_says = list_user_says.filter((row) => {
                return (row != undefined && row != null && row.trim().length > 0 && row.indexOf(':') != -1)
            });
            if (list_user_says.length > 0) {
                // them moi hoac cap nhat
                for (var i = 0; i < list_user_says.length; i++) {
                    var arr_param = list_user_says[i].split(':');
                    var param = arr_param[1];
                    var entity_name = arr_param[0];

                    if (entity_name.trim().length > 0 && param.trim().length > 0) {
                        let intentEntity: IntentEntityModel = new IntentEntityModel();
                        intentEntity.custom_id = custom_id;
                        intentEntity.param = param.split(' ')[0].trim();
                        intentEntity.entity_name = entity_name;
                        intentEntity.default_value = '';
                        intentEntity.priority = 0;
                        intentEntity.status = 1 ;
                        intentEntity.global_memory_param = null;
                        intentEntity.response_messages = [];
                        intentEntity.intent_entities_suggestion = [];
                        if (intentEntity.param.trim().length > 0) {
                            var that_calback = this;
                            this.updateOrAdd(intentEntity, i, custom_id, list_user_says, function (custom_id) {
                                setTimeout(() => {
                                    //that_calback.clearDoubleValueUserSayPre(custom_id);
                                }, 1000);
                            });
                        }
                    }
                }

                // xoa cac row co param trung param bo nho
                let that = this;
                setTimeout(() => {
                    that.intent_entities_auto_input = that.intent_entities_auto_input.filter((row) => {
                        return (
                            row.custom_id == undefined
                            || (row.custom_id != undefined && row.custom_id != custom_id)
                            || (row.custom_id != undefined && row.custom_id == custom_id && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                            || (row.custom_id != undefined && row.custom_id == '' && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                        )
                    });
                    that.auto_new_intent_entities.emit(that.intent_entities_auto_input);
                    that.clearTrash(custom_id);
                }, 500);

                this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
            }
            this.updateDelete(custom_id, list_user_says);
        }
    }

    clearTrash(custom_id) {
        let that = this;
        setTimeout(() => {
            that.intent_entities_auto_input = that.intent_entities_auto_input.filter((row) => {
                return (
                    row.custom_id == undefined
                    || (row.custom_id != undefined && row.custom_id != custom_id)
                    || (row.custom_id != undefined && row.custom_id == custom_id && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                    || (row.custom_id != undefined && row.custom_id == '' && this.kiem_tra_trun_param_bo_nho(row.param.trim()))
                )
            });
            that.auto_new_intent_entities.emit(that.intent_entities_auto_input);
        }, 500);
    }

    clickInput(event) {
        //this.filteredStates = [];
    }

    getValueOld(event) {
        // this.filteredStates = [];
        //co 2 cach lay custom_id
        if (this.index == undefined) {
            var custom_id = this.findCustomID(this.query_default);
            var list_user_says = [];
            if (this.usersays != undefined && this.usersays.length > 0) {
                for (var i = 0; i < this.usersays.length; i++) {
                    if (this.usersays[i] != undefined && this.usersays[i].custom_id == custom_id && this.usersays[i].content != undefined) {
                        var tmp_user_say_string = this.usersays[i].content;
                        var list_tmp = tmp_user_say_string.split('@');
                        if (list_tmp.length > 0) {
                            for (var j = 0; j < list_tmp.length; j++) {
                                list_user_says.push(list_tmp[j].trim());
                            }
                        }
                    }
                }
            }
        } else {
            if (this.usersays[this.index] != undefined && this.usersays[this.index].custom_id != undefined) {
                var custom_id = this.usersays[this.index].custom_id;
                var list_user_says = [];
                var tmp_user_say_string = this.usersays[this.index].content;
                var list_tmp = tmp_user_say_string.split('@');
                if (list_tmp.length > 0) {
                    for (var j = 0; j < list_tmp.length; j++) {
                        list_user_says.push(list_tmp[j].trim());
                    }
                }
            }
        }
        // update cau noi user_say
        localStorage.setItem('user_say_'+ custom_id, this.query_default);
    }

    updateOrAdd(obj, index, custom_id, list_user_says, callback) {
        var sum = -1;
        var check_them = true;
        for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
            if (this.intent_entities_auto_input[i].custom_id != undefined
                && this.intent_entities_auto_input[i].custom_id == obj.custom_id ) {
                sum++;
                if (sum == index) {
                    //cap nhat, truoc khi cap nhat cung phai kiem tra gia tri param cua dong cu, xem param hien tai co trung voi param cu bo nho hay khong, neu trung, de nguyen dong cu va them dong moi vao
                    if (this.kiem_tra_trun_param_bo_nho(this.intent_entities_auto_input[i].param)) {
                        // gia tri moi
                        if (this.kiem_tra_trun_param_bo_nho(obj.param.trim())) {
                            //neu khong trung
                            if (this.intent_entities_auto_input[i].custom_id != undefined && this.intent_entities_auto_input[i].custom_id == obj.custom_id) {
                                this.intent_entities_auto_input[i].param = obj.param;
                                this.intent_entities_auto_input[i].entity_name = obj.entity_name;
                            }
                        }
                    } else {
                        // loai bo row co param trung voi param bo nho ra khoi cac dieu kien check cua custom_id
                        this.intent_entities_auto_input[i].custom_id = '';
                        //neu trung voi param bo nho, giu nguyen dong cu, them dong moi
                        obj.is_check_param = true;
                        if (!this.kiem_tra_trung(obj.param, obj.entity_name )) {
                            if (this.kiem_tra_khai_niem_ton_tai(obj.entity_name.trim())) {
                                if (this.list_param_name_memory != undefined) {
                                    //kiem tra co trung param bo nho hay khong
                                    if (this.kiem_tra_trun_param_bo_nho(obj.param)) {
                                        this.intent_entities_auto_input.push(obj);
                                    }
                                } else {
                                    this.intent_entities_auto_input.push(obj);
                                }
                                callback(custom_id);
                                this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
                                var param_check = '';
                                var entity_name_check = '';
                                //lay gia tri param, entity_name cua row is_check_param  moi nhat, vi truong hop nay co the trung
                                for (var k = this.intent_entities_auto_input.length - 1; k >= 0; k--) {
                                    if ( this.intent_entities_auto_input[k].is_check_param != undefined) {
                                        param_check = this.intent_entities_auto_input[k].param;
                                        entity_name_check = this.intent_entities_auto_input[k].entity_name;
                                        break;
                                    }
                                }
                                //xoa het cai is_check_param, them lai dong moi
                                this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                                    return (row.is_check_param == undefined || (row.is_check_param != undefined && this.kiem_tra_co_trong_usersays_list(list_user_says, row.param.trim())))
                                });
                                obj.param = param_check;
                                obj.entity_name = entity_name_check;
                                obj.is_check_param = true;
                                if (!this.kiem_tra_trung(obj.param, obj.entity_name )) {
                                    if (this.kiem_tra_khai_niem_ton_tai(obj.entity_name.trim())) {
                                        this.intent_entities_auto_input.push(obj);
                                    }
                                }
                            }
                        }
                    }
                    check_them = false;
                }
            }
        }

        if (check_them) {
            if (!this.kiem_tra_trung(obj.param, obj.entity_name )) {
                if (this.kiem_tra_khai_niem_ton_tai(obj.entity_name.trim())) {
                    if (this.list_param_name_memory != undefined) {
                        //kiem tra co trung param bo nho hay khong
                        if (this.kiem_tra_trun_param_bo_nho(obj.param)) {
                            callback(custom_id);
                            this.intent_entities_auto_input.push(obj);
                        }
                    } else {
                        this.intent_entities_auto_input.push(obj);
                        callback(custom_id);
                    }
                }
            }
        }
    }

    // Ham cap nhat xoa cac gia tri trung cua tat ca cac cau usersays
    clearTrungDuLieu() {
        // for (var i = 0; i < this.usersays.length; i++) {
        //     // so sanh voi usersay truoc do
        //     for (var j = 0; j < i; j++) {
        //         if (this.usersays[i].custom_id != undefined) {
        //             this.updateDelete(this.usersays[i].custom_id, []);
        //         }
        //     }
        // }
    }

    // Ham xoa cac row co param trung voi usersay trc do
    clearDoubleValueUserSayPre(custom_id) {
        var index = -1;
        for (var i = 0; i < this.usersays.length; i++) {
            if (this.usersays[i].custom_id != undefined && this.usersays[i].custom_id == custom_id) {
                index = i;
            }
        }
        if (index != -1) {
            //chi update tu thang hien tai tro ve sau
            var row_deletes = [];
            // console.log('this.usersays', this.usersays[index]);
            // console.log('index', index);
            // console.log('this.intent_entities_auto_input', this.intent_entities_auto_input);
            for (var i = index; i < this.usersays.length; i++) {
                if (this.usersays[i].custom_id != undefined) {
                    for (var j = 0; j < this.intent_entities_auto_input.length; j++) {
                        if (this.intent_entities_auto_input[j].custom_id != undefined && this.intent_entities_auto_input[j].custom_id == this.usersays[i].custom_id) {
                            //so sanh voi cac row phia trc xem co trung param hay khong
                            if (this.check_pram(this.intent_entities_auto_input[j].param, this.intent_entities_auto_input[j].custom_id)) {
                                row_deletes.push(this.intent_entities_auto_input[j]);
                            }
                        }
                    }
                }
            }
            // console.log('row_deletes', row_deletes);
            for (var z = 0; z < row_deletes.length; z++) {
                this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                    return (row != row_deletes[z])
                });
            }
            this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
        }
    }

    clearParamInOneCustomId(custom_id) {
        var row_deletes = [];
        for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
            if (this.intent_entities_auto_input[i].custom_id != undefined && this.intent_entities_auto_input[i].custom_id == custom_id) {
                for (var j = i + 1; j < this.intent_entities_auto_input.length; j++) {
                    if (this.intent_entities_auto_input[j].custom_id != undefined && this.intent_entities_auto_input[j].custom_id == custom_id) {
                        if (this.intent_entities_auto_input[i].param.trim() == this.intent_entities_auto_input[j].param.trim()) {
                            //neu trung  param tren cung 1 custom_id
                            row_deletes.push(this.intent_entities_auto_input[i]);
                        }
                    }
                }
            }
        }
        for (var z = 0; z < row_deletes.length; z++) {
            this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                return (row != row_deletes[z])
            });
        }
        this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
    }

    // so sanh voi cac row phia trc xem co trung param hay khong
    check_pram(param, custom_id) {
        for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
            if (this.intent_entities_auto_input[i] != undefined && this.intent_entities_auto_input[i].custom_id != undefined && this.intent_entities_auto_input[i].custom_id != custom_id && this.intent_entities_auto_input[i].param.trim() == param.trim()) {
                return true;
            }
        }
        return false;
    }

    kiem_tra_co_trong_usersays_list(list_user_says, param) {
        for (var i = 0; i < list_user_says.length; i++) {
            if (list_user_says[i].trim() == param.trim()) {
                return true;
            }
        }
        return false;
    }

    public kiem_tra_trun_param_bo_nho(param) {
        var is_check_add = true;
        // Kiem tra ten khai niem nhap vao cong trong he thong hay khong moi cho them vao bang ben duoi
        if ( this.list_param_name_memory.length > 0) {
            for (var i =0 ; i <  this.list_param_name_memory.length; i++) {
                if ( this.list_param_name_memory[i].trim() == param.trim()) {
                    is_check_add = false;
                    break;
                }
            }
        }

        return is_check_add;
    }

    public kiem_tra_khai_niem_ton_tai(ten_khai_niem) {
        var is_check_add = 0;
        // Kiem tra ten khai niem nhap vao cong trong he thong hay khong moi cho them vao bang ben duoi
        if ( this.intent_entities.length > 0) {
            for (var i =0 ; i <  this.intent_entities.length; i++) {
                if ( this.intent_entities[i] == '@'+ten_khai_niem.trim()) {
                    is_check_add = 1;
                }
            }
        }
        return is_check_add;
    }



    kiem_tra_trung(param, entity_name) {
        for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
            if (this.intent_entities_auto_input[i].param.trim() == param.trim()) {
                return true;
            }
        }
        return false;
    }

    findCustomID(value) {
        for (var i = 0; i < this.usersays.length; i++) {
            if (this.usersays[i].content != undefined) {
                if (this.usersays[i].content.trim() == value.trim()) {
                    return this.usersays[i].custom_id;
                }
            }
        }
        return '';
    }

    selectValue (val) {
    }

    enterSelect(event, val, val_pre, my_input) {
        this.enter_select = true;
        //dat con tro
        let that = this;
        setTimeout(() => {
            if (that.index_end_select != undefined && that.index_end_select.trim().length > 0) {
                that.query_default = that.index_start + val + ':' + that.index_end_select;
            } else {
                that.query_default = that.index_start + val + ':';
            }
            // fix loi khong co khoang trang trc '@'
            that.spaceResulst();

            that.value_select.emit(that.query_default.trim());
            //that.input_auto_tmp.nativeElement.focus();
            // that.input_auto_tmp.nativeElement.selectionEnd = 2;
            // that.input_auto_tmp.nativeElement.selectionStart = 2;
            that.filteredStates = [];
            setTimeout(() => {
                if (that.index_end_select.trim().length == 0) {
                    var index_contro = (that.index_start + val).length + 2;
                } else {
                    var index_contro = (that.index_start + val).length + 1;
                }
                this.setSelectionRange(my_input, index_contro, index_contro);
                that.index_start = that.index_end_select = '';
            }, 200);
        }, 200);
    }

    spaceResulst() {
        this.query_default = this.query_default.replace(/_+_/g, '_'); // thay thế 2_ thành 1_
        var string_res = '';
        var arrar_string = this.query_default.split('');
        for (var i = 0; i < arrar_string.length; i++) {
            if (arrar_string[i] == '@') {
               if (this.query_default.charAt(i - 1) != ' ') {
                   string_res += ' ' + arrar_string[i];
               } else {
                   string_res += arrar_string[i];
               }
            } else {
                string_res += arrar_string[i];
            }
        }
        this.query_default = string_res.trim();
    }

    setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

    updateDelete(custom_id, list_user_says) {
        // get cau noi user_say
        var value_pre = localStorage.getItem('user_say_'+ custom_id);
        if (value_pre != null && value_pre != undefined && this.query_default != undefined && this.query_default != null) {
            if (this.query_default.trim().length > 0) {
                var list_usersay_pre = value_pre.split('@');
                list_usersay_pre = list_usersay_pre.filter((row) => {
                    return (row != undefined && row != null && row.trim().length > 0 && row.indexOf(':') != -1)
                });
                var list_usersay_query_default = this.query_default.split('@');
                list_usersay_query_default = list_usersay_query_default.filter((row) => {
                    return (row != undefined && row != null && row.trim().length > 0 && row.indexOf(':') != -1)
                });
                var n_value_pre = list_usersay_pre.length;
                var n_query_default = list_usersay_query_default.length;
                if (n_value_pre > n_query_default) {
                    var list_index_delete = [];
                    for ( var k = 0; k < list_usersay_pre.length; k++) {
                        if (this.query_default.trim().indexOf(list_usersay_pre[k].trim()) == -1) {
                            list_index_delete.push(k);
                        }
                    }
                    var rows_delete = this.getUpdateDeleteByIndex(custom_id, list_index_delete);
                    if (rows_delete.length > 0) {
                        for (var z = 0; z < rows_delete.length; z++) {
                            this.intent_entities_auto_input = this.intent_entities_auto_input.filter((row) => {
                                return (row != rows_delete[z])
                            });
                        }
                        this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
                        // update cau noi user_say
                        localStorage.setItem('user_say_'+ custom_id, this.query_default);
                    }
                }
            }
        }
    }

    getUpdateDeleteByIndex(custom_id, indexs_delete) {
        var rows_delete = [];
        if (this.intent_entities_auto_input.length > 0 && indexs_delete.length > 0) {
            for (var k = 0; k < indexs_delete.length ; k++) {
                var index_cur = 0;
                for (var i = 0; i < this.intent_entities_auto_input.length; i++) {
                    if (this.intent_entities_auto_input[i].custom_id != undefined
                        && this.intent_entities_auto_input[i].custom_id == custom_id ) {
                        if (indexs_delete[k] == index_cur) {
                            rows_delete.push(this.intent_entities_auto_input[i]);
                        }
                        index_cur++;
                    }
                }
            }
        }
        return rows_delete;
    }

    addRow() {
        let intentEntity: IntentEntityModel = new IntentEntityModel();
        intentEntity.param = '';
        intentEntity.entity_name = '';
        intentEntity.default_value = '';
        intentEntity.priority = 0;
        intentEntity.status = 1 ;
        intentEntity.global_memory_param = null;
        intentEntity.response_messages = [];
        intentEntity.intent_entities_suggestion = [];
        this.intent_entities_auto_input.push(intentEntity);
        this.auto_new_intent_entities.emit(this.intent_entities_auto_input);
    }

    getCustomId() {
        var custom_id = '';
        //co 2 cach lay custom_id
        if (this.usersays[this.index] != undefined && this.usersays[this.index].custom_id != undefined) {
            custom_id = this.usersays[this.index].custom_id;
            var list_user_says = [];
            var tmp_user_say_string = this.usersays[this.index].content;
            var list_tmp = tmp_user_say_string.split('@');
            if (list_tmp.length > 0) {
                for (var j = 0; j < list_tmp.length; j++) {
                    list_user_says.push(list_tmp[j].trim());
                }
            }
        }
        console.log('custom_id', custom_id);
        return custom_id;
    }


}