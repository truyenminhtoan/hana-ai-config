import { Component, Input, Output, EventEmitter, ElementRef, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
    selector: 'res-card-component',
    templateUrl: 'res-card-component.component.html',
    styleUrls: ['res-card-component.component.scss']
})

export class ResCardComponent {

    @Input() res: any;
    @Input() index: any;
    private is_block = false;
    private is_block_nutbam = false;
    @ViewChild('title_input') title_input: ElementRef;
    @Output() res_output = new EventEmitter<any>();
    @Output() is_submit: any;

    private is_block_80_title = false;
    private is_block_80_subtitle = false;
    
    constructor(myElement: ElementRef) {

    }

    public addNutBam() {
        if (this.res.message.attachment.payload.elements[0].buttons.length < 3) {
            this.res.message.attachment.payload.elements[0].buttons.push({ type: 'postback', title: '', payload: '' });
            this.checkBlock();
        }
    }

    public deleteButtonCard(button) {
        this.res.message.attachment.payload.elements[0].buttons = this.res.message.attachment.payload.elements[0].buttons.filter((row) => {
            return row !== button
        });
        this.checkBlock();
    }
    
    public checkBlock() {
        if (this.res.message.attachment.payload.elements[0].buttons.length >= 3) {
            this.is_block_nutbam = true;
        } else {
            this.is_block_nutbam = false;
        }
        //this.checkValidate();
    }

    
    public checkValidate() {
        let that = this;
        setTimeout(function () {
            //that.checkTitle();
        }, 500);
        if (this.res != undefined) {
            console.log('this.res', this.res);
            this.res_output.emit(this.res);
        }

    }
    public checkTitle() {
        var check_button = false;
        if (this.res.message.attachment.payload.elements[0].buttons.length > 0) {
            for (var k = 0; k < this.res.message.attachment.payload.elements[0].buttons.length; k++) {
                if (
                    (this.res.message.attachment.payload.elements[0].buttons[k].item_url != undefined && this.res.message.attachment.payload.elements[0].buttons[k].item_url.trim().length > 0) ||
                    (this.res.message.attachment.payload.elements[0].buttons[k].payload != undefined && this.res.message.attachment.payload.elements[0].buttons[k].payload.trim().length > 0) ||
                    (this.res.message.attachment.payload.elements[0].buttons[k].title != undefined && this.res.message.attachment.payload.elements[0].buttons[k].title.trim().length > 0)
                ) {
                    check_button = true;
                }


            }
        }

        if ((this.res.message.attachment.payload.elements[0].buttons.length > 0 && check_button)
        || (this.res.message.attachment.payload.elements[0].image_url != null && this.res.message.attachment.payload.elements[0].image_url.trim().length > 0)
        || (this.res.message.attachment.payload.elements[0].subtitle != null && this.res.message.attachment.payload.elements[0].subtitle.trim().length >0)) {

            //console.log('xxxxxxxxxxxx');
            if (this.res.message.attachment.payload.elements[0].title == null || (this.res.message.attachment.payload.elements[0].title != null && this.res.message.attachment.payload.elements[0].title.trim().length == 0)) {
                //this.title_input.nativeElement.focus();
                this.is_block = true;
                //this.is_submit.emit(true);
            } else {
                this.is_block = false;

                if (this.res.message.attachment.payload.elements[0].title.trim().length > 80) {
                    this.is_block_80_title = true;
                } else {
                    this.is_block_80_title = false;
                }
                if (this.res.message.attachment.payload.elements[0].subtitle.trim().length > 80) {
                    this.is_block_80_subtitle = true;
                } else {
                    this.is_block_80_subtitle = false;;
                }
            }
        } else {
            this.is_block = false;
            this.is_block_80_title = false;
        }
    }
}