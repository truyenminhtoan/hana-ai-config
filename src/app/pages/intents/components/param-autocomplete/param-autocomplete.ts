import { Component, Input, Output, EventEmitter} from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {EntityService} from "../../../../_services/entity.service";
import {StringFormat} from "../../../../themes/validators/string.format";
import {ComponentInteractionService} from "../../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
import * as _ from 'underscore';

@Component({
    selector: 'param-autocomplete',
    templateUrl: 'param-autocomplete.html',
    styleUrls: ['param-autocomplete.scss']
})
export class ParamAutoComponent {

    @Input() query_default: any;
    @Input() entities_name: any;
    @Output() value_select = new EventEmitter<any>();
    @Input() index: any;
    @Input() type: any;
    stateCtrl: FormControl;
    filteredStates: any;
    private selected = '';
    private eventSubscription: Subscription;

    private selectedCountry = '';
    constructor(private entitieService: EntityService, private _componentService: ComponentInteractionService) {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();
    }

    check(event) {
        if (this.query_default) {
            this.filteredStates = this.filterStates(this.query_default);
        } else {
            if (this.entities_name != undefined) {
                var non_duplidated_data = _.uniq(this.entities_name);
                non_duplidated_data.sort(function(a, b) {
                    var nameA = a.toLowerCase(), nameB = b.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                this.filteredStates = non_duplidated_data;
            } else {
                this.filteredStates = [];
            }

        }
        this.value_select.emit(this.query_default);
    }

    selectValue (obj) {
        localStorage.setItem('index-param', this.index);
        localStorage.setItem('type-param', this.type);
        this._componentService.eventPublisher$.next('auto-select-entity');
        this.value_select.emit(obj);
    }

    filterStates(val: string) {
        val = val.trim();
        if (this.entities_name != undefined) {
            var non_duplidated_data = _.uniq(this.entities_name);
            non_duplidated_data.sort(function(a, b) {
                var nameA = a.toLowerCase(), nameB = b.toLowerCase()
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            });
            return non_duplidated_data.filter(item => {
                return (StringFormat.formatText(item).indexOf(val.trim().toLowerCase()) !== -1)
            });
        }
    }

    enterSelect(event, obj) {
        //console.log('event', event);
        if (event.source != undefined) {
            obj = event.source.value;
        }
        localStorage.setItem('index-param', this.index);
        localStorage.setItem('type-param', this.type);
        localStorage.setItem('obj_param_cur', obj);
        this._componentService.eventPublisher$.next('auto-select-entity');
        this.value_select.emit(obj);
    }
    
}