import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
    selector: 'intent-action-auto',
    templateUrl: 'intents_action_auto_complete.html',
    styleUrls: ['intents_action_auto_complete.scss']
})
export class IntentActionAutoComponent {

    @Input() query_default: any;
    @Input() list_actions: any;
    @Output() value_select = new EventEmitter<any>();

    stateCtrl: FormControl;
    filteredStates: any;
    private selected = '';

    constructor() {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();
    }

    check() {
        if (this.query_default) {
            this.filteredStates = this.filterStates(this.query_default);
        } else {
            this.filteredStates = this.list_actions.slice();
        }
        this.value_select.emit(this.query_default);
    }

    selectValue (obj) {
        this.value_select.emit(obj);
    }

    filterStates(val: string) {
        if (val != null) {
            val = val.trim();
        }
        return this.list_actions.filter(option => new RegExp(`^${val}`, 'gi').test(option));
    }

    enterSelect($event, obj) {
        this.value_select.emit(obj);
    }

    clickAddEntity() {

    }
}