import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {ActionBasic} from "../../../../_models/actions.model";
import {IntentModel, IntentEntityModel} from "../../../../_models/intents.model";
import {ObjectMemoryService} from "../../../../_services/object_memory.service";
import {ObjectMemoryModel} from "../../../../_models/object_memory.model";
import {Observable} from "rxjs/Rx";
import {StringFormat} from "../../../../themes/validators/string.format";
@Component({
    selector: 'object-auto-complete',
    templateUrl: 'object_auto_complete.html',
    styleUrls: ['object_auto_complete.scss']
})
export class ObjectAutoComponent {

    @Input() query_default: any;
    @Input() list_actions: any;
    @Input() objects: any;
    @Output() value_select = new EventEmitter<any>();

    stateCtrl: FormControl;
    filteredStates: any;
    private selected = '';

    // tslint:disable-next-line:member-access
    @Input() source: any;
    @Output() output = new EventEmitter<any>();
    @Output() memory_id = new EventEmitter<any>();
    private currentIntent: IntentModel= new IntentModel();
    @Input() selectDefault: any;
    @Input() user_say_intent_entities: any;
    @Output() output_user_say_intent_entitie = new EventEmitter<any>();
    @Input() listEntyties: any;
    @Output() memory_name = new EventEmitter<any>();
    @Output() list_param_name = new EventEmitter<any>();
    @Input() auto_intent_entities: any;
    @Output() auto_intent_entities_new = new EventEmitter<any>();


    @Output() auto_intent_entities_full = new EventEmitter<any>();

    constructor(private objectService: ObjectMemoryService) {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();
    }

    check() {
        if (this.query_default) {
            this.filteredStates = this.filterStates(this.query_default);
        } else {
            this.filteredStates = this.list_actions.slice();
        }
    }

    selectValue (obj) {
        this.value_select.emit(obj);
        for (var i = 0; i < this.objects.length; i++) {
            if (this.objects[i].name == obj) {
                var object_id = this.objects[i].id;
                this.handleChangeObjectMem(object_id);
            }
        }
    }

    filterStates(val: string) {
        var tmp_list = this.list_actions.filter(item => {
            return (StringFormat.formatText(item).indexOf(val.trim().toLowerCase()) !== -1)
        });
        tmp_list.sort(function(a, b) {
            var nameA = a.toLowerCase(), nameB = b.toLowerCase()
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        return tmp_list;
        //return this.list_actions.filter(option => new RegExp(`^${val}`, 'gi').test(option));
    }

    enterSelect($event, obj) {
        this.value_select.emit(obj);
    }

    clickAddEntity() {

    }

    // XU li chon bo nho
    public handleChangeObjectMem(value) {
        this.getObjectMemoryDetail(value, (cb) => {
            if (cb) {
                this.currentIntent.intent_entities = [];
                this.currentIntent.intent_entities = this.currentIntent.intent_entities.filter((obj) => {
                    return (obj.global_memory_param == '' || obj.global_memory_param == null)
                });
                cb.object_memory_params.filter((item) => {

                    if (item.is_criteria != undefined && item.is_criteria == 1) {
                        let intentEntity: IntentEntityModel = new IntentEntityModel();
                        intentEntity.is_required = false;
                        intentEntity.param = item.param;
                        intentEntity.entity_name = item.entity_name;
                        intentEntity.default_value = '';
                        intentEntity.priority = 0;
                        intentEntity.status =1 ;
                        intentEntity.response_messages = [];
                        intentEntity.intent_entities_suggestion = [];
                        this.currentIntent.intent_entities.push(intentEntity);

                        if (intentEntity.entity_name != undefined && intentEntity.entity_name != null) {
                            this.user_say_intent_entities = this.user_say_intent_entities.filter((row) => {
                                return (row.param.trim() != intentEntity.param.trim() || row.entity_name.trim() != intentEntity.entity_name.trim())
                            });
                        }
                    }
                });
                //4. Bên dưới bảng đã có tham số (kich_thuoc với entity là: kich_thuoc), sau đó chọn bộ nhớ có tham số (kich_thuoc với entity: kich_thuoc), do 2 tham số này trùng nhau cả tên và loại dữ liệu nên chỉ giữ lại tham số của bộ nhớ
                if (this.currentIntent.intent_entities !== undefined && this.user_say_intent_entities !== undefined) {
                    for (var i =0; i < this.currentIntent.intent_entities.length; i++) {
                        this.user_say_intent_entities = this.user_say_intent_entities.filter((row) => {
                            return (row.param != this.currentIntent.intent_entities[i].param || row.entity_name != this.currentIntent.intent_entities[i].entity_name)
                        });
                        //7. Dạy các câu '@duan:ten_du_an', '@sys.any:tien_ich'
                        //8. Kéo xuống phần hành động chọn bộ nhớ 'yeucau_chungcu' (có 2 tham số 'ten_du_an' , 'tien_ich')
                        //KQ: Bên chỗ bảng tham số có cả 2 tham số tên 'ten_du_an', 2 tham số tên 'tien_ich'
                        // MM: Mỗi tham số chỉ sinh ra 1 dòng
                        this.auto_intent_entities = this.auto_intent_entities.filter((row) => {
                            return (row.param != this.currentIntent.intent_entities[i].param)
                        });
                    }
                }

                this.memory_name.emit(cb.name);
                this.auto_intent_entities_full.emit(cb.object_memory_params);
                this.output.emit(this.currentIntent.intent_entities);
                this.output_user_say_intent_entitie.emit(this.user_say_intent_entities);
                var list_param_name = [];
                //console.log('cb.object_memory_params', cb.object_memory_params);
                for (var i = 0; i < cb.object_memory_params.length; i++) {
                    list_param_name.push(cb.object_memory_params[i].param);
                }
                this.list_param_name.emit(list_param_name);

                if (this.auto_intent_entities != undefined) {
                    var auto_intent_entities_new = this.auto_intent_entities.filter((row) => {
                        return ((row.param != null && row.param.trim().length > 0) ||
                        (row.entity_name != null && row.entity_name.trim().length > 0) ||
                        row.default_value != null && row.default_value.trim().length > 0 ||
                        (row.response_messages != undefined && row.response_messages.length) > 0 ||
                        (row.intent_entities_suggestion!= undefined && row.intent_entities_suggestion.length > 0) ||
                        row.is_required)
                    });
                    this.auto_intent_entities_new.emit(auto_intent_entities_new);
                }

            }
        });
    }

    public getObjectMemoryDetail(id, callback) {
        let actionOperate: Observable<ObjectMemoryModel>;
        actionOperate = this.objectService.getObjectMemoryById(id);
        actionOperate.subscribe(
            (data) => {

                callback(data);
            },
            (err) => {
                // load event fail
                callback(false);
            });
    }
}