
import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'memory-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class MemoryAutocompleteComponent {

  public filteredList = [];
  public elementRef;
  selectedIdx: number;
  @Input() sources: any;
  @Input() query: any;
  @Output() output = new EventEmitter<any>();

  constructor(myElement: ElementRef) {
    this.elementRef = myElement;
    this.selectedIdx = -1;
  }

  filter() {
    if (this.query !== ""){
      this.filteredList = this.sources.filter(function(el){
        return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
      }.bind(this));
    }else{
      this.filteredList = this.sources;
    }
  }

  filter_click() {
    this.filteredList = this.sources;
  }

  select(item){
    this.query = item;
    this.filteredList = [];
    this.output.emit(this.query);
  }

  handleBlur() {
    if (this.selectedIdx > -1) {
      this.query = this.filteredList[this.selectedIdx];
    }
    this.filteredList = [];
    this.selectedIdx = -1;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.selectedIdx = -1;
  }


}