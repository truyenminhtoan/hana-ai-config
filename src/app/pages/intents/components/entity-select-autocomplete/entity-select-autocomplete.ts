import { Component, Input, Output, EventEmitter} from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {EntityService} from "../../../../_services/entity.service";
import {StringFormat} from "../../../../themes/validators/string.format";
import {ComponentInteractionService} from "../../../../_services/compoent-interaction-service";

@Component({
    selector: 'entity-value-select-autocomplete',
    templateUrl: 'entity-select-autocomplete.html',
    styleUrls: ['entity-select-autocomplete.scss']
})
export class EntityValueSelectAutoComponent {

    @Input() query_default: any;
    @Input() entities_name: any;
    @Output() value_select = new EventEmitter<any>();
    
    stateCtrl: FormControl;
    filteredStates: any;
    private selected = '';
   

    constructor(private entitieService: EntityService,  private _componentService: ComponentInteractionService) {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();
    }

    check(event) {
        if (this.query_default) {
            this.filteredStates = this.filterStates(this.query_default);
        } else {
            this.filteredStates = this.entities_name.slice();
        }

        this.value_select.emit(this.query_default);
    }

    selectValue (obj) {
       // this._componentService.eventPublisher$.next('auto-select-entity');
        this.value_select.emit(obj);

    }

    filterStates(val: string) {
        val = val.trim();
        //return this.entities_name.filter(option => new RegExp(`^${val}`, 'gi').test(option));
        // return this.entities_name.filter(item => {
        //     return (StringFormat.formatText(item).indexOf(val.trim().toLowerCase()) !== -1)
        // });

        return this.entities_name.filter(item => {
            return (StringFormat.formatText(item).indexOf(StringFormat.formatText(val.trim())) !== -1)
        });
    }

    enterSelect($event, obj) {
        this.value_select.emit(obj);
    }
    
}