import { Component, AfterViewInit, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { UserModel } from '../../_models/user.model';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../_services/notification.service';
declare var $: any;
declare var gapi: any;
declare const FB: any;
@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  // tslint:disable-next-line:max-line-length
  constructor(private userService: UserService,  private route: ActivatedRoute,  private router: Router, private notify: NotificationService) { }
  public ngAfterViewInit() {
    this.router.navigate(['/login-hana']);
    // $.Backbone.history.start();
  }

  public ngOnInit() {
    // $.getScript('../../../assets/js/core/jquery.validate.min.js');
    // $('#loginFormValidation').validate();
    // FACEBOOK INIT
    FB.init({
      appId: '378516159201961',
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
  }

  public getLogin(uname: string, pwd: string) {
    let actionOperate: Observable<UserModel>;
    let user = { username: uname, password: pwd };
    actionOperate = this.userService.getLogin(user);
    actionOperate.subscribe(
        (data) => {
          // data.is_lock = false;
          //localStorage.setItem('currentUser', JSON.stringify(data));
          window.location.href = "/#/pages/agents/agentList";
        },
        (err) => {
          // load event fail
        });
  }

  /**
   * LOGIN GOOGLE
   */
  public sigin() {
    gapi.signin2.render('my-signin2', {
      onsuccess: (param) => this.onSignIn(param),
      scope: 'profile email',
      width: 37,
      height: 37
    });
  }

  public onSignIn(googleUser) {
    let profile = googleUser.getBasicProfile();
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());
    this.findOrAdd(profile.getName(), profile.getImageUrl(), profile.getEmail());
  }

  /**
   * FACEBOOK LOGIN
   */
  public onFacebookLoginClick() {
    FB.login((response) => {
      if (response.authResponse) {
        FB.api('/me', { fields: 'id,name,email' }, (res) => {
          console.log('Good to see you, ' + JSON.stringify(res) + '.');
          this.findOrAdd(res.name, '', res.email);
        });
      }
    }, { scope: 'email,public_profile', return_scopes: true });
  }

  // toantm: login or register social
  public findOrAdd(fname: string, img: string, uname: string) {
    let actionOperate: Observable<UserModel>;
    let user = new UserModel();
    user.full_name = fname;
    user.username = uname;
    user.avatar_url = img;
    actionOperate = this.userService.loginOrRegisterSocial(user);
    actionOperate.subscribe(
      (data) => {
        window.location.href = "/#/pages/agents/agentList";
      },
      (err) => {
        console.log('');
      });
  }

  public checkExisted(uname: string, callback) {
    let actionOperate: Observable<UserModel>;
    actionOperate = this.userService.getUserByEmail(uname);
    actionOperate.subscribe(
      (data) => {
        // true: existed
        if (data) {
          return callback(data);
        }
        callback(0);
      },
      (err) => {
        callback(2);
      });
  }
}
