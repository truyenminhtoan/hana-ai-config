// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { LoginComponent } from './login.component';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { routing } from './login.routing';
import { UserService } from '../../_services/user.service';
import { NotificationService } from '../../_services/notification.service';

@NgModule({
    imports: [
        CommonModule,
        AngularFormsModule,
        routing
    ],
    declarations: [
        LoginComponent,
    ],
    providers: [UserService, NotificationService],
    exports: [
        LoginComponent,
    ]
})
export class LoginModule {

}
