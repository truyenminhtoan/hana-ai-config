import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {ActionsService} from "../../../_services/actions.service";
import {NotificationService} from "../../../_services/notification.service";
import {ActionModel, ActionBasic, Meta, Inputs, Outputs} from "../../../_models/actions.model";
import { StringFormat } from '../../../themes/validators/string.format';
import {ProductsService} from "../../../_services/products.service";
import {ProductCriteria} from "../../../_models/products.model";
import {EventsService} from "../../../_services/events.service";
import {EventParamsModel, EventsModel} from "../../../_models/events.model";
import {UserModel} from "../../../_models/user.model";
import {EntitieModel} from "../../../_models/entities.model";
import {EntityService} from "../../../_services/entity.service";
import { ActivatedRoute } from '@angular/router';
import {ValidationService} from "../../../_services/ValidationService";
import {ObjectMemoryModel, ObjectMemoryParamsModel} from "../../../_models/object_memory.model";
import {ObjectMemoryService} from "../../../_services/object_memory.service";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
@Component({
    selector: 'action-edit',
    templateUrl: 'action-edit.component.html',
    styleUrls: ['action-edit.component.scss']
})

export class ActionEditComponent implements OnInit, OnChanges, AfterViewInit {

    private currentAction: ActionModel = new ActionModel();
    private err_name = '';
    private currentAction_name_custom = '';
    private agentId = '';
    private currentUser: UserModel;
    public states = [];
    public actionBasics: ActionBasic[] = [];
    private type_res = true;
    private checkboxData = { true: 'true', false: 'false' };
    private searchTypes = [];
    private selected = 'proximate';
    private input_kqhds: Inputs[] = [];
    private entities: EntitieModel[] = [];
    private product_id = '';
    private action_id_edit = '';
    private sub: any;
    private is_show = false;
    private lists: ActionModel[] = [];
    private currentAction_name_custom_res = '';
    private product_criterias: ProductCriteria[] = [];
    private product_criterias_name = [];
    private id_loading = true;
    private id_loading_action_template = true;
    private is_update = true;
    private currentAgentId = '';
    private is_focus_noidungtrave = false;
    private is_focus_tieu_de_nut_bam = false;
    private noi_dung_tra_ve_trc_khi_loc = '';
    // View
    @ViewChild('name') input_name: ElementRef;
    @ViewChild('noidung_tra_ve_trc_thong_tin_loc') input_noidung_tra_ve_trc_thong_tin_loc: ElementRef;

    private name_old = '';

    constructor(
        private route: ActivatedRoute,
        private eventService: EventsService,
        private notificationService: NotificationService,
        private actionsService: ActionsService,
        private entityService: EntityService,
        private productService: ProductsService,
        private validationService: ValidationService,
        private objMemoryService: ObjectMemoryService,
        private _componentService: ComponentInteractionService) {

        //Sort search type
        this.searchTypes.sort(function(a, b) {
            var nameA = a.value.toLowerCase(), nameB = b.value.toLowerCase()
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
        });

        this.currentAgentId = localStorage.getItem('currentAgent_Id');
        if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
            this.currentAction.actionBaseName = 'search';
            this.iniKetQuaHanhDong();
            this.getActionEdit();
            this.loadListEntity();
            this.loadListActions();
            this.loadListSearchTypes();
            this.searchTypes.sort(function(a, b){
                if(a.value < b.value) return -1;
                if(a.value > b.value) return 1;
                return 0;
            });
        } else {
            this.notificationService.showNoAgentId();
        }
    }

    public loadListSearchTypes() {
        this.lists = [];
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListSearchType();
        actionOperate.subscribe(
            (data) => {
                console.log('data', data);
                if (data.searchTypes != undefined && data.searchTypes.length > 0) {
                    for (var i = 0; i < data.searchTypes.length; i++) {
                        this.searchTypes.push({'key': data.searchTypes[i].symbol, 'value': data.searchTypes[i].name});
                    }
                    //Sort search type
                    this.searchTypes.sort(function(a, b) {
                        var nameA = a.value.toLowerCase(), nameB = b.value.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                    });
                }
            },
            (err) => {
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh search type");
            });
    }

    public loadListActions() {
        this.lists = [];
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListActionByAgentId(this.currentAgentId);
        actionOperate.subscribe(
            (group_actions) => {
                if (group_actions !== null) {
                    for (var i = 0; i< group_actions.length; i++) {
                        if (group_actions[i].data != undefined) {
                            for (var j = 0; j< group_actions[i].data.length; j++) {
                                this.lists.push(group_actions[i].data[j]);
                            }
                        }
                    }
                }
            },
            (err) => {
                this.notificationService.showWarning("Xảy ra lỗi khi lấy danh sách hành động");
            });
    }

    public getActionEdit() {
        this.is_show = true;
        this.sub = this.route.params.subscribe((params) => {
            this.action_id_edit = params['id'];
        });
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getActionById(this.action_id_edit);
        actionOperate.subscribe(
            (action) => {
                //console.log('action', JSON.stringify(action));
                this.currentAction = action;
                this.name_old = this.currentAction.nameDisplay;
                this.currentAction_name_custom_res = StringFormat.formatText(this.currentAction.name);
                this.product_id = this.currentAction.meta[0].key;
                this.productService.getPromiseProductById(this.currentAction.meta[0].value)
                    .then((product) => {
                        if (product.product_criterias !== undefined) {
                            this.product_criterias = product.product_criterias;
                            if (product.product_criterias.length > 0) {
                                for (var j = 0; j < product.product_criterias.length; j++) {
                                    this.product_criterias_name.push(product.product_criterias[j].name);
                                }
                            }
                        }
                    });
                this.currentAction_name_custom_res = StringFormat.formatText(this.currentAction.name);
                // Sort input
                if (this.currentAction.inputs && this.currentAction.inputs != undefined) {

                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (this.currentAction.inputs[i].meta) {
                            var tmp_meta: Meta[] = [];
                            if (this.currentAction.inputs[i].meta.length >= 3) {
                                for (var j = 0; j < this.currentAction.inputs[i].meta.length; j++) {
                                    if (this.currentAction.inputs[i].meta[j].key == 'searchType') {
                                        tmp_meta.push(this.currentAction.inputs[i].meta[j]);
                                        break;
                                    }
                                }
                                for (var j = 0; j < this.currentAction.inputs[i].meta.length; j++) {
                                    if (this.currentAction.inputs[i].meta[j].key == 'required') {
                                        tmp_meta.push(this.currentAction.inputs[i].meta[j]);
                                        break;
                                    }
                                }
                                for (var j = 0; j < this.currentAction.inputs[i].meta.length; j++) {
                                    if (this.currentAction.inputs[i].meta[j].key == 'weight') {
                                        tmp_meta.push(this.currentAction.inputs[i].meta[j]);
                                        break;
                                    }
                                }
                                for (var j = 0; j < this.currentAction.inputs[i].meta.length; j++) {
                                    if (this.currentAction.inputs[i].meta[j].key == 'entity_info') {
                                        tmp_meta.push(this.currentAction.inputs[i].meta[j]);
                                        break;
                                    }
                                }
                            }
                            this.currentAction.inputs[i].meta = tmp_meta;

                        }
                    }

                    // Phan hoi thanh cong
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'type_display') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'type_display');
                                if (this.currentAction.inputs[i].defaultValue == 'card') {
                                    this.type_res = true;
                                } else {
                                    this.type_res = false;
                                }
                                break;
                            }
                        }
                    }
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'title') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'title');
                                break;
                            }
                        }
                    }
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'message_if_has_solution') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'message_if_has_solution');
                                break;
                            }
                        }
                    }
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'description') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'description');
                                break;
                            }
                        }
                    }
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'url_image') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'url_image');
                                break;
                            }
                        }
                    }
                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                        if (!this.currentAction.inputs[i].meta) {
                            if (this.currentAction.inputs[i].param == 'button_title') {
                                this.updateValueInput(this.currentAction.inputs[i].defaultValue, 'button_title');
                                break;
                            }
                        }
                    }
                }
                if (this.currentAction.meta.length>0) {
                    var product_id = this.currentAction.meta[0].value;
                    this.product_id = product_id;
                    if (product_id != '') {
                        this.productService.getPromiseProductById(product_id)
                            .then((product) => {
                                this.currentAction_name_custom = StringFormat.formatText(product.name);
                            });
                    }
                }

                //Sort thuoc tinh
                this.currentAction.inputs.sort(function(a, b) {
                    var nameA = a.param.toLowerCase(), nameB = b.param.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });


                this.noi_dung_tra_ve_trc_khi_loc = this.input_kqhds[2].defaultValue;

                this.id_loading = false;
            },
            (err) => {
                this.notificationService.showWarning("Xảy ra lỗi khi lấy thông tin hành động");
            });
    }

    public updateValueInput(value, param) {
        for (var i = 0; i < this.input_kqhds.length; i++) {
            if (this.input_kqhds[i].param == param) {
                this.input_kqhds[i].defaultValue = value;
                break;
            }
        }
    }

    public changeType() {
        this.type_res = !this.type_res;
        //this.iniKetQuaHanhDong();
    }

    public loadListEntity() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entityService.getListEntityPromise(agentId, 1)
                .then((data) => {
                    this.entities = data;
                })
        } else {
            this.notificationService.showWarning("Không nhận dạng được Agent Id");
        }
    }

    public iniKetQuaHanhDong() {
        this.input_kqhds = [];
        var tmp_input_type_display = new Inputs();
        tmp_input_type_display.param = 'type_display';
        if (this.type_res) {
            tmp_input_type_display.defaultValue = 'card';
        } else {
            tmp_input_type_display.defaultValue = 'quick_reply';
        }

        var tmp_input_title = new Inputs();
        tmp_input_title.param = 'title';
        tmp_input_title.defaultValue = '';

        var tmp_input_message_if_has_solution = new Inputs();
        tmp_input_message_if_has_solution.param = 'message_if_has_solution';
        tmp_input_message_if_has_solution.defaultValue = '';

        var tmp_input_description = new Inputs();
        tmp_input_description.param = 'description';
        tmp_input_description.defaultValue = '';

        var tmp_input_url_image = new Inputs();
        tmp_input_url_image.param = 'url_image';
        tmp_input_url_image.defaultValue = '';

        var tmp_input_button = new Inputs();
        tmp_input_button.param = 'button_title';
        tmp_input_button.defaultValue = '';

        this.input_kqhds.push(tmp_input_type_display);
        this.input_kqhds.push(tmp_input_title);
        this.input_kqhds.push(tmp_input_message_if_has_solution);
        this.input_kqhds.push(tmp_input_description);
        this.input_kqhds.push(tmp_input_url_image);
        this.input_kqhds.push(tmp_input_button);
    }

    public checkName(name, id) {
        for (var i = 0; i < this.lists.length; i++) {
            if (this.lists[i].nameDisplay == name.trim() && this.lists[i].id != id) {
                return false;
            }
        }

        return true;
    }

    public updateNumber(event, index) {
        if (event.target.value == 0) {
            this.currentAction.inputs[index].meta[2].value = 1;
        }
    }

    public loadListActionsCheckName(calback) {
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListActionByAgentId(localStorage.getItem('currentAgent_Id'));
        actionOperate.subscribe(
            (group_actions) => {
                this.lists = [];
                if (group_actions !== null) {
                    for (var i = 0; i< group_actions.length; i++) {
                        console.log('group_actions[i].data', JSON.stringify(group_actions[i].data));
                        if (group_actions[i].data != undefined && group_actions[i].data.length > 0) {
                            for (var j = 0; j< group_actions[i].data.length; j++) {
                                this.lists.push(group_actions[i].data[j]);
                            }
                        }
                    }
                    // setTimeout(() =>calback(true), 1000);
                }
                calback(true);
            },
            (err) => {
                calback(false);
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh sách hành động để kiểm tra trùng tên");
            });
    }

    public saveAction() {
        if (this.currentAction.nameDisplay != undefined && this.currentAction.nameDisplay != null && this.currentAction.nameDisplay.trim().length > 0) {
            this.loadListActionsCheckName((cb) => {
                if (cb) {
                    if (this.checkName(this.currentAction.nameDisplay, this.currentAction.id)) {
                        if (this.validationService.validateAlphabet(this.currentAction.nameDisplay.trim())) {
                            if (this.product_id != null && this.product_id.trim().length >0  ) {

                                // Kiem tra currentAction.inputs
                                if (this.is_show == false) {
                                    this.notificationService.showWarning("Sản phẩm bạn chọn không có thuộc tính dùng tìm kiếm");
                                    this.is_focus_tieu_de_nut_bam = true;
                                    return;
                                }

                                //check noi dung tra ve truoc thong tin loc
                                if (this.currentAction.actionBaseName != 'search') {
                                    //console.log('this.input_kqhds', JSON.stringify(this.input_kqhds));
                                    if (this.input_kqhds.length > 0) {
                                        this.input_kqhds[2].defaultValue = this.noi_dung_tra_ve_trc_khi_loc;
                                        if (this.input_kqhds[2].defaultValue ==  null || (this.input_kqhds[2].defaultValue != null && this.input_kqhds[2].defaultValue.trim().length ==0)) {
                                            this.notificationService.showWarning("Nội dung trả về trước thông tin lọc không được rỗng");
                                            this.input_noidung_tra_ve_trc_thong_tin_loc.nativeElement.focus();
                                            return;
                                        }

                                        if (this.noi_dung_tra_ve_trc_khi_loc != undefined && this.noi_dung_tra_ve_trc_khi_loc != null && this.noi_dung_tra_ve_trc_khi_loc.trim().length > 640) {
                                            this.notificationService.showWarning("Nội dung trả về trước thông tin lọc chỉ cho phép tối đa 640 ký tự");
                                            this.input_noidung_tra_ve_trc_thong_tin_loc.nativeElement.focus();
                                            return;
                                        }

                                        if (!this.type_res) {
                                            //check tieu de nut bam
                                            if (this.input_kqhds[5].defaultValue ==  null || (this.input_kqhds[5].defaultValue != null && this.input_kqhds[5].defaultValue.trim().length ==0)) {
                                                this.notificationService.showWarning("Nội dung tiêu đề nút bấm không được rỗng");
                                                this.is_focus_tieu_de_nut_bam = true;
                                                this._componentService.eventPublisher$.next('is_focus_tieu_de_nut_bam');
                                                return;
                                            } else {
                                                this.is_focus_tieu_de_nut_bam = false;
                                            }
                                        } else {
                                            //check tieu de
                                            if (this.input_kqhds[1].defaultValue ==  null || (this.input_kqhds[1].defaultValue != null && this.input_kqhds[1].defaultValue.trim().length ==0)) {
                                                this.notificationService.showWarning("Nội dung tiêu đề không được rỗng");
                                                this.is_focus_noidungtrave = true;
                                                this._componentService.eventPublisher$.next('is_focus_noidungtrave');
                                                return;
                                            } else {
                                                this.is_focus_noidungtrave = false;
                                            }
                                        }

                                        if (this.input_kqhds[4].defaultValue != undefined && this.input_kqhds[3].defaultValue != undefined && this.input_kqhds[5].defaultValue != undefined) {
                                            if (this.input_kqhds[4].defaultValue.trim().length == 0 && this.input_kqhds[3].defaultValue.trim().length == 0 && this.input_kqhds[5].defaultValue.trim().length == 0) {
                                                this.notificationService.showWarning("Url hình ảnh, mô tả ngắn, tiêu đề nút bấm không được đồng thời để trống");
                                                return;
                                            }
                                        }

                                    }
                                }


                                // tao event
                                var event = new EventsModel();
                                event.name = this.currentAction.nameDisplay;
                                event.name_old = this.name_old;
                                event.agent_id = localStorage.getItem('currentAgent_Id');
                                event.type = this.currentAction.actionBaseName;
                                //event.event_params = [];
                                let event_params: EventParamsModel[] = [];
                                if (this.currentAction.inputs.length > 0) {
                                    for (var i = 0; i < this.currentAction.inputs.length; i++) {
                                        if (this.currentAction.inputs[i].name != undefined) {
                                            var tmp_event_param = new EventParamsModel();
                                            tmp_event_param.name = this.currentAction.inputs[i].name.trim();
                                            tmp_event_param.param = this.currentAction.inputs[i].param.trim();;
                                            event_params.push(tmp_event_param);
                                        }
                                    }
                                }
                                event.event_params = event_params;
                                console.log('event dua len:', JSON.stringify(event));
                                this.addEvent(event, (cb_event, outputs) => {
                                    if (cb_event) {
                                        console.log('outputs', outputs);
                                        this.pushAction(outputs);

                                    } else {
                                        this.notificationService.showDanger('Thêm sự kiện thất bại');
                                    }
                                });

                            } else {
                                this.notificationService.showWarning("Bạn chưa chọn sản phẩm");
                            }
                        } else {
                            this.input_name.nativeElement.focus();
                            this.notificationService.showWarning('Tên hành động chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
                        }
                    } else {
                        this.input_name.nativeElement.focus();
                        this.notificationService.showWarning("Tên hành động đã tồn tại");
                    }
                }
            });
        } else {
            this.input_name.nativeElement.focus();
            this.notificationService.showWarning("Tên hành động không được rỗng");
        }
    }

    public pushAction(outputs) {
        this.currentAction.name = this.currentAction.nameDisplay;
        //this.currentAction.agentId = localStorage.getItem('currentAgent_Id');
        this.currentAction.inputs = this.currentAction.inputs.filter((input) => {
            return input.meta != undefined;
        });

        let meta = new Meta();
        meta.key = "productId";
        if (this.product_id != '') {
            meta.value = this.product_id;
        } else {
            meta.value = this.currentAction.meta[0].value;
        }

        let tmp_metas: Meta[] = [];
        tmp_metas.push(meta);
        this.currentAction.meta = tmp_metas;

        if (this.currentAction.actionBaseName != 'search') {
            if (this.input_kqhds.length > 0) {
                for (var i =0 ; i< this.input_kqhds.length; i++) {
                    if (this.input_kqhds[i].defaultValue != '') {
                        this.currentAction.inputs.push(this.input_kqhds[i]);
                    }
                }
            }
        }

        for (var i = 0; i < this.currentAction.inputs.length; i++) {
            if (this.currentAction.inputs[i].param == 'type_display') {
                if (!this.type_res) {
                    this.currentAction.inputs[i].defaultValue = 'quick_reply';
                } else {
                    this.currentAction.inputs[i].defaultValue = 'card';
                }
            }
        }

        // neu loai la card
        if (!this.type_res) {
            this.currentAction.inputs = this.currentAction.inputs.filter((input) => {
                return (input.param != 'title' && input.param != 'description' && input.param != 'url_image');
            });

            //console.log('this.currentAction.inputs ', JSON.stringify(this.currentAction.inputs ));
        }


        this.currentAction.outputs = outputs;
        //trim
        for (var i = 0 ; i< this.currentAction.inputs.length; i++) {
            if (this.currentAction.inputs[i].defaultValue != undefined) {
                this.currentAction.inputs[i].defaultValue = this.currentAction.inputs[i].defaultValue.trim();
            }
        }
        console.log('##### DU LIEU DUA LEN:', JSON.stringify(this.currentAction));
        this.id_loading = true;
        this.updateAction((cb) => {
            this.id_loading = false;
            this.id_loading_action_template = false;
            if (cb) {
                let that = this;
                // setTimeout(function () {
                //     that.is_update = false;
                // }, 1000);
                this.notificationService.showSuccess('Cập nhật hành động thành công');
            } else {
                this.notificationService.showDanger('Cập nhật hành động thất bại');
            }
        });
    }

    public addEvent(event, callback) {
        let actionOperate: Observable<ActionModel>;
        actionOperate = this.eventService.add_or_update_array(event);
        actionOperate.subscribe(
            (action_add) => {
                let outputs:Outputs[] = [];
                if (this.currentAction.actionBaseName != 'search') {
                    /*
                    //OLD
                     var tmp_output = new Outputs();
                     tmp_output.eventId = action_add[0].id;
                     tmp_output.eventBaseName = 'no_solution';
                     tmp_output.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";
                     outputs.push(tmp_output);
                     */
                    var tmp_output = new Outputs();
                    tmp_output.eventId = action_add[1].id;
                    tmp_output.eventBaseName = 'no_solution';
                    tmp_output.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";
                    outputs.push(tmp_output);

                    // Thêm theo ý Hiếu gà
                    var tmp_output_has_solution = new Outputs();
                    tmp_output_has_solution.eventId = action_add[0].id;
                    tmp_output_has_solution.eventBaseName = 'has_solution';
                    tmp_output_has_solution.eventName = this.currentAction.nameDisplay.trim() + " có kết quả";
                    outputs.push(tmp_output_has_solution);
               
                } else {
                    var tmp_output_has_solution = new Outputs();
                    tmp_output_has_solution.eventId = action_add[0].id;
                    tmp_output_has_solution.eventBaseName = 'has_solution';
                    tmp_output_has_solution.eventName = this.currentAction.nameDisplay.trim() + " có kết quả";

                    var tmp_output_no_solution = new Outputs();
                    tmp_output_no_solution.eventId = action_add[1].id;
                    tmp_output_no_solution.eventBaseName = 'no_solution';
                    tmp_output_no_solution.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";

                    var tmp_output_exhause_solution = new Outputs();
                    tmp_output_exhause_solution.eventId = action_add[2].id;
                    tmp_output_exhause_solution.eventBaseName = 'exhause_solution';
                    tmp_output_exhause_solution.eventName = this.currentAction.nameDisplay.trim() + " hết kết quả";

                    outputs.push(tmp_output_has_solution);
                    outputs.push(tmp_output_no_solution);
                    outputs.push(tmp_output_exhause_solution);
                }

                //this.notificationService.showSuccess('Thêm event thành công');
                callback(true, outputs);
            },
            (err) => {
                // load event fail
                callback(false, []);
            });
    }

    public updateAction(callback) {
        console.log('##### DU LIEU DUA LEN:', JSON.stringify(this.currentAction));
        let actionOperate: Observable<ActionModel>;
        actionOperate = this.actionsService.update(this.currentAction);
        actionOperate.subscribe(
            (data) => {
                callback(true);
            },
            (err) => {
                // load event fail
                callback(false);
            });
    }

    public filterStates(val: string) {
        return val ? this.states.filter((s) => new RegExp(val, 'gi').test(s)) : this.states;
    }

    public checkEmptyUpdate(value) {
        if (value != null && value != '') {
            this.currentAction_name_custom_res = StringFormat.formatText(value);
        }
    }

    public getListActionTemplate() {
        let actionOperate: Observable<ActionBasic[]>;
        actionOperate = this.actionsService.getListActionTemplate();
        actionOperate.subscribe(
            (data) => {
                this.actionBasics = data;
                this.id_loading_action_template = false;
            },
            (err) => {
                this.notificationService.showDanger("Lỗi khi lấy danh sách action template");
            });
    }

    public ngOnInit() {
    }

    public ngAfterViewInit() {
        this.getListActionTemplate();
    }

    public ngOnChanges(changes: any) {
    }

    public getStyle(value) {
        if (value != undefined && value.trim().length == 0) {
            return "#f44336";
        } else {
            return "";
        }
    }
}
