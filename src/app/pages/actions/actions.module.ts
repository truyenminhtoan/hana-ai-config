// Angular Imports
import { NgModule } from '@angular/core';
import { FormsModule as AngularFormsModule } from '@angular/forms';

// This Module's Components
import { ActionsComponent } from './actions.component';
import { ActionNewComponent } from './action-new/action-new.component';
import { ActionListComponent } from './action-list/action-list.component';
import { routing } from './actions.routing';
import { CommonModule } from '@angular/common';
import { NgaModule } from '../../themes/nga.module';
import { NotificationService } from "../../_services/notification.service";
import { ProductsService } from "../../_services/products.service";
import { EventsService } from "../../_services/events.service";
import { DataTableModule } from "angular2-datatable/index";
import { FilterAcitonsDataPipe } from "./filter-data-pipe/filter-data.pipe";
import { ActionEditComponent } from "./action-edit/action-edit.component";
import { ProductAutocompleteComponent } from "./components/product-autocomplete/select-autocomplete.component";
import { ResponseAutocompleteComponent } from "./components/reponse-autocomplete/select-autocomplete.component";
import { ObjectMemoryService } from "../../_services/object_memory.service";
import { ActionInputAutoComplete } from "./components/input-autocomplete/select-autocomplete.component";
import { ValidationService } from "../../_services/ValidationService";
import { ProductSelect2CustomeComponent } from "./components/product-select2-custome/product-select2-custome";
import { MdAutocompleteModule } from '@angular/material';
import { SelectModule } from 'ng2-select';
import { EntityValueAutoComponent } from "./components/entity-value-autocomplete/entity-value-autocomplete";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from "ng2-bootstrap/index";
import { Ng2SmartTableModule } from "ng2-smart-table/index";
import { EntityValueAutoComponent2 } from "./components/entity-value-autocomplete-noi-dung-tra-ve-truoc-khi-loc/entity-value-autocomplete";

@NgModule({
    imports: [
        CommonModule,
        routing,
        AngularFormsModule,
        NgaModule,
        DataTableModule,
        MdAutocompleteModule,
        SelectModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        TabsModule,
        Ng2SmartTableModule,
        ReactiveFormsModule
    ],
    declarations: [
        ActionsComponent,
        ActionListComponent,
        ActionNewComponent,
        ActionEditComponent,
        FilterAcitonsDataPipe,
        ProductAutocompleteComponent,
        ResponseAutocompleteComponent,
        ActionInputAutoComplete,
        ProductSelect2CustomeComponent,
        EntityValueAutoComponent,
        EntityValueAutoComponent2,
    ],
    exports: [
        ActionsComponent,
        ActionListComponent,
        ActionNewComponent,
        ActionEditComponent,
        FilterAcitonsDataPipe,
        ProductAutocompleteComponent,
        ResponseAutocompleteComponent,
        ActionInputAutoComplete,
        ProductSelect2CustomeComponent,
        EntityValueAutoComponent,
        EntityValueAutoComponent2,
    ],
    providers: [
        NotificationService,
        ProductsService,
        EventsService,
        ObjectMemoryService,
        ValidationService
    ],
})
export class ActionsModule {

}
