import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionsComponent } from './actions.component';
import { ActionListComponent } from './action-list/action-list.component';
import { ActionNewComponent } from './action-new/action-new.component';
import {ActionEditComponent} from "./action-edit/action-edit.component";

const routes: Routes = [
  {
    path: '',
    component: ActionsComponent,
    children: [
      { path: 'actionList', component: ActionListComponent },
      { path: 'actionNew', component: ActionNewComponent },
      { path: 'actionEdit/:id', component: ActionEditComponent },
    ]
  }
];

// tslint:disable-next-line:eofline
export const routing = RouterModule.forChild(routes);
