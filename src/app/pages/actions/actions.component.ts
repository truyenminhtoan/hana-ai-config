import { Component } from '@angular/core';

@Component({
    selector: 'actions',
    template: '<router-outlet></router-outlet>'
})
export class ActionsComponent {

}
