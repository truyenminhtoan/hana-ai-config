import {Component, OnInit} from '@angular/core';
import {ActionsService} from "../../../_services/actions.service";
import {NotificationService} from "../../../_services/notification.service";
import {Observable} from "rxjs/Rx";
import {ActionModel} from "../../../_models/actions.model";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
declare var $: any;
declare var swal: any;

@Component({
    selector: 'action-list',
    templateUrl: 'action-list.component.html',
    styleUrls: ['action-list.component.scss']
})
export class ActionListComponent implements OnInit {
    private lists: ActionModel[] = [];
    public filterQuery = "";
    public rowsOnPage = 5;
    public sortBy = "name";
    public sortOrder = "asc";
    private id_loading = true;
    private eventSubscription: Subscription;

    constructor(
        private actionsService: ActionsService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService) {
    }
    ngOnInit() {
        this.loadListActions();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-actions') {
                    this.loadListActions();
                }
            }
        );
    }

    public loadListActions() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            let actionOperate: Observable<any>;
            actionOperate = this.actionsService.getListActionByAgentId(agentId);
            actionOperate.subscribe(
                (group_actions) => {
                    this.lists = [];
                    if (group_actions !== null) {
                        for (var i = 0; i< group_actions.length; i++) {
                            if (group_actions[i].data != undefined) {
                                for (var j = 0; j< group_actions[i].data.length; j++) {
                                    this.lists.push(group_actions[i].data[j]);
                                }
                            }
                        }
                    } else {
                        this.lists = [];
                    }
                    this.id_loading = false;
                },
                (err) => {
                });
        } else {
            this.notificationService.showNoAgentId();
        }
        this.notificationService.clearConsole();
    }

    public deleteAciton(actionId) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            let actionOperate: Observable<ActionModel>;
            actionOperate = this.actionsService.delete(actionId);
            actionOperate.subscribe(
                (data) => {
                    this._componentService.eventPublisher$.next('load-actions');
                    this.notificationService.showSuccess("Xóa hành động thành công");
                },
                (err) => {
                    // load event fail
                    this.notificationService.showWarning("Xóa hành động không thành công");
                });
        }, (dismiss) => {
            console.log(dismiss);
        });
    }
}
