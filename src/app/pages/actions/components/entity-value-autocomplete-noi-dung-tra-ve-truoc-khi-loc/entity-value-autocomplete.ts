import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {ComponentInteractionService} from "../../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
import {StringFormat} from "../../../../themes/validators/string.format";

@Component({
    selector: 'entity-value-autocomplete-2',
    templateUrl: 'entity-value-autocomplete.html',
    styleUrls: ['entity-value-autocomplete.scss']
})
export class EntityValueAutoComponent2 {

    @Input() query_default: any;
    @Input() lable: any;
    @Input() product_criterias_name: any;
    @Input() require: any;
    @Input() is_focus: any;
    @Input() is_focus_tieu_de_button: any;
    @Output() value_select = new EventEmitter<any>();
    @ViewChild('input_auto') input_auto: ElementRef;
    stateCtrl: FormControl;
    filteredStates: any;
    public tmp_string = '';
    @Input() product_id: any;
    private eventSubscription: Subscription;

    constructor(private _componentService: ComponentInteractionService) {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();

        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'is_focus_noidungtrave') {
                    if (this.is_focus) {
                        this.input_auto.nativeElement.focus();
                    }
                }
            }
        );

        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'is_focus_tieu_de_nut_bam') {
                    this.input_auto.nativeElement.focus();
                }
            }
        );
    }

    check(event) {
        if (this.query_default != undefined) {
            if (this.query_default != null && this.query_default.trim().length == 0) {
                this.tmp_string = '';
            }

            if (event.keyCode == 32) {//tab
                // luu gia tri
                this.tmp_string = this.query_default;
            }
            var list_query = this.query_default.split(" {");

            var aTmp = this.query_default.split(" ");
            aTmp = aTmp.filter((row) => {
                return row.trim().length > 0
            });

            if (aTmp != undefined) {
                var value_filter = aTmp[aTmp.length - 1];
                var tmp_1 = aTmp[aTmp.length - 1];
                if (value_filter != undefined) {
                    value_filter = value_filter.replace('{', '');
                }
            }

            var tmp_value_filter = '';
            if (list_query.length > 0) {
                tmp_value_filter = list_query[list_query.length - 1];
            }
            this.filteredStates = this.filterStates(tmp_value_filter.replace('{', ''), tmp_1);
            this.value_select.emit(this.query_default);
            this.input_auto.nativeElement.focus();
        }
    }
    
    selectValue (val) {
    }

    filterStates(val: string, tmp_1: string) {
        if (tmp_1 != undefined && val != undefined) {
            if (tmp_1.length == 0) {
                return [];
            }
            if (tmp_1.indexOf('{') != -1) {
                if (tmp_1.length == 1) {
                    return this.product_criterias_name;
                } else {
                    if (val.length > 0) {
                        var val_select =  this.product_criterias_name.filter(item => {
                            return (item != undefined && StringFormat.formatText(item).indexOf(StringFormat.formatText(val)) !== -1)
                        });
                        return val_select;
                    } else {
                        return this.product_criterias_name;
                    }
                }
            } else {
                var val_select =  this.product_criterias_name.filter(item => {
                    return (item != undefined && StringFormat.formatText(item).indexOf(StringFormat.formatText(val)) !== -1)
                });
                return val_select;
            }
        } else {
            return [];
        }
    }

    enterSelect($event, val) {
        let that = this;
        (function(){
            setTimeout(() => {
                that.query_default = that.tmp_string + '{' + val.trim()+'} ';
                that.tmp_string = that.query_default;
                that.input_auto.nativeElement.focus();
                that.query_default = that.query_default.trim() + " ";
                var query_default_tmp = '';
                var tmp = that.query_default.split('{');
                if (tmp.length > 0) {
                    for ( var k = 0; k < tmp.length; k++) {
                        if (tmp[k].indexOf('}') != -1 ) {
                            query_default_tmp += '{'+tmp[k].trim() + ' ';
                        }
                    }
                }
                //console.log('query_default_tmp', query_default_tmp);
                that.query_default = query_default_tmp;
                that.value_select.emit(that.query_default);
                that.filteredStates = [];
            }, 200);
        })();
    }

}