
import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';

import {ProductsService} from "../../../../_services/products.service";
import {EntityService} from "../../../../_services/entity.service";
import {Inputs, Meta} from "../../../../_models/actions.model";
import {StringFormat} from "../../../../themes/validators/string.format";
import {ProductsModel} from "../../../../_models/products.model";

@Component({
  selector: 'product-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class ProductAutocompleteComponent {

  public filteredList = [];
  public elementRef;
  selectedIdx: number;
  @Input() sources: any;
  @Input() query: any;
  @Input() entities: any;
  @Output() output = new EventEmitter<any>();
  @Output() output2 = new EventEmitter<any>();
  @Output() is_show = new EventEmitter<any>();
  @Input() products: ProductsModel[] = [];
  @Output() product_id = new EventEmitter<any>();
  @Output() name_memory = new EventEmitter<any>();
  @Output() product_criterias = new EventEmitter<any>();
  
  constructor(myElement: ElementRef, 
              private productService: ProductsService, 
              private entityService: EntityService) {
    this.elementRef = myElement;
    this.selectedIdx = -1;

  }

  filter() {
    if (this.query !== ""){
      this.filteredList = this.sources.filter(function(el){
        return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
      }.bind(this));
    }else{
      this.filteredList = this.sources;
    }


  }

  filter_click() {
    this.filteredList = this.sources;
  }

  select(item){
    this.query = item;
    this.filteredList = [];
    this.output2.emit(this.query);
    if (this.products.length > 0) {
      for (var i =0; i< this.products.length; i++) {
        if (this.products[i].name == item) {
          this.product_id.emit(this.products[i].id);
          this.product_criterias.emit(this.products[i].product_criterias);
          this.getThongTinTieuChiHanhDong(this.products[i].id);
          this.name_memory.emit(StringFormat.formatText(this.products[i].name));
        }
      }
    }

  }

  public getThongTinTieuChiHanhDong(productId) {
    this.productService.getPromiseProductById(productId)
        .then((product) => {
          let input_actions: Inputs[] = [];
          if (product.product_criterias.length > 0) {
            for (var i = 0; i < product.product_criterias.length; i++) {
              if (product.product_criterias[i].type == 'criteria') {
                var tmp_input = new Inputs();
                tmp_input.param = product.product_criterias[i].name;
                tmp_input.defaultValue = 'NULL';
                tmp_input.param = product.product_criterias[i].name;

                let tmp_meta: Meta[] = [];

                var meta_search_type = new Meta();
                meta_search_type.key = 'searchType';
                meta_search_type.value = '';
                tmp_meta.push(meta_search_type);

                var meta_required = new Meta();
                meta_required.key = 'required';
                meta_required.value = false;
                tmp_meta.push(meta_required);

                var meta_weight = new Meta();
                meta_weight.key = 'weight';
                meta_weight.value = '1';
                tmp_meta.push(meta_weight);

                var meta_entity = new Meta();
                meta_entity.key = 'entity_info';
                meta_entity.value = product.product_criterias[i].entity_id;
                tmp_meta.push(meta_entity);

                tmp_input.meta = tmp_meta;
                input_actions.push(tmp_input);
              }
            }

            this.emitTer(input_actions);
          } else {
            this.emitTer([]);
          }

        });
  }

  public emitTer(inputs) {
    for (var i =0 ; i < inputs.length; i++) {
      for (var j = 0; j < inputs[i].meta.length; j++) {
        if (inputs[i].meta[j].key == 'entity_info') {
          inputs[i].meta[j].value = this.searchEntity(inputs[i].meta[j].value);
        }
      }
    }
    if (inputs.length > 0) {
      this.output.emit(inputs);
      this.is_show.emit(true);
    } else {
      this.is_show.emit(false);
    }

  }

  public searchEntity(id) {
    for (var i = 0; i < this.entities.length; i++) {
      if (this.entities[i].id == id) {
        return this.entities[i].name;
      }
    }
    return '--';
  }

  handleBlur() {
    if (this.selectedIdx > -1) {
      this.query = this.filteredList[this.selectedIdx];
    }
    this.filteredList = [];
    this.selectedIdx = -1;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.selectedIdx = -1;
  }


}