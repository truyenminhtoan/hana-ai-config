
import { Component, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'reponse-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class ResponseAutocompleteComponent {

  public filteredList = [];
  public elementRef;
  selectedIdx: number;
  @Input() sources: any;
  @Input() query: any;
  @Output() output = new EventEmitter<any>();
  @Output() value = new EventEmitter<any>();
  @Input() query_string: any;
  
  constructor(myElement: ElementRef) {
    this.elementRef = myElement;
    this.selectedIdx = -1;
  }

  filter(event) {
    if (event.target.value == "{"){
      this.filteredList = this.sources;
    } else {
      this.filteredList = [];
    }
    this.value.emit(this.query_string);
  }

  filter_click() {
    this.filteredList = this.sources;
  }

  select(item){
    this.query_string = '';
    this.query = item;
    this.filteredList = [];
    if (item == 'Hình ảnh') {
      this.output.emit(true);
    } else {
      this.output.emit(false);
    }
    
  }

  handleBlur() {
    if (this.selectedIdx > -1) {
      this.query = this.filteredList[this.selectedIdx];
    }
    this.filteredList = [];
    this.selectedIdx = -1;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.selectedIdx = -1;
  }


}