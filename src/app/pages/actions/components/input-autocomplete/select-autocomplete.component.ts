import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'action-input-autocomplete',
  templateUrl: 'select-autocomplete.component.html',
  styleUrls: ['select-autocomplete.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class ActionInputAutoComplete {

  public filteredList = [];
  public elementRef;
  selectedIdx: number;
  @Input() sources: any;
  @Input() query: any;
  @Input() title: any;
  @Output() output = new EventEmitter<any>();

  constructor(myElement: ElementRef) {
    this.elementRef = myElement;
    this.selectedIdx = -1;
  }

  filter(event) {
    if (this.query != undefined) {
      for (var i = 0; i < this.sources.length; i++) {
        if (this.sources[i].name.indexOf('@') == -1) {
          this.sources[i].name = '@' + this.sources[i].name;
        }
      }
      this.filteredList = this.sources.filter(function(el){
        if (el.name != undefined) {
          return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
        }
      }.bind(this));
      var tmp = event.target.value.split(" ");
      if (tmp[tmp.length - 1] == '@') {
        this.filteredList = this.sources;
      } else {
        if (this.query.indexOf('@') > -1) {
          this.filteredList = this.sources.filter(function(el){
            if (el.name != undefined) {
              return el.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
            }
          }.bind(this));
        }
      }

      if (event.keyCode == 32) {//tab
        var string_tmp = '';
        var aTmp = this.query.split(" ");
        for (var i = 0; i < aTmp.length; i++) {
          if (aTmp[i] != '' && aTmp[i]!= ' ') {
            if (aTmp[i].indexOf('{') > -1 && aTmp[i].indexOf('}') > -1) {
              string_tmp += aTmp[i] + ' ';
            } else {
              string_tmp += '{' + aTmp[i] + '} ';
            }
          }
        }
        this.query = string_tmp + ' ';
      }

      this.output.emit(this.query);
    }
  }


  filter_click() {
    this.filteredList = this.sources;
  }

  select(item){
    this.query = this.query + ' ' + item.replace('@', '') + ':';
    this.query = this.query.replace('@ ', '@');
    this.output.emit(this.query);
    this.filteredList = [];
  }

  handleBlur() {
    if (this.selectedIdx > -1) {
      this.query = this.filteredList[this.selectedIdx];
    }
    this.filteredList = [];
    this.selectedIdx = -1;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.selectedIdx = -1;
  }


}