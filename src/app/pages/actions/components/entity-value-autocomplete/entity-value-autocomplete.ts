import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {ComponentInteractionService} from "../../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
import {StringFormat} from "../../../../themes/validators/string.format";

@Component({
    selector: 'entity-value-autocomplete',
    templateUrl: 'entity-value-autocomplete.html',
    styleUrls: ['entity-value-autocomplete.scss']
})
export class EntityValueAutoComponent {

    @Input() query_default: any;
    @Input() lable: any;
    @Input() product_criterias_name: any;
    @Input() require: any;
    @Input() is_focus: any;
    @Input() is_focus_tieu_de_button: any;
    @Output() value_select = new EventEmitter<any>();
    @ViewChild('input_auto') input_auto: ElementRef;
    stateCtrl: FormControl;
    filteredStates: any;
    public tmp_string = '';
    @Input() product_id: any;
    @Input() maxlength: any;
    private eventSubscription: Subscription;
    private index_auto_key = -1;
    private index_end_select = '';
    private index_start = '';

    @Input() css_color: any;
    
    constructor(private _componentService: ComponentInteractionService) {
        if (this.maxlength == undefined) {
            this.maxlength = 10000;
        }
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();

        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'is_focus_noidungtrave') {
                    if (this.is_focus) {
                        this.input_auto.nativeElement.focus();
                    }
                }
            }
        );

        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'is_focus_tieu_de_nut_bam') {
                    this.input_auto.nativeElement.focus();
                }
            }
        );
    }

    check(event) {
        console.log('vao day');
        if (event.key != 'Enter') {
            if (event.key == '{' || event.key == 'Shift') {
                this.index_auto_key = event.target.selectionStart;
            }
            var value_old = event.target.value.slice(event.target.selectionStart, event.target.value.length);
            var value_filter =  event.target.value.slice(this.index_auto_key - 1, event.target.selectionEnd);
            // console.log('event.key', event.key);
            // console.log('value_old', value_old);
            // console.log('value_filter', value_filter);
            if (this.query_default != undefined) {
                if (this.query_default != null && this.query_default.trim().length == 0) {
                    this.tmp_string = '';
                }
                var index_auto = event.target.selectionEnd;
                this.index_end_select = event.target.value.slice(index_auto, event.target.value.length)
                this.index_start =  event.target.value.slice(0, this.index_auto_key - 1);
                this.filteredStates = this.filterStates(value_filter);
                this.query_default =  event.target.value;
            }
            this.value_select.emit(this.query_default);
        }
    }

    emitValue(event) {
        this.value_select.emit(this.query_default);
    }
    
    selectValue (val) {
    }

    filterStates(val: string) {
        if (val.indexOf('{') != -1 && val.trim().length > 0) {
            var val_select =  this.product_criterias_name.filter(item => {
                return (item != undefined && StringFormat.formatText(item).indexOf(StringFormat.formatText(val.replace('{', ''))) !== -1)
            });
            return val_select;
        } else {
            return [];
        }
    }

    enterSelect($event, val, my_input) {
        let that = this;
        (function(){
            setTimeout(() => {
                // console.log('index_end_select', that.index_end_select);
                // console.log('index_start', that.index_start);
                if (that.index_end_select != undefined && that.index_end_select.trim().length > 0) {
                    that.query_default = that.index_start + '{'+ val +'}' + ' ' + that.index_end_select;
                } else {
                    that.query_default = that.index_start + '{'+ val +'}';
                }
                that.value_select.emit(that.query_default.trim());
                that.filteredStates = [];
                setTimeout(() => {
                    if (that.index_end_select.trim().length == 0) {
                        var index_contro = (that.index_start + val).length + 2;
                    } else {
                        var index_contro = (that.index_start + val).length + 1;
                    }
                    that.setSelectionRange(my_input, index_contro, index_contro);
                    that.index_start = that.index_end_select = '';
                }, 200);
                that.value_select.emit(that.query_default);
            }, 200);
        })();
    }

    setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

}