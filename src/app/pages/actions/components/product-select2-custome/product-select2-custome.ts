import {Component, ViewEncapsulation, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { EntityService } from '../../../../_services/entity.service';
import {Subscription} from 'rxjs/Rx';
import * as _ from 'underscore';
import {ProductsService} from "../../../../_services/products.service";
import {UserModel} from "../../../../_models/user.model";
import {ProductsModel} from "../../../../_models/products.model";
import {Inputs, Meta} from "../../../../_models/actions.model";
import {StringFormat} from "../../../../themes/validators/string.format";

@Component({
    selector: 'product-select2-custome',
    templateUrl: 'product-select2-custome.html',
    styleUrls: ['product-select2-custome.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProductSelect2CustomeComponent implements OnInit {

    @Input() customeValues: ProductsModel[] = [];
    @Input() customeActive: any;
    @Input() cusDisabled: any;
    @Output() customeActiveChange = new EventEmitter();
    public elementRef;
    selectedIdx: number;
    @Input() sources: any;
    @Input() query: any;
    @Input() entities: any;
    @Output() output = new EventEmitter<any>();
    @Output() output2 = new EventEmitter<any>();
    @Output() is_show = new EventEmitter<any>();
    @Input() products: ProductsModel[] = [];
    @Output() product_id = new EventEmitter<any>();
    @Output() name_memory = new EventEmitter<any>();
    @Output() product_criterias = new EventEmitter<any>();
    @Output() product_criterias_name = new EventEmitter<any>();
    private product_name = [];

    public active: Array<Object> = [];
    private currentUser: UserModel;

    constructor(private entityService: EntityService,
                private productService: ProductsService) {
    }
    
    public ngOnInit() {
        if (this.customeActive != undefined && this.customeActive.trim().length > 0) {
           this.loadProductActive(this.customeActive);
        }

        if (this.customeValues == undefined || (this.customeValues.length == 0)) {
            this.getListProduct();
        } else {
            this.products = this.customeValues;
        }
        
    }

    public loadProductActive(productId: string) {
        this.productService.getPromiseProductById(productId)
            .then((product) => {
                this.active = [{ id: this.customeActive, text: product.name }];
            });
    }

    private setValueText(enties) {
        _.each(enties, function (value) {
            value.text = value.name;
        });
        return enties;
    }

    public loadEntity(entityId: string) {
        this.entityService.getEntityById(entityId).subscribe((data) => {
            this.active = [{ id: this.customeActive, text: data.name }];
        }, (error) => {
        });
    }
    public selected(item: any): void {
        this.customeActive = item.id;
        this.customeActiveChange.emit(this.customeActive);

        this.query = item.text;
        this.output2.emit(this.query);
        if (this.products.length > 0) {
            for (var i =0; i< this.products.length; i++) {
                if (this.products[i].name == item.text) {
                    this.product_id.emit(this.products[i].id);
                    this.product_criterias.emit(this.products[i].product_criterias);
                    if (this.products[i].product_criterias != undefined) {
                        this.product_name = [];
                        for (var j = 0; j < this.products[i].product_criterias.length; j++) {
                            //this.product_name.push('{' + this.products[i].product_criterias[j].name);
                            this.product_name.push(this.products[i].product_criterias[j].name);
                        }
                        this.product_criterias_name.emit(this.product_name);
                    }
                    this.getThongTinTieuChiHanhDong(this.products[i].id);
                    this.name_memory.emit(StringFormat.formatText(this.products[i].name));
                }
            }
        }


    }

    public removed(value: any): void {

    }

    public typed(value: any): void {

    }

    public refreshValue(value: any): void {
        this.customeActive = value;
    }

    public getListProduct() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.productService.getListProductByAgentId(agentId)
                .then((data) => {
                    this.customeValues = this.setValueText(data);
                    this.products = this.customeValues;
                })
        }
    }

    public getThongTinTieuChiHanhDong(productId) {
        this.productService.getPromiseProductById(productId)
            .then((product) => {
                console.log('product.product_criterias', product.product_criterias);
                let input_actions: Inputs[] = [];
                if (product.product_criterias.length > 0) {
                    product.product_criterias.sort(function(a, b) {
                        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        // names must be equal
                        return 0;
                    });
                    for (var i = 0; i < product.product_criterias.length; i++) {
                        if (product.product_criterias[i].type == 'criteria') {
                            var tmp_input = new Inputs();
                            tmp_input.name = product.product_criterias[i].name;
                            tmp_input.param = product.product_criterias[i].param;
                            tmp_input.defaultValue = 'NULL';
                           // tmp_input.param = product.product_criterias[i].name;

                            let tmp_meta: Meta[] = [];

                            var meta_search_type = new Meta();
                            meta_search_type.key = 'searchType';
                            meta_search_type.value = '';
                            tmp_meta.push(meta_search_type);

                            var meta_required = new Meta();
                            meta_required.key = 'required';
                            meta_required.value = false;
                            tmp_meta.push(meta_required);

                            var meta_weight = new Meta();
                            meta_weight.key = 'weight';
                            meta_weight.value = '1';
                            tmp_meta.push(meta_weight);

                            var meta_entity = new Meta();
                            meta_entity.key = 'entity_info';
                            meta_entity.value = product.product_criterias[i].entity_name;
                            tmp_meta.push(meta_entity);

                            tmp_input.meta = tmp_meta;
                            input_actions.push(tmp_input);
                        }
                    }

                    console.log('input_actions', input_actions);


                    this.emitTer(input_actions);
                } else {
                    this.emitTer([]);
                }

            });
    }

    public emitTer(inputs) {
        // for (var i =0 ; i < inputs.length; i++) {
        //     for (var j = 0; j < inputs[i].meta.length; j++) {
        //         if (inputs[i].meta[j].key == 'entity_info') {
        //             inputs[i].meta[j].value = this.searchEntity(inputs[i].meta[j].value);
        //         }
        //     }
        // }
        if (inputs.length > 0) {
            this.output.emit(inputs);
            this.is_show.emit(true);
        } else {
            this.is_show.emit(false);
        }

    }

    public searchEntity(id) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i].id == id) {
                return this.entities[i].name;
            }
        }
        return '--';
    }

}
