import { Component, OnInit, OnChanges, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {ActionsService} from "../../../_services/actions.service";
import {NotificationService} from "../../../_services/notification.service";
import {ActionModel, ActionBasic, Meta, Inputs, Outputs} from "../../../_models/actions.model";
import {ProductsService} from "../../../_services/products.service";
import {ProductCriteria} from "../../../_models/products.model";
import {EventsService} from "../../../_services/events.service";
import {EventParamsModel, EventsModel} from "../../../_models/events.model";
import {UserModel} from "../../../_models/user.model";
import {EntitieModel} from "../../../_models/entities.model";
import {EntityService} from "../../../_services/entity.service";
import { Router, ActivatedRoute } from '@angular/router';
import {StringFormat} from "../../../themes/validators/string.format";
import {ValidationService} from "../../../_services/ValidationService";
import * as _ from 'underscore';
import { ComponentInteractionService } from '../../../_services/compoent-interaction-service';

@Component({
    selector: 'action-new',
    templateUrl: 'action-new.component.html',
    styleUrls: ['action-new.component.scss']
})

export class ActionNewComponent implements OnInit, OnChanges, AfterViewInit {

    private currentAction: ActionModel = new ActionModel();
    private product_id = '';
    private currentUser: UserModel;
    public states = [];
    public actionBasics: ActionBasic[] = [];
    private type_res = true;
    private checkboxData = { true: 'true', false: 'false' };
    private currentAction_name_custom_res = '';
    private searchTypes = [];

    private is_show = false;
    private selected = 'proximate';
    private input_kqhds: Inputs[] = [];
    private entities: EntitieModel[] = [];
    private selected_lhd = 'search';
    private lists: ActionModel[] = [];
    private product_criterias: ProductCriteria[] = [];
    private product_criterias_name = [];
    private id_loading = true;
    private is_load_entity = true;
    private is_load_list_action = true;
    private currentAgentId = '';
    private is_focus_noidungtrave = false;
    private is_focus_tieu_de_nut_bam = false;
    private noi_dung_tra_ve_trc_khi_loc = '';

    // View
    @ViewChild('name') input_name: ElementRef;
    @ViewChild('noidung_tra_ve_trc_thong_tin_loc') input_noidung_tra_ve_trc_thong_tin_loc: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private productsService: ProductsService,
        private eventService: EventsService,
        private notificationService: NotificationService,
        private actionsService: ActionsService,
        private entityService: EntityService,
        private validationService: ValidationService,
        private _componentService: ComponentInteractionService
    ) {

        this.loadListSearchTypes();
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser && this.currentUser.partner_id) {
            this.currentAgentId = localStorage.getItem('currentAgent_Id');
            if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
                this.currentAction.actionBaseName = 'search';
                this.iniKetQuaHanhDong();
                this.loadListEntity();
                this.loadListActions();
                this.currentAction.inputs = [];
                this.searchTypes.sort(function(a, b){
                    if(a.value < b.value) return -1;
                    if(a.value > b.value) return 1;
                    return 0;
                });
                this.id_loading = false;
            } else {
                this.notificationService.showNoAgentId();
            }
        } else {
            this.notificationService.showNoPartnerId();
        }

    }

    public loadListSearchTypes() {
        this.lists = [];
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListSearchType();
        actionOperate.subscribe(
            (data) => {
                console.log('data', data);
                if (data.searchTypes != undefined && data.searchTypes.length > 0) {
                    for (var i = 0; i < data.searchTypes.length; i++) {
                        this.searchTypes.push({'key': data.searchTypes[i].symbol, 'value': data.searchTypes[i].name});
                    }
                    //Sort search type
                    this.searchTypes.sort(function(a, b) {
                        var nameA = a.value.toLowerCase(), nameB = b.value.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                    });
                }
            },
            (err) => {
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh search type");
            });
    }

    public loadListActions() {
        this.lists = [];
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListActionByAgentId(localStorage.getItem('currentAgent_Id'));
        actionOperate.subscribe(
            (group_actions) => {
                if (group_actions !== null) {
                    for (var i = 0; i< group_actions.length; i++) {
                        if (group_actions[i].data != undefined && group_actions[i].data.length > 0) {
                            for (var j = 0; j< group_actions[i].data.length; j++) {
                                this.lists.push(group_actions[i].data[j]);
                            }
                        }
                    }
                }
                this.is_load_list_action = false;
            },
            (err) => {
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh sách hành động");
            });
    }

    public changeType() {
        this.type_res = !this.type_res;
        this.iniKetQuaHanhDong();
    }

    public loadListEntity() {
        this.entityService.getListEntityPromise(this.currentAgentId, 1)
            .then((data) => {
                this.entities = data;
                this.is_load_entity = false;
            })
    }

    private setValueText(obj) {
        _.each(obj, function (value) {
            value.text = value.name;
        });
        return obj;
    }

    public iniKetQuaHanhDong() {
        this.input_kqhds = [];
        var tmp_input_type_display = new Inputs();
        tmp_input_type_display.param = 'type_display';
        if (this.type_res) {
            tmp_input_type_display.defaultValue = 'card';
        } else {
            tmp_input_type_display.defaultValue = 'quick_reply';
        }

        var tmp_input_title = new Inputs();
        tmp_input_title.param = 'title';
        tmp_input_title.defaultValue = '';

        var tmp_input_message_if_has_solution = new Inputs();
        tmp_input_message_if_has_solution.param = 'message_if_has_solution';
        tmp_input_message_if_has_solution.defaultValue = '';

        var tmp_input_description = new Inputs();
        tmp_input_description.param = 'description';
        tmp_input_description.defaultValue = '';

        var tmp_input_url_image = new Inputs();
        tmp_input_url_image.param = 'url_image';
        tmp_input_url_image.defaultValue = '';

        var tmp_input_button = new Inputs();
        tmp_input_button.param = 'button_title';
        tmp_input_button.defaultValue = '';

        this.input_kqhds.push(tmp_input_type_display);
        this.input_kqhds.push(tmp_input_title);
        this.input_kqhds.push(tmp_input_message_if_has_solution);
        this.input_kqhds.push(tmp_input_description);
        this.input_kqhds.push(tmp_input_url_image);
        this.input_kqhds.push(tmp_input_button);
    }

    public checkName(name) {
        for (var i = 0; i < this.lists.length; i++) {
            if (this.lists[i].nameDisplay == name.trim()) {
                return false;
            }
        }

        return true;
    }

    public loadListActionsCheckName(calback) {
        let actionOperate: Observable<any>;
        actionOperate = this.actionsService.getListActionByAgentId(localStorage.getItem('currentAgent_Id'));
        actionOperate.subscribe(
            (group_actions) => {
                this.lists = [];
                if (group_actions !== null) {
                    for (var i = 0; i< group_actions.length; i++) {
                        if (group_actions[i].data != undefined && group_actions[i].data.length > 0) {
                            for (var j = 0; j< group_actions[i].data.length; j++) {
                                this.lists.push(group_actions[i].data[j]);
                            }
                        }
                    }
                    // setTimeout(() =>calback(true), 1000);
                }
                calback(true);
            },
            (err) => {
                calback(false);
                this.notificationService.showDanger("Xảy ra lỗi khi lấy danh sách hành động để kiểm tra trùng tên");
            });
    }

    public saveAction() {
        if (this.currentAction.nameDisplay != undefined && this.currentAction.nameDisplay != null && this.currentAction.nameDisplay.trim().length > 0) {
            this.loadListActionsCheckName((cb) => {
                if (cb) {
                    if (!this.checkName(this.currentAction.nameDisplay)) {
                        this.input_name.nativeElement.focus();
                        this.notificationService.showWarning("Tên hành động đã tồn tại");
                        return;
                    }
                    if (this.validationService.validateAlphabet(this.currentAction.nameDisplay)) {
                        if (this.product_id != null && this.product_id.trim().length >0  ) {
                            //console.log('this.currentAction.inputs', this.currentAction.inputs);
                            // Kiem tra currentAction.inputs
                            if (this.currentAction.inputs == undefined || this.currentAction.inputs.length == 0) {
                                this.notificationService.showWarning("Sản phẩm bạn chọn không có thuộc tính dùng tìm kiếm");
                                this.is_focus_tieu_de_nut_bam = true;
                                return;
                            }
                            //check noi dung tra ve truoc thong tin loc
                            if (this.currentAction.actionBaseName != 'search') {
                                if (this.input_kqhds.length > 0) {
                                    this.input_kqhds[2].defaultValue = this.noi_dung_tra_ve_trc_khi_loc;
                                    if (this.input_kqhds[2].defaultValue ==  null || (this.input_kqhds[2].defaultValue != null && this.input_kqhds[2].defaultValue.trim().length ==0)) {
                                        this.notificationService.showWarning("Nội dung trả về trước thông tin lọc không được rỗng");
                                        this.input_noidung_tra_ve_trc_thong_tin_loc.nativeElement.focus();
                                        return;
                                    }

                                    if (this.noi_dung_tra_ve_trc_khi_loc != undefined && this.noi_dung_tra_ve_trc_khi_loc != null && this.noi_dung_tra_ve_trc_khi_loc.trim().length > 640) {
                                        this.notificationService.showWarning("Nội dung trả về trước thông tin lọc chỉ cho phép tối đa 640 ký tự");
                                        this.input_noidung_tra_ve_trc_thong_tin_loc.nativeElement.focus();
                                        return;
                                    }

                                    if (!this.type_res) {
                                        //check tieu de nut bam
                                        if (this.input_kqhds[5].defaultValue ==  null || (this.input_kqhds[5].defaultValue != null && this.input_kqhds[5].defaultValue.trim().length ==0)) {
                                            this.notificationService.showWarning("Nội dung tiêu đề nút bấm không được rỗng");
                                            this.is_focus_tieu_de_nut_bam = true;
                                            this._componentService.eventPublisher$.next('is_focus_tieu_de_nut_bam');
                                            return;
                                        } else {
                                            this.is_focus_tieu_de_nut_bam = false;
                                        }
                                    } else {
                                        //check tieu de
                                        if (this.input_kqhds[1].defaultValue ==  null || (this.input_kqhds[1].defaultValue != null && this.input_kqhds[1].defaultValue.trim().length ==0)) {
                                            this.notificationService.showWarning("Nội dung tiêu đề không được rỗng");
                                            this.is_focus_noidungtrave = true;
                                            this._componentService.eventPublisher$.next('is_focus_noidungtrave');
                                            return;
                                        } else {
                                            this.is_focus_noidungtrave = false;
                                        }
                                    }

                                    if (this.input_kqhds[4].defaultValue != undefined && this.input_kqhds[3].defaultValue != undefined && this.input_kqhds[5].defaultValue != undefined) {
                                        if (this.input_kqhds[4].defaultValue.trim().length == 0 && this.input_kqhds[3].defaultValue.trim().length == 0 && this.input_kqhds[5].defaultValue.trim().length == 0) {
                                            this.notificationService.showWarning("Url hình ảnh, mô tả ngắn, tiêu đề nút bấm không được đồng thời để trống");
                                            return;
                                        }
                                    }
                                }
                            }

                            // tao event
                            var event = new EventsModel();
                            event.name = this.currentAction.nameDisplay;
                            //event.name_text = StringFormat.formatText(this.currentAction.nameDisplay);
                            event.name_old = StringFormat.formatText(this.currentAction.nameDisplay);
                            event.agent_id = localStorage.getItem('currentAgent_Id');
                            event.type = this.currentAction.actionBaseName;
                            //event.event_params = [];
                            let event_params: EventParamsModel[] = [];
                            if (this.currentAction.inputs.length > 0) {
                                for (var i = 0; i < this.currentAction.inputs.length; i++) {
                                    var tmp_event_param = new EventParamsModel();
                                    tmp_event_param.name = this.currentAction.inputs[i].name.trim();;
                                    tmp_event_param.param = this.currentAction.inputs[i].param.trim();;
                                    event_params.push(tmp_event_param);
                                }
                            }
                            event.event_params = event_params;

                            this.addEvent(event, (cb_event, outputs) => {
                                if (cb_event) {
                                    this.pushAction(outputs);

                                } else {
                                    this.notificationService.showDanger('Thêm sự kiện thất bại');
                                }
                            });

                        } else {
                            this.notificationService.showWarning("Bạn chưa chọn sản phẩm");
                        }
                    } else {
                        this.input_name.nativeElement.focus();
                        this.notificationService.showWarning('Tên hành động chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
                    }
                }
            });
        } else {
            this.input_name.nativeElement.focus();
            this.notificationService.showWarning("Tên hành động không được rỗng");
        }
    }


    public pushAction(outputs) {
        this.currentAction.name = this.currentAction.nameDisplay.trim();
        this.currentAction.agentId = this.currentAgentId;
        let meta = new Meta();
        // meta.value = "productId";
        // meta.key = this.product_id;
        meta.value = this.product_id;
        meta.key = 'productId';
        let tmp_metas: Meta[] = [];
        tmp_metas.push(meta);
        this.currentAction.meta = tmp_metas;

        if (this.currentAction.actionBaseName != 'search') {
            if (this.input_kqhds.length > 0) {
                for (var i =0 ; i< this.input_kqhds.length; i++) {
                    if (this.input_kqhds[i].defaultValue != '') {
                        this.currentAction.inputs.push(this.input_kqhds[i]);
                    }
                }
            }
        }

        for (var i = 0; i < this.currentAction.inputs.length; i++) {
            if (this.currentAction.inputs[i].param == 'type_display') {
                if (!this.type_res) {
                    this.currentAction.inputs[i].defaultValue = 'quick_reply';
                } else {
                    this.currentAction.inputs[i].defaultValue = 'card';
                }
            }
        }

        // neu loai la card
        if (!this.type_res) {
            this.currentAction.inputs = this.currentAction.inputs.filter((input) => {
                return (input.param != 'title' && input.param != 'description' && input.param != 'url_image');
            });
        }

        this.currentAction.outputs = outputs;

        for (var i =0 ; i< this.currentAction.inputs.length; i++) {
            if (this.currentAction.inputs[i].meta != undefined) {
                for (var k = 0; k < this.currentAction.inputs[i].meta.length; k++) {
                    if (this.currentAction.inputs[i].meta[k].key=='searchType' && this.currentAction.inputs[i].meta[k].value == '') {
                        this.currentAction.inputs[i].meta[k].value = 'proximate';
                    }
                }
            }
        }
        for (var i = 0 ; i< this.currentAction.inputs.length; i++) {
            if (this.currentAction.inputs[i].defaultValue != undefined) {
                this.currentAction.inputs[i].defaultValue = this.currentAction.inputs[i].defaultValue.trim();
            }
        }

        this.addAction((cb) => {
            if (cb) {

            } else {
                this.notificationService.showDanger('Thêm hành động thất bại');
            }
        });
    }

    public addEvent(event, callback) {
        let actionOperate: Observable<ActionModel>;
        actionOperate = this.eventService.add_or_update_array(event);
        actionOperate.subscribe(
            (action_add) => {
                let outputs:Outputs[] = [];
                if (this.currentAction.actionBaseName != 'search') {
                    /*
                    // OLD
                     var tmp_output = new Outputs();
                     tmp_output.eventId = action_add[0].id;
                     tmp_output.eventBaseName = 'no_solution';
                     tmp_output.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";
                     outputs.push(tmp_output);
                     */

                    var tmp_output = new Outputs();
                    tmp_output.eventId = action_add[1].id;
                    tmp_output.eventBaseName = 'no_solution';
                    tmp_output.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";
                    outputs.push(tmp_output);

                    // Thêm theo ý Hiếu gà
                    var tmp_output_has_solution = new Outputs();
                    tmp_output_has_solution.eventId = action_add[0].id;
                    tmp_output_has_solution.eventBaseName = 'has_solution';
                    tmp_output_has_solution.eventName = this.currentAction.nameDisplay.trim() + " có kết quả";
                    outputs.push(tmp_output_has_solution);
                } else {
                    var tmp_output_has_solution = new Outputs();
                    tmp_output_has_solution.eventId = action_add[0].id;
                    tmp_output_has_solution.eventBaseName = 'has_solution';
                    tmp_output_has_solution.eventName = this.currentAction.nameDisplay.trim() + " có kết quả";

                    var tmp_output_no_solution = new Outputs();
                    tmp_output_no_solution.eventId = action_add[1].id;
                    tmp_output_no_solution.eventBaseName = 'no_solution';
                    tmp_output_no_solution.eventName = this.currentAction.nameDisplay.trim() + " không có kết quả";

                    var tmp_output_exhause_solution = new Outputs();
                    tmp_output_exhause_solution.eventId = action_add[2].id;
                    tmp_output_exhause_solution.eventBaseName = 'exhause_solution';
                    tmp_output_exhause_solution.eventName = this.currentAction.nameDisplay.trim() + " hết kết quả";

                    outputs.push(tmp_output_has_solution);
                    outputs.push(tmp_output_no_solution);
                    outputs.push(tmp_output_exhause_solution);
                }

                //this.notificationService.showSuccess('Thêm event thành công');
                callback(true, outputs);
            },
            (err) => {
                // load event fail
                callback(false, []);
            });
    }

    public addAction(callback) {
        let actionOperate: Observable<ActionModel>;
        actionOperate = this.actionsService.add(this.currentAction);
        actionOperate.subscribe(
            (action_add) => {
                this.notificationService.showSuccess('Thêm hành động thành công');
                this.router.navigate(['/pages/actions/actionEdit/' + action_add.actionId], { relativeTo: this.route });
                callback(true);
            },
            (err) => {
                // load event fail
                callback(false);
            });
    }


    public filterStates(val: string) {
        return val ? this.states.filter((s) => new RegExp(val, 'gi').test(s)) : this.states;
    }

    public checkEmptyUpdate(value) {
        if (value != null && value != '') {
            this.currentAction_name_custom_res = StringFormat.formatText(value);
        }
    }

    public updateNumber(event, index) {
        if (event.target.value == 0) {
            this.currentAction.inputs[index].meta[2].value = 1;
        }
    }

    public getListProduct() {
        this.productsService.getListProductByPartnerId(this.currentUser.partner_id)
            .then((data) => {
                this.states = data;
            })
    }

    public getListActionTemplate() {
        let actionOperate: Observable<ActionBasic[]>;
        actionOperate = this.actionsService.getListActionTemplate();
        actionOperate.subscribe(
            (data) => {
                this.actionBasics = data;
            },
            (err) => {
                this.notificationService.showDanger("Lỗi khi lấy danh sách action template");
            });
    }

    public ngOnInit() {
    }

    public ngAfterViewInit() {
        this.getListActionTemplate();
    }

    public ngOnChanges(changes: any) {
    }

    public getStyle(value) {
        if (value != undefined && value.trim().length == 0) {
            return "#f44336";
        } else {
            return "";
        }
    }
}

