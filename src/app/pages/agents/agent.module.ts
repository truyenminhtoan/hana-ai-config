import { ReactiveFormsModule } from '@angular/forms';
// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from "./agent.routing";
import { AgentListComponent } from "./agent-list/agent-list.component";
import { AgentComponent } from "./agent.component";
import { NotificationService } from "../../_services/notification.service";
import { ActionsPipe } from './pipes/actions.pipe';
import { InputTextareaModule } from 'primeng/primeng';
import { NgaModule } from "../../themes/nga.module";
@NgModule({
    imports: [
        NgaModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        routing,
        InputTextareaModule,
    ],
    declarations: [
        AgentListComponent,
        AgentComponent,
        ActionsPipe,

    ],
    providers: [
        NotificationService
    ],
    exports: [
        AgentListComponent,
        AgentComponent,
        ActionsPipe,

    ]
})
export class AgentModule {

}
