import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AgentsModel } from '../../../_models/agents.model';
import { NotificationService } from '../../../_services/notification.service';
import { UserModel } from '../../../_models/user.model';
import { Observable } from "rxjs/Rx";
import { AgentService } from '../../../_services/agents.service';
import { ComponentInteractionService } from '../../../_services/compoent-interaction-service';
import { StringFormat } from '../../../themes/validators/string.format';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
import { Subscription } from 'rxjs/Rx';
declare var swal: any;

@Component({
    selector: 'agent-list',
    templateUrl: 'agent-list.component.html',
    styleUrls: ['agent-list.component.css']
})
export class AgentListComponent implements OnInit {

    private currentUser: UserModel;
    private agents = [];
    private currentAgent: AgentsModel = new AgentsModel();
    private id_loading = true;
    @ViewChild('btnCloseAdd') btnCloseAdd: ElementRef;
    @ViewChild('btnCloseEdit') btnCloseEdit: ElementRef;
    @ViewChild('name_new') name_new: ElementRef;
    @ViewChild('name_edit') name_edit: ElementRef;
    private eventSubscription: Subscription;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private agentService: AgentService,
        private _componentService: ComponentInteractionService) {
    }

    ngOnInit() {

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser != undefined) {
            this.loadListAgent();
            this.intAgent();
        } else {
            this.notificationService.showNoPartnerId();
        }
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-agents') {
                    this.loadListAgent();
                }
            }
        );

        if (localStorage.getItem('remarketting') == 'true') {
            window.location.reload();
            localStorage.setItem('remarketting', 'false')
        }

        this._componentService.eventPublisher$.next('load-agents-delete');
    }

    public clickAgentEdit() {
        this._componentService.eventPublisher$.next('click-agent-edit');
    }

    public intAgent() {
        this.currentAgent.name = '';
        this.currentAgent.partner_user_id = this.currentUser.id;
        this.currentAgent.partner_id = this.currentUser.partner_id;
        this.currentAgent.is_public = true;
        this.currentAgent.status = 1;
        this.currentAgent.time_zone = "GMT+7";
        this.currentAgent.language = "Việt Nam";
    }

    public loadListAgent() {
        this.getListAgentByPartnerId(this.currentUser.partner_id);
        this.notificationService.clearConsole();
    }

    public getListAgentByPartnerId(partnerId) {
        try {
            this.agentService.getListAgentByPartnerId(partnerId)
                .then((data) => {
                    this.id_loading = false;
                    this.agents = data;
                    if (this.agents.length > 0) {
                        for (var i = 0; i < this.agents.length; i++) {
                            this.agents[i].tmp_name = this.agents[i].name;
                            this.agents[i].tmp_avatar_url = this.agents[i].avatar_url;
                            if (this.agents[i].name != null) {
                                this.agents[i].name = this.agents[i].name.slice(0, 20);
                                if (this.agents[i].name.length >= 15) {
                                    this.agents[i].name += '...';
                                }
                            }
                            if (this.agents[i].description != null) {
                                this.agents[i].tmp_description = this.agents[i].description;
                                this.agents[i].description = this.agents[i].description.slice(0, 17);
                                this.agents[i].description_full = this.agents[i].description;
                                if (this.agents[i].description.length >= 20) {
                                    this.agents[i].description += '...';
                                }
                            }
                        }
                        this.agents.sort(function (a, b) {
                            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            // names must be equal
                            return 0;
                        });
                    }
                });
        } catch (error) {
            this.notificationService.showWarning("Err get List Agent By PartnerId");
        }
    }

    public checkName(name, id) {
        if (id != false) {
            for (var i = 0; i < this.agents.length; i++) {
                if (this.agents[i].id != id && this.agents[i].tmp_name.trim() == name.trim()) {

                    return false;
                }
            }
        } else {
            for (var i = 0; i < this.agents.length; i++) {
                if (this.agents[i].tmp_name.trim() == name.trim()) {
                    return false;
                }
            }
        }

        return true;
    }

    public updateStatus() {
        // toantm fix bug ko show diablog
        $('#myModal').modal('show');

        this.currentAgent = new AgentsModel();
        this.intAgent();
    }

    public newAgent() {
        if (this.currentAgent.name == null || this.currentAgent.name.trim().length == 0) {
            this.notificationService.showWarning('Tên trợ lý không được rỗng');
            this.name_new.nativeElement.focus();
        } else {
            if (this.currentAgent.name.trim().length <= 50) {
                console.log('this.currentAgent.name', this.currentAgent.name);
                if (StringFormat.checkRegexp(this.currentAgent.name, /([?*+!%`=^$@#[\]\\(){}|-])/)) {
                    this.notificationService.showWarning('Tên trợ lý không được chưa ký tự đặc biệt');
                    this.name_new.nativeElement.focus();
                } else {
                    this.agentService.getListAgentByPartnerId(this.currentUser.partner_id)
                        .then((data) => {
                            this.agents = data;
                            for (var i = 0; i < this.agents.length; i++) {
                                this.agents[i].tmp_name = this.agents[i].name;
                                this.agents[i].tmp_avatar_url = this.agents[i].avatar_url;
                                if (this.agents[i].name != null) {
                                    this.agents[i].name = this.agents[i].name.slice(0, 20);
                                    if (this.agents[i].name.length >= 15) {
                                        this.agents[i].name += '...';
                                    }
                                }
                                if (this.agents[i].description != null) {
                                    this.agents[i].tmp_description = this.agents[i].description;
                                    this.agents[i].description = this.agents[i].description.slice(0, 17);
                                    this.agents[i].description_full = this.agents[i].description;
                                    if (this.agents[i].description.length >= 20) {
                                        this.agents[i].description += '...';
                                    }
                                }
                            }
                            this.agents.sort(function (a, b) {
                                var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                                if (nameA < nameB) {
                                    return -1;
                                }
                                if (nameA > nameB) {
                                    return 1;
                                }
                                // names must be equal
                                return 0;
                            });

                            // thuc hien them
                            if (this.checkName(this.currentAgent.name, false)) {
                                this.currentAgent.name = this.currentAgent.name;
                                this.currentAgent.avatar_url = this.currentAgent.avatar_url;
                                this.currentAgent.description = this.currentAgent.description;
                                if (this.currentAgent.name != undefined && this.currentAgent.name != null) {
                                    this.currentAgent.name = this.currentAgent.name.trim();
                                }
                                if (this.currentAgent.avatar_url != undefined && this.currentAgent.avatar_url != null) {
                                    this.currentAgent.avatar_url = this.currentAgent.avatar_url.trim();
                                }
                                if (this.currentAgent.description != undefined && this.currentAgent.description != null) {
                                    this.currentAgent.description = this.currentAgent.description.trim();
                                }
                                this.pushNewAgent((cb) => {
                                    if (cb) {
                                        this.loadListAgent();
                                        this._componentService.eventPublisher$.next('load-sidebar');
                                        this._componentService.eventPublisher$.next('load-agents');
                                        this.notificationService.showSuccess('Thêm trợ lý thành công');
                                        this.btnCloseAdd.nativeElement.click();
                                        $('body').removeClass("modal-open");
                                        this.router.navigate(['/pages/agents/agentList'], { relativeTo: this.route });
                                    } else {
                                        this.notificationService.showDanger('Thêm trợ lý thất bại');
                                    }
                                });
                            } else {
                                this.name_new.nativeElement.focus();
                                this.notificationService.showWarning('Tên trợ lý đã tồn tại');
                            }
                        });
                }
            } else {
                this.name_edit.nativeElement.focus();
                this.notificationService.showWarning('Tên trợ lý tối đa 50 kí tự');
            }
        }
    }

    public pushNewAgent(callback) {
        this.currentAgent.name = this.currentAgent.name.trim();
        let actionOperate: Observable<AgentsModel>;
        actionOperate = this.agentService.add(this.currentAgent);
        actionOperate.subscribe(
            (data) => {
                this.currentAgent = data;
                localStorage.setItem('currentAgent_Id', this.currentAgent.id);
                callback(true);
            },
            (err) => {
                callback(false);
            });
    }

    public deleteAgent(id: string, agentName) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.agentService.deleteAgent(id)
                .then((res) => {
                    let agentId = localStorage.getItem('currentAgent_Id');
                    if (agentId != null && agentId != undefined) {
                        if (id == agentId) {
                            // Update agentId
                            var check_null = true;
                            for (var i = 0; i < this.agents.length; i++) {
                                if (this.agents[i].id !== id) {
                                    check_null = false;
                                    localStorage.setItem('currentAgent_Id', this.agents[i].id);
                                    break;
                                }
                            }
                            if (check_null) {
                                localStorage.setItem('currentAgent_Id', '');
                            }
                        }
                    }
                    this.agents = this.agents.filter((row) => {
                        return row.id !== id
                    });
                    this._componentService.eventPublisher$.next('load-sidebar');
                    this._componentService.eventPublisher$.next('load-agents');
                    $('.modal').modal('hide');
                    $('body').removeClass("modal-open");
                    this.notificationService.showSuccess('Xóa trợ lý "' + agentName + '" thành công');
                });
        }, (dismiss) => {
            console.log(dismiss);
        });
    }

    public showAgentEdit(id) {
        // toantm fix bug ko show diablog
        $('#myModalEdit_' + id).modal('show');
    }
}
