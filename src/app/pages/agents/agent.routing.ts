import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {AgentComponent} from "./agent.component";
import {AgentListComponent} from "./agent-list/agent-list.component";

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: AgentComponent,
        children: [
            { path: 'agentList', component: AgentListComponent },
            { path: 'agentNew', component: AgentListComponent }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
