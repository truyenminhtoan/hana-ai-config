import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { Validator } from './../../../../../../themes/validators/validator';
import { Component, Input, OnInit, OnChanges, AfterContentInit, SimpleChanges } from '@angular/core';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { Observable } from 'rxjs/Rx';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
declare var jQuery: any;
declare var $: any;
declare var swal: any;
import * as _ from 'underscore';
import { NotificationService } from '../../../../../../_services/notification.service';
import { StringFormat } from '../../../../../../themes/validators/string.format';
import { IntentService } from '../../../../../../_services/intents.service';
@Component({
  selector: 'campaigns-send-now',
  templateUrl: 'campaigns-send-now.component.html',
  styleUrls: ['campaigns-send-now.component.scss']
})
export class CampaignsSendNowComponent implements OnChanges {
  @Input() private campaignSendNowId: string;
  @Input() private campaignSendNowChangeNameId: string;
  @Input() private appId: string;
  @Input() private campaign: any = { id: '', name: '' };
  private loadingProcess = false;
  private campaignName = 'hi';
  private searchTypeData;
  private templateBroadcast: any = {};
  private messageContent = '';
  private queries = [];
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  // block
  private changeBlockId: string = 'BLOCK_CHANGE_ID';
  private outputBlockId: string = 'BLOCK_OUTPUT_ID';
  private block = { block_group_id: 'default', id: 'default', name: 'BLOCK 1', content: '[{\"type\":1,\"message\":{\"text\":\"eeeeeeeee\"}}]', type: 'content' };
  private blockCurrent: any = {};

  // isSending
  private isSending = false;
  private isSendingTimeOut = 5 * 1000; // 5s

  private isTachTiep = false;

  // filter moi
  private isLoadDataFilter = false;
  private searchTypeDataFilter2;
  private conditionsSaved = [];
  private conditionDefault: any = {
    syntax: 'VÀ',
    field: {
      value: '',
      label: ''
    },
    operator: {
      value: '',
      label: ''
    },
    value: {
      value: '',
      label: ''
    }
  };

  constructor(private configService: ConfigService, private log: Log, private campaignService: CampaignService, private intentService: IntentService, private facebookService: FacebookService, private notificationService: NotificationService) { }

  public ngOnChanges(changes: SimpleChanges): void {
    this.isTachTiep = false;
    // tslint:disable-next-line:only-arrow-functions
    $('.campaign-name').keydown(function (event) {
      if (event.keyCode === 13) {
        this.log.info('`Enter` key is not allowed.');
        event.preventDefault();
        return false;
      }
    });

    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
      this.getSearchTypeFilterV2(this.appId);
    }
    if (!this.appId) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng.');
      return;
    }
    let temp = [];
    this.isSending = false;
    this.block.content = '[]';
    this.blockCurrent = this.block;
    if (this.campaign.template_broadcast.length === 0) {
      let body = { campaign_id: this.campaign.id, content: '' };
      this.addTemplateBroadcast(body);
    } else {
      this.templateBroadcast = this.campaign.template_broadcast[0];
      if (!this.templateBroadcast.content) {
        this.templateBroadcast.content = '';
      }

      // checkbox
      if (this.templateBroadcast.data_set_size && this.templateBroadcast.expand_days) {
        this.isTachTiep = true;
      }

      this.blockCurrent.content = this.templateBroadcast.content;
      EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
    }
  }

  public setLoading() {
    this.loadingProcess = true;
  }

  public setUnLoading() {
    this.loadingProcess = false;
  }

  public campaignChange() {
    let body = { id: this.campaign.id, name: this.campaign.name };
    this.updateCampaign(body);
  }

  public _addCondition2(condition) {
    let that = this;
    let arr = [];
    if (condition) {
      arr = JSON.parse(condition);
    }
    this.conditionsSaved = arr;
    this.getTotalCustomer(arr);
  }

  public updateCampaign(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateCampaign(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
        EmitterService.get(this.campaignSendNowChangeNameId).emit(this.campaign);
      },
      (error) => {
        // this.log.info(error);
        this.log.info(error);
      });
  }

  public addTemplateBroadcast(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('them thanh cong');
        this.templateBroadcast = result;
      },
      (error) => {
        // this.log.info(error);
        this.log.info(error);
      });
  }

  public updateTemplateBroadcast() {
    // this.templateBroadcast.content = this.messageContent;
    // if (this.templateBroadcast.content_text && !this.templateBroadcast.content) {
    //   this.templateBroadcast.content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
    // }
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateTemplateBroadcast(this.templateBroadcast);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat thanh cong');
      },
      (error) => {
        this.log.info(error);
      });
  }

  public send() {
    swal({
      title: this.configService.facebookPolicyTitle,
      text: this.configService.facebookPolicy,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Tiếp tục gửi',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      //
      this.sending();
    }, (dismiss) => {
      //
    });
  }

  public sending() {
    if (_.isEmpty(this.appId)) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng muốn gửi tin nhắn');
      return;
    }

    if (this.totalRecords === 0) {
      this.notificationService.showDanger('Không có khách hàng thỏa điều kiện gửi');
      return;
    }

    if (this.isTachTiep) {
      if (this.templateBroadcast.data_set_size !== null && (this.templateBroadcast.data_set_size < 1 || this.templateBroadcast.data_set_size > 1000)) {
        this.notificationService.showDanger('Số lượng khách hàng nhập trong khoảng 1 đến 1000');
        return;
      }

      if (this.templateBroadcast.data_set_size !== null && this.templateBroadcast.data_set_size % 1 !== 0) {
        this.notificationService.showDanger('Số lượng khách hàng là số nguyên dương');
        return;
      }

      if (this.templateBroadcast.data_set_size !== null && this.templateBroadcast.expand_days === null) {
        this.notificationService.showDanger('Bạn phải nhập thời gian gửi giữa mỗi đợt');
        return;
      }

      if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.expand_days <= 0) {
        this.notificationService.showDanger('Thời gian gửi giữa mỗi đợt phải lớn hơn 0');
        return;
      }

      if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.expand_days % 1 !== 0) {
        this.notificationService.showDanger('Thời gian gửi là số nguyên dương');
        return;
      }
    }

    if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.data_set_size === null) {
      this.notificationService.showDanger('Bạn phải nhập số lượng khách hàng');
      return;
    }

    let body: any = {};
    body.app_id = this.appId;
    body.template_broadcast_id = this.templateBroadcast.id;

    body.campaign_id = this.templateBroadcast.campaign_id;
    if (this.isTachTiep === true && this.templateBroadcast.data_set_size !== null && this.templateBroadcast.expand_days !== null) {
      body.data_set_size = this.templateBroadcast.data_set_size;
      body.expand_days = this.templateBroadcast.expand_days;
    }

    body.customer_filter = JSON.parse(this.campaign.customer_target);
    if (this.templateBroadcast.content) {
      body.structure_content = this.templateBroadcast.content;
    }
    //  else {
    //   body.structure_content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
    // }
    this.formatBlock(body.structure_content, (cb) => {
      if (!cb || JSON.parse(cb).length === 0) {
        this.notificationService.showDanger('Vui lòng nhập đầy đủ nội dung tin nhắn gửi đến khách hàng');
        return;
      }
      body.structure_content = cb;
      this.sendMessages(body);
    });
    // alert(JSON.stringify(body));
  }

  public sendMessages(body) {
    this.isSending = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.sendMessageStruct(body);
    actionOperate.subscribe(
      (cus) => {
        if (cus && cus.code === 200) {
          this.notificationService.showSuccess('Yêu cầu gửi tin nhắn của bạn đã được tiếp nhận');
          setTimeout(() => this.isSending = false, this.isSendingTimeOut);
        } else {
          this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
          this.isSending = false;
        }
      },
      (error) => {
        this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        this.isSending = false;
        this.log.info(error);
      });
  }

  public messageChange(event) {
    if (event.target.textContent.length >= 10) {
      event.preventDefault();
    }
  }

  public getTotalCustomer(body) {
    let options: any = {};
    options.app_id = this.appId;
    options.queries = body;
    let actionOperate: Observable<any>;
    options.has_fb_userchat = true; // chi lay kh co userchat
    actionOperate = this.facebookService.getTotalCustomer(options);
    actionOperate.subscribe(
      (result) => {
        this.totalRecords = result.count;
        this.totalNotFb = result.not_fb_count;
        this.totalFb = result.fb_count;
      },
      (error) => {
        this.log.info(error);
      });
  }

  public updateBlock(body) {
    try {
      // alert(JSON.stringify(body));
      let content = JSON.parse(body.content);
      if (this.templateBroadcast) {
        if (content.length === 0) {
          this.templateBroadcast.content = '';
        } else {
          this.templateBroadcast.content = body.content;
        }
        this.updateTemplateBroadcast();
      }
    } catch (error) {
      this.log.info(error);
    }
  }

  public formatBlock(content, callback) {
    if (!content) {
      callback(null);
      return;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getValidateFacebookJSON({ messages: content });
    actionOperate.subscribe(
      (json) => {
        if (json.code === 200) {
          callback(json.data);
        } else {
          callback(null);
        }
      },
      (error) => {
        callback(null);
        this.log.info(error);
      });
  }

  public deleteCampaign(campaign) {
    EmitterService.get('DELETE_CAMPAIGN').emit(campaign);
  }

  public updateDataSize(val) {
    // (keyup)="templateBroadcast.data_set_size='122334'.replace(/[^0-9]/g,'')"
    this.log.info(val.value);
    this.templateBroadcast.data_set_size = 1;
    val.value = 1;
  }

  public clearTachtiep() {
    if (!this.isTachTiep) {
      this.templateBroadcast.data_set_size = null;
      this.templateBroadcast.expand_days = null;
      this.updateTemplateBroadcast();
    }
  }

  public getSearchTypeFilterV2(appId) {
    this.setLoading();
    this.isLoadDataFilter = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        this.searchTypeDataFilter2 = result;
        this.isLoadDataFilter = false;
        this._addCondition2(this.campaign.customer_target);
        this.setUnLoading();
      },
      (error) => {
        this.setUnLoading();
        this.isLoadDataFilter = false;
        this.log.info(error);
      });
  }

  public filterv2(data) {
    this.queries = data.query_filter;
    this.getTotalCustomer(this.queries);
    let body = { id: this.campaign.id, customer_target: JSON.stringify(this.queries) };
    this.campaign.customer_target = JSON.stringify(this.queries);
    this.updateCampaign(body);
  }
}
