import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { Validator } from './../../../../../../themes/validators/validator';
import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { Observable } from 'rxjs/Rx';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { StringFormat } from '../../../../../../themes/validators/string.format';
import * as moment from 'moment';
import * as _ from 'underscore';
import { NotificationService } from '../../../../../../_services/notification.service';
declare var jQuery: any;
declare var $: any;
declare var swal: any;
@Component({
  selector: 'campaigns-schedule',
  templateUrl: 'campaigns-schedule.component.html',
  styleUrls: ['campaigns-schedule.component.scss']
})
export class CampaignsScheduleComponent implements OnInit, OnChanges {
  @Input() private campaignScheduleId: string;
  @Input() private campaignScheduleChangeNameId: string;
  @Input() private appId: string;
  @Input() private campaign: any = { id: '', name: '' };
  private loadingProcess = false;
  private campaignName = '';
  private searchTypeData;
  private templateBroadcast: any = {};
  private messageContent = '';
  private queries = [];
  private textAll = '';
  private checked = false;
  private dateValue;
  private timeValue;
  private repeatType = 'none';
  private daysSelected: any[] = [2, 3, 4, 5, 6, 7, 1];
  // Sum customer filter
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  // enable campaign
  private isEnabled = true;
  // tslint:disable-next-line:max-line-length
  private repeatValueInit = [{ label: 'Lặp lại: Không', name: 'none' }, { label: 'Lặp lại: Mỗi ngày', name: 'everyday' }, { label: 'Lặp lại: Cuối tuần (T7-CN)', name: 'weekends' }, { label: 'Lặp lại: Mỗi tháng', name: 'everymonth' }, { label: 'Lặp lại: Ngày làm việc (T2-T6)', name: 'workdays' }, { label: 'Lặp lại: Tùy chọn', name: 'custom' }];
  private daysOfWeekInit = [{ label: 'Thứ hai', value: 2, active: true }, { label: 'Thứ ba', value: 3, active: true }, { label: 'Thứ tư', value: 4, active: true }, { label: 'Thứ năm', value: 5, active: true }, { label: 'Thứ sáu', value: 6, active: true }, { label: 'Thứ bảy', value: 7, active: true }, { label: 'Chủ nhật', value: 1, active: true }];
  private repeatValue = [];
  private daysOfWeek = [];

  private input14Moment: any = new Date();
  private minMoment = new Date();
  private pickerColor: string = '#000';

  private block = { block_group_id: 'default', id: 'default', name: 'BLOCK 1', content: '[{\"type\":1,\"message\":{\"text\":\"eeeeeeeee\"}}]', type: 'content' };
  private blockCurrent: any = {};

  // filter moi
  private isLoadDataFilter = false;
  private searchTypeDataFilter2;
  private conditionsSaved = [];
  private conditionDefault: any = {
    syntax: 'VÀ',
    field: {
      value: '',
      label: ''
    },
    operator: {
      value: '',
      label: ''
    },
    value: {
      value: '',
      label: ''
    }
  };

  constructor(private configService: ConfigService, private log: Log, private notificationService: NotificationService, private campaignService: CampaignService, private facebookService: FacebookService) { }

  public setLoading() {
    this.loadingProcess = true;
  }

  public setUnLoading() {
    this.loadingProcess = false;
  }

  public ngOnInit() {
    // tslint:disable-next-line:only-arrow-functions
    $('.campaign-name').keydown(function (event) {
      if (event.keyCode === 13) {
        this.log.info('`Enter` key is not allowed.');
        event.preventDefault();
        return false;
      }
    });
    let that = this;
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
      this.getSearchTypeFilterV2(this.appId);
    }
    if (!this.appId) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng.');
      return;
    }

    let temp = [];
    EmitterService.get(this.campaignScheduleId).subscribe((data: any) => {
      // this._clearCondition();
      // this.getSearchType(this.appId);
      // this.campaignName = data.name;
      let d = new Date();
      this.dateValue = moment(d).format('YYYY-MM-DD');
      this.timeValue = d;
      this.repeatType = 'none';

      // block
      this.block.content = '[]';
      this.blockCurrent = this.block;

      // init
      this.repeatValue = this.repeatValueInit;
      this.daysOfWeek = this.daysOfWeekInit;

      this.campaign = data;
      if (data.template_broadcast.length === 0) {
        let body = { campaign_id: this.campaign.id, content: '' };
        this.addTemplateBroadcast(body);
      } else {
        this.templateBroadcast = data.template_broadcast[0];
        if (!this.templateBroadcast.content) {
          this.templateBroadcast.content = '';
        }
        this.blockCurrent.content = this.templateBroadcast.content;

        EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
        // init condition
        this.daysOfWeek = _.map(this.daysOfWeek, (element) => {
          return _.extend({}, element, { active: true });
        });
        if (this.templateBroadcast.condition) {
          let condition = JSON.parse(this.templateBroadcast.condition);
          if (condition.start_time) {
            let d1 = new Date(condition.start_time);
            this.input14Moment = d1;
          }
          if (condition.repeat) {
            this.repeatType = condition.repeat.name;
            if (condition.repeat.dayofweek) {
              this.daysSelected = condition.repeat.dayofweek;
              this.daysOfWeek.filter((item) => {
                if (_.contains(this.daysSelected, item.value)) {
                  item.active = true;
                } else {
                  item.active = false;
                }
              });
            }
          }
        }
        // this._clearCondition();
        // alert(this.daysSelected);
        this._addCondition(data.customer_target);
      }
    });
  }

  public ngOnChanges(changes: SimpleChanges): void {
    $('.campaign-name').keydown(function (event) {
      if (event.keyCode === 13) {
        this.log.info('`Enter` key is not allowed.');
        event.preventDefault();
        return false;
      }
    });
    let that = this;
    // $.getScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', () => {
    // $.getScript('../../../../assets/js/core/jquery-ui.min.1.12.1.js', () => {
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
      this.getSearchTypeFilterV2(this.appId);
    }
    if (!this.appId) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng.');
      return;
    }

    let temp = [];
    // this._clearCondition();
    // this.getSearchType(this.appId);
    // this.campaignName = data.name;
    let d = new Date();
    this.dateValue = moment(d).format('YYYY-MM-DD');
    this.timeValue = d;
    this.repeatType = 'none';

    // block
    this.block.content = '[]';
    this.blockCurrent = this.block;

    // init
    this.repeatValue = this.repeatValueInit;
    this.daysOfWeek = this.daysOfWeekInit;

    // this.log.info('get emiit: ', JSON.stringify(data));
    //  alert('new: ' +JSON.stringify(this.campaign));
    if (this.campaign.template_broadcast.length === 0) {
      let body = { campaign_id: this.campaign.id, content: '' };
      this.addTemplateBroadcast(body);
    } else {
      this.templateBroadcast = this.campaign.template_broadcast[0];
      if (!this.templateBroadcast.content) {
        this.templateBroadcast.content = '';
      }
      this.blockCurrent.content = this.templateBroadcast.content;

      EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
      // init condition
      this.daysOfWeek = _.map(this.daysOfWeek, (element) => {
        return _.extend({}, element, { active: true });
      });
      if (this.templateBroadcast.condition) {
        let condition = JSON.parse(this.templateBroadcast.condition);
        if (condition.start_time) {
          let d1 = new Date(condition.start_time);
          this.input14Moment = d1;
        }
        if (condition.repeat) {
          this.repeatType = condition.repeat.name;
          if (condition.repeat.dayofweek) {
            this.daysSelected = condition.repeat.dayofweek;
            this.daysOfWeek.filter((item) => {
              if (_.contains(this.daysSelected, item.value)) {
                item.active = true;
              } else {
                item.active = false;
              }
            });
          }
        }
      }
    }
    // });
  }

  public campaignChange() {
    let body = { id: this.campaign.id, name: this.campaign.name };
    this.updateCampaign(body);
  }

  public getSearchType(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        let searchType = [];
        result.filter((item) => {
          let search: any = { id: item.param, type: item.value_type, label: item.name };
          if (item.value_type === 'list') {
            if (item.page_values.length > 0) {
              search.list = item.page_values;
              searchType.push(search);
            }
          } else {
            searchType.push(search);
          }
        });
        this.searchTypeData = searchType;
        // alert(this.searchTypeData);
        $.getScript('../../../../../assets/plugins/structured-filter/js/structured-filter.js', () => {
          this._queryBuilder2();
        });
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public _queryBuilder2() {
    let that = this;
    $('#myFilterSchedule').structFilter({
      highlight: true,
      buttonLabels: true,
      submitReady: true,
      fields: this.searchTypeData
    });

    this._addCondition(this.campaign.customer_target);

    // tslint:disable-next-line:only-arrow-functions
    $('#myFilterSchedule').on('submit.search', function (event) {
      // do something
    });
    $('#myFilterSchedule').on('change.search', function (event) {
      // do something
      // let queries = [];
      let data = $('#myFilterSchedule').structFilter('val');
      that.queries = data;

      // data.filter((item) => {
      //   item.value.label = item.value.value;
      // });
      // that.templateBroadcast.condition = JSON.stringify(data);
      // alert('update condition:' + JSON.stringify(that.templateBroadcast));
      // that.updateTemplateBroadcast();
      // alert('old:'+JSON.stringify(that.campaign));
      let body = { id: that.campaign.id, customer_target: JSON.stringify(data) };
      that.updateCampaign(body);
      that.getTotalCustomer(data);
    });
  }

  public _addCondition(condition) {
    let that = this;
    let arr = [];
    if (condition) {
      arr = JSON.parse(condition);
    }
    $.getScript('../../../../../assets/plugins/structured-filter/js/structured-filter.js', () => {
      $('#myFilterSchedule').structFilter({
        highlight: true,
        buttonLabels: true,
        submitReady: true
      });
      $('#myFilterSchedule').structFilter('val', arr);
    });
  }

  public changeIsOn(event) {
    // if (event.checked === true && (!this.templateBroadcast || !this.templateBroadcast.text_content || this.templateBroadcast.text_content.trim().length === 0)) {
    //   this.notificationService.showDanger('Vui lòng nhập nội dung gửi tin nhắn');
    //   this.templateBroadcast.is_on = false;
    //   event.checked = false;
    //   return;
    // }
    this.templateBroadcast.is_on = event;
    this.updateTemplateBroadcast();
  }

  public updateCampaign(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateCampaign(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
        EmitterService.get(this.campaignScheduleChangeNameId).emit(this.campaign);
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public addTemplateBroadcast(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('them thanh cong');
        this.templateBroadcast = result;
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public updateTemplateBroadcast() {
    try {
      // this.templateBroadcast.content = this.messageContent;
      // if (this.templateBroadcast.content_text && !this.templateBroadcast.content) {
      //   this.templateBroadcast.content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
      // }
      this.templateBroadcast.type = 'scheduler';
      let actionOperate: Observable<any>;
      actionOperate = this.campaignService.updateTemplateBroadcast(this.templateBroadcast);
      actionOperate.subscribe(
        (result) => {
          // this.log.info('cap nhat thanh cong');
        },
        (err) => {
          // this.log.info(err);
        });
    } catch (error) {

    }
  }

  public changeDate(event) {
    let dateStr = moment(event).format('YYYY-MM-DD HH:mm');
    if (this.templateBroadcast.condition) {
      let condition = JSON.parse(this.templateBroadcast.condition);
      condition.start_time = dateStr;
      this.templateBroadcast.condition = JSON.stringify(condition);
    } else {
      let condition: any = {};
      condition.start_time = dateStr;
      this.templateBroadcast.condition = JSON.stringify(condition);
    }
    this.updateTemplateBroadcast();

    // let scheduleName = moment(event).format('DD/MM/YYYY HH:mm');
    // let body = { id: this.campaign.id, name: scheduleName };
    // this.campaign.name = scheduleName;
    // this.updateCampaign(body);
  }

  public changeRepeatType(event) {
    // INIT
    this.daysOfWeek = this.daysOfWeekInit;
    this.daysSelected = [2, 3, 4, 5, 6, 7, 1];
    // alert(event);
    if (this.templateBroadcast.condition) {
      let condition = JSON.parse(this.templateBroadcast.condition);
      condition.repeat = { name: event };
      if (event === 'custom') {
        condition.repeat = { name: 'custom', dayofweek: this.daysSelected };
      }
      this.templateBroadcast.condition = JSON.stringify(condition);
    } else {
      let condition: any = {};
      condition.repeat = { name: event };
      if (event === 'custom') {
        condition.repeat = { name: 'custom', dayofweek: this.daysSelected };
      }
      this.templateBroadcast.condition = JSON.stringify(condition);
    }
    this.updateTemplateBroadcast();
  }

  public selectDays(value) {
    if (_.contains(this.daysSelected, value)) {
      this.daysSelected = _.without(this.daysSelected, value);
    } else {
      this.daysSelected.push(value);
    }
    // sort day
    this.daysSelected = _.sortBy(this.daysSelected);
    if (_.contains(this.daysSelected, 1)) {
      this.daysSelected = _.reject(this.daysSelected, (num) => { return num === 1; });
      this.daysSelected.push(1);
    }

    if (this.templateBroadcast.condition) {
      let condition = JSON.parse(this.templateBroadcast.condition);
      if (this.daysSelected.length === 7) {
        condition.repeat = { name: 'everyday' };
      } else if (this.daysSelected.length === 0) {
        condition.repeat = { name: 'none' };
      } else {
        condition.repeat = { name: 'custom', dayofweek: this.daysSelected };
      }
      this.templateBroadcast.condition = JSON.stringify(condition);
      this.updateTemplateBroadcast();
    }
  }

  public getTotalCustomer(body) {
    let options: any = {};
    options.app_id = this.appId;
    options.queries = body;
    let actionOperate: Observable<any>;
    options.has_fb_userchat = true; // chi lay kh co userchat
    actionOperate = this.facebookService.getTotalCustomer(options);
    actionOperate.subscribe(
      (result) => {
        this.totalRecords = result.count;
        this.totalNotFb = result.not_fb_count;
        this.totalFb = result.fb_count;
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public updateBlock(body) {
    try {
      // alert(JSON.stringify(body));
      let content = JSON.parse(body.content);
      if (this.templateBroadcast) {
        if (content.length === 0) {
          this.templateBroadcast.content = '';
        } else {
          this.templateBroadcast.content = body.content;
        }
        this.updateTemplateBroadcast();
      }
    } catch (error) {

    }
  }

  public deleteCampaign(campaign) {
    EmitterService.get('DELETE_CAMPAIGN').emit(campaign);
  }

  public _addCondition2(condition) {
    let that = this;
    let arr = [];
    if (condition) {
      arr = JSON.parse(condition);
    }
    this.conditionsSaved = arr;
    this.getTotalCustomer(arr);
  }

  public getSearchTypeFilterV2(appId) {
    this.setLoading();
    this.isLoadDataFilter = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        this.searchTypeDataFilter2 = result;
        this.isLoadDataFilter = false;
        this._addCondition2(this.campaign.customer_target);
        this.setUnLoading();
      },
      (error) => {
        this.setUnLoading();
        this.isLoadDataFilter = false;
        this.log.info(error);
      });
  }

  public filterv2(data) {
    this.queries = data.query_filter;
    this.getTotalCustomer(this.queries);
    let body = { id: this.campaign.id, customer_target: JSON.stringify(this.queries) };
    this.campaign.customer_target = JSON.stringify(this.queries);
    this.updateCampaign(body);
  }
}
