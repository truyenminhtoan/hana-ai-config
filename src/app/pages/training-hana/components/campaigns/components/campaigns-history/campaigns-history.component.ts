import { Component, Input, OnInit, OnChanges, AfterContentInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { EmitterService } from '../../../../../../_services/emitter.service';
declare var $: any;
@Component({
  selector: 'campaigns-history',
  templateUrl: 'campaigns-history.component.html',
  styleUrls: ['campaigns-history.component.scss']
})
export class CampaignsHistoryComponent implements  OnChanges, AfterContentInit {
  @Input() campaignHistoryId: string;
  @Input() viewSelectedId: string;
  @Input() data;
  private datasource: any = [];
  private campaign: any = [];
  private broadcasts: any = [];

  // MdPaginator Inputs
  length = 100;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];

  // MdPaginator Output
  pageEvent: PageEvent;

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  changeEvent(event) {
    let pageIndex = event.pageIndex;
    this.loadCarsLazy(pageIndex);
  }

  constructor() { }

  public ngAfterContentInit(): void {
    $('[rel="tooltip"]').tooltip();
  }

  public ngOnChanges(changes: any) {
    let temp = [];
    // EmitterService.get(this.campaignHistoryId).subscribe((data: any) => {
      this.datasource = this.data.broadcast;
      if (this.datasource) {
        this.broadcasts = this.datasource.slice(0, this.pageSize);
        this.length = this.datasource.length;
      }
      this.campaign = this.data.campaign;
    // });
  }

  public loadCarsLazy(index) {
    if (this.datasource) {
      // this.cars = this.datasource.slice(event.first, (event.first + event.rows));
      this.broadcasts = this.datasource.slice(index * this.pageSize, index * this.pageSize + this.pageSize);
    }
  }

  public changeView(campaignId) {
    EmitterService.get(this.viewSelectedId).emit({ campaign_id: campaignId });
  }

}
