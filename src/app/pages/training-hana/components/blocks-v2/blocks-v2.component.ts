import { Log } from './../../../../_services/log.service';
import { NotificationService } from './../../../../_services/notification.service';
import { Component, OnInit, ElementRef, AfterContentInit, ViewChildren, OnDestroy } from '@angular/core';
import { EmitterService } from '../../../../_services/emitter.service';
import { Observable } from 'rxjs/Rx';
import { BlocksService } from '../../../../_services/blocks.service';
import { StringFormat } from '../../../../themes/validators/string.format';
import { BlockGroup, Block } from '../../../../_models/blocks.model';
declare var $: any;
declare var swal: any;
import _ from 'underscore';
import { IntentService } from '../../../../_services/intents.service';
@Component({
  selector: 'blocks-v2',
  templateUrl: 'blocks-v2.component.html',
  styleUrls: ['blocks-v2.component.scss']
})
export class BlocksV2Component implements OnInit, OnDestroy {
  @ViewChildren('input') private inputs;
  private changeBlockId: string = 'BLOCK_CHANGE_ID';
  private outputBlockId: string = 'BLOCK_OUTPUT_ID';
  private blockCurrent: Block;
  private groupBlocks: BlockGroup[] = [];
  private partnerId;

  constructor(private notificationService: NotificationService, private log: Log, private blocksService: BlocksService, private elRef: ElementRef, private intentService: IntentService) {
    // hidden overflow of body
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  public ngOnInit(): void {
    this.notificationService.clearConsole();
    // scroll left
    let h = $(window).height();
    $('.left-scroll').slimScroll({
      height: h - 60 + 'px'
    });
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser.app_using && currentUser.app_using.id) {
        this.partnerId = currentUser.app_using.id;
        this.getListGroupBlocks(this.partnerId);
      }
    }
    EmitterService.get(this.outputBlockId).subscribe((data: any) => {
      // alert(data);
      // delete data.type;
      // this.log.info('remove: ' + data);
      // alert('ada')
      // this.formatBlock(data.content, (cb) => {
      //   if (cb) {
      //     data.content_validate = cb;
      //   }
      //   this.updateNewBlock(data);
      // });
    });
    EmitterService.get('RELOAD_EDIT').subscribe((data: any) => {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.partnerId = currentUser.partner_id;
        this.getListGroupBlocks(this.partnerId);
      }
    });

    EmitterService.get('DELETE_BLOCK_ITEM').subscribe((data: any) => {
      this.deleteNewBlock(data.blockId);
    });
  }

  public ngOnDestroy(): void {
    $('body').removeAttr('style');
  }

  public blockSelected(item, type) {
    this.getDetailBlock(item.id, type);
  }

  public groupNameChange(group) {
    let body = { id: group.id, name: group.name };
    this.updateNewGroup(body);
  }

  public addBlock(groupIndex) {
    try {
      let name = StringFormat.generateName('BLOCK', this.groupBlocks[groupIndex].blocks, 'name');
      let block: Block = new Block();
      block.name = name;
      block.block_group_id = this.groupBlocks[groupIndex].id;
      block.content = '[]';
      this.addNewBlock(block, groupIndex);
    } catch (error) {
      this.log.info(error);
    }

  }

  public addGroup() {
    try {
      let name = StringFormat.generateName('NHÓM', this.groupBlocks, 'name');
      let blockGroup: BlockGroup = new BlockGroup();
      blockGroup.name = name;
      blockGroup.partner_id = this.partnerId;
      blockGroup.blocks = [];
      // this.groupBlocks.push(blockGroup);
      this.addGroupBlock(blockGroup);
    } catch (error) {
      this.log.info(error);
    }
  }

  public getListGroupBlocks(partnerId) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.getListGroupBlocks(partnerId);
    actionOperate.subscribe(
      (result) => {
        this.groupBlocks = result;
      },
      (err) => {
        this.log.info(err);
      });
  }

  public getDetailBlock(blockId, type) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.getDetailListBlocks(blockId);
    actionOperate.subscribe(
      (result) => {
        result.type = type;
        this.blockCurrent = result;
        // ham nay dung khi input undefined
        // alert(JSON.stringify(this.blockCurrent));
        //localStorage.setItem('blockCurrent', this.blockCurrent);
        EmitterService.get('SELECT_BLOCK_GALLERY').emit(this.blockCurrent);
        EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
      },
      (err) => {
        this.log.info(err);
      });
  }

  public addGroupBlock(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.addNewGroup(body);
    actionOperate.subscribe(
      (result) => {
        this.groupBlocks.push(result);
        // let a = this.inputs.toArray().find((e) => {
        //   return e.nativeElement.getAttribute('name') === 'group_2';
        // }).nativeElement;
        // alert(a);
      },
      (err) => {
        this.log.info(err);
      });
  }

  public updateNewGroup(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.updateNewGroup(body);
    actionOperate.subscribe(
      (result) => {
        this.log.info('cap nhat thanh cong');
      },
      (err) => {
        this.log.info(err);
      });
  }

  public addNewBlock(body, groupIndex) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.addNewBlock(body);
    actionOperate.subscribe(
      (result) => {
        if (!this.groupBlocks[groupIndex].blocks) {
          this.groupBlocks[groupIndex].blocks = [];
        }
        this.groupBlocks[groupIndex].blocks.push(result);
        this.log.info('Them block thanh cong');
      },
      (err) => {
        this.log.info(err);
      });
  }

  public updateBlock(data) {
    this.formatBlock(data.content, (cb) => {
      if (cb) {
        data.content_validate = cb;
      }
      this.updateNewBlock(data);
    });
  }

  public updateNewBlock(body) {
    delete body.type;
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.updateNewBlock(body);
    actionOperate.subscribe(
      (result) => {
        let blockNew = result[0];
        if (blockNew) {
          let i = 0;
          let j = 0;
          this.groupBlocks.filter((item) => {
            if (blockNew.block_group_id === item.id) {
              // tim block theo id de cap nhat
              item.blocks.filter((it) => {
                if (it.id === blockNew.id) {
                  // it = blockNew;
                  // it.name = blockNew.name;
                  // this.log.info('Cap nhat block thanh cong');
                  this.groupBlocks[i].blocks[j] = blockNew;
                }
                j++;
              });
            }
            i++;
          });
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

  public deleteGroupBlock(groupId, index) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      let actionOperate: Observable<any>;
      actionOperate = this.blocksService.deleteNewGroup(groupId);
      actionOperate.subscribe(
        (result) => {
          EmitterService.get('SELECT_BLOCK').emit(null);
          this.groupBlocks.splice(index, 1);
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        },
        (err) => {
          this.log.info(err);
        });
    }, (dismiss) => {
      this.log.info(dismiss);
    });
  }

  public deleteNewBlock(blockId) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      let actionOperate: Observable<any>;
      actionOperate = this.blocksService.deleteNewBlock(blockId);
      actionOperate.subscribe(
        (result) => {
          // this.groupBlocks[groupIndex].blocks.splice(blockIndex, 1);
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          }).then(() => {
            this.groupBlocks.filter((item) => {
              item.blocks = item.blocks.filter((it) => it.id !== blockId);
            });
            EmitterService.get('SELECT_BLOCK').emit(null);
            // window.location.reload();
          }, (dismiss) => {
            this.groupBlocks.filter((item) => {
              item.blocks = item.blocks.filter((it) => it.id !== blockId);
            });
            EmitterService.get('SELECT_BLOCK').emit(null);
          });
        },
        (err) => {
          this.log.info(err);
        });
    }, (dismiss) => {
      this.log.info(dismiss);
    });
  }

  public handleKeyDown(i, event) {
    $(event).attr('size', $(event).val().length);
    // alert(data);
    // let data = event.target.textContent;
    // this.log.info('data:', data);
    // if (data.length > 10) {
    //   this.groupBlocks[i].name = data.substr(0, 10);
    // } else {
    //   this.groupBlocks[i].name = data;
    // }
    // this.log.info('grop:', this.groupBlocks[i].name);
    // if (event.keyCode === 13) {
    //   event.preventdefault();
    // }
    // else if (event.keyCode === 40) {
    //   // action
    // }
    // else if (event.keyCode === 38) {
    //   // action
    // }
  }

  public formatBlock(content, callback) {
    if (!content) {
      callback(null);
      return;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getValidateFacebookJSON({ messages: content });
    actionOperate.subscribe(
      (json) => {
        if (json.code === 200) {
          callback(json.data);
        } else {
          callback(null);
        }
      },
      (err) => {
        callback(null);
        this.log.info(err);
      });
  }
}
