import { Log } from './../../../../_services/log.service';
import { AgentService } from './../../../../_services/agents.service';
import { Component, AfterViewInit, ViewEncapsulation, OnInit, ViewChildren, ViewChild, ElementRef, Inject, OnDestroy, HostListener } from '@angular/core';
import { IntentService } from '../../_services/intents.service';
import { Observable } from 'rxjs/Rx';
import { IntentModel, UserSayModel } from '../../_models/intents.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { MD_DIALOG_DATA, MdDialog } from '@angular/material';
declare var $: any;
declare var swal: any;
declare var Taggle: any; // https://sean.is/poppin/tags/
import * as _ from 'underscore';
import { ComfirmDeleteDialogComponent } from './components/comfirm-delete-dialog/comfirm-delete-dialog.component';
import { ConfigService } from '../../_services/config.service';
import { TrainingDontUnderstandDialogComponent } from './components/training-dont-understand-dialog/training-dont-understand-dialog.component';
import { NotificationService } from '../../../../_services/notification.service';
import { ApplicationService } from '../../../../_services/application.service';

@Component({
  selector: 'training-faq',
  templateUrl: 'training-faq.component.html',
  styleUrls: ['training-faq.component.scss', '../../../../../assets/plugins/taggle/taggle.min.css']
})

export class TrainingFaqComponent implements OnInit, AfterViewInit, OnDestroy {
  private currentUser;
  private contextSelected = 0;
  private agentId;
  private versionApp;
  private isOn = true;

  constructor(
    private elRef: ElementRef,
    private notificationService: NotificationService,
    private appService: ApplicationService,
    private log: Log,
    private agentService: AgentService) {
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  public ngOnInit(): void {
    // this.checkToken();
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser.app_using) {
        this.versionApp = this.currentUser.version;
      }
    }
    this.loadAgentAcive((agent) => {
      this.agentId = agent.id;
      this.isOn = agent.is_understand_no_unicode;
    });
/* 
    let h = $(window).height();
    $('.faq-scroll').slimScroll({
      height: h - 60 + 'px'
    }); */
  }

  public ngAfterViewInit(): void {
  }

  public ngOnDestroy(): void {
    $('body').removeAttr('style');
  }

  public loadAgentAcive(callback) {
    if (this.currentUser.app_using && this.currentUser.app_using.id) {
      this.appService.getAgentActiveByPartnerId(this.currentUser.app_using.id)
        .then((agent) => {
          this.getAgentInfo(agent.agent_id, (cb) => {
            callback(cb);
          });
        });
    } else {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
  }

  public changeIsOn(event) {
    this.isOn = event;
    let agent = { id: this.agentId, is_understand_no_unicode: this.isOn };
    this.updateAgent(agent);
  }

  public updateAgent(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.agentService.update(body);
    actionOperate.subscribe(
      (agent) => {
        // thanh cong
      },
      (err) => {
        this.log.info(err);
      });
  }

  public getAgentInfo(agentId, callback) {
    let actionOperate: Observable<any>;
    let body = { agent_id: agentId, version: this.versionApp };
    actionOperate = this.agentService.getAgentInfo(body);
    actionOperate.subscribe(
      (agent) => {
        // thanh cong
        callback(agent);
      },
      (err) => {
        callback(null);
        this.log.info(err);
      });
  }

}
