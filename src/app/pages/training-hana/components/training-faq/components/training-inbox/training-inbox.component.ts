import { Log } from './../../../../../../_services/log.service';
import { LazyLoadEvent } from './../../../../../../../assets/plugins/primeng/components/common/lazyloadevent.d';
import { StringFormat } from './../../../../../../themes/validators/string.format';
import { Component, AfterViewInit, ViewEncapsulation, OnInit, ViewChildren, ViewChild, ElementRef, Inject, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { IntentModel, UserSayModel } from '../../../../_models/intents.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { MD_DIALOG_DATA, MdDialog, PageEvent } from '@angular/material';
declare var $: any;
declare var swal: any;
declare var Taggle: any; // https://sean.is/poppin/tags/
import * as _ from 'underscore';
import { ComfirmDeleteDialogComponent } from '../comfirm-delete-dialog/comfirm-delete-dialog.component';
import { ApplicationService } from '../../../../../../_services/application.service';
import { ConfigService } from '../../../../../../_services/config.service';
import { NotificationService } from '../../../../../../_services/notification.service';
import { IntentService } from '../../../../../../_services/intents.service';
import { BlocksService } from '../../../../../../_services/blocks.service';

@Component({
  selector: 'training-inbox',
  templateUrl: 'training-inbox.component.html',
  styleUrls: ['training-inbox.component.scss']
})
export class TrainingInboxComponent implements OnInit, AfterViewInit {
  public intents: any = [];
  public intentCurrents: any = [];
  public blocks: any = [];
  public intentFallback: any;
  @Input() contextSelected: any;
  @Input() agentId;
  public partnerId;
  public currentUser;
  public appId;
  public loadingProcess = false;
  private color = 'primary';
  private mode = 'indeterminate';
  private value = 50;
  private isSaving = false;
  @ViewChild('inputSearch') private inputSearch: ElementRef;

  // MdPaginator Inputs
  private length = 0;
  private pageSize = 5;
  private pageIndex = 0;

  private _event: LazyLoadEvent;
  public paginate(event: LazyLoadEvent) {
    this.pageIndex = parseInt(event.first + '', 10);
    this.pageSize = parseInt(event.rows + '', 10);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    this.length = this.intents.length;
  }

  public paginateDel() {
    // this.loadCarsLazy2(pageIndex, event.rows);
    this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
    if (this.intentCurrents.length === 0) {
      this.pageIndex = this.pageIndex - this.pageSize;
      this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
      this.length = this.intents.length;
    }
  }

  // public loadCarsLazy2(index, pageSize) {
  //   if (this.intents) {
  //     this.pageSize = pageSize;
  //     this.pageIndex = index;
  //     this.intentCurrents = this.intents.slice(event.first, (event.first + event.rows));
  //   }
  // }

  public showSwal(type) {
    if (type === 'basic') {
      swal({
        title: "Here's a message!",
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      });
    } else if (type === 'warning-message-and-cancel') {
      swal({
        title: 'Bạn chắc chắn muốn xóa FAQ này?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Có',
        cancelButtonText: 'Không',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then(() => {
        swal({
          title: 'Xóa thành công!',
          text: '',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false
        });
      }, (dismiss) => {
      });
    }
  }

  public ngOnInit(): void {
    this.checkCurrentAppToken();
    this.contextSelected = 0;
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser.app_using && this.currentUser.app_using.id) {
        this.partnerId = this.currentUser.app_using.id;
      }
    }
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (!this.agentId) {
      this.loadAgentAcive((agent) => {
        this.agentId = agent.agent_id;
        this.getListBlock((cb) => {
          this.getListIntent(0, null);
        });
      });
    } else {
      this.getListBlock((cb) => {
        this.getListIntent(0, null);
      });
    }
    if (this.inputSearch) {
      let eventInputObservable = Observable.fromEvent(this.inputSearch.nativeElement, 'keyup');
      eventInputObservable.subscribe();
    }

  }
  public ngAfterViewInit(): void {
    // this.loadPopup();

    // if (this.currentUser) {
    //   $('.version-' + this.currentUser.version).show();
    //   $('.role-' + this.currentUser.role_id).show();
    //   $('.navbar-daotao').css('border-bottom', '2px solid #e9604a');
    //   this.log.info(JSON.stringify(this.currentUser));
    // }
  }

  // docaohuynh
  public loadPopup() {
    this.appService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        this.appId = integrate.applications[0].app_id;
        let r = document.createElement('script');
        r.type = 'text/javascript';
        r.async = !0;
        r.src = this.configService.hostResource + '/hana/' + this.appId;
        let c = document.getElementsByTagName('script')[0];
        c.parentNode.insertBefore(r, c);
      });
  }
  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:member-ordering
  constructor(
    private dialog: MdDialog,
    private log: Log,
    private appService: ApplicationService,
    private configService: ConfigService,
    private notificationService: NotificationService, private intentService: IntentService, private route: ActivatedRoute, private router: Router,
    private blocksService: BlocksService) {
  }

  public loadAgentAcive(callback) {
    this.appService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        callback(agent);
      });
  }

  public getListIntent(index, contextName) {
    if (this.loadingProcess) {
      return;
    }
    this.intents = [];
    this.intentFallback = undefined;
    this.loadingProcess = true;
    this.contextSelected = index;
    let body = { agent_id: this.agentId + '', context_name: '', faq_type: 1};
    if (contextName) {
      body.context_name = contextName;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getListFAQ(body);
    actionOperate.subscribe(
      (intents) => {
        if (!intents) {
          this.notificationService.showDanger('Tải dữ liệu thất bại, vui lòng tải lại...');
          return;
        }
        // alert(JSON.stringify(intents));
        this.loadingProcess = false;
        intents.filter((item) => {
          if (item.type === 2) {
            this.intentFallback = item;
          }
        });
        if (!this.intentFallback) {
          let intentNew = { id: UUID.UUID(), is_new: true, name: 'default_inbox_fallback', agent_id: this.agentId, type: 2, usersays: [], faq: 1, response_messages: [] };
          intentNew.response_messages = [{ content: JSON.stringify([{ type: 1, message: { posibleTexts: [] } }]) }];
          this.intentFallback = intentNew;
        }
        this.intents = _.reject(intents, (item) => { return item.type === 2 });

        if (this.intents) {
          this.intentCurrents = this.intents.slice(0, this.pageSize);
          this.length = this.intents.length;
          // console.log('intentCurrents:' + JSON.stringify(this.intentCurrents));
        }
      },
      (err) => {
        this.notificationService.showDanger('Tải dữ liệu thất bại, vui lòng tải lại...');
        this.loadingProcess = false;
        this.log.info(err);
      });
  }

  public getListBlock(callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.blocksService.getListGroupBlocks(this.partnerId);
    actionOperate.subscribe(
      (blocks) => {
        if (!blocks) {
          this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
          callback(false);
          return;
        }
        let block = [];
        blocks.filter((item) => {
          if (item.blocks) {
            block = _.union(block, item.blocks);
          }
        });
        let temp = [];
        block.filter((item) => {
          temp.push({ value: item.id, display: item.name });
        });
        this.blocks = temp;
        callback(true);
      },
      (err) => {
        callback(false);
        this.log.info(err);
        this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
      });
  }

  public addIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(body);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Cập nhật thất bại');
          callback(false);
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          callback(false);
        } else {
          this.notificationService.showSuccess('Thêm thành công');
          callback(intent);
        }
      },
      (err) => {
        callback(false);
        this.log.info(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }

  public deleteUsersay(Id, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateStatusUsersay({ id: Id, status: 'ignoreTrain' });
    actionOperate.subscribe(
      (intent) => {
        callback(true);
      },
      (err) => {
        this.log.info(err);
        callback(false);
      });
  }

  public updateIntent(body, callback) {
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.updateFAQ(body);
    actionOperate.subscribe(
      (intent) => {
        this.loadingProcess = false;
        if (!intent) {
          this.notificationService.showDanger('Cập nhật thất bại');
          callback(false);
          return;
        }
        // alert(JSON.stringify(intents));
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('Ý định đã tồn tại');
          callback(false);
          return;
        }
        this.notificationService.showSuccess('Cập nhật thành công');
        callback(intent);
      },
      (err) => {
        callback(false);
        this.log.info(err);
        this.notificationService.showDanger('Cập nhật thất bại');
        this.loadingProcess = false;
      });
  }

  public deleteIntent(index) {
    swal({
      title: 'Bạn chắc chắn muốn xóa FAQ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      if (this.contextSelected === 2) {
        let intent = this.intents[index];
        this.deleteUsersay(intent.mogo_id, (cb) => {
          this.intents.splice(index, 1);
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        });
      } else {
        // delete this.intents[index];
        let intent = this.intentCurrents[index];
        if (intent.is_new) {
          this.intents = this.intents.filter((it) => {
            return it.id !== intent.id;
          });
          // this.length = this.intents.length;
          // this.intentCurrents.splice(index, 1);

          // if (this.intents) {
          //   this.intentCurrents = this.intents.slice(this.pageIndex, this.pageSize);
          //   this.length = this.intents.length;

          //   if (this.intentCurrents.length === 0 && this.pageIndex > 0) {
          //     this.pageIndex = this.pageIndex - this.pageSize;
          //     this.intentCurrents = this.intents.slice(this.pageIndex, this.pageSize);
          //     this.length = this.intents.length;
          //   }
          // }

          this.paginateDel();

          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        } else {
          let actionOperate: Observable<any>;
          actionOperate = this.intentService.delete(intent.id);
          actionOperate.subscribe(
            (intents) => {
              // alert(JSON.stringify(intents));
              // this.intents = intents;
              let itent = this.intentCurrents[index];
              this.intents = this.intents.filter((it) => {
                return it.id !== itent.id;
              });
              // this.length = this.intents.length;
              // if (this.intents) {
              //   this.intentCurrents = this.intents.slice(this.pageIndex, this.pageSize);
              //   this.length = this.intents.length;
              //   if (this.intentCurrents.length === 0 && this.pageIndex > 0) {
              //     this.pageIndex--;
              //     this.intentCurrents = this.intents.slice(this.pageIndex, this.pageSize);
              //     this.length = this.intents.length;
              //   }
              // }
              this.paginateDel();
              swal({
                title: 'Xóa thành công!',
                text: '',
                type: 'success',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
              });
            },
            (err) => {
              this.log.info(err);
            });
        }
      }
    }, (dismiss) => {
    })
  }

  public updateFallback(intent) {
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (this.loadingProcess) {
      return;
    }
    this.loadingProcess = true;
    if (intent.is_new) {
      delete intent.is_new;
      this.addIntent(intent, (cb) => {
        if (cb) {
          this.intentFallback.id = cb.id;
        }
        this.loadingProcess = false;
      });
    } else {
      this.updateIntent(intent, (cb) => {
        if (cb) {
          this.intentFallback.id = cb[0].id;
        }
        this.loadingProcess = false;
      });
    }
  }

  public update(intent) {
    if (!this.partnerId) {
      this.notificationService.showDanger('Vui lòng chọn ứng dụng đào tạo...');
      return;
    }
    if (this.loadingProcess) {
      return;
    }
    let intentNew = this.intents[intent.index];
    if (_.isEmpty(intent.usersays)) {
      this.notificationService.showDanger('Vui lòng nhập câu nói khách hàng...');
      return;
    }
    // alert(JSON.stringify(intent));
    if (!intent.response && !intent.response_message_blocks) {
      let checkResponse = JSON.parse(intent.response)[0].message.posibleTexts;
      if (_.isEmpty(checkResponse)) {
        this.notificationService.showDanger('Vui lòng nhập thông tin bot trả lời...');
        return;
      }
    }

    intentNew.name = intent.usersays[0].content;
    intentNew.usersays = intent.usersays;
    intentNew.type_answer = intent.type_answer;
    intentNew.agent_id = this.agentId;
    if (intent.response) {
      intentNew.response_messages = [{ content: intent.response }];
    }
    intentNew.response_message_blocks = intent.response_message_blocks;
    if (this.contextSelected === 0 || this.contextSelected === 2) {
      intentNew.name = intentNew.name + '_inbox';
    }
    this.loadingProcess = true;
    if (intentNew.is_new) {
      delete intentNew.is_new;
      this.addIntent(intentNew, (cb) => {
        if (cb) {
          this.intents[intent.index].id = cb.id;
          delete this.intents[intent.index].is_dont_understand;
          delete this.intents[intent.index].mogo_id;
          delete this.intents[intent.index].is_new;
        }
        this.loadingProcess = false;
      });
    } else {
      delete intentNew.is_new;
      this.updateIntent(intentNew, (cb) => {
        if (cb) {
          this.intents[intent.index].id = cb[0].id;
        }
      });
    }
  }

  public addNewItem() {
    this.isSaving = true;
    // let intentNew = { id: UUID.UUID(), is_new: true, usersays: [], faq: 1, response_messages: [] };
    // this.intents.unshift(intentNew);
    // faq_type: 1: inbox, 2: comment
    let intentNew = { id: UUID.UUID(), name: UUID.UUID(), faq: 1, faq_type: 1, agent_id: this.agentId };

    let actionOperate: Observable<any>;
    actionOperate = this.intentService.add(intentNew);
    actionOperate.subscribe(
      (intent) => {
        if (!intent) {
          this.notificationService.showDanger('Thêm thất bại');
          // callback(false);
          this.isSaving = false;
          return;
        }
        // this.intents = intents;
        if (intent === 202) {
          this.notificationService.showWarning('FAQ đã tồn tại');
          this.isSaving = false;
          // callback(false);
        } else {
          // this.notificationService.showSuccess('Thêm thành công');
          // callback(intent);
          this.intents.unshift(intent);
          this.isSaving = false;

          if (this.intents) {
            this.intentCurrents = this.intents.slice(0, this.pageSize);
            this.configService.log('IntentCurrentABCDEF ' + JSON.stringify(this.intentCurrents));
            this.length = this.intents.length;
            this.pageIndex = 0;
          }
        }
      },
      (err) => {
        // callback(false);
        this.isSaving = false;
        this.log.info(err);
        this.notificationService.showDanger('Thêm thất bại');
      });
  }
  public transform(searchTerm: string) {
    searchTerm = StringFormat.formatText(searchTerm);
    if (searchTerm != null && searchTerm.length > 0) {
      let intentTemp = this.intents.filter((item) => {
        let temp = [];
        if (item.usersays) {
          temp = item.usersays.filter((says) => {
            return StringFormat.formatText(says.content).indexOf(searchTerm) !== -1
          });
        }

        let response = '';
        if (item.response_messages && item.response_messages[0]) {
          response = item.response_messages[0].content;
        }
        return (temp.length > 0 || StringFormat.formatText(response).indexOf(searchTerm) !== -1);
      });

      if (intentTemp) {
        this.intentCurrents = intentTemp.slice(0, this.pageSize);
        this.length = intentTemp.length;
        this.pageIndex = 0;
      }
    } else {
      if (this.intents) {
        this.intentCurrents = this.intents.slice(0, this.pageSize);
        this.length = this.intents.length;
        this.pageIndex = 0;
      }
    }
  }

  public checkCurrentAppToken() {
    if ($('#token') && $('#token').val()) {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if ($('#token').val().indexOf(currentUser.app_using.id) === -1) {
          location.reload();
        }
      }
    }
  }
}
