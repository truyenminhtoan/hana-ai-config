import { NotificationService } from './../../../../_services/notification.service';
import { Component } from '@angular/core';

@Component({
    selector: 'config-drap-bot',
    templateUrl: 'config-drap-bot.component.html',
    styleUrls: ['config-drap-bot.component.scss']
})
export class ConfigDrapBotComponent {
    private hourAvailable = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    private beginHour;
    private endHour;

    private availableCars = [];

    private selectedCars = [];

    private draggedCar;

    constructor(private notificationService: NotificationService) { }

    public addTo() {
        if (!this.beginHour || !this.endHour) {
            return this.notificationService.showDanger('Vui lòng chọn đầy đủ thời gian bắt đầu và thời gian kết thúc');
        }
        this.availableCars.push('từ ' + this.beginHour + ' đến ' + this.endHour);
        this.beginHour = null;
        this.endHour = null;
    }

    public ngOnInit() {
        this.selectedCars = [[], [], [], [], [], [], []];
    }

    public dragStart(event, car) {
        this.draggedCar = car;
    }

    public drop(event, i) {
        if (this.selectedCars[i].length >= 5) {
            this.notificationService.showDanger('Tối đa 5 mốc thời gian');
            this.draggedCar = null;
            return;
        }
        if (this.draggedCar) {
            let draggedCarIndex = this.findIndex(this.draggedCar);
            this.selectedCars[i] = [...this.selectedCars[i], this.draggedCar];
            // this.availableCars = this.availableCars.filter((val, i) => i != draggedCarIndex);
            this.draggedCar = null;
        }
    }

    public dropAt(i, j) {
        this.selectedCars[i].splice(j, 1);
    }

    public dragEnd(event) {
        this.draggedCar = null;
    }

    public findIndex(car) {
        let index = -1;
        for (let j = 0; j < this.availableCars.length; j++) {
            if (car === this.availableCars[j]) {
                index = j;
                break;
            }
        }
        return index;
    }
}
