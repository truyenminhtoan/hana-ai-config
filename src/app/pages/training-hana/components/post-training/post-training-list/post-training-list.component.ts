import { NotificationService } from './../../../../../_services/notification.service';
import { Observable } from 'rxjs/Rx';
import { EmitterService } from './../../../../../_services/emitter.service';
import { PostFilterDialogComponent } from './../components/post-filter-dialog/post-filter-dialog.component';
import { StringFormat } from './../../../../../themes/validators/string.format';
import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef, Directive, HostListener
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookPostService } from '../../../../../_services/fbpost.service';
import { ConfigService } from '../../../../../_services/config.service';
import { ElementRef, NgZone } from '@angular/core';
import { posibleTexts } from '../../../../../_models/messages.model';
import { DateAdapter, MdDialog } from '@angular/material';
import { PostHastagDialogComponent } from '../components/post-hastag-dialog/post-hastag-dialog.component';
import { CommentsService } from '../../../../../_services/index';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';

declare var $: any;
declare var _: any;


@Component({
    selector: 'post-training-list',
    templateUrl: 'post-training-list.component.html',
    styleUrls: ['post-training-list.component.scss']
})
export class PostTrainingListComponent implements OnInit {

    @Input() lstPosts: any;
    @Input() appId: string;
    @Input() partnerId: string;
    @Input() loadingProcess = false;


    @Output() postChange = new EventEmitter<any>();


    private loading = true;
    private textSearch: any;
    private picker: any;
    private resultPicker: any;
    private resultPickerTo: any;
    private postSelected: any;
    private listHastag: any;
    private isDestroy: boolean;

    // toantm
    private fromDate: any;
    private toDate: any;
    private tags = [];

    constructor(
        private ngZone: NgZone,
        private dialog: MdDialog,
        private elementRef: ElementRef,
        private facebookPostService: FacebookPostService,
        private router: Router,
        private route: ActivatedRoute,
        private serviceConfig: ConfigService,
        private commentService: CommentsService,
        private notificationService: NotificationService,
        private cdRef: ChangeDetectorRef) {
        this.lstPosts = [];

    }
    public ngAfterViewInit() {
        console.log('');
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
    public ngOnInit() {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser.app_using && currentUser.app_using.id) {
                this.partnerId = currentUser.app_using.id;
                this.appId = currentUser.app_using.applications[0].app_id;
            }
        }
        this.getListHashtag({});
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes['appId'] && this.appId) {
            console.log('CHANEAPPID');
            let body = {
                app_id: this.appId,
                page_size: 20,
                last_item: 0
            };
            if (this.listHastag) {
                this.getListPost(body, 'ngOnChanges');
            } else {
                this.getListHashtagCallBack((cb) => {
                    this.getListPost(body, 'ngOnChanges');
                });
            }

        }
    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }
    public getHashTagFallback(callback) {
        let actionOperate: Observable<any>;
        actionOperate = this.commentService.getHashTagFallback({ partner_id: this.partnerId });
        actionOperate.subscribe(
            (result) => {
                if (result) {
                    callback(result);
                } else {
                    callback(false);
                }
            },
            (err) => {
                callback(false);
                console.log(err);
            });
    }
    public getListHashtag(body: any) {
        body.partner_id = this.partnerId;
        this.facebookPostService.findAllHashtagPost(JSON.stringify(body))
            .then((posts) => {
                this.listHastag = posts;
                // this.getHashTagFallback((cb) => {
                //     if (cb) {
                //         this.listHastag = cb;
                //     }
                //     if (posts) {
                //         this.listHastag = this.listHastag.concat(posts);
                //     }
                // });
            });
    }
    public getListHashtagCallBack(callback) {
        this.facebookPostService.findAllHashtagPost(JSON.stringify({ partner_id: this.partnerId }))
            .then((posts) => {
                this.listHastag = posts;
                callback(this.listHastag);
                // this.getHashTagFallback((cb) => {
                //     if (cb) {
                //         this.listHastag = cb;
                //     }
                //     if (posts) {
                //         this.listHastag = this.listHastag.concat(posts);
                //     }
                //     callback(this.listHastag);
                // });
            });
    }
    public getListPost(body: any, from: string) {
        console.log('GETLISTPOSTEEEEEEEEEEEEEEEEEEEEEEE ' + from);
        if (this.textSearch && this.textSearch !== '') {
            body.key = this.textSearch;
        }
        this.loading = true;
        this.facebookPostService.findPost(JSON.stringify(body))
            .then((posts) => {
                if (posts) {
                    // console.log('ALL POST ' + JSON.stringify(posts));
                    _.each(posts.data, (post) => {
                        let postFind = _.find(this.lstPosts, (lstP) => {
                            return lstP.post_id === post.post_id;
                        });
                        if (!postFind) {
                            // console.log('pushss');
                            _.each(post.childrens, (child) => {
                                child.content_full = JSON.parse(child.content_full);
                                child.selected = false;
                                // if (child.content) {
                                //     child.tags = StringFormat.hashTagRegex(child.content);
                                //     if (child.tags) {
                                //         child.tags = child.tags.join(' ');
                                //     }
                                // }
                            });
                            post.content_full = JSON.parse(post.content_full);
                            post.selected = false;
                            // alert(JSON.stringify(post));
                            // if (post.content) {
                            //     post.tags = StringFormat.hashTagRegex(post.content);
                            //     if (post.tags) {
                            //         post.tags = post.tags.join(' ');
                            //     }
                            // }
                            this.setCollapse(post);
                        }
                    });
                    this.loading = false;
                    this.callChange();
                }
            });
    }
    public generateTag(post) {
        if (!this.listHastag) {
            this.getListHashtagCallBack((cb) => {
                if (post.content) {
                    let tags = [];
                    let hashTag = StringFormat.hashTagRegex(post.content);
                    if (!hashTag) {
                        hashTag = [];
                    }
                    hashTag.filter((item) => {
                        this.listHastag.filter((it) => {
                            if (!it.prefix) {
                                it.prefix = '';
                            }
                            let tag = '#' + it.name + it.prefix;
                            if (tag === item) {
                                tags.push(tag);
                            }
                        });
                    });
                    if (tags) {
                        post.tags = tags.join(' ');
                    }
                    // set con
                    _.each(post.childrens, (child) => {
                        if (child.content) {
                            let tagsChild = [];
                            let hashTagChild = StringFormat.hashTagRegex(child.content);
                            if (!hashTagChild) {
                                hashTagChild = [];
                            }
                            hashTagChild.filter((item) => {
                                this.listHastag.filter((it) => {
                                    if (!it.prefix) {
                                        it.prefix = '';
                                    }
                                    let tag = '#' + it.name + it.prefix;
                                    if (tag === item) {
                                        tagsChild.push(tag);
                                    }
                                });
                            });
                            if (tagsChild) {
                                child.tags = tagsChild.join(' ');
                            }
                        }
                    });
                }
                this.lstPosts.push(post);
                this.callChange();
            });
        } else {
            if (post.content) {
                let tags = [];
                let hashTag = StringFormat.hashTagRegex(post.content);
                if (!hashTag) {
                    hashTag = [];
                }
                hashTag.filter((item) => {
                    this.listHastag.filter((it) => {
                        if (!it.prefix) {
                            it.prefix = '';
                        }
                        let tag = '#' + it.name + it.prefix;
                        if (tag === item) {
                            tags.push(tag);
                        }
                    });
                });
                // post.tags = StringFormat.hashTagRegex(post.content);
                if (tags) {
                    post.tags = tags.join(' ');
                }
                _.each(post.childrens, (child) => {
                    if (child.content) {
                        let tagsChild = [];
                        let hashTagChild = StringFormat.hashTagRegex(child.content);
                        if (!hashTagChild) {
                            hashTagChild = [];
                        }
                        hashTagChild.filter((item) => {
                            this.listHastag.filter((it) => {
                                if (!it.prefix) {
                                    it.prefix = '';
                                }
                                let tag = '#' + it.name + it.prefix;
                                if (tag === item) {
                                    tagsChild.push(tag);
                                }
                            });
                        });
                        if (tagsChild) {
                            child.tags = tagsChild.join(' ');
                        }
                    }
                });
            }
            // this.lstPosts.push(post);
            this.callChange();
        }
    }
    public generateUpdateTag(post) {
        if (!this.listHastag) {
            this.getListHashtagCallBack((cb) => {
                if (post.content) {
                    let tags = [];
                    let hashTag = StringFormat.hashTagRegex(post.content);
                    if (!hashTag) {
                        hashTag = [];
                    }
                    hashTag.filter((item) => {
                        this.listHastag.filter((it) => {
                            if (!it.prefix) {
                                it.prefix = '';
                            }
                            let tag = '#' + it.name + it.prefix;
                            if (tag === item) {
                                tags.push(tag);
                            }
                        });
                    });
                    if (tags) {
                        post.tags = tags.join(' ');
                    }
                    // set con
                    _.each(post.childrens, (child) => {
                        if (child.content) {
                            let tagsChild = [];
                            let hashTagChild = StringFormat.hashTagRegex(child.content);
                            if (!hashTagChild) {
                                hashTagChild = [];
                            }
                            hashTagChild.filter((item) => {
                                this.listHastag.filter((it) => {
                                    if (!it.prefix) {
                                        it.prefix = '';
                                    }
                                    let tag = '#' + it.name + it.prefix;
                                    if (tag === item) {
                                        tagsChild.push(tag);
                                    }
                                });
                            });
                            if (tagsChild) {
                                child.tags = tagsChild.join(' ');
                            }
                        }
                    });
                }
                this.callChange();
            });
        } else {
            if (post.content) {
                let tags = [];
                let hashTag = StringFormat.hashTagRegex(post.content);
                if (!hashTag) {
                    hashTag = [];
                }
                hashTag.filter((item) => {
                    this.listHastag.filter((it) => {
                        if (!it.prefix) {
                            it.prefix = '';
                        }
                        let tag = '#' + it.name + it.prefix;
                        if (tag === item) {
                            tags.push(tag);
                        }
                    });
                });
                // post.tags = StringFormat.hashTagRegex(post.content);
                if (tags) {
                    post.tags = tags.join(' ');
                }
                _.each(post.childrens, (child) => {
                    if (child.content) {
                        let tagsChild = [];
                        let hashTagChild = StringFormat.hashTagRegex(child.content);
                        if (!hashTagChild) {
                            hashTagChild = [];
                        }
                        hashTagChild.filter((item) => {
                            this.listHastag.filter((it) => {
                                if (!it.prefix) {
                                    it.prefix = '';
                                }
                                let tag = '#' + it.name + it.prefix;
                                if (tag === item) {
                                    tagsChild.push(tag);
                                }
                            });
                        });
                        if (tagsChild) {
                            child.tags = tagsChild.join(' ');
                        }
                    }
                });
            }
            this.callChange();
        }
    }
    public postScroll(event) {
        console.log('SCROLLING');
        let $div = $(event.target);
        if ($div[0].scrollHeight - $div.scrollTop() === $div.outerHeight()) {
            console.log('SCROLL bottom ' + this.lstPosts.length);
            let body = {
                app_id: this.appId,
                page_size: 20,
                last_item: this.lstPosts.length
            };

            this.getListPost(body, 'postScroll');
        }
    }

    public setCollapse(post) {
        if (post.childrens.length === 0) {
            post.isCollapse = false;
        } else {
            post.isCollapse = true;
            post.is_expand = false;
        }
        console.log('COLLAPSE ' + post.isCollapse);
        this.lstPosts.push(post);
        let length = this.lstPosts.length - 1;
        this.generateTag(this.lstPosts[length]);
        this.callChange();
    }

    public togglePost(status: string, post: any) {
        if (status === 'show') {
            post.is_expand = false;
        } else {
            post.is_expand = true;
        }
        this.callChange();
    }

    public usingPost(post) {
        console.log('aaaaaaaaa:', this.loadingProcess);
        if (this.loadingProcess) {
            return;
        }
        _.each(this.lstPosts, (postE) => {
            _.each(postE.childrens, (child) => {
                child.selected = false;
            });
            postE.selected = false;
        });
        post.selected = true;
        this.callChange();
        this.postChange.emit(post);
        this.postSelected = post.content_full;
        console.log('bbbbbbbb:', this.loadingProcess);
        console.log('sssssssssssssssss ' + this.postSelected);
    }

    public searchByTag() {
        EmitterService.get('RESET_POST_SELECTED').emit('reset');
        // if (this.textSearch && this.textSearch.trim() === '') {

        let body: any = {
            app_id: this.appId,
            page_size: 20,
            last_item: 0
        };
        if (this.textSearch && this.textSearch.trim() === '') {
            body.key = this.textSearch;
        }
        if (this.fromDate) {
            body.from_date = this.fromDate;
        }
        if (this.toDate) {
            body.to_date = this.toDate;
        }
        this.lstPosts = [];
        this.getListPost(body, 'searchByTag');
    }

    public searchAll() {
        EmitterService.get('RESET_POST_SELECTED').emit('reset');
        // if (this.textSearch && this.textSearch.trim() === '') {
        let body: any = {
            app_id: this.appId,
            page_size: 20,
            last_item: 0
        };
        if (this.textSearch && this.textSearch.trim() === '') {
            body.key = this.textSearch;
        }
        if (this.fromDate) {
            body.from_date = this.fromDate;
        }
        if (this.toDate) {
            body.to_date = this.toDate;
        }
        this.lstPosts = [];
        this.getListPost(body, 'searchAll');
        // }
    }

    public openDialogHashTags(post, i, j) {
        // alert(JSON.stringify(post))
        if (!this.partnerId) {
            return this.notificationService.showDanger('Đã xảy ra lỗi. Vui lòng thực hiện lại sau');
        }
        if (!post.post_id) {
            return this.notificationService.showDanger('Đã xảy ra lỗi. Vui lòng thực hiện lại sau');
        }
        let dialogRef = this.dialog.open(PostHastagDialogComponent, {
            data: {
                listHastag: this.listHastag,
                app_id: this.appId,
                post_id: post.post_id,
                message: post.content
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            // console.log(`Dialog result: ${result}`);
            if (result) {
                if (j) {
                    this.lstPosts[i].childrens[j].content = result.message;
                    this.generateUpdateTag(this.lstPosts[i].childrens[j]);
                    // if (result.message) {
                    //     let tag = StringFormat.hashTagRegex(result.message);
                    //     this.lstPosts[i].childrens[j].tags = StringFormat.hashTagRegex(result.message);
                    //     if (tag) {
                    //         this.lstPosts[i].childrens[j].tags = tag.join(' ');
                    //     } else {
                    //         this.lstPosts[i].childrens[j].tags = '';
                    //     }
                    // }
                } else {
                    this.lstPosts[i].content = result.message;
                    this.generateUpdateTag(this.lstPosts[i]);
                    // let tag = StringFormat.hashTagRegex(result.message);
                    // if (tag) {
                    //     this.lstPosts[i].tags = tag.join(' ');
                    // } else {
                    //     this.lstPosts[i].tags = '';
                    // }
                }
            }
        });
    }

    // filter theo ngay
    public openFilterDate() {
        let dialogRef = this.dialog.open(PostFilterDialogComponent, {
            // height: '310px',
            width: '30%',
            data: {
                // customers: this.selectedCustomers,
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            // console.log(`Dialog result: ${result}`);
            if (result) {
                this.fromDate = result.from_date;
                this.toDate = result.to_date;
                this.searchAll();
            }
        });
    }

    // lay ds faq theo bai post
    public addTag(post) {
        let postId = 0;
        let i = 0;
        let j = 0;
        this.loadingProcess = true;
        let body: any = { partner_id: this.partnerId, post_id: postId, tags: this.tags };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.facebookPostService.updatePostById(body);
        actionOperate.subscribe(
            (context) => {
                this.loadingProcess = false;
                this.callChange();
                if (context) {
                    if (j) {
                        this.lstPosts[i].childrens[j].message = context.data.message;
                    } else {
                        this.lstPosts[i].message = context.data.message;
                    }
                }
            },
            (err) => {
                this.loadingProcess = false;
                this.callChange();
                console.log(err);
            });
    }
}
