import { Log } from './../../../../../../_services/log.service';
import { StringFormat } from './../../../../../../themes/validators/string.format';
import { Observable } from 'rxjs/Rx';
import { FacebookPostService } from './../../../../../../_services/fbpost.service';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { NotificationService } from '../../../../../../_services/notification.service';
declare var $: any;
declare var swal: any;
import _ from 'underscore';
@Component({
    selector: 'post-hastag-dialog',
    templateUrl: 'post-hastag-dialog.component.html',
    styleUrls: ['post-hastag-dialog.component.scss']
})
export class PostHastagDialogComponent implements OnInit {
    private listHastag: any = [];
    private tags = [];
    private loadingProcess = false;
    private appId;
    private postId;

    constructor(
        @Inject(MD_DIALOG_DATA) public data: any,
        public dialogRef: MdDialogRef<PostHastagDialogComponent>,
        private facebookPostService: FacebookPostService,
        private log: Log,
        private notificationService: NotificationService) {
        if (data.listHastag) {
            data.listHastag.filter((item) => {
                if (!item.prefix) {
                    item.prefix = '';
                }
                this.listHastag.push('#' + item.name + item.prefix);
            });
            // this.listHastag = data.listHastag;
            this.appId = data.app_id;
            this.postId = data.post_id;
            // let tags = StringFormat.hashTagRegex(data.message);
            // if (tags) {
            //     this.tags = tags;
            // }


            let hashTag = StringFormat.hashTagRegex(data.message);
            if (!hashTag) {
                hashTag = [];
            }
            hashTag.filter((item) => {
                this.listHastag.filter((it) => {
                    // let tag = '#' + it.name + it.prefix;
                    if (it === item) {
                        this.tags.push(it);
                    }
                });
            });
            // post.tags = StringFormat.hashTagRegex(post.content);
            // if (tagsArr) {
            //     this.tags = tagsArr.join(' ');
            // }

        }
    }

    public ngOnInit() {
    }

    public addTag() {
        this.loadingProcess = true;
        let body: any = { app_id: this.appId, post_id: this.postId, tags: this.tags };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.facebookPostService.updatePostById(body);
        actionOperate.subscribe(
            (context) => {
                this.loadingProcess = false;
                if (context) {
                    let mess = context.data.message;
                    this.dialogRef.close({ message: mess });
                } else {
                    this.notificationService.showDanger('Gắn hashtag thất bại. Vui lòng thực hiện lại sau');
                }
            },
            (err) => {
                this.notificationService.showDanger('Gắn hashtag thất bại. Vui lòng thực hiện lại sau');
                this.loadingProcess = false;
                this.log.info(err);
            });
    }
}
