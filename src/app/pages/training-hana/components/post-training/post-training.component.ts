import { Log } from './../../../../_services/log.service';
import { EmitterService } from './../../../../_services/emitter.service';
import { MdDialog } from '@angular/material';
import { CommentsService } from './../../../../_services/comment.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { StringFormat } from './../../../../themes/validators/string.format';
import { ApplicationService } from './../../../../_services/application.service';
import { NotificationService } from './../../../../_services/notification.service';
import { IntentService } from './../../../../_services/intents.service';
import { Observable } from 'rxjs/Rx';
import { Component, NgZone, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FacebookPostService } from '../../../../_services/fbpost.service';
import { ConfigService } from '../../../../_services/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { UUID } from 'angular2-uuid';
declare var $: any;
declare var swal: any;
import _ from 'underscore';
import { OnDestroy } from 'angular2-color-picker/node_modules/@angular/core/src/metadata/lifecycle_hooks';
import { PostFilterDialogComponent } from './components/post-filter-dialog/post-filter-dialog.component';
@Component({
    selector: 'post-training',
    templateUrl: 'post-training.component.html',
    styleUrls: ['post-training.component.scss']
})
export class PostTrainingComponent implements OnInit, OnDestroy {
    private lstPosts: any;
    private postOpen: any;
    private currentUser: any;
    private partnerId: any;
    private application: any;
    private appId: any;

    private isexpire = false;

    // ################################################# TOAN TM ###################################
    private postSelected: any;
    private intents = [];
    private intentCurrents = [];
    private length = 0;
    private pageSize = 5;
    private pageIndex = 0;
    private loadingProcess = false;
    private agentId;
    private postContextId;
    private intentFallback: any;
    private isDestroy: boolean;
    private tags: any = [];
    // ################################################# END TM ###################################

    constructor(
        private ngZone: NgZone,
        private elementRef: ElementRef,
        private facebookPostService: FacebookPostService,
        private router: Router,
        private route: ActivatedRoute,
        private serviceConfig: ConfigService,
        private intentService: IntentService,
        private notificationService: NotificationService,
        private applicationService: ApplicationService,
        private commentsService: CommentsService,
        private log: Log,
        private elRef: ElementRef,
        private cdRef: ChangeDetectorRef) {
        elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
        this.checkLoged();
        this.reqcheckFacebookIntegration();
        let h = $(window).height();
        $('.faq-scroll').slimScroll({
            height: h - 60 + 'px'
        });
    }
    // public ngAfterViewInit() {
    //     this.log.info('');
    // }

    public ngOnDestroy(): void {
        this.isDestroy = true;
        $('body').removeClass('sidebar-mini');
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
    public ngOnInit() {
        // tslint:disable-next-line:only-arrow-functions
        setTimeout(function () {
            $('body').addClass('sidebar-mini');
        }, 1000);
        this.lstPosts = [];
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser.app_using && currentUser.app_using.id) {
                this.partnerId = currentUser.app_using.id;
                // this.getHashTagFallback();
                // this.getHashTag();
                // load agent active;
                this.getAgentActive();
                if (this.currentUser.deadlined) {
                    this.isexpire = true;
                }
            }
        }

        EmitterService.get('RESET_POST_SELECTED').subscribe((data: any) => {
            this.postOpen = null;
        });
    }

    public checkLoged() {
        if (localStorage.getItem('currentUser') === 'undefined') {
            this.router.navigate(['/login-hana']);
        } else {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
        if (this.currentUser.app_using) {
            this.partnerId = this.currentUser.app_using.id;
            this.application = this.currentUser.app_using.applications[0];
            this.appId = this.application.app_id;
            // this.callChange();
            this.log.info('PARNERIDDDDDDDD ' + this.partnerId);
        } else {
            localStorage.removeItem('currentUser');
            window.location.href = '/';
        }
    }

    // public ngOnDestroy(): void {

    // }

    public usingPost(post) {
        // this.setLoading();
        this.log.info('CHONPOSSST ' + JSON.stringify(post));
        this.callChange();
        // this.getListFAQByPostId(post);
        this.init();
        // this.createFallbackPost(post);
        // Neu load bai viet moi nhat thanh cong => load y dinh
        this.getPostNew(post, (cb) => {
            // this.setUnLoading();
            if (cb) {
                this.postOpen = post;
                this.addPostContext(post);
            }
        });
        // this.createHashTag(post);
    }

    // ################################################# BEGIN TOAN TM ###################################
    // toantm1
    public reqcheckFacebookIntegration() {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            let appId = currentUser.app_using.applications[0].app_id;
            let isIntegrationFacebook = false;
            if (currentUser.app_using.integrations) {
                currentUser.app_using.integrations.filter((item) => {
                    if (item.gatewayId === 1 && item.pageId && item.pageId.trim().length > 0) {
                        isIntegrationFacebook = true;
                    }
                });
            }
            if (!isIntegrationFacebook) {
                this.router.navigate(['/training-hana/faq']);
                return;
            }
            this.checkIntegrateFacebook(appId);
        }
    }
    public checkIntegrateFacebook(appId: string) {
        this.applicationService.getOnlyIntegrationGateway(appId, 1)
            .then((integrate) => {
                if (integrate) {
                    let fanpageInfo = integrate;
                    if (fanpageInfo && fanpageInfo.pageId && (fanpageInfo.isExpire !== 1)) {
                        // this.loadBlocks();
                    } else {
                        if (fanpageInfo.isExpire === 1 || fanpageInfo.isExpire === '1') {
                            this.router.navigate(['/training-hana/connect-integration'], { relativeTo: this.route });
                            return;
                        }
                    }
                }
            });
    }
    public init() {
        this.intentFallback = null;
        this.intents = [];
    }
    // tao fallback intent cho bai post
    public createFallbackPost(post) {
        if (!this.intentFallback && this.agentId) {
            let intentNew = { id: UUID.UUID(), is_new: true, name: 'default_fallback_' + post.post_id, agent_id: this.agentId, type: 2, usersays: [], faq: 1, intent_contexts: [], response_messages: [] };
            intentNew.response_messages = [{ content: JSON.stringify([{ type: 1, message: { posibleTexts: [] } }]) }];
            intentNew.intent_contexts = [{
                contexts: {
                    name: this.postOpen.post_id + '_post_context',
                    name_text: this.postOpen.post_id + '_post_context',
                    agent_id: this.agentId,
                    status: 1,
                    is_hidden: 1
                },
                type: 0,
                status: 1,
                is_hidden: 1
            }];
            this.intentFallback = intentNew;
        }
    }
    // toantm
    public createHashTag(post) {
        if (!post.content) {
            return;
        }
        let hashTag = StringFormat.hashTagRegex(post.content);
        if (!hashTag) {
            hashTag = [];
        }
        let contextIds = [];
        // commentsService
        this.getListHashtag((tags) => {
            if (tags) {
                // tags.filter((item) => {
                //     let tag = '#' + item.name + item.prefix;
                //     alert(tag);
                //     if (_.indexOf(hashTag, tag) > -1) {
                //         contextIds.push(item.context_id);
                //     }
                // });
                // alert(hashTag);
                hashTag.filter((item) => {
                    tags.filter((it) => {
                        let tag = '#' + it.name + it.prefix;
                        if (tag === item) {
                            contextIds.push(it.context_id);
                        }
                    });
                });

                if (contextIds.length === 0) {
                    return;
                }
                // let tagg = _.intersection([1, 2, 3], [101, 2, 1, 10], [2, 1]);
                // let contextIds = _.pluck(tags, 'context_id');
                this.getListFAQByContextId(contextIds, (cb) => {
                    // no comment
                });
            }
        });
    }
    // them ds hashtag detected tu bai post
    public addListHashtag(hashTags, callback) {
        let body: any = { partner_id: this.partnerId, tags: hashTags };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.commentsService.addListHashtag(body);
        actionOperate.subscribe(
            (tags) => {
                callback(tags);
            },
            (err) => {
                callback(false);
            });
    }
    // add danh sach context
    public getListFAQByContextIdOfPost(contextIds, callback) {
        this.loadingProcess = true;
        this.callChange();
        let body: any = { partner_id: this.partnerId, context_id: contextIds, isPost: true };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getListFAQByContextId(body);
        actionOperate.subscribe(
            (intents) => {
                this.loadingProcess = false;
                this.callChange();
                this.intentCurrents = [];
                intents.filter((item) => {
                    if (item.type === 2) {
                        this.intentFallback = item;
                    }
                });
                if (intents.length === 0 || !this.intentFallback) {
                    this.createFallbackPost(this.postOpen);
                }
                let it = _.reject(intents, (item) => { return item.type === 2; });
                this.intents = this.intents.concat(it);
                if (this.intents) {
                    this.intentCurrents = this.intents.slice(0, this.pageSize);
                    this.length = this.intents.length;
                }
                callback(true);
                // this.cdRef.detectChanges();
            },
            (err) => {
                this.loadingProcess = false;
                this.callChange();
                callback(true);
                this.log.info(err);
                this.intentCurrents = [];
            });
    }
    // add danh sach context
    public getListFAQByContextId(contextIds, callback) {
        let body: any = { partner_id: this.partnerId, context_id: contextIds };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getListFAQByContextId(body);
        actionOperate.subscribe(
            (intents) => {
                this.intentCurrents = [];
                let it = _.reject(intents, (item) => { return item.type === 2; });
                this.intents = this.intents.concat(it);
                if (this.intents) {
                    this.intentCurrents = this.intents.slice(0, this.pageSize);
                    this.length = this.intents.length;
                }
                this.intentCurrents.filter((item) => {
                    item.disabled = true;
                });
                callback(true);
            },
            (err) => {
                callback(true);
                this.log.info(err);
                this.intentCurrents = [];
            });
    }
    // lay ds faq theo bai post
    public getListFAQByPostId(post) {
        let body: any = { partner_id: this.partnerId, post_id: post.post_id };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getListFAQByPostId(body);
        actionOperate.subscribe(
            (intents) => {
                // this.tagSelected = tag;
                // this.tagName = tag.name;
                this.intentCurrents = [];
                this.intents = _.reject(intents, (item) => { return item.type === 2; });
                if (this.intents) {
                    this.intentCurrents = this.intents.slice(0, this.pageSize);
                    this.length = this.intents.length;
                }
            },
            (err) => {
                this.log.info(err);
                this.intentCurrents = [];
            });
    }
    // lay ds faq theo bai post
    public addPostContext(post) {
        this.loadingProcess = true;
        this.callChange();
        let body: any = { partner_id: this.partnerId, post_id: post.post_id };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.facebookPostService.addPostContext(body);
        actionOperate.subscribe(
            (context) => {
                if (context) {
                    this.loadingProcess = false;
                    this.callChange();
                    this.postContextId = _.pluck(context, 'context_id');
                    // load bai viet theo ngu canh
                    this.getListFAQByContextIdOfPost(this.postContextId, (cb) => {
                        // them tag add ds tag
                        // this.createHashTag(post);
                    });
                    let h = $(window).height();
                    $('.faq-scroll').slimScroll({
                        height: h - 60 + 'px',
                    });
                } else {
                    this.loadingProcess = false;
                    this.callChange();
                    // this.createHashTag(post);
                }
            },
            (err) => {
                this.loadingProcess = false;
                this.callChange();
                this.log.info(err);
            });
    }
    // lay thong tin bai viet moi nhat tu facebook
    public getPostNew(post, callback) {
        this.loadingProcess = true;
        this.callChange();
        if (!this.appId) {
            this.log.info('app_id not found');
            this.loadingProcess = false;
            this.callChange();
            return;
        }
        if (post && !post.post_id) {
            this.log.info('post_id not found');
            this.loadingProcess = false;
            this.callChange();
            return;
        }
        let body: any = { app_id: this.appId, post_id: post.post_id };
        let that = this;
        let actionOperate: Observable<any>;
        actionOperate = this.facebookPostService.getPostNew(body);
        actionOperate.subscribe(
            (context) => {
                this.loadingProcess = false;
                if (context) {
                    post.content = context.message;
                    callback(true);
                } else {
                    callback(false);
                    this.postOpen = null;
                    this.notificationService.showDanger('Lỗi khi tải bài post. Vui lòng thực hiện lại.');
                }
            },
            (err) => {
                this.loadingProcess = false;
                this.callChange();
                callback(false);
                this.postOpen = null;
                this.notificationService.showDanger('Lỗi khi tải bài post. Vui lòng thực hiện lại.');
                this.log.info(err);
            });
    }
    // them 1 intent cua bai post
    public addNewItem() {
        if (!this.agentId) {
            this.notificationService.showWarning('Thêm thất bại. Vui lòng thực hiện lại sau');
            this.getAgentActive();
            return;
        }
        this.loadingProcess = true;
        this.callChange();
        // let intentNew = { id: UUID.UUID(), is_new: true, usersays: [], faq: 1, response_messages: [] };
        // this.intents.unshift(intentNew);
        let intentNew = { id: UUID.UUID(), name: UUID.UUID(), is_just_understand_in_context: true, faq: 1, agent_id: this.agentId, intent_contexts: [] };

        // intentNew.intent_contexts = [{
        //     contexts: { id: this.postOpen.context_id },
        //     type: 0,
        //     status: 1
        // }];

        intentNew.intent_contexts = [{
            contexts: {
                name: this.postOpen.post_id + '_post_context',
                name_text: this.postOpen.post_id + '_post_context',
                agent_id: this.agentId,
                status: 1,
                is_hidden: 1
            },
            type: 0,
            status: 1,
            is_hidden: 1
        }];

        let actionOperate: Observable<any>;
        actionOperate = this.intentService.add(intentNew);
        actionOperate.subscribe(
            (intent) => {
                if (!intent) {
                    this.notificationService.showDanger('Thêm thất bại');
                    // callback(false);
                    this.loadingProcess = false;
                    this.callChange();
                    return;
                }
                // this.intents = intents;
                if (intent === 202) {
                    this.notificationService.showWarning('FAQ đã tồn tại');
                    this.loadingProcess = false;
                    this.callChange();
                    // callback(false);
                } else {
                    // this.notificationService.showSuccess('Thêm thành công');
                    // callback(intent);
                    this.intents.unshift(intent);
                    this.loadingProcess = false;
                    this.callChange();

                    if (this.intents) {
                        this.intentCurrents = this.intents.slice(0, this.pageSize);
                        this.length = this.intents.length;
                        this.pageIndex = 0;
                    }
                }
            },
            (err) => {
                // callback(false);
                this.loadingProcess = false;
                this.callChange();
                this.log.info(err);
                this.notificationService.showDanger('Thêm thất bại');
            });
    }
    // lay thong tin agent active
    public getAgentActive() {
        // this.loadingProcess = true;
        this.callChange();
        this.applicationService.getAgentActiveByPartnerId(this.partnerId)
            .then((agent) => {
                if (agent) {
                    // this.loadingProcess = false;
                    // this.callChange();
                    this.agentId = agent.agent_id;
                }
            });
    }
    // filter intent
    public transform(searchTerm: string) {
        searchTerm = StringFormat.formatText(searchTerm);
        if (searchTerm != null && searchTerm.length > 0) {
            let intentTemp = this.intents.filter((item) => {
                let temp = [];
                if (item.usersays) {
                    temp = item.usersays.filter((says) => {
                        return StringFormat.formatText(says.content).indexOf(searchTerm) !== -1
                    });
                }
                let response = '';
                if (item.response_messages && item.response_messages[0]) {
                    response = item.response_messages[0].content;
                }
                return (temp.length > 0 || StringFormat.formatText(response).indexOf(searchTerm) !== -1);
            });

            if (intentTemp) {
                this.intentCurrents = intentTemp.slice(0, this.pageSize);
                this.length = intentTemp.length;
                this.pageIndex = 0;
            }
        } else {
            if (this.intents) {
                this.intentCurrents = this.intents.slice(0, this.pageSize);
                this.length = this.intents.length;
                this.pageIndex = 0;
            }
        }
    }
    // phan trang y dinh
    public paginate(event: LazyLoadEvent) {
        this.pageIndex = parseInt(event.first + '', 10);
        this.pageSize = parseInt(event.rows + '', 10);
        this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
        this.length = this.intents.length;
    }
    // xoa intent
    public paginateDel() {
        // this.loadCarsLazy2(pageIndex, event.rows);
        this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
        if (this.intentCurrents.length === 0) {
            this.pageIndex = this.pageIndex - this.pageSize;
            this.intentCurrents = this.intents.slice(this.pageIndex, (this.pageIndex + this.pageSize));
            this.length = this.intents.length;
        }
        // if (this.intentCurrents.length === 0) {
        //   this.intentCurrents = this.intents.slice(this.pageIndex - this.pageSize, this.pageIndex);
        // }
    }
    // delete intent
    public deleteIntent(index) {
        swal({
            title: 'Bạn chắc chắn muốn xóa FAQ?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            // delete this.intents[index];
            let intent = this.intentCurrents[index];
            if (intent.is_new) {
                this.intents = this.intents.filter((it) => {
                    return it.id !== intent.id;
                });
                this.length = this.intents.length;
                this.paginateDel();
                swal({
                    title: 'Xóa thành công!',
                    text: '',
                    type: 'success',
                    confirmButtonClass: 'btn btn-success',
                    buttonsStyling: false
                });
            } else {
                let actionOperate: Observable<any>;
                actionOperate = this.intentService.delete(intent.id);
                actionOperate.subscribe(
                    (intents) => {
                        // alert(JSON.stringify(intents));
                        // this.intents = intents;
                        let itent = this.intentCurrents[index];
                        this.intents = this.intents.filter((it) => {
                            return it.id !== itent.id;
                        });
                        this.length = this.intents.length;
                        // this.intentCurrents.splice(index, 1);
                        this.paginateDel();
                        swal({
                            title: 'Xóa thành công!',
                            text: '',
                            type: 'success',
                            confirmButtonClass: 'btn btn-success',
                            buttonsStyling: false
                        });
                    },
                    (err) => {
                        this.log.info(err);
                    });
            }
        }, (dismiss) => {
            // a
        });
    }
    // lay danh sach hashtag
    public getListHashtag(callback) {
        this.facebookPostService.findAllHashtagPost(JSON.stringify({ partner_id: this.partnerId }))
            .then((posts) => {
                if (posts) {
                    callback(posts);
                } else {
                    callback(false);
                }
            });
    }
    public setLoading() {
        this.loadingProcess = true;
        this.callChange();
    }
    public setUnLoading() {
        this.loadingProcess = false;
        this.callChange();
    }
    // ################################################# END TOAN TM ###################################
}
