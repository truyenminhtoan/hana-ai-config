import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {NotificationService} from "../../../../../../_services/notification.service";
import {BlocksService} from "../../../../../../_services/blocks.service";
import {Observable} from "rxjs/Observable";
import {EmitterService} from "../../../../../../_services/emitter.service";
declare var $: any;
declare var swal: any;


@Component({
    selector: 'edit-menu-dialog',
    templateUrl: 'edit-menu.components.html',
    styleUrls: ['edit-menu.components.scss']
})
export class EditMenuDialogComponent {

    @Input() menu_cur: any;

    private url_value = '';
    private title_value = '';
    private text_value = '';
    private  menu : any;

    private dropdownList = [];
    private selectedItems = [];

    private dropdownListGroups = [];
    private selectedItemsGroup = [];

    private dropdownListSequnces = [];
    private selectedItemsSequnces = [];

    private dropdownSettings = {};

    /**text*/
    private selectedItemsGroupText = [];
    private selectedItemsSequncesText = [];

    /**text delete*/
    private selectedItemsGroupTextDelete = [];
    private selectedItemsSequncesTextDelete = [];

    /**group delete*/
    private selectedItemsGroupDelete = [];
    private selectedItemsSequncesDelete = [];


    /** Menu dung de gan gia tri save */
    private  menu_save : any;

    constructor(private blocksService: BlocksService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < EditMenuDialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        let that = this;
        if (this.data != undefined && this.data.menu_edit != undefined) {
            console.log('this.data.menu_edit', this.data.menu_edit);
            this.menu = this.data.menu_edit;
            // dung cho editr
            if (this.menu['menuRoot'].type == 'web_url') {
                this.url_value = this.menu['menuRoot'].value;
                this.title_value = this.menu['menuRoot'].title
            } else {
                if (this.menu['menuRoot'].type == 'nested') {
                    this.title_value = this.menu['menuRoot'].title;
                } else {
                    let decodedString = Buffer.from(this.menu['menuRoot'].value, 'base64').toString('utf8');
                    decodedString = JSON.parse(decodedString);

                    if (decodedString['postback_type'] == 'text') {
                        this.title_value = decodedString['title'];
                        this.text_value = decodedString['content'];
                        //group
                        if (this.data.selected_groups_text != undefined && this.data.selected_groups_text.length > 0) {
                            this.selectedItemsGroupText = this.data.selected_groups_text;
                        }
                        //sequence
                        if (this.data.selected_sequences_text != undefined && this.data.selected_sequences_text.length > 0) {
                            this.selectedItemsSequncesText = this.data.selected_sequences_text;
                        }
                        //group
                        if (this.data.selected_groups_text_delete != undefined && this.data.selected_groups_text_delete.length > 0) {
                            this.selectedItemsGroupTextDelete = this.data.selected_groups_text_delete;
                        }
                        //sequence
                        if (this.data.selected_sequences_text_delete != undefined && this.data.selected_sequences_text_delete.length > 0) {
                            this.selectedItemsSequncesTextDelete = this.data.selected_sequences_text_delete;
                        }
                    } else {
                        this.title_value = decodedString['title'];
                        if (this.data.selected_blocks != undefined && this.data.selected_blocks.length > 0) {
                            this.selectedItems = this.data.selected_blocks;
                        } else {
                            this.text_value = decodedString['content'];
                        }
                        //group
                        if (this.data.selected_groups != undefined && this.data.selected_groups.length > 0) {
                            this.selectedItemsGroup = this.data.selected_groups;
                        }
                        //sequence
                        if (this.data.selected_sequences != undefined && this.data.selected_sequences.length > 0) {
                            this.selectedItemsSequnces = this.data.selected_sequences;
                        }
                        //group
                        if (this.data.selected_groups_delete != undefined && this.data.selected_groups_delete.length > 0) {
                            this.selectedItemsGroupDelete = this.data.selected_groups_delete;
                        }
                        //sequence
                        if (this.data.selected_sequences_delete != undefined && this.data.selected_sequences_delete.length > 0) {
                            this.selectedItemsSequncesDelete = this.data.selected_sequences_delete;
                        }
                    }
                }
            }
            Object.keys(this.data.blocks).forEach(function(k) {
                that.dropdownList.push({
                    "id": that.data.blocks[k].id,
                    "name": that.data.blocks[k].name
                }, );
            });
            Object.keys(this.data.groups).forEach(function(k) {
                that.dropdownListGroups.push({
                    "id": that.data.groups[k].id,
                    "name": that.data.groups[k].name
                }, );
            });
            Object.keys(this.data.sequences).forEach(function(k) {
                that.dropdownListSequnces.push({
                    "id": that.data.sequences[k].id,
                    "name": that.data.sequences[k].name
                }, );
            });
        }
        this.loadConfigs();
    }

    public okClick() {
        this.returnData();
        if (!this.checkMenuValidate(this.menu_save.menuRoot)) {
            this.notificationService.showDanger("Menu phải nhập tiêu đề và nội dung của 1 trong 4 dạng text/ blocks/ url/ menu con");
        } else {
            if (!this.checkUrl(this.menu_save.menuRoot)) {
                this.notificationService.showDanger("Giá trị url không hợp lệ");
            } else {
                this.dialogRef.close(this.menu_save);
            }
        }
    }

    public checkUrl(menu) {
        let check = true;
        if (menu.type == 'web_url') {
            if (!this.isValidURL(menu.value.trim())) {
                check = false;
            }
        }

        return check;
    }

    isValidURL(url){
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if(RegExp.test(url)){
            return true;
        }else{
            return false;
        }
    }

    public checkMenuValidate(menu) {
        var check = true;
        if (menu.title.trim().length== 0) {
            check = false;
            return check;
        }
        if (menu.type == 'web_url') {
            if (menu.value.trim().length == 0) {
                check = false;
            }
        } else {
            if (menu.type != 'nested') {
                let decodedString = atob(menu.value);
                decodedString = JSON.parse(decodedString);
                if (decodedString['postback_type'] == 'text') {
                    if (decodedString['content'].trim().length == 0) {
                        check = false;
                    }
                } else {
                    if (decodedString['block_ids'].length == 0) {
                        check = false;
                    }
                }
            }
        }

        return check;
    }

    checkExists(item, lists) {
        for (let i = 0; i < lists.length; i++) {
            if (lists[i].id == item.id) {
                return false;
            }
        }

        return true;
    }

    loadConfigs() {
        let that = this;
        if (this.data.blocks != undefined) {
            that.dropdownList = [];
            Object.keys(this.data.blocks).forEach(function(k) {
                let item_add = {
                    "id": that.data.blocks[k].id,
                    "name": that.data.blocks[k].name
                };
                if (that.checkExists(item_add, that.dropdownList) == true) {
                    that.dropdownList.push(item_add);
                }
            });
        }
        this.dropdownList.sort(function (a, b) {
            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select block name",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            maxHeight: 50
        };
    }

    updateTypeMenu(type) {
        this.menu.menuRoot.type_custom = type;
        this.returnData();
    }


    onSelectedBlock(event) {
        this.returnData();
    }

    onAddGroup(event) {
        let that = this;
        if (event.display == event.value) {
            swal({
                title: 'Thêm nhóm',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroup.length; i++) {
                                if (that.selectedItemsGroup[i].value == event.display) {
                                    that.selectedItemsGroup[i].value = result_add_group.id;
                                    that.selectedItemsGroup[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroup = this.selectedItemsGroup.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    onAddGroupText(event) {
        let that = this;
        if (event.display == event.value) {
            swal({
                title: 'Thêm nhóm',
                text: 'Nhóm bạn vừa nhập hiện tại không tồn tại, bạn có muốn thêm nhóm này không?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(() => {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                let app_id = currentUser.app_using.applications[0].app_id;
                let body = {'app_id': app_id, 'name': event.display};
                let actionOperate: Observable<any>;
                actionOperate = that.blocksService.addNewGroupBlock(body);
                actionOperate.subscribe(
                    (result_add_group) => {
                        if (result_add_group.id != undefined) {
                            for (let i = 0 ; i < that.selectedItemsGroupText.length; i++) {
                                if (that.selectedItemsGroupText[i].value == event.display) {
                                    that.selectedItemsGroupText[i].value = result_add_group.id;
                                    that.selectedItemsGroupText[i].id =  result_add_group.id;
                                }
                            }
                            that.dropdownListGroups.push({
                                'id': result_add_group.id,
                                'name': result_add_group.name
                            });
                            that.notificationService.showSuccess("Tạo nhóm thành công!");
                            EmitterService.get('RELOAD_DATA_BLOCK').emit([]);
                        }
                        that.returnData();
                    },
                    (err) => {
                        console.log(err);
                    });
            }, (dismiss) => {
                this.selectedItemsGroupText = this.selectedItemsGroupText.filter((row) => {
                    return (row.value != event.value)
                });
            });
        } else {
            this.returnData();
        }
    }

    returnData() {

        /** đối với trường hợp update thì truyền id, không cần truyền index */

        // trim data
        this.text_value = this.text_value.trim();
        this.title_value = this.title_value.trim();
        this.url_value = this.url_value.trim();

        let payload;
        switch (this.menu.menuRoot.type_custom) {
            case 'text':
                let group_ids_text = _.pluck(this.selectedItemsGroupText, 'id');
                let sequence_ids_text = _.pluck(this.selectedItemsSequncesText, 'id');
                let group_ids_delete = _.pluck(this.selectedItemsGroupTextDelete, 'id');
                let sequence_ids_delete = _.pluck(this.selectedItemsSequncesTextDelete, 'id');
                payload = new Buffer(JSON.stringify({
                    "title": this.title_value,
                    "postback_type":"text",
                    "content": this.text_value,
                    "group_ids": group_ids_text,
                    "sequence_ids": sequence_ids_text,
                    "remove_group_ids": group_ids_delete,
                    "remove_sequence_ids": sequence_ids_delete,
                })).toString('base64');
                let menu_text = {
                    "app_id" : this.menu.app_id,
                    "menuRoot" : {
                        "id": this.menu.menuRoot.id,
                        "messengerProfileId" : this.menu.menuRoot.messengerProfileId,
                        "type" : "postback",
                        "title" : this.title_value,
                        "index" : this.menu.menuRoot.index,
                        "value": payload
                    }
                };
                this.menu_save = menu_text;
                break;
            case 'block':
                let block_ids = _.pluck(this.selectedItems, 'id');
                let group_ids = _.pluck(this.selectedItemsGroup, 'id');
                let sequence_ids = _.pluck(this.selectedItemsSequnces, 'id');
                let group_ids_delete_block = _.pluck(this.selectedItemsGroupDelete, 'id');
                let sequence_ids_delete_block = _.pluck(this.selectedItemsSequncesDelete, 'id');
                payload = new Buffer(JSON.stringify({
                    "title": this.title_value,
                    "postback_type": "link_blocks",
                    "block_ids": block_ids,
                    "group_ids": group_ids,
                    "sequence_ids": sequence_ids,
                    "remove_group_ids": group_ids_delete_block,
                    "remove_sequence_ids": sequence_ids_delete_block,
                })).toString('base64');
                let menu_postback = {
                    "app_id" : this.menu.app_id,
                    "menuRoot" : {
                        "id": this.menu.menuRoot.id,
                        "messengerProfileId" : this.menu.menuRoot.messengerProfileId,
                        "type" : "postback",
                        "title" : this.title_value,
                        "value": payload
                    }
                };
                this.menu_save = menu_postback;
                break;
            case 'web_url':
                let menu_web_url = {
                    "app_id" : this.menu.app_id,
                    "menuRoot" : {
                        "id": this.menu.menuRoot.id,
                        "messengerProfileId" : this.menu.menuRoot.messengerProfileId,
                        "type" : "web_url",
                        "title" : this.title_value,
                        "value": this.url_value
                    }
                };
                this.menu_save = menu_web_url;
                break;
            default:
                let menu_nested = {
                    "app_id" : this.menu.app_id,
                    "menuRoot" : {
                        "id": this.menu.menuRoot.id,
                        "messengerProfileId" : this.menu.menuRoot.messengerProfileId,
                        "type" : "nested",
                        "title" : this.title_value,
                    }
                };
                this.menu_save = menu_nested;
                break;
        }
    }

    public getStyleText(n) {
        if (n == 0) {
            return "rgba(183, 28, 28, 0.39)";
        } else {
            return "";
        }
    }

}