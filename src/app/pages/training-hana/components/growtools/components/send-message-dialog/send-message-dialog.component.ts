import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { StringFormat } from './../../../../../../themes/validators/string.format';
import { Validator } from './../../../../../../themes/validators/validator';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import * as _ from 'underscore';
import * as lodash from 'lodash';
import { Observable } from 'rxjs/Rx';
import { SelectItem } from 'primeng/primeng';
import { NotificationService } from '../../../../../../_services/notification.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { IntentService } from '../../../../../../_services/intents.service';
declare var $: any;
declare var swal: any;
@Component({
  selector: 'send-message-dialog',
  templateUrl: 'send-message-dialog.component.html',
  styleUrls: ['send-message-dialog.component.scss']
})
export class SendMessageGrowDialogComponent {
  private messageTexts = []; // gui comment, random
  private message: any;
  private customers: any;
  private appId: any;
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  private blockCurrent: any = { block_group_id: '', id: '', name: '', content: '[]', type: 'content' };
  private isSending = false;
  private queries;
  private fullName;
  private isCheckAll = false;
  private customerOutputs = [];

  // tslint:disable-next-line:max-line-length
  constructor( @Inject(MD_DIALOG_DATA) public data: any, 
  private log: Log,
  public dialogRef: MdDialogRef<SendMessageGrowDialogComponent>, private notificationService: NotificationService, private facebookService: FacebookService, private campaignService: CampaignService, private intentService: IntentService, private configService: ConfigService) {

    let temp = [];

    // this.data.customers.filter((item) => {
    //   temp.push(_.pick(item, 'id', 'page_customer_id', 'full_name', 'gender', 'page_id', 'fb_userchat', 'fb_conversation_id', 'fb_can_reply_comment_id'));
    //   // delete item.avatar_url;
    //   // delete item.comment_count;
    //   // delete item.inbox_count;
    //   // delete item.like_count;
    //   // delete item.comment_last_time;
    //   // delete item.inbox_last_time;
    //   // delete item.last_interaction;
    //   // delete item.phone;
    //   // delete item.real_name;
    // });

    // // this.customers = StringFormat.zipJson(temp);
    this.isCheckAll = this.data.is_check_all;
    this.customerOutputs = this.data.customer_outputs;
    this.queries = this.data.queries;
    this.fullName = this.data.full_name;
    this.appId = this.data.app_id;
    this.messageTexts = this.data.message_text;
    if (!this.messageTexts) {
      this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
      this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
      this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
    } else {
      try {
        switch (this.messageTexts.length) {
          case 0:
            this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
          case 1:
            this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
          case 2:
            this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
            break;
          default:
            break;
        }
      } catch (error) {
        this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
        this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
        this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
      }
    }
    // this.blockCurrent = this.data.message_block;
    // alert(JSON.stringify(this.blockCurrent));
    // EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
    // let fb = lodash.countBy(this.customers, { fb_userchat: null });
    // this.totalFb = fb.false ? fb.false : 0;
    // this.totalRecords = this.customers.length;
    // this.totalNotFb = this.totalRecords - this.totalFb;
  }

  public reset() {
    this.messageTexts = [];
    this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
    this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
    this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
    // this.blockCurrent.content = '[]';
    // EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
  }

  public send() {
    swal({
      title: this.configService.facebookPolicyTitle,
      text: this.configService.facebookPolicy,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Tiếp tục gửi',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      //
      this.sending();
    }, (dismiss) => {
      //
    });
  }

  public sending() {
    // if (_.isEmpty(this.customers)) {
    //   this.notificationService.showDanger('Vui lòng chọn khách hàng gửi tin nhắn');
    //   return;
    // }
    if (_.isEmpty(this.appId)) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng muốn gửi tin nhắn');
      return;
    }
    let isEmpty = false;
    this.messageTexts.filter((item) => {
      if (!item.message) {
        isEmpty = true;
      }
    });
    if (!this.messageTexts || !this.messageTexts[0] || this.messageTexts.length === 0 || isEmpty) {
      this.notificationService.showDanger('Vui lòng nhập nội dung gửi tin nhắn text');
      return;
    }

    let checkEditMess = false;
    this.messageTexts.filter((item) => {
      if (item.message === 'Xin chào {{ten_khach_hang}}.') {
        checkEditMess = true;
      }
    });

    if (checkEditMess) {
      this.notificationService.showDanger('Bạn vui lòng nhập đầy đủ nội dung các tin nhắn.');
      return;
    }

    // check noi dung chua lien ket
    let isLink = false;
    this.messageTexts.filter((item) => {
      if (Validator.isURL(item.message)) {
        isLink = true;
      }
      if (Validator.isEmail(item.message)) {
        isLink = true;
      }
    });

    if (isLink) {
      this.notificationService.showDanger('Nội dung tin nhắn không được chứa liên kết: website, email, hình ảnh, video,...');
      return;
    }

    let campaign: any = {};
    campaign.app_id = this.appId;
    campaign.type = 'grow';

    let arrMess = _.pluck(this.messageTexts, 'message');
    arrMess = _.reject(arrMess, (item) => {
      return !item.trim() || item === 'Xin chào {{ten_khach_hang}}.';
    });

    campaign.template_broadcast = { content_text: JSON.stringify(arrMess) };
    this.isSending = true;
    this.addCampaign(campaign);
  }

  public sendMessages(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.sendMessageStruct(body);
    actionOperate.subscribe(
      (cus) => {
        this.isSending = false;
        if (cus && cus.code === 200) {
          this.notificationService.showSuccess('Yêu cầu gửi tin nhắn của bạn đã được tiếp nhận');
        } else {
          this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        }
      },
      (err) => {
        this.isSending = false;
        this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        this.log.info(err);
      });
  }

  public addCampaign(campaign) {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.findOrCreateCampaign(campaign);
    actionOperate.subscribe(
      (result) => {
        // send message
        // alert(JSON.stringify(result));
        let body: any = {};
        body.customer_filter = JSON.stringify(this.queries);
        body.full_name = this.fullName;
        body.app_id = this.appId;
        body.template_broadcast_id = result[0].id;
        body.customers = this.customers;
        body.campaign_id = result[0].campaign;
        let arrMess = _.pluck(this.messageTexts, 'message');
        arrMess = _.reject(arrMess, (item) => {
          return !item.trim();
        });
        body.text_content = JSON.stringify(arrMess);
        if (this.isCheckAll && _.size(this.customerOutputs) === 0) {
          body.action = 'all';
        } else if
           (this.isCheckAll && _.size(this.customerOutputs) > 0) {
          body.action = 'unselected';
        } else if
             (!this.isCheckAll && _.size(this.customerOutputs) > 0) {
          body.action = 'selected';
        }
        body.customers = this.customerOutputs;
        // body.structure_content = this.blockCurrent.content_validate;
        this.sendMessages(body);
        // this.formatBlock(body.structure_content, (cb) => {
        //   if (cb) {
        //     body.structure_content = cb;
        //   } else {
        //     body.structure_content = null;
        //   }
        // });
        this.dialogRef.close('');
      },
      (err) => {
        this.isSending = false;
        this.log.info(err);
        this.notificationService.showDanger('Gửi tin nhắn đến khách hàng thất bại');
      });
  }

  public formatBlock(content, callback) {
    if (!content) {
      callback(null);
      return;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getValidateFacebookJSON({ messages: content });
    actionOperate.subscribe(
      (json) => {
        if (json.code === 200) {
          callback(json.data);
        } else {
          callback(null);
        }
      },
      (err) => {
        callback(null);
        this.log.info(err);
      });
  }

  public addMoreText() {
    this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
  }

  public removeText(i) {
    if (this.messageTexts.length <= 3) {
      this.notificationService.showDanger('Bạn phải nhập ít nhất 3 tin nhắn gửi random để tránh bị facebok xem là "SPAM"');
      return;
    }
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.messageTexts.splice(i, 1);
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      // huy
    });
  }

  public updateMessageText() {

  }
}
