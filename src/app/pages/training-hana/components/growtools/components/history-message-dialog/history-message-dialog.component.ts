import { Log } from './../../../../../../_services/log.service';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA, PageEvent } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import * as _ from 'underscore';
import * as lodash from 'lodash';
import { Observable } from 'rxjs/Rx';
import { SelectItem } from 'primeng/primeng';
import { NotificationService } from '../../../../../../_services/notification.service';
import { CampaignService } from '../../../../../../_services/campaign.service';
declare var $: any;
declare var swal: any;

@Component({
  selector: 'history-message-grow-dialog',
  templateUrl: 'history-message-dialog.component.html',
  styleUrls: ['history-message-dialog.component.scss']
})
export class HistoryMessageGrowDialogComponent implements OnInit {
  private campaignHistoryId: string = '';
  private datasource: any = [];
  private campaign: any = [];
  private broadcasts: any = [];
  private appId;
  private messageLoading = 'Đang tải dữ liệu';

  // MdPaginator Inputs
  length = 100;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];

  // MdPaginator Output
  pageEvent: PageEvent;

  public ngOnInit(): void {
    this.findCampaign();
  }
  constructor( @Inject(MD_DIALOG_DATA) public data: any,
  private log: Log,
  public dialogRef: MdDialogRef<HistoryMessageGrowDialogComponent>, public campaignService: CampaignService, public notificationService: NotificationService) {
    this.appId = this.data.app_id;
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  changeEvent(event) {
    let pageIndex = event.pageIndex;
    this.loadCarsLazy(pageIndex);
  }

  public loadCarsLazy(index) {
    if (this.datasource) {
      // this.cars = this.datasource.slice(event.first, (event.first + event.rows));
      this.broadcasts = this.datasource.slice(index * this.pageSize, index * this.pageSize + this.pageSize);
    }
  }

  public getListBroadcast() {
    this.messageLoading = 'Đang tải dữ liệu...';
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getListBroadcast(this.campaignHistoryId);
    actionOperate.subscribe(
      (result) => {
        this.datasource = result;

        if (this.datasource && this.datasource.length > 0) {
          this.datasource.filter((item) => {
            try {
              item.content_text = JSON.parse(item.content).text_content;
              if (!item.content_text) {
                item.content_text = 'Nội dung block';
              }
            } catch (error) {
              this.log.info(error);
              item.content_text = 'Không xác định';
            }
          });

          this.messageLoading = '';
          this.broadcasts = this.datasource.slice(0, this.pageSize);
          this.length = this.datasource.length;
        } else {
          this.messageLoading = 'Không có dữ liệu';
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

  public findCampaign() {

    let campaign: any = {};
    campaign.app_id = this.appId;
    campaign.type = 'grow';
    campaign.template_broadcast = {};

    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.findOrCreateCampaign(campaign);
    actionOperate.subscribe(
      (result) => {
        this.campaignHistoryId = result[0].campaign;
        this.getListBroadcast();
      },
      (err) => {
        this.log.info(err);
      });
  }

  private cancelBroadcast(item) {
      swal({
          title: 'Huỷ thông điệp',
          text: 'Bạn có chắc chắn muốn huỷ thông điệp này?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Đồng ý',
          cancelButtonText: 'Không',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
      }).then(() => {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          let app_id = currentUser.app_using.applications[0].app_id;
          let body = {'appId': app_id, 'broadcastId': item.broadcast_id};
          let actionOperate: Observable<any>;
          actionOperate = this.campaignService.cancelBroadcast(body);
          actionOperate.subscribe(
              (res) => {
                  this.notificationService.showSuccess("Thực hiện huỷ thành công, thông điệp sử được huỷ gửi trong vài phút");
                  for (let i = 0; i < this.broadcasts.length; i++) {
                    if (this.broadcasts[i] === item) {
                        this.broadcasts[i].status = -1;
                        break;
                    }
                  }
              },
              (err) => {
                  console.log(err);
                  this.notificationService.showSuccess("Thực hiện huỷ thất bại");
              });
      }, (dismiss) => {
      });
  }
}
