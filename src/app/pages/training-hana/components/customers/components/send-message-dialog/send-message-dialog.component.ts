import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { StringFormat } from './../../../../../../themes/validators/string.format';
import { Validator } from './../../../../../../themes/validators/validator';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import * as _ from 'underscore';
import * as lodash from 'lodash';
import { Observable } from 'rxjs/Rx';
import { SelectItem } from 'primeng/primeng';
import { NotificationService } from '../../../../../../_services/notification.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { IntentService } from '../../../../../../_services/intents.service';
declare var $: any;
declare var swal: any;
@Component({
  selector: 'send-message-dialog',
  templateUrl: 'send-message-dialog.component.html',
  styleUrls: ['send-message-dialog.component.scss']
})
export class SendMessageDialogComponent {
  private messageTexts = []; // gui comment, random
  private message: any;
  private customers: any;
  private appId: any;
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  private blockCurrent: any = { block_group_id: '', id: '', name: '', content: '[]', type: 'content' };
  private dataSetSize = null;
  private expendDays = null;
  private isSending = false;
  private queries;
  private fullName;
  private isTachTiep = false;
  private isCheckAll = false;
  private customerOutputs = [];

  // tslint:disable-next-line:max-line-length
  constructor( @Inject(MD_DIALOG_DATA) public data: any,
  private log: Log,
  public configService: ConfigService, public dialogRef: MdDialogRef<SendMessageDialogComponent>, private notificationService: NotificationService, private facebookService: FacebookService, private campaignService: CampaignService, private intentService: IntentService) {
    // let temp = [];

    // this.data.customers.filter((item) => {
    //   temp.push(_.pick(item, 'id', 'page_customer_id', 'full_name', 'gender', 'page_id', 'fb_userchat', 'fb_conversation_id', 'fb_can_reply_comment_id'));
    // });

    // this.customers = StringFormat.zipJson(temp);
    this.isCheckAll = this.data.is_check_all;
    this.customerOutputs = this.data.customer_outputs;
    this.queries = this.data.queries;
    this.fullName = this.data.full_name;
    this.appId = this.data.app_id;
    this.blockCurrent = this.data.message_block;
    this.isSending = false;
  }

  public reset() {
    this.blockCurrent.content = '[]';
    EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
  }

  public send() {
    swal({
      title: this.configService.facebookPolicyTitle,
      text: this.configService.facebookPolicy,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Tiếp tục gửi',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      //
      this.sending();
    }, (dismiss) => {
      //
    });
  }

  public sending() {
    if (_.isEmpty(this.appId)) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng muốn gửi tin nhắn');
      return;
    }

    if (this.isTachTiep) {
      if (this.dataSetSize !== null && (this.dataSetSize < 1 || this.dataSetSize > 1000)) {
        this.notificationService.showDanger('Số lượng khách hàng nhập trong khoảng 1 đến 1000');
        return;
      }

      if (this.dataSetSize !== null && this.dataSetSize % 1 !== 0) {
        this.notificationService.showDanger('Số lượng khách hàng là số nguyên dương');
        return;
      }

      if (this.dataSetSize !== null && this.expendDays === null) {
        this.notificationService.showDanger('Bạn phải nhập thời gian gửi giữa mỗi đợt');
        return;
      }

      if (this.expendDays !== null && this.expendDays <= 0) {
        this.notificationService.showDanger('Thời gian gửi giữa mỗi đợt phải lớn hơn 0');
        return;
      }
      if (this.expendDays !== null && this.expendDays % 1 !== 0) {
        this.notificationService.showDanger('Thời gian gửi là số nguyên dương');
        return;
      }

      if (this.expendDays !== null && this.dataSetSize === null) {
        this.notificationService.showDanger('Bạn phải nhập số lượng khách hàng');
        return;
      }
    }

    let content: any;
    try {
      content = JSON.parse(this.blockCurrent.content);
      if (content.length === 0) {
        this.blockCurrent.content = '';
      }
    } catch (error) {
      this.blockCurrent.content = '';
    }
    this.isSending = true;
    this.formatBlock(this.blockCurrent.content, (cb) => {
      if (!cb || JSON.parse(cb).length === 0) {
        this.notificationService.showDanger('Vui lòng nhập đầy đủ nội dung tin nhắn gửi đến khách hàng');
        this.isSending = false;
        return;
      }
      this.blockCurrent.content_validate = cb;
      let campaign: any = {};
      campaign.app_id = this.appId;
      campaign.type = 'any';

      campaign.template_broadcast = { content: this.blockCurrent.content, content_validate: cb, data_set_size: this.dataSetSize, expand_days: this.expendDays };
      this.addCampaign(campaign);
    });
  }

  public sendMessages(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.sendMessageStruct(body);
    actionOperate.subscribe(
      (cus) => {
        this.isSending = false;
        if (cus && cus.code === 200) {
          this.notificationService.showSuccess('Yêu cầu gửi tin nhắn của bạn đã được tiếp nhận');
        } else {
          this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        }
      },
      (err) => {
        this.isSending = false;
        this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        this.log.info(err);
      });
  }

  public addCampaign(campaign) {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.findOrCreateCampaign(campaign);
    actionOperate.subscribe(
      (result) => {
        // send message
        // alert(JSON.stringify(result));
        let body: any = {};
        body.customer_filter = JSON.stringify(this.queries);
        body.full_name = this.fullName;
        body.app_id = this.appId;
        body.template_broadcast_id = result[0].id;
        body.customers = this.customers;
        body.structure_content = this.blockCurrent.content_validate;
        body.campaign_id = result[0].campaign;
        if (this.isTachTiep) {
          if (this.dataSetSize !== null) {
            body.data_set_size = this.dataSetSize;
          }
          if (this.expendDays !== null) {
            body.expand_days = this.expendDays;
          }
        }
        if (this.isCheckAll && _.size(this.customerOutputs) === 0) {
          body.action = 'all';
        } else if
           (this.isCheckAll && _.size(this.customerOutputs) > 0) {
          body.action = 'unselected';
        } else if
             (!this.isCheckAll && _.size(this.customerOutputs) > 0) {
          body.action = 'selected';
        }
        body.customers = this.customerOutputs;
        this.sendMessages(body);
        this.dialogRef.close('');
      },
      (err) => {
        this.log.info(err);
        this.notificationService.showDanger('Gửi tin nhắn đến khách hàng thất bại');
        this.isSending = false;
      });
  }

  public formatBlock(content, callback) {
    if (!content) {
      callback(null);
      return;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getValidateFacebookJSON({ messages: content });
    actionOperate.subscribe(
      (json) => {
        if (json.code === 200) {
          callback(json.data);
        } else {
          callback(null);
        }
      },
      (err) => {
        callback(null);
        this.log.info(err);
      });
  }

  public addMoreText() {
    this.messageTexts.push({ message: 'Xin chào {{ten_khach_hang}}.' });
  }

  public removeText(i) {
    if (this.messageTexts.length <= 3) {
      this.notificationService.showDanger('Bạn phải nhập ít nhất 3 tin nhắn gửi random để tránh bị facebok xem là "SPAM"');
      return;
    }
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.messageTexts.splice(i, 1);
      swal({
        title: 'Xóa thành công!',
        text: '',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
        buttonsStyling: false
      });
    }, (dismiss) => {
      // huy
    });
  }

  public updateMessageText() {

  }
}
