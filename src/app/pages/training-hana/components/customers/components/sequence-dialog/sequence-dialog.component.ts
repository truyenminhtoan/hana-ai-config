import { Log } from './../../../../../../_services/log.service';
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FacebookService } from '../../../../../../_services/facebook.service';
import { NotificationService } from '../../../../../../_services/notification.service';
import { CampaignService } from '../../../../../../_services/campaign.service';
import * as _ from 'underscore';
@Component({
  selector: 'sequence-dialog',
  templateUrl: 'sequence-dialog.component.html',
  styleUrls: ['sequence-dialog.component.scss']
})
export class SequenceDialogComponent implements OnInit {

  private myControl = new FormControl();
  private options = [];
  private filteredOptions: Observable<any[]>;
  private queries;
  private fullName;
  private customers: any = [];
  private actionType;
  private appId = '';
  private campaignsSequence: any = [];
  private isCheckAll = false;
  private customerOutputs = [];

  public ngOnInit() {
    this.getListGroup();
  }

  private filter(name: string): any[] {
    name = name.trim();
    name = name.toLowerCase();
    // return this.options.filter((option) => new RegExp(`^${name}`, 'gi').test(option.name.trim()));
    return this.options.filter((option) => option.name.toLowerCase().trim().indexOf(name) > -1);
  }

  private displayFn(user: any): string {
    return user ? user.name : user;
  }

  // tslint:disable-next-line:member-ordering
  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:member-ordering
  constructor( @Inject(MD_DIALOG_DATA) public data: any,
  private log: Log,
  public dialogRef: MdDialogRef<SequenceDialogComponent>, private facebookService: FacebookService, private campaignService: CampaignService, private notificationService: NotificationService) {
    this.isCheckAll = this.data.is_check_all;
    this.customerOutputs = this.data.customer_outputs;
    this.queries = this.data.queries;
    this.fullName = this.data.full_name;
    this.actionType = this.data.action_type;
    this.appId = this.data.app_id;
  }

  private action() {
    let options: any = {};
    options.app_id = this.appId;
    options.queries = this.queries;
    options.full_name = this.fullName;
    options.campaign_id = this.myControl.value.id;
    options.queries = this.queries;
    if (this.isCheckAll && _.size(this.customerOutputs) === 0) {
      options.action = 'all';
    } else if
       (this.isCheckAll && _.size(this.customerOutputs) > 0) {
        options.action = 'unselected';
    } else if
         (!this.isCheckAll && _.size(this.customerOutputs) > 0) {
          options.action = 'selected';
    }
    options.customers = this.customerOutputs;
    if (this.actionType === 'add') {
      this.addListCustomerToGroup(options);
    }
    if (this.actionType === 'delete') {
      this.deleteListCustomerFromGroup(options);
    }
    this.dialogRef.close('');
  }

  private close() {
    this.log.info(this.myControl.value);
    this.dialogRef.close('');
  }

  private getListGroup() {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getAllCampaign(this.appId);
    actionOperate.subscribe(
      (services) => {
        services.filter((item) => {
          if (item.type === 'sequence') {
            this.campaignsSequence.push(item);
          }
        });
        this.options = this.campaignsSequence;
        this.filteredOptions = this.myControl.valueChanges
          .startWith(null)
          .map((user) => user && typeof user === 'object' ? user.name : user)
          .map((name) => name ? this.filter(name) : this.options.slice());
      },
      (err) => {
        this.log.info(err);
      });
  }

  private addListCustomerToGroup(body) {
    body.has_fb_userchat = true;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addCustomerToSequence(body);
    actionOperate.subscribe(
      (result) => {
        if (result) {
          this.notificationService.showSuccess('Thêm khách hàng vào sequence thành công');
        } else {
          this.notificationService.showDanger('Thêm khách hàng vào sequence thất bại');
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

  private deleteListCustomerFromGroup(body) {
    body.has_fb_userchat = true;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.removeCustomerToSequence(body);
    actionOperate.subscribe(
      (result) => {
        if (result) {
          this.notificationService.showSuccess('Xóa khách hàng từ sequence thành công');
        } else {
          this.notificationService.showDanger('Xóa khách hàng từ sequence thất bại');
        }
      },
      (err) => {
        this.log.info(err);
        this.notificationService.showDanger('Xóa khách hàng từ sequence thất bại');
      });
  }
}
