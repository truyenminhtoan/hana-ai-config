import { Log } from './../../../../../_services/log.service';
import { ApplicationService } from './../../../../../_services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { LazyLoadEvent } from 'primeng/primeng';
import { FacebookService } from '../../../../../_services/facebook.service';
import { Observable } from 'rxjs/Observable';
import { MdDialog } from '@angular/material';
import { GroupsDialogComponent } from '../components/groups-dialog/groups-dialog.component';
import _ from 'underscore';
import { SendMessageDialogComponent } from '../components/send-message-dialog/send-message-dialog.component';
import { NotificationService } from '../../../../../_services/notification.service';
import { Message } from '../../../../../../assets/plugins/primeng/components/common/message';
import { SequenceDialogComponent } from '../components/sequence-dialog/sequence-dialog.component';
import { HistoryMessageDialogComponent } from '../components/history-message-dialog/history-message-dialog.component';
declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'customer-list',
  templateUrl: 'customer-list.component.html',
  styleUrls: ['customer-list.component.scss']
})
export class CustomerListComponent implements OnInit, AfterViewInit, OnDestroy {

  /** Filter data ##########################################################################################*/
  // @Input searchTypeDataFilter2 cấu trúc mẫu
  private searchTypeDataFilter2;

  // @input conditionsSaved là chuỗi query output trc đó được lưu lại, dùng để hiện thị list conditions khi load list khách hàng lần đầu tiên
  // private conditionsSaved = [{"syntax":"and","syntax_display":"VÀ","field":{"value":"cus.full_name","label":"Tên khách hàng"},"operator":{"value":"ct","label":"chứa"},"value":{"value":"toan111","label":"toan111"}},{"syntax":"and","syntax_display":"VÀ","field":{"value":"cus.gender","label":"Giới tính"},"operator":{"value":"nin","label":"không là"},"value":{"value":"male,female","label":"Nam, Nu"}},{"syntax":"and","syntax_display":"VÀ","field":{"value":"pag.last_interaction","label":"Tương tác gần nhất"},"operator":{"value":"bw","label":"trong khoảng"},"value":{"value":"2017-10-31T17:00:00.000Z","label":"2017-10-31T17:00:00.000Z","value2":"2017-11-29T17:00:00.000Z","label2":"2017-11-29T17:00:00.000Z"}},{"syntax":"or","syntax_display":"HOẶC","field":{"value":"ph.phone","label":"Số điện thoại"},"operator":{"value":"nin","label":"không là"},"value":{"value":"0123456789,0123456710","label":"0123456789,0123456710"}}];
  private conditionsSaved = [];

  // @input condition default khi nhấn button thêm condition, nếu không truyền tham số này thì sẽ lấy giá trị mặc định
  // private condition_default: any = {
  //   "syntax": "VÀ",
  //   "field": {
  //     "value": "cus.full_name",
  //     "label": "Tên khách hàng"
  //   },
  //   "operator": {
  //     "value": "ct",
  //     "label": "chứa"
  //   },
  //   "value": {
  //     "value": "Nguyen Tam",
  //     "label": "Nguyen Tam"
  //   }
  // };

  private condition_default: any = {
    "syntax": "VÀ",
    "field": {
      "value": "",
      "label": ""
    },
    "operator": {
      "value": "",
      "label": ""
    },
    "value": {
      "value": "",
      "label": ""
    }
  };

  private is_load_data_filter = false;
  /** End filter data ##########################################################################################*/


  private searchTypeData;
  private buttomFilter = false;
  private queries: any = [];
  private isFilter = false;
  private keySearch = '';
  private appId = 'c3514ef6-9ae0-4073-a725-d434fc9930fb';
  private pageSize = 50;

  private datasource: any[] = [];
  private cars: any[] = [];
  private totalRecords: number = 0;
  private totalSelects: number = 0;
  private selectedCustomers: any[] = [];
  private tableMessage = 'Không có dữ liệu';
  private fanpageInfo;

  private messageText: any = [{ message: 'Xin chào {{ten_khach_hang}}.' }];
  private messageBlock: any = { block_group_id: '', id: '', name: '', content: '[]', type: 'content' };
  private selectedItems = [];
  private check_status = false;
  private loadingdata = false;
  private customer_outputs = [];

  private checkedAll = false;

  constructor(
    private applicationService: ApplicationService,
    private route: ActivatedRoute,
    private router: Router,
    private elRef: ElementRef,
    private facebookService: FacebookService,
    private dialog: MdDialog,
    private log: Log,
    private notificationService: NotificationService) {
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  public ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
      let isIntegrationFacebook = false;
      if (currentUser.app_using.integrations) {
        currentUser.app_using.integrations.filter((item) => {
          if (item.gatewayId === 1 && item.pageId && item.pageId.trim().length > 0) {
            isIntegrationFacebook = true;
          }
        });
      }
      if (!isIntegrationFacebook) {
        this.router.navigate(['/remarketingv2/faq']);
        return;
      }
      this.checkIntegrateFacebook(this.appId);
    }
    $.getScript('../../../assets/js/core/jquery-ui.min.1.12.1.js', () => {
      this.getSearchTypeFilterV2(this.appId);
      this.getSearchType(this.appId);
      this.search(0, this.pageSize, true);

      let h = $(window).height();
      $('.card').slimScroll({
        height: h - 60 + 'px'
      });
    });

    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
    }
    this.getSearchTypeFilterV2(this.appId);
    this.getSearchType(this.appId);
    this.search(0, this.pageSize, true);
  }


  public ngOnDestroy(): void {
    $('body').removeAttr('style');
  }

  public onRowSelect(event) {
    // console_bk.log('select row');
    // this.router.navigate(['/pages/services/serviceEdit/' + event.data.id], { relativeTo: this.route });

  }

  public actionSearch() {
    this.selectedItems = [];
    this.getDataCheck();
    this.searchLoc(0, this.pageSize, true);
  }

  public ngAfterViewInit() {
    $('.dropdown-toggle').dropdown();
    // $('body').removeClass('sidebar-mini');
    $('.condition-item1').dropdown();
  }

  public loadCarsLazy(event: LazyLoadEvent) {
    this.loadingdata = true;
    // console_bk.log('HUYNHDC  loaddata');
    // alert(JSON.stringify(event));
    this.pageSize = event.rows;
    this.search(event.first, event.rows, false);
    // alert(1);

    // if (this.datasource) {
    //   this.cars = this.datasource.slice(event.first, (event.first + event.rows));
    // }
  }

  public searchLoc(currentItem, pageSize, notCount) {
        let options: any = {};
        options.app_id = this.appId;
        options.last_item = currentItem;
        options.page_size = pageSize;
        options.queries = this.queries;
        options.full_name = this.keySearch;
        // console_bk.log('options', options);
        if (notCount) {
            this.getTotalCustomer(options);
        }
        this.getListServiceLoc(options);
  }

  public search(currentItem, pageSize, notCount) {
    let options: any = {};
    options.app_id = this.appId;
    options.last_item = currentItem;
    options.page_size = pageSize;
    options.queries = this.queries;
    options.full_name = this.keySearch;
    // console_bk.log('options', options);
    if (notCount) {
      this.getTotalCustomer(options);
    }
    this.getListService(options);
  }

  public getSearchType(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        let searchType = [];
        result.filter((item) => {
          let search: any = { id: item.param, type: item.value_type, label: item.name };
          if (item.value_type === 'list') {
            if (item.page_values.length > 0) {
              search.list = item.page_values;
              searchType.push(search);

              // item.page_values.filter((it) => {
              //   it.label = `${it.label}(${it.count})`;
              // });
            }
          } else {
            searchType.push(search);
          }
        });
        this.searchTypeData = searchType;
        $.getScript('../../../../../../assets/plugins/structured-filter/js/structured-filter.js', () => {
          // this._queryBuilder2();
        });
      },
      (err) => {
        this.log.info(err);
      });
  }

  public getSearchTypeFilterV2(appId) {
    this.is_load_data_filter = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        this.searchTypeDataFilter2 = result;
        this.is_load_data_filter = false;
      },
      (err) => {
        this.is_load_data_filter = false;
        // console_bk.log(err);
      });
  }

  public reloadSearchType(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        // this.searchTypeData = result;
        this.searchTypeDataFilter2 = result;
      },
      (err) => {
        this.log.info(err);
      });
  }

  public getListService(body) {
    // alert('request server');
    this.tableMessage = 'Đang tải dữ liệu';
    this.selectedCustomers = [];
    this.datasource = [];
    this.cars = [];
    let actionOperate: Observable<any>;
    body.has_fb_userchat = true; // chi lay kh co userchat
    actionOperate = this.facebookService.getListCustomer(body);
    actionOperate.subscribe(
      (services) => {
        // alert(JSON.stringify(services));
        // this.datasource = services;
        // this.totalRecords = this.datasource.length;
        // this.cars = services;

        // this.datasource = services;
        // this.totalRecords = services.length;
        // this.cars = this.datasource.slice(0, this.pageSize);
        this.cars = services;
        if (this.check_status) {
          _.each(this.cars, (item) => {
            let uncheck = _.find(this.selectedItems, (itemSec) => {
              return item.id === itemSec.id;
            });
            if (uncheck) {
              item.status = false;
            } else {
              item.status = true;
            }
          });
        } else {
          _.each(this.cars, (item) => {
            let uncheck = _.find(this.selectedItems, (itemSec) => {
              return item.id === itemSec.id;
            });
            if (uncheck) {
              item.status = true;
            } else {
              item.status = false;
            }
          });
        }
        if (this.cars.length < 1) {
          this.tableMessage = 'Không có dữ liệu';
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

    public getListServiceLoc(body) {
        // alert('request server');
        this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.datasource = [];
        this.cars = [];
        let actionOperate: Observable<any>;
        body.has_fb_userchat = true; // chi lay kh co userchat
        actionOperate = this.facebookService.getListCustomer(body);
        actionOperate.subscribe(
            (services) => {
                // alert(JSON.stringify(services));
                // this.datasource = services;
                // this.totalRecords = this.datasource.length;
                // this.cars = services;

                // this.datasource = services;
                // this.totalRecords = services.length;
                // this.cars = this.datasource.slice(0, this.pageSize);
                this.cars = services;
                $('#click_all').prop('checked', false);
                this.checkAll(false);
                if (this.cars.length < 1) {
                    this.tableMessage = 'Không có dữ liệu';
                }
            },
            (err) => {
                this.log.info(err);
            });
    }

  public getTotalCustomer(body) {
    let actionOperate: Observable<any>;
    body.has_fb_userchat = true; // chi lay kh co userchat
    actionOperate = this.facebookService.getTotalCustomer(body);
    actionOperate.subscribe(
      (result) => {
        this.totalRecords = result.count;
        console.log('this.totalRecords', this.totalRecords);
      },
      (err) => {
        this.log.info(err);
      });
  }

  public toggleFilter() {
    this.buttomFilter = !this.buttomFilter;
  }

  public _queryBuilder2() {
    let that = this;
    $('#myFilter').structFilter({
      highlight: true,
      buttonLabels: true,
      submitReady: true,
      fields: this.searchTypeData
    });

    // tslint:disable-next-line:only-arrow-functions
    $('#myFilter').on('submit.search', function (event) {
      // do something
      alert(event);
    });

    // tslint:disable-next-line:only-arrow-functions
    $('#myFilter').on('change.search', function (event) {
      // do something
      let queries = [];
      let data = $('#myFilter').structFilter('val');
      that.queries = data;
      that.search(0, this.pageSize, true);
    });
  }

  public openGroupDialog(type) {
    // let ids = _.pluck(this.selectedCustomers, 'page_customer_id');
    if (!this.check_status && this.customer_outputs.length === 0) {
      this.notificationService.showDanger('Không có khách hàng được chọn để đưa vào nhóm');
      return;
    }
    let dialogRef = this.dialog.open(GroupsDialogComponent, {
      // height: '310px',
      // width: '50%',
      data: {
        is_check_all: this.check_status,
        customer_outputs: this.customer_outputs,
        queries: this.queries,
        full_name: this.keySearch,
        action_type: type,
        app_id: this.appId
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadSearchType(this.appId);
    });
  }

  public openSequenceDialog(type) {
    // let ids = _.pluck(this.selectedCustomers, 'page_customer_id');
    // if (_.isEmpty(ids)) {
    //   this.notificationService.showSuccess('Vui lòng chọn khách hàng trước');
    //   return;
    // }
    //let check_status =  $('#click_all').is(":checked");
    let check_status =  this.check_status;
    if (!check_status && this.customer_outputs.length === 0) {
      this.notificationService.showDanger('Không có khách hàng được chọn để đưa vào quy trình');
      return;
    }
    let dialogRef = this.dialog.open(SequenceDialogComponent, {
      // height: '310px',
      // width: '50%',
      data: {
        is_check_all: check_status,
        customer_outputs: this.customer_outputs,
        queries: this.queries,
        full_name: this.keySearch,
        action_type: type,
        app_id: this.appId
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadSearchType(this.appId);
    });
  }

  public openSendMessageDialog() {
    // if (_.isEmpty(this.selectedCustomers)) {
    //   this.notificationService.showSuccess('Vui lòng chọn khách hàng gửi tin nhắn');
    //   return;
    // }
    //let check_status =  $('#click_all').is(":checked");
    let check_status =  this.check_status;
    if (!check_status && this.customer_outputs.length === 0) {
      this.notificationService.showDanger('Không có khách hàng được chọn để gửi tin nhắn');
      return;
    }
    let dialogRef = this.dialog.open(SendMessageDialogComponent, {
      // height: '310px',
      width: '50%',
      data: {
        // customers: this.selectedCustomers,
        queries: this.queries,
        is_check_all: check_status,
        customer_outputs: this.customer_outputs,
        full_name: this.keySearch,
        app_id: this.appId,
        message_text: this.messageText,
        message_block: this.messageBlock
      }
    });
  }

  public openHistoryMessageDialog() {
    let dialogRef = this.dialog.open(HistoryMessageDialogComponent, {
      // height: '310px',
      width: '50%',
      data: {
        app_id: this.appId,
      }
    });
  }

  public checkIntegrateFacebook(appId: string) {
    let that = this;
    this.applicationService.getOnlyIntegrationGateway(appId, 1)
      .then((integrate) => {
        if (integrate) {
          this.fanpageInfo = integrate;
          // this.showModel();
          if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1)) {
            // truong hop accessToken con han su dung
            // this.log.info('load facebook');
            this.loadData();
          } else {
            // let body: any = {};
            // body.app_id = this.appId;
            // body.acess_token = this.pageAccessGrowToken;
            // this.createGrowToolGateway(body);
            if (this.fanpageInfo.isExpire === 1 || this.fanpageInfo.isExpire === "1") {
              this.router.navigate(['/remarketingv2/connect-integration'], { relativeTo: this.route });
              return;
            }
          }
        }
      });
  }

  public loadData() {
    /* $.getScript('../../../assets/js/core/jquery-ui.min.1.12.1.js', () => {
      this.getSearchTypeFilterV2(this.appId);
      this.search(0, this.pageSize);

      let h = $(window).height();
      $('.card').slimScroll({
        height: h - 60 + 'px'
      });
    }); */
    this.getSearchTypeFilterV2(this.appId);
    this.search(0, this.pageSize, true);

    let h = $(window).height();
    $('.card').slimScroll({
      height: h - 60 + 'px'
    });
  }

   /** @TamNguyen */
   public getDataCheck() {
      if (!this.selectedItems) {
        this.selectedItems = [];
      }
      // let check_status =  $('#click_all').is(":checked");
      let check_status =  this.check_status;
      this.customer_outputs = [];
      // - Nếu check all: Gửi query + Gửi tập khách hàng Uncheck - Nếu Uncheck all: Gửi tập khách hàng check
      this.customer_outputs = this.selectedItems;

      console.log('this.selectedItems', this.selectedItems);
      console.log('this.customer_outputs', this.customer_outputs);
      console.log('this.check_status', check_status);

      if (check_status && this.customer_outputs.length === 0) {
        this.totalSelects = this.totalRecords;
      } else if (check_status && this.customer_outputs.length > 0) {
        this.totalSelects = this.totalRecords - this.customer_outputs.length;
      } else if (!this.check_status && this.customer_outputs.length === 0) {
        this.totalSelects = 0;
      } else if (!check_status && this.customer_outputs.length > 0) {
        this.totalSelects = this.customer_outputs.length;
      }
  }
  // // docaohuynh
  // public getDataCheck() {
  //   // check if check all
  //   if (this.loadingdata) {
  //     this.loadingdata = false;
  //   } else {
  //     if (this.selectedItems && this.selectedItems.length === this.cars.length) {
  //       this.check_status = true;
  //     } else if (this.selectedItems && this.selectedItems.length === 0) {
  //
  //     }
  //     this.check_status = !this.check_status;
  //   }
  //   // console_bk.log('selectedItems status check 0 ' + this.check_status);
  //
  //   if (this.selectedItems) {
  //     // console_bk.log('selectedItems', this.selectedItems.length + ' status check 0 ' + this.check_status);
  //   } else {
  //     this.selectedItems = [];
  //   }
  //   this.customer_outputs = [];
  //   // - Nếu check all: Gửi query + Gửi tập khách hàng Uncheck - Nếu Uncheck all: Gửi tập khách hàng check
  //   if (this.check_status === true) {
  //     this.customer_outputs = this.cars.filter((row) => {
  //       return this.checkExist(row.id);
  //     });
  //   } else {
  //     this.customer_outputs = this.selectedItems;
  //   }
  //   this.log.info('check_statusl:' + this.check_status);
  //   this.log.info('customer_outputs:' + this.customer_outputs);
  //   if (this.check_status && this.customer_outputs.length === 0) {
  //     this.totalSelects = this.totalRecords;
  //   } else if (this.check_status && this.customer_outputs.length > 0) {
  //     this.totalSelects = this.totalRecords - this.customer_outputs.length;
  //   } else if (!this.check_status && this.customer_outputs.length === 0) {
  //     this.totalSelects = 0;
  //   } else if (!this.check_status && this.customer_outputs.length > 0) {
  //     this.totalSelects = this.customer_outputs.length;
  //   }
  // }

  public checkExist(id) {
    let resulst = this.selectedItems.filter((row) => {
      return row.id === id;
    });
    if (resulst.length === 0) {
      return true;
    } else {
      return false;
    }
  }

  public filter(data) {
    this.queries = data;
    this.search(0, this.pageSize, true);
  }

  public filterv2(data) {
    this.selectedItems = [];
    this.getDataCheck();
    // // console_bk.log('data output query_filter', JSON.stringify(data.query_filter));
    this.queries = data.query_filter;
    this.search_filter(0, this.pageSize, true);
  }

  public search_filter(currentItem, pageSize, notCount) {
        let options: any = {};
        options.app_id = this.appId;
        options.last_item = currentItem;
        options.page_size = pageSize;
        options.queries = this.queries;
        options.full_name = this.keySearch;
        // console_bk.log('options', options);
        if (notCount) {
            this.getTotalCustomer(options);
        }
        this.getListServiceFilter(options);
  }

    public getListServiceFilter(body) {
        // alert('request server');
        this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.datasource = [];
        this.cars = [];
        let actionOperate: Observable<any>;
        body.has_fb_userchat = true; // chi lay kh co userchat
        actionOperate = this.facebookService.getListCustomer(body);
        actionOperate.subscribe(
            (services) => {
                // alert(JSON.stringify(services));
                // this.datasource = services;
                // this.totalRecords = this.datasource.length;
                // this.cars = services;

                // this.datasource = services;
                // this.totalRecords = services.length;
                // this.cars = this.datasource.slice(0, this.pageSize);
                this.cars = services;
                if (this.cars.length < 1) {
                    this.tableMessage = 'Không có dữ liệu';
                }
                $('#click_all').prop('checked', false);
                this.checkAll(false);
            },
            (err) => {
                this.log.info(err);
            });
    }

  public checked(carValue) {

    if (this.check_status) {
      if (!carValue.status) {
        this.selectedItems.push(carValue);
      } else {
        this.selectedItems = _.filter(this.selectedItems, (item) => {
          return item.id !== carValue.id;
        });
      }
    } else {
      if (carValue.status) {
        this.selectedItems.push(carValue);
      } else {
        this.selectedItems = _.filter(this.selectedItems, (item) => {
          return item.id !== carValue.id;
        });
      }
    }


    console.log('this.selectedItems.length', this.selectedItems.length);
    console.log('this.totalRecords', this.totalRecords);

    /** @Tam them vao */
      if (this.selectedItems.length === this.totalRecords || this.selectedItems.length == 0) {
          let check_all = true;
          let check_un_check_all = true;
          this.selectedItems.filter((row) => {
              if (row.status == false) {
                  check_all = false;
              }
              if (row.status == true) {
                  check_un_check_all = false;
              }
          });
          if (check_all) {
              $('#click_all').prop('checked', true);
              this.checkAll(true);
          }
          if (this.selectedItems.length === this.totalRecords && check_un_check_all) {
              $('#click_all').prop('checked', false);
              this.checkAll(false);
          }
      } else {
          $('#click_all').prop('checked', false);
      }
    /** End @Tam them vao */

    this.getDataCheck();
  }
  public checkAll(event) {
    _.each(this.cars, (item) => {
      if (event) {
        item.status = true;
      } else {
        item.status = false;
      }
    });

    if (event) {
      this.check_status = true;
    } else {
      this.check_status = false;
    }
    this.selectedItems = [];

    this.getDataCheck();
  }

}
