import { HashtagsComponent } from './components/hashtags/hashtags.component';
import { AuthGuard } from './../../_guards/auth.guard';
import { CheckIntegrationFacebookComponent } from './components/growtools/check-integration-facebook/check-integration-facebook.component';
import { CheckFacebookLoginComponent } from './components/growtools/check-facebook-login/check-facebook-login.component';
import { CustomerGrowListComponent } from './components/growtools/customer-list/customer-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { CustomerListComponent } from './components/customers/customer-list/customer-list.component';
import { TrainingFaqComponent } from './components/training-faq/training-faq.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { BlocksV2Component } from './components/blocks-v2/blocks-v2.component';
import { PostTrainingComponent } from './components/post-training/post-training.component';
import { SettingMenusComponent } from './components/setting-menus/setting-menus.component';
import { CommentsComponent } from './components/comments/comments.component';
import { OrdersComponent } from './components/orders/orders.component';
import { TrainingHanaComponent } from './training-hana.component';

const routes: Routes = [
  {
    path: '',
    component: TrainingHanaComponent,
    children: [
      { path: 'customers', component: CustomerListComponent, canActivate: [AuthGuard] },
      { path: 'faq', component: TrainingFaqComponent, canActivate: [AuthGuard] },
      { path: 'blocksv2', component: BlocksV2Component, canActivate: [AuthGuard] },
      { path: 'campaigns', component: CampaignsComponent, canActivate: [AuthGuard] },
      { path: 'growthtools', component: CustomerGrowListComponent, canActivate: [AuthGuard] },
      { path: 'connect-facebook', component: CheckFacebookLoginComponent, canActivate: [AuthGuard] },
      { path: 'connect-integration', component: CheckIntegrationFacebookComponent, canActivate: [AuthGuard] },
      { path: 'post', component: PostTrainingComponent, canActivate: [AuthGuard] },
      { path: 'setting-menu', component: SettingMenusComponent },
      { path: 'hashtags', component: HashtagsComponent, canActivate: [AuthGuard] },
      { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard] },
    ]
  },
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
