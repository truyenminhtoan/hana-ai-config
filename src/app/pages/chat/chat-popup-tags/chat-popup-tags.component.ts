import { Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { ValidationService } from '../../../_services/ValidationService';
import { ColorPickerService } from 'angular2-color-picker';
import { ConfigService } from '../../../_services/config.service';
import { NotificationService } from '../../../_services/notification.service';
import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef
} from '@angular/core';

declare var window: any;
declare var $: any;

@Component({
    selector: 'chat-popup-tags',
    templateUrl: 'chat-popup-tags.component.html',
    styleUrls: ['chat-popup-tags.component.scss']
})
export class ChatPopupTagsComponent {
    @Input() partnerId: string;
    @Input() listTags = [];
    @Input() subscription: any;

    private color: string = '#127bdc';


    constructor(private router: Router, private applicationService: ApplicationService, private notificationService: NotificationService,
        private validationService: ValidationService, private cdRef: ChangeDetectorRef, private cpService: ColorPickerService, private serviceConfig: ConfigService) {
        // $.getScript('../assets/js/jscolor.min.js');
      }


    private deleteTag(event, id, deleted) {
        // console_bk.log('DELTE deleteTag');
        if (deleted) {
          this.listTags = this.listTags.filter((item) => item.delele !== deleted);
        } else {
          this.listTags = this.listTags.filter((item) => item.id !== id);
        }
        this.cdRef.detectChanges();
      }
      private addTagss() {
        // console_bk.log('add addTag ' + JSON.stringify(this.listMember));
        this.listTags.push({ id: -1, color: this.color, name: '', delele: this.listTags.length + 1 });
        this.cdRef.detectChanges();
      }
    
      private saveTag() {
        // console_bk.log('save saveTag');
        // console_bk.log(JSON.stringify(this.listTags));
        let body = {
          partner_id: this.partnerId,
          tags: this.listTags
        };
        this.applicationService.saveTagsByPartnerId(body)
          .then((agents) => {
            if (agents) {
              // this.notificationService.showSwal('Thành công');
              let tagaction = {
                  type: 'addtag',
                  room: this.subscription.room,
                  data: {
                  }
              };
              window.converse.actionglobal.actionListener(tagaction);
              this.notificationService.showSuccessTimer('Thành công', 3000);
              $('#createQuickTemplateModel').modal('hide');
            }
          });
    
      }


}
