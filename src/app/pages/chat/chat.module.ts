// Angular Imports
import { NgModule } from '@angular/core';
import { ApplicationService } from '../../_services/index';
import { ChatComponent } from './chat.component';
import { CommonModule } from '@angular/common';
import { routing } from './chat.routing';

@NgModule({
    imports: [
        CommonModule,
        routing
    ],
    declarations: [
        ChatComponent
    ],
    exports: [
        ChatComponent
    ],
    providers: [
        ApplicationService
    ],
})
export class ChatModule {

}
