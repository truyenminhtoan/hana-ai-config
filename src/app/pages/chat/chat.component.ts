import { Component, AfterViewInit, OnInit, ElementRef, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';
import { ConfigService } from '../../_services/config.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../_services/application.service';
import { NotificationService } from '../../_services/notification.service';
declare var $: any;
declare var _: any;
declare var require: any;
declare var window: any;
@Component({
  selector: 'chat',
  templateUrl: 'chat.component.html',
  styleUrls: ['./assets/css/material-dashboard.css', 'chat.component.scss']
})
export class ChatComponent {
  private currentUser: any;
  private partnerId: any;
  private listTags = [];
  private subscription: any;
  constructor(private router: Router) {
    this.checkRedirect();
  }

  public checkRedirect() {
    console.log('REDIRECT');
    this.router.navigate(['/chat-v2']);
  }
}
