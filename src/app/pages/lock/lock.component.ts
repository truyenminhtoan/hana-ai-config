import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { UserModel } from '../../_models/user.model';
import { Observable } from 'rxjs/Observable';
declare var $: any;
@Component({
  selector: 'lock',
  templateUrl: 'lock.component.html',
  styleUrls: ['lock.component.scss']
})
export class LockComponent implements OnInit, AfterViewInit {
  private currentUser: UserModel;
  private returnUrl: any;
  constructor(private userService: UserService, 
              private route: ActivatedRoute, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  public ngAfterViewInit() {
    $.getScript('../../../assets/js/login.js');
  }

  public ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  public getLogin(pwd: string) {
    let actionOperate: Observable<UserModel>;
    let user = { username: this.currentUser.username, password: pwd };
    actionOperate = this.userService.getLogin(user);
    actionOperate.subscribe(
      (data) => {
        data.is_lock = false;
        localStorage.setItem('currentUser', JSON.stringify(data));
        this.router.navigate([this.returnUrl]);
      },
      (err) => {
        console.log(err);
        // load event fail
      });
  }
}
