// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { LockComponent } from './lock.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './lock.routing';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      routing
    ],
    declarations: [
        LockComponent,
    ],
    exports: [
        LockComponent,
    ]
})
export class LockModule {

}
