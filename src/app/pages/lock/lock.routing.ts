import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LockComponent } from './lock.component';
import { AuthGuard } from '../../_guards/auth.guard';

const routes: Routes = [
  { path: '', component: LockComponent},
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
