import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { GuideAdvisoryComponent } from './guide-advisory.component';

const routes: Routes = [
  { path: '', component: GuideAdvisoryComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
