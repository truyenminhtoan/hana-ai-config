import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { RegisterHanaComponent } from './register-hana.component';

const routes: Routes = [
  { path: '', component: RegisterHanaComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
