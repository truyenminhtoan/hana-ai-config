import { Component, AfterViewInit, OnInit, NgZone } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { UserModel } from '../../_models/user.model';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../_services/notification.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserLoginModel } from '../../_models/userlogin.model';
import { ValidationService } from '../../_services/ValidationService';
import { ConfigService } from '../../_services/config.service';
import { ApplicationService } from '../../_services/application.service';
declare var $: any;
declare var _: any;
declare var gapi: any;
declare var swal: any;
declare const FB: any;
@Component({
  selector: 'register-hana',
  templateUrl: 'register-hana.component.html',
  styleUrls: ['register-hana.component.scss']
})
export class RegisterHanaComponent implements OnInit, AfterViewInit {
  // tslint:disable-next-line:max-line-length
  private hasErrorMsg: boolean;
  private errorMsg: string;
  private username: string;
  private password: string;
  private auth2: any;
  private typeLogin: string;
  private currentUser: any;
  private userInfo: any = {};
  private loadingProcess: boolean = false;
  private color = 'red';
  private mode = 'indeterminate';
  private value = 50;
  private bufferValue = 75;
  private submit = false;
  private complexForm: FormGroup;
  constructor(private applicationService: ApplicationService, private userService: UserService, private validationService: ValidationService, private route: ActivatedRoute, private zone: NgZone,
    private router: Router, private notify: NotificationService, private serviceConfig: ConfigService, private fb: FormBuilder) {
    this.complexForm = fb.group({
      name: [null, Validators.required],
      username: [null, Validators.required],
      phone: [null, Validators.required],
      password: [null, Validators.required],
      passwordconfirm: [null, Validators.required]
    });

    this.checkLoged();
  }
  public ngAfterViewInit() {
    $.getScript('../../../assets/js/login.js');
    $.getScript('../../../assets/js/popup-hana.js');
    this.sigin();
  }

  public ngOnInit() {
    this.hasErrorMsg = false;
    this.route
      .queryParams
      .subscribe((params) => {
        let paramT = params['t'];
        let affiliate = params['ref'];
        if (affiliate) {
          localStorage.setItem('affiliate', affiliate);
        }
        if (paramT === 'advance') {
          localStorage.setItem('registerFrom', paramT);
        } else if (paramT === 'basic') {
          localStorage.setItem('registerFrom', paramT);
        } else if (paramT === 'special') {
          localStorage.setItem('registerFrom', paramT);
        }
      });

    FB.init({
      appId: this.serviceConfig.facebookConfig.client_id,
      // appId: '108207942999174',
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
  }
  public submitForm(value: any) {
    // console_bk.log(value);
    if (this.submit) {
      return;
    }
    this.submit = true;
    // let user = new UserModel();
    let username = value.username ? value.username.trim() : '';
    let name = value.name ? value.name.trim() : '';
    let phone = value.phone ? value.phone.trim() : '';
    let password = value.password ? value.password.trim() : '';
    let passwordconfirm = value.passwordconfirm ? value.passwordconfirm.trim() : '';
    // console_bk.log(JSON.stringify(value));
    if (name === '') {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập tên';
      this.submit = false;
      return;
    }

    if (username === '') {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập email';
      this.submit = false;
      return;
    }

    if (password === '') {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập mật khẩu';
      this.submit = false;
      return;
    }

    if (passwordconfirm === '') {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập lại mật khẩu';
      this.submit = false;
      return;
    }

    if (!this.validationService.validateEmail(username)) {
      this.hasErrorMsg = true;
      this.errorMsg = 'Vui lòng nhập đúng email';
      this.submit = false;
      return;
    }

    if (password !== passwordconfirm) {
      this.hasErrorMsg = true;
      this.errorMsg = 'Mật khẩu không trùng khớp';
      this.submit = false;
      return;
    }

    let body = {
      username: username, password: password, full_name: name,
      email: username, type_login: 'email', phone: phone,
      register_from: localStorage.getItem('registerFrom'),
      affiliate: localStorage.getItem('affiliate')
    };
    this.userService.createBackendUsername(body)
      .then((usersRes) => {
        this.submit = false;
        if (usersRes) {
          this.submit = false;
          this.hasErrorMsg = false;
          // console_bk.log('LOGIN WITH SOCIAL ' + JSON.stringify(usersRes));
          this.showSwal();
        } else {
          this.hasErrorMsg = true;
          this.errorMsg = 'Tài khoản đã tồn tại';
        }
      });

    /* if (localStorage.getItem('affiliate')) {
      // goi api affiliate
      let bodyAff = {
        name: name,
        email: username,
        ref: localStorage.getItem('affiliate'),
        phone: phone
      };
      this.userService.createAffiliateUser(bodyAff)
        .then((usersRes) => {
        });
    } */

  }

  /**
   * LOGIN GOOGLE
   */
  public sigin() {
    let that = this;
    gapi.load('auth2', () => {
      that.auth2 = gapi.auth2.init({
        client_id: this.serviceConfig.googleConfig.client_id,
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      that.attachSignin(document.getElementById('my-signin-hana'));
    });
  }

  public attachSignin(element) {
    let that = this;
    this.zone.run(() => {
      that.getUserInfoGoogle(element);
    });
  }

  public getUserInfoGoogle(element) {
    let that = this;
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        this.loadingProcess = true;
        let profile = googleUser.getBasicProfile();
        this.userInfo.token = googleUser.getAuthResponse().id_token;
        this.userInfo.id = profile.getId();
        this.userInfo.name = profile.getName();
        this.userInfo.img = profile.getImageUrl();
        localStorage.removeItem('accessToken');
        this.findOrAdd(profile.getEmail(), profile.getName(), profile.getImageUrl(), profile.getEmail(), googleUser.getAuthResponse().id_token, 'email');
      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  // toantm: login or register social
  public findOrAdd(username: string, fullName: string, img: string, email: string, accessToken: string, source: string) {
    this.zone.run(() => {
      let user = new UserLoginModel();
      user.full_name = fullName;
      user.username = username;
      user.url_avatar = img;
      user.email = email ? email : '';
      user.access_token = accessToken ? accessToken : '';
      user.type_login = source;
      if (localStorage.getItem('registerFrom')) {
        user.register_from = localStorage.getItem('registerFrom');
      }
      if (localStorage.getItem('affiliate')) {
        user.affiliate = localStorage.getItem('affiliate');
      }
      let that = this;
      this.userService.loginWithSocial(user)
        .then((usersRes) => {
          if (usersRes) {
            usersRes.login_type = source;
            localStorage.setItem('currentUser', JSON.stringify(usersRes));
            this.applicationService.getListPartnerByUsername(username)
              .then((apps) => {
                if (apps) {
                  // console_bk.log('LIST AP length ' + apps.length);
                  if (apps.length === 0) {
                    if (source === 'facebook') {
                      this.router.navigate(['/application-hana/application-list']);
                    } else {
                      this.router.navigate(['/application-hana/application-create']);
                    }
                  } else if (apps.length === 1) {
                    usersRes.apps = apps;
                    localStorage.setItem('currentUser', JSON.stringify(usersRes));
                    this.usingApp(apps, fullName, username);
                  } else {
                    this.router.navigate(['/application-hana/application-list']);
                  }
                }
              });
            // return this.router.navigate(['/application-hana/application-redirect']);
            if (usersRes.status === 200) {
              /* if (localStorage.getItem('affiliate')) {
                let bodyAff = {
                  name: fullName,
                  email: email,
                  ref: localStorage.getItem('affiliate'),
                };
                this.userService.createAffiliateUser(bodyAff)
                  .then((usersRes) => {
                  });
              } */
            }
          } else {
            this.hasErrorMsg = true;
            this.errorMsg = 'Thông tin đăng nhập không chính xác';
          }
        });

      /* if (localStorage.getItem('affiliate')) {
        // goi api affiliate
        let bodyAff = {
          name: fullName,
          email: email,
          ref: localStorage.getItem('affiliate'),
        };
        this.userService.createAffiliateUser(bodyAff)
          .then((usersRes) => {
          });
      } */
    });
  }

  /**
   * FACEBOOK LOGIN
   */
  public onFacebookLoginClick() {
    this.loadingProcess = true;
    FB.login((response) => {
      if (response.authResponse) {
        FB.api('/me', { fields: 'id,name,email' }, (res) => {
          localStorage.setItem('accessToken', response.authResponse.accessToken);
          this.findOrAdd(res.id, res.name, 'https://graph.facebook.com/' + res.id + '/picture?width=150&height=150', res.email, response.authResponse.accessToken, 'facebook');
          return;
        });
      }
    }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
  }

  private usingApp(app: any, fullname: any, username: any) {
    this.zone.run(() => {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        currentUser.version = app.version;
        currentUser.partner_id = app.id;
        // // console_bk.log('app:', app);
        currentUser.app_using = app[0];
        currentUser.apps = app;
        if (currentUser.role_id !== 1) {
          let user = app[0].backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
          if (user) {
            currentUser.role_id = user.role_id;
          }
        }
        localStorage.setItem('currentUser', JSON.stringify(currentUser));
        this.currentUser = currentUser;
        // console.log('USSSSSSSSSSSSSSS ' + JSON.stringify(this.currentUser));
      }
      this.getTryUsing();
      this.router.navigate(['/' + this.currentUser.app_using.id + '/messages']);
    });
  }

  private getTryUsing() {
    let integrations = this.currentUser.app_using.integrations;
    let fbInte = _.find(integrations, (inte) => {
      return inte.gatewayId === 1;
    });
    if (fbInte) {
      if (fbInte.isExpire === 0) {
        let body = {
          page_id: fbInte.pageId,
          partner_id: this.currentUser.partner_id
        }
        this.applicationService.getTryUsing(body)
          .then((apps) => {

            // console.log('paypay ' + JSON.stringify(apps));
            this.currentUser.payment = apps.data;
            this.currentUser.paymentstatus = false;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          });
      }
    }
  }
  private checkLoged() {
    // console_bk.log('CURRRENT ' + localStorage.getItem('currentUser'));
    if (localStorage.getItem('currentUser') && localStorage.getItem('currentUser') !== 'undefined') {
      // this.router.navigate(['/application-hana/application-redirect']);
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.applicationService.getListPartnerByUsername(currentUser.username)
        .then((apps) => {
          if (apps) {
            // console_bk.log('LIST AP length ' + apps.length);
            if (apps.length === 0) {
              this.router.navigate(['/application-hana/application-create']);
            } else if (apps.length === 1) {
              this.usingApp(apps[0], currentUser.full_name, currentUser.username);
            } else {
              this.router.navigate(['/application-hana/application-list']);
            }
          }
        });
    }
  }

  private clearError() {
    this.hasErrorMsg = false;
  }

  private showSwal() {
    swal({
      type: "success",
      title: "Thành công!",
      text: "Bạn đã đăng ký tài khoản thành công vui lòng kiểm tra email để kích hoạt tài khoản!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success"

    }).then(() => {
      // console_bk.log('Đang nhập');
      this.router.navigate(['/login-hana']);
    }, (dismiss) => {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        // // console_bk.log('giữ nó');
      }
    })
  }

}

