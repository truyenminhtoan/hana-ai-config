// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { RegisterHanaComponent } from './register-hana.component';
import { UserService } from '../../_services/user.service';
import { NotificationService } from '../../_services/notification.service';
import { routing } from './register-hana.routing';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdProgressBarModule } from '@angular/material';
import { ValidationService } from '../../_services/ValidationService';
import { ApplicationService } from '../../_services/application.service';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        MdProgressBarModule
    ],
    declarations: [
        RegisterHanaComponent,
    ],
    providers: [UserService, NotificationService, ValidationService, ApplicationService],
    exports: [
        RegisterHanaComponent,
        MdProgressBarModule
    ]
})
export class RegisterHanaModule {

}
