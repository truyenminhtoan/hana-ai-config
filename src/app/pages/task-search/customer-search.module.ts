// import { FilterStatusPipe } from './../../themes/pipes/filterStatus-pipe';
import { Log } from './../../_services/log.service';
import { PostFilterDialogComponent } from './components/post-training/components/post-filter-dialog/post-filter-dialog.component';
import { HashTagPipe } from './components/hashtags/pipes/hashtags.pipe';
import { HashtagsComponent } from './components/hashtags/hashtags.component';
import { ConfigDrapBotComponent } from './components/config-drap-bot/config-drap-bot.component';
import { CheckIntegrationFacebookComponent } from './components/growtools/check-integration-facebook/check-integration-facebook.component';
import { CheckFacebookLoginComponent } from './components/growtools/check-facebook-login/check-facebook-login.component';
import { SendMessageGrowDialogComponent } from './components/growtools/components/send-message-dialog/send-message-dialog.component';
import { HistoryMessageGrowDialogComponent } from './components/growtools/components/history-message-dialog/history-message-dialog.component';
import { CustomerGrowListComponent } from './components/growtools/customer-list/customer-list.component';
// Angular Imports
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
// This Module's Components
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { routing } from './customer-search.routing';
import { NgaModule } from '../../themes/nga.module';
import { MdProgressSpinnerModule, MdDialogModule, MdAutocompleteModule, MdPaginatorModule, MatChipsModule, MatSlideToggleModule, MatButtonToggleModule, MatPaginatorModule, MatButtonModule, MatDialogModule, MatInputModule, MatAutocompleteModule, MatProgressSpinnerModule, MatIconModule, MatSelectModule, MatDatepickerModule, MdNativeDateModule } from '@angular/material';
import { CustomerListComponent } from './components/customers/customer-list/customer-list.component';
import { TrainingFaqComponent } from './components/training-faq/training-faq.component';
import { DragDropModule, DataTableModule, OrderListModule, SharedModule, ConfirmDialogModule } from 'primeng/primeng';
import { TrainingFaqPipe } from './components/training-faq/pipes/training-faq.pipe';
import { ComfirmDeleteDialogComponent } from './components/training-faq/components/comfirm-delete-dialog/comfirm-delete-dialog.component';
import { TrainingDontUnderstandComponent } from './components/training-faq/components/training-dont-understand/training-dont-understand.component';
import { TrainingDontUnderstandDialogComponent } from './components/training-faq/components/training-dont-understand-dialog/training-dont-understand-dialog.component';
import { TrainingInboxComponent } from './components/training-faq/components/training-inbox/training-inbox.component';
import { TrainingCommentComponent } from './components/training-faq/components/training-comment/training-comment.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { SelectButtonModule, OverlayPanelModule, InputTextModule, MultiSelectModule } from 'primeng/primeng';
import { RlTagInputModule } from 'angular2-tag-input';
import { TagInputModule } from 'ng2-tag-input';
import { GroupsDialogComponent } from './components/customers/components/groups-dialog/groups-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { SendMessageDialogComponent } from './components/customers/components/send-message-dialog/send-message-dialog.component';
import { MomentModule } from 'angular2-moment';
import { CampaignsSendNowComponent } from './components/campaigns/components/campaigns-send-now/campaigns-send-now.component';
import { CampaignsSequenceComponent } from './components/campaigns/components/campaigns-sequence/campaigns-sequence.component';
import { CampaignsScheduleComponent } from './components/campaigns/components/campaigns-schedule/campaigns-schedule.component';
// import { BlockDialogComponent } from './components/blocks/block-dialog/block-dialog.component';
import { CalendarModule, InputTextareaModule } from 'primeng/primeng';
import { CampaignsHistoryComponent } from './components/campaigns/components/campaigns-history/campaigns-history.component';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { BlocksV2Component } from './components/blocks-v2/blocks-v2.component';
import { BlockItemsComponent } from './components/blocks-v2/components/block-items/block-items.component';
import { ResCardComponent } from './components/blocks-v2/components/block-items/components/res-card-component/res-card-component.component';

import { CampaignService } from '../../_services/campaign.service';
import { BlocksService } from '../../_services/blocks.service';
import { FacebookService } from '../../_services/facebook.service';
import { NotificationService } from '../../_services/notification.service';
import { ApplicationService } from '../../_services/application.service';
import { IntentService } from '../../_services/intents.service';
import { CampaignsSequenceDetailComponent } from './components/campaigns/components/campaigns-sequence-detail/campaigns-sequence-detail.component';
import { SequenceDialogComponent } from './components/customers/components/sequence-dialog/sequence-dialog.component';
import { HistoryMessageDialogComponent } from './components/customers/components/history-message-dialog/history-message-dialog.component';
import { Autosize } from 'angular2-autosize';
import '../../../styles/styles_remaketing.scss';
import { PaginatorModule } from 'primeng/primeng';
import { MatCheckboxModule } from '@angular/material';
import { PostTrainingComponent } from './components/post-training/post-training.component';
import { PostTrainingListComponent } from './components/post-training/post-training-list/post-training-list.component';
import { FacebookPostService } from '../../_services/fbpost.service';
import { DropdownModule } from 'primeng/primeng';
import { ValidationService} from '../../_services/ValidationService';
import { AddCompanyDialogComponent } from './components/add-company/add-company.component';
import { EditTaskSearchDialogComponent } from './components/edit/edit.component';
import { AddTaskSearchDialogComponent } from './components/add/add.component';
import { TaskSearchService } from '../../_services/task-search.service';
import { TaskSearchComponent } from './customer-search.component';
import {CustomerSearchService} from "../../_services/custommer-search";

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgaModule,
        routing,
        TagInputModule,
        OverlayPanelModule,
        DragDropModule,
        DataTableModule,
        OrderListModule,
        SharedModule,
        SelectButtonModule,
        InputTextModule,
        MomentModule,
        MultiSelectModule,
        ConfirmDialogModule,
        CalendarModule,
        InputTextareaModule,
        DateTimePickerModule,
        MatChipsModule,
        MatSlideToggleModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatPaginatorModule,
        MatDialogModule,
        MatInputModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatSelectModule,
        MatDatepickerModule,
        MdNativeDateModule,
        MatCheckboxModule,
        PaginatorModule,
        MatDatepickerModule,
        DropdownModule
    ],
    declarations: [
        TaskSearchComponent,
        AddTaskSearchDialogComponent,
        EditTaskSearchDialogComponent,
        AddCompanyDialogComponent
    ],
    providers: [
        NotificationService,
        ApplicationService,
        IntentService,
        FacebookService,
        NotificationService,
        CampaignService,
        BlocksService,
        FacebookPostService,
        TaskSearchService,
        Log,
        ValidationService,
        CustomerSearchService,
        { provide: LOCALE_ID, useValue: 'vi-VN' }
    ],
    exports: [
        TaskSearchComponent,
    ],
    bootstrap: [
        AddTaskSearchDialogComponent,
        EditTaskSearchDialogComponent,
        AddCompanyDialogComponent
    ]
})
export class TaskSearchModule {
}
