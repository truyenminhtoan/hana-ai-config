import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import { MdDialog } from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Observable';
import { NotificationService } from '../../../../_services/notification.service';
import { CustomerSearchService } from '../../../../_services/custommer-search';
import { ValidationService } from '../../../../_services/ValidationService';
import {TaskSearchService} from "../../../../_services/task-search.service";
declare var $: any;
declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: 'edit.component.html',
    styleUrls: ['edit.component.scss']
})
export class EditTaskSearchDialogComponent {

    constructor(private dialog: MdDialog, private validationService: ValidationService, private customerSearchService: CustomerSearchService, private taskSearchService: TaskSearchService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < EditTaskSearchDialogComponent > ) {}

    private backend_user_created_id = '';
    private backend_partner_id = '';
    private name = '';
    private categorys = [];
    private category = '';
    private price = '';
    private customers = [];
    private customer = '';
    private id_edit = '';
    private description = '';
    private loadingProcess = false;
    private is_bot_created = false;
    private bot_created_id = '';
    private create_name = '';
    private email = '';
    private phone = '';

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data !== undefined) {
            if (this.data.backend_user_created_id !== undefined) {
                this.backend_user_created_id = this.data.backend_user_created_id;
            }
            if (this.data.partner_id !== undefined) {
                this.backend_partner_id = this.data.partner_id;
            }
            if (this.data.id_edit !== undefined) {
                this.id_edit = this.data.id_edit;
            }
        } else {
            return;
        }
        this.getData();
    }

    private getData()
    {
        this.loadingProcess = true;
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.getDemandCategory({ 'partner_id': this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.categorys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                } else {
                    this.categorys = [];
                }
                let actionOperate2: Observable<any>;
                actionOperate2 = this.customerSearchService.getCustomerNew({ 'partner_id': this.backend_partner_id});
                actionOperate2.subscribe(
                    (services2) => {
                        if (services2.errorCode === 200) {
                            for (let i = 0; i < services2.data.customers.length; i++) {
                                let title_show = '';
                                let phone = '';
                                let email = '';
                                if (services2.data.customers[i].phone !== null && services2.data.customers[i].phone.trim().length > 0) {
                                    title_show += 'Phone: ' + services2.data.customers[i].phone;
                                    phone = services2.data.customers[i].phone;
                                }
                                if (services2.data.customers[i].email !== null && services2.data.customers[i].email.trim().length > 0) {
                                    title_show += '  Email: ' + services2.data.customers[i].email;
                                    email = services2.data.customers[i].email;
                                }

                                this.customers.push(
                                    {
                                        label: services2.data.customers[i].name,
                                        value: services2.data.customers[i].id,
                                        title_show: title_show,
                                        email: email,
                                        phone: phone
                                    }
                                );
                            }
                        }
                        this.getDetail();
                    },
                    (err) => {
                    });
            },
            (err) => {
            });
    }

    public getCompannys() {
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.getDemandCategory({ 'partner_id': this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.categorys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                } else {
                    this.categorys = [];
                }
            },
            (err) => {
            });
    }

    public getCustomerNew() {
        let actionOperate: Observable<any>;
        actionOperate = this.customerSearchService.getCustomerNew({ 'partner_id': this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    for (let i = 0; i < services.data.customers.length; i++) {
                        this.customers.push( {label: services.data.customers[i].name, value: services.data.customers[i].id});
                    }
                }
            },
            (err) => {
            });
    }

    public getDetail() {
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.getDemandDetail({ 'id': this.id_edit.toString()});
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    let data = services.data;
                    this.name = data['name'];
                    this.price = data['price'];
                    this.description = data['description'];
                    this.category = data['demand_category_id'];
                    this.backend_user_created_id = data['backend_user_created_id'];
                    this.backend_partner_id = data['backend_partner_id'];
                    this.customer = data['customer_id'];
                    if ((!data['demand_category_id']) && this.categorys.length > 0) {
                        this.category = this.categorys[0].value;
                    }
                    if (data['bot_created_name'] !== null) {
                        this.create_name = 'Bot';
                        this.is_bot_created = true;
                        this.bot_created_id = data['bot_created_id'];
                    } else {
                        this.create_name = data['creater'];
                    }
                    this.onChangeKH();
                } else {
                    this.notificationService.showDanger('Lỗi khi lấy dữ liệu chi tiết nhu cầu!');
                }
                this.loadingProcess = false;
            },
            (err) => {
                this.notificationService.showDanger('Lỗi khi lấy dữ liệu chi tiết nhu cầu!');
            });
    }


    public okClick() {
        if (this.name.trim().length === 0) {
            this.notificationService.showDanger('Tên nhu cầu không được bỏ trống!');
            return;
        }
        // if (!this.customer) {
        //     this.notificationService.showDanger('Bạn chưa chọn khách hàng!');
        //     return;
        // }
        let body = {
            'name': this.name.trim(),
            //'backend_user_created_id': this.backend_user_created_id,
            'backend_partner_id': this.backend_partner_id,
            'category_id': this.category,
            'customer_id': this.customer,
            'price': this.price,
            'description': this.description.trim(),
            'demand_category_id': this.category,
            'id': this.id_edit,
        };
        if (this.is_bot_created) {
            //body['bot_created_id'] = this.bot_created_id;
        } else {
            body['backend_user_created_id'] = this.backend_user_created_id;
        }
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.updateDemand(body);
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    this.notificationService.showSuccess('Cập nhật nhu cầu thành công!');
                    this.dialogRef.close(body);
                } else {
                    this.notificationService.showSuccess('Cập nhật nhu cầu thất bại!');
                }
            },
            (err) => {
                this.notificationService.showSuccess('Cập nhật nhu cầu thất bại!');
            });
    }

    private onChangeKH() {
        this.phone = '';
        this.email = '';
        if (this.customers.length > 0) {
            for (let i = 0 ; i < this.customers.length; i++) {
                if (this.customers[i].value === this.customer) {
                    this.phone = this.customers[i].phone;
                    this.email = this.customers[i].email;
                    break;
                }
            }
        }
    }

}