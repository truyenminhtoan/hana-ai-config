import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import { MdDialog } from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {Observable} from 'rxjs/Observable'
import {NotificationService} from '../../../../_services/notification.service';
import {CustomerSearchService} from '../../../../_services/custommer-search';
import {ValidationService} from '../../../../_services/ValidationService';
import {AddCompanyDialogComponent} from '../add-company/add-company.component';
import {TaskSearchService} from '../../../../_services/task-search.service';
declare var $: any;
declare var swal: any;
@Component({
    selector: 'add',
    templateUrl: 'add.component.html',
    styleUrls: ['add.component.scss']
})
export class AddTaskSearchDialogComponent {

    constructor(private dialog: MdDialog, private validationService: ValidationService,private customerSearchService: CustomerSearchService, private taskSearchService: TaskSearchService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < AddTaskSearchDialogComponent > ) {}

    private backend_user_created_id = '';
    private backend_partner_id = '';
    private name = '';
    private categorys = [];
    private sources = [];
    private category = '';
    private price = '';
    private customers = [];
    private customers_full = [];
    private customer = '';
    private description = '';
    private create_name = '';
    private email = '';
    private phone = '';


    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data !== undefined) {
            if (this.data.backend_user_created_id !== undefined) {
                this.backend_user_created_id = this.data.backend_user_created_id;
            }
            if (this.data.partner_id !== undefined) {
                this.backend_partner_id = this.data.partner_id;
            }
            if (this.data.create_name !== undefined) {
                this.create_name = this.data.create_name;
            }
        }
        this.getCompannys();
        this.getCustomerNew();
    }

    public getCompannys() {
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.getDemandCategory({ 'partner_id': this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.categorys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                    if (this.categorys.length > 0) {
                        this.category = this.categorys[0].value;
                    }
                } else {
                    this.categorys = [];
                }
            },
            (err) => {
            });
    }

    public getCustomerNew() {
        let actionOperate: Observable<any>;
        actionOperate = this.customerSearchService.getCustomerNew({ 'partner_id': this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    for (let i = 0; i < services.data.customers.length; i++) {
                        //this.customers.push( {label: services.data.customers[i].name, value: services.data.customers[i].id});
                        let title_show = '';
                        let phone = '';
                        let email = '';
                        if (services.data.customers[i].phone !== null && services.data.customers[i].phone.trim().length > 0) {
                            title_show += 'Phone: ' + services.data.customers[i].phone;
                            phone = services.data.customers[i].phone;
                        }
                        if (services.data.customers[i].email !== null && services.data.customers[i].email.trim().length > 0) {
                            title_show += '  Email: ' + services.data.customers[i].email;
                            email = services.data.customers[i].email;
                        }
                        this.customers.push(
                            {
                                label: services.data.customers[i].name,
                                value: services.data.customers[i].id,
                                title_show: title_show,
                                email: email,
                                phone: phone
                            }
                        );
                    }
                }
            },
            (err) => {
            });
    }
    

    public okClick() {
        if (this.name.trim().length === 0) {
            this.notificationService.showDanger('Tên nhu cầu không được bỏ trống!');
            return;
        }
        // if (!this.customer) {
        //     this.notificationService.showDanger('Bạn chưa chọn khách hàng!');
        //     return;
        // }
        let body = {
            'name': this.name.trim(),
            'backend_user_created_id': this.backend_user_created_id,
            'backend_partner_id': this.backend_partner_id,
            'category_id': this.category,
            'customer_id': this.customer,
            'price': this.price,
            'description': this.description.trim()
        };
        let actionOperate: Observable<any>;
        actionOperate = this.taskSearchService.createDemand(body);
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    this.notificationService.showSuccess('Thêm nhu cầu thành công!');
                    this.dialogRef.close(body);
                } else {
                    this.notificationService.showSuccess('Thêm nhu cầu thất bại!');
                }
            },
            (err) => {
                this.notificationService.showSuccess('Thêm nhu cầu thất bại!');
            });
    }

    private addCompany() {
        // let dialogRef = this.dialog.open(AddCompanyDialogComponent, {
        //     width: '50%',
        //     data: {
        //         'partner_id': this.backend_partner_id,
        //         'backend_user_created_id': this.backend_user_created_id
        //     }
        // });
        // dialogRef.afterClosed().subscribe(data => {
        //     if (data) {
        //
        //     }
        // });
    }


    private onChangeKH() {
        this.phone = '';
        this.email = '';
        if (this.customers.length > 0) {
            for (let i = 0 ; i < this.customers.length; i++) {
                if (this.customers[i].value === this.customer) {
                    this.phone = this.customers[i].phone;
                    this.email = this.customers[i].email;
                    break;
                }
            }
        }
    }

}