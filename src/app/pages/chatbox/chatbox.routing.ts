import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ChatboxComponent } from './chatbox.component';

const routes: Routes = [
  { path: '', component: ChatboxComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
