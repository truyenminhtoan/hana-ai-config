// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { ChatboxComponent } from './chatbox.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './chatbox.routing';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      routing
    ],
    declarations: [
        ChatboxComponent,
    ],
    exports: [
        ChatboxComponent,
    ]
})
export class ChatboxModule {

}
