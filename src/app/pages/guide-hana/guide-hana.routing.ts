import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { GuideHanaComponent } from './guide-hana.component';

const routes: Routes = [
  { path: '', component: GuideHanaComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
