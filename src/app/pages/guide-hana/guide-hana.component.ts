import { Component, AfterViewInit, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

declare var $: any;

@Component({
    selector: 'guide-hana',
    templateUrl: 'guide-hana.component.html',
    styleUrls: ['guide-hana.component.scss']
})
export class GuideHanaComponent implements AfterViewInit, OnInit {
    private safeURL: any;
    private videoURL: string = 'https://www.youtube.com/watch?v=lt8d7FIrWoI';
    private id: string = 'v1L62mC2TGw';
    constructor(private _sanitizer: DomSanitizer, private router: Router) {
        this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
    }

    public ngOnInit() {
        let h = $(window).height();
        $('.guide-scroll').slimScroll({
            height: h - 60 + 'px'
        });
        let current = localStorage.getItem('currentUser');
        if (current && current !== 'undefined') {
            // console.log('');
        } else {
            this.router.navigate(['/login-hana']);
        }
    }

    public ngAfterViewInit() {
        $('.dropdown-toggle').dropdown();
    }
}