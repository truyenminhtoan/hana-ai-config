// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { CommonModule } from '@angular/common';
import { YoutubePlayerModule } from 'ng2-youtube-player';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../themes/nga.module';
import { routing } from './guide-hana.routing';
import { GuideHanaComponent } from './guide-hana.component';


@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        NgaModule,
        YoutubePlayerModule
    ],
    declarations: [
        GuideHanaComponent,
    ],
    exports: [
        GuideHanaComponent,
    ]
})
export class GuideHanaModule {

}
