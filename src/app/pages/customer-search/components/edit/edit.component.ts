import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import { MdDialog } from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {Observable} from 'rxjs/Observable'
import {NotificationService} from '../../../../_services/notification.service';
import {CustomerSearchService} from "../../../../_services/custommer-search";
import {ValidationService} from "../../../../_services/ValidationService";
import {AddCompanyDialogComponent} from "../add-company/add-company.component";
declare var $: any;
declare var swal: any;

@Component({
    selector: 'edit',
    templateUrl: 'edit.component.html',
    styleUrls: ['edit.component.scss']
})
export class EditCustommerSearchDialogComponent {

    constructor(private dialog: MdDialog, private validationService: ValidationService, private custommerService: CustomerSearchService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < EditCustommerSearchDialogComponent > ) {}

    private cus_username = '';
    private backend_user_created_id = '';
    private backend_partner_id = '';
    private name = '';
    private department = '';
    private position = '';
    private email = '';
    private phone = '';
    private phoneposible = '';
    private note = '';
    private address = '';
    private company = '';
    private source  = '';
    private companys = [];
    private sources = [];
    private id_edit = '';
    private create_name = '';
    private is_bot_created = false;
    private bot_created_id = '';

    private loadingProcess = false;


    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data !== undefined) {
            if (this.data.backend_user_created_id !== undefined) {
                this.backend_user_created_id = this.data.backend_user_created_id;
            }
            if (this.data.partner_id !== undefined) {
                this.backend_partner_id = this.data.partner_id;
            }
            if (this.data.id_edit !== undefined) {
                this.id_edit = this.data.id_edit;
            }
        } else {
            return;
        }
        this.getData();
    }

    public getData() {
        this.loadingProcess = true;
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCompanyeCustomer({ "partner_id": this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.companys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                }
                let actionOperate2: Observable<any>;
                actionOperate2 = this.custommerService.getSourceCustomer({});
                actionOperate2.subscribe(
                    (services2) => {
                        if (services2.data != '') {
                            for (let i = 0; i < services2.data.length; i++) {
                                this.sources.push( {label: services2.data[i].name, value: services2.data[i].id});
                            }
                            if (this.sources.length > 0) {
                                this.source = this.sources[0].value;
                            }
                        }
                        this.getDetail();
                    },
                    (err) => {
                    });

            },
            (err) => {
            });
    }

    public getDetail() {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCustomerDetail({ "id": this.id_edit});
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    let data = services.data;
                    this.name = data['name'] ? data['name'] : '';
                    this.email = data['email'] ? data['email'] : '';
                    this.phone = data['phone'] ? data['phone'] : '';
                    this.backend_user_created_id = data['backend_user_created_id'] ? data['backend_user_created_id'] : '';
                    this.source = data['source_id'];
                    this.department = data['department'];
                    this.note = data['note'];
                    this.position = data['position'];
                    this.phoneposible = data['phone_posible'];
                    this.backend_partner_id = data['backend_partner_id'];
                    this.company = data['company_id'];
                    this.address = data['address'];
                    if (data['bot_created_name'] !== null) {
                        this.create_name = 'Bot';
                        this.is_bot_created = true;
                        this.bot_created_id = data['bot_created_id'];
                    } else {
                        this.create_name = data['creater'];
                    }
                } else {
                    this.notificationService.showDanger("Lỗi khi lấy dữ liệu chi tiết khách hàng!");
                }
                this.loadingProcess = false;
            },
            (err) => {
                this.notificationService.showDanger("Lỗi khi lấy dữ liệu chi tiết khách hàng!");
            });
    }

    public getCompannys() {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCompanyeCustomer({ "partner_id": this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.companys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                }
            },
            (err) => {
            });
    }

    public getSources() {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getSourceCustomer({});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.sources.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                    if (this.sources.length > 0) {
                        this.source = this.sources[0].value;
                    }
                }
            },
            (err) => {
            });
    }


    public okClick() {
        if (this.name.trim().length === 0) {
            this.notificationService.showDanger("Tên khách hàng không được bỏ trống!");
            return;
        }
        if (this.email.trim().length > 0) {
            if (!this.validationService.validateEmail(this.email.trim())) {
                this.notificationService.showDanger("Email bạn nhập không đúng định dạng!");
                return;
            }
        }
        if (this.phone.trim().length > 0) {
            if (!this.validationService.validatePhone(this.phone.trim())) {
                this.notificationService.showDanger("Số điện thoại bạn nhập không đúng định dạng!");
                return;
            }
        }
        if (this.email.trim().length === 0 && this.phone.trim().length === 0) {
            this.notificationService.showDanger("Vui lòng nhập thông tin số điện thoại hoặc email!");
            return;
        }
        let body = {
            "name": this.name.trim(),
            "backend_partner_id": this.backend_partner_id,
            "phone": this.phone.trim(),
            "email": this.email.trim()
        };
        if (this.is_bot_created) {
            //body['bot_created_id'] = this.bot_created_id;
        } else {
            body['backend_user_created_id'] = this.backend_user_created_id;
        }
        if (this.address) {
            body['address'] = this.address.trim();
        }
        if (this.position) {
            body['position'] = this.position.trim();
        }
        if (this.source) {
            body['source_id'] = this.source;
        }
        if (this.department) {
            body['department'] = this.department.trim();
        }
        if (this.department) {
            body['department'] = this.department.trim();
        }
        if (this.company) {
            body['company_id'] = this.company;
        }
        if (this.id_edit) {
            body['id'] = this.id_edit;
        }
        if (this.note) {
            body['note'] = this.note;
        }

        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.updateCustomer(body);
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    this.notificationService.showSuccess("Cập nhật khách hàng thành công!");
                    this.dialogRef.close(body);
                } else {
                    this.notificationService.showSuccess("Cập nhật khách hàng thất bại!");
                }
            },
            (err) => {
                this.notificationService.showSuccess("Cập nhật khách hàng thất bại!");
            });
    }

    private addCompany() {
        let dialogRef = this.dialog.open(AddCompanyDialogComponent, {
            width: '50%',
            data: {
                'partner_id': this.backend_partner_id,
                'backend_user_created_id': this.backend_user_created_id
            }
        });
        dialogRef.afterClosed().subscribe(id_add => {
            if (id_add) {
                this.getCompannysV2(id_add);
            }
        });
    }

    public getCompannysV2(id_add) {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCompanyeCustomer({ "partner_id": this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data !== '') {
                    this.companys = [];
                    for (let i = 0; i < services.data.length; i++) {
                        this.companys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                    this.company = id_add;
                } else {
                    this.companys = [];
                }
            },
            (err) => {
            });
    }

}