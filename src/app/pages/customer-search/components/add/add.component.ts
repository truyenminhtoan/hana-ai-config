import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import { MdDialog } from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {Observable} from 'rxjs/Observable'
import {NotificationService} from '../../../../_services/notification.service';
import {CustomerSearchService} from "../../../../_services/custommer-search";
import {ValidationService} from "../../../../_services/ValidationService";
import {AddCompanyDialogComponent} from "../add-company/add-company.component";
declare var $: any;
declare var swal: any;
@Component({
    selector: 'add',
    templateUrl: 'add.component.html',
    styleUrls: ['add.component.scss']
})
export class AddCustommerSearchDialogComponent {

    constructor(private dialog: MdDialog, private validationService: ValidationService, private custommerService: CustomerSearchService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < AddCustommerSearchDialogComponent > ) {}

    private cus_username = '';
    private backend_user_created_id = '';
    private backend_partner_id = '';
    private name = '';
    private department = '';
    private position = '';
    private email = '';
    private phone = '';
    private address = '';
    private note = '';
    private company = '';
    private source  = '';
    private companys = [];
    private sources = [];
    private create_name = '';


    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data !== undefined) {
            if (this.data.backend_user_created_id !== undefined) {
                this.backend_user_created_id = this.data.backend_user_created_id;
            }
            if (this.data.partner_id !== undefined) {
                this.backend_partner_id = this.data.partner_id;
            }
            if (this.data.create_name !== undefined) {
                this.create_name = this.data.create_name;
            }
        }
        this.getCompannys();
        this.getSources();
    }

    public getCompannys() {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCompanyeCustomer({ "partner_id": this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    this.companys = [];
                    for (let i = 0; i < services.data.length; i++) {
                        this.companys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                }
            },
            (err) => {
            });
    }

    public getSources() {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getSourceCustomer({});
        actionOperate.subscribe(
            (services) => {
                if (services.data != '') {
                    for (let i = 0; i < services.data.length; i++) {
                        this.sources.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                    if (this.sources.length > 0) {
                        this.source = this.sources[0].value;
                    }
                }
            },
            (err) => {
            });
    }


    public okClick() {
        if (this.name.trim().length === 0) {
            this.notificationService.showDanger("Tên khách hàng không được bỏ trống!");
            return;
        }
        if (this.email.trim().length > 0) {
            if (!this.validationService.validateEmail(this.email.trim())) {
                this.notificationService.showDanger("Email bạn nhập không đúng định dạng!");
                return;
            }
        }
        if (this.phone.trim().length > 0) {
            if (!this.validationService.validatePhone(this.phone.trim())) {
                this.notificationService.showDanger("Số điện thoại bạn nhập không đúng định dạng!");
                return;
            }
        }
        if (this.email.trim().length === 0 && this.phone.trim().length === 0) {
            this.notificationService.showDanger("Vui lòng nhập thông tin số điện thoại hoặc email!");
            return;
        }
        let body = {
            "name": this.name.trim(),
            "backend_user_created_id": this.backend_user_created_id,
            "backend_partner_id": this.backend_partner_id,
            "phone": this.phone.trim(),
            "email": this.email.trim()
        };
        if (this.address) {
            body['address'] = this.address.trim();
        }
        if (this.position) {
            body['position'] = this.position.trim();
        }
        if (this.source) {
            body['source_id'] = this.source;
        }
        if (this.department) {
            body['department'] = this.department.trim();
        }
        if (this.company) {
            body['company_id'] = this.company;
        }
        if (this.note) {
            body['note'] = this.note;
        }
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.CreateCustomer(body);
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    this.notificationService.showSuccess("Thêm khách hàng thành công!");
                    this.dialogRef.close(body);
                } else {
                    this.notificationService.showSuccess("Thêm khách hàng thất bại!");
                }
            },
            (err) => {
                this.notificationService.showSuccess("Thêm khách hàng thất bại!");
            });

        //this.dialogRef.close(this.menu_save);
    }

    private addCompany() {
        let dialogRef = this.dialog.open(AddCompanyDialogComponent, {
            width: '50%',
            data: {
                'partner_id': this.backend_partner_id,
                'backend_user_created_id': this.backend_user_created_id
            }
        });
        dialogRef.afterClosed().subscribe(id_add => {
            if (id_add) {
                this.getCompannysV2(id_add);
            }
        });
    }

    public getCompannysV2(id_add) {
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCompanyeCustomer({ "partner_id": this.backend_partner_id});
        actionOperate.subscribe(
            (services) => {
                if (services.data !== '') {
                    this.companys = [];
                    for (let i = 0; i < services.data.length; i++) {
                        this.companys.push( {label: services.data[i].name, value: services.data[i].id});
                    }
                    this.company = id_add;
                } else {
                    this.companys = [];
                }
            },
            (err) => {
            });
    }



}