import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import {Observable} from 'rxjs/Observable'
import {NotificationService} from '../../../../_services/notification.service';
import {CustomerSearchService} from "../../../../_services/custommer-search";
import {ValidationService} from "../../../../_services/ValidationService";
declare var $: any;
declare var swal: any;
@Component({
    selector: 'add-company',
    templateUrl: 'add-company.component.html',
    styleUrls: ['add-company.component.scss']
})
export class AddCompanyDialogComponent {

    constructor(private validationService: ValidationService, private custommerService: CustomerSearchService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < AddCompanyDialogComponent > ) {}

    private name = '';
    private description = '';
    private fax = '';
    private address = '';
    private backend_user_created_id  = '';
    private backend_partner_id = '';

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        if (this.data !== undefined) {
            if (this.data.backend_user_created_id !== undefined) {
                this.backend_user_created_id = this.data.backend_user_created_id;
            }
            if (this.data.partner_id !== undefined) {
                this.backend_partner_id = this.data.partner_id;
            }
        }
    }

    public okClick() {
        if (this.name.trim().length === 0) {
            this.notificationService.showDanger("Tên công ty không được bỏ trống!");
            return;
        }
        let body = {
            "name": this.name.trim(),
            "backend_user_created_id": this.backend_user_created_id,
            "partner_id": this.backend_partner_id,
        };
        if (this.fax) {
            body['fax'] = this.fax.trim();
        }
        if (this.address) {
            body['address'] = this.address.trim();
        }
        if (this.description) {
            body['description'] = this.description;
        }

        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.createCompanyCustomer(body);
        actionOperate.subscribe(
            (services) => {
                if (services.errorCode === 200) {
                    this.notificationService.showSuccess("Thêm công ty thành công!");
                    this.dialogRef.close(services.data.id);
                } else {
                    this.notificationService.showSuccess("Thêm công ty thất bại!");
                }
            },
            (err) => {
                this.notificationService.showSuccess("Thêm công ty thất bại!");
            });
    }

}