import { HashtagsComponent } from './components/hashtags/hashtags.component';
import { AuthGuard } from './../../_guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { CustomerSearchComponent } from './customer-search.component';

const routes: Routes = [
    {
        path: '',
        component: CustomerSearchComponent
    },
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
