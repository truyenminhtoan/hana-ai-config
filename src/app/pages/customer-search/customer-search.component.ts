import {
    NotificationService
} from './../../_services/notification.service';
import {
    Component,
    AfterViewInit,
    OnInit
} from '@angular/core';
import {
    CustomerSearchService
} from '../../_services/custommer-search';
declare var $: any;
import { Observable } from 'rxjs/Observable';
import { MdDialog } from '@angular/material';
import { LazyLoadEvent } from 'primeng/primeng';
import { AddCustommerSearchDialogComponent } from './components/add/add.component';
import { EditCustommerSearchDialogComponent } from './components/edit/edit.component';
declare var swal: any;
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
type AOA = Array<Array<any>>;
declare var swal: any;
import _ from 'underscore';
function s2ab(s: string): ArrayBuffer {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
    };
    return buf;
}
import { FormControl } from '@angular/forms';

@Component({
    selector: 'customer-search',
    templateUrl: 'customer-search.component.html',
    styleUrls: ['customer-search.component.scss']
})
export class CustomerSearchComponent implements AfterViewInit, OnInit {

    constructor(private custommerService: CustomerSearchService, private dialog: MdDialog, private notificationService: NotificationService) { }

    private partner_id = '';
    private pageSize = 10;
    private lists: any[] = [];
    private totalRecords: number = 0;
    private totalSelects: number = 0;
    private selectedCustomers: any[] = [];
    private tableMessage = 'Không có dữ liệu';
    private fanpageInfo;
    private selectedItems = [];
    private loadingdata = false;

    /** Filter */
    private sname = '';
    private semail = '';
    private smobile = '';
    private suser_created = '';

    private loadingProcess = false;
    /** cau hinh chuc nang doc excel*/
    data: AOA = [];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };
    fileName: string = "SheetJS.xlsx";

    private user_creates = [];
    private user_creates_auto = [];

    countries: any[];
    text: string;
    myControl: FormControl = new FormControl();
    options = [
        'One',
        'Two',
        'Three'
    ];
    filteredCountriesSingle = [];

    public ngAfterViewInit(): void {
        $('.dropdown-toggle').dropdown();
        $('.dropdown').dropdown();
        $('body').addClass('sidebar-mini');
        $('.sidebar').hide();
        $('.main-panel').css('width', '100%');
        $('.container-fluid').css('background-color', '#fff');
    }
    public ngOnInit() {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.partner_id = currentUser.partner_id;
        }
        this.search(0, this.pageSize, true);

        let that = this;
        $(document).keypress(function (e) {
            if (e.which === 13) {
                // alert('You pressed enter!');
                that.filterCustommer();
            }
        });
        this.getInviteUsersFull();
    }

    public getInviteUsersFull() {
        this.user_creates = [];
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getInviteUsers({ partner_id: this.partner_id });
        actionOperate.subscribe(
            (services) => {
                _.each(services, (item) => {
                    this.user_creates.push({ name: item.full_name });
                });

            },
            (err) => {
            });
    }

    public filterCountrySingle(event) {
        let query = this.suser_created;
        this.filteredCountriesSingle = this.filterCountry(query, this.user_creates);
    }

    filterCountry(query, countries: any[]): any[] {
        let filtered: any[] = [];

        if (query.trim().length > 0) {
            for (let i = 0; i < countries.length; i++) {
                let country = countries[i];
                if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                    filtered.push(country);
                }
            }
        } else {
            filtered = this.user_creates;
        }

        return filtered;
    }

    private filterCustommer() {
        this.search(0, this.pageSize, true);
    }

    public search(currentItem, pageSize, notCount) {
        if (!this.partner_id) {
            this.notificationService.showDanger("partner_id is undefined!")
            return;
        }
        let options: any = {};
        options.partner_id = this.partner_id;
        options.position = currentItem;
        options.limit = pageSize;

        if (this.sname) {
            options.name = this.sname.trim();
        }
        if (this.smobile) {
            options.phone = this.smobile.trim();
        }
        if (this.semail) {
            options.email = this.semail.trim();
        }
        if (this.suser_created) {
            options.creater = this.suser_created.trim();
        }

        this.getListService(options);
    }

    public getListService(body) {
        this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.lists = [];
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCustomerNew(body);
        actionOperate.subscribe(
            (services) => {
                if (body['position'] !== undefined && body['position'] === 0 && services.data.count !== undefined) {
                    this.totalRecords = services.data.count;
                }
                if (services.data === 'Khong co data') {
                    this.lists = [];
                } else {
                    this.lists = services.data.customers;
                }
                if (this.lists.length < 1) {
                    this.tableMessage = 'Không có dữ liệu';
                } else {
                    for (let i = 0; i < this.lists.length; i++) {
                        let stt = i + 1;
                        if (body['position'] !== 0) {
                            stt += body['position'];
                        }
                        this.lists[i].stt = stt;
                        if (this.lists[i].bot_created_name !== null) {
                            this.lists[i].creater = this.lists[i].bot_created_name;
                        }
                    }
                    // _.each(this.lists, (item) => {
                    //     let stt = 1;
                    //     if (body['position'] !== 0) {
                    //         stt += body['position'];
                    //     }
                    //     item.stt = stt;
                    //     if (item.bot_created_name !== null) {
                    //         item.creater = item.bot_created_name;
                    //     }
                    // });
                }
            },
            (err) => {
            });
    }

    public loadCarsLazy(event: LazyLoadEvent) {
        this.loadingdata = true;
        this.pageSize = event.rows;
        this.search(event.first, event.rows, false);
    }

    private showAdd() {
        if (!this.partner_id) {
            this.notificationService.showDanger('partner_id is undefined!')
            return;
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        console.log('currentUser', currentUser);
        if (!currentUser.username) {
            this.notificationService.showDanger('username is undefined!')
            return;
        }
        let dialogRef = this.dialog.open(AddCustommerSearchDialogComponent, {
            width: '50%',
            data: {
                'partner_id': this.partner_id,
                'backend_user_created_id': currentUser.id,
                'create_name': currentUser.full_name
            }
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                // reload list
                this.search(0, this.pageSize, true);
            }
        });
    }

    private showEdit(item) {
        if (!this.partner_id) {
            this.notificationService.showDanger('partner_id is undefined!')
            return;
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (!currentUser.username) {
            this.notificationService.showDanger('username is undefined!')
            return;
        }
        let dialogRef = this.dialog.open(EditCustommerSearchDialogComponent, {
            width: '50%',
            data: {
                'partner_id': this.partner_id,
                'backend_user_created_id': currentUser.id,
                'id_edit': item.id
            }
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                this.search(0, this.pageSize, true);
            }
        });
    }

    private deleteItem(item) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            let actionOperate: Observable<any>;
            actionOperate = this.custommerService.deleteCustomer({ "id": item.id });
            actionOperate.subscribe(
                (res) => {
                    if (res.errorCode === 200) {
                        this.notificationService.showSuccess("Xóa khách hàng thành công!");
                        this.search(0, this.pageSize, true);
                    } else {
                        this.notificationService.showSuccess("Xóa khách hàng thất bại!");
                    }
                },
                (err) => {
                    this.notificationService.showDanger("Xóa khách hàng thất bại:" + err);
                });
        }, (dismiss) => {
            //console.log(dismiss);
        });
    }

    private exportExcel() {
        this.loadingProcess = true;
        let data_exports = [];
        let data_dowload = [];
        let header = [
            'STT',
            'Họ Tên',
            'Điện Thoại',
            'Email',
            'Người Tạo',
            'Địa chỉ',
            'Ghi chú',
            'Ngày Tạo'
        ];
        data_dowload.push(header);
        let options: any = {};
        options.partner_id = this.partner_id;
        if (this.sname) {
            options.name = this.sname;
        }
        if (this.smobile) {
            options.phone = this.smobile;
        }
        if (this.semail) {
            options.email = this.semail;
        }
        if (this.suser_created) {
            options.creater = this.suser_created;
        }
        let actionOperate: Observable<any>;
        actionOperate = this.custommerService.getCustomerNew(options);
        actionOperate.subscribe(
            (services) => {
                if (services.data === 'Khong co data') {
                    data_exports = [];
                } else {
                    data_exports = services.data.customers;
                }
                if (data_exports.length > 0) {
                    for (let i = 0; i < data_exports.length; i++) {
                        if (data_exports[i].bot_created_name !== null) {
                            data_exports[i].creater = data_exports[i].bot_created_name;
                        }
                        let row = [
                            (i + 1),
                            data_exports[i].name,
                            data_exports[i].phone,
                            data_exports[i].email,
                            data_exports[i].creater,
                            data_exports[i].address,
                            data_exports[i].note,
                            data_exports[i].created_date,
                        ];
                        data_dowload.push(row);
                    }
                }
                this.loadingProcess = false;
                this.exportByData(data_dowload, 'DSKhachhang.xlsx');
            },
            (err) => {
                this.loadingProcess = false;
                this.notificationService.showDanger("Lỗi khi lấy dữ liệu!");
            });
    }

    exportByData(data, file_name): void {
        /* generate worksheet */
        const ws = XLSX.utils.aoa_to_sheet(data);

        /* set width for colum */
        var wscols = [
            // {wch:20}
        ];
        var wsrows = [
            // {wch:20}
        ];
        wsrows.push({ hpx: 26 });
        if (data[0] != undefined) {
            var n_column = data[0].length;
            wscols.push({ wch: 5 });
            wscols.push({ wch: 20 });
            for (var i = 1; i < n_column; i++) {
                wscols.push({ wch: 20 });
            }
            ws['!cols'] = wscols;
            ws['!rows'] = wsrows;
        }
        /* generate workbook and add the worksheet */
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        const wbout = XLSX.write(wb, this.wopts);
        saveAs(new Blob([s2ab(wbout)]), file_name);
    }

    private select(name) {
        this.suser_created = name;
        this.filteredCountriesSingle = [];
    }


}