import { NotificationService } from './../../_services/notification.service';
import { Component, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import { EmitterService } from "../../_services/emitter.service";
declare var $: any;
@Component({
  selector: 'remarketing-v2',
  templateUrl: 'remarketing-v2.component.html'
})
export class RemarketingV2Component implements AfterViewInit, OnInit {

  private menutype = 'menuremarketing';
  constructor(private notificationService: NotificationService) { }
  public ngAfterViewInit(): void {
    $('.dropdown-toggle').dropdown();
    $('.dropdown').dropdown();
    $('body').removeClass('sidebar-mini');
  }


  public ngOnInit() {
    localStorage.setItem('remarketting', 'true');
    this.notificationService.clearConsole();

  }

  public close() {
    // $('.modal-hana').css('display', 'none');
  }

}
