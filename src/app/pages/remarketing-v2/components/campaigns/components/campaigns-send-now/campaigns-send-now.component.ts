import { Log } from './../../../../../../_services/log.service';
import { ConfigService } from './../../../../../../_services/config.service';
import { Validator } from './../../../../../../themes/validators/validator';
import { Component, Input, OnInit, OnChanges, AfterContentInit, SimpleChanges } from '@angular/core';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { Observable } from 'rxjs/Rx';
import { CampaignService } from '../../../../../../_services/campaign.service';
import { FacebookService } from '../../../../../../_services/facebook.service';
declare var jQuery: any;
declare var $: any;
declare var swal: any;
declare var TimePicker: any;

import * as _ from 'underscore';
import { NotificationService } from '../../../../../../_services/notification.service';
import { StringFormat } from '../../../../../../themes/validators/string.format';
import { IntentService } from '../../../../../../_services/intents.service';
import * as moment from 'moment';

@Component({
  selector: 'campaigns-send-now',
  templateUrl: 'campaigns-send-now.component.html',
  styleUrls: ['campaigns-send-now.component.scss']
})
export class CampaignsSendNowComponent implements OnChanges {
  @Input() private campaignSendNowId: string;
  @Input() private campaignSendNowChangeNameId: string;
  @Input() private appId: string;
  @Input() private campaign: any = { id: '', name: '' };
  private loadingProcess = false;
  private campaignName = 'hi';
  private searchTypeData;
  private templateBroadcast: any = {};
  private messageContent = '';
  private queries = [];
  private totalRecords = 0;
  private totalNotFb = 0;
  private totalFb = 0;
  // block
  private changeBlockId: string = 'BLOCK_CHANGE_ID';
  private outputBlockId: string = 'BLOCK_OUTPUT_ID';
  private block = { block_group_id: 'default', id: 'default', name: 'BLOCK 1', content: '[{\"type\":1,\"message\":{\"text\":\"eeeeeeeee\"}}]', type: 'content' };
  private blockCurrent: any = {};

  // isSending
  private isSending = false;
  private isSendingTimeOut = 5 * 1000; // 5s

  private isTachTiep = false;

  // filter moi
  private isLoadDataFilter = false;
  private searchTypeDataFilter2;
  private conditionsSaved = [];
  private conditionDefault: any = {
    syntax: 'VÀ',
    field: {
      value: '',
      label: ''
    },
    operator: {
      value: '',
      label: ''
    },
    value: {
      value: '',
      label: ''
    }
  };

  private timeAction = false;
  private txtTimeFrom = '';
  private txtTimeTo = '';

  constructor(private configService: ConfigService, private log: Log, private campaignService: CampaignService, private intentService: IntentService, private facebookService: FacebookService, private notificationService: NotificationService) { }

  public ngOnChanges(changes: SimpleChanges): void {
      this.isTachTiep = false;
      // tslint:disable-next-line:only-arrow-functions
      $('.campaign-name').keydown(function (event) {
          if (event.keyCode === 13) {
              this.log.info('`Enter` key is not allowed.');
              event.preventDefault();
              return false;
          }
      });

      if (localStorage.getItem('currentUser')) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          this.appId = currentUser.app_using.applications[0].app_id;
          this.getSearchTypeFilterV2(this.appId);
      }
      if (!this.appId) {
          this.notificationService.showDanger('Vui lòng chọn lại ứng dụng.');
          return;
      }
      let temp = [];
      this.isSending = false;
      this.block.content = '[]';
      this.blockCurrent = this.block;
      if (this.campaign.template_broadcast.length === 0) {
          let body = {campaign_id: this.campaign.id, content: ''};
          this.addTemplateBroadcast(body);
      } else {
          this.templateBroadcast = this.campaign.template_broadcast[0];
          if (!this.templateBroadcast.content) {
              this.templateBroadcast.content = '';
          }

          /** @Tam them */
          this.timeAction = false;
          this.txtTimeFrom = '';
          this.txtTimeTo = '';
          if (this.templateBroadcast.daily_time_config && this.templateBroadcast.daily_time_config !== undefined) {
              try {
                  let daily_time_config = JSON.parse(this.templateBroadcast.daily_time_config);
                  if (daily_time_config['timeAction'] === 'periodtime') {
                      this.txtTimeFrom = daily_time_config['dailyStartTime'];
                      this.txtTimeTo = daily_time_config['dailyEndTime'];
                      this.timeAction = true;
                  } else {
                      if (daily_time_config['dailyStartTime'] != undefined && daily_time_config['dailyEndTime']) {
                          this.txtTimeFrom = daily_time_config['dailyStartTime'];
                          this.txtTimeTo = daily_time_config['dailyEndTime'];
                          this.timeAction = true;
                      }
                  }
              } catch (error) {

              }
          }

          // checkbox
          if (this.templateBroadcast.data_set_size && this.templateBroadcast.expand_days) {
              this.isTachTiep = true;
          }

          this.blockCurrent.content = this.templateBroadcast.content;
          EmitterService.get('SELECT_BLOCK').emit(this.blockCurrent);
      }
      $.getScript('../../../../assets/js/plugins/time_control_custom.js');
  }

  public setLoading() {
    this.loadingProcess = true;
  }

  public setUnLoading() {
    this.loadingProcess = false;
  }

  public campaignChange() {
    let body = { id: this.campaign.id, name: this.campaign.name };
    this.updateCampaign(body);
  }

  public _addCondition2(condition) {
    let that = this;
    let arr = [];
    if (condition) {
      arr = JSON.parse(condition);
    }
    this.conditionsSaved = arr;
    this.getTotalCustomer(arr);
  }

  public updateCampaign(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateCampaign(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
        EmitterService.get(this.campaignSendNowChangeNameId).emit(this.campaign);
      },
      (error) => {
        // this.log.info(error);
        this.log.info(error);
      });
  }

  public addTemplateBroadcast(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('them thanh cong');
        this.templateBroadcast = result;
      },
      (error) => {
        // this.log.info(error);
        this.log.info(error);
      });
  }

  public updateTemplateBroadcast() {
    // this.templateBroadcast.content = this.messageContent;
    // if (this.templateBroadcast.content_text && !this.templateBroadcast.content) {
    //   this.templateBroadcast.content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
    // }
    /** @Tam thêm cấu hình gửi tin trong ngày*/
    let that = this;
    setTimeout(function(){
        let daily_time_config: any;
        if (that.timeAction) {
            that.txtTimeFrom = $('#time_from').val();
            that.txtTimeTo = $('#time_to').val();
            console.log('that.txtTimeFrom', that.txtTimeFrom);
            console.log('that.txtTimeTo', that.txtTimeTo);
            if (that.txtTimeFrom !== undefined && that.txtTimeFrom !== null && that.txtTimeFrom.trim().length > 0 &&
                that.txtTimeTo != undefined && that.txtTimeTo !== null && that.txtTimeTo.trim().length > 0) {
                if (that.txtTimeFrom.trim() === that.txtTimeTo.trim()) {
                    daily_time_config = {
                        "timeAction": "fulltime",
                        "dailyStartTime": that.txtTimeFrom.trim(),
                        "dailyEndTime": that.txtTimeTo.trim(),
                        "inDate": false
                    };
                    that.templateBroadcast['daily_time_config'] = JSON.stringify(daily_time_config);
                } else {
                    let inDate = true;
                    let starttime = moment(that.txtTimeFrom.trim(), "HH:mm");
                    let endtime = moment(that.txtTimeTo.trim(), "HH:mm");
                    if (starttime > endtime) {
                        inDate = false;
                    }
                    daily_time_config = {
                        "timeAction": "periodtime",
                        "dailyStartTime": that.txtTimeFrom.trim(),
                        "dailyEndTime": that.txtTimeTo.trim(),
                        "inDate": inDate
                    };
                    that.templateBroadcast['daily_time_config'] = JSON.stringify(daily_time_config);
                }
            } else {
                daily_time_config = { "timeAction": "fulltime" };
                that.templateBroadcast['daily_time_config'] = JSON.stringify(daily_time_config);
            }
        } else {
            daily_time_config = { "timeAction": "fulltime" };
            that.templateBroadcast['daily_time_config'] = JSON.stringify(daily_time_config);
        }
        /** End @Tam thêm cấu hình gửi tin trong ngày*/
        let actionOperate: Observable<any>;
        actionOperate = that.campaignService.updateTemplateBroadcast(that.templateBroadcast);
        actionOperate.subscribe(
            (result) => {
                // that.log.info('cap nhat thanh cong');
            },
            (error) => {
                that.log.info(error);
            });
    }, 1000);

  }

  public send() {
    swal({
      title: this.configService.facebookPolicyTitle,
      text: this.configService.facebookPolicy,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Tiếp tục gửi',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      //
      this.sending();
    }, (dismiss) => {
      //
    });
  }

  public sending() {
    if (_.isEmpty(this.appId)) {
      this.notificationService.showDanger('Vui lòng chọn lại ứng dụng muốn gửi tin nhắn');
      return;
    }

    if (this.totalRecords === 0) {
      this.notificationService.showDanger('Không có khách hàng thỏa điều kiện gửi');
      return;
    }

    if (this.isTachTiep) {
      if (this.templateBroadcast.data_set_size !== null && (this.templateBroadcast.data_set_size < 1 || this.templateBroadcast.data_set_size > 1000)) {
        this.notificationService.showDanger('Số lượng khách hàng nhập trong khoảng 1 đến 1000');
        return;
      }

      if (this.templateBroadcast.data_set_size !== null && this.templateBroadcast.data_set_size % 1 !== 0) {
        this.notificationService.showDanger('Số lượng khách hàng là số nguyên dương');
        return;
      }

      if (this.templateBroadcast.data_set_size !== null && this.templateBroadcast.expand_days === null) {
        this.notificationService.showDanger('Bạn phải nhập thời gian gửi giữa mỗi đợt');
        return;
      }

      if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.expand_days <= 0) {
        this.notificationService.showDanger('Thời gian gửi giữa mỗi đợt phải lớn hơn 0');
        return;
      }

      if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.expand_days % 1 !== 0) {
        this.notificationService.showDanger('Thời gian gửi là số nguyên dương');
        return;
      }
    }

    if (this.templateBroadcast.expand_days !== null && this.templateBroadcast.data_set_size === null) {
      this.notificationService.showDanger('Bạn phải nhập số lượng khách hàng');
      return;
    }

    if (this.timeAction) {
      if (this.txtTimeFrom === undefined || this.txtTimeFrom === null || (this.txtTimeFrom !== undefined && this.txtTimeFrom.trim().length === 0)) {
        this.notificationService.showDanger('Vui lòng nhập khoảng thời gian cho phép gửi tin');
        return;
      }
      if (this.txtTimeTo === undefined || this.txtTimeTo === null || (this.txtTimeTo !== undefined && this.txtTimeTo.trim().length === 0)) {
        this.notificationService.showDanger('Vui lòng nhập khoảng thời gian cho phép gửi tin');
        return;
      }
    }

    let body: any = {};
    body.app_id = this.appId;
    body.template_broadcast_id = this.templateBroadcast.id;

    body.campaign_id = this.templateBroadcast.campaign_id;
    if (this.isTachTiep === true && this.templateBroadcast.data_set_size !== null && this.templateBroadcast.expand_days !== null) {
      body.data_set_size = this.templateBroadcast.data_set_size;
      body.expand_days = this.templateBroadcast.expand_days;
    }

    body.customer_filter = JSON.parse(this.campaign.customer_target);
    if (this.templateBroadcast.content) {
      body.structure_content = this.templateBroadcast.content;
    }
    //  else {
    //   body.structure_content = JSON.stringify([{ message: { text: this.templateBroadcast.content_text } }]);
    // }
    this.formatBlock(body.structure_content, (cb) => {
      if (!cb || JSON.parse(cb).length === 0) {
        this.notificationService.showDanger('Vui lòng nhập đầy đủ nội dung tin nhắn gửi đến khách hàng');
        return;
      }
      body.structure_content = cb;
      this.sendMessages(body);
    });
    // alert(JSON.stringify(body));
  }

  public sendMessages(body) {
    /** @Tam thêm cấu hình gửi tin trong ngày*/
    let daily_time_config: any;
    if (this.timeAction) {
      this.txtTimeFrom = $('#time_from').val();
      this.txtTimeTo = $('#time_to').val();
      if (this.txtTimeFrom.trim() === this.txtTimeTo.trim()) {
        daily_time_config = {
              "timeAction": "fulltime",
              "dailyStartTime": this.txtTimeFrom.trim(),
              "dailyEndTime": this.txtTimeTo.trim(),
              "inDate": false
        };
      } else {
        let inDate = true;
        let starttime = moment(this.txtTimeFrom.trim(), "HH:mm");
        let endtime = moment(this.txtTimeTo.trim(), "HH:mm");
        if (starttime > endtime) {
          inDate = false;
        }
        daily_time_config = {
          "timeAction": "periodtime",
          "dailyStartTime": this.txtTimeFrom.trim(),
          "dailyEndTime": this.txtTimeTo.trim(),
          "inDate": inDate
        };
      }
    } else {
      daily_time_config = { "timeAction": "fulltime" };
    }
    body['daily_time_config'] = daily_time_config;
    /** End @Tam thêm cấu hình gửi tin trong ngày*/


    this.isSending = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.sendMessageStruct(body);
    actionOperate.subscribe(
      (cus) => {
        if (cus && cus.code === 200) {
          this.notificationService.showSuccess('Yêu cầu gửi tin nhắn của bạn đã được tiếp nhận');
          setTimeout(() => this.isSending = false, this.isSendingTimeOut);
        } else {
          this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
          this.isSending = false;
        }
      },
      (error) => {
        this.notificationService.showDanger('Yêu cầu gửi tin nhắn của bạn thất bại');
        this.isSending = false;
        this.log.info(error);
      });
  }

  public messageChange(event) {
    if (event.target.textContent.length >= 10) {
      event.preventDefault();
    }
  }

  public getTotalCustomer(body) {
    let options: any = {};
    options.app_id = this.appId;
    options.queries = body;
    options.last_item = 1;
    options.page_size = 1;
    let actionOperate: Observable<any>;
    options.has_fb_userchat = true; // chi lay kh co userchat
    actionOperate = this.facebookService.getListCustomer(options);
    actionOperate.subscribe(
      (result) => {
        // this.totalRecords = result.count;
        // this.totalNotFb = result.not_fb_count;
        // this.totalFb = result.fb_count;

        this.totalRecords = result.total;
        this.totalNotFb = 0;
        this.totalFb = 0;
      },
      (error) => {
        this.log.info(error);
      });
  }

  public updateBlock(body) {
    try {
      // alert(JSON.stringify(body));
      let content = JSON.parse(body.content);
      if (this.templateBroadcast) {
        if (content.length === 0) {
          this.templateBroadcast.content = '';
        } else {
          this.templateBroadcast.content = body.content;
        }
        this.updateTemplateBroadcast();
      }
    } catch (error) {
      this.log.info(error);
    }
  }

  public formatBlock(content, callback) {
    if (!content) {
      callback(null);
      return;
    }
    let actionOperate: Observable<any>;
    actionOperate = this.intentService.getValidateFacebookJSON({ messages: content });
    actionOperate.subscribe(
      (json) => {
        if (json.code === 200) {
          callback(json.data);
        } else {
          callback(null);
        }
      },
      (error) => {
        callback(null);
        this.log.info(error);
      });
  }

  public deleteCampaign(campaign) {
    EmitterService.get('DELETE_CAMPAIGN').emit(campaign);
  }

  public updateDataSize(val) {
    // (keyup)="templateBroadcast.data_set_size='122334'.replace(/[^0-9]/g,'')"
    this.log.info(val.value);
    this.templateBroadcast.data_set_size = 1;
    val.value = 1;
  }

  public clearTachtiep() {
    if (!this.isTachTiep) {
      this.templateBroadcast.data_set_size = null;
      this.templateBroadcast.expand_days = null;
      this.updateTemplateBroadcast();
    }
  }

  public getSearchTypeFilterV2(appId) {
    this.setLoading();
    this.isLoadDataFilter = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        this.searchTypeDataFilter2 = result;
        this.isLoadDataFilter = false;
        this._addCondition2(this.campaign.customer_target);
        this.setUnLoading();
      },
      (error) => {
        this.setUnLoading();
        this.isLoadDataFilter = false;
        this.log.info(error);
      });
  }

  public filterv2(data) {
    this.queries = data.query_filter;
    this.getTotalCustomer(this.queries);
    let body = { id: this.campaign.id, customer_target: JSON.stringify(this.queries) };
    this.campaign.customer_target = JSON.stringify(this.queries);
    this.updateCampaign(body);
  }

  private changeConfigTime(event) {
    if (event) {
      this.timeAction = true;
      $.getScript('../../../../assets/js/plugins/time_control_custom.js');
    } else {
      this.timeAction = false;
    }
    this.updateTemplateBroadcast();
  }
}
