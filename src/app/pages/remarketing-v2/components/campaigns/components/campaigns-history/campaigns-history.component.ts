import { Component, Input, OnInit, OnChanges, AfterContentInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { EmitterService } from '../../../../../../_services/emitter.service';
import {NotificationService} from "../../../../../../_services/notification.service";
import {CampaignService} from "../../../../../../_services/campaign.service";
import { Observable } from 'rxjs/Rx';
declare var $: any;
declare var swal: any;

@Component({
  selector: 'campaigns-history',
  templateUrl: 'campaigns-history.component.html',
  styleUrls: ['campaigns-history.component.scss']
})
export class CampaignsHistoryComponent implements  OnChanges, AfterContentInit {
  @Input() campaignHistoryId: string;
  @Input() viewSelectedId: string;
  @Input() data;
  private datasource: any = [];
  private campaign: any = [];
  private broadcasts: any = [];

  // MdPaginator Inputs
  length = 100;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];

  // MdPaginator Output
  pageEvent: PageEvent;

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  changeEvent(event) {
    let pageIndex = event.pageIndex;
    this.loadCarsLazy(pageIndex);
  }

  constructor(public campaignService: CampaignService, public notificationService: NotificationService) { }

  public ngAfterContentInit(): void {
    $('[rel="tooltip"]').tooltip();
  }

  public ngOnChanges(changes: any) {
    let temp = [];
    // EmitterService.get(this.campaignHistoryId).subscribe((data: any) => {
      this.datasource = this.data.broadcast;
      if (this.datasource) {
        this.broadcasts = this.datasource.slice(0, this.pageSize);
        this.length = this.datasource.length;
      }
      this.campaign = this.data.campaign;
    // });
  }

  public loadCarsLazy(index) {
    if (this.datasource) {
      // this.cars = this.datasource.slice(event.first, (event.first + event.rows));
      this.broadcasts = this.datasource.slice(index * this.pageSize, index * this.pageSize + this.pageSize);
    }
  }

  public changeView(campaignId) {
    EmitterService.get(this.viewSelectedId).emit({ campaign_id: campaignId });
  }

  private cancelBroadcast(item) {
        swal({
            title: 'Huỷ thông điệp',
            text: 'Bạn có chắc chắn muốn huỷ thông điệp này?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            let app_id = currentUser.app_using.applications[0].app_id;
            let body = {'appId': app_id, 'broadcastId': item.broadcast_id};
            let actionOperate: Observable<any>;
            actionOperate = this.campaignService.cancelBroadcast(body);
            actionOperate.subscribe(
                (res) => {
                    if (res.code == 200) {
                        this.notificationService.showSuccess("Thực hiện huỷ thành công, thông điệp sẽ được huỷ gửi trong vài phút");
                        for (let i = 0; i < this.broadcasts.length; i++) {
                            if (this.broadcasts[i] === item) {
                                this.broadcasts[i].status = -1;
                                break;
                            }
                        }
                    }
                    if (res.code == 205) {
                        this.notificationService.showDanger("Thông điệp này đã gửi hoàn thành bạn không thể hủy");
                        for (let i = 0; i < this.broadcasts.length; i++) {
                            if (this.broadcasts[i] === item) {
                                this.broadcasts[i].status = 1;
                                break;
                            }
                        }
                    }
                },
                (err) => {
                    console.log(err);
                    this.notificationService.showSuccess("Thực hiện huỷ thất bại");
                });
        }, (dismiss) => {
        });
   }

}
