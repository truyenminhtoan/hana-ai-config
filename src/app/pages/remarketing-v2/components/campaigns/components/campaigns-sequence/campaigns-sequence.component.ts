import { Log } from './../../../../../../_services/log.service';
import { Component, Input, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { EmitterService } from '../../../../../../_services/emitter.service';
import { Observable } from 'rxjs/Rx';
import { CampaignService } from '../../../../../../_services/campaign.service';
import * as _ from 'underscore';
import { NotificationService } from '../../../../../../_services/notification.service';
import * as moment from 'moment';
declare var $: any;
declare var swal: any;

@Component({
  selector: 'campaigns-sequence',
  templateUrl: 'campaigns-sequence.component.html',
  styleUrls: ['campaigns-sequence.component.scss']
})
export class CampaignsSequenceComponent implements OnInit, AfterViewInit {
  @Input() private campaignSequenceId: string;
  @Input() private campaignSequenceChangeNameId: string;
  @Input() private viewSelectedId: string;
  private deviceObjects = [{ name: 'Gửi ngay', value: 1 }, { name: 'phút', value: 2 }, { name: 'giờ', value: 3 }, { name: 'ngày', value: 4 }];
  private campaign: any = { id: '', name: '' };
  private broadcasts: any = [];
  private broadcastView: any = [];

  private timeAction = false;
  private txtTimeFrom;
  private txtTimeTo;

  constructor(private cdRef: ChangeDetectorRef, private campaignService: CampaignService, private log: Log, private notificationService: NotificationService) { }

  public ngOnInit(): void {
    $('.dropdown-content').hide();
    EmitterService.get(this.campaignSequenceId).subscribe((data: any) => {
      // alert(JSON.stringify(data));
      this.broadcasts = [];
      this.campaign = data;
      let tempBroadcast = data.template_broadcast;
      tempBroadcast.filter((item) => {
        try {
          let condition = JSON.parse(item.condition);
          if (!condition.duration) {
            condition.duration = { value: 1, type: this.deviceObjects[3] };
          } else {
            let duration = this.deviceObjects.filter((it) => it.value === condition.duration.type.value)[0];
            if (duration) {
              condition.duration.type = duration;
            }
          }
          item.duration = condition.duration;
        } catch (error) {
          // this.log.info(error);
          let condition = { value: 1, type: this.deviceObjects[3] };
          item.condition = JSON.stringify({ duration: { value: 1, type: this.deviceObjects[0] } });
          item.duration = condition;
        }
      });
      this.broadcasts = tempBroadcast;
      this.broadcastView = this.broadcasts.filter((item) => item.status === 1);

      /** @Tam them vao */
      this.timeAction = false;
      this.txtTimeFrom = '';
      this.txtTimeTo = '';
      if (this.broadcastView.length > 0) {
        let tmp_template = this.broadcastView[0];
        /** @Tam them */
        if (tmp_template.daily_time_config && tmp_template.daily_time_config !== undefined) {
          try {
            let daily_time_config = JSON.parse(tmp_template.daily_time_config);
            if (daily_time_config != null && daily_time_config['timeAction'] != undefined && daily_time_config['timeAction'] === 'periodtime') {
              this.txtTimeFrom = daily_time_config['dailyStartTime'];
              this.txtTimeTo = daily_time_config['dailyEndTime'];
              this.timeAction = true;
            } else {
                if (daily_time_config['dailyStartTime'] !== undefined && daily_time_config['dailyEndTime']) {
                    this.txtTimeFrom = daily_time_config['dailyStartTime'];
                    this.txtTimeTo = daily_time_config['dailyEndTime'];
                    this.timeAction = true;
                }
            }
            $.getScript('../../../../assets/js/plugins/time_control_custom.js');
          } catch (error) {

          }
        }
      }

      this.getListBroadcasts(this.broadcasts[0].campaign);
    });
  }

  public ngAfterViewInit() {
    $('.dropdown-content').hide();
    $('body').click((event) => {
      if (!$(event.target).is('.duration-input') && !$(event.target).is('.duration-select') && !$(event.target).is('.col') && !$(event.target).is('b')) {
        $('.dropdown-content').hide();
      }
    });
  }

  public showHide(event) {
    $('.dropdown-content').hide();
    $(event.target).closest('li.body').find('div.dropdown-content').show();
  }

  public campaignChange() {
    let body = { id: this.campaign.id, name: this.campaign.name };
    this.updateCampaign(body);
  }

  public addBroadCast(campaignId) {
    let broadcast = { campaign_id: campaignId, is_on: false, condition: {}, duration: {}, content_text: '', content: '', priority: 2 };
    broadcast.duration = { value: 1, type: this.deviceObjects[3] };
    // broadcast.priority = this.broadcasts.length + 1;

    broadcast.priority = _.max(this.broadcasts, (item) => { return item.priority; }).priority + 1;
    this.updateValueDuration(broadcast);
    this.addTemplateBroadcast(broadcast);
  }

  public changeIsOnBroadCast(event, templateBroadcast) {
    templateBroadcast.is_on = event.checked;
    this.updateTemplateBroadcast(templateBroadcast);
  }

  public close(event) {
    $(event).removeClass('active');
  }

  public onChangeObj(index, newObj) {
    this.broadcastView[index].duration.type = newObj;
    let condition = JSON.parse(this.broadcastView[index].condition);
    condition.duration = this.broadcastView[index].duration;

    let duration = 0;
    if (condition.duration.type.value === 2) {
      // phut
      duration = 60 * condition.duration.value;
    } else if (condition.duration.type.value === 3) {
      // gio
      duration = 60 * 60 * condition.duration.value;
    } else if (condition.duration.type.value === 4) {
      // ngay
      duration = 60 * 60 * 24 * condition.duration.value;
    }
    condition.type = 'sequence';
    condition.duration_seconds = duration;
    this.broadcastView[index].condition = JSON.stringify(condition);
    this.updateTemplateBroadcast(this.broadcastView[index]);
  }

  public updateDurationValue(broadcast) {
    // broadcast.duration.type = newObj;
    // let condition = JSON.parse(this.broadcasts[index].condition);
    // condition.duration = this.broadcasts[index].duration;
    this.updateValueDuration(broadcast);
    this.updateTemplateBroadcast(broadcast);
  }

  public updateValueDuration(broadcast) {
    let duration = 0;
    if (!broadcast.duration.value) {
      broadcast.duration.value = 1;
    }
    if (broadcast.duration.type.value === 2) {
      // phut
      duration = 60 * broadcast.duration.value;
    } else if (broadcast.duration.type.value === 3) {
      // gio
      duration = 60 * 60 * broadcast.duration.value;
    } else if (broadcast.duration.type.value === 4) {
      // ngay
      duration = 60 * 60 * 24 * broadcast.duration.value;
    }
    broadcast.condition = JSON.stringify({ duration: broadcast.duration, type: 'sequence', duration_seconds: duration });
  }

  public changeView(campaignId, templateId) {
    EmitterService.get(this.viewSelectedId).emit({ campaign_id: campaignId, template_broadcast_id: templateId });
  }

  public addTemplateBroadcast(body) {
    // alert('addTemplateBroadcast');
    // alert(this.broadcasts.length);
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('them thanh cong');
        // this.templateBroadcast = result;
        try {
          let condition = JSON.parse(result.condition);
          if (!condition.duration) {
            condition.duration = { value: 1, type: this.deviceObjects[3] };
          }
          let duration = this.deviceObjects.filter((it) => it.value === condition.duration.type.value)[0];
          if (duration) {
            result.duration = { value: 1, type: duration };
          }
          this.broadcasts.push(result);
          this.broadcastView = this.broadcasts.filter((item) => item.status === 1);
          // console.log(JSON.stringify(this.broadcasts));
          // this.cdRef.detectChanges();

          // Cap nhat lai tat ca
          this.actionUpdateAll();

        } catch (error) {
          this.notificationService.showDanger('Thêm thất bại. Vui lòng thực hiện lại sau.');
          this.log.info(error);
          // result.duration = { value: 1, type: this.deviceObjects[3] };
          // this.broadcasts.push(result);
        }
      },
      (err) => {
        this.log.info(err);
      });
  }

  public updateTemplateBroadcast(template) {
    try {
      template.type = 'sequence';
      let actionOperate: Observable<any>;
      actionOperate = this.campaignService.updateTemplateBroadcast(template);
      actionOperate.subscribe(
        (result) => {
          // this.log.info('cap nhat thanh cong');
        },
        (err) => {
          // this.log.info(err);
        });
    } catch (error) {
      // this.log.info('cap nhat thanh cong');
    }
  }

  public deleteBroadcast(id) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {

      let actionOperate: Observable<any>;
      actionOperate = this.campaignService.deleteTemplateBroadcast(id);
      actionOperate.subscribe(
        (result) => {
          this.broadcasts.filter((item) => {
            if (item.id === id) {
              item.status = -1;
            }
          });
          this.broadcastView = _.reject(this.broadcastView, (item) => { return item.id === id; });
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });

          // sort sau khi delete
          // let ids = _.pluck(this.broadcasts, 'id');
          // this.sortTemplateBroadcast({ template_broadcast_ids: ids });
        },
        (err) => {
          // this.log.info(err);
          this.notificationService.showDanger('Xóa thất bại');
        });
    }, (dismiss) => {
      // this.log.info(err);
    });
  }

  public updateCampaign(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.updateCampaign(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
        EmitterService.get(this.campaignSequenceChangeNameId).emit(this.campaign);
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public sortTemplateBroadcast(body) {
    // this.templateBroadcast.content = this.messageContent;
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.sortTemplateBroadcast(body);
    actionOperate.subscribe(
      (result) => {
        // this.log.info('cap nhat campaign thanh cong');
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public getListBroadcasts(campaignId) {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getListBroadcast(campaignId);
    actionOperate.subscribe(
      (result) => {
        result.filter((itemCount) => {
          this.broadcasts.filter((itemClient) => {
            if (itemCount.template_broadcast_id === itemClient.id) {
              itemClient.num_sent = itemCount.num_sent;
              itemClient.num_delivered = itemCount.num_delivered;
              itemClient.num_seen = itemCount.num_seen;
            }
          });
        });
      },
      (err) => {
        // this.log.info(err);
      });
  }
  public deleteCampaign(campaign) {
    EmitterService.get('DELETE_CAMPAIGN').emit(campaign);
  }

  public validateInput(event, i) {
    if (event.target.value) {
      let value: any = event.target.value.replace(/[^0-9]/gi, '');
      if (value.substr(0, 1) === 0 || value.substr(0, 1) === 0) {
        value = value.substr(0);
      }
      if (value.length > 3) {
        value = value.substr(0, 3);
      }
      // tslint:disable-next-line:radix
      if (parseInt(value) <= 0) {
        return this.broadcasts[i].duration.value = 1;
      }
      this.broadcasts[i].duration.value = value;
    }
  }

  private actionUpdateAll() {
      this.broadcastView.filter((template) => {
          /** @Tam thêm cấu hình gửi tin trong ngày*/
          let daily_time_config: any;
          if (this.timeAction) {
              if (this.txtTimeFrom !== undefined && this.txtTimeFrom !== null && this.txtTimeFrom.trim().length > 0 &&
                  this.txtTimeTo != undefined && this.txtTimeTo !== null && this.txtTimeTo.trim().length > 0) {
                  if (this.txtTimeFrom.trim() === this.txtTimeTo.trim()) {
                      daily_time_config = {
                          "timeAction": "fulltime",
                          "dailyStartTime": this.txtTimeFrom.trim(),
                          "dailyEndTime": this.txtTimeTo.trim(),
                          "inDate": false
                      };
                      template['daily_time_config'] = JSON.stringify(daily_time_config);
                  } else {
                      let inDate = true;
                      let starttime = moment(this.txtTimeFrom.trim(), "HH:mm");
                      let endtime = moment(this.txtTimeTo.trim(), "HH:mm");
                      if (starttime > endtime) {
                          inDate = false;
                      }
                      daily_time_config = {
                          "timeAction": "periodtime",
                          "dailyStartTime": this.txtTimeFrom.trim(),
                          "dailyEndTime": this.txtTimeTo.trim(),
                          "inDate": inDate
                      };
                      template['daily_time_config'] = JSON.stringify(daily_time_config);
                  }
              } else {
                  daily_time_config = { "timeAction": "fulltime" };
                  template['daily_time_config'] = JSON.stringify(daily_time_config);
              }
          } else {
              daily_time_config = { "timeAction": "fulltime" };
              template['daily_time_config'] = JSON.stringify(daily_time_config);
          }

          /** End @Tam thêm cấu hình gửi tin trong ngày*/
          let actionOperate: Observable<any>;
          actionOperate = this.campaignService.updateTemplateBroadcast(template);
          actionOperate.subscribe(
              (result) => {
                  // this.log.info('cap nhat thanh cong');
              },
              (err) => {
                  // this.log.info(err);
              });
      });
  }

  public updateAllTemplateBroadcast() {
      let that = this;
      setTimeout(function(){
          that.txtTimeFrom = $('#time_from').val();
          that.txtTimeTo = $('#time_to').val();
          that.actionUpdateAll();
      }, 1000);
  }

  private changeConfigTime(event) {
    if (event) {
      this.timeAction = true;
      $.getScript('../../../../assets/js/plugins/time_control_custom.js');
    } else {
      this.timeAction = false;
    }
    this.updateAllTemplateBroadcast();
  }
}
