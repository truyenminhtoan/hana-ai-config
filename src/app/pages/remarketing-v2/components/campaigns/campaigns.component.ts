import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from './../../../../_services/application.service';
import { Log } from './../../../../_services/log.service';
import { Component, OnInit, OnChanges, ElementRef, OnDestroy } from '@angular/core';
import { FacebookService } from '../../../../_services/facebook.service';
declare var jQuery: any;
declare var $: any;
declare var swal: any;
import _ from 'underscore';
import { Observable } from 'rxjs/Rx';
import { UUID } from 'angular2-uuid';
import { EmitterService } from '../../../../_services/emitter.service';
import { CampaignService } from '../../../../_services/campaign.service';
import { StringFormat } from '../../../../themes/validators/string.format';
import * as moment from 'moment';
import { NotificationService } from '../../../../_services/notification.service';
@Component({
  selector: 'campaigns',
  templateUrl: 'campaigns.component.html',
  styleUrls: ['campaigns.component.scss']
})
export class CampaignsComponent implements OnInit, OnDestroy {
  private campaignSendNowId = 'CAMPAIGN_SEND_NOW_ID';
  private campaignSendNowChangeNameId = 'CAMPAIGN_SEND_NOW_CHAGE_NAME_ID';
  private campaignSequenceId = 'CAMPAIGN_SEQUENCE_ID';
  private campaignSequenceChangeNameId = 'CAMPAIGN_SEQUENCE_CHANGE_NAME_ID';
  private campaignSequenceDetailId = 'CAMPAIGN_SEQUENCE_DETAIL_ID';
  private campaignScheduleId = 'CAMPAIGN_SCHEDULE_ID';
  private campaignScheduleChangeNameId = 'CAMPAIGN_SCHEDULE_CHAGE_NAME_ID';
  private campaignHistoryId = 'CAMPAIGN_HISTORY_ID';
  private viewSelectedId = 'CAMPAIGN_SELECTED_ID';
  private viewSelected = '';

  private appId = 'c3514ef6-9ae0-4073-a725-d434fc9930fb';
  private searchTypeData;
  private queries: any = [];

  private campaignsNow = [];
  private campaignsSequence = [];
  private campaignsSchedule = [];
  private campaignSelected: any = {};
  private currentUser: any;
  private historySelected;
  private isexpire = false;
  private loadingProcess = false;
  private fanpageInfo;

  private block = { block_group_id: 'default', id: 'default', name: 'BLOCK 1', content: '[{\"type\":1,\"message\":{\"text\":\"eeeeeeeee\"}}]', type: 'content' };
  private blockCurrent: any = [];

  constructor(private route: ActivatedRoute,
    private router: Router, private applicationService: ApplicationService, private elRef: ElementRef, private log: Log, private notificationService: NotificationService, private facebookService: FacebookService, private campaignService: CampaignService) {
    // this.campaignsNow.push({ name: 'default' });
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  public setLoading() {
    this.loadingProcess = true;
  }

  public setUnLoading() {
    this.loadingProcess = false;
  }

  public ngOnInit(): void {

    let h = $(window).height();
    $('.left-scroll').slimScroll({
      height: h - 60 + 'px'
    });
    $('.rigth-scroll').slimScroll({
      height: h - 60 + 'px'
    });
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.appId = currentUser.app_using.applications[0].app_id;
      this.currentUser = currentUser;
      console.log('EXPIREEEEEEEEE ' + this.currentUser.deadlined);
      if (this.currentUser.deadlined) {
        this.isexpire = true;
      }
      this.checkIntegrateFacebook(this.appId);
      // this.getAllCampaign(this.appId);
    }

    EmitterService.get(this.campaignSendNowChangeNameId).subscribe((data: any) => {
      this.campaignsNow.filter((item) => {
        if (item.id === data.id) {
          // this.log.info('data:', JSON.stringify(data));
          item.name = data.name;
        }
      });
    });

    EmitterService.get(this.campaignScheduleChangeNameId).subscribe((data: any) => {
      this.campaignsSchedule.filter((item) => {
        if (item.id === data.id) {
          // this.log.info('data:', JSON.stringify(data));
          item.name = data.name;
        }
      });
    });

    EmitterService.get(this.campaignSequenceChangeNameId).subscribe((data: any) => {
      this.campaignsSequence.filter((item) => {
        if (item.id === data.id) {
          // this.log.info('data:', JSON.stringify(data));
          item.name = data.name;
        }
      });
    });

    EmitterService.get(this.viewSelectedId).subscribe((data: any) => {
      this.log.info('data:' + JSON.stringify(data));
      this.getDetailCampaign(data.campaign_id, data.template_broadcast_id);
    });

    EmitterService.get('DELETE_CAMPAIGN').subscribe((data: any) => {
      this.deleteCampaign(data);
    });
  }

  public ngOnDestroy(): void {
    $('body').removeAttr('style');
  }

  public checkIntegrateFacebook(appId: string) {
    let that = this;
    this.applicationService.getOnlyIntegrationGateway(appId, 1)
      .then((integrate) => {
        if (integrate) {
          this.fanpageInfo = integrate;
          // this.showModel();
          if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1)) {
            // truong hop accessToken con han su dung
            // this.log.info('load facebook');
            this.getAllCampaign(this.appId);
          } else {
            // let body: any = {};
            // body.app_id = this.appId;
            // body.acess_token = this.pageAccessGrowToken;
            // this.createGrowToolGateway(body);
            if (this.fanpageInfo.isExpire === 1 || this.fanpageInfo.isExpire === '1') {
              this.router.navigate(['/remarketingv2/connect-integration'], { relativeTo: this.route });
              return;
            }
          }
        }
      });
  }

  public campaignSelect(item) {
    // alert(JSON.stringify(item));
    if (item) {
      if (item.type === 'sequence') {
        this.viewSelected = 'sequence_list';
        this.campaignSelected = item;
        // EmitterService.get(this.campaignSequenceId).emit(item);
        this.getDetailCampaignSequence(item.id);
        return;
      }
      // Neu khong co lich su broadcast chuyen sang man hinh chi tiet chien dich
      this.getListBroadcaset(item, (cb) => {
        if (cb) {
          if (item.type === 'sequence') {
            this.viewSelected = 'sequence_list';
            this.campaignSelected = item;
            return;
          }
          this.viewSelected = 'list';
          this.campaignSelected = item;
        } else {
          this.getDetailCampaign(item.id, null);
        }
      });
      // alert(item.id);
    }
  }

  public updateValueDuration(broadcast) {
    let duration = 0;
    if (broadcast.duration.type.value === 2) {
      // phut
      duration = 60 * broadcast.duration.value;
    } else if (broadcast.duration.type.value === 3) {
      // gio
      duration = 60 * 60 * broadcast.duration.value;
    } else if (broadcast.duration.type.value === 4) {
      // ngay
      duration = 60 * 60 * 24 * broadcast.duration.value;
    }
    broadcast.condition = JSON.stringify({ duration: broadcast.duration, type: 'sequence', duration_seconds: duration });
  }

  public addCampaign(type) {
    let body: any = {};
    body.app_id = this.appId;
    body.name = type;
    body.type = type;

    if (type === 'now') {
      body.name = StringFormat.generateName('GỬI NGAY', this.campaignsNow, 'name');
    }

    if (type === 'scheduler') {
      let today = moment().add(1, 'days');
      let dateStr = today.format('DD/MM/YYYY HH:mm');
      body.name = dateStr;

      let startTime = today.format('YYYY-MM-DD HH:mm');
      let condition: any = {};
      condition.start_time = startTime;
      condition.repeat = { name: 'none' };
      body.template_broadcast = [];
      body.template_broadcast.push({ status: 1, condition: JSON.stringify(condition) });
    }

    if (type === 'sequence') {
      body.name = StringFormat.generateName('Chuỗi', this.campaignsSequence, 'name');
      let templateBroadcast: any = {};
      // let condition: any = {};
      // condition.duration = { value: 1, type: { name: 'Gửi ngay', value: 1 } };
      // condition.type = 'sequence';
      // condition.duration_seconds = 0;
      body.template_broadcast = [];
      templateBroadcast = { status: 1, duration: { value: 1, type: { name: 'Gửi ngay', value: 1 } } };
      this.updateValueDuration(templateBroadcast);
      body.template_broadcast.push(templateBroadcast);
    }

    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.addCampaign(body);
    actionOperate.subscribe(
      (result) => {
        switch (type) {
          case 'now':
            this.campaignSelected = result;
            this.campaignsNow.push(result);
            EmitterService.get(this.campaignSendNowId).emit(result);
            EmitterService.get(this.viewSelectedId).emit({ campaign_id: result.id, template_broadcast_id: null });
            break;
          case 'sequence':
            // this.campaignsSequence.push({ id: uid, name: 'campaign default' });
            this.campaignsSequence.push(result);
            this.campaignSelected = result;
            EmitterService.get(this.campaignSequenceId).emit(result);
            // this.viewSelected = 'sequence_list';
            this.campaignSelect(result);
            break;
          case 'scheduler':
            this.campaignSelected = result;
            this.campaignsSchedule.push(result);
            EmitterService.get(this.campaignScheduleId).emit(result);
            EmitterService.get(this.viewSelectedId).emit({ campaign_id: result.id, template_broadcast_id: null });
            break;
          default:

        }
      },
      (err) => {
        // this.log.info(err);
      });
  }

  public getAllCampaign(appId) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getAllCampaign(appId);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        result.filter((item) => {
          switch (item.type) {
            case 'now':
              this.campaignsNow.push(item);
              break;
            case 'sequence':
              this.campaignsSequence.push(item);
              break;
            case 'scheduler':
              this.campaignsSchedule.push(item);
              break;
            default:
          }
        });
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
      });
  }

  public getDetailCampaign(campaignId, templateBroadcastId) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getDetailCampaign(campaignId);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result) {
          this.campaignSelected = result;
          this.viewSelected = this.campaignSelected.type;
          if (this.campaignSelected.type === 'now') {
            // EmitterService.get(this.campaignSendNowId).emit(result);
          } else if (this.campaignSelected.type === 'scheduler') {
            // EmitterService.get(this.campaignScheduleId).emit(result);
          } else if (this.campaignSelected.type === 'sequence') {
            if (result.template_broadcast) {
              let templateBroadcast = result.template_broadcast.filter((item) => item.id === templateBroadcastId)[0];
              // alert(templateBroadcast)
              if (templateBroadcast) {
                // alert(JSON.stringify(templateBroadcast))
                // alert(JSON.stringify(templateBroadcast));
                result.template_broadcast = [templateBroadcast];
                this.campaignSelected = result;
                // EmitterService.get(this.campaignSequenceDetailId).emit(result);
              }
            }
          }
        }
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
      });
  }

  public getDetailCampaignSequence(campaignId) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getDetailCampaign(campaignId);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result) {
          this.campaignSelected = result;
          EmitterService.get(this.campaignSequenceId).emit(result);
        }
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
      });
  }

  public getListBroadcaset(campaign, callback) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.getListBroadcast(campaign.id);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result.length > 0) {
          let campaignObj: any = {};
          campaignObj.broadcast = result;
          campaignObj.campaign = campaign;
          this.historySelected = campaignObj;
          // EmitterService.get(this.campaignHistoryId).emit(campaignObj);
          callback(true);
        } else {
          callback(false);
        }
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
        callback(false);
      });
  }

  public deleteCampaign(campaign) {
    swal({
      title: 'Bạn chắc chắn muốn xóa?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Có',
      cancelButtonText: 'Không',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.deleteCampaignTemplate(campaign, (cb) => {
        if (!cb) {
          this.notificationService.showDanger('Xóa thất bại');
          return;
        }
        this.deleteCampaigns(campaign, (cb1) => {
          if (!cb1) {
            this.notificationService.showDanger('Xóa thất bại');
            return;
          }
          swal({
            title: 'Xóa thành công!',
            text: '',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          }).then(() => {
            this.campaignSelected = {};
            this.viewSelected = '';
            this.campaignsNow = this.campaignsNow.filter((item) => item.id !== campaign.id);
            this.campaignsSchedule = this.campaignsSchedule.filter((item) => item.id !== campaign.id);
            this.campaignsSequence = this.campaignsSequence.filter((item) => item.id !== campaign.id);
          }, (dismiss) => {
            this.campaignSelected = {};
            this.viewSelected = '';
            this.campaignsNow = this.campaignsNow.filter((item) => item.id !== campaign.id);
            this.campaignsSchedule = this.campaignsSchedule.filter((item) => item.id !== campaign.id);
            this.campaignsSequence = this.campaignsSequence.filter((item) => item.id !== campaign.id);
          });
        });
      });
    }, (dismiss) => {
    });
  }

  public deleteCampaigns(campaign, callback) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.deleteCampaign(campaign.id);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result) {
          callback(true);
        } else {
          callback(false);
        }
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
        callback(false);
      });
  }

  public deleteCampaignTemplate(campaign, callback) {
    this.setLoading();
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.deleteCampaignTemplate(campaign.id);
    actionOperate.subscribe(
      (result) => {
        this.setUnLoading();
        if (result) {
          callback(true);
        } else {
          callback(false);
        }
      },
      (err) => {
        this.setUnLoading();
        // this.log.info(err);
        callback(false);
      });
  }

  public updateBlock(body) {
    // alert(JSON.stringify(body));
  }
}