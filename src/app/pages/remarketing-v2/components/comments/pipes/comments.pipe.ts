import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/Rx';
import { StringFormat } from '../../../../../themes/validators/string.format';

@Pipe({
    name: 'searchPipe',
    pure: false
})
export class CommentPipe implements PipeTransform {
    public transform(data: any[], searchTerm: string): any[] {
        if (searchTerm != null && searchTerm.trim().length > 0) {
            searchTerm = StringFormat.formatText(searchTerm.trim());
            return data.filter((item) => {
                return (StringFormat.formatText(item.name).indexOf(StringFormat.formatText(searchTerm)) !== -1)
            });
        } else {
            return data;
        }
    }
}
