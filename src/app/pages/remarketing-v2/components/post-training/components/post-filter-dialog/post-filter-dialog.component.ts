import { NotificationService } from './../../../../../../_services/notification.service';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
// import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
// import { default as _rollupMoment } from 'moment';
// const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
// export const MY_FORMATS = {
//     parse: {
//         dateInput: 'YYYY-MM-DD',
//     },
//     display: {
//         dateInput: 'DD-MM-YYYY',
//         monthYearLabel: 'YYYY-MM-DD',
//         dateA11yLabel: 'YYYY-MM-DD',
//         monthYearA11yLabel: 'YYYY-MM-DD',
//     },
// };



@Component({
    selector: 'post-filter-dialog',
    templateUrl: 'post-filter-dialog.component.html',
    styleUrls: ['post-filter-dialog.component.scss']
})
export class PostFilterDialogComponent {
    private fromDate;
    private toDate;
    constructor( @Inject(MD_DIALOG_DATA) public data: any, private notificationService: NotificationService, public dialogRef: MdDialogRef<PostFilterDialogComponent>) {

    }

    public search() {
        if (!this.fromDate) {
            return this.notificationService.showDanger('Bạn vui lòng nhập ngày bắt đầu');
        }
        if (!this.fromDate) {
            return this.notificationService.showDanger('Bạn vui lòng nhập ngày kết thúc');
        }
        this.dialogRef.close({ from_date: this.fromDate, to_date: this.toDate });
    }

    public onInput(event) {
        this.fromDate = _moment(event.value).format('YYYY-MM-DD');
        // this.log.info('%s-%s-%s', event.value._i.year, event.value._i.year, event.month._i.date);
    }
    public onChange(event) {
        this.fromDate = _moment(event.value).format('YYYY-MM-DD');
    }
    public onInputEnd(event) {
        this.toDate = _moment(event.value).format('YYYY-MM-DD');
        // this.log.info(_moment(event.value).format('YYYY-MM-DD'));
        // this.log.info('%s-%s-%s', event.value._i.year, event.value._i.year, event.month._i.date);
    }
    public onChangeEnd(event) {
        this.toDate = _moment(event.value).format('YYYY-MM-DD');
    }
}
