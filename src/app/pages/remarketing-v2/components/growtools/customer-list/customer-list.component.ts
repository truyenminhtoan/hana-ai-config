import { Log } from './../../../../../_services/log.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignService } from './../../../../../_services/campaign.service';
import { ConfigService } from './../../../../../_services/config.service';
import { ApplicationService } from './../../../../../_services/application.service';
import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { LazyLoadEvent } from 'primeng/primeng';
import { FacebookService } from '../../../../../_services/facebook.service';
import { Observable } from 'rxjs/Observable';
import { MdDialog } from '@angular/material';
import _ from 'underscore';
import { SendMessageGrowDialogComponent } from '../components/send-message-dialog/send-message-dialog.component';
import { NotificationService } from '../../../../../_services/notification.service';
import { Message } from '../../../../../../assets/plugins/primeng/components/common/message';
import { HistoryMessageGrowDialogComponent } from '../components/history-message-dialog/history-message-dialog.component';
declare var jQuery: any;
declare var $: any;
declare var $: any;
declare var swal: any;
declare const FB: any;
@Component({
  selector: 'custome-grow-list',
  templateUrl: 'customer-list.component.html',
  styleUrls: ['customer-list.component.scss']
})
export class CustomerGrowListComponent implements OnInit, AfterViewInit, OnDestroy {
  private searchTypeData;
  private buttomFilter = false;
  private queries: any = [];
  private isFilter = false;
  private keySearch = '';
  private appId = 'c3514ef6-9ae0-4073-a725-d434fc9930fb';
  private pageSize = 50;
  private pageAccessGrowToken;

  private datasource: any[] = [];
  private cars: any[] = [];
  private totalRecords: number = 0;
  private totalSelects: number = 0;

  private selectedCustomers: any[] = [];
  private tableMessage = 'Đang tải dữ liệu';

  private messageText: any = [{ message: 'Xin chào {{ten_khach_hang}}.' }];
  private messageBlock: any = { block_group_id: '', id: '', name: '', content: '[]', type: 'content' };
  private fanpageInfo;

  private selectedItems;
  private check_status = false;
  private customer_outputs = [];

  private checkedAll = false;


  // @Input searchTypeDataFilter2 cấu trúc mẫu
  private searchTypeDataFilter2;

  private conditionsSaved = [];
  private condition_default: any = {
    "syntax": "VÀ",
    "field": {
      "value": "",
      "label": ""
    },
    "operator": {
      "value": "",
      "label": ""
    },
    "value": {
      "value": "",
      "label": ""
    }
  };
  private is_load_data_filter = false;

  constructor(private route: ActivatedRoute,
    private log: Log,
    private router: Router, private campaignService: CampaignService, private applicationService: ApplicationService, private elRef: ElementRef, private facebookService: FacebookService, private dialog: MdDialog, private notificationService: NotificationService, private serviceConfig: ConfigService) {
    elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  public ngOnInit(): void {
    // this.cdRef.detectChanges();
    $.getScript('../../../assets/js/core/jquery-ui.min.1.12.1.js', () => {
      if (localStorage.getItem('currentUser')) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.appId = currentUser.app_using.applications[0].app_id;
        this.checkIntegrateFacebook(this.appId);
      }
    });

    // FB.init({
    //   appId: this.serviceConfig.facebookConfig.client_id,
    //   cookie: false,  // enable cookies to allow the server to access
    //   xfbml: true,  // parse social plugins on this page
    //   version: 'v2.8' // use graph api version 2.5
    // });
    // let that = this;
    // FB.login((response) => {
    //   if (response.authResponse) {
    //     FB.api('/me', { fields: 'id,name,email' }, (res) => {
    //       this.log.info(response.authResponse.accessToken);
    //       localStorage.setItem('accessTokenGrow', response.authResponse.accessToken);
    //       that.pageAccessGrowToken = response.authResponse.accessToken;
    //       // this.cdRef.detectChanges();
    //       // this.loadData();
    //       if (localStorage.getItem('currentUser')) {
    //         that.checkIntegrateFacebook(that.appId);
    //       }
    //     });
    //   }
    // }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });


    // $.getScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', () => {
    // if (localStorage.getItem('currentUser')) {
    //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //   this.appId = currentUser.app_using.applications[0].app_id;
    // }
    // this.getSearchType(this.appId);
    // this.search(0, this.pageSize);
  }

  public loadData() {
    // this.getSearchType(this.appId);
    this.getSearchTypeFilterV2(this.appId);
    this.search(0, this.pageSize, true);

    let h = $(window).height();
    $('.card').slimScroll({
      height: h - 60 + 'px'
    });
  }

  public ngOnDestroy(): void {
    $('body').removeAttr('style');
  }

  public onRowSelect(event) {
    // this.router.navigate(['/pages/services/serviceEdit/' + event.data.id], { relativeTo: this.route });

  }

  public actionSearch() {
    this.selectedItems = [];
    this.getDataCheck();
    this.searchLoc(0, this.pageSize, true);
  }

  public ngAfterViewInit() {
    $('.dropdown-toggle').dropdown();
    // $('body').removeClass('sidebar-mini');
    $('.condition-item1').dropdown();
  }

  public loadCarsLazy(event: LazyLoadEvent) {
    this.pageSize = event.rows;
    this.search(event.first, event.rows, false);
  }

  public search(currentItem, pageSize, notCount) {
    let options: any = {};
    options.app_id = this.appId;
    options.last_item = currentItem;
    options.page_size = pageSize;
    options.queries = this.queries;
    options.full_name = this.keySearch;
    if (notCount) {
      this.getTotalCustomer(options);
    }
    this.getListService(options);
  }

  public searchFilter(currentItem, pageSize, notCount) {
        let options: any = {};
        options.app_id = this.appId;
        options.last_item = currentItem;
        options.page_size = pageSize;
        options.queries = this.queries;
        options.full_name = this.keySearch;
        if (notCount) {
            this.getTotalCustomer(options);
        }
        this.getListServiceFilter(options);
  }

  public searchLoc(currentItem, pageSize, notCount) {
        let options: any = {};
        options.app_id = this.appId;
        options.last_item = currentItem;
        options.page_size = pageSize;
        options.queries = this.queries;
        options.full_name = this.keySearch;
        if (notCount) {
            this.getTotalCustomer(options);
        }
        this.getListServiceLoc(options);
  }

  public getSearchType(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        let searchType = [];
        result.filter((item) => {
          let search: any = { id: item.param, type: item.value_type, label: item.name };
          if (item.value_type === 'list') {
            if (item.page_values.length > 0 && item.name !== 'Chọn nhóm') {
              search.list = item.page_values;
              searchType.push(search);

              // item.page_values.filter((it) => {
              //   it.label = `${it.label}(${it.count})`;
              // });
            }
          } else {
            searchType.push(search);
          }
        });
        this.searchTypeData = searchType;
        $.getScript('../../../../../../assets/plugins/structured-filter/js/structured-filter.js', () => {
          this._queryBuilder2();
        });
        // this._queryBuilder2();
      },
      (err) => {
        this.log.info(err);
      });
  }

  public reloadSearchType(appId) {
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        let searchType = [];
        result.filter((item) => {
          let search: any = { id: item.param, type: item.value_type, label: item.name };
          if (item.value_type === 'list') {
            if (item.page_values.length > 0) {
              search.list = item.page_values;
              searchType.push(search);
            }
          } else {
            searchType.push(search);
          }
        });
        this.searchTypeData = searchType;
        $('#myFilter').structFilter({
          highlight: true,
          buttonLabels: true,
          submitReady: true,
          fields: this.searchTypeData
        });
      },
      (err) => {
        this.log.info(err);
      });
  }

  public getListService(body) {
    // alert('request server');
    // this.tableMessage = 'Đang tải dữ liệu';
    this.selectedCustomers = [];
    this.datasource = [];
    this.cars = [];
    let actionOperate: Observable<any>;
    body.has_fb_userchat = false; // chi lay kh khong co userchat
    actionOperate = this.facebookService.getListCustomer(body);
    actionOperate.subscribe(
      (services) => {
        // alert(JSON.stringify(services));
        // this.datasource = services;
        // this.totalRecords = this.datasource.length;
        // this.cars = services;

        // this.datasource = services;
        // this.totalRecords = services.length;
        // this.cars = this.datasource.slice(0, this.pageSize);
        this.cars = services;
          if (this.check_status) {
              _.each(this.cars, (item) => {
                  let uncheck = _.find(this.selectedItems, (itemSec) => {
                      return item.id === itemSec.id;
                  });
                  if (uncheck) {
                      item.status = false;
                  } else {
                      item.status = true;
                  }
              });
          } else {
              _.each(this.cars, (item) => {
                  let uncheck = _.find(this.selectedItems, (itemSec) => {
                      return item.id === itemSec.id;
                  });
                  if (uncheck) {
                      item.status = true;
                  } else {
                      item.status = false;
                  }
              });
          }
        if (this.cars.length < 1) {
          this.tableMessage = 'Không có dữ liệu';
        }
        // this.cdRef.detectChanges();
      },
      (err) => {
        this.log.info(err);
      });
  }

    public getListServiceFilter(body) {
        // alert('request server');
        // this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.datasource = [];
        this.cars = [];
        let actionOperate: Observable<any>;
        body.has_fb_userchat = false; // chi lay kh khong co userchat
        actionOperate = this.facebookService.getListCustomer(body);
        actionOperate.subscribe(
            (services) => {
                // alert(JSON.stringify(services));
                // this.datasource = services;
                // this.totalRecords = this.datasource.length;
                // this.cars = services;

                // this.datasource = services;
                // this.totalRecords = services.length;
                // this.cars = this.datasource.slice(0, this.pageSize);
                this.cars = services;
                $('#click_all').prop('checked', false);
                this.checkAll(false);
                if (this.cars.length < 1) {
                    this.tableMessage = 'Không có dữ liệu';
                }
                // this.cdRef.detectChanges();
            },
            (err) => {
                this.log.info(err);
            });
    }

    public getListServiceLoc(body) {
        // alert('request server');
        // this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.datasource = [];
        this.cars = [];
        let actionOperate: Observable<any>;
        body.has_fb_userchat = false; // chi lay kh khong co userchat
        actionOperate = this.facebookService.getListCustomer(body);
        actionOperate.subscribe(
            (services) => {
                // alert(JSON.stringify(services));
                // this.datasource = services;
                // this.totalRecords = this.datasource.length;
                // this.cars = services;

                // this.datasource = services;
                // this.totalRecords = services.length;
                // this.cars = this.datasource.slice(0, this.pageSize);
                this.cars = services;
                $('#click_all').prop('checked', false);
                this.checkAll(false);
                if (this.cars.length < 1) {
                    this.tableMessage = 'Không có dữ liệu';
                }
                // this.cdRef.detectChanges();
            },
            (err) => {
                this.log.info(err);
            });
    }

  public getTotalCustomer(body) {
    let actionOperate: Observable<any>;
    body.has_fb_userchat = false; // chi lay kh khong co userchat
    actionOperate = this.facebookService.getTotalCustomer(body);
    actionOperate.subscribe(
      (result) => {
        this.totalRecords = result.count;
      },
      (err) => {
        this.log.info(err);
      });
  }

  public toggleFilter() {
    this.buttomFilter = !this.buttomFilter;
  }

  public _queryBuilder2() {
    let that = this;
    $('#myFilter').structFilter({
      highlight: true,
      buttonLabels: true,
      submitReady: true,
      fields: this.searchTypeData
    });

    // tslint:disable-next-line:only-arrow-functions
    $('#myFilter').on('submit.search', function (event) {
      // do something
      alert(event);
    });

    // tslint:disable-next-line:only-arrow-functions
    $('#myFilter').on('change.search', function (event) {
      // do something
      let queries = [];
      let data = $('#myFilter').structFilter('val');
      that.queries = data;
      that.search(0, this.pageSize, true);
    });
  }

  public openSendMessageDialog() {
    // if (_.isEmpty(this.selectedCustomers)) {
    //   this.notificationService.showSuccess('Vui lòng chọn khách hàng gửi tin nhắn');
    //   return;
    // }
    if (!this.check_status && this.customer_outputs.length === 0) {
      this.notificationService.showDanger('Không có khách hàng được chọn để gửi tin nhắn');
      return;
    }
    // if (this.totalRecords < 1) {
    //   this.notificationService.showDanger('Không có khách hàng được chọn để gửi tin nhắn');
    //   return;
    // }
    let dialogRef = this.dialog.open(SendMessageGrowDialogComponent, {
      // height: '310px',
      width: '50%',
      data: {
        is_check_all: this.check_status,
        customer_outputs: this.customer_outputs,
        queries: this.queries,
        full_name: this.keySearch,
        app_id: this.appId,
        message_text: this.messageText,
        message_block: this.messageBlock
      }
    });
  }

  public openHistoryMessageDialog() {
    let dialogRef = this.dialog.open(HistoryMessageGrowDialogComponent, {
      // height: '310px',
      width: '50%',
      data: {
        app_id: this.appId,
      }
    });
  }
  // getOnlyIntegrationGateway

  public checkIntegrateFacebook(appId: string) {
    let that = this;
    this.applicationService.getOnlyIntegrationGateway(appId, 1)
      .then((integrate) => {
        if (integrate) {
          this.fanpageInfo = integrate;
          // this.showModel();
          if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1) && this.fanpageInfo.isGrowExpire && this.fanpageInfo.isGrowExpire === '0') {
            // truong hop accessToken con han su dung
            // this.log.info('load facebook');
            this.loadData();
          } else {
            // let body: any = {};
            // body.app_id = this.appId;
            // body.acess_token = this.pageAccessGrowToken;
            // this.createGrowToolGateway(body);
            if (this.fanpageInfo.isExpire === 1 || this.fanpageInfo.isExpire === "1") {
              this.router.navigate(['/remarketingv2/connect-integration'], { relativeTo: this.route });
              return;
            }
            this.router.navigate(['/remarketingv2/connect-facebook'], { relativeTo: this.route });
          }
        }
      });
  }

  public createGrowToolGateway(body) {
    let actionOperate: Observable<any>;
    actionOperate = this.campaignService.createGrowToolGateway(body);
    actionOperate.subscribe(
      (result) => {
        this.loadData();
      },
      (err) => {
        this.log.info(err);
        this.notificationService.showDanger('Tài khoản hiện tại không quản lý page bạn đang dùng');
      });
  }


  // public getDataCheck() {
  //   if (!this.selectedItems) {
  //     this.selectedItems = [];
  //   }
  //   this.customer_outputs = [];
  //   if (this.cars.length === this.selectedItems.length) {
  //     this.check_status = true;
  //   }
  //   if (this.selectedItems.length == 0) {
  //     this.check_status = false;
  //   }
  //   // - Nếu check all: Gửi query + Gửi tập khách hàng Uncheck - Nếu Uncheck all: Gửi tập khách hàng check
  //   if (this.check_status === true) {
  //     this.customer_outputs = this.cars.filter((row) => {
  //       return this.checkExist(row.id);
  //     });
  //   } else {
  //     this.customer_outputs = this.selectedItems;
  //   }
  //   this.log.info('check_statusl:' + this.check_status);
  //   this.log.info('customer_outputs:' + this.customer_outputs);
  //   if (this.check_status && this.customer_outputs.length === 0) {
  //     this.totalSelects = this.totalRecords;
  //   } else if (this.check_status && this.customer_outputs.length > 0) {
  //     this.totalSelects = this.totalRecords - this.customer_outputs.length;
  //   } else if (!this.check_status && this.customer_outputs.length === 0) {
  //     this.totalSelects = 0;
  //   } else if (!this.check_status && this.customer_outputs.length > 0) {
  //     this.totalSelects = this.customer_outputs.length;
  //   }
  // }

  /** @TamNguyen */
  public getDataCheck() {
        if (!this.selectedItems) {
            this.selectedItems = [];
        }
        // let check_status =  $('#click_all').is(":checked");
        let check_status =  this.check_status;
        this.customer_outputs = [];
        // - Nếu check all: Gửi query + Gửi tập khách hàng Uncheck - Nếu Uncheck all: Gửi tập khách hàng check
        this.customer_outputs = this.selectedItems;

        console.log('this.selectedItems', this.selectedItems);
        console.log('this.customer_outputs', this.customer_outputs);
        console.log('this.check_status', check_status);

        if (check_status && this.customer_outputs.length === 0) {
            this.totalSelects = this.totalRecords;
        } else if (check_status && this.customer_outputs.length > 0) {
            this.totalSelects = this.totalRecords - this.customer_outputs.length;
        } else if (!this.check_status && this.customer_outputs.length === 0) {
            this.totalSelects = 0;
        } else if (!check_status && this.customer_outputs.length > 0) {
            this.totalSelects = this.customer_outputs.length;
        }
  }


  public checkExist(id) {
    let resulst = this.selectedItems.filter((row) => {
      return row.id === id;
    });
    if (resulst.length === 0) {
      return true;
    } else {
      return false;
    }
  }

  public getSearchTypeFilterV2(appId) {
    this.is_load_data_filter = true;
    let actionOperate: Observable<any>;
    actionOperate = this.facebookService.getSearchType(appId);
    actionOperate.subscribe(
      (result) => {
        result = result.filter((item) => item.param !== 'seq.campaign_id' &&  item.param !== 'pcg1.app_group_id')
        this.searchTypeDataFilter2 = result;
        this.is_load_data_filter = false;
      },
      (err) => {
        this.is_load_data_filter = false;
        console.log(err);
      });
  }

  public filterv2(data) {
    this.selectedItems = [];
    this.getDataCheck();
    // console.log('data output query_filter', JSON.stringify(data.query_filter));
    // console.log('data output condition', JSON.stringify(data.conditions));
    this.queries = data.query_filter;
    this.searchFilter(0, this.pageSize, true);
  }

  public checked(carValue) {
        if (this.selectedItems == undefined) {
          this.selectedItems = [];
        }
        if (this.check_status) {
            if (!carValue.status) {
                this.selectedItems.push(carValue);
            } else {
                this.selectedItems = _.filter(this.selectedItems, (item) => {
                    return item.id !== carValue.id;
                });
            }
        } else {
            if (carValue.status) {
                this.selectedItems.push(carValue);
            } else {
                this.selectedItems = _.filter(this.selectedItems, (item) => {
                    return item.id !== carValue.id;
                });
            }
        }

        console.log('this.selectedItems.length', this.selectedItems.length);
        console.log('this.totalRecords', this.totalRecords);

        /** @Tam them vao */
        if (this.selectedItems.length === this.totalRecords || this.selectedItems.length == 0) {
            let check_all = true;
            let check_un_check_all = true;
            this.selectedItems.filter((row) => {
                if (row.status == false) {
                    check_all = false;
                }
                if (row.status == true) {
                    check_un_check_all = false;
                }
            });
            if (check_all && carValue.status) {
                $('#click_all').prop('checked', true);
                this.checkAll(true);
            }
            if (this.selectedItems.length === this.totalRecords && check_un_check_all) {
                $('#click_all').prop('checked', false);
                this.checkAll(false);
            }
        } else {
            $('#click_all').prop('checked', false);
        }
        /** End @Tam them vao */

        this.getDataCheck();
  }
  public checkAll(event) {
        _.each(this.cars, (item) => {
            if (event) {
                item.status = true;
            } else {
                item.status = false;
            }
        });

        if (event) {
            this.check_status = true;
        } else {
            this.check_status = false;
        }
        this.selectedItems = [];

        this.getDataCheck();
  }

}
