import { Log } from './../../../../../_services/log.service';
import { NotificationService } from './../../../../../_services/notification.service';
import { CampaignService } from './../../../../../_services/campaign.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from './../../../../../_services/config.service';
import { Component, OnInit, ChangeDetectorRef, NgZone, OnDestroy } from '@angular/core';
import { PlatformLocation } from '@angular/common';
declare const FB: any;
@Component({
    selector: 'check-facebook-login',
    templateUrl: 'check-facebook-login.component.html',
    styleUrls: ['check-facebook-login.component.scss']
})
export class CheckFacebookLoginComponent implements OnInit, OnDestroy {
    private appId;
    private showLogin = false;
    constructor(private cdRef: ChangeDetectorRef,
                private serviceConfig: ConfigService,
                private campaignService: CampaignService, 
                private notificationService: NotificationService,
                private route: ActivatedRoute,
                public router: Router,
                private log: Log,
                private platformLocation: PlatformLocation,
                private ngZone: NgZone) {
    }
    public ngOnInit(): void {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.appId = currentUser.app_using.applications[0].app_id;
        }
        FB.init({
            // appId: '378516159201961',
            appId: this.serviceConfig.facebookConfig.growtool_client_id,
            cookie: false,  // enable cookies to allow the server to access
            // the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.5
        });
        FB.getLoginStatus((response) => {
            this.log.info(response);
            if (response.authResponse) {
                localStorage.setItem('accessTokenGrow', response.authResponse.accessToken);
                let pageAccessGrowToken = response.authResponse.accessToken;
                this.createGrowToolGateway(this.appId, pageAccessGrowToken);
            } else {
                this.showLogin = true;
            }
        });
    }

    public ngOnDestroy(): void {
        window.location.reload();
    }

    public clickLogin() {
        let that = this;
        FB.login((response) => {
            if (response.authResponse) {
                FB.api('/me', { fields: 'id,name,email' }, (res) => {
                    this.log.info(response);
                    if (response.authResponse) {
                        localStorage.setItem('accessTokenGrow', response.authResponse.accessToken);
                        let pageAccessGrowToken = response.authResponse.accessToken;
                        this.createGrowToolGateway(this.appId, pageAccessGrowToken);
                    } else {
                        this.showLogin = true;
                    }
                });
            }
        }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
    }

    public createGrowToolGateway(appId, token) {
        let body: any = {};
        body.app_id = appId;
        body.acess_token = token;
        let actionOperate: Observable<any>;
        actionOperate = this.campaignService.createGrowToolGateway(body);
        actionOperate.subscribe(
            (result) => {
                if (result.code === 200) {
                    // this.router.navigate(['/remarketingv2/growtool?refresh=1']);
                    // this.router.navigate(['/remarketingv2/growtool?refresh=1']);

                    this.router.navigated = false;
                    this.router.navigateByUrl('/customer-hana/growthtools?refresh=1');
                    // this.platformLocation.onPopState(() => {
                    //     if(this.platformLocation.pathname.startsWith('/remarketingv2/growtool')) {
                    //         this.ngZone.run(() => {
                    //             this.log.info('Reloading component');
                    //         });
                    //     }
                    // });
                    return;
                }
                if (result.code === 203) {
                    this.notificationService.showDanger('Tài khoản hiện tại không quản lý page bạn đang dùng');
                    return;
                }
                if (result.code === 205) {
                    // this.notificationService.showDanger('Tài khoản hiện tại không quản lý page bạn đang dùng');
                    this.showLogin = true;
                    return;
                }
                this.notificationService.showDanger('Hệ thống đang bận, vui lòng thử lại sau.');
            },
            (err) => {
                this.log.info(err);
                this.notificationService.showDanger('Tài khoản hiện tại không quản lý page bạn đang dùng');
            });
    }
}
