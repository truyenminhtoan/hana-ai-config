import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'comfirm-delete-dialog',
  templateUrl: 'comfirm-delete-dialog.component.html',
  styleUrls: ['comfirm-delete-dialog.component.scss']
})
export class ComfirmDeleteDialogComponent {
  constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<ComfirmDeleteDialogComponent>) { }
}
