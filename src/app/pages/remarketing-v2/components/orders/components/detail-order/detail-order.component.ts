import {
    Component,
    Inject,
    OnInit, Input, EventEmitter, Output, ElementRef
} from '@angular/core';
import {
    MdDialogRef,
    MD_DIALOG_DATA
} from '@angular/material';
import {
    ClipboardService
} from 'ng2-clipboard/ng2-clipboard';
import * as _ from 'underscore';
import { NotificationService } from '../../../../../../_services/notification.service';
import { Observable } from 'rxjs/Observable';;
import { OrderService } from '../../../../../../_services';
declare var $: any;
declare var swal: any;

@Component({
    selector: 'detail-order',
    templateUrl: 'detail-order.component.html',
    styleUrls: ['detail-order.component.scss']
})
export class DetailOrderDialogComponent {

    private orderDetail: any;
    private note = '';

    constructor(private orderService: OrderService, private notificationService: NotificationService, @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef < DetailOrderDialogComponent > ) {}

    public copyToClipboard() {
        this.clipboard.copy(this.data.message);
        this.dialogRef.close('');
    }

    ngOnInit() {
        let that = this;
        if (this.data !== undefined) {
            if (this.data !== undefined && this.data['order'] !== undefined) {
                this.orderDetail = this.data['order'];
                this.note = this.orderDetail.note;
            }
        }
    }

    public okClick() {
        if (!this.orderDetail) {
            this.notificationService.showDanger('Đơn hàng không nhận dạng được, vui lòng refesh lại trang!');
            return;
        }
        let actionOperateOrder: Observable<any>;
        actionOperateOrder = this.orderService.updateOrder({id: this.orderDetail.id, note: this.note.trim()});
        actionOperateOrder.subscribe(
            (data) => {
                if (data.errorCode !== undefined && data.errorCode === 200) {
                    this.dialogRef.close(this.note.trim());
                    this.orderDetail.note = this.note;
                    this.notificationService.showSuccess('Cập nhật đơn hàng thành công');
                } else {
                    this.notificationService.showDanger('Cập nhật đơn hàng thất bại');
                }
            },
            (err) => {
                console.log(err);
            });
    }
}