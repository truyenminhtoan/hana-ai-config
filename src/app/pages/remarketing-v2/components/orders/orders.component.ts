import { ApplicationService } from './../../../../../_services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { LazyLoadEvent } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
import { MdDialog } from '@angular/material';
import { GroupsDialogComponent } from '../components/groups-dialog/groups-dialog.component';
import _ from 'underscore';
import { SendMessageDialogComponent } from '../components/send-message-dialog/send-message-dialog.component';
import { SequenceDialogComponent } from '../components/sequence-dialog/sequence-dialog.component';
import { HistoryMessageDialogComponent } from '../components/history-message-dialog/history-message-dialog.component';
import { FacebookService } from '../../../../_services/facebook.service';
import { NotificationService } from '../../../../_services/notification.service';
import { OrderService } from '../../../../_services/order.service';
import { ConfigService } from '../../../../_services/config.service';
declare var jQuery: any;
declare var $: any;
declare var Base64: any;

import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { AddMenuDialogComponent } from '../setting-menus/components/add-menu/add-menu.components';
import { DetailOrderDialogComponent } from './components/detail-order/detail-order.component';
import { NavigationExtras } from '@angular/router/src/router';
type AOA = Array<Array<any>>;
declare var swal: any;
function s2ab(s: string): ArrayBuffer {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
    };
    return buf;
}

@Component({
    selector: 'order-list',
    templateUrl: 'orders.component.html',
    styleUrls: ['orders.component.scss']
})
export class OrdersComponent implements OnInit, AfterViewInit {

    private appId = 'c3514ef6-9ae0-4073-a725-d434fc9930fb';
    private partnerId: any;
    private pageSize = 20;

    private datasource: any[] = [];
    private cars: any[] = [];
    private orders: any[] = [];
    private totalRecords: number = 0;
    private selectedCustomers;
    private tableMessage = 'Không có dữ liệu';
    private selectedStatusOrder;
    private sortF = 'name';

    private data_exports = [];
    private list_checked_delete = [];

    /** cau hinh chuc nang doc excel*/
    data: AOA = [];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };
    fileName: string = "SheetJS.xlsx";

    /** Filter */
    private selectedCreateOrder: string = '';
    private inputFilter = '';
    private orderStatus = [
        { value: '', label: 'All' },
        { value: 'PreOrder', label: 'Đơn nháp' },
        { value: 'Order', label: 'Đơn chi tiết' },
        { value: 'OrderDone', label: 'Đơn đã xác nhận' }
    ];
    private sourceLists = [
        { value: '', label: 'All' },
        { value: 'facebook', label: 'Facebook' },
        { value: 'web', label: 'Web' }
    ];
    private userCreates = [];
    private rangeDates: Date[];
    private value: Date;
    private date_from = '';
    private date_to = '';
    private selectedSource;
    private id_loading = false;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private elRef: ElementRef,
        private facebookService: FacebookService,
        private orderService: OrderService,
        private dialog: MdDialog,
        private configService: ConfigService,
        private notificationService: NotificationService) {
        elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
    }

    public ngOnInit(): void {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.appId = currentUser.app_using.applications[0].app_id;
            this.partnerId = currentUser.app_using.id;
        }
        $.getScript('../../../assets/js/core/jquery-ui.min.1.12.1.js', () => {
            let h = $(window).height();
            $('.card').slimScroll({
                height: h - 60 + 'px'
            });
        });
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.appId = currentUser.app_using.applications[0].app_id;
        }
        this.search(0, this.pageSize, true);

        this.getuserCreate();

        let that = this;
        setInterval(function () {
            if (that.router.url == '/customers/orders') {
                $('body').addClass('sidebar-mini');
                $('.sidebar').hide();
                $(".main-panel").css("width", "100%");
                $(".container-fluid").css("background-color", "#fff");
            }
        }, 500);
    }

    public ngAfterViewInit() {
        $('body').addClass('sidebar-mini');
    }

    public getuserCreate() {
        let body = { "app_id": this.appId };
        let actionOperateOrder: Observable<any>;
        actionOperateOrder = this.orderService.getCreaterOrder(body);
        actionOperateOrder.subscribe(
            (users) => {
                console.log('users', users);
                if (users.data != undefined) {
                    this.userCreates.push({ value: '', label: 'All' });
                    for (let i = 0; i < users.data.length; i++) {
                        this.userCreates.push({ value: users.data[i], label: users.data[i] });
                    }
                } else {
                    this.userCreates = [];
                }
            },
            (err) => {
                console.log(err);
            });
    }

    public search(currentItem, pageSize, notCount) {
        if (this.rangeDates != undefined && this.rangeDates[0] != undefined && this.rangeDates[1] != undefined) {
            let day_from = this.rangeDates[0].getDate();
            let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
            let year_from = this.rangeDates[0].getFullYear();
            this.date_from = year_from + '-' + month_from + '-' + day_from;

            let day_to = this.rangeDates[1].getDate();
            let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
            let year_to = this.rangeDates[1].getFullYear();
            this.date_to = year_to + '-' + month_to + '-' + day_to;
        } else {
            if (this.rangeDates != undefined && this.rangeDates[0] != undefined) {
                let day_from = this.rangeDates[0].getDate();
                let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[0].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[0].getDate();
                let month_to = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[0].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else if (this.rangeDates != undefined && this.rangeDates[1] != undefined) {
                let day_from = this.rangeDates[1].getDate();
                let month_from = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[1].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[1].getDate();
                let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[1].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else {
                this.date_from = '';
                this.date_to = '';
            }
        }
        let request_body: any = {};
        request_body.app_id = this.appId;
        request_body.limit = this.pageSize;
        request_body.position = currentItem;
        if (this.selectedSource != undefined && this.selectedSource.trim().length > 0) {
            request_body['source'] = this.selectedSource;
        }
        if (this.selectedStatusOrder != undefined && this.selectedStatusOrder.trim().length > 0) {
            request_body['order_status'] = this.selectedStatusOrder;
        }
        if (this.selectedCreateOrder != undefined && this.selectedCreateOrder.trim().length > 0) {
            request_body['creater'] = this.selectedCreateOrder;
        }
        if (this.inputFilter != undefined && this.inputFilter.trim().length > 0) {
            request_body['key_search'] = this.inputFilter;
        }
        if (this.date_from != undefined && this.date_from.trim().length > 0) {
            request_body['time_start'] = this.date_from;
        }
        if (this.date_to != undefined && this.date_to.trim().length > 0) {
            request_body['time_end'] = this.date_to;
        }

        this.getListService(request_body);
    }

    public getTotalOrders(body) {
        let actionOperate: Observable<any>;
        body.has_fb_userchat = true; // chi lay kh co userchat
        actionOperate = this.orderService.getOrderByParam({ app_id: this.appId });
        actionOperate.subscribe(
            (data) => {
                console.log('data.data.orders', data.data.orders);
                if (data.data.count != undefined) {
                    this.totalRecords = data.data.count;
                }
                console.log('this.totalRecords ', this.totalRecords);
            },
            (err) => {

            });
    }

    public getListService(request_body) {
        // alert('request server');
        this.tableMessage = 'Đang tải dữ liệu';
        this.selectedCustomers = [];
        this.datasource = [];

        let actionOperateOrder: Observable<any>;
        actionOperateOrder = this.orderService.getOrderByParam(request_body);
        actionOperateOrder.subscribe(
            (data) => {
                if (request_body['position'] != undefined && request_body['position'] == 0 && data.data.count != undefined) {
                    this.totalRecords = data.data.count;
                }
                if (data.data.orders != undefined && data.data.orders != 'Khong co data') {
                    this.orders = data.data.orders;
                    if (this.orders.length < 1) {
                        this.tableMessage = 'Không có dữ liệu';
                    } else {
                        for (let i = 0; i < this.orders.length; i++) {
                            this.orders[i].stt = i + 1;
                            if (this.orders[i].order_status == "PreOrder") {
                                this.orders[i].order_status_vn = 'Đơn nháp';
                            } else {
                                this.orders[i].order_status_vn = 'Đơn chi tiết';
                            }
                            if (this.list_checked_delete.indexOf(this.orders[i].id) === -1) {
                                this.orders[i].status = false;
                            } else {
                                this.orders[i].status = true;
                            }
                        }
                    }
                } else {
                    this.orders = [];
                    this.tableMessage = 'Không có dữ liệu';
                }
            },
            (err) => {
                console.log(err);
            });
    }

    public loadCarsLazy(event: LazyLoadEvent) {
        this.pageSize = event.rows;
        this.search(event.first, event.rows, false);
    }

    public onRowSelect(event) {
        // this.router.navigate(['/pages/services/serviceEdit/' + event.data.id], { relativeTo: this.route });

    }

    public changeSort(event) {
        if (!event.order) {
            this.sortF = 'name';
        } else {
            this.sortF = event.field;
        }
    }

    public exportExcel() {
        if (!this.appId) {
            return;
        }
        this.id_loading = true;
        let data_dowload = [];
        let header = [
            'STT',
            'SỐ ĐIỆN THOẠI',
            'HỌ VÀ TÊN',
            'ĐỊA CHỈ',
            'NGUỒN KÊNH',
            'MÃ SẢN PHẨM',
            'MÔ TẢ',
            'SỐ LƯỢNG',
            'ĐƠN GIÁ',
            'TRẠNG THÁI',
            'NGÀY TẠO'
        ];

        data_dowload.push(header);


        if (this.rangeDates != undefined && this.rangeDates[0] != undefined && this.rangeDates[1] != undefined) {
            let day_from = this.rangeDates[0].getDate();
            let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
            let year_from = this.rangeDates[0].getFullYear();
            this.date_from = year_from + '-' + month_from + '-' + day_from;
            let day_to = this.rangeDates[1].getDate();
            let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
            let year_to = this.rangeDates[1].getFullYear();
            this.date_to = year_to + '-' + month_to + '-' + day_to;
        } else {
            if (this.rangeDates != undefined && this.rangeDates[0] != undefined) {
                let day_from = this.rangeDates[0].getDate();
                let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[0].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[0].getDate();
                let month_to = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[0].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else if (this.rangeDates != undefined && this.rangeDates[1] != undefined) {
                let day_from = this.rangeDates[1].getDate();
                let month_from = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[1].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[1].getDate();
                let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[1].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else {
                this.date_from = '';
                this.date_to = '';
            }
        }
        let request_body: any = {};
        request_body.app_id = this.appId;
        if (this.selectedSource != undefined && this.selectedSource.trim().length > 0) {
            request_body['source'] = this.selectedSource;
        }
        if (this.selectedStatusOrder != undefined && this.selectedStatusOrder.trim().length > 0) {
            request_body['order_status'] = this.selectedStatusOrder;
        }
        if (this.selectedCreateOrder != undefined && this.selectedCreateOrder.trim().length > 0) {
            request_body['creater'] = this.selectedCreateOrder;
        }
        if (this.inputFilter != undefined && this.inputFilter.trim().length > 0) {
            request_body['key_search'] = this.inputFilter;
        }
        if (this.date_from != undefined && this.date_from.trim().length > 0) {
            request_body['time_start'] = this.date_from;
        }
        if (this.date_to != undefined && this.date_to.trim().length > 0) {
            request_body['time_end'] = this.date_to;
        }
        let actionOperateOrder: Observable<any>;
        actionOperateOrder = this.orderService.getOrderByParam(request_body);
        actionOperateOrder.subscribe(
            (data) => {
                if (data.data.orders != undefined && data.data.orders != 'Khong co data') {
                    this.data_exports = data.data.orders;
                    if (this.data_exports.length > 0) {
                        for (let i = 0; i < this.data_exports.length; i++) {
                            let row = [
                                this.data_exports[i].stt,
                                this.data_exports[i].phone,
                                this.data_exports[i].name,
                                this.data_exports[i].address,
                                this.data_exports[i].source,
                                this.data_exports[i].product_code,
                                this.data_exports[i].note,
                                this.data_exports[i].quality,
                                this.data_exports[i].price,
                                this.data_exports[i].order_status_vn,
                                this.data_exports[i].created_date,
                            ];
                            data_dowload.push(row);
                        }
                    }
                    this.id_loading = false;
                    this.exportByData(data_dowload, 'order.xlsx');
                } else {
                    this.id_loading = false;
                    this.notificationService.showDanger("Lỗi khi lấy dữ liệu!");
                }
            },
            (err) => {
                this.id_loading = false;
                console.log(err);
            });
    }

    exportByData(data, file_name): void {
        /* generate worksheet */
        const ws = XLSX.utils.aoa_to_sheet(data);

        /* set width for colum */
        var wscols = [
            // {wch:20}
        ];
        var wsrows = [
            // {wch:20}
        ];
        wsrows.push({ hpx: 26 });
        if (data[0] != undefined) {
            var n_column = data[0].length;
            wscols.push({ wch: 5 });
            wscols.push({ wch: 20 });
            for (var i = 1; i < n_column; i++) {
                wscols.push({ wch: 20 });
            }
            ws['!cols'] = wscols;
            ws['!rows'] = wsrows;
        }
        /* generate workbook and add the worksheet */
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        const wbout = XLSX.write(wb, this.wopts);
        saveAs(new Blob([s2ab(wbout)]), file_name);
    }


    public onFilterDate() {
        this.list_checked_delete = [];
        if (this.rangeDates != undefined && this.rangeDates[0] != undefined && this.rangeDates[1] != undefined) {
            let day_from = this.rangeDates[0].getDate();
            let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
            let year_from = this.rangeDates[0].getFullYear();
            this.date_from = year_from + '-' + month_from + '-' + day_from;

            let day_to = this.rangeDates[1].getDate();
            let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
            let year_to = this.rangeDates[1].getFullYear();
            this.date_to = year_to + '-' + month_to + '-' + day_to;
        } else {
            if (this.rangeDates != undefined && this.rangeDates[0] != undefined) {
                let day_from = this.rangeDates[0].getDate();
                let month_from = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[0].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[0].getDate();
                let month_to = this.rangeDates[0].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[0].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else if (this.rangeDates != undefined && this.rangeDates[1] != undefined) {
                let day_from = this.rangeDates[1].getDate();
                let month_from = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_from = this.rangeDates[1].getFullYear();
                this.date_from = year_from + '-' + month_from + '-' + day_from;

                let day_to = this.rangeDates[1].getDate();
                let month_to = this.rangeDates[1].getMonth() + 1; // add 1 because months are indexed from 0
                let year_to = this.rangeDates[1].getFullYear();
                this.date_to = year_to + '-' + month_to + '-' + day_to;
            } else {
                this.date_from = '';
                this.date_to = '';
            }
        }

        let request_body: any = {};
        request_body.app_id = this.appId;
        request_body.limit = this.pageSize;
        request_body.position = 0;

        if (this.selectedSource != undefined && this.selectedSource.trim().length > 0) {
            request_body['source'] = this.selectedSource;
        }
        if (this.selectedStatusOrder != undefined && this.selectedStatusOrder.trim().length > 0) {
            request_body['order_status'] = this.selectedStatusOrder;
        }
        if (this.selectedCreateOrder != undefined && this.selectedCreateOrder.trim().length > 0) {
            request_body['creater'] = this.selectedCreateOrder;
        }
        if (this.inputFilter != undefined && this.inputFilter.trim().length > 0) {
            request_body['key_search'] = this.inputFilter;
        }
        if (this.date_from != undefined && this.date_from.trim().length > 0) {
            request_body['time_start'] = this.date_from;
        }
        if (this.date_to != undefined && this.date_to.trim().length > 0) {
            request_body['time_end'] = this.date_to;
        }

        this.getListService(request_body);
    }

    public onFilterDateClear() {

        let request_body = { "app_id": this.appId };
        if (this.selectedSource != undefined && this.selectedSource.trim().length > 0) {
            request_body['source'] = this.selectedSource;
        }
        if (this.selectedStatusOrder != undefined && this.selectedStatusOrder.trim().length > 0) {
            request_body['order_status'] = this.selectedStatusOrder;
        }
        if (this.selectedCreateOrder != undefined && this.selectedCreateOrder.trim().length > 0) {
            request_body['creater'] = this.selectedCreateOrder;
        }
        if (this.inputFilter != undefined && this.inputFilter.trim().length > 0) {
            request_body['key_search'] = this.inputFilter;
        }

        this.getListService(request_body);
    }


    private checked(item, event) {
        console.log('event', event);
        if (event) {
            if (this.list_checked_delete.indexOf(item.id) === -1) {
                this.list_checked_delete.push(item.id);
            }
        } else {
            this.list_checked_delete = this.list_checked_delete.filter(function (row) {
                return row !== item.id;
            });
        }
        console.log('this.list_checked_delete', this.list_checked_delete);
    }

    private deleteOrders() {
        if (this.list_checked_delete.length === 0) {
            this.notificationService.showDanger("Không có đơn hàng nào được chọn");
            return;
        }
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            let actionOperateOrder: Observable<any>;
            actionOperateOrder = this.orderService.destroyListOrder({ orderIds: this.list_checked_delete });
            actionOperateOrder.subscribe(
                (data) => {
                    if (data.errorCode != undefined && data.errorCode == 200) {
                        this.notificationService.showSuccess("Xóa (" + this.list_checked_delete.length + " đơn hàng) thành công!");
                        this.search(0, this.pageSize, true);
                        this.list_checked_delete = [];
                    } else {
                        this.notificationService.showDanger("Xóa đơn hàng thất bại");
                    }
                },
                (err) => {
                    console.log(err);
                });
        }, (dismiss) => {
            //console.log(dismiss);
        });

    }

    private showEditOrder(order) {
        let dialogRef = this.dialog.open(DetailOrderDialogComponent, {
            width: '50%',
            data: {
                'order': order,
            }
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data) {

            }
        });
    }

    private showChat(roomId) {
        console.log('room_id', roomId);
        let navi = '@' + Base64.encode(roomId);
        let navigationExtras: NavigationExtras = {
            queryParams: { p: navi },
            fragment: 'hana'
        };
        this.router.navigate(['/' + this.partnerId + '/messages'], navigationExtras);
    }

}
