import { Component, OnInit, ViewChildren} from '@angular/core';
import { NotificationService } from '../../../_services/notification.service';
import { StringFormat } from '../../../themes/validators/string.format';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import {ProductsService} from "../../../_services/products.service";
import {ProductsModel} from "../../../_models/products.model";
type AOA = Array<Array<any> >;
declare var swal: any;

function s2ab(s:string):ArrayBuffer {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
    };
    return buf;
}

@Component({
    selector: 'product-data',
    templateUrl: 'product-data.component.html',
    styleUrls: ['product-data.component.scss'],
    providers: [ProductsService]
})
export class ProductDataComponent implements OnInit {

    @ViewChildren('file') file;
    private productId: any;
    private sub: any;
    private data_products = [];
    // cau hinh phan trang
    public filterQuery = "";
    public rowsOnPage = 5;
    public sortBy = "name";
    public sortOrder = "asc";
    // list param of products
    private row_params_product_cur = [];
    private row_params_product_cur_all = [];
    // cusr product
    private product: ProductsModel;
    // data push
    private data_push = [];
    // cau hinh chuc nang doc excel
    data:AOA = [];
    wopts:XLSX.WritingOptions = { bookType:'xlsx', type:'binary' };
    fileName:string = "SheetJS.xlsx";
    private data_product_full = "";
    private is_loading = true;

    private keys_list_of_product = [];

    private colspan = 5;

    private data_export = [];

    // cau hinh chuc nang doc excel
    data_select_file:AOA = [];

    constructor(
        private productService: ProductsService,
        private route: ActivatedRoute,
        private notificationService: NotificationService) {
        // my code
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe((params) => {
            this.productId = params['id'];
            // load product
            this.productService.getPromiseProductById(this.productId)
                .then((product) => {
                    this.fileName = product.name;
                    var row_params = [];
                    product.product_criterias.sort(function(a, b) {
                        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        // names must be equal
                        return 0;
                    });
                    this.row_params_product_cur_all = [];
                    for (var i = 0; i < product.product_criterias.length; i++) {
                        if (product.product_criterias[i].type == "criteria") {
                            if (row_params.length < 5) {
                                row_params.push(product.product_criterias[i].param);
                            }
                            this.row_params_product_cur_all.push(product.product_criterias[i].param);
                        }
                    }
                    for (var i = 0; i < product.product_criterias.length; i++) {
                        if (product.product_criterias[i].type != "criteria") {
                            if (row_params.length < 5) {
                                row_params.push(product.product_criterias[i].param);
                            }
                            this.row_params_product_cur_all.push(product.product_criterias[i].param);
                        }
                    }
                    this.row_params_product_cur = row_params;


                    this.getList();
                    this.getListDataExport();
                });
        });

    }


    //############################# CHUC NANG DOC EXCEL #################################
    public onFileChange(evt:any) {
        try {
            const scope = this;
            scope.data_select_file = [];
            /* wire up file reader */
            const target:DataTransfer = (<DataTransfer>(evt.target));
            if(target.files.length != 1) throw new Error("Cannot upload multiple files on the entry");
            const reader = new FileReader();
            reader.onload = function (e:any) {
                /* read workbook */
                const bstr = e.target.result;
                //console.log('e.target', e.target);
                try {
                    const wb = XLSX.read(bstr, {type:'binary'});
                    /* grab first sheet */
                    const wsname = wb.SheetNames[0];
                    var ws = wb.Sheets[wsname];
                    var wscols = [
                        {wch:200}
                    ];
                    ws['!cols'] = wscols;
                    //console.log('ws', ws);
                    /* save data to scope */
                    scope.data_select_file = (<AOA>(XLSX.utils.sheet_to_json(ws, {header:1})));
                    console.log('scope.data_select_file', scope.data_select_file);
                    scope.pushDataToService(scope.data_select_file);
                }
                catch(err) {
                    scope.getList();
                    scope.getListDataExport();
                    scope.notificationService.showDanger("Tập tin không đúng định dạng!");
                }
            };
            reader.readAsBinaryString(target.files[0]);
        }
        catch(err) {
            this.notificationService.showDanger("Vui lòng kiểm tra lại file của bạn!");
        }

    }

    pushDataToService(data) {
        if (data.length > 0) {
            var data_push = [];
            var list_keys = data[0]
            //Kiem tra param tren file xem co dung dinh dang khong
            if (JSON.stringify(list_keys)== JSON.stringify(this.row_params_product_cur_all)) {
                //console.log('data@@@@@@@', data);
                //console.log('this.row_params_product_cur.length', this.row_params_product_cur.length);
                for (var i = 1; i < data.length; i++) {
                    if (data[i].length > 0) {
                        var row = {
                            product_id: this.productId
                        };
                        for (var j = 0; j < this.row_params_product_cur_all.length; j++) {
                            if (data[i][j] == undefined) {
                                data[i][j] = '';
                            }
                            row[list_keys[j]] = data[i][j];
                        }
                        data_push.push(row);
                    }
                }
                if (this.data_select_file.length > 0) {
                    var data_tmp = [];
                    data_tmp.push(list_keys);
                    for (var i = 1; i < this.data_select_file.length; i++) {
                        data_tmp.push([{'id': '', data: this.data_select_file[i]}]);
                    }
                    this.data_select_file = data_tmp;
                }
                this.data_push = data_push;
            } else {
                this.data_select_file = [];
                this.getList();
                this.getListDataExport();
                this.notificationService.showDanger("Tập tin của bạn có dữ liệu không hợp lệ. Vui lòng kiểm tra lại!");
            }
        }
    }

    export():void {
        var data_export = [];
        //console.log('this.data_export@@@@@@@', this.data_export);
        if (this.data_export.length > 0) {
            // push param
            data_export.push(this.row_params_product_cur_all);
            // push value
            for (var i = 0; i < this.data_export.length; i++) {
                data_export.push(this.data_export[i][0].data);
            }
        }

        /* generate worksheet */
        const ws = XLSX.utils.aoa_to_sheet(data_export);

        /* set width for colum */
        var wscols = [
            // {wch:20}
        ];
        if (this.data_export[0] != undefined) {
            var n_column = this.data_export[0].length;
            for (var i = 0; i < n_column; i++) {
                wscols.push({wch:25});
            }
            // var wscols = [
            //     {wch:20}
            // ];
            ws['!cols'] = wscols;
        }

        /* generate workbook and add the worksheet */
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        const wbout = XLSX.write(wb, this.wopts);
        //console.log(this.fileName);
        saveAs(new Blob([s2ab(wbout)]), 'export_'+ StringFormat.clearTextVi(this.fileName) +'.xlsx');
    }
    //############################# END CHUC NANG DOC EXCEL #################################

    dowload_file() {
        var data_dowload = [];
        data_dowload.push(this.row_params_product_cur_all);
        this.exportByData(data_dowload, 'export_'+ StringFormat.clearTextVi(this.fileName) +'.xlsx');
    }

    exportByData(data, file_name):void {
        /* generate worksheet */
        const ws = XLSX.utils.aoa_to_sheet(data);

        /* set width for colum */
        var wscols = [
            // {wch:20}
        ];
        if (data[0] != undefined) {
            var n_column = data[0].length;
            for (var i = 0; i < n_column; i++) {
                wscols.push({wch:20});
            }
            ws['!cols'] = wscols;
        }
        /* generate workbook and add the worksheet */
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        const wbout = XLSX.write(wb, this.wopts);
        saveAs(new Blob([s2ab(wbout)]), file_name);
    }

    addListsProductItem() {
        var body = JSON.stringify(this.data_push);
        //return;
        this.productService.addListsProductItem(body)
            .then((res) => {
                if (res.errorCode == 200) {
                    this.getList();
                    this.getListDataExport();
                    this.notificationService.showSuccess('Thêm dữ liệu sản phẩm thành công');
                } else {
                    this.notificationService.showDanger('Xảy ra lỗi trong quá trình thêm dữ liệu sản phẩm');
                }
            });
    }

    save() {
        swal({
            title: 'Dữ liệu sản phẩm đang có sẽ mất đi. Bạn có muốn tiếp tục?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            if (this.data_push.length > 0) {
                this.productService.destroyProductItem(this.productId)
                    .then((res) => {
                        this.addListsProductItem();
                    });
            } else {
                this.notificationService.showWarning('File bạn chọn không có dữ liệu hoặc bạn chưa chọn file');
            }
        }, (dismiss) => {
            console.log(dismiss);
        });
    }


    getListDataExport() {
        this.data_export = [];
        this.productService.getListsProductItem(this.productId)
            .then((res) => {
                if (res.errorCode == 200) {
                    var data_products = res.data;
                    // dong dau tien la mang cac key
                    this.data_export.push(this.row_params_product_cur_all);
                    // cac dong tiep theo la value
                    for (var i = 0; i < data_products.length; i++) {
                        var data_row = [];
                        // xoa cac key khong dung
                        var id = data_products[i]._id;
                        delete data_products[i].created_dete_date;
                        delete data_products[i].status;
                        delete data_products[i].product_id;
                        delete data_products[i].hash_code;
                        delete data_products[i].modified_date;
                        delete data_products[i].created_dete_;
                        delete data_products[i]._id;
                        var keys_row = this.row_params_product_cur_all;
                        if (keys_row.length > 0) {
                            for (var j = 0 ; j < keys_row.length; j++) {
                                data_row.push(data_products[i][keys_row[j]]);
                            }
                        }
                        this.data_export.push([{'id': id, data: data_row, 'index': (i + 1)}]);
                    }
                    // o day co khi data bi thua mang rong
                    this.data_export = this.data_export.filter((row) => {
                        return row.length > 0
                    });
                }
                // param
                this.data_export[0] = this.row_params_product_cur_all;
                this.is_loading = false;
                // loai data[0]
                var tmp_data = [];
                for (var i = 1; i < this.data_export.length; i++) {
                    tmp_data.push(this.data_export[i]);
                }
                this.data_export = tmp_data;
                console.log('this.data_export', this.data_export);

            });
    }
    
    getList() {
        this.data = [];
        this.productService.getListsProductItem(this.productId)
            .then((res) => {
                if (res.errorCode == 200) {
                    this.data_products = res.data;
                    this.data_product_full = JSON.stringify(res.data);
                    // dong dau tien la mang cac key
                    this.data.push(this.row_params_product_cur);
                    // cac dong tiep theo la value
                    for (var i = 0; i < this.data_products.length; i++) {
                        var data_row = [];
                        // xoa cac key khong dung
                        var id = this.data_products[i]._id;
                        delete this.data_products[i].created_dete_date;
                        delete this.data_products[i].status;
                        delete this.data_products[i].product_id;
                        delete this.data_products[i].hash_code;
                        delete this.data_products[i].modified_date;
                        delete this.data_products[i].created_dete_;
                        delete this.data_products[i]._id;
                        var keys_row = this.row_params_product_cur;
                        if (keys_row.length > 0) {
                            for (var j = 0 ; j < keys_row.length; j++) {
                                if (data_row.length < 5) {
                                    data_row.push(this.data_products[i][keys_row[j]]);
                                }
                            }
                        }
                        this.data.push([{'id': id, data: data_row, 'index': (i + 1)}]);
                    }
                    // o day co khi data bi thua mang rong
                    this.data = this.data.filter((row) => {
                       return row.length > 0
                    });
                    //console.log('keys_pri', this.row_params_product_cur);
                    //console.log('this.data', JSON.stringify(this.data));
                } else {
                    //this.notificationService.showWarning('Dữ liệu sản phẩm rỗng');
                }
                
                // param
                this.data[0] = this.row_params_product_cur;
                this.is_loading = false;
                this.colspan = this.data[0].length;

                // loai data[0]
                var tmp_data = [];
                for (var i = 1; i < this.data.length; i++) {
                    tmp_data.push(this.data[i]);
                }
                this.data = tmp_data;

            });
    }


    deleteProductItem(row) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            if (row[0].id != undefined && row[0].id.trim().length > 0) {
                // xoa product item tren server
                this.productService.deleteProductItem(row[0].id)
                    .then((res) => {
                        this.notificationService.showSuccess('Xóa dữ liệu sản phẩm thành công!');
                        this.getList();
                        this.getListDataExport();
                    });
            } else {
                this.data = this.data.filter((item) => {
                    return item != row
                });
            }
        }, (dismiss) => {
            console.log(dismiss);
        });
    }
}
