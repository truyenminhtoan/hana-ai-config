
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
import {ProductsService} from "../../../../_services/products.service";
import {NotificationService} from "../../../../_services/notification.service";

@Component({
    selector: 'product-item-edit',
    templateUrl: 'product-item-edit.component.html',
    styleUrls: ['product-item-edit.component.scss'],
    providers: [ProductsService]
})
export class ProductItemEditComponent implements OnInit {
    
    private data_products = [];
    private data_product_items : any;
    //keys
    private data_product_items_keys = [];
    private is_loading = true;
    private product_id: string;
    private product_item_id: string;
    private sub: any;

    constructor(
        private productService: ProductsService,
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService) {
            
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe((params) => {
            this.product_id = params['id_product'];
            this.product_item_id = params['id_product_item'];
            this.getDataProductItem();
        });
    }

    getDataProductItem() {

        this.productService.getPromiseProductById(this.product_id)
            .then((product) => {
                var row_params = [];
                product.product_criterias.sort(function(a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                for (var i = 0; i < product.product_criterias.length; i++) {
                    if (product.product_criterias[i].type == "criteria") {
                        row_params.push(product.product_criterias[i].param);
                    }
                }
                for (var i = 0; i < product.product_criterias.length; i++) {
                    if (product.product_criterias[i].type != "criteria") {
                        row_params.push(product.product_criterias[i].param);
                    }
                }
                //row_params.push('product_id');
                //console.log('row_params', row_params);
                this.productService.getListsProductItem(this.product_id)
                    .then((res) => {
                        if (res.errorCode == 200) {
                            this.data_products = res.data;
                            var tmp_data_product_items = [];
                            for (var i = 0; i < this.data_products.length; i++) {
                                // xoa cac key khong dung
                                var id = this.data_products[i]._id;
                                //this.product_item_id =  this.data_products[i]._id;
                                if (id == this.product_item_id) {
                                    delete this.data_products[i].created_dete_date;
                                    delete this.data_products[i].status;
                                    delete this.data_products[i].product_id;
                                    delete this.data_products[i].hash_code;
                                    delete this.data_products[i].modified_date;
                                    delete this.data_products[i].created_dete_;
                                    //delete this.data_products[i]._id;
                                    if (row_params.length > 0) {
                                        for (var j = 0 ; j < row_params.length; j++) {
                                            var key_row = row_params[j];
                                            var value_row = this.data_products[i][key_row];
                                            tmp_data_product_items[key_row] = value_row;
                                        }
                                    }
                                }
                            }

                            this.data_product_items = tmp_data_product_items;
                            this.is_loading = false;
                            this.data_product_items_keys = Object.keys(this.data_product_items);
                            //console.log('tmp_data_product_items', tmp_data_product_items);
                            //console.log('data_product_items_keys', this.data_product_items_keys);
                        } else {
                            this.notificationService.showWarning('Xảy ra lỗi khi lấy dữ liệu sản phẩm');
                        }
                    });

            });



    }

    public saveForm() {
        // check Phải có ít nhất một thuộc tính có dữ liệu moi cho luu
        var check = false;
        for (var i = 0; i < this.data_product_items_keys.length; i++) {
            if (this.data_product_items[this.data_product_items_keys[i]].trim().length > 0) {
                check = true;
                break;
            }
        }
        if (!check) {
            this.notificationService.showWarning('Phải có ít nhất một thuộc tính có dữ liệu');
            return;
        }

        var obj_push = {};
        for (var i = 0; i < this.data_product_items_keys.length; i++) {
            obj_push[this.data_product_items_keys[i]] = this.data_product_items[this.data_product_items_keys[i]].trim();
        }
        obj_push['_id'] = this.product_item_id;




        this.productService.updatePromiseProductItem(obj_push)
            .then((res) => {
                this.notificationService.showSuccess('Cập nhật dữ liệu sản phẩm thành công');
            });
    }

}
