import { Component, OnInit, Input, OnChanges, ViewEncapsulation } from '@angular/core';
import { ElementRef, ViewChild, Pipe, Injectable } from '@angular/core';
import { Router, ActivatedRoute, Params, Route } from '@angular/router';
import { ProductsService } from '../../../_services/index';
import { ProductsModel} from '../../../_models/index';
import { NotificationService } from "../../../_services/notification.service";
import {UserModel} from "../../../_models/user.model";
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
declare var $: any;
import {Subscription} from 'rxjs/Rx';
declare var swal: any;

@Component({
    selector: 'product-list',
    templateUrl: 'product-list.component.html',
    styleUrls: ['product-list.component.scss'],
    providers: [ProductsService]
})

export class ProductListComponent implements OnInit {
    
    @ViewChild('inputSearch') input: ElementRef;
    @ViewChild('buttonSearch') buttonSearch: ElementRef;
    private products = [];
    private currentUser: UserModel;
    public filterQuery = "";
    public rowsOnPage = 5;
    public sortBy = "name";
    public sortOrder = "asc";
    private id_loading = true;
    private eventSubscription: Subscription;

    constructor(private productService: ProductsService, private route: ActivatedRoute,
        private router: Router, private notificationService: NotificationService,
                private _componentService: ComponentInteractionService) {
    }
    
    ngOnInit() {
        this.loadListProducts();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-products') {
                    this.loadListProducts();
                }
            }
        );
    }

    public loadListProducts() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.productService.getListProductByAgentId(agentId)
                .then((products) => {
                    this.products = products;
                    if (this.products != undefined && this.products.length > 0) {
                        for (var i = 0; i < this.products.length; i++) {
                            if (this.products[i].description != null && this.products[i].description.length > 0) {
                                this.products[i].description = this.products[i].description.slice(0, 90);
                                if (this.products[i].description.length >= 90) {
                                    this.products[i].description += '...';
                                }
                            }
                        }
                    }
                    this.id_loading = false;
                })
        } else {
            this.notificationService.showNoAgentId();
        }
    }

    public showProductDetail(product: ProductsModel): void {
        this.router.navigate(['/pages/products/detail-product/' + product.id]);
    }

    public deleteProduct(productId) {
        var that = this;
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            that.productService.deleteProductById(productId)
                .then((res) => {
                    //this._componentService.eventPublisher$.next('load-products');
                    that.products = that.products.filter((row) => {
                        return row.id != productId
                    });
                    that.notificationService.showSuccess('Xóa sản phẩm thành công');
                });
        }, (dismiss) => {
            console.log(dismiss);
        });
    }
    
    
}
