import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductDataComponent } from './product-data-component/product-data.component';
import {ProductItemEditComponent} from "./product-data-component/product-item-edit-component/product-item-edit.component";
const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [
      { path: 'product-list', component: ProductListComponent },
      { path: 'product-new', component: ProductNewComponent },
      { path: 'product-detail/:id', component: ProductDetailComponent },
      // quan ly du lieu san pham
      { path: 'product-data-list/:id', component: ProductDataComponent },
      { path: 'product-item-edit/:id_product/:id_product_item', component: ProductItemEditComponent },
    ]
  },
];

// tslint:disable-next-line:eofline
export const routing = RouterModule.forChild(routes);
