import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { StringFormat } from '../../../themes/validators';

@Pipe({
  name: 'searchPipe',
  pure: false
})
export class ActionsPipe implements PipeTransform {
    public transform(data: any[], searchTerm: string): any[] {
        if (searchTerm != null && searchTerm.trim().length > 0) {
            if (StringFormat.checkRegexp(searchTerm.trim(), /^[a-zA-Z0-9_ ]+$/)) {
                searchTerm = StringFormat.formatText(searchTerm.trim());
                return data.filter(item => {
                    return StringFormat.formatText(item.name).indexOf(searchTerm.trim()) !== -1
                });
            } else {
                return [];
            }
        } else {
            return data;
        }
    }
}
