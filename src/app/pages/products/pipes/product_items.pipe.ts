
import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { StringFormat } from '../../../themes/validators';

@Pipe({
    name: 'productItemPipe',
    pure: false
})
export class ProductItemPipe implements PipeTransform {
    public transform(data: any[], searchTerm: string): any[] {
        if (searchTerm != null && searchTerm.trim().length > 0) {
            var data_param = [];
            if (data != undefined && data.length > 0) {
                data_param = data[0];
                console.log('searchTerm', searchTerm);
                console.log('data', data);
                return data.filter(item => {
                    return this.checkExist(searchTerm, item[0].data)
                });
            } else {
                return [];
            }
        } else {
            return data;
        }
    }

    private checkExist(value, list) {
        var check = false;
        for (let i = 0; i < list.length; i++) {
            let value_cp = StringFormat.formatTextUserSays(list[i]);
            var value_format = StringFormat.formatTextUserSays(value);
            if (value_cp.indexOf(value_format) !== -1
                || list[i].indexOf(value.trim()) !== -1
                || (list[i].trim() == value.trim())) {

                check = true;
            }
        }
        return check;
    }
}
