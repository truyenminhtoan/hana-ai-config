import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { EntityService } from '../../../../_services/entity.service';
import {Subscription} from 'rxjs/Rx';
import {ComponentInteractionService} from "../../../../_services/compoent-interaction-service";
import * as _ from 'underscore';
@Component({
    selector: 'select2-custome',
    templateUrl: 'select2-custome.component.html',
    styleUrls: ['select2-custome.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class Select2CustomeComponent {

    @Input() customeValues: any;

    @Input() customeActive: any;

    @Input() cusDisabled: any;

    @Output() customeActiveChange = new EventEmitter();

    // private value: any = {};
    public active: Array<Object> = [];
    private eventSubscription: Subscription;

    constructor(private entityService: EntityService, private _componentService: ComponentInteractionService) {
    }
    
    public ngOnInit() {

        if (this.customeActive) {
            this.loadEntity(this.customeActive);
        }
        //Event listener
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-entity-select') {
                    // update entity list
                    let agentId = localStorage.getItem('currentAgent_Id');
                    if (agentId) {
                        this.entityService.getListEntityPromise(agentId, false)
                            .then((data) => {
                                this.customeValues = this.setValueText(data);
                            })
                    }
                }
            }
        );
    }

    private setValueText(enties) {
        _.each(enties, function (value) {
            value.text = value.name;
        });
        return enties;
    }

    public loadEntity(entityId: string) {
        this.entityService.getEntityById(entityId).subscribe((data) => {
            this.active = [{ id: this.customeActive, text: data.name }];
        }, (error) => {
        });
    }
    public selected(value: any): void {
        this.customeActive = value.id;
        this.customeActiveChange.emit(this.customeActive);
    }

    public removed(value: any): void {

    }

    public typed(value: any): void {

    }

    public refreshValue(value: any): void {
        this.customeActive = value;
    }

}
