import { Component, ViewEncapsulation, ViewChildren } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductsService, Logger, EntityService } from '../../../_services/index';
import { ProductsModel, ProductCriteria } from '../../../_models/index';
import { StringFormat } from '../../../themes/validators/string.format';
import * as _ from 'underscore';
import { EntitieModel } from '../../../_models/entities.model';
import { NotificationService } from '../../../_services/notification.service';
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {ValidationService} from "../../../_services/ValidationService";
import {ViewChild, ElementRef } from '@angular/core';
import {UserModel} from "../../../_models/user.model";
import {Subscription} from 'rxjs/Rx';

@Component({
    selector: 'product-detail',
    templateUrl: 'product-detail.component.html',
    encapsulation: ViewEncapsulation.Emulated,
    styleUrls: ['product-detail.component.scss'],
    providers: [ProductsService, Logger, EntityService]
})
export class ProductDetailComponent {
    private productId: any;
    private products = new ProductsModel();
    private productsCri: ProductCriteria;
    // toantm
    private productNameOld = '';
    private productCriterias: ProductCriteria[];
    private productCriteriasTmp = [];
    private entities: EntitieModel[];
    private entity = new EntitieModel();
    private productCriteriasNew = [];
    private sub: any;
    private buttonClick = false;
    private formError = false;
    private entityName: string;
    private checkboxData = { true: 'criteria', false: 'attribute' };
    complexForm: FormGroup;
    private currentEntity = new EntitieModel();
    private lists = [];
    private is_load_list_product = true;
    private is_load_list_entities = true;
    private is_load_product_edit = true;
    private is_update = true;
    private currentUser: UserModel;
    private currentAgentId = '';
    @ViewChild('btnCloseAddEntity') btnCloseAddEntity: ElementRef;
    @ViewChildren('input_auto') inputs;
    @ViewChild('entity_name') entity_name: ElementRef;
    @ViewChild('productName') input_product_name: ElementRef;
    private eventSubscription: Subscription;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder,
                private productService: ProductsService,
                private entitieService: EntityService,
                private notificationService: NotificationService,
                private _componentService: ComponentInteractionService,
                private validationService: ValidationService) { }

    public ngOnInit() {

        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'add-entity_product') {
                    this.addNewEntityProduct();
                }
            }
        );

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser && this.currentUser.partner_id) {
            this.currentAgentId = localStorage.getItem('currentAgent_Id');
            if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
                this.sub = this.route.params.subscribe((params) => {
                    this.productId = params['id'];
                });
                this.loadEnties();
                this.loadProduct();
                this.loadComplexForm();
                this.loadProductCriterias();
                this.loadListProducts();

                this.currentEntity.name = '';
                this.currentEntity.type = 0;
                this.currentEntity.is_system = 0;
                this.currentEntity.status = 1;
                this.currentEntity.agent_id = this.currentAgentId;
                this.currentEntity.type = 1;

            } else {
                this.notificationService.showNoAgentId();
            }
        } else {
            this.notificationService.showNoPartnerId();
        }
    }

    public loadListProducts() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.productService.getListProductByAgentId(agentId)
                .then((products) => {
                    this.lists = products;
                    this.is_load_list_product = false;
                })
        } else {
            this.notificationService.showNoAgentId();
        }
    }

    private submitForm(value: any) {
        this.products.name = this.products.name.trim();
        if (this.products.name === undefined || (this.products != undefined && this.products.name.length ==0)) {
            this.notificationService.showWarning('Vui lòng nhập tên sản phẩm');
            this.input_product_name.nativeElement.focus();
        } else {
            if (!this.validationService.validateAlphabet(this.products.name)) {
                this.input_product_name.nativeElement.focus();
                this.notificationService.showWarning('Tên sản phẩm chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
            } else {

                if (this.products.name.trim().length > 255) {
                    this.input_product_name.nativeElement.focus();
                    this.notificationService.showWarning('Tên sản phẩm chỉ cho phép nhập tối đa 255 ký tự');
                    return;
                }
                if (this.products.description.trim().length > 255) {
                    this.notificationService.showWarning('Mô tả chỉ cho phép nhập tối đa 255 ký tự');
                    return;
                }

                if (this.productCriterias.length == 0 && this.productCriteriasTmp.length == 0) {
                    this.notificationService.showWarning('Phải có ít nhất một thuộc tính có dữ liệu');
                    return;
                }

                var entity_names = [];
                this.entitieService.getListEntityPromise(localStorage.getItem('currentAgent_Id'), 1) .then((data) => {
                    entity_names = _.pluck(data, 'name');
                    if (this.productCriterias != undefined && this.productCriterias.length > 0) {
                        for (var i = 0; i < this.productCriterias.length; i++) {
                            if (this.productCriterias[i].name == null || (this.productCriterias[i].name != null && this.productCriterias[i].name.trim().length == 0)) {
                                this.notificationService.showWarning('Tên thuộc tính không được rỗng');
                                var num = 0;
                                this.inputs.toArray().find((e) => {
                                    if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.length == 0)) {
                                        if (num < 1) {
                                            e.nativeElement.focus();
                                        }
                                        num++;
                                    }
                                });
                                return;
                            }

                            if (this.productCriterias[i].name != null && this.productCriterias[i].name.trim().length > 0) {
                                if (this.productCriterias[i].entity_name == null || (this.productCriterias[i].entity_name != null && this.productCriterias[i].entity_name.trim().length == 0)) {
                                    this.notificationService.showWarning("Vui lòng chọn khái niệm");
                                    this._componentService.eventPublisher$.next('entity-value-synonyms-auto#'+i);
                                    return false;
                                } else {
                                    var check_entity_name = false;
                                    for (var k = 0; k < entity_names.length; k++) {
                                        if (entity_names[k].trim() == this.productCriterias[i].entity_name.trim()) {
                                            check_entity_name = true;
                                        }
                                    }
                                    if (!check_entity_name) {
                                        this.notificationService.showWarning("Thông tin khái niệm không tồn tại");
                                        this._componentService.eventPublisher$.next('entity-value-synonyms-auto#' + i);
                                        return false;
                                    }
                                }
                            }

                            // Check tham so trung nhau
                            for (var j = 0; j < this.productCriterias.length; j++) {
                                if (this.productCriterias[j] != this.productCriterias[i] && this.productCriterias[j].param == this.productCriterias[i].param) {
                                    this.notificationService.showWarning("Tham số không được trùng nhau");
                                    return;
                                }
                            }
                            for (var j = 0; j < this.productCriteriasTmp.length; j++) {
                                if (this.productCriteriasTmp[j] != this.productCriterias[i] && this.productCriteriasTmp[j].param == this.productCriterias[i].param) {
                                    this.notificationService.showWarning("Tham số không được trùng nhau");
                                    return;
                                }
                            }
                        }
                    }

                    if (this.productCriteriasTmp != undefined && this.productCriteriasTmp.length > 0) {
                        for (var i = 0; i < this.productCriteriasTmp.length; i++) {
                            if (this.productCriteriasTmp[i].name == null || (this.productCriteriasTmp[i].name != null && this.productCriteriasTmp[i].name.trim().length == 0)) {
                                this.notificationService.showWarning('Tên thuộc tính không được rỗng');
                                var num = 0;
                                this.inputs.toArray().find((e) => {
                                    if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.length == 0)) {
                                        if (num < 1) {
                                            e.nativeElement.focus();
                                        }
                                        num++;
                                    }
                                });
                                return;
                            }

                            if (this.productCriteriasTmp[i].name != null && this.productCriteriasTmp[i].name.trim().length > 0) {
                                if (this.productCriteriasTmp[i].entity_name == null || (this.productCriteriasTmp[i].entity_name != null && this.productCriteriasTmp[i].entity_name.trim().length == 0)) {
                                    this.notificationService.showWarning("Vui lòng chọn khái niệm");
                                    this._componentService.eventPublisher$.next('entity-value-synonyms-auto#'+i);
                                    return false;
                                } else {
                                    var check_entity_name = false;
                                    for (var k = 0; k < entity_names.length; k++) {
                                        if (entity_names[k].trim() == this.productCriteriasTmp[i].entity_name.trim()) {
                                            check_entity_name = true;
                                        }
                                    }
                                    if (!check_entity_name) {
                                        this.notificationService.showWarning("Thông tin khái niệm không tồn tại");
                                        this._componentService.eventPublisher$.next('entity-value-synonyms-auto#' + i);
                                        return false;
                                    }
                                }
                            }

                            // Check tham so trung nhau
                            for (var j = 0; j < this.productCriterias.length; j++) {
                                if (this.productCriteriasTmp[i] != this.productCriterias[j] && this.productCriteriasTmp[i].param == this.productCriterias[j].param) {
                                    this.notificationService.showWarning("Tham số không được trùng nhau");
                                    return;
                                }
                            }
                        }
                    }

                    this.formError = false;
                    this.productCriteriasNew = [];
                    this.buttonClick = true;
                    let productUpdate = { product_id: this.productId, agent_id: '', product_name: '', product_name_old: '', criterias: this.productCriterias };

                    // toantm: them 2 thuoc tinh de tao object memory
                    productUpdate.agent_id = localStorage.getItem('currentAgent_Id');
                    productUpdate.product_name_old = this.productNameOld;
                    productUpdate.product_name = this.products.name;

                    this.updateProduct();
                });
            }
        }
    }

    private loadComplexForm() {
        this.complexForm = this.fb.group({
            'entityName': [null, Validators.compose([Validators.required,
                Validators.minLength(5), Validators.maxLength(20)])],
            'description' : new FormControl()
        });
    }

    private updateProduct() {
        this.productService.getListProductByAgentId(localStorage.getItem('currentAgent_Id'))
            .then((products) => {
                this.lists = products;
                if (this.checkName(this.products.name, this.products.id)) {
                    // this.products.product_criterias = undefined;
                    this.products.product_criterias = this.productCriterias;
                    for (var i = 0; i < this.productCriteriasTmp.length; i++) {
                        this.products.product_criterias.push(this.productCriteriasTmp[i]);
                    }
                    this.productCriteriasTmp = [];
                    // toantm bo sung de tao object mem
                    this.products.product_name_old = this.productNameOld;
                    // trim
                   this.trimObject();
                    this.is_load_list_product = true;
                    this.productService.updatePromiseProduct(this.products)
                        .then((product) => {
                            this.buttonClick = false;
                            this.notificationService.showSuccess('Cập nhật sản phẩm thành công');
                            this.is_load_list_product = false;
                            this.loadProductCriterias();
                        });
                } else {
                    this.input_product_name.nativeElement.focus();
                    this.notificationService.showWarning('Tên sản phẩm đã tồn tại');
                }
            }).catch(function (error) {
            this.notificationService.showDanger("Xảy ra lỗi khi lấy danh sách khái hiện hiện có để kiểm tra trùng tên khái niệm");
        });

    }

    public checkName(name, id) {
        for (var i = 0; i < this.lists.length; i++) {
            if (this.lists[i].id != id && this.lists[i].name.trim() == name.trim()) {
                return false;
            }
        }
        return true;
    }

    private trimObject() {
        if (this.products.product_criterias.length > 0) {
            for (var i = 0; i < this.products.product_criterias.length; i++) {
                this.products.product_criterias[i].name = this.products.product_criterias[i].name.trim();
                this.products.product_criterias[i].param = this.products.product_criterias[i].param.trim();
                this.products.product_criterias[i].entity_name = this.products.product_criterias[i].entity_name.trim();
            }
        }
    }

    private loadEnties() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entitieService.getListEntityPromise(agentId, 1)
                .then((data) => {
                    this.entities = this.setValueText(data);
                    this.is_load_list_entities = false;
                })
        }
    }

    public addNewEntityProduct() {
        this.currentEntity.name = localStorage.getItem('current_entity_add');
        if (this.currentEntity.name != null && this.currentEntity.name.trim().length > 0) {
            this._componentService.eventPublisher$.next('load-entity-select');
            this.loadEnties();
            this._componentService.eventPublisher$.next('close-btnCloseEdit');
            var criteria_nam_cur = localStorage.getItem('criteria_nam_cur');

            // tu dong fill vao
            if (criteria_nam_cur != undefined && criteria_nam_cur != null && this.productCriterias.length > 0 && this.productCriterias[criteria_nam_cur] != undefined) {
                this.productCriterias[criteria_nam_cur].entity_name = this.currentEntity.name;
                localStorage.setItem('entity_name_cur', this.currentEntity.name);
                this._componentService.eventPublisher$.next('auto-fill-entity-select');
            }

            var index = parseInt(criteria_nam_cur) - 1000;
            if (index != undefined && index != null && this.productCriteriasTmp.length > 0 && this.productCriteriasTmp[index] != undefined) {
                this.productCriteriasTmp[index].entity_name = this.currentEntity.name;
                localStorage.setItem('entity_name_cur', this.currentEntity.name);
                this._componentService.eventPublisher$.next('auto-fill-entity-select');
            }

            this.currentEntity.name = '';
        }
    }

    private setValueText(enties) {
        _.each(enties, function (value) {
            value.text = value.name;
        });
        return enties;
    }

    private loadProduct() {
        this.productService.getPromiseProductById(this.productId)
            .then((product) => {
                this.products = product;
                this.productNameOld = product.name;

                var product_criterias = [];
                this.products.product_criterias.sort(function(a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
                for (var i = 0; i < this.products.product_criterias.length; i++) {
                    if (this.products.product_criterias[i].type == "criteria") {
                        product_criterias.push(this.products.product_criterias[i]);
                    }
                }
                for (var i = 0; i < this.products.product_criterias.length; i++) {
                    if (this.products.product_criterias[i].type != "criteria") {
                        product_criterias.push(this.products.product_criterias[i]);
                    }
                }
                this.products.product_criterias = product_criterias;

                this.is_load_product_edit = false;
            });
    }

    private loadProductCriterias() {
        this.productService.getPromiseProductCriterias(this.productId)
            .then((criterias) => {
                this.productCriterias = criterias;
                this.productCriterias.sort(function(a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });

                var product_criterias = [];
                for (var i = 0; i < this.productCriterias.length; i++) {
                    if (this.productCriterias[i].type == "criteria") {
                        product_criterias.push(this.productCriterias[i]);
                    }
                }
                for (var i = 0; i < this.productCriterias.length; i++) {
                    if (this.productCriterias[i].type != "criteria") {
                        product_criterias.push(this.productCriterias[i]);
                    }
                }
                this.productCriterias = product_criterias;

            });
    }

    private addNewRow(): void {
        this.productsCri = new ProductCriteria();
        this.productsCri.product_id = this.productId;
        this.productsCri.type = this.checkboxData.false;
        this.productsCri.name = '';
        this.productsCri.param = '';
        this.productCriteriasTmp.push(this.productsCri);
    }

    private deleteRow(productCriteria: ProductCriteria): void {
        this.productCriterias = this.productCriterias.filter((criteria) => {
            return criteria !== productCriteria
        });
        this.productCriteriasTmp = this.productCriteriasTmp.filter((criteria) => {
            return criteria !== productCriteria
        });
        this.productsCri = undefined;
    }

    private validateCriteriaName(productCriteria: ProductCriteria): void {
        if (this.productCriterias != undefined) {
            for (var i = 0; i< this.productCriterias.length; i++) {
                if (this.productCriterias[i] == productCriteria) {
                    this.productsCri = this.productCriterias[i];
                }
            }
        }
        if (this.productCriterias != undefined) {
            for (var i = 0; i< this.productCriteriasTmp.length; i++) {
                if (this.productCriteriasTmp[i] == productCriteria) {
                    this.productsCri = this.productCriteriasTmp[i];
                }
            }
        }
        if (this.productsCri != undefined) {
            if (this.productsCri.name != '' && this.productsCri.name != null && this.productsCri.name.trim().length > 0) {
                this.productsCri.param = StringFormat.formatTextProduct(this.productsCri.name);
            } else {
                this.productsCri.param = '';
            }
        }
        
    }

    public gotoProductData() {
        this.router.navigate(['/pages/products/product-data-list', this.products.id]);
    }
}