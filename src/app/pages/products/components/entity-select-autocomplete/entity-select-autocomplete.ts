import { Component, ViewChild, ViewChildren, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { EntityService } from "../../../../_services/entity.service";
import { ComponentInteractionService } from "../../../../_services/compoent-interaction-service";
import { Subscription } from 'rxjs/Rx';
import { StringFormat } from "../../../../themes/validators/string.format";
declare var $: any;

@Component({
    selector: 'entity-select-auto',
    templateUrl: 'entity-select-autocomplete.html',
    styleUrls: ['entity-select-autocomplete.scss']
})
export class EntitySelectAutoComponent {

    @Input() query_default: any;
    @Input() entities: any;
    @Input() criteria_nam: any;
    @Output() out_criteria_nam = new EventEmitter<any>();
    @Output() value_select = new EventEmitter<any>();
    @Input() index: any;
    @ViewChild('input_auto') input_auto: ElementRef;
    @ViewChild('link_add_entity') link_add_entity: ElementRef;
    @ViewChildren('input_auto') inputs;
    stateCtrl: FormControl;
    filteredStates: any;
    private selected = '';
    private is_focus = false;
    @Output() value_name_select = new EventEmitter<any>();
    private eventSubscription: Subscription;

    private selected_old = '';

    constructor(private _componentService: ComponentInteractionService, private entitieService: EntityService) {

        if (this.query_default != undefined) {
            this.query_default = '';
        }

        let that = this;
        setTimeout(function () {
            for (var i = 0; i < that.entities.length; i++) {
                if (that.entities[i].name == that.query_default) {
                    that.selected = that.entities[i].name.trim();
                    that.selected_old = that.entities[i].name.trim();
                }
            }
        }, 500);

        this.stateCtrl = new FormControl();
        if (this.entities == undefined) {
            this.loadEnties();
        } else {
            for (var i = 0; i < this.entities.length; i++) {
                if (this.entities[i].name == this.query_default) {
                    this.selected = this.entities[i].name.trim();
                }
            }
        }
        if (this.index == undefined) {
            this.index = 0;
        }


        //Event listener
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'auto-fill-entity-select') {
                    var entity_name_cur = localStorage.getItem('entity_name_cur');
                    var criteria_nam_cur = localStorage.getItem('criteria_nam_cur');
                    if (criteria_nam_cur != null && entity_name_cur != null) {
                        if (criteria_nam_cur == that.criteria_nam) {
                            that.query_default = entity_name_cur;
                            that.selected = that.query_default;
                        }
                    }

                }

                if (event.indexOf('entity-value-synonyms-auto') != -1) {
                    this.inputs.toArray().find((e) => {
                        // console.log('e.nativeElement', e.nativeElement);
                        if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.trim().length == 0)) {
                            if (e.nativeElement.name.trim() == event.trim()) {
                                e.nativeElement.focus();
                                e.nativeElement.click();
                                localStorage.setItem('nativeElement_product_cur', e.nativeElement.name.trim());
                                setTimeout(function () {
                                    localStorage.setItem('nativeElement_product_cur', null);
                                }, 500);
                            }
                        }
                    });

                }


            }
        );
    }

    check(event) {
        localStorage.setItem('criteria_nam_cur', this.criteria_nam)
        this.is_focus = false;
        if (event.type == 'click') {
            this.filteredStates = this.entities.slice();
        }
        else {
            if (this.selected) {
                this.filteredStates = this.filterStates(this.selected);
            } else {
                this.filteredStates = this.entities.slice();
            }
        }
        this.selected_old = this.selected;
        this.value_name_select.emit(this.selected);
    }

    clickCheck(event) {
        localStorage.setItem('criteria_nam_cur', this.criteria_nam)
        this.is_focus = false;
        if (event.type == 'click') {
            this.filteredStates = this.entities;
            this.entities.sort(function (a, b) {
                var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            });


            var that = this;
            setTimeout(function () {
                that.entities.sort(function (a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });

                that.filteredStates = that.entities;
            }, 500);


        }
        else {
            this.filteredStates = this.entities.slice();
        }
        this.selected_old = this.selected;
        this.value_name_select.emit(this.selected);
    }

    selectValue(obj) {
        this.selected_old = this.selected;
        this.value_select.emit(obj.id);
        this.value_name_select.emit(obj.name);
    }

    filterStates(val: string) {
        val = val.trim();
        this.selected_old = this.selected;
        return this.entities.filter(item => {
            return (StringFormat.formatText(item.name).indexOf(StringFormat.formatText(val.trim())) !== -1)
        });
    }

    enterSelect($event, obj) {
        this.selected_old = this.selected;
        this.value_select.emit(obj.id);
    }

    private loadEnties() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entitieService.getListEntityPromise(agentId, 1)
                .then((data) => {
                    this.entities = data;
                    for (var i = 0; i < this.entities.length; i++) {
                        if (this.entities[i].id == this.query_default) {
                            this.selected = this.entities[i].name.trim();
                        }
                    }
                    this.entities.sort(function (a, b) {
                        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        // names must be equal
                        return 0;
                    });
                })
        }
    }

    clickAddEntity() {
        if (this.selected_old != undefined && this.selected_old.trim().length > 0) {
            this.selected = ' ' + this.selected_old;
            this.value_name_select.emit(this.selected_old);
        }

        this.out_criteria_nam.emit(this.criteria_nam);
        this.link_add_entity.nativeElement.click();
    }

    public showModel() {
        $('#myModal').modal('show');
    }

    public showModel1() {
        // $('#myModal1').show();
        $('#myModal1').modal('show');
    }
}