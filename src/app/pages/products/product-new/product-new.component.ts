import { Component, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService, Logger, EntityService } from '../../../_services/index';
import { ProductsModel, ProductCriteria } from '../../../_models/index';
import { Observable } from 'rxjs/Observable';
import { StringFormat } from '../../../themes/validators/string.format';
import * as _ from 'underscore';
import { NotificationService } from '../../../_services/notification.service';
import { EntitieModel } from '../../../_models/entities.model';
import { UserModel } from '../../../_models/user.model';
import { ComponentInteractionService } from "../../../_services/compoent-interaction-service";
import { ValidationService } from "../../../_services/ValidationService";
import {Subscription} from 'rxjs/Rx';

@Component({
  selector: 'product-new',
  templateUrl: 'product-new.component.html',
  styleUrls: ['product-new.component.scss'],
  providers: [ProductsService, Logger, EntityService, NotificationService]
})
export class ProductNewComponent {
  private products = new ProductsModel();
  private productCriterias = [];
  private productsCri: ProductCriteria;
  private productCriteriasNew = [];
  private productCriteriasTmp = [];
  private entities: EntitieModel[];
  private entity = new EntitieModel();
  private formError = false;
  private checkboxData = { true: 'criteria', false: 'attribute' };
  private currentUser: UserModel;
  private currentAgentId = '';
  private currentEntity = new EntitieModel();
  private lists = [];
  private entities_name = [];
  private is_load_list_product = true;
  private is_load_list_entities = true;
  @ViewChild('btnCloseAddEntity') btnCloseEdit: ElementRef;
  @ViewChild('productName') input_product_name: ElementRef;
  @ViewChild('entity_name') entity_name: ElementRef;
  @ViewChildren('input_auto') inputs;
  private eventSubscription: Subscription;

  private disabled_btn_save = false;

  constructor(
    private productService: ProductsService,
    private entitieService: EntityService,
    private notificationService: NotificationService,
    private router: Router,
    private entityService: EntityService,
    private _componentService: ComponentInteractionService,
    private validationService: ValidationService) {
    //constructor
  }

  public ngOnInit() {
    this.eventSubscription = this._componentService.eventReceiver$.subscribe(
        event => {
          if (event == 'add-entity_product') {
            this.addNewEntityProduct();
          }
        }
    );

    this.eventSubscription = this._componentService.eventReceiver$.subscribe(
        event => {
          if (event == 'load-products') {
            this.loadListProducts();
          }
        }
    );

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser && this.currentUser.partner_id) {
      this.currentAgentId = localStorage.getItem('currentAgent_Id');
      if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
        this.initEntity();
        this.loadEnties();
        this.loadListProducts();

        this.products.name = '';
        this.products.description = '';
        this.formError = false;
        this.productsCri = new ProductCriteria();
        this.productsCri.type = this.checkboxData.false;
        this.productCriteriasTmp.push(this.productsCri);
      } else {
        this.notificationService.showNoAgentId();
      }
    } else {
      this.notificationService.showNoPartnerId();
    }
  }

  public initEntity() {
    this.currentEntity.name = '';
    this.currentEntity.type = 0;
    this.currentEntity.is_system = 0;
    this.currentEntity.status = 1;
    this.currentEntity.agent_id = localStorage.getItem('currentAgent_Id');
    this.currentEntity.type = 1;
  }

  public loadListProducts() {
    let agentId = localStorage.getItem('currentAgent_Id');
    if (agentId) {
      this.productService.getListProductByAgentId(agentId)
          .then((products) => {
            this.lists = products;
            this.is_load_list_product = false;
          })
    } else {
      this.notificationService.showNoAgentId();
    }
  }

  public checkName(name) {
    for (var i = 0; i < this.lists.length; i++) {
      if (this.lists[i].name.trim() == name.trim()) {
        return false;
      }
    }

    return true;
  }

  public addNewEntityProduct() {
    this.currentEntity.name = localStorage.getItem('current_entity_add');
    if (this.currentEntity.name != null && this.currentEntity.name.trim().length > 0) {
      this._componentService.eventPublisher$.next('load-entity-select');
      this.loadEnties();
      this._componentService.eventPublisher$.next('close-btnCloseEdit');
      var criteria_nam_cur = localStorage.getItem('criteria_nam_cur');
      // tu dong fill vao
      if (criteria_nam_cur != undefined && criteria_nam_cur != null && this.productCriteriasTmp.length > 0 && this.productCriteriasTmp[criteria_nam_cur] != undefined) {
        this.productCriteriasTmp[criteria_nam_cur].entity_name = this.currentEntity.name;
        localStorage.setItem('entity_name_cur', this.currentEntity.name);
        this._componentService.eventPublisher$.next('auto-fill-entity-select');
      }
    }
  }

  private submitForm() {
    this.disabled_btn_save = true;

    if (this.products.name != null && this.products.name.trim().length > 0) {
      if (!this.validationService.validateAlphabet(this.products.name)) {
        this.input_product_name.nativeElement.focus();
        this.notificationService.showWarning('Tên sản phẩm chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_"');
        this.disabled_btn_save = false;
      } else {
        
        if (this.products.name.trim().length > 255) {
          this.input_product_name.nativeElement.focus();
          this.notificationService.showWarning('Tên sản phẩm chỉ cho phép nhập tối đa 255 ký tự');
          this.disabled_btn_save = false;
          return;
        }
        if (this.products.description.trim().length > 255) {
          this.notificationService.showWarning('Mô tả chỉ cho phép nhập tối đa 255 ký tự');
          this.disabled_btn_save = false;
          return;
        }

        if (this.productCriterias.length == 0 && this.productCriteriasTmp.length == 0) {
          this.notificationService.showWarning('Phải có ít nhất một thuộc tính có dữ liệu');
          this.disabled_btn_save = false;
          return;
        }
        
        this.productService.getListProductByAgentId(localStorage.getItem('currentAgent_Id'))
            .then((products) => {
              this.lists = products;
              if (this.checkName(this.products.name)) {
                var entity_names = [];
                this.entityService.getListEntityPromise(localStorage.getItem('currentAgent_Id'), 1) .then((data) => {
                  entity_names = _.pluck(data, 'name');
                  if (this.productCriteriasTmp != undefined && this.productCriteriasTmp.length > 0) {
                    for (var i = 0; i < this.productCriteriasTmp.length; i++) {
                      if (this.productCriteriasTmp[i].name == null || (this.productCriteriasTmp[i].name != null && this.productCriteriasTmp[i].name.trim().length == 0)) {
                        this.notificationService.showWarning('Tên thuộc tính không được rỗng');
                        var num = 0;
                        this.inputs.toArray().find((e) => {
                          if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.length == 0)) {
                            if (num < 1) {
                              e.nativeElement.focus();
                            }
                            num++;
                          }
                        });
                        this.disabled_btn_save = false;
                        return;
                      }

                      if (this.productCriteriasTmp[i].name != null && this.productCriteriasTmp[i].name.trim().length > 0) {
                        if (this.productCriteriasTmp[i].entity_name == null || (this.productCriteriasTmp[i].entity_name != null && this.productCriteriasTmp[i].entity_name.trim().length == 0)) {
                          this.notificationService.showWarning("Vui lòng chọn khái niệm");
                          this._componentService.eventPublisher$.next('entity-value-synonyms-auto#' + i);
                          this.disabled_btn_save = false;
                          return false;
                        } else {
                          var check_entity_name = false;
                          for (var k = 0; k < entity_names.length; k++) {
                            if (entity_names[k].trim() == this.productCriteriasTmp[i].entity_name.trim()) {
                              check_entity_name = true;
                            }
                          }
                          if (!check_entity_name) {
                            this.disabled_btn_save = false;
                            this.notificationService.showWarning("Thông tin khái niệm không tồn tại");
                            this._componentService.eventPublisher$.next('entity-value-synonyms-auto#' + i);
                            return false;
                          }
                        }
                      }

                      // Check tham so trung nhau
                      for (var j = 0; j < this.productCriteriasTmp.length; j++) {
                        if (this.productCriteriasTmp[j] != this.productCriteriasTmp[i] && this.productCriteriasTmp[j].param == this.productCriteriasTmp[i].param) {
                          this.disabled_btn_save = false;
                          this.notificationService.showWarning("Tham số không được trùng nhau");
                          return;
                        }
                      }

                    }
                    this.products.name = this.products.name.trim();
                    this.products.description = this.products.description.trim();
                    this.createProduct();
                  }
                });
              } else {
                this.input_product_name.nativeElement.focus();
                this.disabled_btn_save = false;
                this.notificationService.showWarning('Tên sản phẩm đã tồn tại');
              }
            }).catch(function (error) {
              this.disabled_btn_save = false;
              this.notificationService.showDanger("Xảy ra lỗi khi lấy danh sách khái hiện hiện có để kiểm tra trùng tên khái niệm");
        });
      }
    } else {
      this.input_product_name.nativeElement.focus();
      this.disabled_btn_save = false;
      this.notificationService.showWarning('Vui lòng nhập tên sản phẩm');
    }
  }

  private validateCriteriaName(productCriteria: ProductCriteria): void {
    for (var i = 0; i < this.productCriteriasTmp.length; i++) {
      if (this.productCriteriasTmp[i] == productCriteria) {
        this.productsCri = this.productCriteriasTmp[i];
      }
    }
    if (this.productsCri != undefined) {
      if (this.productsCri.name != '' && this.productsCri.name != null && this.productsCri.name.trim().length > 0) {
        this.productsCri.param = StringFormat.formatTextProduct(this.productsCri.name);
      } else {
        this.productsCri.param = '';
      }
    }
  }

  private createProduct() {
    this.products.vendor = '';
    this.products.product_criterias = this.productCriteriasTmp;

    // trim
    if (this.products.product_criterias.length > 0) {
      for (var i = 0; i < this.products.product_criterias.length; i++) {
        this.products.product_criterias[i].name = this.products.product_criterias[i].name.trim();
        this.products.product_criterias[i].param = this.products.product_criterias[i].param.trim();
        this.products.product_criterias[i].entity_name = this.products.product_criterias[i].entity_name.trim();
      }
    }

    this.products.agent_id = localStorage.getItem('currentAgent_Id');
    this.is_load_list_product = true;
    this.productService.createPromiseProduct(this.products)
      .then((product) => {
        this.products = product;
        this.notificationService.showSuccess('Thêm sản phẩm thành công');
        this.is_load_list_product = false;
        this.router.navigate(['/pages/products/product-detail', this.products.id]);
      });
  }

  private loadEnties() {
    this.entitieService.getListEntityPromise(this.currentAgentId, 1)
      .then((data) => {
        this.entities = this.setValueText(data);
        if (this.entities.length > 0) {
          for (var i = 0; i < this.entities.length; i++) {
            this.entities_name.push('@' + this.entities[i].name);
          }
        }
        this.is_load_list_entities = false;
      });
  }

  private setValueText(enties) {
    _.each(enties, function (value) {
      value.text = value.name;
    });
    return enties;
  }

  private addNewRow(): void {
    if (this.productsCri) {
      this.formError = false;
      this.productCriterias.push(this.productsCri);
      this.productCriteriasNew = [];
      this.productsCri = new ProductCriteria();
      this.productsCri.type = this.checkboxData.false;
      this.productCriteriasTmp.push(this.productsCri);
    } else {
      this.formError = false;
      this.productsCri = new ProductCriteria();
      this.productsCri.type = this.checkboxData.false;
      this.productCriteriasTmp.push(this.productsCri);
    }

  }

  private deleteRow(productCriteria: ProductCriteria): void {
    this.productCriteriasTmp = this.productCriteriasTmp.filter((criteria) => {
      return criteria !== productCriteria
    });
    this.productCriterias = this.productCriterias.filter((criteria) => {
      return criteria !== productCriteria
    });
    this.productsCri = undefined;
  }

}
