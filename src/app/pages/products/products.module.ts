// Angular Imports
import { NgModule } from '@angular/core';
// This Module's Components
import { ProductsComponent } from './products.component';
import { CommonModule } from '@angular/common';
import { routing } from './products.routing';
import { NgaModule } from '../../themes/nga.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductNewComponent } from './product-new/product-new.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2TableModule } from 'ng2-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ActionsPipe } from './pipes/actions.pipe';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { SelectModule } from 'ng2-select';
import { Select2CustomeComponent } from './product-detail/select2-custome/select2-custome.component';
import { TagInputModule } from 'ng2-tag-input';
import { NotificationService } from "../../_services/notification.service";
import { DataTableModule } from "angular2-datatable/index";
import { ValidationService } from "../../_services/ValidationService";
import { EntitySelectAutoComponent } from "./components/entity-select-autocomplete/entity-select-autocomplete";
import { MdAutocompleteModule } from '@angular/material';
import { ProductDataComponent } from "./product-data-component/product-data.component";
import { KeysPipe } from "./pipes/keys";
import { ProductItemPipe } from "./pipes/product_items.pipe";
import { ProductItemEditComponent } from "./product-data-component/product-item-edit-component/product-item-edit.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        SelectModule,
        PaginationModule,
        TabsModule,
        Ng2SmartTableModule,
        Ng2TableModule,
        FormsModule, ReactiveFormsModule,
        routing,
        TagInputModule,
        DataTableModule,
        MdAutocompleteModule
    ],
    declarations: [
        ProductsComponent,
        ProductListComponent,
        ProductNewComponent,
        Select2CustomeComponent,
        ProductDetailComponent,
        ActionsPipe,
        ProductItemPipe,
        KeysPipe,
        EntitySelectAutoComponent,
        ProductDataComponent,
        ProductItemEditComponent
    ],
    exports: [
        ProductsComponent,
        ProductListComponent,
        ProductNewComponent,
        ProductDetailComponent,
        Select2CustomeComponent,
        ActionsPipe,
        ProductItemPipe,
        KeysPipe,
        EntitySelectAutoComponent,
        ProductDataComponent,
        ProductItemEditComponent
    ],
    providers: [
        NotificationService,
        ValidationService
    ]
})
export class ProductsModule {

}
