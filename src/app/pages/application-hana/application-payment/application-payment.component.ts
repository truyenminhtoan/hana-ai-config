import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { PaymentHistoryModel } from '../../../_models/payment-history.model';
import { NotificationService } from '../../../_services/notification.service';
declare var jQuery: any;
declare var $: any;
declare var swal: any;
import * as _ from 'underscore';
import { Observable } from 'rxjs/Observable';
import { ApplicationService } from '../../../_services/application.service';
import { EmitterService } from "../../../_services/emitter.service";
@Component({
    selector: 'application-payment',
    templateUrl: 'application-payment.component.html',
    styleUrls: ['application-payment.component.scss']
})
export class ApplicationPaymentComponent implements OnInit, AfterViewInit {
    @ViewChild('maVoucher') private maVoucher;
    @Input() private partnerId;
    private histories: PaymentHistoryModel[] = [];
    private tableMessage = '';
    private totalRecords = 1;
    private pageId;
    private messageVoucher = '';

    private pageInfo: any = {};
    private isCheckVoucher = false;
    private isShowTut = false;

    private paymentInto;
    private paymentContent;
    private tutPaymentContent;

    constructor(private notificationService: NotificationService, private applicationService: ApplicationService) { }

    public ngOnInit() {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser.app_using && currentUser.app_using.integrations) {
                currentUser.app_using.integrations.filter((item) => {
                    if (item.pageId) {
                        this.pageId = item.pageId;
                        this.getPageInfo();
                    }
                });
            } else {
                this.getAppDetail();
            }
        }

        EmitterService.get('applicationPaymentId').subscribe((data: any) => {
            this.pageId = data.page_id;
            this.getPageInfo();
        });
        $('.material-icons').click((event) => {
            $('.modal-hana').show();
        });

        $('body').click((event) => {
            if (!$(event.target).is('.material-icons')) {
                $('.modal-hana').hide();
            }
        });
    }

    public ngAfterViewInit() {
        this.maVoucher.nativeElement.focus();
        // tslint:disable-next-line:only-arrow-functions
        $(window).keydown(function (event) {
            /* if (event.keyCode === 13) {
                console.log('`Enter` key is not allowed.');
                event.preventDefault();
                return false;
            } */
        });
    }

    public checkVoucherCode(code) {
        if (!code) {
            this.notificationService.showDanger('Vui lòng nhập mã khuyến mãi...');
            this.maVoucher.nativeElement.focus();
            return;
        }

        this.requestCheckVoucherCode(code);
    }

    public requestCheckVoucherCode(code) {
        this.isCheckVoucher = true;
        let options: any = {};
        options.page_id = this.pageId;
        options.code = code;

        let actionOperate: Observable<any>;
        actionOperate = this.applicationService.activeVoucher(options);
        actionOperate.subscribe(
            (result) => {
                this.isCheckVoucher = false;
                // alert(result);
                // 202
                if (result.errorCode === 202) {
                    this.notificationService.showDanger('Mã khuyến mãi đã được kích hoạt...');
                    return;
                }

                // 202
                if (result.errorCode === 203) {
                    let startTime;
                    let stopTime;
                    _.filter(result.data, (item) => {
                        if (item.meta_key === 'start_time') {
                            startTime = item.meta_value;
                        }
                    });

                    _.filter(result.data, (item) => {
                        if (item.meta_key === 'stop_time') {
                            stopTime = item.meta_value;
                        }
                    });
                    this.notificationService.showDanger(`Mã khuyến mãi không hợp lệ, mã chỉ sử dụng từ ngày ${startTime} đến ngày ${stopTime} `);
                    return;
                }

                // 202
                if (result.errorCode === 204) {
                    this.notificationService.showDanger('Mã khuyến mãi không tồn tại...');
                    return;
                }

                if (result.errorCode === 206) {
                    let addDays;
                    _.filter(result.data, (item) => {
                        if (item.meta_key === 'voucher_value') {
                            addDays = item.meta_value;
                        }
                    });
                    if (addDays) {
                        this.messageVoucher = `Bạn được khuyến mãi ${addDays}% trong lần chuyển khoản đầu tiên`;
                    } else {
                        this.messageVoucher = `Mã khuyến mãi được giảm giá trong lần chuyển khoản đầu tiên`;
                    }
                    return;
                }

                // 201
                if (result.errorCode === 201) {
                    this.notificationService.showDanger('Hệ thống đang bận vui lòng thử lại sau...');
                    return;
                }

                // this.notificationService.showSuccess('Kích hoạt thành công');
                this.messageVoucher = `Bạn được cộng ${result.data.value} ngày dùng`;
                this.getPageInfo();
            },
            (err) => {
                this.isCheckVoucher = false;
                // console.log(err);
                this.notificationService.showDanger('Hệ thống đang bận vui lòng thử lại sau...');
            });
    }

    public getPageInfo() {
        this.histories = [];
        let actionOperate: Observable<any>;
        actionOperate = this.applicationService.getPageInfo(this.pageId);
        actionOperate.subscribe(
            (result) => {
                if (result) {
                    this.pageInfo = result.data;
                    if (this.pageInfo.hn_page_payment_history) {
                        this.histories = this.pageInfo.hn_page_payment_history;
                        this.totalRecords = this.histories.length;
                    }

                    if (this.pageInfo.hn_payment) {
                        this.pageInfo.hn_payment.filter((item) => {
                            if (item.key === 'thong_tin_chuyen_khoan') {
                                this.paymentInto = item.value;
                            }
                            if (item.key === 'noi_dung_chuyen_khoan') {
                                this.paymentContent = item.value;
                            }
                            if (item.key === 'huong_dan_noi_dung_chuyen_khoan') {
                                this.tutPaymentContent = item.value;
                            }
                        });
                    }
                }
            },
            (err) => {
                // console.log(err);
            });
    }

    public close() {
        // tslint:disable-next-line:only-arrow-functions
        // $('body').mouseup(function () {
        //     if (!this.isShowTut) {
        //         $('.modal-hana').hide();
        //         this.isShowTut = true;
        //     }
        // });
    }

    public open() {
        // $('.modal-hana').show();
        // this.isShowTut = false;
    }

    private getAppDetail() {
        // console.log('TICH HOP ' + this.partnerId);
        let that = this;
        if (!this.partnerId) {
            return false;
        }
        this.applicationService.getPartnerDetailById(this.partnerId)
            .then((integrate) => {
                if (integrate.integrations) {
                    integrate.integrations.filter((item) => {
                        if (item.pageId) {
                            this.pageId = item.pageId;
                            this.getPageInfo();
                        }
                    });
                }
            });
    }
}
