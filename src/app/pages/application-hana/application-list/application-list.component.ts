import {
  Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy, Directive,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ConfigService } from '../../../_services/config.service';
import { TruncatePipe } from '../pipes/truncate';
import { FacebookService } from '../../../_services/facebook.service';

declare var $: any;
declare var _: any;
declare var swal: any;

@Component({
  selector: 'application-list',
  templateUrl: 'application-list.component.html',
  // encapsulation: ViewEncapsulation.None,
  styleUrls: ['application-list.component.scss', '../plugin/semantic.css']
})



export class ApplicationListComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  private currentUser: any;
  private listApplication: any;
  private color = 'primary';
  private mode = 'indeterminate';
  private value = 50;
  private bufferValue = 75;
  private loadingProcess: boolean = true;
  private timerObserver: Subscription;

  constructor(private applicationService: ApplicationService, private router: Router, private cdRef: ChangeDetectorRef,
    private serviceConfig: ConfigService, private facebookService: FacebookService) {
    // console_bk.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB 1');
  }

  public ngOnInit() {
    this.loadingProcess = true;
    // console_bk.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB 2');
    let current = localStorage.getItem('currentUser');
    if (current && current !== 'undefined') {
      // console_bk.log('aaaaaaaaaaaa 1');
      // console_bk.log('aaaaaaaaaaaa 1' + localStorage.getItem('currentUser'));
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      // console_bk.log('aaaaaaaaaaaa 2');
      this.getListApplication();
    } else {
      // console_bk.log('aaaaaaaaaaaaa ' + localStorage.getItem('currentUser'));
      this.router.navigate(['/login-hana']);
    }
  }

  ngOnChanges() {
  }

  ngAfterViewInit() {
    /* $('.hana-config-name')
       .popup()
       ;*/
    $('.dropdown-toggle').dropdown();
    setTimeout(function () {
      $.getScript('../../../assets/js/popup-hana.js');
    }, 2000);
  }
  public createApp() {
    this.router.navigate(['/application-hana/application-create']);
  }

  private configApp() {
    // window.location.href = '/application-hana/application-config';
    this.router.navigate(['/application-hana/application-config']);
  }

  private configAppDetail(appid) {
    // console_bk.log(appid);
    window.location.href = '/#/application-hana/application-config-detail/' + appid;
  }

  /** @Tam them */
  private resetAgentId() {
     localStorage.setItem('currentAgent_Id', 'NULL');
  }

  private usingApp(app) {
    // console_bk.log('APP: ' + JSON.stringify(app));
    // alert(JSON.stringify(app));
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      currentUser.app_using = app;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      localStorage.setItem('currentAgent_Id', 'NULL');
      this.currentUser = currentUser;
    }

    this.getTryUsing();
    //  window.open('http://test.mideasvn.com:8001/authen/', '_blank');
    // let windows = window;
    /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
      .then((apps) => {
        let callbackUrl = this.serviceConfig.HOST_MY_WEB + '/#/chat';
        this.newTab(this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token + '/' + btoa(callbackUrl));
      }); */
      this.router.navigate(['/chat']);
      // window.location.href = '/#/chat';
  }

  // toantm TEST
  /*private usingApp(app) {
    let user = { partner_id: 1 };
    user = JSON.parse(localStorage.getItem('currentUser'));
    user.partner_id = app.id;
    localStorage.setItem('currentUser', JSON.stringify(user));
    // console_bk.log('user:', JSON.stringify(user));
    this.router.navigate(['/pages/agents/agentList']);
  }*/

  private newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb' + url);
    var form = document.createElement("form");
    form.method = "GET";
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }


  ngOnDestroy() {
    // console.log();
  }


  private getTryUsing() {
    // console.log('UERRRRRR ' + JSON.stringify(this.currentUser));
    let integrations = this.currentUser.app_using.integrations;
    if (!integrations) {
      return false;
    }
    let fbInte = _.find(integrations, (inte) => {
      return inte.gatewayId === 1;
    });
    if (fbInte) {
      if (fbInte.isExpire === 0) {
        let body = {
          page_id: fbInte.pageId,
          partner_id: this.currentUser.partner_id
        }
        this.applicationService.getTryUsing(body)
          .then((apps) => {

            // console.log('paypay ' + JSON.stringify(apps));
            this.currentUser.payment = apps.data;
            this.currentUser.paymentstatus = false;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          });
      }
    }
  }

  private getListApplication() {
    this.applicationService.getListPartnerByUsername(this.currentUser.username)
      .then((apps) => {
        if (apps) {
          // console_bk.log(' CURRRENT ' + JSON.stringify(this.currentUser));
          // console_bk.log('LIST AP pppp ' + JSON.stringify(apps));
          if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.currentUser = currentUser;
            currentUser.apps = apps;
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
          }
          if (this.currentUser.login_type === 'facebook' && apps.length === 0) {
            this.getAllFanpage();
            this.listApplication = apps;
            this.loadingProcess = false;
            this.cdRef.detectChanges();
          } else if (apps.length === 0) {
            this.router.navigate(['/application-hana/application-create']);
          } else {
            this.listApplication = apps;
            this.loadingProcess = false;
            this.cdRef.detectChanges();
          }

        }
      });
  }

  private showSwal(id, appid) {
    // console_bk.log('xóa id ' + id + ' APP ID ' + appid);
    swal({
      title: 'Bạn có chắc chắn muốn xóa ứng dụng này?',
      text: 'Bạn không thể khôi phục lại khi đã xóa!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Vâng, Tôi muốn xóa!',
      cancelButtonText: 'Không, Tôi muốn giữ ',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      // console_bk.log('xóa nó');
      this.applicationService.deleteApp(id, appid)
        .then((apps) => {
          if (apps) {
            this.getListApplication();
            swal({
              title: 'Đã xóa!',
              text: 'Ứng dụng của bạn đã bị xóa.',
              type: 'success',
              confirmButtonClass: 'btn btn-success',
              buttonsStyling: false
            });
          }
        });

    }, (dismiss) => {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        // console_bk.log('giữ nó');
      }
    })
  }

  private getAllFanpage() {
    this.facebookService.getAllFanpageIntegrate()
      .then((integrate) => {
        if (integrate) {
          let fanpages = integrate.userAccounts;
          if (fanpages.length > 0) {
            this.listAllFanpage();
          } else {
            this.router.navigate(['/application-hana/application-create']);
          }
          this.cdRef.detectChanges();
        }
      });
  }

  private listAllFanpage() {
    this.showModel();
  }

  private showModel() {
    $('#listFanpageModal').modal('show');
  }

  private showGuideModel() {
    $('#appListGuideModal').modal('show');
    setTimeout(() => {
      $('.images-guide-app-list').bxSlider({
        mode: 'horizontal',
        moveSlides: 1,
        slideMargin: 40,
        infiniteLoop: true,
        // slideWidth: 900,
        minSlides: 1,
        maxSlides: 1,
        speed: 800,
      });
    }, 300);

  }
}
