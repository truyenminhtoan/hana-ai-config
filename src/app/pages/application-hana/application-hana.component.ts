import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';

// import { NavbarComponent } from '../../themes/navbar/navbar.component';

// @Component({
//     selector: 'application-hana',
//     templateUrl: 'application-hana.component.html',
//     styleUrls: ['application-hana.component.scss']
// })
declare var $: any;

@Component({
    selector: 'application-hana',
    template: '<router-outlet></router-outlet>',
    // encapsulation: ViewEncapsulation.Native,
    styleUrls: ['application-hana.component.scss']
})
export class ApplicationHanaComponent implements AfterViewInit {
    public ngAfterViewInit() {
        $('.dropdown-toggle').dropdown();
    }
    // public close() {
    //     $('.modal-hana').css('display', 'none');
    // }
}
