import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ApplicationHanaComponent } from './application-hana.component';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationCreateComponent } from './application-create/application-create.component';
import { ApplicationConfigComponent } from './application-config/application-config.component';
import { ApplicationConfigDetailComponent } from './application-config-detail/application-config-detail.component';
import { ApplicationRedirectComponent } from './application-redirect/application-redirect.component';
import { ApplicationConfigDetailNewComponent } from './application-config-detail-new/application-config-detail-new.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationHanaComponent,
    children: [
      { path: 'application-list', component: ApplicationListComponent },
      { path: 'application-redirect', component: ApplicationRedirectComponent },
      { path: 'application-create', component: ApplicationCreateComponent },
      { path: 'application-config/:id', component: ApplicationConfigComponent },
      { path: 'application-config-detail/:id', component: ApplicationConfigDetailComponent },
      { path: 'application-config-app/:id', component: ApplicationConfigDetailNewComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
