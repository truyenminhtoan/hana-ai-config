import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
// import { FacebookService, ValidationService } from '../../../_services';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { Observable } from 'rxjs/Observable';
import { ColorPickerService } from 'angular2-color-picker';
import { NotificationService } from '../../../_services/notification.service';
import { ConfigService } from '../../../_services/config.service';
import { FacebookService } from '../../../_services/facebook.service';
import { ValidationService } from '../../../_services/ValidationService';


declare var $: any;
declare var swal: any;
declare const FB: any;
declare var _: any;

interface FileReaderEventTarget extends EventTarget {
  result: string;
}
interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

@Component({
  selector: 'application-config-detail-new',
  templateUrl: 'application-config-detail-new.component.html',
  styleUrls: ['application-config-detail-new.component.scss'],

})
export class ApplicationConfigDetailNewComponent implements OnInit, OnChanges, AfterViewInit {
  public fanpageInfo: any;
  public isIntegrate: boolean;
  public pageAccessToken: any;
  public partnerId: any;
  public appId: any;
  public appDetai: any;
  private sub: any;
  public listMember = [];
  public script: any;
  public isNewMsg = false;
  public isNewMember = false;
  public isNewInfo = false;
  public isUnderstand = false;
  public isNewDemand = false;
  private color: string = '#127bdc';
  private notificationConfigs = [];
  private listEmailReport = [];
  private listEmailReportString: any;
  private listEmailFacebook = [];
  private listEmailFacebookString: any;
  private listTags = [];
  private agentActive: any;
  private listAgent = [];
  private selectedValue: any;
  private colorBackground: string = '#127bdc';
  private colorBgContentChat: string = '#127bdc';
  private colorContentChat: string = '#ffffff';
  private play_sound: any;
  private show_notify: any;
  public listMemberInvited: any = [];

  private min: number = 200;
  private max: number = 800;
  private step: number = 1;
  private valueWidth: number = 330;
  private valueHeight: number = 280;
  private currentUser: any;
  private app: any;
  private collapsePopup: number = 1;
  private requireInfomation: number = 0;

  private hideCommentOption: any = [
    { key: 0, value: 'Không ẩn' },
    { key: 1, value: 'Ẩn tất cả' },
    { key: 2, value: 'Ẩn có số điện thoại' }
  ];
  private hideComment: number = 0;

  favoriteSeason: string;

  seasons = [
    'Winter',
    'Spring',
    'Summer',
    'Autumn',
  ];

  constructor(private facebookService: FacebookService, private route: ActivatedRoute, private router: Router, private applicationService: ApplicationService,
    private validationService: ValidationService, private cdRef: ChangeDetectorRef, private cpService: ColorPickerService,
    private notificationService: NotificationService, private serviceConfig: ConfigService) {
      console.log('DETAIL NEW');
    // $.getScript('../assets/js/jscolor.min.js');
  }

  private showModel(type) {
    if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1)) {
      $('#exampleModalLong').modal('show');
    } else {
      FB.login((response) => {
        if (response.authResponse) {
          FB.api('/me', { fields: 'id,name,email' }, (res) => {
            localStorage.setItem('accessToken', response.authResponse.accessToken);
            this.pageAccessToken = response.authResponse.accessToken;
            this.cdRef.detectChanges();
            $('#exampleModalLong').modal('show');
          });
        }
      }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
      // }
    }


  }
  private showScript(type) {
    $('#scriptModel').modal('show');
  }

  private usingApp(app) {
    // console_bk.log('APP: ' + JSON.stringify(app));
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.currentUser = currentUser;
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      currentUser.app_using = app;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
    }
    //  window.open('http://test.mideasvn.com:8001/authen/', '_blank');
    // let windows = window;
    /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
      .then((apps) => {
        this.newTab(this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token);
      }); */
      this.router.navigate(['/chat']);
  }

  private newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb' + url);
    var form = document.createElement("form");
    form.method = "GET";
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }

  private readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e: FileReaderEvent) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  ngOnDestroy() {
    // Clean sub to avoid memory leak
    this.sub.unsubscribe();
  }
  ngOnInit() {
    this.createListMember();
    // this.getScript();
    // console_bk.log('INITTTTTTTTTTTTTTTTTTTTTT ' + localStorage.getItem('accessToken'));
    this.sub = this.route.params.subscribe((params) => {
      this.partnerId = params['id'];
    });
    // console_bk.log('partner id ' + this.partnerId);
    this.getAppDetail();
    if (localStorage.getItem('accessToken')) {
      this.pageAccessToken = localStorage.getItem('accessToken');
    }

    FB.init({
      // appId: '378516159201961',
      appId: this.serviceConfig.facebookConfig.client_id,
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
    // var $validator = $('.wizard-card form').validate({
    //     rules: {
    //         firstname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         lastname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         email: {
    //             required: true,
    //             minlength: 3,
    //         }
    //     },
    //
    //     errorPlacement: function (error, element) {
    //         $(element).parent('div').addClass('has-error');
    //     }
    // });

    

  }

  ngOnChanges() {
    // console_bk.log('CHANGE EEEEEEEEMIT    ' + this.isIntegrate);
    // console_bk.log('CHANGE EEEEEEEEMIT    ' + this.appId);
    if (this.isIntegrate) {
      this.checkIntegrateFacebook(this.appId);
    }
  }

  ngAfterViewInit() {
    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',

      onNext: function (tab, navigation, index) {
        var $valid = $('.wizard-card form').valid();
        if (!$valid) {
          // $validator.focusInvalid();
          return false;
        }
      },

      onInit: function (tab, navigation, index) {

        //check number of tabs and fill the entire row
        var $total = navigation.find('li').length;
        var $width = 100 / $total;
        var $wizard = navigation.closest('.wizard-card');

        var $display_width = $(document).width();

        if ($display_width < 600 && $total > 3) {
          $width = 50;
        }

        navigation.find('li').css('width', $width + '%');
        var $first_li = navigation.find('li:first-child a').html();
        var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
        $('.wizard-card .wizard-navigation').append($moving_div);

        //    this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });

        $('.moving-tab').css('transition', 'transform 0s');
      },

      onTabClick: function (tab, navigation, index) {

        // var $valid = $('.wizard-card form').valid();
        //
        // if (!$valid) {
        //     return false;
        // } else {
        //     return true;
        // }
      },

      onTabShow: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        var $wizard = navigation.closest('.wizard-card');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function () {
          $('.moving-tab').text(button_text);
        }, 150);

        var checkbox = $('.footer-checkbox');

        if (index !== 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }

        // this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
      }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {

      this.readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
      // console_bk.log('click');

      var wizard = $(this).closest('.wizard-card');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');
    
    $('.wizard-card').each(function () {

      var $wizard = $(this);
      var index = $wizard.bootstrapWizard('currentIndex');
      // this.refreshAnimation($wizard, index);

      var total_steps = $wizard.find('li').length;
      var move_distance = $wizard.width() / total_steps;
      var step_width = move_distance;
      move_distance *= index;

      var $current = index + 1;

      if ($current == 1) {
        move_distance -= 8;
      } else if ($current == total_steps) {
        move_distance += 8;
      }

      $wizard.find('.moving-tab').css('width', step_width);
      $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

      });

      $('.moving-tab').css({
        'transition': 'transform 0s'
      });
    });
    setTimeout(function() {
      $.getScript('../../../assets/js/popup-hana.js');
    }, 2000);
    
  }

  private checkIntegrateFacebook(appId: string) {
    // console_bk.log('checkIntegrateFacebook ' + appId);
    let that = this;
    /*this.facebookService.checkIntegrationGateway(appId)
     .then((integrate) => {
     if (integrate.code === 200) {
     that.fanpageInfo = integrate.integrationGateway;
     this.cdRef.detectChanges();
     }
     });*/
    this.applicationService.getIntegrationByAppId(appId, 1)
      .then((integrate) => {
        if (integrate) {
          that.fanpageInfo = integrate;
          this.cdRef.detectChanges();
        }
      });
  }

  private getAppDetail() {
    // console_bk.log('TICH HOP ');
    let that = this;
    this.applicationService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP LOGGGGGGG' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.status);
        this.app = integrate;
        this.appId = integrate.applications[0].app_id;
        this.appDetai = integrate;
        this.checkIntegrateFacebook(this.appId);
        this.getScript();
        this.cdRef.detectChanges();
        // toantm
        if (localStorage.getItem('currentUser')) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          currentUser.version = integrate.version;
          if (currentUser.role_id !== 1) {
            let user = integrate.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
            if (user) {
              currentUser.role_id = user.role_id;
            }
          }
          // console_bk.log('THONG TIN USER:', JSON.stringify(currentUser));
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
        }
      });
  }

  private counterUpdate(event: boolean) {
    console.log('CHANGE UNPAGE SUB');
    this.isIntegrate = event;
    this.checkIntegrateFacebook(this.appId);
    // if (event) {
    //   this.checkIntegrateFacebook(this.appId);
    // } else {
    //   this.fanpageInfo = null;
    // }
  }

  private createListMember() {
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
  }
  private getScript() {
    this.applicationService.getIntegrationByAppId(this.appId, 2)
      .then((integrate) => {
        // console_bk.log('getScript ' + JSON.stringify(integrate));
        if (integrate) {
          this.script = integrate.script;
          this.colorBackground = integrate.background_color;
          this.colorContentChat = integrate.color_content ? integrate.color_content : this.colorContentChat;
          this.colorBgContentChat = integrate.background_color_content ? integrate.background_color_content : this.colorBgContentChat;

          this.play_sound = integrate.play_sound ? integrate.play_sound : 0;
          this.show_notify = integrate.show_notify ? integrate.show_notify : 0;
          this.collapsePopup = integrate.collapse ? integrate.collapse : 1;
          this.requireInfomation = integrate.require_infomation ? integrate.require_infomation : 0;
          // this.collapsePopup = 0;
          // console_bk.log('LOAD BACKGOUND');
          this.cdRef.detectChanges();
        }
      });
  }
  private addNewEmail() {
    // console_bk.log('Addnew user');
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.cdRef.detectChanges();
  }
  private removeNewEmail(event, id) {
    // console_bk.log('remove0 user ' + id  );
    // this.listMember.push({email: '', status: 'add'});
    this.listMember = this.listMember.filter((item) => item.id !== id);
    this.cdRef.detectChanges();
  }

  private checkEmailExits(event, id) {
    let object = _.find(this.listMember, (obj) => { return obj.id === id });
    if (object.email) {
      // this.applicationService.getEmailInviteByAppId(object.email, this.partnerId)
      //   .then((integrate) => {
      //     // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
      //     // console_bk.log('TICH HOP ' + integrate.code);
      //     if (integrate.code === 200) {
      //
      //     }
      //   });

      // let actionOperate: Observable<any>;
      // actionOperate = this.applicationService.getEmailInviteByAppId(object.email, this.partnerId);
      // actionOperate.subscribe(
      //   (data) => {
      //     // console_bk.log('DATA ' + JSON.stringify(data));
      //     if (data.errorCode === 200) {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'none';
      //         }
      //       });
      //     } else {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'add';
      //         }
      //       });
      //     }
      //   },
      //   (err) => {
      //     // console_bk.log('ERROR');
      //   });
    }
  }
  private inviteEmail() {
    let emailInvite = [];
    let emailRoles = [];
    this.listMember = this.listMember.filter((item) => item.email !== '');
    _.each(this.listMember, (member) => {
      if (this.validationService.validateEmail(member.email)) {
        // console_bk.log('EMAIL ' + member.email);
        emailInvite.push(member.email);
        emailRoles.push({ email: member.email, role: 3, status: 0 });
      } else {
        // console_bk.log('KHONG  ' + member.email);
      }
    });
    if (emailRoles.length === 0) {
      this.notificationService.showWarningTimer('Vui lòng nhập email', 3000);
      return false;
    }
    // console_bk.log('EMAIL   ' + JSON.stringify(emailInvite));
    this.applicationService.getEmailInviteByAppId(emailInvite, this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.code);
        if (integrate) {
          _.each(emailRoles, (erole) => {

            let findV = _.find(integrate, (item) => { return item === erole.email; });
            // console_bk.log('EMAIL 22  ' + JSON.stringify(findV));
            if (findV) {
              erole.status = 1;
            }
          });

          // console_bk.log('EMAIL 11111  ' + JSON.stringify(emailRoles));
          this.applicationService.insertEmailInviteByAppId(emailRoles, this.partnerId, 'email', this.appDetai.company, this.appId)
            .then((invites) => {
              // console_bk.log('TICH HOP xx ' + JSON.stringify(invites));
              if (invites) {
                // console_bk.log('EMAIL  DONE ' + JSON.stringify(emailInvite));
                this.listMember = [];
                this.createListMember();
                // this.notificationService.showSwal('Mời thành công');
                this.listUserInvited();
                this.notificationService.showSuccessTimer('Mời thành công', 3000);
              }
            });
        }
      });

  }

  private listUserInvited() {
    this.applicationService.getListUserInvited(this.partnerId)
      .then((invites) => {
        if (invites) {
          this.listMemberInvited = invites;
        }
      });
  }

  private loadNotificaton() {
    if (this.notificationConfigs.length === 0) {
      this.notificationConfigs.push({ id: -1, notification_type: 'new_cus', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'all_msg', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'dont_understand', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'info_customer', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'new_demand', status: 1 });
    }

    // this.cdRef.detectChanges();
    this.applicationService.getNotificationConfigByPartnerId(this.partnerId)
      .then((notifications) => {
        if (notifications) {
          if (notifications.length > 0) {
            // console_bk.log('LIST NOTIFICAON ' + JSON.stringify(notifications));
            this.notificationConfigs = notifications;
          }
        }
      });
    this.loadEmailReport();
    this.loadEmailFacebook();
    this.getScript();
    // console_bk.log('Load loadNotificaton ');
  }
  private loadEmailReport() {
    /*if (this.listEmailReportString) {
     this.listEmailReport.push({id: this.listEmailReport.length + 1 , email: 'docaohuynh@gmail.com'});
     } else {
     this.listEmailReport.push({id: this.listEmailReport.length + 1 , email: ''});
     }*/
    this.listEmailReport = [];
    this.applicationService.getEmailReportByPartnerId(this.partnerId)
      .then((emails) => {
        if (emails) {
          // console_bk.log('EMAIL '  + JSON.stringify(emails));
          if (emails.email) {
            // console_bk.log('EMAIL      LLL 11 ' + emails.email);
            let emailSplit = emails.email.split(',');
            _.each(emailSplit, (em) => {
              // console_bk.log('EMAIL      LLL  ' + em + ' length ' + this.listEmailReport.length + 1);
              this.listEmailReport.push({ id: +this.listEmailReport.length + 1, email: em });
            });
            // console_bk.log('EMAIL      LLL 11 ' + JSON.stringify(this.listEmailReport));
            this.cdRef.detectChanges();
          }
        }
      });
  }

  private loadEmailFacebook() {
    this.listEmailFacebook = [];
    this.applicationService.getEmailFacebookByPartnerId(this.appId, this.partnerId)
      .then((emails) => {
        if (emails) {
          // console_bk.log('EMAIL '  + JSON.stringify(emails));
          if (emails.email) {
            // console_bk.log('EMAIL      LLL 11 ' + emails.email);
            let emailSplit = emails.email.split(',');
            _.each(emailSplit, (em) => {
              // console_bk.log('EMAIL      LLL  ' + em + ' length ' + this.listEmailFacebook.length + 1);
              this.listEmailFacebook.push({ id: +this.listEmailFacebook.length + 1, email: em });
            });
            // console_bk.log('EMAIL      LLL 11 ' + JSON.stringify(this.listEmailFacebook));
            this.cdRef.detectChanges();
          }
        }
      });
  }

  private deleteEmailReport(event, id) {
    // console_bk.log('DELTE deleteEmailReport');
    this.listEmailReport = this.listEmailReport.filter((item) => item.id !== id);
    this.cdRef.detectChanges();
  }
  private addEmailReport() {
    // console_bk.log('add addEmailReport ' + JSON.stringify(this.listMember));
    this.listEmailReport.push({ id: this.listEmailReport.length + 1, email: '' });
    this.cdRef.detectChanges();
  }
  private saveEmailReport() {
    let er = false;
    // console_bk.log('save saveEmailReport');
    // console_bk.log(JSON.stringify(this.listEmailReport));
    let listE = [];
    _.each(this.listEmailReport, (em) => {
      if (em.email !== '') {
        if (this.validationService.validateEmail(em.email)) {
          listE.push(em.email);
        } else {
          // this.notificationService.showSwal('Email ' + em.email + ' không đúng định dạng');
          this.notificationService.showWarningTimer('Email ' + em.email + ' không đúng định dạng', 3000);
          // console_bk.log('KHONG DUNG DINH sang');
          er = true;
          return;
        }
        // listE.push(em.email);
      }
    });
    if (er) {
      return;
    }
    let body = {
      partner_id: this.partnerId,
      email: listE.toString()
    };
    this.applicationService.saveEmailReportByPartnerId(body)
      .then((notifications) => {
        if (notifications) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
    // console_bk.log('EMAIL ' + listE.toString());
  }
  private deleteEmailFacebook(event, id) {
    // console_bk.log('DELTE deleteEmailFacebook');
    this.listEmailFacebook = this.listEmailFacebook.filter((item) => item.id !== id);
    this.cdRef.detectChanges();
  }
  private addEmailFacebook() {
    // console_bk.log('add addEmailFacebook ' + JSON.stringify(this.listMember));
    this.listEmailFacebook.push({ id: this.listEmailFacebook.length + 1, email: '' });
    this.cdRef.detectChanges();
  }
  private saveEmailFacebook() {
    let er = false;
    // console_bk.log('save saveEmailFacebook');
    // console_bk.log(JSON.stringify(this.listEmailFacebook));
    let listE = [];
    _.each(this.listEmailFacebook, (em) => {
      if (em.email !== '') {
        if (this.validationService.validateEmail(em.email)) {
          listE.push(em.email);
        } else {
          // this.notificationService.showSwal('Email ' + em.email + ' không đúng định dạng');
          this.notificationService.showWarningTimer('Email ' + em.email + ' không đúng định dạng', 3000);
          // console_bk.log('KHONG DUNG DINH sang');
          er = true;
          return;
        }

      }
    });
    if (er) {
      return;
    }
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      email: listE.toString(),
      gatewayId: 1
    };
    // console_bk.log('save saveEmailFaceboo k' + JSON.stringify(body));
    this.applicationService.saveEmailFacebookByPartnerId(body)
      .then((face) => {
        if (face) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        } else {
          // this.notificationService.showSwal('Bạn chưa cấu hình tích hợp facebook nên không thể cấu hình chức năng này');
          this.notificationService.showWarningTimer('Bạn chưa cấu hình tích hợp facebook nên không thể cấu hình chức năng này', 3000);
        }
      });

  }
  private saveNotification() {
    // console_bk.log('NOTIFI ' + JSON.stringify(this.notificationConfigs));
    let body = {
      partner_id: this.partnerId,
      notification: this.notificationConfigs
    };
    this.applicationService.saveNotificationConfigByPartnerId(body)
      .then((notifications) => {
        if (notifications) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
  }
  private loadConfig() {
    // console_bk.log('Load loadConfig ');
    this.getAgentActive();
    this.getAllAgent();
    this.getScript();
    this.getTags();
    this.getHideCommentConfig();
  }
  private loadInvite() {
    this.listUserInvited();
  }
  private getAgentActive() {
    this.applicationService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          this.agentActive = agent;
          // console_bk.log('Agent ACTIVE '  + JSON.stringify(agent));
          this.selectedValue = agent.agent_id;
          // console_bk.log(this.selectedValue);
          this.cdRef.detectChanges();

        }
      });
  }
  private getAllAgent() {
    this.applicationService.getAllAgentByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          // console_bk.log('Agent s '  + JSON.stringify(agent));
          this.listAgent = agent;
          this.cdRef.detectChanges();
        }
      });
  }

  private getHideCommentConfig() {
    this.applicationService.getHideCommentConfig(this.appId, this.partnerId)
    .then((result) => {
        if (result) {
          // console_bk.log('Agent sdddd '  + JSON.stringify(result));
          this.hideComment = result.hideCommentOption;
          this.cdRef.detectChanges();
        }
      });
  }

  private getTags() {
    this.applicationService.getAllTagsByPartnerId(this.partnerId)
      .then((tags) => {
        if (tags) {
          // console_bk.log('ALL TGS ' + JSON.stringify(tags));
          this.listTags = tags;
        }
      });
  }
  private saveAgentConfig() {
    // console_bk.log(JSON.stringify(this.agentActive));
    // console_bk.log(JSON.stringify('isON ' + this.agentActive.is_on));

    let body = {
      partner_id: this.partnerId,
      agent_id: this.selectedValue,
      is_on: this.agentActive.is_on
    };
    this.applicationService.saveAgentConfigByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
  }

  private saveCommentConfig() {
    let body = {
      partner_id: this.partnerId,
      app_id: this.appId,
      hide_comment: this.hideComment
    };
    this.applicationService.saveCommentConfigByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
  }

  private saveColorConfig() {
    // console_bk.log('save saveColorConfig ' + this.colorBackground);
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'background_color',
          value: this.colorBackground
        },
        {
          key: 'background_color_content',
          value: this.colorBgContentChat
        },
        {
          key: 'color_content',
          value: this.colorContentChat
        },
        {
          key: 'popup_height',
          value: this.valueHeight
        },
        {
          key: 'popup_width',
          value: this.valueWidth
        },
        {
          key: 'collapse',
          value: this.collapsePopup
        },
        {
          key: 'require_infomation',
          value: this.requireInfomation
        }
      ]
    };
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
  }

  private savePlaySound() {
    // console_bk.log('save saveColorConfig ' + this.colorBackground);
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'play_sound',
          value: this.play_sound
        },
        {
          key: 'show_notify',
          value: this.show_notify
        }
      ]
    };
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });
  }

  private deleteTag(event, id, deleted) {
    // console_bk.log('DELTE deleteTag');
    if (deleted) {
      this.listTags = this.listTags.filter((item) => item.delele !== deleted);
    } else {
      this.listTags = this.listTags.filter((item) => item.id !== id);
    }
    this.cdRef.detectChanges();
  }
  private addTagss() {
    // console_bk.log('add addTag ' + JSON.stringify(this.listMember));
    this.listTags.push({ id: -1, color: this.color, name: '', delele: this.listTags.length + 1 });
    this.cdRef.detectChanges();
  }

  private saveTag() {
    // console_bk.log('save saveTag');
    // console_bk.log(JSON.stringify(this.listTags));
    let body = {
      partner_id: this.partnerId,
      tags: this.listTags
    };
    this.applicationService.saveTagsByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        }
      });

  }
  private finishDone() {
    this.router.navigate(['/application-hana/application-list']);
  }

  private nextStep(step) {
    $('#' + step).click();
  }
}

