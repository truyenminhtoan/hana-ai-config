import { ConfigDrapBotComponent } from './config-drap-bot/config-drap-bot.component';
// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// This Module's Components
import { ApplicationHanaComponent } from './application-hana.component';
// import { NgaModule } from 'nga.module';
// import { SideBarComponent } from '../../themes/sidebar/sidebar.component';
import { NavbarComponent } from '../../themes/navbar/navbar.component';
import { ColorPickerModule } from 'angular2-color-picker';
import { MdSliderModule, MdSelectModule, MdProgressBarModule, MdProgressSpinnerModule, MdRadioModule, MatDialogModule, MdDialogModule } from '@angular/material';
import { routing } from './application-hana.routing';
import { NgaModule } from '../../themes/nga.module';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationCreateComponent } from './application-create/application-create.component';
import { ApplicationConfigComponent } from './application-config/application-config.component';
import { ApplicationFacebookChooseComponent } from './application-facebook-choose/application-facebook-choose.component';
import { ApplicationConfigDetailComponent } from './application-config-detail/application-config-detail.component';
import { ApplicationRedirectComponent } from './application-redirect/application-redirect.component';
import { NotificationService } from '../../_services/notification.service';
// import { ConfigService } from '../../_services/config.service';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ApplicationFanpageChooseComponent } from './application-fanpage-choose/application-fanpage-choose.component';
import { ApplicationConfigDetailNewComponent } from './application-config-detail-new/application-config-detail-new.component';
import { FacebookService } from '../../_services/facebook.service';
import { ApplicationService } from '../../_services/application.service';
import { ValidationService } from '../../_services/ValidationService';
import { ApplicationPaymentComponent } from "./application-payment/application-payment.component";
import { DataTableModule, SharedModule, DragDropModule } from 'primeng/primeng';
import { MdTooltipModule } from '@angular/material';
import { PopupDialogComponent } from './application-config-detail/components/popup-dialog/popup-dialog.component';
import { TagInputModule } from 'ng2-tag-input';
@NgModule({
    imports: [
        routing,
        NgaModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule,
        MdSliderModule,
        MdSelectModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        SlimLoadingBarModule.forRoot(),
        MdRadioModule,
        MatDialogModule,
        MdDialogModule,
        DataTableModule,
        TagInputModule,
        SharedModule,
        DragDropModule,
        MdTooltipModule
    ],
    declarations: [
        ApplicationHanaComponent,
        ApplicationListComponent,
        ApplicationCreateComponent,
        ApplicationConfigComponent,
        ApplicationFacebookChooseComponent,
        ApplicationFanpageChooseComponent,
        ApplicationConfigDetailComponent,
        ApplicationConfigDetailNewComponent,
        ApplicationRedirectComponent,
        ApplicationPaymentComponent,
        ConfigDrapBotComponent,
        PopupDialogComponent
        // SideBarComponent,
        // NavbarComponent,
    ],
    exports: [
        ApplicationHanaComponent,
        SlimLoadingBarModule,
        MdSelectModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        MdRadioModule,
        ConfigDrapBotComponent,
        PopupDialogComponent
    ],
    providers: [
        FacebookService,
        ApplicationService,
        ValidationService,
        NotificationService,
        // ConfigService
    ],
    bootstrap: [
        PopupDialogComponent
    ]
})
export class ApplicationHanaModule {

}
