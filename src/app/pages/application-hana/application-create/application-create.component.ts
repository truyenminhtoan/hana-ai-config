import {Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef} from '@angular/core';

import { ApplicationService } from '../../../_services/application.service';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { NotificationService } from '../../../_services/notification.service';

declare var $: any;
declare var _: any;

@Component({
    selector: 'application-create',
    templateUrl: 'application-create.component.html',
    styleUrls: ['application-create.component.scss']
})
export class ApplicationCreateComponent implements OnInit , AfterViewInit{
  private appName: any;
  private complexForm: FormGroup;
  private submit = false;
  private error = false;
  private errorMsg: any;
  private currentUser: any;
  private partnerId: any;
  private color = 'primary';
  private mode = 'indeterminate';
  private value = 50;
  private bufferValue = 75;
  private loadingProcess: boolean = false;
  private formErrors = {
    appName: ''
  };
  private validationMessages = {
    appName: {
      required:      'Name is required.',
      minlength:     'Name must be at least 4 characters long.',
      maxlength:     'Name cannot be more than 24 characters long.',
    },
  };
  constructor(private applicationService: ApplicationService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private cdRef: ChangeDetectorRef,
        private notificationService: NotificationService) {
      this.complexForm = fb.group({
      appName : [null, Validators.required]});
  }

  public ngOnInit() {
    // console_bk.log('ApplicationCreateComponent');
    this.checkLoged();
  }
  public ngAfterViewInit() {
    $('#appsname').focus();
     $('.dropdown-toggle').dropdown();
    setTimeout(function() {
      $.getScript('../../../assets/js/popup-hana.js');
    }, 2000);
  }

  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    this.cdRef.detectChanges();
  }

  public submitForm (value: any) {
    this.loadingProcess = true;
    let name = value.appName ? value.appName.trim() : '';
    if (this.submit) {
      return;
    }
    this.submit = true;
    if (name === '') {
      this.error = true;
      this.errorMsg = 'Vui lòng nhập tên';
      this.submit = false;
      this.loadingProcess = false;
      return;
    }

    // console_bk.log(' NAME ' + name);
    this.notificationService.showWarningTimer('Đang tạo ứng dụng và đồng bộ dữ liệu vui lòng chờ ít phút...', 6000);
    this.applicationService.createBackendPartner(value.appName, this.currentUser.id)
      .then((integrate) => {
        this.submit = false;
        if (integrate) {
          // console_bk.log('Fanpage ' + JSON.stringify(integrate));
          this.router.navigate(['/application-hana/application-config', integrate.apllication.partner_id]);
        } else {
          this.router.navigate(['/application-hana/application-list']);
        }
      });
  }
}
