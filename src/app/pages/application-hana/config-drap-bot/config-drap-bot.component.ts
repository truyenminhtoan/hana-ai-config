import { EmitterService } from './../../../_services/emitter.service';
import { NotificationService } from './../../../_services/notification.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'config-drap-bot',
    templateUrl: 'config-drap-bot.component.html',
    styleUrls: ['config-drap-bot.component.scss']
})
export class ConfigDrapBotComponent implements OnInit {
    @Input() private inBotTime;
    @Output() private outBotTime = new EventEmitter<any>();
    private hourAvailable = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    private beginHour = null;
    private endHour = null;

    private availableTimes = [];

    private selectedTimes = [];
    private selectedTimesOutput = [];

    private draggedTime;

    constructor(private notificationService: NotificationService) { }

    public submitAdd() {
        if (this.beginHour === null || this.endHour === null) {
            return this.notificationService.showDanger('Vui lòng chọn đầy đủ thời gian bắt đầu và thời gian kết thúc');
        }
        if (this.beginHour >= this.endHour) {
            return this.notificationService.showDanger('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        }
        this.availableTimes.push({ start: this.beginHour, end: this.endHour });
        this.beginHour = null;
        this.endHour = null;
    }

    public addToSelectTime(i, beginHour, endHour) {
        // if (beginHour && endHour) {
        this.selectedTimes[i].push({ start: beginHour, end: endHour });
        this.outputTime();
        // }
    }

    public checkSelectedEmpty() {
        let i = 0;
        this.selectedTimes.filter((item) => {
            if (item.length === 0) {
                console.log(i, item.length);
                this.addToSelectTime(i, 0, 23);
            }
            i++;
        });
    }

    public ngOnInit() {
        EmitterService.get('CONFIG_BOT_TIME_LOAD').subscribe((data: any) => {
            this.selectedTimes = [[], [], [], [], [], [], []];
            this.selectedTimesOutput = [{}, {}, {}, {}, {}, {}, {}];
            this.inBotTime = data;
            if (this.inBotTime) {
                try {
                    let json = JSON.parse(this.inBotTime);
                    if (json) {
                        json.filter((item) => {
                            if (item.data) {
                                item.data.filter((it) => {
                                    this.addToSelectTime(item.day, it.start, it.end);
                                });
                            }
                        });
                        // this.checkSelectedEmpty();
                    }
                } catch (error) {
                    // error
                }
            }
        });
    }

    public dragStart(event, time) {
        this.draggedTime = time;
    }

    public drop(event, i) {
        if (this.selectedTimes[i].length >= 5) {
            this.notificationService.showDanger('Tối đa 5 mốc thời gian');
            this.draggedTime = null;
            return;
        }
        if (this.draggedTime) {
            let draggedTimeIndex = this.findIndex(this.draggedTime);
            this.selectedTimes[i] = [...this.selectedTimes[i], this.draggedTime];
            // this.availableTimes = this.availableTimes.filter((val, i) => i != draggedTimeIndex);
            this.draggedTime = null;
            this.outputTime();
        }
    }

    public dropAt(i, j) {
        this.selectedTimes[i].splice(j, 1);
        this.outputTime();
    }

    public dropAtAvailiable(i) {
        this.availableTimes.splice(i, 1);
    }

    public dragEnd(event) {
        this.draggedTime = null;
    }

    public findIndex(time) {
        let index = -1;
        for (let j = 0; j < this.availableTimes.length; j++) {
            if (time === this.availableTimes[j]) {
                index = j;
                break;
            }
        }
        return index;
    }

    public outputTime() {
        let i = 0;
        this.selectedTimes.filter((item) => {
            this.selectedTimesOutput[i].day = i;
            this.selectedTimesOutput[i].data = item;
            i++;
        });
        this.outBotTime.emit(JSON.stringify(this.selectedTimesOutput));
    }
}
