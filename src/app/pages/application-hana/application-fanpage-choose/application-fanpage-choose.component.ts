import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef
} from '@angular/core';
import { FacebookService } from '../../../_services/facebook.service';
import { ApplicationService } from '../../../_services/application.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../../../_services/config.service';
import { NotificationService } from '../../../_services/index';

declare var $: any;
declare var _: any;

@Component({
    selector: 'application-fanpage-choose',
    templateUrl: 'application-fanpage-choose.component.html',
    styleUrls: ['application-fanpage-choose.component.scss', '../plugin/semantic.css']
})
export class ApplicationFanpageChooseComponent implements OnInit, OnChanges {

    @Input() fanpageInfo: any;
    @Input() pageAccessToken: any;
    @Input() isIntegrate: boolean;
    @Input() appId: string;
    @Input() partnerId: string;


    @Output() fanpageInfoChange = new EventEmitter();
    @Output() isIntegrateChange = new EventEmitter<any>();
    // public isIntegrate: boolean;
    public listFanpages: any;
    private loading = true;
    private color = 'primary';
    private currentUser: any;
    private isDestroy: boolean;
    private mode = 'indeterminate';
    private value = 50;
    private bufferValue = 75;
    private loadingProcess: boolean = false;
    constructor(private facebookService: FacebookService, private cdRef: ChangeDetectorRef,
        private router: Router, private route: ActivatedRoute, private applicationService: ApplicationService,
        private serviceConfig: ConfigService, private notificationService: NotificationService) {
    }
    public ngOnInit() {
        this.checkLoged();

    }

    public ngOnChanges() {

    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }

    public checkLoged() {
        if (localStorage.getItem('currentUser') === 'undefined') {
            this.router.navigate(['/login-hana']);
        } else {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
        if (this.currentUser.login_type === 'facebook') {
            this.getAllFanpage();
        }
        this.callChange();
    }
    public getAllFanpage() {
        this.facebookService.getAllFanpageIntegrate()
            .then((integrate) => {
                if (integrate) {
                    this.listFanpages = integrate.userAccounts;
                    this.callChange();
                }
            });
    }

    public pageSubscribedApp(fanpage) {

        if (this.loadingProcess) {
            return false;
        }
        this.loadingProcess = true;
        this.notificationService.showWarningTimer('Đang tạo ứng dụng và đồng bộ dữ liệu vui lòng chờ ít phút...', 6000);
        // let that = this;
        // tao app
        console.log(' fanpageeee ' + JSON.stringify(fanpage));
        this.applicationService.createBackendPartner(fanpage.name, this.currentUser.id)
            .then((integrate) => {
                if (integrate) {
                    console.log('fanpageeee createBackendPartner ' + JSON.stringify(fanpage));
                    this.facebookService.pageSubscribedAppJSON(integrate.apllication.app_id, fanpage, integrate.apllication.partner_id)
                        .then((integratefp) => {
                            if (integratefp !== false) {
                                console.log('fanpageeee USING' + JSON.stringify(fanpage));
                                // this.inviteUserNameFacebook(fanpage, integrate.apllication.partner_id, integrate.apllication.app_id);
                                this.getAppAndUsing(integrate.apllication.partner_id);
                            } else {
                                this.router.navigate(['/application-hana/application-list']);
                            }
                        });
                } else {
                    this.router.navigate(['/application-hana/application-list']);
                }
            });

    }

    public inviteUserNameFacebook(fanpage, partnerId, appId) {
        let emailInvite = [];
        let userfacebooks = [];
        _.each(fanpage.roles, (role) => {
            userfacebooks.push({ email: role.id, role: role.role, status: 0 });
            emailInvite.push(role.id);
        });
        this.applicationService.getEmailInviteByAppId(emailInvite, partnerId)
            .then((integrate) => {
                this.loadingProcess = false;
                if (integrate) {
                    _.each(userfacebooks, (erole) => {
                        let findV = _.find(integrate.data, (item) => { return item === erole.email; });
                        if (findV) {
                            erole.status = 1;
                        }
                    });
                    this.applicationService.insertEmailInviteByAppId(userfacebooks, partnerId, 'facebook', '', appId)
                        .then((invites) => {
                            // console_bk.log('TICH HOP xx ' + JSON.stringify(invites));
                        });
                }
            });
    }
    private getAppAndUsing(partnerId: any) {
        // console.log('getPartnerDetailById');
        this.applicationService.getPartnerDetailById(partnerId)
            .then((integrate) => {
                this.usingApp(integrate);
            });
    }
    private usingApp(app) {
        console.log('APPPPPPP: ' + JSON.stringify(app));
        // alert(JSON.stringify(app));
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            currentUser.version = app.version;
            currentUser.partner_id = app.id;
            currentUser.app_using = app;
            if (currentUser.role_id !== 1) {
                let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
                if (user) {
                    currentUser.role_id = user.role_id;
                }
            }
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
            this.currentUser = currentUser;
            this.getTryUsing();

        }

        //  window.open('http://test.mideasvn.com:8001/authen/', '_blank');
        // let windows = window;
        /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
            .then((apps) => {
                this.newTab(this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token);
            }); */

    }


    private getTryUsing() {
        let integrations = this.currentUser.app_using.integrations;
        let fbInte = _.find(integrations, (inte) => {
            return inte.gatewayId === 1;
        });
        if (fbInte) {
            if (fbInte.isExpire === 0) {
                let body = {
                    page_id: fbInte.pageId,
                    partner_id: this.currentUser.partner_id
                }
                this.applicationService.getTryUsing(body)
                    .then((apps) => {

                        // console.log('paypay ' + JSON.stringify(apps));
                        this.currentUser.payment = apps.data;
                        this.currentUser.paymentstatus = false;
                        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                        console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ' + '/' + this.currentUser.app_using.id + '/messages');
                        this.router.navigate(['/' + this.currentUser.app_using.id + '/messages']);
                    });
            }
        }
    }

    private newTab(url) {
        // console_bk.log('OPen new tabbbbbbbbbbbbbbb' + url);
        var form = document.createElement('form');
        form.method = 'GET';
        form.action = url;
        document.body.appendChild(form);
        form.submit();
    }

    private callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
