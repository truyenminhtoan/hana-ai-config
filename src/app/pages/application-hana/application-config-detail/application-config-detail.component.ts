import { AgentService } from './../../../_services/agents.service';
import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
// import { FacebookService, ValidationService } from '../../../_services';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { Observable } from 'rxjs/Observable';
import { ColorPickerService } from 'angular2-color-picker';
import { NotificationService } from '../../../_services/notification.service';
import { ConfigService } from '../../../_services/config.service';
import { FacebookService } from '../../../_services/facebook.service';
import { ValidationService } from '../../../_services/ValidationService';
import { EmitterService } from "../../../_services/emitter.service";
// import initWizard = require('../../../../assets/js/init/initWizard.js');
import { ClipboardService } from 'ng2-clipboard/ng2-clipboard';

declare var $: any;
declare var swal: any;
declare const FB: any;
// declare var _: any;
import * as _ from 'underscore';
import { MdDialog } from '@angular/material';
import { PopupDialogComponent } from '../application-config-detail/components/popup-dialog/popup-dialog.component';
interface FileReaderEventTarget extends EventTarget {
  result: string;
}
interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

@Component({
  selector: 'application-config-detail',
  templateUrl: 'application-config-detail.component.html',
  // encapsulation: ViewEncapsulation.None,
  styleUrls: ['application-config-detail.component.scss'],
  // styleUrls: ['application-config-detail.component.scss', '../plugin/semantic.css'],
  // styleUrls: ['application-config-detail.component.scss', '../plugin/semantic.css', '../plugin/step/assets/css/style.css', '../plugin/step/assets/css/form-elements.css'],
})
export class ApplicationConfigDetailComponent implements OnInit, OnChanges, AfterViewInit {
  public fanpageInfo: any;
  public isIntegrate: boolean;
  public pageAccessToken: any;
  public partnerId: any;
  public appId: any;
  public appDetai: any;
  public sub: any;
  public listMember = [];
  public script: any;
  private isDestroy: boolean;
  public isNewMsg = false;
  public isNewMember = false;
  public isNewInfo = false;
  public isUnderstand = false;
  public isNewDemand = false;
  public color: string = '#127bdc';
  public notificationConfigs = [];
  public listEmailReport = [];
  public listEmailReportString: any;
  public listEmailFacebook = [];
  public listEmailFacebookString: any;
  public listTags = [];
  public agentActive: any;
  public listAgent = [];
  public selectedValue: any;
  public colorBackground: string = '#127bdc';
  public colorBgContentChat: string = '#127bdc';
  public colorBgContentChatcskh: string = '#f4f7f9';
  public colorContentChat: string = '#ffffff';
  // tslint:disable-next-line:variable-name
  public play_sound: any;
  // tslint:disable-next-line:variable-name
  public show_notify: any;
  public listMemberInvited: any = [];

  public min: number = 200;
  public max: number = 800;
  public step: number = 1;
  public valueWidth: number = 330;
  public valueHeight: number = 280;
  public currentUser: any;
  public app: any;
  public popupIntegrate: any;
  public collapsePopup: number = 1;
  public requireInfomation: number = 0;

  public isShowPurcharsed = false;

  public timeComebackDay: number = 0;
  public timeComebackHour: number = 2;
  public timeComebackMin: number = 0;
  public valueReadTime = 5;

  public hideCommentOption: any = [
    { key: 0, value: 'Không ẩn' },
    { key: 1, value: 'Ẩn tất cả' },
    { key: 2, value: 'Ẩn có số điện thoại' }
  ];
  public divChatOption: any = [
    { key: 0, value: 'Gửi cho tất cả tài khoản' },
    { key: 1, value: 'Chia đều cho tài khoản ưu tiên đang online' },
  ];
  public divChatChoose: number = 0;
  public hideComment: number = 0;

  public favoriteSeason: string;

  public seasons = [
    'Winter',
    'Spring',
    'Summer',
    'Autumn',
  ];

  // toantm1: continous_step_dont_understand_silience
  public dontUnserstandNum = null;

  constructor(public agentService: AgentService, public clipboard: ClipboardService, public facebookService: FacebookService, public route: ActivatedRoute, public router: Router, public applicationService: ApplicationService,
    public validationService: ValidationService, public cdRef: ChangeDetectorRef, public cpService: ColorPickerService,
    public notificationService: NotificationService, public serviceConfig: ConfigService, public dialog: MdDialog) {
    console.log('DETAILCONF');
    // $.getScript('../assets/js/jscolor.min.js');
  }

  public showModel(type) {
    if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1)) {
      $('#exampleModalLong').modal('show');
    } else {
      FB.login((response) => {
        if (response.authResponse) {
          FB.api('/me', { fields: 'id,name,email' }, (res) => {
            localStorage.setItem('accessToken', response.authResponse.accessToken);
            this.pageAccessToken = response.authResponse.accessToken;
            this.callChange();
            $('#exampleModalLong').modal('show');
          });
        }
      }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
    }
  }

  public showScript(type) {
    $('#scriptModel').modal('show');
  }

  public usingApp(app) {
    // console.log('APP: ' + JSON.stringify(app));
    // alert(JSON.stringify(app));
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.currentUser = currentUser;
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      currentUser.app_using = app;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
    }
    //  window.open('http://test.mideasvn.com:8001/authen/', '_blank');
    // let windows = window;
    // this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
    //   .then((apps) => {
    //     this.newTab(this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token);
    //   });
    this.getAppInfo(); // toan lay thong tin app & redirect sang authen
  }

  public newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb' + url);
    let form = document.createElement("form");
    form.method = "GET";
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }


  public ngOnDestroy(): void {
    this.isDestroy = true;
  }
  public readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();

      // tslint:disable-next-line:only-arrow-functions
      reader.onload = function (e: FileReaderEvent) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  ngOnInit() {
    EmitterService.get('applicationPaymentId').subscribe((data: any) => {
      if (data.page_id) {
        this.isShowPurcharsed = true;
      }
    });
    this.createListMember();
    // this.getScript();
    // console_bk.log('INITTTTTTTTTTTTTTTTTTTTTT ' + localStorage.getItem('accessToken'));
    this.sub = this.route.params.subscribe((params) => {
      this.partnerId = params['id'];
    });
    // console_bk.log('partner id ' + this.partnerId);
    this.getAppDetail();
    if (localStorage.getItem('accessToken')) {
      this.pageAccessToken = localStorage.getItem('accessToken');
    }

    FB.init({
      // appId: '378516159201961',
      appId: this.serviceConfig.facebookConfig.client_id,
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
    // var $validator = $('.wizard-card form').validate({
    //     rules: {
    //         firstname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         lastname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         email: {
    //             required: true,
    //             minlength: 3,
    //         }
    //     },
    //
    //     errorPlacement: function (error, element) {
    //         $(element).parent('div').addClass('has-error');
    //     }
    // });

  }

  ngOnChanges() {
    // console_bk.log('CHANGE EEEEEEEEMIT    ' + this.isIntegrate);
    // console_bk.log('CHANGE EEEEEEEEMIT    ' + this.appId);
    if (this.isIntegrate) {
      this.checkIntegrateFacebook(this.appId);
    }
  }

  ngAfterViewInit() {
    $('.dropdown-toggle').dropdown();
    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      // tslint:disable-next-line:object-literal-key-quotes
      'tabClass': 'nav nav-pills',
      // tslint:disable-next-line:object-literal-key-quotes
      'nextSelector': '.btn-next',
      // tslint:disable-next-line:object-literal-key-quotes
      'previousSelector': '.btn-previous',

      onNext: function (tab, navigation, index) {
        // tslint:disable-next-line:no-var-keyword
        var $valid = $('.wizard-card form').valid();
        if (!$valid) {
          // $validator.focusInvalid();
          return false;
        }
      },

      onInit: function (tab, navigation, index) {

        //check number of tabs and fill the entire row
        var $total = navigation.find('li').length;
        var $width = 100 / $total;
        var $wizard = navigation.closest('.wizard-card');

        var $display_width = $(document).width();

        if ($display_width < 600 && $total > 3) {
          $width = 50;
        }

        navigation.find('li').css('width', $width + '%');
        var $first_li = navigation.find('li:first-child a').html();
        var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
        $('.wizard-card .wizard-navigation').append($moving_div);

        //    this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });

        $('.moving-tab').css('transition', 'transform 0s');
      },

      onTabClick: function (tab, navigation, index) {

        // var $valid = $('.wizard-card form').valid();
        //
        // if (!$valid) {
        //     return false;
        // } else {
        //     return true;
        // }
      },

      onTabShow: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        var $wizard = navigation.closest('.wizard-card');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function () {
          $('.moving-tab').text(button_text);
        }, 150);

        var checkbox = $('.footer-checkbox');

        if (index !== 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }

        // this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
      }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {

      this.readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
      // console_bk.log('click');

      var wizard = $(this).closest('.wizard-card');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');

    $('.wizard-card').each(function () {

      var $wizard = $(this);
      var index = $wizard.bootstrapWizard('currentIndex');
      // this.refreshAnimation($wizard, index);

      var total_steps = $wizard.find('li').length;
      var move_distance = $wizard.width() / total_steps;
      var step_width = move_distance;
      move_distance *= index;

      var $current = index + 1;

      if ($current == 1) {
        move_distance -= 8;
      } else if ($current == total_steps) {
        move_distance += 8;
      }

      $wizard.find('.moving-tab').css('width', step_width);
      $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

      });

      $('.moving-tab').css({
        'transition': 'transform 0s'
      });
    });
    setTimeout(function () {
      $.getScript('../../../assets/js/popup-hana.js');
    }, 2000);

  }

  public checkIntegrateFacebook(appId: string) {
    // console_bk.log('checkIntegrateFacebook ' + appId);
    let that = this;
    /*this.facebookService.checkIntegrationGateway(appId)
     .then((integrate) => {
     if (integrate.code === 200) {
     that.fanpageInfo = integrate.integrationGateway;
     this.callChange();
     }
     });*/
    this.applicationService.getIntegrationByAppId(appId, 1)
      .then((integrate) => {
        if (integrate) {
          that.fanpageInfo = integrate;
          this.callChange();
        }
      });
  }

  public getAppDetail() {
    console.log('TICH HOP getAppDetail ' + this.partnerId);
    let that = this;
    this.applicationService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP LOGGGGGGG' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.status);
        this.app = integrate;
        this.appId = integrate.applications[0].app_id;
        this.appDetai = integrate;
        this.checkIntegrateFacebook(this.appId);
        this.getScript();
        this.callChange();
        // alert(JSON.stringify(integrate));
        // toantm
        if (localStorage.getItem('currentUser')) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          currentUser.version = integrate.version;
          currentUser.app_using = integrate;
          if (currentUser.role_id !== 1) {
            let user = integrate.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
            if (user) {
              currentUser.role_id = user.role_id;
            }
          }
          if (currentUser.app_using && currentUser.app_using.integrations) {
            currentUser.app_using.integrations.filter((item) => {
              if (item.pageId) {
                this.isShowPurcharsed = true;
                EmitterService.get('applicationPaymentId').emit({ page_id: item.pageId });
              }
            });
          }
          // console_bk.log('THONG TIN USER:', JSON.stringify(currentUser));
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
        }
      });
  }

  // toantm: Lay lai thong tin app & redirect den authen
  public getAppInfo() {
    console.log('TICH HOP ' + this.partnerId);
    this.applicationService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        if (localStorage.getItem('currentUser')) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          currentUser.version = integrate.version;
          currentUser.app_using = integrate;
          if (currentUser.role_id !== 1) {
            let user = integrate.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
            if (user) {
              currentUser.role_id = user.role_id;
            }
          }
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, currentUser.app_using.id, currentUser.app_using.version)
            .then((apps) => {
              let callbackUrl = this.serviceConfig.HOST_MY_WEB + '/#/chat';
              this.newTab(this.serviceConfig.consoleHana + '/authen/' + currentUser.app_using.id + '/' + apps.token + '/' + btoa(callbackUrl));
              // this.newTab(this.serviceConfig.consoleHana + '/authen/' + currentUser.app_using.id + '/' + apps.token);
            }); */
          this.router.navigate(['/chat']);
        }
      });
  }

  public counterUpdate(event: boolean) {
    console.log('CHANGE UNPAGE SUB');
    this.isIntegrate = event;
    this.checkIntegrateFacebook(this.appId);
    // if (event) {
    //   this.checkIntegrateFacebook(this.appId);
    // } else {
    //   this.fanpageInfo = null;
    // }
  }

  public createListMember() {
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
  }
  public getScript() {
    this.applicationService.getIntegrationByAppId(this.appId, 2)
      .then((integrate) => {
        // console_bk.log('getScript ' + JSON.stringify(integrate));
        if (integrate) {
          this.popupIntegrate = integrate;
          this.script = integrate.script;
          this.colorBackground = integrate.background_color;
          this.colorContentChat = integrate.color_content ? integrate.color_content : this.colorContentChat;
          this.colorBgContentChat = integrate.background_color_content ? integrate.background_color_content : this.colorBgContentChat;
          this.colorBgContentChatcskh = integrate.background_color_content_cskh ? integrate.background_color_content_cskh : this.colorBgContentChatcskh;

          this.play_sound = integrate.play_sound ? integrate.play_sound : 0;
          this.show_notify = integrate.show_notify ? integrate.show_notify : 0;
          this.collapsePopup = integrate.collapse ? integrate.collapse : 1;
          this.requireInfomation = integrate.require_infomation ? integrate.require_infomation : 0;

          this.valueReadTime = integrate.readSpeed ? integrate.readSpeed : 5;

          if (integrate.timehanacomeback) {
            this.timeComebackDay = 0;
            this.timeComebackHour = 0;
            this.timeComebackMin = parseInt(integrate.timehanacomeback) / 60;
          }
          if (integrate.dividendchat) {
            this.divChatChoose = integrate.dividendchat;
          }
          // this.collapsePopup = 0;
          // console_bk.log('LOAD BACKGOUND');
          this.callChange();
        }
      });
  }
  public addNewEmail() {
    // console_bk.log('Addnew user');
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.callChange();
  }
  public removeNewEmail(event, id) {
    // console_bk.log('remove0 user ' + id  );
    // this.listMember.push({email: '', status: 'add'});
    this.listMember = this.listMember.filter((item) => item.id !== id);
    this.callChange();
  }

  public checkEmailExits(event, id) {
    let object = _.find(this.listMember, (obj) => { return obj.id === id });
    if (object.email) {
      // this.applicationService.getEmailInviteByAppId(object.email, this.partnerId)
      //   .then((integrate) => {
      //     // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
      //     // console_bk.log('TICH HOP ' + integrate.code);
      //     if (integrate.code === 200) {
      //
      //     }
      //   });

      // let actionOperate: Observable<any>;
      // actionOperate = this.applicationService.getEmailInviteByAppId(object.email, this.partnerId);
      // actionOperate.subscribe(
      //   (data) => {
      //     // console_bk.log('DATA ' + JSON.stringify(data));
      //     if (data.errorCode === 200) {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'none';
      //         }
      //       });
      //     } else {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'add';
      //         }
      //       });
      //     }
      //   },
      //   (err) => {
      //     // console_bk.log('ERROR');
      //   });
    }
  }
  public inviteEmail() {
    let emailInvite = [];
    let emailRoles = [];
    this.listMember = this.listMember.filter((item) => item.email !== '');
    _.each(this.listMember, (member) => {
      if (this.validationService.validateEmail(member.email)) {
        // console_bk.log('EMAIL ' + member.email);
        emailInvite.push(member.email);
        emailRoles.push({ email: member.email, role: 3, status: 0 });
      } else {
        // console_bk.log('KHONG  ' + member.email);
      }
    });
    if (emailRoles.length === 0) {
      this.notificationService.showWarningTimer('Vui lòng nhập email', 3000);
      return false;
    }
    // console_bk.log('EMAIL   ' + JSON.stringify(emailInvite));
    this.applicationService.getEmailInviteByAppId(emailInvite, this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.code);
        if (integrate) {
          _.each(emailRoles, (erole) => {

            let findV = _.find(integrate, (item) => { return item === erole.email; });
            // console_bk.log('EMAIL 22  ' + JSON.stringify(findV));
            if (findV) {
              erole.status = 1;
            }
          });

          // console_bk.log('EMAIL 11111  ' + JSON.stringify(emailRoles));
          this.applicationService.insertEmailInviteByAppId(emailRoles, this.partnerId, 'email', this.appDetai.company, this.appId)
            .then((invites) => {
              // console_bk.log('TICH HOP xx ' + JSON.stringify(invites));
              if (invites) {
                // console_bk.log('EMAIL  DONE ' + JSON.stringify(emailInvite));
                this.listMember = [];
                this.createListMember();
                // this.notificationService.showSwal('Mời thành công');
                this.listUserInvited();
                this.notificationService.showSuccessTimer('Mời thành công', 3000);
              }
            });
        }
      });

  }

  public listUserInvited() {
    this.applicationService.getListUserInvited(this.partnerId)
      .then((invites) => {
        if (invites) {
          this.listMemberInvited = invites;
        }
      });
  }

  public loadNotificaton() {
    if (this.notificationConfigs.length === 0) {
      this.notificationConfigs.push({ id: -1, notification_type: 'new_cus', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'all_msg', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'dont_understand', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'info_customer', status: 1 });
      this.notificationConfigs.push({ id: -1, notification_type: 'new_demand', status: 1 });
    }

    // this.callChange();
    this.applicationService.getNotificationConfigByPartnerId(this.partnerId)
      .then((notifications) => {
        if (notifications) {
          if (notifications.length > 0) {
            // console_bk.log('LIST NOTIFICAON ' + JSON.stringify(notifications));
            this.notificationConfigs = notifications;
          }
        }
      });
    this.loadEmailReport();
    this.loadEmailFacebook();
    this.getScript();
    // console_bk.log('Load loadNotificaton ');
  }
  public loadEmailReport() {
    /*if (this.listEmailReportString) {
     this.listEmailReport.push({id: this.listEmailReport.length + 1 , email: 'docaohuynh@gmail.com'});
     } else {
     this.listEmailReport.push({id: this.listEmailReport.length + 1 , email: ''});
     }*/
    this.listEmailReport = [];
    this.applicationService.getEmailReportByPartnerId(this.partnerId)
      .then((emails) => {
        if (emails) {
          // console_bk.log('EMAIL '  + JSON.stringify(emails));
          if (emails.email) {
            // console_bk.log('EMAIL      LLL 11 ' + emails.email);
            let emailSplit = emails.email.split(',');
            _.each(emailSplit, (em) => {
              // console_bk.log('EMAIL      LLL  ' + em + ' length ' + this.listEmailReport.length + 1);
              this.listEmailReport.push({ id: +this.listEmailReport.length + 1, email: em });
            });
            // console_bk.log('EMAIL      LLL 11 ' + JSON.stringify(this.listEmailReport));
            this.callChange();
          }
        }
      });
  }

  public loadEmailFacebook() {
    this.listEmailFacebook = [];
    this.applicationService.getEmailFacebookByPartnerId(this.appId, this.partnerId)
      .then((emails) => {
        if (emails) {
          // console_bk.log('EMAIL '  + JSON.stringify(emails));
          if (emails.email) {
            // console_bk.log('EMAIL      LLL 11 ' + emails.email);
            let emailSplit = emails.email.split(',');
            _.each(emailSplit, (em) => {
              // console_bk.log('EMAIL      LLL  ' + em + ' length ' + this.listEmailFacebook.length + 1);
              this.listEmailFacebook.push({ id: +this.listEmailFacebook.length + 1, email: em });
            });
            // console_bk.log('EMAIL      LLL 11 ' + JSON.stringify(this.listEmailFacebook));
            this.callChange();
          }
        }
      });
  }

  public deleteEmailReport(event, id) {
    // console_bk.log('DELTE deleteEmailReport');
    this.listEmailReport = this.listEmailReport.filter((item) => item.id !== id);
    this.callChange();
  }
  public addEmailReport() {
    // console_bk.log('add addEmailReport ' + JSON.stringify(this.listMember));
    this.listEmailReport.push({ id: this.listEmailReport.length + 1, email: '' });
    this.callChange();
  }
  public saveEmailReport() {
    let er = false;
    // console_bk.log('save saveEmailReport');
    // console_bk.log(JSON.stringify(this.listEmailReport));
    let listE = [];
    _.each(this.listEmailReport, (em) => {
      if (em.email !== '') {
        if (this.validationService.validateEmail(em.email)) {
          listE.push(em.email);
        } else {
          // this.notificationService.showSwal('Email ' + em.email + ' không đúng định dạng');
          this.notificationService.showWarningTimer('Email ' + em.email + ' không đúng định dạng', 3000);
          // console_bk.log('KHONG DUNG DINH sang');
          er = true;
          return;
        }
        // listE.push(em.email);
      }
    });
    if (er) {
      return;
    }
    let body = {
      partner_id: this.partnerId,
      email: listE.toString()
    };
    this.applicationService.saveEmailReportByPartnerId(body)
      .then((notifications) => {
        if (notifications) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
    // console_bk.log('EMAIL ' + listE.toString());
  }
  public deleteEmailFacebook(event, id) {
    // console_bk.log('DELTE deleteEmailFacebook');
    this.listEmailFacebook = this.listEmailFacebook.filter((item) => item.id !== id);
    this.callChange();
  }
  public addEmailFacebook() {
    // console_bk.log('add addEmailFacebook ' + JSON.stringify(this.listMember));
    this.listEmailFacebook.push({ id: this.listEmailFacebook.length + 1, email: '' });
    this.callChange();
  }
  public saveEmailFacebook() {
    let er = false;
    // console_bk.log('save saveEmailFacebook');
    // console_bk.log(JSON.stringify(this.listEmailFacebook));
    let listE = [];
    _.each(this.listEmailFacebook, (em) => {
      if (em.email !== '') {
        if (this.validationService.validateEmail(em.email)) {
          listE.push(em.email);
        } else {
          // this.notificationService.showSwal('Email ' + em.email + ' không đúng định dạng');
          this.notificationService.showWarningTimer('Email ' + em.email + ' không đúng định dạng', 3000);
          // console_bk.log('KHONG DUNG DINH sang');
          er = true;
          return;
        }

      }
    });
    if (er) {
      return;
    }
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      email: listE.toString(),
      gatewayId: 1
    };
    // console_bk.log('save saveEmailFaceboo k' + JSON.stringify(body));
    this.applicationService.saveEmailFacebookByPartnerId(body)
      .then((face) => {
        if (face) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công', 3000);
        } else {
          // this.notificationService.showSwal('Bạn chưa cấu hình tích hợp facebook nên không thể cấu hình chức năng này');
          this.notificationService.showWarningTimer('Bạn chưa cấu hình tích hợp facebook nên không thể cấu hình chức năng này', 5000);
        }
      });

  }
  public saveNotification() {
    // console_bk.log('NOTIFI ' + JSON.stringify(this.notificationConfigs));
    let body = {
      partner_id: this.partnerId,
      notification: this.notificationConfigs
    };
    this.applicationService.saveNotificationConfigByPartnerId(body)
      .then((notifications) => {
        if (notifications) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }
  public loadConfig() {
    // console_bk.log('Load loadConfig ');
    this.getAgentActive();
    this.getAllAgent();
    this.getScript();
    this.getTags();
    this.getHideCommentConfig();
  }
  public loadInvite() {
    this.listUserInvited();
  }
  public getAgentActive() {
    this.applicationService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          this.agentActive = agent;

          // toantm1
          EmitterService.get('CONFIG_BOT_TIME_LOAD').emit(this.agentActive.time_on_bot);
          // console_bk.log('Agent ACTIVE '  + JSON.stringify(agent));
          this.selectedValue = agent.agent_id;
          // console_bk.log(this.selectedValue);

          // load so lan khong hieu
          let agentTemp = this.listAgent.filter((ag) => {
            return ag.id === this.selectedValue;
          })[0];
          if (agentTemp) {
            this.dontUnserstandNum = agentTemp.continous_step_dont_understand_silience;
          }
          this.callChange();

        }
      });
  }
  public getAllAgent() {
    this.applicationService.getAllAgentByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          // console_bk.log('Agent s '  + JSON.stringify(agent));
          this.listAgent = agent;

          // load so lan khong hieu
          let agentTemp = this.listAgent.filter((ag) => {
            return ag.id === this.selectedValue;
          })[0];
          if (agentTemp) {
            this.dontUnserstandNum = agentTemp.continous_step_dont_understand_silience;
          }
          this.callChange();
        }
      });
  }

  public getHideCommentConfig() {
    this.applicationService.getHideCommentConfig(this.appId, this.partnerId)
      .then((result) => {
        if (result) {
          // console_bk.log('Agent sdddd '  + JSON.stringify(result));
          this.hideComment = result.hideCommentOption;
          this.callChange();
        }
      });
  }

  public getTags() {
    this.applicationService.getAllTagsByPartnerId(this.partnerId)
      .then((tags) => {
        if (tags) {
          // console_bk.log('ALL TGS ' + JSON.stringify(tags));
          this.listTags = tags;
        }
      });
  }
  public saveAgentConfig() {
    // console_bk.log(JSON.stringify(this.agentActive));
    // console_bk.log(JSON.stringify('isON ' + this.agentActive.is_on));
    let gt = +this.dontUnserstandNum;
    if (_.isNaN(gt)) {
      return this.notificationService.showDanger('Cài đặt Hana im lặng yêu cầu là số nguyên và không chứa text và ký tự đặc biệt');
    }
    if (!(_.isNumber(gt))) {
      return this.notificationService.showDanger('Cài đặt Hana im lặng yêu cầu là số nguyên và không chứa text và ký tự đặc biệt');
    }
    if (_.isEmpty(this.dontUnserstandNum)) {
      this.dontUnserstandNum = null;
    } else {
      if (gt < 0) {
        return this.notificationService.showDanger('Cài đặt Hana im lặng yêu cầu là số không âm');
      }
    }

    let body = {
      partner_id: this.partnerId,
      agent_id: this.selectedValue,
      is_on: this.agentActive.is_on,
      time_on_bot: this.agentActive.time_on_bot
    };
    this.applicationService.saveAgentConfigByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          let timeHanacomback = (this.timeComebackDay * 86400) + (this.timeComebackHour * 3600) + (this.timeComebackMin * 60);
          this.saveConfigTimeComeback(timeHanacomback);
          // cap nhat gia tri max khong hieu cho agent
          this.updateDontUnserstandNumAgent(this.selectedValue);
          // console.log('time ' + timeHanacomback);
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }

  public saveCommentConfig() {
    let body = {
      partner_id: this.partnerId,
      app_id: this.appId,
      hide_comment: this.hideComment
    };
    this.applicationService.saveCommentConfigByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }

  public saveDivChatConfig() {
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'dividendchat',
          value: this.divChatChoose
        }
      ]
    };
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }
  public saveConfigTimeComeback(time) {
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'timehanacomeback',
          value: time
        },
        {
          key: 'readSpeed',
          value: this.valueReadTime
        }
      ]
    };
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
        }
      });
  }
  public saveColorConfig() {
    // console_bk.log('save saveColorConfig ' + this.colorBackground);
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'background_color',
          value: this.colorBackground
        },
        {
          key: 'background_color_content',
          value: this.colorBgContentChat
        },
        {
          key: 'background_color_content_cskh',
          value: this.colorBgContentChatcskh
        },
        {
          key: 'color_content',
          value: this.colorContentChat
        },
        {
          key: 'popup_height',
          value: this.valueHeight
        },
        {
          key: 'popup_width',
          value: this.valueWidth
        },
        {
          key: 'collapse',
          value: this.collapsePopup
        },
        {
          key: 'require_infomation',
          value: this.requireInfomation
        }
      ]
    };
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
          this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }

  public savePlaySound() {
    // console_bk.log('save saveColorConfig ' + this.colorBackground);
    let body = {
      partner_id: this.partnerId,
      appId: this.appId,
      gatewayId: 2,
      keys: [
        {
          key: 'play_sound',
          value: this.play_sound
        },
        {
          key: 'show_notify',
          value: this.show_notify
        }
      ]
    };
    this.applicationService.saveScriptConfigByPartnerId(body)
      .then((config) => {
        if (config) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });
  }

  public deleteTag(event, id, deleted) {
    // console_bk.log('DELTE deleteTag');
    if (deleted) {
      this.listTags = this.listTags.filter((item) => item.delele !== deleted);
    } else {
      this.listTags = this.listTags.filter((item) => item.id !== id);
    }
    this.callChange();
  }
  public addTagss() {
    // console_bk.log('add addTag ' + JSON.stringify(this.listMember));
    this.listTags.push({ id: -1, color: this.color, name: '', delele: this.listTags.length + 1 });
    this.callChange();
  }

  public saveTag() {
    // console_bk.log('save saveTag');
    // console_bk.log(JSON.stringify(this.listTags));
    let body = {
      partner_id: this.partnerId,
      tags: this.listTags
    };
    this.applicationService.saveTagsByPartnerId(body)
      .then((agents) => {
        if (agents) {
          // this.notificationService.showSwal('Thành công');
          this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
        }
      });

  }
  public finishDone() {
    this.router.navigate(['/application-hana/application-list']);
  }

  public nextStep(step) {
    $('#' + step).click();
  }

  // toantm1
  public paymentInfo() {

  }

  public copyToClipboard() {
    this.clipboard.copy(this.script);
  }

  // toantm1
  public updateConfigBotTime(data) {
    this.agentActive.time_on_bot = data;
  }

  // toantm1
  public updateDontUnserstandNumAgent(agentId) {
    if (!agentId) {
      return;
    }

    let agentSelected = this.listAgent.filter((agent) => {
      return agent.id === agentId;
    })[0];

    let actionOperate: Observable<any>;
    let body = { id: agentSelected.id, name: agentSelected.name, partner_id: agentSelected.partner_id, continous_step_dont_understand_silience: this.dontUnserstandNum };
    actionOperate = this.agentService.update(body);
    actionOperate.subscribe(
      (ag) => {
        // success
        if (ag[0]) {
          this.dontUnserstandNum = ag[0].continous_step_dont_understand_silience;
        }
      },
      (err) => {
        console.log(err);
      });
  }

  public openDialogWebMessenger() {
    let dialogRef = this.dialog.open(PopupDialogComponent, {
      width: '50%',
      data: {
        title: 'Mã nhúng tích hợp website / Messenger',
        script: this.script,
        fanpageInfo: this.fanpageInfo,
        appId: this.appId,
        partnerId: this.partnerId,
        popupIntegrate: this.popupIntegrate
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result) {
          // console_bk.log('GETTTTTTTTTTTTTTTTTTTTTTTTTT ' + JSON.stringify(result));
        }
      }
    });
  }


  public callChange() {
    if (this.isDestroy) {
      return false;
    }
    this.cdRef.detectChanges();
  }
}

