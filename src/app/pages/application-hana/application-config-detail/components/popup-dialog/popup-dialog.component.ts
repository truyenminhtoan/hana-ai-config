import { Component, Inject, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ClipboardService } from 'ng2-clipboard/ng2-clipboard';
import { NotificationService, ValidationService, ConfigService, ApplicationService } from '../../../../../_services/index';


declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;

@Component({
    selector: 'popup-dialog',
    templateUrl: 'popup-dialog.component.html',
    styleUrls: ['popup-dialog.component.scss']
})
export class PopupDialogComponent {
    private isDestroy: boolean;
    private title: string;
    private script: string;
    private fanpageInfo: any;
    private appId: string;
    private partnerId: any;
    private websites = [];
    private popupIntegrate: any;
    private scriptmessenger: any;
    constructor( @Inject(MD_DIALOG_DATA) public data: any, private clipboard: ClipboardService, public dialogRef: MdDialogRef<PopupDialogComponent>,
        private cdRef: ChangeDetectorRef, private notificationService: NotificationService, private validationService: ValidationService,
        private configservice: ConfigService, private applicationService: ApplicationService) {
        this.title = data.title;
        this.script = data.script;
        this.fanpageInfo = data.fanpageInfo;
        this.appId = data.appId;
        this.partnerId = data.partnerId;
        this.popupIntegrate = data.popupIntegrate;
        this.scriptmessenger = data.popupIntegrate.scriptmessenger;
        this.getWhiteListDomain();
    }
    public ngOnDestroy(): void {
        this.isDestroy = true;
    }

    public close() {
        this.dialogRef.close();
    }

    public getWhiteListDomain() {
        this.websites = [];
        let body = {
            appId: this.appId,
            type: 'chat_plugin'
        };
        this.applicationService.getWhitelistDomain(body)
        .then((whitelist) => {
          if (whitelist) {
              let list = whitelist.whitelistedDomains;
              _.each(list, (web) => {
                  this.websites.push(web.url);
              });
          }
        });
    }

    private copyToClipboard(type) {
        if (type === 'website') {
            this.clipboard.copy(this.script);
        } else {
            this.clipboard.copy(this.scriptmessenger);
        }
        this.dialogRef.close();
        this.notificationService.showSuccessTimer('Copy thành công', 3000);
    }

    private addToGroup(value) {
        value = value.value;
        if (!this.validationService.validateHttpsUrl(value)) {
            this.websites = _.filter(this.websites, (web) => {
                return web !== value;
            });
            this.notificationService.showWarningTimer('Trang web phải đúng định dạng và bắt đầu bằng https://', 3000);
            return false;
        }
        console.log('adddvalue ' + JSON.stringify(this.websites));
    }


    private removeOutGroup(value) {
        value = value.value;
        this.websites = _.filter(this.websites, (web) => {
            return web !== value;
        });
        console.log('remove from ' + value);
    }
    private updateWebsiteLinks() {
        if (this.websites.length === 0) {
            this.notificationService.showWarningTimer('Bạn phải nhập website', 3000);
            return false;
        }
        let body = {
            appId: this.appId,
            whiteListedDomains: this.websites,
            type: 'chat_plugin'
        };
        this.applicationService.createWhitelistDomain(body)
        .then((integrate) => {
          if (integrate) {
              this.notificationService.showSuccessTimer('Thành công', 3000);
          }
        });
        console.log('api update ');
    }
    private createScriptMessenger() {
        if (this.websites.length === 0) {
            this.notificationService.showWarningTimer('Bạn phải nhập website', 3000);
            return false;
        }
        let pageId = this.fanpageInfo.pageId;
        let scriptmessenger = ''
            + '<script> '
            + '	  window.fbAsyncInit = function() '
            + '{		FB.init({ '
            + '		  appId            : \'' + this.configservice.facebookConfig.client_id + '\', '
            + '		  autoLogAppEvents : true, '
            + '		  xfbml            : true, '
            + '		  version          : \'v2.11\' '
            + '		});'
            + '};'
            + '(function(d, s, id) '
            + '{		 var js, fjs = d.getElementsByTagName(s)[0]; '
            + '		 if (d.getElementById(id)) {return;} '
            + '		 js = d.createElement(s);'
            + 'js.id = id;'
            + 'js.src = \'https://connect.facebook.net/en_US/sdk.js\';'
            + 'fjs.parentNode.insertBefore(js, fjs);'
            + '}(document, \'script\', \'facebook-jssdk\'));'
            + '</script> '
            + '<div class="fb-customerchat" '
            + ' page_id="' + pageId + '" '
            + ' minimized="true"> '
            + '</div>';
        this.scriptmessenger = scriptmessenger;
        this.savePlaySound();
        this.updateWebsiteLinks();
        console.log('creates ' + scriptmessenger);
    }

    private savePlaySound() {
        // console_bk.log('save saveColorConfig ' + this.colorBackground);
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'scriptmessenger',
                    value: this.scriptmessenger
                }
            ]
        };
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    // this.notificationService.showSwal('Thành công');
                    //   this.notificationService.showSuccessTimer('Thành công! Cài đặt này có thể mất vài phút để có hiệu lực', 5000);
                }
            });
    }

    private callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }

}
