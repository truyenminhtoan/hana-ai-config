import {
  Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
  ChangeDetectorRef
} from '@angular/core';
import { FacebookService } from '../../../_services/facebook.service';
import { ApplicationService } from "../../../_services/application.service";
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { EmitterService } from "../../../_services/emitter.service";

declare var $: any;
declare var _: any;

@Component({
  selector: 'application-facebook-choose',
  templateUrl: 'application-facebook-choose.component.html',
  styleUrls: ['application-facebook-choose.component.scss', '../plugin/semantic.css']
})
export class ApplicationFacebookChooseComponent implements OnInit, OnChanges {
  @Input() fanpageInfo: any;
  @Input() pageAccessToken: any;
  @Input() isIntegrate: boolean;
  @Input() appId: string;
  @Input() partnerId: string;

  private isDestroy: boolean;

  @Output() fanpageInfoChange = new EventEmitter();
  @Output() isIntegrateChange = new EventEmitter<any>();
  // public isIntegrate: boolean;
  public listFanpages: any;
  private loading = true;
  private color = 'primary';
  private mode = 'indeterminate';
  private value = 50;
  private bufferValue = 75;
  private loadingProcess: boolean = false;
  constructor(private facebookService: FacebookService, private cdRef: ChangeDetectorRef, private applicationService: ApplicationService) {
  }
  public ngOnInit() {
    // console_bk.log('fanpageINfo sao khong co log cho nay' + JSON.stringify(this.fanpageInfo));
    if (this.fanpageInfo && (this.fanpageInfo.pageId) && (this.fanpageInfo.isExpire !== 1)) {
      // console_bk.log('fanpageINfo tich hợp rồi hiển thị thông tin và hủy kết nối');
      this.isIntegrate = true;
    } else {
      this.isIntegrate = false;
      // console_bk.log('fanpageINfo chưa tích hợp hiển thị danh sách fanpage ');
    }
    if (this.pageAccessToken) {
      this.getAllFanpage();
    }
  }

  public ngOnDestroy(): void {
    this.isDestroy = true;
  }

  public ngOnChanges() {
    // console_bk.log('fanpageINfo ssssssssssss' + JSON.stringify(this.fanpageInfo));
    if (this.fanpageInfo && (this.fanpageInfo.pageId) && (this.fanpageInfo.isExpire !== 1)) {
      // console_bk.log('fanpageINfo tich hợp rồi hiển thị thông tin và hủy kết nối');
      this.isIntegrate = true;
    } else {
      this.isIntegrate = false;
      // console_bk.log('fanpageINfo chưa tích hợp hiển thị danh sách fanpage ');
    }
    if (this.pageAccessToken) {
      this.getAllFanpage();
    }
  }

  public getAllFanpage() {
    this.facebookService.getAllFanpage(this.pageAccessToken, this.partnerId)
      .then((integrate) => {
        // console_bk.log('Fanpagessss ' + JSON.stringify(integrate));

        if (integrate) {
          // console_bk.log('Huy TICH HOP ' + JSON.stringify(integrate.userAccounts));
          this.listFanpages = integrate.userAccounts;
          this.loading = false;
          this.callChange();
        }
      });
  }
  public pageUnSubscribedApp() {
    // let that = this;
    this.loadingProcess = true;
    this.facebookService.pageUnSubscribedAppJSON(this.appId, this.fanpageInfo, this.partnerId)
      .then((integrate) => {
        // console_bk.log('Huy TICH HOP ' + JSON.stringify(integrate));
        // if (integrate) {
        console.log('HUYN KET NOI THANH CONG');
        this.isIntegrate = false;
        this.fanpageInfo = undefined;
        this.isIntegrateChange.emit(this.isIntegrate);
        this.isIntegrateChange.emit(this.fanpageInfo);
        $('#exampleModalLong').modal('hide');
        // }
      });
  }

  public pageSubscribedApp(fanpage) {
    console.log('pageSubscribedApp');
    // let that = this;
    this.loadingProcess = true;
    // console_bk.log('pageSubscribedApp ' + JSON.stringify(fanpage));

    this.facebookService.pageSubscribedAppJSON(this.appId, fanpage, this.partnerId)
      .then((integrate) => {
        console.log('integrate:', fanpage);
        EmitterService.get('applicationPaymentId').emit({page_id: fanpage.id});
        if (integrate !== false) {
          // that.fanpageInfo = integrate.integrationGateway;
          this.isIntegrate = true;
          this.isIntegrateChange.emit(this.isIntegrate);
          this.loadingProcess = false;
          // this.inviteUserNameFacebook(fanpage);
        }
      });
  }

  public inviteUserNameFacebook(fanpage) {
    // let that = this;
    // // console_bk.log('pageSubscribedApp ' + JSON.stringify(fanpage));
    let emailInvite = [];
    let userfacebooks = [];
    _.each(fanpage.roles, (role) => {
      userfacebooks.push({ email: role.id, role: role.role, status: 0 });
      emailInvite.push(role.id);
    });
    // console_bk.log('pageSubscribedApp ' + JSON.stringify(userfacebooks));
    this.applicationService.getEmailInviteByAppId(emailInvite, this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.code);
        this.loadingProcess = false;
        if (integrate) {
          // _.each(integrate.data, (invited) => {
          //   userfacebooks = userfacebooks.filter( (item) => item.email !== invited);
          // });
          _.each(userfacebooks, (erole) => {
            let findV = _.find(integrate.data, (item) => { return item === erole.email; });
            // console_bk.log('EMAIL 22  ' + JSON.stringify(findV));
            if (findV) {
              erole.status = 1;
            }
          });
          // console_bk.log('EMAIL 11111  ' + JSON.stringify(userfacebooks));
          this.applicationService.insertEmailInviteByAppId(userfacebooks, this.partnerId, 'facebook', '', this.appId)
            .then((invites) => {
              // console_bk.log('TICH HOP xx ' + JSON.stringify(invites));
            });
        }
      });
  }

  public callChange() {
    if (this.isDestroy) {
      return false;
    }
    this.cdRef.detectChanges();
  }
}
