import { ClipboardService } from 'ng2-clipboard/ng2-clipboard';
import { Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
// import { FacebookService, ValidationService } from '../../../_services';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { Observable } from 'rxjs/Observable';
import { NotificationService } from '../../../_services/notification.service';
import { ConfigService } from '../../../_services/config.service';
import { FacebookService } from '../../../_services/facebook.service';
import { ValidationService } from '../../../_services/ValidationService';
// import initWizard = require('../../../../assets/js/init/initWizard.js');

declare var $: any;
declare var swal: any;
declare const FB: any;
declare var _: any;

interface FileReaderEventTarget extends EventTarget {
  result: string;
}
interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

@Component({
  selector: 'application-config',
  templateUrl: 'application-config.component.html',
  // encapsulation: ViewEncapsulation.None,
  styleUrls: ['application-config.component.scss'],
  // styleUrls: ['application-config.component.scss', '../plugin/semantic.css', '../plugin/step/assets/css/style.css', '../plugin/step/assets/css/form-elements.css'],
})
export class ApplicationConfigComponent implements OnInit, OnChanges, AfterViewInit {
  public fanpageInfo: any;
  public isIntegrate: boolean;
  public pageAccessToken: any;
  public partnerId: any;
  public appId: any;
  public appDetai: any;
  public listMember = [];
  public listMemberInvited: any = [];
  public script: any;
  private currentUser: any;

  constructor(private clipboard: ClipboardService, private facebookService: FacebookService, private route: ActivatedRoute, private router: Router, private applicationService: ApplicationService,
    private validationService: ValidationService, private cdRef: ChangeDetectorRef, private notificationService: NotificationService,
    private serviceConfig: ConfigService) {
  }

  public showModel(type) {
    if (this.fanpageInfo && this.fanpageInfo.pageId && (this.fanpageInfo.isExpire !== 1)) {
      $('#exampleModalLong').modal('show');
    } else {
      FB.login((response) => {
        if (response.authResponse) {
          FB.api('/me', { fields: 'id,name,email' }, (res) => {
            this.pageAccessToken = response.authResponse.accessToken;
            this.cdRef.detectChanges();
            $('#exampleModalLong').modal('show');
          });
        }
      }, { scope: 'email,public_profile,pages_messaging,manage_pages,read_page_mailboxes,publish_pages pages_messaging_subscriptions', return_scopes: true });
      // }
    }


  }
  public showScript(type) {
    $('#scriptModel').modal('show');
  }

  public readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e: FileReaderEvent) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  ngOnInit() {
    let current = localStorage.getItem('currentUser');
    if (current && current !== 'undefined') {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    } else {
      // console_bk.log('aaaaaaaaaaaaa ' + localStorage.getItem('currentUser'));
      this.router.navigate(['/login-hana']);
    }
    this.createListMember();
    // this.getScript();
    // console_bk.log('INITTTTTTTTTTTTTTTTTTTTTT ' + localStorage.getItem('accessToken'));
    this.route.params.subscribe((params) => {
      this.partnerId = params['id'];
    });
    // console_bk.log('partner id ' + this.partnerId);
    this.getAppDetail();
    if (localStorage.getItem('accessToken')) {
      this.pageAccessToken = localStorage.getItem('accessToken');
    }

    FB.init({
      // appId: '378516159201961',
      appId: this.serviceConfig.facebookConfig.client_id,
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.8' // use graph api version 2.5
    });
    // var $validator = $('.wizard-card form').validate({
    //     rules: {
    //         firstname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         lastname: {
    //             required: true,
    //             minlength: 3
    //         },
    //         email: {
    //             required: true,
    //             minlength: 3,
    //         }
    //     },
    //
    //     errorPlacement: function (error, element) {
    //         $(element).parent('div').addClass('has-error');
    //     }
    // });



  }

  ngOnChanges() {
    // console_bk.log('CHANGE EEEEEEEEMIT    ' + this.isIntegrate);
    console.log('CHANGE EEEEEEEEMIT    ' + this.appId);
    if (this.isIntegrate) {
      this.checkIntegrateFacebook(this.appId);
      this.getAllApp();
    }
  }

  public getAllApp() {
    this.applicationService.getListPartnerByUsername(this.currentUser.username)
      .then((apps) => {
        if (apps) {
          if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.currentUser = currentUser;
            currentUser.apps = apps;
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
          }
        }
      });
  }

  ngAfterViewInit() {
    $('.dropdown-toggle').dropdown();
    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',

      onNext: function (tab, navigation, index) {
        var $valid = $('.wizard-card form').valid();
        if (!$valid) {
          // $validator.focusInvalid();
          return false;
        }
      },

      onInit: function (tab, navigation, index) {

        //check number of tabs and fill the entire row
        var $total = navigation.find('li').length;
        var $width = 100 / $total;
        var $wizard = navigation.closest('.wizard-card');

        var $display_width = $(document).width();

        if ($display_width < 600 && $total > 3) {
          $width = 50;
        }

        navigation.find('li').css('width', $width + '%');
        var $first_li = navigation.find('li:first-child a').html();
        var $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
        $('.wizard-card .wizard-navigation').append($moving_div);

        //    this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });

        $('.moving-tab').css('transition', 'transform 0s');
      },

      onTabClick: function (tab, navigation, index) {

        // var $valid = $('.wizard-card form').valid();
        //
        // if (!$valid) {
        //     return false;
        // } else {
        //     return true;
        // }
      },

      onTabShow: function (tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index + 1;

        var $wizard = navigation.closest('.wizard-card');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        var button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function () {
          $('.moving-tab').text(button_text);
        }, 150);

        var checkbox = $('.footer-checkbox');

        if (index !== 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }

        // this.refreshAnimation($wizard, index);
        var total_steps = $wizard.find('li').length;
        var move_distance = $wizard.width() / total_steps;
        var step_width = move_distance;
        move_distance *= index;

        var $current = index + 1;

        if ($current == 1) {
          move_distance -= 8;
        } else if ($current == total_steps) {
          move_distance += 8;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
      }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {

      this.readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
      // console_bk.log('click');

      var wizard = $(this).closest('.wizard-card');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');

    $('.wizard-card').each(function () {

      var $wizard = $(this);
      var index = $wizard.bootstrapWizard('currentIndex');
      // this.refreshAnimation($wizard, index);

      var total_steps = $wizard.find('li').length;
      var move_distance = $wizard.width() / total_steps;
      var step_width = move_distance;
      move_distance *= index;

      var $current = index + 1;

      if ($current == 1) {
        move_distance -= 8;
      } else if ($current == total_steps) {
        move_distance += 8;
      }

      $wizard.find('.moving-tab').css('width', step_width);
      $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

      });

      $('.moving-tab').css({
        'transition': 'transform 0s'
      });
    });
    setTimeout(function () {
      $.getScript('../../../assets/js/popup-hana.js');
    }, 2000);
  }

  private checkIntegrateFacebook(appId: string) {
    // console_bk.log('checkIntegrateFacebook ' + appId);
    let that = this;
    /*this.facebookService.checkIntegrationGateway(appId)
      .then((integrate) => {
          if (integrate.code === 200) {
            that.fanpageInfo = integrate.integrationGateway;
            this.cdRef.detectChanges();
          }
      });*/
    this.applicationService.getIntegrationByAppId(appId, 1)
      .then((integrate) => {
        if (integrate) {
          that.fanpageInfo = integrate;
          this.cdRef.detectChanges();
        }
      });
  }

  private getAppDetail() {
    // console_bk.log('TICH HOP ');
    let that = this;
    this.applicationService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP LOGGGGGGG' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.status);
        this.appId = integrate.applications[0].app_id;
        this.appDetai = integrate;
        // console_bk.log(' APP ID ' + this.appId);
        this.checkIntegrateFacebook(this.appId);
        this.getScript();
        this.cdRef.detectChanges();
      });
  }

  private counterUpdate(event: boolean) {
    this.isIntegrate = event;
    if (event) {
      this.checkIntegrateFacebook(this.appId);
      this.getAllApp();
    } else {
      this.fanpageInfo = null;
    }
  }

  private createListMember() {
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
  }
  private getScript() {
    this.applicationService.getIntegrationByAppId(this.appId, 2)
      .then((integrate) => {
        // console_bk.log('getScript ' + JSON.stringify(integrate));
        if (integrate) {
          this.script = integrate.script;
          this.cdRef.detectChanges();
        }
      });
  }
  private addNewEmail() {
    // console_bk.log('Addnew user');
    this.listMember.push({ id: this.listMember.length + 1, email: '', status: 'add' });
    this.cdRef.detectChanges();
  }
  private removeNewEmail(event, id) {
    // console_bk.log('remove0 user ' + id  );
    // this.listMember.push({email: '', status: 'add'});
    this.listMember = this.listMember.filter((item) => item.id !== id);
  }

  private checkEmailExits(event, id) {
    let object = _.find(this.listMember, (obj) => { return obj.id === id });
    if (object.email) {
      // this.applicationService.getEmailInviteByAppId(object.email, this.partnerId)
      //   .then((integrate) => {
      //     // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
      //     // console_bk.log('TICH HOP ' + integrate.code);
      //     if (integrate.code === 200) {
      //
      //     }
      //   });

      // let actionOperate: Observable<any>;
      // actionOperate = this.applicationService.getEmailInviteByAppId(object.email, this.partnerId);
      // actionOperate.subscribe(
      //   (data) => {
      //     // console_bk.log('DATA ' + JSON.stringify(data));
      //     if (data.errorCode === 200) {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'none';
      //         }
      //       });
      //     } else {
      //       _.each(this.listMember, (member) => {
      //         if (member.id === id) {
      //           member.status = 'add';
      //         }
      //       });
      //     }
      //   },
      //   (err) => {
      //     // console_bk.log('ERROR');
      //   });
    }
  }
  private inviteEmail() {
    let emailInvite = [];
    let emailRoles = [];
    this.listMember = this.listMember.filter((item) => item.email !== '');
    _.each(this.listMember, (member) => {
      if (this.validationService.validateEmail(member.email)) {
        // console_bk.log('EMAIL ' + member.email);
        emailInvite.push(member.email);
        emailRoles.push({ email: member.email, role: 3, status: 0 });
      } else {
        // console_bk.log('KHONG  ' + member.email);
      }
    });
    if (emailRoles.length === 0) {
      this.notificationService.showWarningTimer('Vui lòng nhập email', 3000);
      return false;
    }
    // console_bk.log('EMAIL   ' + JSON.stringify(emailInvite));
    this.applicationService.getEmailInviteByAppId(emailInvite, this.partnerId)
      .then((integrate) => {
        // console_bk.log('TICH HOP ' + JSON.stringify(integrate));
        // console_bk.log('TICH HOP ' + integrate.code);
        if (integrate) {
          /*_.each(integrate.data, (invited) => {
            emailRoles = emailRoles.filter( (item) => item.email !== invited);
          });*/
          _.each(emailRoles, (erole) => {
            let findV = _.find(integrate, (item) => { return item === erole.email; });
            if (findV) {
              erole.status = 1;
            }
          });
          // console_bk.log('EMAIL 11111  ' + JSON.stringify(emailRoles));
          this.applicationService.insertEmailInviteByAppId(emailRoles, this.partnerId, 'email', this.appDetai.company, this.appId)
            .then((invites) => {
              // console_bk.log('TICH HOP xx ' + JSON.stringify(invites));
              if (invites) {
                // console_bk.log('EMAIL  DONE ' + JSON.stringify(emailInvite));
                this.listMember = [];
                this.createListMember();
                this.listUserInvited();
                // this.notificationService.showSwal('Mời thành công');
                this.notificationService.showSuccessTimer('Mời thành công', 3000);
              }
            });
        }
      });

  }

  private listUserInvited() {
    this.applicationService.getListUserInvited(this.partnerId)
      .then((invites) => {
        if (invites) {
          this.listMemberInvited = invites;
        }
      });
  }
  private inviteDone() {
    // this.router.navigate(['/application-hana/application-list']);
    this.usingApp();
  }

  private usingApp() {

    this.getAppInfo();

    // if (localStorage.getItem('currentUser')) {
    //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //   currentUser.version = 2;
    //   currentUser.partner_id = this.partnerId;
    //   currentUser.app_using = this.appDetai;
    //   currentUser.version = 2;
    //   if (currentUser.role_id !== 1) {
    //     currentUser.role_id = 4;
    //   }
    //   localStorage.setItem('currentUser', JSON.stringify(currentUser));
    // }
    // this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, this.partnerId, 2)
    //   .then((apps) => {
    //     // console_bk.log('TOKEN AAAAAAAAAAAA  ' + JSON.stringify(apps));
    //     this.newTab(this.serviceConfig.consoleHana + '/authen/' + this.partnerId + '/' + apps.token);

    //   });
  }

  // toantm: Lay lai thong tin app & redirect den authen
  private getAppInfo() {
    // console_bk.log('TICH HOP ');
    this.applicationService.getPartnerDetailById(this.partnerId)
      .then((integrate) => {
        if (localStorage.getItem('currentUser')) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          currentUser.version = integrate.version;
          console.log('APPUSEING ' + JSON.stringify(currentUser.app_using));
          currentUser.app_using = integrate;
          console.log('APPUSEING ' + JSON.stringify(currentUser.app_using));
          if (currentUser.role_id !== 1) {
            let user = integrate.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
            if (user) {
              currentUser.role_id = user.role_id;
            }
          }
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          // console.log('APPUSEING ' + JSON.stringify(currentUser));
          // return false;
          /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, currentUser.app_using.id, currentUser.app_using.version)
          .then((apps) => {
            let callbackUrl = this.serviceConfig.HOST_MY_WEB + '/#/chat';
            this.newTab(this.serviceConfig.consoleHana + '/authen/' + currentUser.app_using.id + '/' + apps.token + '/' + btoa(callbackUrl));
          }); */
          this.router.navigate(['/' + currentUser.app_using.id + '/messages']);
        }
      });
  }

  private newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb');
    let form = document.createElement('form');
    form.method = 'GET';
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }

  private nextStep() {
    $('#invite-tab').click();
  }

  private copyToClipboard() {
    this.clipboard.copy(this.script);
  }
}
