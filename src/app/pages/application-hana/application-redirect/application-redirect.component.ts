import {
  Component, OnInit, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy, Directive,
  ViewEncapsulation
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ConfigService } from '../../../_services/config.service';
import { NgZone } from '@angular/core';

declare var $: any;
declare var _: any;

@Component({
  selector: 'application-redirect',
  templateUrl: 'application-redirect.component.html',
  // encapsulation: ViewEncapsulation.None,
  styleUrls: ['application-redirect.component.scss']
})



export class ApplicationRedirectComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  private currentUser: any;
  private listApplication: any;
  private timerObserver: Subscription;

  constructor(private applicationService: ApplicationService, private router: Router, private cdRef: ChangeDetectorRef, private serviceConfig: ConfigService,
    private zone: NgZone, route: ActivatedRoute) {
    console.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB 1 ' + localStorage.getItem('currentUser'));
  }

  public ngOnInit() {
    let current = localStorage.getItem('currentUser');
    if (current && current !== 'undefined') {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      console.log('getListApplication');
      this.getListApplication();
    } else {
      this.router.navigate(['/login-hana']);
    }
  }

  ngOnChanges() {
    console.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB ngOnChanges');
  }

  ngAfterViewInit() {
    console.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB ngAfterViewInit');
    $('.hana-config-name')
      .popup()
      ;
  }
  public ngOnDestroy() {
    console.log('ngOnDestroy application-redirect');
  }

  /*ngOnDestroy() {
    console.log('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB ngOnDestroy');
  }*/

  private getTryUsing() {
    let integrations = this.currentUser.app_using.integrations;
    let fbInte = _.find(integrations, (inte) => {
        return inte.gatewayId === 1;
    });
    if (fbInte) {
        if (fbInte.isExpire === 0) {
          let body = {
            page_id: fbInte.pageId,
            partner_id: this.currentUser.partner_id
          }
          this.applicationService.getTryUsing(body)
          .then((apps) => {

              // console.log('paypay ' + JSON.stringify(apps));
              this.currentUser.payment = apps.data;
              this.currentUser.paymentstatus = false;
              localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          });
        }
    }
  }

  private usingApp(app) {
    // console_bk.log('APP ' + JSON.stringify(app));
    //  window.open('http://test.mideasvn.com:8001/authen/', '_blank');
    // let windows = window;
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      this.currentUser  = currentUser;
      this.getTryUsing();
    }
    /* this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
      .then((apps) => {
        // console_bk.log('TOKEN AAAAAAAAAAAA  ' + JSON.stringify(apps));
        // this.newTab(this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token);
        window.location.href = this.serviceConfig.consoleHana + '/authen/' + app.id + '/' + apps.token;

      }); */
      this.router.navigate(['/redirect-app']);
  }

  private newTab(url) {
    // console_bk.log('OPen new tabbbbbbbbbbbbbbb');
    var form = document.createElement("form");
    form.method = "GET";
    form.action = url;
    document.body.appendChild(form);
    form.submit();
  }

  private getListApplication() {
    this.applicationService.getListPartnerByUsername(this.currentUser.username)
      .then((apps) => {
        if (apps) {
          // console_bk.log('LIST AP pppp' );
          console.log('LIST AP length ' + apps.length);
          if (apps.length === 0) {
            this.router.navigate(['/application-hana/application-create']);
          } else if (apps.length === 1) {
            // console_bk.log('TAO APP su dung app');
            // this.usingApp(apps[0]);
            // toantm: Bo redirect qua CRM
            this.router.navigate(['/application-hana/application-list']);            
          } else {
            // console_bk.log('danh sach app');
            this.router.navigate(['/application-hana/application-list']);
          }
        }
      });
  }
}

