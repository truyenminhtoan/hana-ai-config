import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntitiesComponent } from './entities.component';
import { EntityListComponent } from './entity-list/entity-list.component';
import { EntityNewComponent } from './entity-new/entity-new.component';
import { EntityEditComponent } from "./entity-edit/entity-edit.component";

const routes: Routes = [
  {
    path: '',
    component: EntitiesComponent,
    children: [
      { path: 'entityList', component: EntityListComponent },
      { path: 'entityNew', component: EntityNewComponent },
      { path: 'entityEdit/:id', component: EntityEditComponent },
    ]
  },
];

export const routing = RouterModule.forChild(routes);
