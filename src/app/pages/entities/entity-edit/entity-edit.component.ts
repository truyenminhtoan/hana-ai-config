import { Component, OnInit, ViewChildren, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import { NotificationService } from '../../../_services/notification.service';
import { EntityValuesModel, EntityValueSynonymsModel, EntitieModel } from "../../../_models/entities.model";
declare var $: any;
import * as _ from 'underscore';
import {EntityService} from "../../../_services/entity.service";
import { ActivatedRoute } from '@angular/router';
import {ValidationService} from "../../../_services/ValidationService";
import {FormControl} from "@angular/forms/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'entity-edit',
  templateUrl: 'entity-edit.component.html',
  styleUrls: ['entity-edit.component.css']
})
export class EntityEditComponent  implements OnInit {

  private currentEntity: EntitieModel = new EntitieModel();
  private sum_entity_value_no_synonyms = 1;
  private sum_entity_value_synonyms = 0;
  private type_synonyms = false;
  private sub: any;
  private entityId: any;
  private entityValuesModelEdit: EntityValuesModel[] = [];
  private entityValuesNoSnonymsEdit: EntityValuesModel[] = [];
  public rowsOnPage = 10;
  public sortBy = "index";
  public sortOrder = "asc";
    public activePage1 = 1;
    public activePage2 = 2;
  private list: EntitieModel[] = [];
  private entities_name = [];
  private is_load_list_entiy = true; 
  private is_load_entity_edit = true;
  private is_update = true;
  private currentAgentId = '';
  private entityValuesModelEditDelete: EntityValuesModel[] = [];
  private entityValuesNoSnonymsEditDelete: EntityValuesModel[] = [];
  //View
  @ViewChild('name') input_name: ElementRef;
  @ViewChildren('input') inputs;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private notificationService: NotificationService,
               private entityService: EntityService,
               private validationService: ValidationService) {

      this.currentAgentId = localStorage.getItem('currentAgent_Id');
      if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
          this.intTuChinh();
          this.loadListEntity();
      } else {
          this.notificationService.showNoAgentId();
      }
  }

  public loadListEntity() {
      this.entityService.getListEntityPromise(this.currentAgentId, 1)
          .then((data) => {
              this.list = data;
              if (this.list.length > 0) {
                  for (var i = 0; i< this.list.length; i++) {
                      this.entities_name.push('@'+ this.list[i].name);
                  }
              }
              this.is_load_list_entiy = false;
          })
  }

  public intTuChinh() {
      // Tu chinh
      let entityValue = new EntityValuesModel();
      entityValue.main_word = '';
      entityValue.status = 1;
      this.entityValuesNoSnonymsEdit.push(entityValue);
      this.activePage2 = (this.entityValuesNoSnonymsEdit.length / 10) + 1;

  }
  public ngOnInit() {
    this.loadEdit();
  }

  public loadEdit() {

      this.entityValuesModelEditDelete = [];
      this.entityValuesNoSnonymsEditDelete = [];
      this.currentEntity.entity_values = [];
      this.entityValuesModelEdit = [];
      this.entityValuesNoSnonymsEdit = [];

      this.sub = this.route.params.subscribe((params) => {
          this.entityId = params['id'];
          let actionOperate = this.entityService.getEntityById(this.entityId);
          actionOperate.subscribe(
              (data) => {
                  this.currentEntity = data;
                  if (this.currentEntity.type == 1) {
                      this.type_synonyms = true;
                      this.currentEntity.entity_values.sort(function(a, b) {
                          var nameA = a.main_word.toLowerCase(), nameB = b.main_word.toLowerCase()
                          if (nameA < nameB) {
                              return -1;
                          }
                          if (nameA > nameB) {
                              return 1;
                          }
                          // names must be equal
                          return 0;
                      });
                      for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
                          if (this.currentEntity.entity_values[i].entity_value_synonyms.length > 0) {
                              let entityValue = new EntityValuesModel();
                              entityValue.id = this.currentEntity.entity_values[i].id;
                              entityValue.main_word = this.currentEntity.entity_values[i].main_word;
                              entityValue.status = this.currentEntity.entity_values[i].status;
                              if (this.currentEntity.entity_values[i].created_date != undefined) {
                                  entityValue.created_date = this.currentEntity.entity_values[i].created_date;
                              }
                              entityValue.index = i;
                              var aEntityValueSynonymsModel: EntityValueSynonymsModel[] = [];
                              this.currentEntity.entity_values[i].entity_value_synonyms.sort(function(a, b) {
                                  var nameA = a.word.toLowerCase(), nameB = b.word.toLowerCase()
                                  if (nameA < nameB) {
                                      return -1;
                                  }
                                  if (nameA > nameB) {
                                      return 1;
                                  }
                                  // names must be equal
                                  return 0;
                              });
                              aEntityValueSynonymsModel = this.currentEntity.entity_values[i].entity_value_synonyms;
                              entityValue.entity_value_synonyms = aEntityValueSynonymsModel;
                              this.entityValuesModelEdit.push(entityValue);
                              this.sum_entity_value_synonyms ++ ;
                          } else {
                              if (this.entityValuesNoSnonymsEdit.length > 0 && this.entityValuesNoSnonymsEdit[0].main_word == '') {
                                  this.entityValuesNoSnonymsEdit = [];
                              }
                              let entityValue = new EntityValuesModel();
                              entityValue.main_word = '';
                              entityValue.index = 0;
                              entityValue.status = 1;
                              this.entityValuesNoSnonymsEdit.push(entityValue);
                              this.sum_entity_value_no_synonyms ++;
                          }
                      }

                      this.addEntityValueSynonyms();
                  } else {
                      if (this.entityValuesNoSnonymsEdit.length > 0 && this.entityValuesNoSnonymsEdit[0].main_word == '') {
                          this.entityValuesNoSnonymsEdit = [];
                      }
                      if (this.currentEntity.entity_values.length == 0) {
                          let entityValue = new EntityValuesModel();
                          entityValue.main_word = '';
                          entityValue.status = 1;
                          this.entityValuesNoSnonymsEdit.push(entityValue);
                      }

                      for (var i = 0; i< this.currentEntity.entity_values.length; i++) {
                          let entityValue = new EntityValuesModel();
                          entityValue.main_word = this.currentEntity.entity_values[i].main_word;
                          entityValue.id = this.currentEntity.entity_values[i].id;
                          entityValue.index = i;
                          entityValue.status = this.currentEntity.entity_values[i].status;
                          if (this.currentEntity.entity_values[i].created_date != undefined) {
                              entityValue.created_date = this.currentEntity.entity_values[i].created_date;
                          }
                          this.entityValuesNoSnonymsEdit.push(entityValue);
                      }

                      this.intTuChinh();
                      this.type_synonyms = false;
                  }
                  this.is_load_entity_edit = false;
                  this.entityValuesModelEditDelete = this.entityValuesModelEdit;
                  this.entityValuesNoSnonymsEditDelete = this.entityValuesNoSnonymsEdit;
                  this.currentEntity.entity_values = this.currentEntity.entity_values.filter((row) => {
                      return row.status != -1;
                  });
                  this.entityValuesModelEdit = this.entityValuesModelEdit.filter((row) => {
                      return row.status != -1;
                  });
                  this.entityValuesNoSnonymsEdit = this.entityValuesNoSnonymsEdit.filter((row) => {
                      return row.status != -1;
                  });
              },
              (err) => {
              });
      });
  }

  public btnAddEntityValueNoSynonyms() {
      this.intTuChinh();
  }

  public addEntityValueSynonyms() {
      this.sum_entity_value_synonyms ++;
      let entityValue = new EntityValuesModel();
      entityValue.main_word = '';
      entityValue.index = this.sum_entity_value_synonyms;
      entityValue.status = 1;
      let tmp = new EntityValueSynonymsModel();
      tmp.word = '';
      tmp.status = 1;
      tmp.index = this.sum_entity_value_synonyms;
      var aEntityValueSynonymsModel: EntityValueSynonymsModel[] = [];
      entityValue.entity_value_synonyms = aEntityValueSynonymsModel;
      this.entityValuesModelEdit.push(entityValue);


      this.activePage1 = (this.entityValuesModelEdit.length / 10) + 1;
  }
  public btnRemoveEntityValueNoSynonyms(obj) {
      this.entityValuesNoSnonymsEdit = _.reject(this.entityValuesNoSnonymsEdit,
          (item) => item == obj);

      this.activePage2 = (this.entityValuesNoSnonymsEdit.length / 10) + 1;
  }

  public deleteEntityValuesModelEdit(obj) {
      var entityValuesModelEdit: EntityValuesModel[] = [];
      entityValuesModelEdit = this.entityValuesModelEdit;
      this.entityValuesModelEdit = [];
      entityValuesModelEdit = entityValuesModelEdit.filter((row) => {
          return row !== obj
      });
      var that = this;
      setTimeout(function () {
         that.entityValuesModelEdit = entityValuesModelEdit;
      }, 50);

      this.activePage1 = (this.entityValuesModelEdit.length / 10) + 1;
  }
    
  public changeType() {
        if (this.currentEntity.type == 1) {
            this.currentEntity.type = 2;
            if (this.entityValuesNoSnonymsEdit.length == 0) {
                this.btnAddEntityValueNoSynonyms();
            }
        } else {
            this.currentEntity.type = 1;
            if (this.entityValuesModelEdit.length == 0) {
                this.addEntityValueSynonyms();
            }
        }
        if (this.entityValuesModelEdit != undefined && this.entityValuesModelEdit.length > 0) {
            for (var i = 0; i< this.entityValuesModelEdit.length; i++) {
                if (this.entityValuesModelEdit[i].entity_value_synonyms != undefined) {
                    this.entityValuesModelEdit[i].entity_value_synonyms = this.entityValuesModelEdit[i].entity_value_synonyms.filter((row) => {
                        return row.word !== ''
                    });
                }
            }
        }
  }

  public checkName(name, id) {
      for (var i = 0; i < this.list.length; i++) {
          if (this.list[i].id != id && this.list[i].name == name.trim()) {
              return false;
          }
      }

      return true;
  }

  public saveEntities(frmValue) {
      this.currentEntity.name = frmValue.name;
      if (this.currentEntity.name != undefined && this.currentEntity.name.trim().length > 0) {
          if (!this.validationService.validateAlphabet(this.currentEntity.name)) {
              this.notificationService.showWarning('Tên khái niệm chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_" ');
              this.input_name.nativeElement.focus();
          } else {
              if (this.checkName(this.currentEntity.name, this.currentEntity.id)) {
                  if (this.currentEntity.type == 1) {
                      this.currentEntity.entity_values = this.entityValuesModelEdit;
                  } else {
                      this.currentEntity.entity_values = this.entityValuesNoSnonymsEdit;
                  }

                  // Tu dong tao tu dong nghia, neu chi nhap tu chinh va tu dong nghia de trong
                  if (this.currentEntity.entity_values != undefined) {
                      if (this.currentEntity.type == 1) {
                          for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
                              if (this.currentEntity.entity_values[i].main_word.length == 0 && this.currentEntity.entity_values[i].entity_value_synonyms != undefined && this.currentEntity.entity_values[i].entity_value_synonyms.length >= 1) {
                                  this.notificationService.showWarning("Hãy vui lòng nhập từ chính");
                                  this.inputs.toArray().find((e) => {
                                      if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.length == 0)) {
                                          e.nativeElement.focus();
                                      }
                                  });
                                  return false;
                              } else {
                                  if (this.currentEntity.entity_values[i].entity_value_synonyms == undefined ||  this.currentEntity.entity_values[i].entity_value_synonyms.length == 0) {
                                      this.currentEntity.entity_values[i].entity_value_synonyms = [];
                                      if (this.currentEntity.entity_values[i].main_word.length > 0) {
                                          let tmp = new EntityValueSynonymsModel();
                                          tmp.word = this.currentEntity.entity_values[i].main_word;
                                          tmp.status = 1;
                                          tmp.index = 0;
                                          this.currentEntity.entity_values[i].entity_value_synonyms.push(tmp);
                                      }
                                  }
                              }
                          }
                      }
                  }

                  // kiem tra va dua cac phan tu da xoa vao
                  if (this.currentEntity.type == 1) {
                      var tmpEntityValuesModelEditDelete = this.entityValuesModelEditDelete;
                      for (var i = 0; i <  this.currentEntity.entity_values.length; i++) {
                          if (this.currentEntity.entity_values[i].id != undefined) {
                              tmpEntityValuesModelEditDelete = tmpEntityValuesModelEditDelete.filter((row) => {
                                  return row.id !== this.currentEntity.entity_values[i].id
                              });
                          }
                      }
                      for (var i = 0; i <  tmpEntityValuesModelEditDelete.length; i++) {
                          if (tmpEntityValuesModelEditDelete[i].id != undefined) {
                              tmpEntityValuesModelEditDelete[i].status = -1;
                              for (var j = 0; j <  tmpEntityValuesModelEditDelete[i].entity_value_synonyms.length; j++) {
                                  tmpEntityValuesModelEditDelete[i].entity_value_synonyms[j].status = -1;
                              }
                          }

                      }
                      tmpEntityValuesModelEditDelete = tmpEntityValuesModelEditDelete.filter((row) => {
                          return row.id != undefined
                      });
                      for (var i = 0; i <  tmpEntityValuesModelEditDelete.length; i++) {
                          this.currentEntity.entity_values.push(tmpEntityValuesModelEditDelete[i]);
                      }
                  } else {
                      var tmpEntityValuesNoSnonymsEditDelete = this.entityValuesNoSnonymsEditDelete;
                      for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
                          if (this.currentEntity.entity_values[i].id != undefined) {
                              tmpEntityValuesNoSnonymsEditDelete = tmpEntityValuesNoSnonymsEditDelete.filter((row) => {
                                  return row.id !== this.currentEntity.entity_values[i].id
                              });
                          }
                      }
                      for (var i = 0; i < tmpEntityValuesNoSnonymsEditDelete.length; i++) {
                          if (tmpEntityValuesNoSnonymsEditDelete[i].id != undefined) {
                              tmpEntityValuesNoSnonymsEditDelete[i].status = -1;
                          }
                      }
                      tmpEntityValuesNoSnonymsEditDelete = tmpEntityValuesNoSnonymsEditDelete.filter((row) => {
                          return row.id != undefined
                      });
                      for (var i = 0; i < tmpEntityValuesNoSnonymsEditDelete.length; i++) {
                          this.currentEntity.entity_values.push(tmpEntityValuesNoSnonymsEditDelete[i]);
                      }
                  }

                  this.trimObj();
                  this.is_update = false;
                  this.putUpdateEntity((cb) => {
                      this.currentEntity.entity_values = this.currentEntity.entity_values.filter((row) => {
                          return row.status != -1;
                      });
                      this.entityValuesModelEdit = this.entityValuesModelEdit.filter((row) => {
                          return row.status != -1;
                      });
                      this.entityValuesNoSnonymsEdit = this.entityValuesNoSnonymsEdit.filter((row) => {
                          return row.status != -1;
                      });
                      if (cb) {
                          this.notificationService.showSuccess('Cập nhật khái niệm "' +this.currentEntity.name+ '" thành công');

                          if (this.currentEntity.type == 1) {
                              this.entityValuesNoSnonymsEdit = [];
                          } else {
                              this.entityValuesModelEdit = [];
                          }
                          let that = this;
                          that.is_update = true;
                          that.loadEdit();
                      } else {
                          let that = this;
                          that.loadEdit();
                          this.notificationService.showDanger('Cập nhật khái niệm "' +this.currentEntity.name+ '" thất bại');
                      }
                  });
              } else {
                  this.input_name.nativeElement.focus();
                  this.notificationService.showWarning('Tên khái niệm đã tồn tại');
              }
          }
      } else {
          this.input_name.nativeElement.focus();
          this.notificationService.showWarning('Tên khái niệm không được rỗng ');
      }
  }

    public autoEntityValueSynonyms(item) {
        for (var i = 0; i < this.entityValuesModelEdit.length; i++) {
            if (this.entityValuesModelEdit[i] == item) {
                if (this.entityValuesModelEdit[i].entity_value_synonyms == undefined) {
                    this.entityValuesModelEdit[i].entity_value_synonyms = [];
                    let tmp = new EntityValueSynonymsModel();
                    tmp.word = item.main_word;
                    tmp.status = 1;
                    tmp.index = 0;
                    this.entityValuesModelEdit[i].entity_value_synonyms.push(tmp);
                } else {
                    if (this.entityValuesModelEdit[i].entity_value_synonyms.length == 0) {
                        this.entityValuesModelEdit[i].entity_value_synonyms = [];
                        let tmp = new EntityValueSynonymsModel();
                        tmp.word = item.main_word;
                        tmp.status = 1;
                        tmp.index = 0;
                        this.entityValuesModelEdit[i].entity_value_synonyms.push(tmp);
                    }
                }
            }
        }
    }

  public trimObj() {
      this.currentEntity.entity_values = this.currentEntity.entity_values.filter((entity) => {
          return entity.main_word.trim().length > 0
      });
      for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
          if (this.currentEntity.entity_values[i].entity_value_synonyms != undefined) {
              this.currentEntity.entity_values[i].entity_value_synonyms = this.currentEntity.entity_values[i].entity_value_synonyms.filter((row) => {
                  return row.word.trim().length > 0
              });
          }
      }
      for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
          this.currentEntity.entity_values[i].main_word = this.currentEntity.entity_values[i].main_word.trim();
          if (this.currentEntity.entity_values[i].entity_value_synonyms != undefined) {
              for (var j = 0; j < this.currentEntity.entity_values[i].entity_value_synonyms.length; j++) {
                  this.currentEntity.entity_values[i].entity_value_synonyms[j].word = this.currentEntity.entity_values[i].entity_value_synonyms[j].word.trim();
              }
          }
      }
  }
  public putUpdateEntity(callback) {

        let actionOperate: Observable<EntitieModel>;
        actionOperate = this.entityService.update(this.currentEntity);
        actionOperate.subscribe(
            (data) => {
                callback(true);
            },
            (err) => {
                // load event fail
                callback(false);
            });
  }

    // Validate tu dong nghia
    public validateTaginput(control: FormControl) {
        // if (!StringFormat.checkRegexp(control.value, /^[a-zA-Z0-9_ ]+$/)) {
        //     return {
        //         'mess_err@': true
        //     };
        // }
        if (control.value.trim().length > 50) {
            return {
                'mess_err_lenght@': true
            };
        }
        return null;
    }
    public validators = [this.validateTaginput];
    public errorMessages = {
        'mess_err@': 'Từ đồng nghĩa chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_" ',
        'mess_err_lenght@': 'Từ đồng nghĩa cho phép nhập tối đa 50 kí tự'
    };
}
