// Angular Imports
import { NgModule } from '@angular/core';
import { NgaModule } from '../../themes/nga.module';
// This Module's Components
import { EntitiesComponent } from './entities.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EntityListComponent } from './entity-list/entity-list.component';
import { EntityNewComponent } from './entity-new/entity-new.component';
import { routing } from './entities.routing';
import { NotificationService } from "../../_services/notification.service";
import { FilterEntitiesDataPipe } from "./filter-data-pipe/filter-data.pipe";
import { EntityEditComponent } from "./entity-edit/entity-edit.component";
import { TagInputModule } from 'ng2-tag-input';
import { DataTableModule } from 'angular2-datatable/index';
import { ValidationService } from "../../_services/ValidationService";
import { MdAutocompleteModule } from '@angular/material';
import { EntityValueSynonymsAutoComponent } from "./components/entity-value-synonyms-autocomplete/entity-value-synonyms-autocomplete";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgaModule,
        routing,
        TagInputModule,
        DataTableModule,
        MdAutocompleteModule,
        FormsModule, ReactiveFormsModule
    ],
    declarations: [
        EntitiesComponent,
        EntityListComponent,
        EntityNewComponent,
        FilterEntitiesDataPipe,
        EntityEditComponent,
        EntityValueSynonymsAutoComponent,
    ],
    providers: [
        NotificationService,
        ValidationService,
    ],
    exports: [
        EntitiesComponent,
        EntityListComponent,
        EntityNewComponent,
        FilterEntitiesDataPipe,
        EntityEditComponent,
        EntityValueSynonymsAutoComponent,
    ]
})
export class EntitiesModule {

}
