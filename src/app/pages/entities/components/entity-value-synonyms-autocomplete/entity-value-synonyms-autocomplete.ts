import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {StringFormat} from "../../../../themes/validators/string.format";
@Component({
    selector: 'entity-value-synonyms-auto',
    templateUrl: 'entity-value-synonyms-autocomplete.html',
    styleUrls: ['entity-value-synonyms-autocomplete.scss']
})
export class EntityValueSynonymsAutoComponent {

    @Input() query_default: any;
    @Input() entity_value_synonyms: any;
    @Output() value_select = new EventEmitter<any>();

    @ViewChild('input_auto') input_auto: ElementRef;
    stateCtrl: FormControl;
    filteredStates: any;
    public tmp_string = '';
    
    constructor() {
        if (this.query_default != undefined) {
            this.query_default = '';
        }
        this.stateCtrl = new FormControl();
    }
    
    check() {;
        this.filteredStates = this.filterStates(this.query_default);
    }
    
    selectValue (val) {
        
    }

    filterStates(val: string) {

        this.entity_value_synonyms.sort(function(a, b) {
            var nameA = a.replace('@', '').toLowerCase(), nameB = b.replace('@', '').toLowerCase()
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });


        //console.log('val', val);
        if (val == '' || val == null) {
            //reset value old when value input = null
            this.tmp_string = '';
            return [];
        } else {
            var val_select;
            if (val != null && val.split(' ').length > 1) {
                var tmp = val.split(' ');
                if (tmp[tmp.length-1].indexOf('@') != -1 &&  tmp[tmp.length-1].indexOf(':') == -1) {
                    this.tmp_string = val;
                    var value__next_filter = tmp[tmp.length-1];
                  //  console.log('xxxxxxxx', value__next_filter);

                    val_select = this.entity_value_synonyms.filter(item => {
                        return (StringFormat.formatText(item).indexOf(value__next_filter.replace('@', '').trim().toLowerCase()) !== -1)
                    });

                }
            } else {
                if (val.indexOf('@') != -1) {
                   // console.log(val.replace('@', ''));

                    var test = this.entity_value_synonyms.filter(item => {
                        return (StringFormat.formatText(item).indexOf(StringFormat.formatText(val.trim())) !== -1)
                    });
                    //console.log('this.test', JSON.stringify(test));

                    //console.log('this.entity_value_synonyms', JSON.stringify(this.entity_value_synonyms));

                    // console.log('xxxxxxxx', val);
                    //
                    // var xxx = this.entity_value_synonyms.filter(item => {
                    //     return (item.indexOf(val.replace('@', '').trim()) !== -1 || StringFormat.formatText(item).indexOf(StringFormat.formatText(val.replace('@', '').trim())) !== -1)
                    // });
                    // console.log('xxx', xxx);

                    val_select =  val ? this.entity_value_synonyms.filter(item => {
                        return (item.indexOf(val.replace('@', '').trim()) !== -1 || StringFormat.formatText(item).indexOf(StringFormat.formatText(val.replace('@', '').trim())) !== -1)
                    }) : this.entity_value_synonyms;
                    if (val) {
                        this.value_select.emit(this.query_default);
                    }
                }
            }
            //console.log('query_default', this.query_default);
            this.value_select.emit(this.query_default);
            return val_select;
        }

    }

    enterSelect($event, val) {
        let that = this;
        (function(){
            setTimeout(() => {
                var value_curent = that.tmp_string + val;
                //console.log('value_curent', value_curent);
                var tmp_value_curent = '';
                //trim value not struct
                if (value_curent != null) {
                    var tmp = value_curent.split(' ');
                    for (var i = 0; i < tmp.length - 1; i++) {
                        tmp_value_curent += tmp[i] + ' ';
                    }
                }
                tmp_value_curent += val;
                //console.log('tmp_value_curent', tmp_value_curent);
                that.query_default = tmp_value_curent.replace('@@', '@').trim()+':';
                that.input_auto.nativeElement.focus();
                that.filteredStates = [];
                //console.log('query_default###', that.query_default);
                that.value_select.emit(that.query_default);

            }, 200);
        })();
    }

}