// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { EntityListComponent } from './entity-list.component';

@NgModule({
    imports: [

    ],
    declarations: [
        EntityListComponent,
    ],
    exports: [
        EntityListComponent,
    ]
})
export class EntityListModule {

}
