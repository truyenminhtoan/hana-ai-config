import {Component, OnInit} from '@angular/core';
import { NotificationService } from '../../../_services/notification.service';
import { EntitieModel } from '../../../_models/entities.model';
import { EntityService } from '../../../_services/entity.service';
import {ComponentInteractionService} from "../../../_services/compoent-interaction-service";
import {Subscription} from 'rxjs/Rx';
declare var $: any;
declare var swal: any;

@Component({
    selector: 'entity-list',
    templateUrl: 'entity-list.component.html',
    styleUrls: ['entity-list.component.scss']
})
export class EntityListComponent implements OnInit {
    
    private filterQuery = "";
    private rowsOnPage = 5;
    private sortBy = "name";
    private sortOrder = "asc";
    private id_loading = true;
    private list: EntitieModel[] = [];
    private eventSubscription: Subscription;

    constructor(
        private entityService: EntityService,
        private notificationService: NotificationService,
        private _componentService: ComponentInteractionService) {
    }

    ngOnInit() {
        this.loadListEntity();
        this.eventSubscription = this._componentService.eventReceiver$.subscribe(
            event => {
                if (event == 'load-entities') {
                    this.loadListEntity();
                }
            }
        );
    }

    public loadListEntity() {
        let agentId = localStorage.getItem('currentAgent_Id');
        if (agentId) {
            this.entityService.getListEntityPromise(agentId, false)
                .then((data) => {
                    this.list = data;
                    this.id_loading = false;
                })
        } else {
            this.notificationService.showNoAgentId();
        }
        setTimeout(function () {
        }, 1000);
    }

    public deleteEntity(id) {
        swal({
            title: 'Bạn chắc chắn muốn xóa?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(() => {
            this.entityService.deleteEntity(id)
                .then((entity) => {
                    this._componentService.eventPublisher$.next('load-entities');
                    this.notificationService.showSuccess('Xóa khái niệm thành công');
                });
            $('body').removeClass("modal-open");
        }, (dismiss) => {
            console.log(dismiss);
        });
    }

}
