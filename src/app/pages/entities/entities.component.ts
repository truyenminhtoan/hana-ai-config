import { Component } from '@angular/core';

@Component({
    selector: 'entities',
    template: '<router-outlet></router-outlet>'
})
export class EntitiesComponent {

}
