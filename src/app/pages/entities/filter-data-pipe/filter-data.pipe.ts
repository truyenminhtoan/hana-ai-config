import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/Rx';
import { StringFormat } from "../../../themes/validators/string.format";

@Pipe({
  name: 'filterIntentPipe',
  pure: false
})
export class FilterEntitiesDataPipe implements PipeTransform {
  public transform(data: any[], searchTerm: string): any[] {
    if (searchTerm != null && searchTerm.trim().length > 0) {
      if (StringFormat.checkRegexp(searchTerm.trim(), /^[a-zA-Z0-9_ ]+$/)) {
        searchTerm = StringFormat.formatText(searchTerm.trim());
        return data.filter(item => {
          return (StringFormat.formatText(item.name).indexOf(StringFormat.formatText(searchTerm)) !== -1)
        });
      } else {
        return [];
      }
    } else {
      return data;
    }
  }
}
