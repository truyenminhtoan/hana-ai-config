import {Component, OnInit, OnChanges, AfterViewInit, ViewChild, ViewChildren, ElementRef} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import { NotificationService } from '../../../_services/notification.service';
import { EntityValuesModel, EntityValueSynonymsModel, EntitieModel } from '../../../_models/entities.model';
declare var $: any;
import { EntityService } from '../../../_services/entity.service';
import { Router, ActivatedRoute } from '@angular/router';
import {ValidationService} from "../../../_services/ValidationService";
import { FormControl } from '@angular/forms';

@Component({
    selector: 'entity-new',
    templateUrl: 'entity-new.component.html',
    styleUrls: ['entity-new.component.scss'],
})
export class EntityNewComponent  implements OnInit, OnChanges, AfterViewInit {
    
    private currentEntity = new EntitieModel();
    private sum_entity_value_no_synonyms = 0;
    private sum_entity_value_synonyms = 0;
    private entityValuesModelEdit: EntityValuesModel[] = [];
    private entityValuesNoSnonymsEdit: EntityValuesModel[] = [];
    private list: EntitieModel[] = [];
    private rowsOnPage = 10;
    public activePage1 = 1;
    public activePage2 = 2;
    private sortBy = "index";
    private sortOrder = "asc";
    private entities_name = [];
    private is_load_list_entiy = true;
    private currentAgentId = '';
    //View
    @ViewChild('name') input_name: ElementRef;
    @ViewChildren('input') inputs;

    private value_tag_tmp = '';
    private index_tag_tmp = -1;
    
    constructor( 
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private entityService: EntityService,
        private validationService: ValidationService)
    {
        this.currentAgentId = localStorage.getItem('currentAgent_Id');
        if (this.currentAgentId != undefined && this.currentAgentId.length > 0) {
            this.currentEntity.type = 1;
            this.currentEntity.name = '';
            this.buildEntityValueNoSynonyms();
            this.initEntityValueSynonyms();
            this.loadListEntity();
        } else {
            this.notificationService.showWarning("Không nhận dạng được Agent Id");
            this.router.navigate(['/pages/agents/agentList']);
        }
    }

    public loadListEntity() {
        this.entityService.getListEntityPromise(this.currentAgentId, 1)
            .then((data) => {
                this.list = data;
                if (this.list.length > 0) {
                    for (var i = 0; i< this.list.length; i++) {
                        this.entities_name.push('@'+ this.list[i].name);
                    }
                }
                this.is_load_list_entiy = false;
            })
    }

    public ngOnInit() {
    }

    public initEntityValueSynonyms() {
        let entityValue = new EntityValuesModel();
        entityValue.main_word = '';
        entityValue.status = 1;
        entityValue.index = 0;
        this.entityValuesModelEdit.push(entityValue);
        this.sum_entity_value_synonyms ++ ;
    }

    public buildEntityValueNoSynonyms() {
        let entityValue = new EntityValuesModel();
        entityValue.main_word = '';
        entityValue.status = 1;
        entityValue.index = this.sum_entity_value_no_synonyms;
        this.entityValuesNoSnonymsEdit.push(entityValue);
        this.sum_entity_value_no_synonyms ++ ;

        this.activePage2 = (this.entityValuesNoSnonymsEdit.length / 10) + 1;
    }
    
    public btnAddEntityValueNoSynonyms() {
        this.buildEntityValueNoSynonyms();
    }
    
    public addEntityValueSynonyms() {
        let entityValue = new EntityValuesModel();
        entityValue.main_word = '';
        entityValue.index = this.sum_entity_value_synonyms;
        entityValue.status = 1;
        let tmp = new EntityValueSynonymsModel();
        tmp.word = '';
        tmp.status = 1;
        tmp.index = this.sum_entity_value_synonyms;
        var aEntityValueSynonymsModel: EntityValueSynonymsModel[] = [];
        entityValue.entity_value_synonyms = aEntityValueSynonymsModel;
        this.entityValuesModelEdit.push(entityValue);
        this.sum_entity_value_synonyms ++;

        this.activePage1 = (this.entityValuesModelEdit.length / 10) + 1;
    }

    public autoEntityValueSynonyms(item) {
        for (var i = 0; i < this.entityValuesModelEdit.length; i++) {
            if (this.entityValuesModelEdit[i] == item) {
                if (this.entityValuesModelEdit[i].entity_value_synonyms == undefined) {
                    this.entityValuesModelEdit[i].entity_value_synonyms = [];
                    let tmp = new EntityValueSynonymsModel();
                    tmp.word = item.main_word;
                    tmp.status = 1;
                    tmp.index = 0;
                    this.entityValuesModelEdit[i].entity_value_synonyms.push(tmp);
                } else {
                    if (this.entityValuesModelEdit[i].entity_value_synonyms.length == 0) {
                        this.entityValuesModelEdit[i].entity_value_synonyms = [];
                        let tmp = new EntityValueSynonymsModel();
                        tmp.word = item.main_word;
                        tmp.status = 1;
                        tmp.index = 0;
                        this.entityValuesModelEdit[i].entity_value_synonyms.push(tmp);
                    }
                }
            }
        }
    }

    public deleteSynName(item) {
        this.entityValuesModelEdit = this.entityValuesModelEdit.filter((row) => {
            return row !== item
        });

        this.activePage1 = (this.entityValuesModelEdit.length / 10) + 1;
    }

    // Tu chinh
    public deleteNoSynName(item) {
        this.entityValuesNoSnonymsEdit = this.entityValuesNoSnonymsEdit.filter((row) => {
            return row !== item
        });
    }
    
    public changeType() {
        if (this.currentEntity.type == 1) {
            this.currentEntity.type = 2;
        } else {
            this.currentEntity.type = 1;
        }
        if (this.entityValuesModelEdit != undefined && this.entityValuesModelEdit.length > 0) {
            for (var i = 0; i< this.entityValuesModelEdit.length; i++) {
                if (this.entityValuesModelEdit[i].entity_value_synonyms != undefined) {
                    this.entityValuesModelEdit[i].entity_value_synonyms = this.entityValuesModelEdit[i].entity_value_synonyms.filter((row) => {
                        return row.word !== ''
                    });
                }
            }
        }
    }

    public checkExistsName(name) {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].name != undefined) {
                if (this.list[i].name == name.trim()) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public saveEntity() {
        if (this.currentEntity.name.trim().length == 0) {
            this.notificationService.showWarning('Tên khái niệm không được rỗng ');
            this.input_name.nativeElement.focus();
        } else {
            if (this.validationService.validateAlphabet(this.currentEntity.name)) {
                if (this.checkExistsName(this.currentEntity.name)) {
                    this.currentEntity.is_system = 0;
                    this.currentEntity.status = 1;
                    this.currentEntity.agent_id = localStorage.getItem('currentAgent_Id');
                    if (this.currentEntity.type == 1) {
                        this.currentEntity.entity_values = this.entityValuesModelEdit;
                    } else {
                        this.currentEntity.entity_values = this.entityValuesNoSnonymsEdit;
                    }
                    // Tu dong tao tu dong nghia, neu chi nhap tu chinh va tu dong nghia de trong
                    if (this.currentEntity.entity_values != undefined) {
                        for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
                            if (this.currentEntity.type == 1) {
                                if (this.currentEntity.entity_values[i].main_word.length == 0 && this.currentEntity.entity_values[i].entity_value_synonyms != undefined && this.currentEntity.entity_values[i].entity_value_synonyms.length >= 1) {
                                    this.notificationService.showWarning("Hãy vui lòng nhập từ chính");
                                    this.inputs.toArray().find((e) => {
                                        if (e.nativeElement.value == null || (e.nativeElement.value != null && e.nativeElement.value.length == 0)) {
                                            e.nativeElement.focus();
                                        }
                                    });
                                    return false;
                                } else {
                                    if (this.currentEntity.entity_values[i].entity_value_synonyms == undefined ||  this.currentEntity.entity_values[i].entity_value_synonyms.length == 0) {
                                        this.currentEntity.entity_values[i].entity_value_synonyms = [];
                                        if ( this.currentEntity.entity_values[i].main_word.length > 0) {
                                            let tmp = new EntityValueSynonymsModel();
                                            tmp.word = this.currentEntity.entity_values[i].main_word;
                                            tmp.status = 1;
                                            tmp.index = 0;
                                            this.currentEntity.entity_values[i].entity_value_synonyms.push(tmp);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    this.pushNewEntity((cb) => {
                        if (cb) {
                        } else {
                            this.notificationService.showDanger('Thêm khái niệm thất bại');
                        }
                    });
                } else {
                    this.input_name.nativeElement.focus();
                    this.notificationService.showWarning("Tên khái niệm đã tồn tại");
                }

            } else {
                this.input_name.nativeElement.focus();
                this.notificationService.showWarning('Tên khái niệm chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_" ');
            }
        }
    }

    public trimObj() {
        this.currentEntity.entity_values = this.currentEntity.entity_values.filter((entity) => {
            return entity.main_word.trim().length > 0
        });
        for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
            if (this.currentEntity.entity_values[i].entity_value_synonyms != undefined) {
                this.currentEntity.entity_values[i].entity_value_synonyms = this.currentEntity.entity_values[i].entity_value_synonyms.filter((row) => {
                    return row.word.trim().length > 0
                });
            }
        }
        for (var i = 0; i < this.currentEntity.entity_values.length; i++) {
            this.currentEntity.entity_values[i].main_word = this.currentEntity.entity_values[i].main_word.trim();
            if (this.currentEntity.entity_values[i].entity_value_synonyms != undefined) {
                for (var j = 0; j < this.currentEntity.entity_values[i].entity_value_synonyms.length; j++) {
                    this.currentEntity.entity_values[i].entity_value_synonyms[j].word = this.currentEntity.entity_values[i].entity_value_synonyms[j].word.trim();
                }
            }
        }
    }

    public pushNewEntity(callback) {

        this.is_load_list_entiy = true;

        this.trimObj();
        let actionOperate: Observable<EntitieModel>;
        actionOperate = this.entityService.addEntity(this.currentEntity);
        actionOperate.subscribe(
            (data) => {
                this.is_load_list_entiy = false;
                this.notificationService.showSuccess('Thêm khái niệm thành công');
                this.router.navigate(['/pages/entities/entityEdit/' + data.id], { relativeTo: this.route });
                callback(true);
            },
            (err) => {
                this.is_load_list_entiy = false;
                callback(false);
            });
    }

    public ngAfterViewInit() {
    }

    public ngOnChanges(changes: any) {
    }

    // Validate tu dong nghia
    public validateTaginput(control: FormControl) {
        // if (!StringFormat.checkRegexp(control.value, /^[a-zA-Z0-9_ ]+$/)) {
        //     return {
        //         'mess_err@': true
        //     };
        // }
        if (control.value.trim().length > 50) {
            return {
                'mess_err_lenght@': true
            };
        }
        return null;
    }
    public validators = [this.validateTaginput];
    public errorMessages = {
        'mess_err@': 'Từ đồng nghĩa chỉ cho phép nhập [a-z], [A-Z], [0-9] và ký tự "_" ',
        'mess_err_lenght@': 'Từ đồng nghĩa cho phép nhập tối đa 50 kí tự'
    };

    processKeyUp(e, el) {
        if(e.keyCode == 65) { // press A
            el.focus();
        }
    }

    private onTextChange($event, index) {
        this.value_tag_tmp = $event;
        this.index_tag_tmp = index;
    }

}
