// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { EntityNewComponent } from './entity-new.component';

@NgModule({
    imports: [

    ],
    declarations: [
        EntityNewComponent,
    ],
    exports: [
        EntityNewComponent,
    ]
})
export class EntityNewModule {

}
