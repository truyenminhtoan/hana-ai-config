import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from '../_guards/auth.guard';
import { AgentListComponent } from "./agents/agent-list/agent-list.component";

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },
  {
    path: 'application-hana',
    loadChildren: 'app/pages/application-hana/application-hana.module#ApplicationHanaModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'crm-customer',
    loadChildren: 'app/pages/crm-customer/crm-customer.module#CrmCustomerModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'customer-search',
    loadChildren: 'app/pages/customer-search/customer-search.module#CustomerSearchModule',
    canActivate: [AuthGuard]
  },
  {
     path: 'demand-search',
     loadChildren: 'app/pages/task-search/customer-search.module#TaskSearchModule',
     canActivate: [AuthGuard]
   },
  {
    path: 'hana-config',
    loadChildren: 'app/pages/hana-config/hana-config.module#HanaConfigModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'remarketingv2',
    loadChildren: 'app/pages/remarketing-v2/remarketing-v2.module#RemarketingV2Module',
    canActivate: [AuthGuard]
  },
  {
    path: 'training-hana',
    loadChildren: 'app/pages/training-hana/training-hana.module#TrainingHanaModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'customer-hana',
    loadChildren: 'app/pages/customer-hana/customer-hana.module#CustomerHanaModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'backend-users',
    loadChildren: 'app/pages/backend-users/backend-users.module#BackendUsersModule',
  },
  {
    path: 'guide-advisory',
    loadChildren: 'app/pages/guide-advisory/guide-advisory.module#GuideAdvisoryModule'
  },
  {
    path: 'guide-hana',
    loadChildren: 'app/pages/guide-hana/guide-hana.module#GuideHanaModule'
  },
  {
    path: 'quick-reply-template',
    loadChildren: 'app/pages/quick-template/quick-template.module#QuickTemplateModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'login-hana',
    loadChildren: 'app/pages/login-hana/login.module#LoginModule'
  },
  {
    path: 'logout-hana',
    loadChildren: 'app/pages/logout-hana/logout-hana.module#LogoutHanaModule'
  },
  {
    path: 'register-hana',
    loadChildren: 'app/pages/register-hana/register-hana.module#RegisterHanaModule'
  },
  {
    path: 'register',
    loadChildren: 'app/pages/register/register.module#RegisterModule'
  },
  // {
  //   path: 'lock',
  //   loadChildren: 'app/pages/lock/lock.module#LockModule'
  // },
  // {
  //   path: 'chat',
  //   loadChildren: 'app/pages/chatbox/chatbox.module#ChatboxModule'
  // },
  // {
  //   path: 'chat-v2',
  //   loadChildren: 'app/pages/chat/chat.module#ChatModule',
  //   canActivate: [AuthGuard]
  // },
  {
    path: 'redirect-app/:id',
    loadChildren: 'app/pages/redirect-app/redirect-app.module#RedirectAppModule',
    canActivate: [AuthGuard]
  },
  {
    path: ':id/messages',
    loadChildren: 'app/pages/chat-v2/chat-v2.module#ChatV2Module',
    canActivate: [AuthGuard]
  },
  {
    path: 'pages',
    component: PagesComponent,
    children: [
      { path: '', redirectTo: '/login-hana', pathMatch: 'full' },
      { path: 'logins', loadChildren: 'app/pages/login/login.module#LoginModule' },
      { path: 'actions', loadChildren: 'app/pages/actions/actions.module#ActionsModule' },
      { path: 'entities', loadChildren: 'app/pages/entities/entities.module#EntitiesModule' },
      { path: 'intents', loadChildren: 'app/pages/intents/intents.module#IntentsModule' },
      { path: 'agents', loadChildren: 'app/pages/agents/agent.module#AgentModule' },
      { path: 'entities', loadChildren: 'app/pages/entities/entities.module#EntitiesModule' },
      { path: 'products', loadChildren: 'app/pages/products/products.module#ProductsModule' },
    ],
    canActivate: [AuthGuard]
  },
  {
        path: 'customers',
        loadChildren: 'app/pages/remarketing-v2/remarketing-v2.module#RemarketingV2Module',
        canActivate: [AuthGuard]
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
