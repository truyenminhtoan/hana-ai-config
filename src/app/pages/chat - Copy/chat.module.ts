// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { ChatComponent } from './chat.component';
import { routing } from './chat.routing';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgaModule } from '../../themes/nga.module';
import { ApplicationService } from '../../_services/application.service';
import { ColorPickerModule, ColorPickerService } from 'angular2-color-picker';
import { ChatPopupTagsComponent } from './chat-popup-tags/chat-popup-tags.component';
import { ValidationService } from '../../_services/ValidationService';
import { ConfigService } from '../../_services/config.service';
import { NotificationService } from '../../_services/notification.service';

@NgModule({
    imports: [
        CommonModule,
        AngularFormsModule,
        ColorPickerModule,
        NgaModule,
        routing
    ],
    declarations: [
        ChatComponent,
        ChatPopupTagsComponent
    ],
    exports: [
        ChatComponent,
    ],
    providers: [
      ApplicationService,
      ValidationService,
      ColorPickerService,
      ConfigService,
      NotificationService
    ],
})
export class ChatModule {

}
