$(function () {
  $(document).ready(function () {
    $('[rel="tooltip"]').tooltip();
    // load backbone
    require(['converse'], function (converse) {
      converse.initialize({
        auto_reconnect: true,
        bosh_service_url: 'https://test3.mideasvn.com:8280/http-bind', // Please use this connection manager only for testing purposes
        keepalive: true,
        sky_ai_name: 'Trợ lý ảo Hana',
        fb_name: 'Facebook User',
        message_carbons: true,
        message_archiving: 'always',
        roster_groups: true,
        show_controlbox_by_default: false,
        strict_plugin_dependencies: false,
        chatstate_notification_blacklist: ['mulles@movim.eu'],
        xhr_user_search: false,
        //            storage: 'session',
        debug: true,
        sky_host: '@10min',
        play_sounds: false,
        show_notify: false,
        sky_room: '@muc.10min',
        hana_agent_id: 'hanaagenthana_378',
        hana_queue_name: '10m_hana_input_chung',
        sky_hanaprefix: 'hanaagenthana_',
        sky_apiserver: 'https://test3.mideasvn.com:8200/ReengBackendBiz/',
        hana_api: 'https://test3.mideasvn.com:9403/',
        hana_api_user: 'http://test.mideasvn.com:8403/',
        hana_fb_api: 'https://test3.mideasvn.com:8105/',
        hana_web_gateway: 'https://test.mideasvn.com:9201/',
        facebook_api: 'http://test3.mideasvn.com:8110/services/facebook',
        file_path: 'https://test3.mideasvn.com:8000/10mindev/file',
        api_web_gate: 'https://test.mideasvn.com:9001',
        partner_minhthu: '2d70832e-4d1c-4399-8bd7-395a4ef81216',

        //            sky_apiserver: 'http://test3.mideasvn.com:9999/api/',
        jid: '10min',

        sky_agent: '21788885-c717-41b9-8fa2-e01a2d05c505',
        sky_partner: '7ea6f744-064a-4ee1-b9b7-ea1ae2957416',
        mideas_partner_id: '481',
        sky_pass: '5483cd1a-5f7a-4e93-be14-de20919ee61c',
        // sky_agent: '10min_nhungnt1206',
        // sky_partner: '2d70832e-4d1c-4399-8bd7-395a4ef81216',
        // sky_pass: '4b6c1849a4a43980260cbf69ff1c5994d46757d7',
        //                 sky_agent: '10min_ntquan9110',
        //                 sky_partner: '90abbf9d-84af-4786-85fc-c7b17c456e84',
        //                 sky_pass: '8826d5837ca43b9700f8fc9d77c898d0e1d633ef',
        // sky_agent: '10min_toan.truyen2.gmail.com',
        // sky_partner: '8204c6e9-b5b2-471e-be16-999143a336e7',
        // mideas_partner_id: '114',
        // sky_pass: '5937cc2c3497cdc662fe1699b93e405a520761d4',
        sky_myname: 'Tôi',
        sky_resource: 'webclient',
        is_connecting: false
        //            authentication: 'anonymous' // Available values are "login", "prebind", "anonymous".
      });
    });

    // init
    $('.chat-left-inner').css({
      'height': (($(window).height())) + 'px'
    });

    // CHAT MESSAGE
    $('.chat-main-box').css({
      'height': (($(window).height())) + 'px'
    });

    $('.info-right-aside').css({
      'height': (($(window).height())) + 'px',
      'margin-top': '-19px'
    });

    $(window).resize(function () {
      // CONTACT
      $('.chat-left-inner').css({
        'height': (($(window).height())) + 'px'
      });
      $('.chatonline').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      $('.chat-left-inner .slimScrollDiv').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      $('.chat-left-inner .slimScrollBar').css({
        'height': (($(window).height()) - 50) + 'px'
      });
      // CHATBOX
      $('.chat-main-box').css({
        'height': (($(window).height())) + 'px'
      });

      $('.chat-box .slimScrollDiv').css({
        'height': (($(window).height()) - 125) + 'px'
      });

      $('.chat-box .slimScrollBar').css({
        'height': (($(window).height()) - 125) + 'px'
      });

      $('.chatonline').slimScroll({
        height: (($(window).height()) - 70) + 'px',
        start: 'top',
        allowPageScroll: false,
        disableFadeOut: false
      });
    });
  });
});
