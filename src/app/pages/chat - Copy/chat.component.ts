import { Component, AfterViewInit, OnInit, ElementRef, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';
import { ConfigService } from '../../_services/config.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../_services/application.service';
import { NotificationService } from '../../_services/notification.service';
declare var $: any;
declare var _: any;
declare var require: any;
declare var window: any;
@Component({
  selector: 'chat',
  templateUrl: 'chat.component.html',
  styleUrls: ['./assets/css/material-dashboard.css', 'chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit, OnDestroy {
  // tslint:disable-next-line:max-line-length
  private currentUser: any;
  private partnerId: any;
  private listTags = [];
  private subscription: any;
  constructor(private ngZone: NgZone, private elementRef: ElementRef, private applicationService: ApplicationService,
    private router: Router, private notificationService: NotificationService, private route: ActivatedRoute, private serviceConfig: ConfigService, private cdRef: ChangeDetectorRef) {
      this.router.navigate(['/chat-v2']);
    /* $.getScript('../../../assets/js/chat/js/converse-new-temp.js', () => {
      if (this.currentUser.app_using) {
        let s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = this.serviceConfig.HOST_MY + '/chat_backend/config?i=' + this.currentUser.id
          + '&p=' + this.currentUser.app_using.id + '&t=' + this.currentUser.token + '&a=' + this.currentUser.app_using.applications[0].app_id;
        this.elementRef.nativeElement.appendChild(s);
        $.getScript('../../../assets/js/chat/js/runable.js');
        $('body').addClass('sidebar-mini');
      } else {
        localStorage.removeItem('currentUser');
        window.location.href = '/';
      }
    }); */
    // this.checkLoged();
    /* if (this.currentUser.app_using) {
      let s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = this.serviceConfig.HOST_MY + '/chat_backend/config?i=' + this.currentUser.id
        + '&p=' + this.currentUser.app_using.id + '&t=' + this.currentUser.token + '&a=' + this.currentUser.app_using.applications[0].app_id;
      this.elementRef.nativeElement.appendChild(s);
      $.getScript('../../../assets/js/chat/js/runable.js');
      $('body').addClass('sidebar-mini');
    } else {
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    } */
  }
  public ngAfterViewInit() {
    $(".dropdown-toggle").dropdown();
    $('[rel="tooltip"]').tooltip();
    // // console_bk.log('AAPP CONTROLER ' + window.AppController);
    if (!window.AppController) {
      this.ngZone.run(() => {
        // $.getScript('../../../assets/js/pushnotification/push-client.js');
        // $.getScript('../../../assets/js/pushnotification/app-controller.js');
        $.getScript('../../../assets/js/pushnotification/main.js');
        // $.getScript('../../../assets/js/pushnotification/encryption/helpers.js');
        // $.getScript('../../../assets/js/pushnotification/encryption/hmac.js');
        // $.getScript('../../../assets/js/pushnotification/encryption/hkdf.js');
        // $.getScript('../../../assets/js/pushnotification/encryption/encryption-factory.js');
        // $.getScript('../../../assets/js/chat/js/jquery.mark.min.js');
      });
    }
    // this.addStyleCss();
  }


  public ngOnInit() {
    // this.checkLoged();
    /* window.my = window.my || {};
    window.my.namespace = window.my.namespace || {};
    window.my.namespace.publicFunc = this.publicFunc.bind(this);
    window.my.namespace.publicFuncObject = this.publicFuncObject.bind(this);
    window.my.namespace.publicFuncAction = this.publicFuncAction.bind(this); */
    // this.router.navigate(['/chat_v2']);

  }

  public ngOnDestroy(): void {
    // $('.dropdown').unbind('click');
    // location.reload();
  }

  public addStyleCss() {
    let style = '.alert.alert-info { background-color: transparent; color: #00bec4; border-color: #e5f6ff; width: 300px; } ' +
      ' .alert.alert-danger { background-color: transparent; color: red; border-color: red; width: 300px; } ' +
      ' .alert.alert-warning { background-color: transparent; color: #c47f00; border-color: #c47f00; width: 300px; } ' +
      ' .mideas-open-chat span b { color: #000000; font-size: 14px; font-style: italic; } ' +
      ' .text-success.high-light { font-weight: bold; font-size: 13px; font-style: italic; }';
    $('<style id=\'myStyle\' type=\'text/css\'>' + style + '</style>').appendTo(document.head);
  }
  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    // this.loadChat();
    // this.cdRef.detectChanges();
    // console_bk.log('AAA' + JSON.stringify(this.currentUser.app_using));
    if (this.currentUser.app_using) {
      this.partnerId = this.currentUser.app_using.id;
      let s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = this.serviceConfig.HOST_MY + '/chat_backend/config?i=' + this.currentUser.id
        + '&p=' + this.currentUser.app_using.id + '&t=' + this.currentUser.token + '&a=' + this.currentUser.app_using.applications[0].app_id;
      this.elementRef.nativeElement.appendChild(s);
      $.getScript('../../../assets/js/chat/js/runable.js');
      $('body').addClass('sidebar-mini');
    } else {
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    }
  }

  public loadChat() {
    if (this.currentUser.app_using) {
      let s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = this.serviceConfig.HOST_MY + '/chat_backend/config?i=' + this.currentUser.id
        + '&p=' + this.currentUser.app_using.id + '&t=' + this.currentUser.token + '&a=' + this.currentUser.app_using.applications[0].app_id;
      this.elementRef.nativeElement.appendChild(s);
      $.getScript('../../../assets/js/chat/js/runable.js');
      $('body').addClass('sidebar-mini');
    } else { }
  }

  public getTags() {
    this.applicationService.getAllTagsByPartnerId(this.partnerId)
      .then((tags) => {
        if (tags) {
          // console_bk.log('ALL TGS ' + JSON.stringify(tags));
          this.listTags = tags;
          this.cdRef.detectChanges();
        }
      });
  }

  public publicFunc(subscription) {
    this.ngZone.run(() => this.privateFunc(subscription));
  }

  public privateFunc(subscription) {
    // console_bk.log('CALLLL OUT SITE ' + JSON.stringify(subscription));
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    let body = {
      push_subscription: subscription,
      partner_id: this.currentUser.app_using.id
    }
    this.applicationService.saveUserNotification(body)
      .then((config) => {
        if (config) { }
      });
  }

  public publicFuncObject(subscription) {
    this.ngZone.run(() => this.privateFuncObject(subscription));
  }

  public privateFuncObject(subscription) {
    // console_bk.log('CALLLL OUT SITE ' + JSON.stringify(subscription));
    if (subscription.status === 403) {
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    }
  }

  public publicFuncAction(subscription) {
    this.ngZone.run(() => this.privateFuncAction(subscription));
  }

  public privateFuncAction(subscription) {
    if (subscription.type === 'addtag') {
      $('#createQuickTemplateModel').modal('show');
      // console_bk.log('ACTIONNNNNNNNNNN 1' + subscription.type);
      this.getTags();
      this.subscription = subscription;
    } else if (subscription.type === 'notificationmsg') {
      // console_bk.log('ACTIONNNNNNNNNNN 2' + subscription.type);
      if (subscription.data.type === 'success') {
        this.notificationService.showSuccessTimer(subscription.data.msg, 3000);
      } else {
        this.notificationService.showWarningTimer(subscription.data.msg, 3000);
      }
    } else if (subscription.type === 'showlog') {
      // console_bk.log('ACTIONNNNNNNNNNN 2' + subscription.type);
      let msgId = subscription.data.msgid;
      // console.log('HIEN THI O DAY NHE chi role postsale thoi ' + msgId );
      // console.log('HIEN THI O DAY NHE chi role postsale thoi ' + JSON.stringify(this.currentUser) );
      if (this.currentUser.app_using) {
        let backendUsers = this.currentUser.app_using.backendPartnerUsers;
        let idUser = this.currentUser.id;
        let UserRole = backendUsers.find((data: any) => { return idUser === data.user_id });
        let role = UserRole.role_id;
        console.log('HIEN THI O DAY NHE chi role postsale thoi ' + JSON.stringify(UserRole) );
        if ((role === this.serviceConfig.ROLE_USER.syssupport) || (role === this.serviceConfig.ROLE_USER.sysadmin)) {
          window.open(
            this.serviceConfig.HOST_MY + '/chat_backend/getMessageById/' + msgId,
            '_blank'
          );
        }
      }
    }

  }

}
