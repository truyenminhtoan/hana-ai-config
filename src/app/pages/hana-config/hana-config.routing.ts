import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HanaConfigComponent } from './hana-config.component';
import { PopupConfigComponent } from './popup-config/popup-config.component';

const routes: Routes = [
  {
    path: '',
    component: HanaConfigComponent,
    children: [
      { path: 'popup-config', component: PopupConfigComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
