import { Component, AfterViewInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'hana-config',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['hana-config.component.scss']
})
export class HanaConfigComponent implements AfterViewInit {
    public ngAfterViewInit() {
        $('.dropdown-toggle').dropdown();
    }

}
