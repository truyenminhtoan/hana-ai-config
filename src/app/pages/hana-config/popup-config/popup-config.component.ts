import { Component, Output, EventEmitter, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { GtConfig, GenericTableComponent } from '@angular-generic-table/core';
import { CrmCustomerService } from '../../../_services/crmcustomer.service';
import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';
import { TableData } from './table-data';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Router } from '@angular/router';
import { ApplicationService } from '../../../_services/application.service';
import { NotificationService } from '../../../_services/notification.service';
import { ConfigService } from '../../../_services/config.service';
import { ChatService } from '../../../_services/index';

declare var $: any;
declare var _: any;

@Component({
    selector: 'popup-config',
    templateUrl: 'popup-config.component.html',
    styleUrls: ['popup-config.component.scss']
})
export class PopupConfigComponent {

    private requireTerm: number = 0;
    private powerby: number = 0;
    private partnerId: any;
    private appId: any;
    private textAreaHtml: any;
    private allAgent;
    private theHtmlString: any;
    private currentUser: any;
    private listAgentChoose: any;
    public isDestroy: boolean;
    private listFilterChoose: any;
    private increateDateNum: number;
    private fbInte: any;
    private collapseOptions: any = [
        { key: 0, value: 'Hình tròn' },
        { key: 1, value: 'Hình chữ nhật' },
    ];
    private collapseChoose: number = 0;
    private positionOption: any = [
        { key: 0, value: 'Phải' },
        { key: 1, value: 'Trái' },
    ];
    private positionChoose: number = 0;

    private soundAlertOptions: any = [
        { key: 0, value: this.configService.HOST_RESOURCE_CHAT + '/mideas/sounds/msg_received', title: 'Âm thanh 1' },
        { key: 1, value: this.configService.HOST_RESOURCE_CHAT + '/mideas/sounds/msg_received1', title: 'Âm thanh 2' },
        { key: 2, value: this.configService.HOST_RESOURCE_CHAT + '/mideas/sounds/msg_received2', title: 'Âm thanh 3' },
    ];
    private soundChoose: any = this.configService.HOST_RESOURCE_CHAT + '/mideas/sounds/msg_received';

    private dividentSeeOptions: any = [
        { key: 0, value: 'Được xem và trả lời' },
        { key: 1, value: 'Chỉ được xem' },
    ];
    private dividentChoose: number = 0;

    private filters = [
        { key: 'filterAll', value: 'Lọc tất cả', show: true },
        { key: 'filterUnread', value: 'Lọc tin nhắn chưa đọc', show: true },
        { key: 'filterComment', value: 'Lọc theo bình luận', show: true },
        { key: 'filterChat', value: 'Lọc theo chat', show: true },
        { key: 'filterDontUn', value: 'Lọc tin không hiểu', show: true },
        { key: 'filterPhone', value: 'Lọc có số điện thoại', show: true },
        { key: 'filterForward', value: 'Lọc theo chuyển cuộc chat', show: true },
        { key: 'filterMe', value: 'Lọc cuộc chat của tôi', show: true }
    ];

    constructor(private crmCustomerService: CrmCustomerService, private router: Router,
        private notificationService: NotificationService, private cdRef: ChangeDetectorRef, private applicationService: ApplicationService,
        private configService: ConfigService, private chatService: ChatService) {
        // this.length = this.data.length;
        this.checkLoged();
        this.theHtmlString = '';
    }
    public ngOnInit(): void {
        /* let h = $(window).height();
        $('.crm-customer-scroll').slimScroll({
            height: h - 60 + 'px'
        }); */
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }

    public checkLoged() {
        let current = localStorage.getItem('currentUser');
        if (current && current !== 'undefined') {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (this.currentUser.app_using) {
                this.partnerId = this.currentUser.app_using.id;
                this.appId = this.currentUser.app_using.applications[0].app_id;
                // console_bk.log('APPUSING ' + this.partnerId + ' ' + this.appId);
                this.getIntegrationFB();
                this.getScript();
            } else {
                localStorage.removeItem('currentUser');
                this.router.navigate(['/login-hana']);
            }
        } else {
            this.router.navigate(['/login-hana']);
        }
    }


    public getIntegrationFB() {
        let integrations = this.currentUser.app_using.integrations;
        let fbInte = _.find(integrations, (inte) => {
            return inte.gatewayId === 1;
        });
        if (fbInte) {
            if (fbInte.isExpire === 0) {
                this.fbInte = fbInte;
            }
        }
    }
    public ngOnDestroy(): void {
        this.isDestroy = true;
    }

    public getAllAgent() {
        this.chatService.getAllAgent(this.appId)
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('USERSBD ' + JSON.stringify(integrate));
                    if (integrate.errorCode === 200) {
                        // console_bk.log('USERSBD ' + JSON.stringify(integrate.data.users));
                        this.allAgent = integrate.data.users;
                        if (this.listAgentChoose.length > 0) {
                            _.each(this.allAgent, (agent) => {
                                _.each(this.listAgentChoose, (agentChoose) => {
                                    if (agent.username_chat === agentChoose) {
                                        agent.checked = true;
                                    }
                                });
                            });
                        } else {
                            _.each(this.allAgent, (agent) => {
                                agent.checked = true;
                            });
                        }

                    }
                    this.callChange();
                }
            });
    }

    public getFilterShow() {
        // console_bk.log('FILTER SAVE ' + JSON.stringify(this.listFilterChoose));
        if (this.listFilterChoose.length > 0) {
            _.each(this.filters, (filter) => {
                _.each(this.listFilterChoose, (filterChoose) => {
                    if (filter.key === filterChoose.key) {
                        if (filterChoose.show === false) {
                            filter.show = false;
                        }
                    }
                });
            });
        }
        this.callChange();
    }

    private getScript() {
        this.applicationService.getIntegrationByAppId(this.appId, 2)
            .then((integrate) => {
                // console_bk.log('getScript ' + JSON.stringify(integrate));
                if (integrate) {
                    this.requireTerm = integrate.require_terms ? integrate.require_terms : 0;
                    this.powerby = integrate.powerby_hide ? integrate.powerby_hide : 0;
                    this.textAreaHtml = integrate.terms_html ? integrate.terms_html : '';
                    this.collapseChoose = integrate.collapse_template ? integrate.collapse_template : 0;
                    this.soundChoose = integrate.sound_bk_url ? integrate.sound_bk_url : this.soundChoose;
                    this.dividentChoose = integrate.dividend_chat_see ? integrate.dividend_chat_see : this.dividentChoose;
                    this.listAgentChoose = integrate.list_agent_chat ? JSON.parse(integrate.list_agent_chat) : [];
                    this.listFilterChoose = integrate.list_filter_chat ? JSON.parse(integrate.list_filter_chat) : [];
                    this.positionChoose = integrate.popup_position ? integrate.popup_position : 0;
                    this.getAllAgent();
                    this.getFilterShow();
                    this.callChange();
                }
            });
    }

    private saveColorConfig() {
        // console_bk.log('save saveColorConfig ' + this.colorBackground);
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'require_terms',
                    value: this.requireTerm
                },
                {
                    key: 'powerby_hide',
                    value: this.powerby
                },
                {
                    key: 'terms_html',
                    value: this.textAreaHtml.trim()
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }

    private saveCollapseConfig() {
        // console_bk.log('save saveColorConfig ' + this.colorBackground);
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'collapse_template',
                    value: this.collapseChoose
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }

    private saveAgentChoose() {
        let arrayAgentChoose = [];
        console.log('AGENTCHOOSE ' + JSON.stringify(this.allAgent));
        _.each(this.allAgent, (agent) => {
            if (agent.checked) {
                arrayAgentChoose.push(agent.username_chat);
            }
        });

        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'list_agent_chat',
                    value: JSON.stringify(arrayAgentChoose)
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }
    private saveSoundConfig() {
        // console_bk.log('save saveColorConfig ' + this.colorBackground);
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'sound_bk_url',
                    value: this.soundChoose
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }
    private playSound(soundUrl) {
        let audioElement = document.createElement('audio');
        audioElement.setAttribute('src', soundUrl + '.mp3');
        audioElement.play();
    }

    private saveSeeDividentConfig() {
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'dividend_chat_see',
                    value: this.dividentChoose
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }

    private saveFilterChoose() {
        let arrayAgentChoose = [];
        console.log('AGENTCHOOSE ' + JSON.stringify(this.allAgent));
        _.each(this.filters, (fil) => {
            arrayAgentChoose.push({
                key: fil.key,
                show: fil.show
            });
        });

        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'list_filter_chat',
                    value: JSON.stringify(arrayAgentChoose)
                }
            ]
        };
        // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }

    private savePositionConfig() {
        // console_bk.log('save saveColorConfig ' + this.colorBackground);
        let body = {
            partner_id: this.partnerId,
            appId: this.appId,
            gatewayId: 2,
            keys: [
                {
                    key: 'popup_position',
                    value: this.positionChoose
                }
            ]
        };
        this.applicationService.saveScriptConfigByPartnerId(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });
    }

    private increaseDate() {
        if (!this.fbInte) {
            return false;
        }
        console.log('GIA HAN BAO NHIEU NGAY  ' + this.increateDateNum);
        let body = {
            appId: this.appId,
            partner_id: this.partnerId,
            page_id: this.fbInte.pageId,
            month: this.increateDateNum
        };
        this.applicationService.increaseTryUsing(body)
            .then((config) => {
                if (config) {
                    this.notificationService.showSuccessTimer('Thành công ! Cài đặt này có thể mất 10 phút để có hiệu lực', 5000);
                }
            });


    }
}
