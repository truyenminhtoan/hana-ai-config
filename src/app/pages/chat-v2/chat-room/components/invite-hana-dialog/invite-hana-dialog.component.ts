import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';

@Component({
    selector: 'invite-hana-dialog',
    templateUrl: 'invite-hana-dialog.component.html',
    styleUrls: ['invite-hana-dialog.component.scss']
})
export class InviteHanaDialogComponent {

    private title: string;
    private type: string;
    private partnerId: string;
    private room: string;
    private sender: string;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<InviteHanaDialogComponent>, private chatService: ChatService) {
        // this.customers = this.data.customers;
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.type = data.type;
        this.partnerId = data.partner_id;
        this.room = data.room;
        this.sender = data.sender;
    }


    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }
    public toggleHana() {
        // console_bk.log('CLICK TOOGLE ' + this.type);
        /* if (this.type === 'invite') {
            let bodyPush = {
                type: 'pickupchat',
                data: {
                    room: this.room + '@muc.10min',
                    from: this.sender + '@10min',
                    is_pick: 0
                }
            }
            this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
                .then((tagR) => {
                });
        } else {
            let bodyPush = {
                type: 'pickupchat',
                data: {
                    room: this.room + '@muc.10min',
                    from: this.sender + '@10min',
                    is_pick: 1
                }
            }
            this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
                .then((tagR) => {
                });
        } */

        this.dialogRef.close(this.type);
    }
}
