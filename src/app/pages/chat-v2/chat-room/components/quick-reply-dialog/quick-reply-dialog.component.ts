import { Component, Inject, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';
import { Router } from '@angular/router';

declare var $: any;
declare var _: any;

@Component({
    selector: 'quick-reply-dialog',
    templateUrl: 'quick-reply-dialog.component.html',
    styleUrls: ['quick-reply-dialog.component.scss']
})
export class QuickReplyDialogComponent {
    private title: string;
    private type: string;
    private partnerId: string;
    private appId: string;
    private room: string;
    private sender: string;
    private templateReply: any;
    private searchContent: string;
    private searchReply: any;

    private allAgent: any;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, private router: Router, public dialogRef: MdDialogRef<QuickReplyDialogComponent>, private chatService: ChatService, private cdRef: ChangeDetectorRef) {
        // this.customers = this.data.customers;
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.type = data.type;
        this.partnerId = data.partner_id;
        this.room = data.room;
        this.sender = data.sender;
        this.appId = data.appId;
        this.templateReply = data.templateReply;
        this.searchReply = data.templateReply;

    }

    public ngOnInit() {
        // console_bk.log('TEMPLATEEE ' + JSON.stringify(this.templateReply));
    }

    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }

    public createQuick() {
        this.dialogRef.close(this.type);
        this.router.navigate(['/quick-reply-template/quick-list']);
    }

    public sendMessage(type, data) {
        if (type === 'send') {
            // console_bk.log('send data ' + data.text_search);
        } else {
            // console_bk.log('edit data ' + data.text_search);
        }
        let dataCB = {
            typeT: type,
            dataT: data
        };
        this.dialogRef.close(dataCB);
    }

    public searchQuick() {
        this.templateReply = this.searchReply;
        if (this.searchContent && this.searchContent !== '') {
            this.templateReply = _.filter(this.searchReply, (tem) => {
                return (tem.text_search.indexOf(this.searchContent) >= 0);
            });
        }
    }

}
