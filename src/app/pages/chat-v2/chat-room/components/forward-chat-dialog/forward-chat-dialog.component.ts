import { Component, Inject, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';
declare var $: any;
declare var _: any;
@Component({
    selector: 'forward-chat-dialog',
    templateUrl: 'forward-chat-dialog.component.html',
    styleUrls: ['forward-chat-dialog.component.scss']
})
export class ForwardChatDialogComponent {

    private title: string;
    private type: string;
    private partnerId: string;
    private appId: string;
    private room: string;
    private sender: string;
    private configGateway: any;
    private isDestroy: boolean;
    private allAgent: any;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<ForwardChatDialogComponent>, private chatService: ChatService, private cdRef: ChangeDetectorRef) {
        // this.customers = this.data.customers;
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.type = data.type;
        this.partnerId = data.partner_id;
        this.room = data.room;
        this.sender = data.sender;
        this.appId = data.appId;
        this.configGateway = data.configGateway;
    }

    public ngOnInit() {
        this.getAllAgent();
    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }
    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }

    public getAllAgent() {
        this.chatService.getAllAgent(this.appId)
            .then((integrate) => {
                if (integrate) {
                    // this.allAgent = integrate.data.users;
                    this.allAgent = [];
                    let users = integrate.data.users;
                    // console_bk.log('ALLagent ' + JSON.stringify(users));
                    let listAgentChoose = this.configGateway.list_agent_chat ? JSON.parse(this.configGateway.list_agent_chat) : [];
                    if (listAgentChoose.length > 0) {
                        _.each(users, (agent) => {
                            _.each(listAgentChoose, (agentChoose) => {
                                if (agent.username_chat === agentChoose) {
                                    this.allAgent.push(agent);
                                }
                            });
                        });
                    } else {
                        this.allAgent = users;
                    }
                    // console_bk.log('UPDATE SUCCESS ' + JSON.stringify(this.allAgent));
                    this.callChange();
                }
            });
    }
    public forwardChat(chat) {
        this.dialogRef.close(chat);
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
