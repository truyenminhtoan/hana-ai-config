import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';

@Component({
    selector: 'private-reply-dialog',
    templateUrl: 'private-reply-dialog.component.html',
    styleUrls: ['private-reply-dialog.component.scss']
})
export class PrivateReplyDialogComponent {
    private message: any;
    private title: string;
    private body: any;

    private messageSend: string;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<PrivateReplyDialogComponent>, private chatService: ChatService) {
        // this.customers = this.data.customers;
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.message = data.message;
        this.title = data.title;
        this.body = data.body;
    }

    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }
    public send() {
        if (this.messageSend) {
            if (this.messageSend.trim() === '') {
                alert('Vui lòng nhập nội dung tin nhắn');
                return false;
            }

        } else {
            alert('Vui lòng nhập nội dung tin nhắn');
            return false;
        }
        this.dialogRef.close({
            msg: this.messageSend,
            bodyCmt: this.body
        });
    }
}
