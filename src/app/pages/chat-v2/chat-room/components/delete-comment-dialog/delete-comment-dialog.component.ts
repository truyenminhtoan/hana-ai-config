import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';

@Component({
    selector: 'delete-comment-dialog',
    templateUrl: 'delete-comment-dialog.component.html',
    styleUrls: ['delete-comment-dialog.component.scss']
})
export class DeleteCommentDialogComponent {
    private message: any;
    private title: string;
    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<DeleteCommentDialogComponent>, private chatService: ChatService) {
        // this.customers = this.data.customers;
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.message = data.message;
        this.title = data.title;
    }

    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }
    public toggleHana() {
        this.dialogRef.close(this.message);
    }
}
