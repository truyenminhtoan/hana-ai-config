import { ApplicationService } from '../../../_services/application.service';
import { ChatService } from '../../../_services/chat.service';
import { ElementRef, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MdDialog } from '@angular/material';
import { InviteHanaDialogComponent } from './components/invite-hana-dialog/invite-hana-dialog.component';
import { ForwardChatDialogComponent } from './components/forward-chat-dialog/forward-chat-dialog.component';
import { QuickReplyDialogComponent } from './components/quick-reply-dialog/quick-reply-dialog.component';
import { ConfigService } from '../../../_services/config.service';
import { UUID } from 'angular2-uuid';
import { Message } from '../../../../assets/plugins/primeng/components/common/message';
import { DeleteCommentDialogComponent } from './components/delete-comment-dialog/delete-comment-dialog.component';
import { PrivateReplyDialogComponent } from './components/private-reply-dialog/private-reply-dialog.component';

import { DomSanitizer } from '@angular/platform-browser';
import { SecurityContext } from '@angular/core';

import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Output, EventEmitter,
    ChangeDetectorRef, Directive, HostListener
} from '@angular/core';
import { NotificationService } from '../../../_services/index';
declare var $: any;
declare var _: any;
declare var Base64: any;
declare var X2JS: any;
declare var slick: any;
declare var window: any;
declare var hanaapi: any;
import * as moment from 'moment';
import { EmitterService } from '../../../_services/emitter.service';

let $msg = hanaapi.env.$msg;
let $iq = hanaapi.env.$iq;
let $pres = hanaapi.env.$pres;
let $build = hanaapi.env.$build;

@Component({
    selector: 'chat-room',
    templateUrl: 'chat-room.component.html',
    styleUrls: ['../assets/css/material-dashboard.css', '../assets/css/chat.css', 'chat-room.component.scss']
})
export class ChatRoomComponent {
    @Input() appId: string;
    @Input() partnerId: string;
    @Input() allCustomerInfo: any;
    @Input() allAgent: any;
    @Input() isOpenChat: any;
    @Input() usernameChat: any;
    @Input() listTags: any;
    @Input() templateReply: any;
    @Input() jid: any;
    @Input() rawReceive: any;
    @Input() agentActive: any;
    @Input() connectStatus: string;
    @Input() userinfoChange: any;
    @Input() chatOpenObject: any;
    @Input() roleUser: any;
    @Input() configGateway: any;
    @Input() emitTraining: string;

    private uuidFacebook: any;
    private allChatMe: any;
    private listMessage: any;
    private scrollRoom = false;
    private textBoxArea: any;
    private postInfo: any;
    private isDestroy: boolean;

    private percentTags: number;
    private countConnet = 0;
    private position = 'above';
    private typingstatus = false;

    private KEY = {
        ENTER: 13,
        FORWARD_SLASH: 47
    };

    @ViewChild('scrollMeChat') private myScrollContainer: ElementRef;
    @ViewChild('uploadImage') private uploadImage: ElementRef;
    @ViewChild('uploadFile') private uploadFile: ElementRef;

    @Output() emitChatRoom = new EventEmitter<any>();



    constructor(private _sanitizer: DomSanitizer, private cdRef: ChangeDetectorRef, private notificationService: NotificationService, private applicationService: ApplicationService, private chatService: ChatService, private dialog: MdDialog, private configService: ConfigService) {
    }
    public ngOnInit() {

    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }

    public ngOnChanges(changes: SimpleChanges) {

        if (changes['connectStatus'] && this.connectStatus) {
            if (this.connectStatus === 'connected') {
                this.countConnet = this.countConnet + 1;
                // console_bk.log('SO LAN CONNECT ' + this.countConnet);
                if (this.countConnet > 1) {
                    let bodyFirst = {
                        app_id: this.appId,
                        size_load: 25
                    };
                    this.listMessage = [];
                    let size = 25;
                    if (this.chatOpenObject.type === 'groupchat') {
                        size = 25;
                    } else {
                        size = 15;
                    }
                    let body = {
                        room_name: this.chatOpenObject.name + '@muc.10min',
                        unix_start: 0,
                        unix_end: 0,
                        size_msg: size,
                        useragent: this.usernameChat.username,
                        sort: 0
                    };
                    this.getMessageHistory(body);
                }
            }
        }
        if (changes['rawReceive'] && this.rawReceive) {
            // console_bk.log('NHAMESSAGE CHATROOM ' + this.rawReceive);
            this.onMessageRawReceive();
        }
        if (this.chatOpenObject && changes['chatOpenObject']) {
            // console_bk.log('MO CHAAAAAAAAAATTTTTTTTTTT ' + JSON.stringify(this.chatOpenObject));
            this.scrollRoom = false;
            this.listMessage = [];
            let size = 25;
            if (this.chatOpenObject.type === 'groupchat') {
                size = 25;
            } else {
                size = 15;
            }
            let body = {
                room_name: this.chatOpenObject.name + '@muc.10min',
                unix_start: 0,
                unix_end: 0,
                size_msg: size,
                useragent: this.usernameChat.username,
                sort: 0
            };
            this.getMessageHistory(body);
            if (this.chatOpenObject.entry === 'fbcomment') {
                this.getPostInfo();
            }
            // mo chat box
            this.allChatMe = false;
            if (this.chatOpenObject.type_user === 'facebook' && this.chatOpenObject.entry === 'groupchat') {
                if (this.chatOpenObject.cus_avatar) {
                    let idUserFb = this.chatOpenObject.cus_avatar.substr(this.chatOpenObject.cus_avatar.indexOf('/picture') - 20, 20);
                    idUserFb = idUserFb.substr(idUserFb.indexOf('/') + 1, 20);
                    this.chatOpenObject.fb_link = 'https://www.facebook.com/' + idUserFb;
                    this.uuidFacebook = idUserFb;
                } else {
                    this.chatOpenObject.fb_link = 'https://www.facebook.com/' + '#';
                    this.uuidFacebook = '#';
                }
            }
            // console_bk.log('MO CHAT BOX ' + JSON.stringify(this.chatOpenObject));

            if (this.listTags) {
                // this.percentTags = 100 / (this.listTags.length + 1) - 1;
                this.percentTags = 100 / (this.listTags.length) - 1;
                // console_bk.log('LISTAGSSSSSSSSSS ' + JSON.stringify(this.listTags));
                _.each(this.listTags, (tagl) => {
                    tagl.opacityRoom = 0.3;
                    _.each(this.chatOpenObject.tags, (tagR) => {
                        if (tagR.id === tagl.id) {
                            tagl.opacityRoom = 1;
                        }
                    });
                });
                // console_bk.log('OPEN  ' + JSON.stringify(this.chatOpenObject));
            }
        }
    }

    public onMessageRawReceive() {
        let x2js = new X2JS();
        let jsonObj = x2js.xml_str2json(this.rawReceive);
        // console_bk.log('HICHICHICCCC ' + JSON.stringify(jsonObj));
        if (!this.chatOpenObject) {
            return false;
        }
        if (jsonObj.message) {
            let message = jsonObj.message;
            let broadcast = message._broadcast;
            if (broadcast) {
                this.handleBroadCastMessage(message);
                return false;
            }
            if (message.x) {
                // console_bk.log('');
            } else {
                if (this.chatOpenObject.name !== this.chatService.getNodeChat(message._from)) {
                    return false;
                }
            }

            if (message.x) {
                this.handleMessageEvent(message);
            } else if (message.body) {
                this.handleMessageBody(message);
            } else if (message.frontendstructure) {
                this.handleMessageStruct(message);
            } else if (message.fbcomment) {
                this.handleMessageComment(message);
            }

        } else if (jsonObj.iq) {
            // console_bk.log('GOI TIN IQ');
        } else if (jsonObj.presence) {
            // console_bk.log('PRESENCE');
        }

    }
    public handleMessageEvent(message) {
        // console_bk.log('MesssageEvent ' + JSON.stringify(message));
        if (message.x.sent === '') {

            let letMessage = _.find(this.listMessage, (msg) => {
                return msg.id === message.x.msgid;
            });
            // console_bk.log('MesssageEventAAA ' + JSON.stringify(letMessage));
            if (letMessage) {
                letMessage.sended = true;
                letMessage.loadingsend = false;
                this.callChange();
            }
        }
    }

    public handleMessageBody(message) {
        if (this.chatService.getNodeChat(message._from) !== this.chatOpenObject.name) {
            return;
        }
        // console_bk.log('HANDLE BODY DDDD ' + JSON.stringify(message));
        if (message._member === this.usernameChat.username) {
            return;
        }
        let timeSend = Date.now();
        this.onMessageBody(message, 'realtime', timeSend);

    }
    public handleMessageStruct(message) {
        if (this.chatService.getNodeChat(message._from) !== this.chatOpenObject.name) {
            return;
        }
        // console_bk.log('HANDLE BODY DDDD ' + JSON.stringify(message));
        if (message._member === this.usernameChat.username) {
            return;
        }
        let timeSend = Date.now();
        this.onMessageStructure(message, 'realtime', timeSend);
    }
    public handleMessageComment(message) {
        this.deleteCommentLoading();
        let timeSend = Date.now();
        if (this.chatService.getNodeChat(message._from) !== this.chatOpenObject.name) {
            return;
        }
        this.onCommentBody(message, 'realtime', timeSend, null);
    }
    public handleBroadCastMessage(message) {
        // console_bk.log('MESSAGE BROADCAST ' + JSON.stringify(message));
        let member = message._member;
        let from = this.chatService.getNodeChat(message._from);
        let text = message.broadcast;
        let broadcast = message._broadcast;

        if (broadcast === 'pickupchat') {
            this.chatOpenObject.is_pick = '1';
            this.callChange();
        } else if (broadcast === 'invitepickupchat') {
            this.chatOpenObject.is_pick = '0';
            this.callChange();
        } else if (broadcast === 'pintags') {
            this.broadcastReceiveTagsEvent(text, from);
        } else if (broadcast === 'likecomment' || broadcast === 'hidecomment' || broadcast === 'deletecomment' || broadcast === 'editcomment') {
            this.broadcastReceiveCommentEvent(broadcast, text, from);
        } else if (broadcast === 'callbackfromfb') {
            this.broadcastFacebookCallbackEvent(text, from);
        } else if (broadcast === 'detectphonenumber') {
            // this.broadcastDetectPhoneEvent(text, from);
        }
    }
    public broadcastFacebookCallbackEvent(text, from) {
        let dataJson = JSON.parse(Base64.decode(text));
        let message = dataJson.data.message;
        let msgId = dataJson.data.msgid;
        let messageOb = _.each(this.listMessage, (msg) => {
            return msg.id === msgId;
        });
        if (messageOb) {
            messageOb.errormsg = true;
            this.callChange();
        }
        this.notificationService.showDangerTimer(message, 10000);
    }
    public broadcastReceiveCommentEvent(broadcast, text, from) {
        // chua xy ly comment
    }
    public broadcastReceiveTagsEvent(text, from) {
        let jsonBase = JSON.parse(Base64.decode(text));
        let tags = jsonBase.data.tags;
        // console_bk.log('OPEN PINGTAG ' + JSON.stringify(this.chatOpenObject.tags));
        this.chatOpenObject.tags = tags;
        if (this.listTags) {
            this.percentTags = 100 / (this.listTags.length + 1) - 1;
            _.each(this.listTags, (tagl) => {
                tagl.opacityRoom = 0.3;
                _.each(this.chatOpenObject.tags, (tagR) => {
                    if (tagR.id === tagl.id) {
                        tagl.opacityRoom = 1;
                    }
                });
            });
        }
        this.callChange();
    }
    public broadcastDetectPhoneEvent(text, from) {
        /* let emitR = {
            room: from
        };
        this.emitChatRoom.emit({
            type: 'detectphonenumber',
            data: emitR
        }); */
    }

    public deleteCommentLoading() {
        this.listMessage = _.filter(this.listMessage, (msg) => {
            return msg.type !== 'temcomment';
        });
    }
    public markSupport(support) {
        let body;
        if (support === 'unneedsp') {
            body = {
                muc_room_id: this.chatOpenObject.name,
                need_support: false
            };
            this.chatOpenObject.need_support = false;
        } else {
            body = {
                muc_room_id: this.chatOpenObject.name,
                need_support: true
            };
            this.chatOpenObject.need_support = true;
        }

        this.chatService.updateMucRoom(JSON.stringify(body))
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('UPDATE SUCCESS');
                    if (support === 'unneedsp') {
                        this.chatOpenObject.need_support = false;
                    } else {
                        this.chatOpenObject.need_support = true;
                    }
                    this.callChange();
                }
            });
    }

    public markUnRead() {
        let body;
        body = {
            muc_room_id: this.chatOpenObject.name,
            last_message_status: 0
        };
        this.chatService.updateMucRoom(JSON.stringify(body))
            .then((integrate) => {
                if (integrate) {
                    this.chatOpenObject.last_message_status = 0;
                    this.callChange();
                }
            });
    }

    public allChatMeSearch() {
        if (this.allChatMe) {
            this.allChatMe = false;
            let emitR = {
                allChatMe: false,
                room: this.chatOpenObject.creater
            };
            this.emitChatRoom.emit({
                type: 'searchRoom',
                data: emitR
            });
            // search all
        } else {
            this.allChatMe = true;
            let emitR = {
                allChatMe: true,
                room: this.chatOpenObject.creater
            };
            this.emitChatRoom.emit({
                type: 'searchRoom',
                data: emitR
            });
            // search me
        }

    }

    public getMessageHistory(body) {
        // console_bk.log('AllAgentAA ' + JSON.stringify(this.allCustomerInfo));
        this.chatService.getStoreMsg(JSON.stringify(body))
            .then((integrate) => {
                if (integrate) {
                    _.each(integrate.msg, (msg) => {
                        this.onMessageReceive(msg.rawmsg, 'offline', integrate.timereceive, msg);
                    });
                    this.scrollRoom = false;
                }
            });
    }

    public onMessageReceive(message, type, time, msg) {
        let x2js = new X2JS();
        let jsonObj = x2js.xml_str2json(message);
        if (this.chatOpenObject.entry === 'groupchat') {
            if (jsonObj.message) {
                if (jsonObj.message.body) {
                    this.onMessageBody(jsonObj.message, type, time);
                } else if (jsonObj.message.frontendstructure) {
                    this.onMessageStructure(jsonObj.message, type, time);
                }
            }
        } else if (this.chatOpenObject.entry === 'fbcomment') {
            if (jsonObj.message) {
                this.onCommentBody(jsonObj.message, type, time, msg);
            }
        }

    }


    public commentShowFB(message) {
        window.open(this.chatService.getFbLink(message.body_json.comment_id), '_blank');
    }
    public commentEdit(type, message) {
        if (type === 'edit') {
            message.is_editing = true;

        } else {
            this.saveCommentFB(message);
            message.is_editing = false;
        }

        this.callChange();
    }
    public saveCommentFB(message) {
        let body = {
            appId: this.appId,
            commentId: message.body_json.comment_id,
            roomId: this.chatOpenObject.name,
            message: message.body_json.message
        };
        this.chatService.editCommentFacebook(JSON.stringify(body))
            .then((fbdata) => {
                if (!fbdata) {
                    return false;
                }
                if (fbdata.code === 200) {
                    this.executeSaveComment(message);
                } else {
                    alert('Lỗi xảy ra khi sửa bình luận');
                }
            });
    }
    public executeSaveComment(message) {
        let body = {
            from: 'fb_user',
            to: this.chatOpenObject.name,
            msgid: message.id,
            content: Base64.encode(JSON.stringify(message.body_json))
        };
        this.chatService.updateStoreMessage(JSON.stringify(body))
            .then((fbdata) => {
            });

        // push to queue
        let bodyPush = {
            type: 'editcomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                actions: {
                    msgid: message.id,
                    content: Base64.encode(JSON.stringify(message.body_json))
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });

    }

    public commentDeleteFBConfirm(message) {
        let dialogRef = this.dialog.open(DeleteCommentDialogComponent, {
            width: '50%',
            data: {
                title: 'Bạn có muốn XÓA bình luận này?',
                message: message
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (result) {
                    this.deleteCommentFB(result);
                }
            }
        });
    }

    public deleteCommentFB(message) {
        // console_bk.log('DELETE ' + JSON.stringify(message));
        let body = {
            appId: this.appId,
            commentId: message.body_json.comment_id,
            roomId: this.chatOpenObject.name,
        };
        this.chatService.deleteCommentFacebook(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    if (fbdata.code === 200) {
                        this.executeDeleteComment(message);
                    }
                }
            });
    }

    public executeDeleteComment(message) {
        let body = {
            from: 'fb_user',
            to: this.chatOpenObject.name,
            msgid: message.id,
            is_delete: 'true'
        };
        this.chatService.updateStoreMessage(JSON.stringify(body))
            .then((fbdata) => {
            });
        // push to queue
        let bodyPush = {
            type: 'deletecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                actions: {
                    msgid: message.id,
                    is_delete: 'true'
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });
        let bodyPush1 = {
            type: 'updatecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                msgid: message.id,
                actions: {
                    is_like: 'false',
                    is_hide: 'false',
                    is_delete: 'true'
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush1))
            .then((tagR) => {
            });
        message.CheckResult.can_remove = false;
        message.CheckResult.can_like = false;
        message.CheckResult.can_hide = false;
        message.CheckResult.can_reply_privately = false;
        this.callChange();
    }


    public commentLikeFB(type, message) {
        // console_bk.log('LIKE MSSS ' + type + '     kkk ' + JSON.stringify(message));
        let body;
        if (type === 'like') {
            body = {
                appId: this.appId,
                commentId: message.body_json.comment_id,
                isLike: true
            };
        } else {
            body = {
                appId: this.appId,
                commentId: message.body_json.comment_id,
                isLike: false
            };
        }
        this.chatService.likeCommentFacebook(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    if (fbdata.code === 200) {
                        this.executeLikeComment(type, message);
                    }
                }
            });
    }
    public executeLikeComment(type, message) {
        let body;
        let like;
        if (type === 'like') {
            like = 'true';
        } else {
            like = 'false';
        }
        body = {
            from: 'fb_user',
            to: this.chatOpenObject.name,
            msgid: message.id,
            is_like: like
        };
        this.chatService.updateStoreMessage(JSON.stringify(body))
            .then((fbdata) => {
            });
        // push to queue
        let bodyPush = {
            type: 'likecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                actions: {
                    msgid: message.id,
                    is_like: like
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });
        let bodyPush1 = {
            type: 'updatecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                msgid: message.id,
                actions: {
                    is_like: like,
                    is_hide: message.msg_status ? message.msg_status.is_hide : 'false',
                    is_delete: message.msg_status ? message.msg_status.is_delete : 'false',
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush1))
            .then((tagR) => {
            });
        if (message.msg_status) {
            message.msg_status.is_like = like;
        } else {
            message.msg_status = {
                is_like: 'true'
            };
        }
        this.callChange();
    }

    public commentHideFB(type, message) {
        // console_bk.log('LIKE MSSS ' + type + '     kkk ' + JSON.stringify(message));
        let body;
        if (type === 'unhide') {
            body = {
                appId: this.appId,
                commentId: message.body_json.comment_id,
                hide: false
            };
        } else {
            body = {
                appId: this.appId,
                commentId: message.body_json.comment_id,
                hide: true
            };
        }
        this.chatService.hideCommentFacebook(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    if (fbdata.code === 200) {
                        this.executeHideComment(type, message);
                    }
                }
            });
    }

    public executeHideComment(type, message) {
        let body;
        let hide;
        if (type === 'unhide') {
            hide = 'false';
        } else {
            hide = 'true';
        }
        body = {
            from: 'fb_user',
            to: this.chatOpenObject.name,
            msgid: message.id,
            is_hide: hide
        };
        this.chatService.updateStoreMessage(JSON.stringify(body))
            .then((fbdata) => {
            });
        // push to queue
        let bodyPush = {
            type: 'hidecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                actions: {
                    msgid: message.id,
                    is_hide: hide
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });
        let bodyPush1 = {
            type: 'updatecomment',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                msgid: message.id,
                actions: {
                    is_like: message.msg_status ? message.msg_status.is_like : 'false',
                    is_hide: hide,
                    is_delete: message.msg_status ? message.msg_status.is_delete : 'false',
                }
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush1))
            .then((tagR) => {
            });
        if (message.msg_status) {
            message.msg_status.is_hide = hide;
        } else {
            message.msg_status = {
                is_hide: 'true'
            };
        }
        this.callChange();
    }
    public checkComment(message) {
        let body = {
            appId: this.appId,
            commentId: message.body_json.comment_id
        };
        this.chatService.checkCommentFacebook(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    if (fbdata.CheckResult) {
                        if (fbdata.CheckResult.error) {
                            message.CheckResult = {
                                is_delete: false
                            };
                        }
                        message.CheckResult = fbdata.CheckResult;
                        this.callChange();
                    }
                }
            });
    }

    public privateReply(message) {
        let body = {
            app: this.appId,
            type: 'groupchat',
            creater: message.from,
            uid: message.from
        };
        // console_bk.log('PRIVATE REPLYYYY ' + JSON.stringify(message));
        this.chatService.checkRoomExist(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    if (fbdata.errorCode === 200) {
                        // co roi thi mo len thoi
                        // console_bk.log('OPEN THIS ROOM ' + fbdata.room);
                        this.emitChatRoom.emit({
                            type: 'openChat',
                            data: {
                                room: fbdata.room
                            }
                        });
                        this.callChange();
                        // let room = _.find()
                    } else {
                        // chua co thi gui tin vao
                        let dialogRef = this.dialog.open(PrivateReplyDialogComponent, {
                            width: '50%',
                            data: {
                                title: 'Nhắn tin tới khách hàng',
                                app_id: this.appId,
                                partner_id: this.partnerId,
                                sender: this.usernameChat.username,
                                room: this.chatOpenObject.name,
                                body: message.body_json
                            }
                        });
                        dialogRef.afterClosed().subscribe((result) => {
                            if (result) {
                                this.sendPrivateMessge(result.msg, result.bodyCmt);
                            }
                        });
                    }
                }
            });
    }

    public sendPrivateMessge(message, bodyJson) {
        let body = {
            appId: this.appId,
            source: 'facebook',
            message: Base64.encode(message),
            roomId: this.chatOpenObject.name,
            usernameChat: Base64.encode(this.usernameChat.username),
            comment: Base64.encode(JSON.stringify(bodyJson))
        };
        this.chatService.fbSendPrivateReplies(JSON.stringify(body))
            .then((fbdata) => {
                if (fbdata) {
                    // console_bk.log('');
                }
            });

    }
    public onMessageBody(messageBody, typeStore, timeStore) {
        let timenow = Date.now();
        let from = messageBody._from;
        let to = messageBody._to;
        let timesend = messageBody._timesend;
        let id = messageBody._id;
        let type = messageBody._type;
        let bodyJson = JSON.parse(Base64.decode(messageBody.body));
        let isCustomerSend = true;
        let isCustomerType = 'customer';
        if (messageBody._member) {
            from = messageBody._member;
        } else {
            from = this.chatService.getNodeChat(from);
        }

        if (typeStore === 'offline') {
            timesend = timenow - (timeStore - timesend);
        } else {
            timesend = timenow;
            timeStore = timenow;
        }

        if (this.chatService.checkIfAgent(from, this.allAgent)) {
            isCustomerSend = false;
            isCustomerType = 'agent';
        } else if (this.chatService.checkIfHana(from)) {
            isCustomerSend = false;
            isCustomerType = 'hana';
        } else if (this.chatService.checkFBUser(from)) {
            isCustomerSend = false;
            isCustomerType = 'fbuser';
        } else {
            isCustomerSend = true;
        }
        // console_bk.log('PUSHMESSAGE IS CUSTOMER ' + JSON.stringify(bodyJson));
        if (bodyJson.type === 'attachment') {
            bodyJson.body = JSON.parse(bodyJson.body);
        }
        let messageJSON = {
            from: from,
            to: to,
            timesend: timesend,
            id: id,
            type: type,
            msg_type: 'body',
            body_json: bodyJson,
            is_customer: isCustomerSend,
            isCustomerType: isCustomerType,
            typeStore: typeStore,
            timeStore: timeStore
        };

        let findMsg = _.find(this.listMessage, (msgf) => {
            return msgf.id === id;
        });

        if (!findMsg) {

            this.pushMessage(messageJSON, typeStore);

        }
    }

    // commentttttt
    public onCommentBody(messageBody, typeStore, timeStore, msg) {
        // console_bk.log('MESSAGE COMMENT ' + JSON.stringify(messageBody));
        let timenow = Date.now();
        let from = messageBody._member ? messageBody._member : messageBody._from;
        let to = messageBody._to;
        let timesend = messageBody._timesend;
        let id = messageBody._id;
        let type = messageBody._type;
        let bodyJson = JSON.parse(Base64.decode(messageBody.fbcomment));
        if (msg) {
            bodyJson = JSON.parse(msg.msg);
            // console_bk.log('BODYCOMMENT ' + JSON.stringify(bodyJson));
        }
        // from = this.chatService.getNodeChat(from);
        if (messageBody._member) {
            from = messageBody._member;
        } else {
            from = this.chatService.getNodeChat(from);
        }
        if (typeStore === 'offline') {
            timesend = timenow - (timeStore - timesend);
        } else {
            timesend = timenow;
            timeStore = timenow;
        }
        let msg_status;
        if (msg && msg.msg_status) {
            msg_status = JSON.parse(msg.msg_status);
        }
        let messageJSON = {
            from: from,
            to: to,
            timesend: timesend,
            id: id,
            type: type,
            msg_type: 'fbcomment',
            msg_status: msg_status,
            body_json: bodyJson,
            typeStore: typeStore,
            timeStore: timeStore
        };

        let findMsg = _.find(this.listMessage, (msgf) => {
            return msgf.id === id;
        });

        if (!findMsg) {
            this.pushMessage(messageJSON, typeStore);
            this.checkComment(messageJSON);

        }
    }
    public onMessageStructure(messageBody, typeStore, timeStore) {
        // // console_bk.log('XIN CHAO NHE messageBody' + JSON.stringify(messageBody));
        let timenow = Date.now();
        let from = messageBody._from;
        let to = messageBody._to;
        let timesend = parseInt(messageBody._timesend, 10);
        let id = messageBody._id;
        let type = messageBody._type;
        let bodyJson;
        if (messageBody.frontendstructure._namespace) {
            bodyJson = JSON.parse(Base64.decode(messageBody.frontendstructure.__text));
        } else {
            bodyJson = JSON.parse(Base64.decode(messageBody.frontendstructure));
        }
        let isCustomerSend = true;
        let isCustomerType = 'customer';
        if (messageBody._member) {
            from = messageBody._member;
        } else {
            from = this.chatService.getNodeChat(from);
        }
        if (typeStore === 'offline') {
            timesend = timenow - (timeStore - timesend);
        } else {
            timesend = timenow;
            timeStore = timenow;
        }
        // console_bk.log("AGETNTTTTTTTT " + from + " AGEBT " + JSON.stringify(this.allAgent));
        if (this.chatService.checkIfAgent(from, this.allAgent)) {
            isCustomerSend = false;
            isCustomerType = 'agent';
        } else if (this.chatService.checkIfHana(from)) {
            isCustomerSend = false;
            isCustomerType = 'hana';
        } else if (this.chatService.checkFBUser(from)) {
            isCustomerSend = false;
            isCustomerType = 'fbuser';
        } else {
            isCustomerSend = true;
        }
        // console_bk.log('PUSHMESSAGE ' + from + ' IS CUSTOMER ' + timeStore);

        _.each(bodyJson, (bodyJ, index) => {
            if (bodyJ.message.text && bodyJ.message.quick_replies) { //quick reply
                let messageJSON = {
                    from: from,
                    to: to,
                    timesend: timesend + index,
                    id: id + index,
                    type: type,
                    msg_type: 'quickreply',
                    body_json: bodyJ,
                    is_customer: isCustomerSend,
                    isCustomerType: isCustomerType,
                    typeStore: typeStore,
                    timeStore: timeStore
                };

                let findMsg = _.find(this.listMessage, (msgf) => {
                    return msgf.id === id;
                });

                if (!findMsg) {

                    this.pushMessage(messageJSON, typeStore);

                }
            } else if (bodyJ.message.text) {
                let messageJSON = {
                    from: from,
                    to: to,
                    timesend: timesend + index,
                    id: id + index,
                    type: type,
                    msg_type: 'text',
                    body_json: bodyJ,
                    is_customer: isCustomerSend,
                    isCustomerType: isCustomerType,
                    typeStore: typeStore,
                    timeStore: timeStore
                };

                let findMsg = _.find(this.listMessage, (msgf) => {
                    return msgf.id === id;
                });
                // console_bk.log('MESSAGETEXT ' + JSON.stringify(messageJSON));
                if (!findMsg) {
                    this.pushMessage(messageJSON, typeStore);

                }
            } else if (bodyJ.message.attachment) {
                if (bodyJ.message.attachment.type === 'image') {

                    let messageJSON = {
                        from: from,
                        to: to,
                        timesend: timesend + index,
                        id: id + index,
                        type: type,
                        msg_type: 'imagestruct',
                        body_json: bodyJ,
                        is_customer: isCustomerSend,
                        isCustomerType: isCustomerType,
                        typeStore: typeStore,
                        timeStore: timeStore
                    };

                    let findMsg = _.find(this.listMessage, (msgf) => {
                        return msgf.id === id;
                    });

                    if (!findMsg) {
                        this.pushMessage(messageJSON, typeStore);

                    }
                } else if (bodyJ.message.attachment.type === 'template') {
                    if (bodyJ.message.attachment.payload.template_type === 'button') {
                        let messageJSON = {
                            from: from,
                            to: to,
                            timesend: timesend + index,
                            id: id + index,
                            type: type,
                            msg_type: 'button',
                            body_json: bodyJ,
                            is_customer: isCustomerSend,
                            isCustomerType: isCustomerType,
                            typeStore: typeStore,
                            timeStore: timeStore
                        };

                        let findMsg = _.find(this.listMessage, (msgf) => {
                            return msgf.id === id;
                        });

                        if (!findMsg) {
                            this.pushMessage(messageJSON, typeStore);

                        }
                    } else {
                        // console_bk.log('CARDTRUCT ' + JSON.stringify(bodyJ));
                        let messageJSON = {
                            from: from,
                            to: to,
                            timesend: timesend + index,
                            id: id + index,
                            type: type,
                            msg_type: 'generic',
                            body_json: bodyJ,
                            is_customer: isCustomerSend,
                            isCustomerType: isCustomerType,
                            typeStore: typeStore,
                            timeStore: timeStore
                        };

                        let findMsg = _.find(this.listMessage, (msgf) => {
                            return msgf.id === id;
                        });

                        if (!findMsg) {

                            this.pushMessage(messageJSON, typeStore);

                            $('#slick-' + messageJSON.id).slick({
                                infinite: false
                            });
                        }
                    }
                }

            } else if (bodyJ.message.attachments) {
                _.each(bodyJ.message.attachments, (attach) => {
                    let messageJSON = {
                        from: from,
                        to: to,
                        timesend: timesend + index,
                        id: id + index,
                        type: type,
                        msg_type: 'attachments',
                        body_json: attach,
                        is_customer: isCustomerSend,
                        isCustomerType: isCustomerType,
                        typeStore: typeStore,
                        timeStore: timeStore
                    };

                    let findMsg = _.find(this.listMessage, (msgf) => {
                        return msgf.id === id;
                    });

                    if (!findMsg) {
                        this.pushMessage(messageJSON, typeStore);

                    }
                });
            }
        });
    }

    public sortMessageRoom() {
        this.listMessage = _.sortBy(this.listMessage, (msg) => {
            return msg.timesend;
        });
        this.callChange();
    }

    public updateInfoSender(messageJSON) {
        // console.log('MESSSSSSSSSSSSSSSS ' + JSON.stringify(messageJSON));
        // console_bk.log('MESSSSSSSSSSSSSSSSBBBBBB ' + JSON.stringify(this.allAgent));
        if (this.chatOpenObject.entry === 'groupchat') {
            if (messageJSON.is_customer) {
                let agent = _.find(this.allCustomerInfo, (ag) => {
                    return ag.customer_username === messageJSON.from;
                });
                // console_bk.log('CUSTOMERRRRRR ' + JSON.stringify(agent));
                if (agent) {
                    messageJSON.url_avatar = agent.avatar_url;
                }
            } else {
                if (messageJSON.isCustomerType === 'agent') {
                    let agent = _.find(this.allAgent, (ag) => {
                        return ag.username_chat === messageJSON.from;
                    });
                    if (agent) {
                        messageJSON.url_avatar = agent.url_avatar;
                    }
                } else if (messageJSON.isCustomerType === 'hana') {

                } else if (messageJSON.isCustomerType === 'fbuser') {
                    messageJSON.url_avatar = this.chatService.getAvatarFbFromCreater(this.chatOpenObject.creater);
                }
            }
        } else {
            this.updateInfoSenderComment(messageJSON);
        }
        if (!this.scrollRoom) {
            this.scrollToBottom();
        }
        this.callChange();
    }

    public updateInfoSenderComment(messageJSON) {
        if (messageJSON.body_json.is_customer) {
            let agent = _.find(this.allCustomerInfo, (ag) => {
                return ag.customer_username === messageJSON.from;
            });
            // console_bk.log('CUSTOMERRRRRR ' + JSON.stringify(agent));
            if (agent) {
                messageJSON.url_avatar = agent.avatar_url;
            }
        } else {
            // console_bk.log('DAY LA AGENT COMMENT ' + JSON.stringify(messageJSON));
            messageJSON.url_avatar = this.chatService.getAvatarFb(messageJSON.body_json.page_id);
        }

        if (!this.scrollRoom) {
            this.scrollToBottom();
        }
        this.callChange();

    }

    public scrollToBottom() {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err) { }
    }

    public pushMessage(messageJSON, typeStore) {

        let timeSend = messageJSON.timesend;
        if (this.listMessage.length === 0) {
            /* let messageJSONIndi = {
                from: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
                to: this.chatOpenObject.name + '@muc.10min',
                timesend: timeSend - 1,
                id: 'msgid',
                type: 'groupchat',
                msg_type: 'msgindicator',
                body_json: {},
                is_customer: true,
                isCustomerType: '',
                typeStore: timeSend,
                timeStore: timeSend,
                loadingsend: true
            };
            this.listMessage.push(messageJSONIndi); */
        } else {
            let msgLast;
            if (typeStore === 'online') {
                msgLast = _.last(this.listMessage);
            } else {
                msgLast = _.first(this.listMessage);
            }
            let lastTime = msgLast.timesend;
            let timeSendDay = moment(timeSend).format('YYYY-MM-DD');
            let lastTimeDay = moment(lastTime).format('YYYY-MM-DD');
            // console_bk.log('DAYFIRST ' + timeSendDay + ' DAYLAST ' + lastTimeDay);
            let boonDay = false;
            if (typeStore === 'online') {
                boonDay = moment(lastTimeDay).isBefore(timeSendDay, 'day')
            } else {
                boonDay = moment(lastTimeDay).isAfter(timeSendDay, 'day')
            }
            if (boonDay) {
                moment.locale('vi');
                let messageTime;
                let timeIndicator;
                if (typeStore === 'online') {
                    messageTime = moment(timeSendDay).format('dddd, [ngày] DD MMMM [năm] YYYY');
                    timeIndicator = timeSend - 1;
                } else {
                    messageTime = moment(lastTimeDay).format('dddd, [ngày] DD MMMM [năm] YYYY');
                    timeIndicator = timeSend + 1;
                }

                let messageJSONIndi = {
                    from: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
                    to: this.chatOpenObject.name + '@muc.10min',
                    timesend: timeIndicator,
                    id: 'msgid',
                    type: 'groupchat',
                    msg_type: 'msgindicator',
                    body_json: {},
                    is_customer: true,
                    isCustomerType: '',
                    typeStore: timeSend,
                    timeStore: timeSend,
                    formatDay: messageTime,
                    loadingsend: true
                };
                this.listMessage.push(messageJSONIndi);
            }
        }
        // console.log('MESSAGEJSON ' + JSON.stringify(messageJSON));
        this.listMessage.push(messageJSON);
        this.sortMessageRoom();
        this.updateInfoSender(messageJSON);
    }
    public chatRoomScroll(event) {
        let $div = $(event.target);
        if ($div.scrollTop() === 0) {
            let msgLast = _.first(this.listMessage);
            if (!msgLast) {
                return;
            }
            // console_bk.log('THISMESAGE ' + JSON.stringify(msgLast));
            let size = 25;
            if (this.chatOpenObject.type === 'groupchat') {
                size = 25;
            } else {
                size = 15;
            }
            let body = {
                room_name: this.chatOpenObject.name + '@muc.10min',
                unix_start: 0,
                unix_end: msgLast.timesend,
                size_msg: size,
                useragent: this.usernameChat.username,
                sort: 0
            };
            this.scrollRoom = true;
            this.getMessageHistory(body);
        }
    }

    public addTags(tag) {
        // console_bk.log('addtags ' + JSON.stringify(tag));
        if (tag.opacityRoom === 1) {
            this.chatOpenObject.tags = _.filter(this.chatOpenObject.tags, (ro) => { return ro.id !== tag.id; });
            let body = {
                muc_room_id: this.chatOpenObject.name,
                tags: this.chatOpenObject.tags
            };
            this.chatService.addTags(JSON.stringify(body))
                .then((tagR) => {
                    if (tagR) {
                    }
                });

            let bodyPush = {
                type: 'pintags',
                data: {
                    room: this.chatOpenObject.name + '@muc.10min',
                    from: this.usernameChat.username + '@10min',
                    tags: this.chatOpenObject.tags
                }
            }
            this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
                .then((tagR) => {
                    if (tagR) {
                    }
                });
            tag.opacityRoom = 0.3;
        } else {
            if (this.chatOpenObject.tags) {
                this.chatOpenObject.tags.push({
                    id: tag.id,
                    color: tag.color
                });
            } else {
                this.chatOpenObject.tags = [];
                this.chatOpenObject.tags.push({
                    id: tag.id,
                    color: tag.color
                });
            }
            let body = {
                muc_room_id: this.chatOpenObject.name,
                tags: this.chatOpenObject.tags
            };
            this.chatService.addTags(JSON.stringify(body))
                .then((tagR) => {
                    if (tagR) {
                    }
                });
            let bodyPush = {
                type: 'pintags',
                data: {
                    room: this.chatOpenObject.name + '@muc.10min',
                    from: this.usernameChat.username + '@10min',
                    tags: this.chatOpenObject.tags
                }
            }
            this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
                .then((tagR) => {
                    if (tagR) {
                    }
                });
            tag.opacityRoom = 1;
        }
        this.callChange();

    }

    public inviteHana(type) {
        let dialogRef;
        if (type === 'invite') {
            // console_bk.log(type);
            dialogRef = this.dialog.open(InviteHanaDialogComponent, {
                width: '50%',
                data: {
                    title: 'Bạn có muốn Mời trợ lý vào cuộc chat',
                    app_id: this.appId,
                    partner_id: this.partnerId,
                    sender: this.usernameChat.username,
                    room: this.chatOpenObject.name,
                    type: 'invite'
                }
            });
        } else {
            // console_bk.log(type);
            dialogRef = this.dialog.open(InviteHanaDialogComponent, {
                width: '50%',
                data: {
                    title: 'Bạn có muốn Bỏ trợ lý khỏi cuộc chat?',
                    app_id: this.appId,
                    partner_id: this.partnerId,
                    sender: this.usernameChat.username,
                    room: this.chatOpenObject.name,
                    type: 'kick'
                }
            });
            // console_bk.log(type);
        }

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                // console_bk.log('AAAAAAA ' + JSON.stringify(result));
                if (result === 'invite') {
                    this.chatOpenObject.is_pick = 0;
                    this.sendInvite();
                } else {
                    this.chatOpenObject.is_pick = 1;
                    this.sendPickup();
                }
            }
        });
    }

    public forwardChat() {
        let dialogRef;
        dialogRef = this.dialog.open(ForwardChatDialogComponent, {
            width: '50%',
            data: {
                title: 'Bạn muốn chuyển cuộc chat này cho ai?',
                app_id: this.appId,
                partner_id: this.partnerId,
                sender: this.usernameChat.username,
                room: this.chatOpenObject.name,
                configGateway: this.configGateway,
                type: 'invite',
                appId: this.appId
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                // console_bk.log('FORWARD CHAT ' + JSON.stringify(result));
                let body = {
                    muc_room_id: this.chatOpenObject.name,
                    member_forwarded: result.username_chat,
                    status_forwarded: 1,
                    supporter: result.username_chat
                };

                this.chatService.updateMucRoom(JSON.stringify(body))
                    .then((integrate) => {
                    });
                let dataSend = { type: 'forwardChat', member: result.username_chat, entry: this.chatOpenObject.entry };
                let iq = $iq({
                    to: this.chatOpenObject.name + '@muc.10min',
                    type: 'set',
                    id: Math.random().toString(36).substr(2, 6)
                }).c('query', {
                    xmlns: 'broadcast'
                }).t(Base64.encode(JSON.stringify(dataSend)));
                let emitR = {
                    room: this.chatOpenObject.name
                };
                this.emitChatRoom.emit({
                    type: 'deleteContact',
                    data: emitR
                });
                this.notificationService.showSuccessTimer('Bạn đã chuyển cuộc chat thành công', 3000);
                this.chatOpenObject = undefined;
                this.callChange();
                hanaapi.sendIQ(iq, this.forwardSuccess.bind(this), this.forwardFail.bind(this));

            }
        });
    }

    public forwardSuccess(success) {
        // console_bk.log('FORWAR success');
    }
    public forwardFail(success) {
        // console_bk.log('FORWAR forwardFail');
    }
    public quickChat() {
        let dialogRef;
        dialogRef = this.dialog.open(QuickReplyDialogComponent, {
            width: '50%',
            data: {
                title: 'Chọn câu trả lời',
                app_id: this.appId,
                partner_id: this.partnerId,
                sender: this.usernameChat.username,
                room: this.chatOpenObject.name,
                type: 'invite',
                appId: this.appId,
                templateReply: this.templateReply
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (result.typeT === 'edit') {
                    let message = result.dataT.text_search;
                    this.textBoxArea = message;
                    this.callChange();
                } else if (result.typeT === 'send') {
                    let message = result.dataT.text_search;
                    if (this.chatOpenObject.entry === 'groupchat') {
                        this.sendChatRoomMessage(message);
                    } else {
                        this.sendChatRoomComment(message, 'message');
                    }
                }
                // console_bk.log('CHONCAUTRALOIÈ        ' + JSON.stringify(result));
            }
        });
    }

    public clickUpload(type, ev) {
        let $inputFile = $(ev.target);
        if (type === 'file') {
            // console_bk.log('UPLOAD FILE ');
            // console_bk.log('UPLOAD IMAGE ');

            let file = ev.target.files[0];
            if (!file) {
                return false;
            }
            let sizeFile = ev.target.files[0].size / 1024 / 1024;
            if (sizeFile >= 5) {
                alert('Bạn chỉ được gửi hình có dung lượng nhỏ hơn 5M');
                $inputFile.val('');
                return false;
            }

            let fileName = $inputFile.val();
            let ext = fileName.match(/\.([^\.]+)$/)[1];
            let typefile = 'file';
            switch (ext) {
                case 'png':
                case 'jpg':
                case 'jpeg':
                case 'gif':
                    typefile = 'image';
                    break;
                case 'doc':
                case 'docx':
                case 'zip':
                case 'rar':
                case 'xls':
                case 'csv':
                case 'xlsx':
                case 'pdf':
                case 'txt':
                    typefile = 'file';
                    break;
                case 'mp3':
                    typefile = 'audio';
                    break;
                case 'mp4':
                case 'avi':
                    typefile = 'video';
                    break;
                default:
                    typefile = undefined;
                    alert('Chỉ gửi các file có định dạng png,jpg,zip,csv,rar,doc,xls,pdf,txt, mp3, mp4');
                    $inputFile.val('');
                    return;
            }

            let formData = new FormData();
            formData.append('file', file);
            formData.append('type', typefile);
            $.ajax({
                url: this.configService.HOST_UPLOAD_IMAGE + '/file',
                type: 'POST',
                data: formData,
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                success: (data) => {
                    $inputFile.val('');
                    try {
                        // console_bk.log('dataaa ' + data);
                        let image = [];
                        let firstImage;
                        _.each(data, (da) => {
                            let imagelink = this.configService.HOST_STATIC_FILE + da;
                            firstImage = imagelink;
                            image.push(imagelink);
                        });
                        // kiem tra comment hay chat kia
                        if (this.chatOpenObject.entry === 'groupchat') {
                            this.sendChatRoomAttachmentMessage(image, typefile);
                        }
                    } catch (e) {

                    }
                },
                error: (XMLHttpRequest, textStatus, errorThrown) => {
                }
            });
        } else {
            // console_bk.log('UPLOAD IMAGE ');

            let file = ev.target.files[0];
            if (!file) {
                return false;
            }
            let sizeFile = ev.target.files[0].size / 1024 / 1024;
            if (sizeFile >= 5) {
                alert('Bạn chỉ được gửi hình có dung lượng nhỏ hơn 5M');
                $inputFile.val('');
                return false;
            }

            let formData = new FormData();
            formData.append('file', file);
            formData.append('type', 'image');
            $.ajax({
                url: this.configService.HOST_UPLOAD_IMAGE,
                type: 'POST',
                data: formData,
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                success: (data) => {
                    $inputFile.val('');
                    try {
                        // console_bk.log('dataaa ' + data);
                        let image = [];
                        let firstImage;
                        _.each(data, (da) => {
                            let imagelink = this.configService.HOST_STATIC_FILE + da;
                            firstImage = imagelink;
                            image.push(imagelink);
                        });
                        if (this.chatOpenObject.entry === 'groupchat') {
                            this.sendChatRoomImageMessage(image);
                        } else {
                            this.sendChatRoomComment(firstImage, 'link');
                        }
                    } catch (e) {

                    }
                },
                error: (XMLHttpRequest, textStatus, errorThrown) => {
                }
            });
        }
    }

    public sendText(ev) {
        let $textarea = $(ev.target);
        if (!this.textBoxArea) {
            return;
        }
        if (ev.keyCode === this.KEY.ENTER) {
            if (!ev.shiftKey) {
                ev.preventDefault();
                let message = this.textBoxArea;
                // message = this._sanitizer.sanitize(SecurityContext.HTML, message);
                // console_bk.log('THISMESSAGE ' + JSON.stringify(this.chatOpenObject));
                if (this.textBoxArea.trim() !== '') {
                    this.sendPickup();
                    if (this.chatOpenObject.entry === 'groupchat') {
                        this.sendChatRoomMessage(message);
                    } else if (this.chatOpenObject.entry === 'fbcomment') {
                        this.sendChatRoomComment(message, 'message');
                    }
                }
                this.textBoxArea = '';
                this.callChange();
            }
        } else {
            if (this.textBoxArea === '') {
                this.iqBroadCastAction('untyping');
                return;
            }
            this.iqBroadCastAction('typing');
        }

    }

    public iqBroadCastAction(status) {
        let dataSend = {};
        if (status === 'untyping') {
            this.typingstatus = false;
            dataSend = {
                type: 'typing',
                data: {
                    status: 'untyping',
                    username: this.usernameChat.username,
                    content: ''
                }
            };
        } else if (status === 'typing') {
            if (this.typingstatus) {
                return false;
            }
            this.typingstatus = true;
            setTimeout(() => {
                this.typingstatus = false;
            }, 10000);
            dataSend = {
                type: 'typing',
                data: {
                    status: 'typing',
                    username: this.usernameChat.username,
                    content: ''
                }
            };
        }
        this.configService.log(' TYPINGGGGGGG ' + status);
        let iq = $iq({
            to: this.chatOpenObject.name + '@muc.10min',
            type: 'set',
            id: Math.random().toString(36).substr(2, 6)
        }).c('query', {
            xmlns: 'broadcast'
        }).t(Base64.encode(JSON.stringify(dataSend)));

        hanaapi.sendIQ(iq, this.forwardSuccess.bind(this), this.forwardFail.bind(this));
    }
    public sendInvite() {
        // console_bk.log('AGENTACTIVE ' + JSON.stringify(this.agentActive));
        let iqinvite = $iq({
            to: this.chatOpenObject.name + '@muc.10min',
            type: 'set',
            id: Math.random().toString(36).substr(2, 6)
        })
            .c('query', {
                xmlns: 'invite'
            });
        let itemmember = $build('member', {
            jid: this.agentActive.agent_id,
            role: 'agent'
        });
        iqinvite = iqinvite.cnode(itemmember.node).up();
        let itemqueue = $build('queue', {
            exchange: this.agentActive.queue_name,
            key: this.agentActive.queue_name,
            is_pick: 0
        });
        iqinvite = iqinvite.cnode(itemqueue.node).up();
        hanaapi.sendIQ(iqinvite, this.pickUpSuccess.bind(this), this.pickUpFail.bind(this));
        this.chatOpenObject.is_pick = 0;
        this.callChange();
        let bodyPush = {
            type: 'pickupchat',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                is_pick: 0
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });
    }
    public sendPickup() {
        this.iqBroadCastAction('untyping');
        let timeSend = Date.now();
        let item = $build('member', {
            jid: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
            role: 'member'
        });
        let iq = $iq({
            to: this.chatOpenObject.name + '@muc.10min',
            type: 'set',
            id: Math.random().toString(36).substr(2, 6)
        }).c('query', {
            xmlns: 'kickbot'
        }).cnode(item.node).up();
        hanaapi.sendIQ(iq, this.pickUpSuccess.bind(this), this.pickUpFail.bind(this));

        this.chatOpenObject.is_pick = 1;
        this.chatOpenObject.last_message_time = timeSend;
        this.callChange();
        this.updateSupporter();

        let bodyPush = {
            type: 'pickupchat',
            data: {
                room: this.chatOpenObject.name + '@muc.10min',
                from: this.usernameChat.username + '@10min',
                is_pick: 1
            }
        }
        this.chatService.pushToQueue(this.partnerId, JSON.stringify(bodyPush))
            .then((tagR) => {
            });
        this.emitChatRoom.emit({
            type: 'sortmucroom',
            data: true
        });
    }
    public pickUpSuccess(value) {
        // console_bk.log('SUCCESS ' + value);
    }
    public pickUpFail(value) {
        // console_bk.log('FAIL ' + value);
    }

    public checkBoxClick(ev, message) {
        if (ev.target.checked) {
            // console_bk.log('THANGNAY CHECKED ' + JSON.stringify(message));
            _.each(this.listMessage, (msg) => {
                msg.checkreply = false;
            });
            message.checkreply = true;
        } else {
            _.each(this.listMessage, (msg) => {
                msg.checkreply = false;
            });
            // console_bk.log('THANGNAY UNNN ' + JSON.stringify(message));
        }
        this.callChange();
    }

    public updateSupporter() {
        if (this.chatOpenObject.supporter !== this.usernameChat.username) {
            let body;
            body = {
                muc_room_id: this.chatOpenObject.name,
                supporter: this.usernameChat.username
            };
            this.chatService.updateMucRoom(JSON.stringify(body))
                .then((integrate) => {
                    // console_bk.log(''); 
                });

        }
    }

    public sendChatRoomMessage(message) {
        let timeSend = Date.now();
        let msgid = UUID.UUID();
        let textjson = { 'type': 'text', 'body': message, 'msg_id': msgid };
        textjson = JSON.parse(this.chatService.regexReplaceEnter(JSON.stringify(textjson)));
        let base64Text = Base64.encode(JSON.stringify(textjson));
        let isCustomerSend = false;
        let isCustomerType = 'agent';
        let messageJSON = {
            from: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
            to: this.chatOpenObject.name + '@muc.10min',
            timesend: timeSend,
            id: msgid,
            type: 'groupchat',
            msg_type: 'body',
            body_json: textjson,
            is_customer: isCustomerSend,
            isCustomerType: isCustomerType,
            typeStore: timeSend,
            timeStore: timeSend,
            loadingsend: true
        };

        let msg = $msg({
            to: this.chatOpenObject.name + '@muc.10min',
            sendfrom: 'backend',
            type: 'groupchat',
            id: msgid
        }).c('body').t(base64Text);
        hanaapi.send(msg);
        this.textBoxArea = '';
        this.setTimeOutMessageSend(messageJSON);
        this.pushMessage(messageJSON, 'online');

    }

    public sendChatRoomComment(messageS, type) {
        // console_bk.log('SEND COMMENT ' + messageS);
        let findChoose = _.find(this.listMessage, (msg) => {
            return msg.checkreply ? msg.checkreply : false;
        });
        if (findChoose) {
            // console_bk.log('TRA LOI TIN NHAN CHO THANG NAY ' + JSON.stringify(findChoose));
            this.excuteSendComment(messageS, type, findChoose);
        } else {
            let listUser = _.filter(this.listMessage, (msg) => {
                // console_bk.log('CUSTOMER ' + msg.is_customer + ' CHECKRESULT ' + msg.CheckResult.can_comment);
                return msg.body_json.is_customer && msg.CheckResult.can_comment;
            });
            if (listUser && listUser.length > 0) {
                findChoose = _.last(listUser);
                // console_bk.log('TRA LOI THANG CUOI  ' + JSON.stringify(findChoose));
                this.excuteSendComment(messageS, type, findChoose);
            } else {
                // console_bk.log('KHONG TIM THAY THANG NAO Tim thang ben phai');
                listUser = _.filter(this.listMessage, (msg) => {
                    // console_bk.log('CUSTOMER ' + msg.is_customer + ' CHECKRESULT ' + msg.CheckResult.can_comment);
                    return !msg.body_json.is_customer && msg.CheckResult.can_comment;
                });
                // console_bk.log('LIST CO THE COMMENT  ' + JSON.stringify(listUser));
                if (listUser && listUser.length > 0) {
                    findChoose = _.last(listUser);
                    // console_bk.log('TRA LOI THANG CUOI  ' + JSON.stringify(findChoose));
                    this.excuteSendComment(messageS, type, findChoose);
                } else {
                    alert('Không còn bình luận nào');
                }
            }
        }

        this.textBoxArea = '';
        this.callChange();
    }

    public excuteSendComment(messageS, type, findChoose) {
        let bodySend;
        if (type === 'message') {
            bodySend = {
                roomId: this.chatOpenObject.name,
                msg_id: findChoose.id,
                parentComment: findChoose.body_json,
                comment: {
                    message: messageS
                }
            };
        } else {
            bodySend = {
                roomId: this.chatOpenObject.name,
                msg_id: findChoose.id,
                parentComment: findChoose.body_json,
                comment: {
                    link: messageS
                }
            };
        }

        let body = {
            appId: this.appId,
            source: 'facebook',
            body: Base64.encode(JSON.stringify(bodySend))
        };
        this.chatService.fbSendComment(JSON.stringify(body))
            .then((integrate) => {
                // console_bk.log('SEND SUCES');
            });

        // loading comment
        let messageJSON = {
            id: 'comment-temp',
            type: 'temcomment',
            msg_type: 'fbcomment',
            body_json: {
                is_customer: false,
                msg: messageS
            }
        };
        let findMsg = _.find(this.listMessage, (msgf) => {
            return msgf.id === 'comment-temp';
        });
        if (!findMsg) {
            this.pushMessage(messageJSON, 'online');
        }
    }

    public sendChatRoomImageMessage(image) {
        this.sendPickup();
        let timeSend = Date.now();
        let msgid = UUID.UUID();
        let textjson = { type: 'images', body: '', img_url: image, msg_id: msgid };
        let base64Text = Base64.encode(JSON.stringify(textjson));
        let attachment = { attachment: { payload: { url: image[0] }, type: 'image' } };

        let isCustomerSend = false;
        let isCustomerType = 'agent';
        let messageJSON = {
            from: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
            to: this.chatOpenObject.name + '@muc.10min',
            timesend: timeSend,
            id: msgid,
            type: 'groupchat',
            msg_type: 'body',
            body_json: textjson,
            is_customer: isCustomerSend,
            isCustomerType: isCustomerType,
            typeStore: timeSend,
            timeStore: timeSend,
            loadingsend: true
        };

        let msg = $msg({
            to: this.chatOpenObject.name + '@muc.10min',
            type: 'groupchat',
            sendfrom: 'backend',
            id: msgid
        }).c('body').t(base64Text);
        hanaapi.send(msg);
        this.setTimeOutMessageSend(messageJSON);
        this.pushMessage(messageJSON, 'online');
    }

    public sendChatRoomAttachmentMessage(image, type) {
        this.sendPickup();
        let timeSend = Date.now();
        let msgid = UUID.UUID();
        // let base64Text = Base64.encode(JSON.stringify(textjson));

        let attachment = { attachments: [{ payload: { url: image[0] }, type: type }] };
        // let textjson  = {"type":"attachment","body":JSON.stringify(attachment),"msg_id":msgid};
        let textjson = { type: 'attachment', body: attachment, msg_id: msgid };
        let isCustomerSend = false;
        let isCustomerType = 'agent';

        let textjsonSend = { type: 'attachment', body: JSON.stringify(attachment), msg_id: msgid };
        let base64Text = Base64.encode(JSON.stringify(textjsonSend));

        let messageJSON = {
            from: this.chatService.getNodeChat(this.usernameChat.username + '@10min'),
            to: this.chatOpenObject.name + '@muc.10min',
            timesend: timeSend,
            id: msgid,
            type: 'groupchat',
            msg_type: 'body',
            body_json: textjson,
            is_customer: isCustomerSend,
            isCustomerType: isCustomerType,
            typeStore: timeSend,
            timeStore: timeSend,
            loadingsend: true
        };

        let msg = $msg({
            to: this.chatOpenObject.name + '@muc.10min',
            type: 'groupchat',
            sendfrom: 'backend',
            id: msgid
        }).c('body').t(base64Text);
        hanaapi.send(msg);
        this.setTimeOutMessageSend(messageJSON);
        this.pushMessage(messageJSON, 'online');

    }

    public setTimeOutMessageSend(message) {
        setTimeout(() => {
            this.updateMessageSended(message);
        }, 10000);
    }
    public updateMessageSended(message) {
        if (!message.sended) {
            message.errormsg = true;
            message.loadingsend = false;
            this.callChange();
        }
    }

    public sendMsgButton() {
        this.sendPickup();
        if (this.chatOpenObject.entry === 'groupchat') {
            this.sendChatRoomMessage(this.textBoxArea);
        } else {
            this.sendChatRoomComment(this.textBoxArea, 'message');
        }

    }


    // POST
    public getPostInfo() {

        let body = {
            post_id: this.chatOpenObject.post_id,
            app_id: this.appId
        }
        this.chatService.getPost(JSON.stringify(body))
            .then((post) => {
                if (!post) {
                    return false;
                }
                if (post.data.length > 0) {
                    this.postInfo = post.data[0];
                    this.explorePostInfo();
                } else {
                    let bodyP = {
                        appId: this.appId,
                        source: 'facebook',
                        postId: this.chatOpenObject.post_id
                    };
                    this.chatService.getPostInfo(JSON.stringify(bodyP))
                        .then((postP) => {
                            if (!postP) {
                                return false;
                            }
                            this.postInfo = postP.data;
                            this.explorePostInfo();
                        });
                }

            });
    }

    public explorePostInfo() {
        // console_bk.log('POST INFO ' + JSON.stringify(this.postInfo));
        this.callChange();
        if (this.postInfo.photos) {
            $('#photo-' + this.postInfo.post_id).slick({
                infinite: false
            });
        }
    }

    public showLogTraining(msg) {
        if ((this.roleUser === this.configService.ROLE_USER.sysadmin) || (this.roleUser === this.configService.ROLE_USER.syssupport)) {
            window.open(
                this.configService.HOST_MY + '/chat_backend/getMessageById/' + msg.id,
                '_blank'
            );
        }
    }
    public trainingContent(msg) {
        let body = {
            type: 'training',
            data: {
                content: msg
            }
        };
        EmitterService.get(this.emitTraining).emit(body);
    }
    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
