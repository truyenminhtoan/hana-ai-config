import { ApplicationService } from '../../../_services/application.service';
import { ChatService } from '../../../_services/chat.service';
import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef, Directive, HostListener
} from '@angular/core';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { EmitterService } from '../../../_services/emitter.service';
import { ConfigService } from '../../../_services/config.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavigationExtras } from '@angular/router/src/router';
declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;


@Component({
    selector: 'chat-contacts',
    templateUrl: 'chat-contacts.component.html',
    styleUrls: ['../assets/css/material-dashboard.css', '../assets/css/chat.css', 'chat-contacts.component.scss']
})
export class ChatContactsComponent {
    @Input() appId: string;
    @Input() partnerId: string;
    @Input() allCustomerInfo: any;
    @Input() allAgent: any;
    @Input() chatFilter: any;
    @Input() chatFilterChanage: any;
    @Input() usernameChat: any;
    @Input() listTags: any;
    @Input() searchChatMe: any;
    @Input() searchChatMeChanage: any;
    @Input() openRoomId: any;
    @Input() sortMucRoomStatus: boolean;
    @Input() rawReceive: any;
    @Input() emitFromRoom: any;
    @Input() connectStatus: any;
    @Input() userinfoChange: any;
    @Input() configGateway: any;
    @Input() hidden: any;
    @Input() roomFocus: any;

    @Output() contactClick = new EventEmitter<any>();

    private inputSearch: string;
    private loadingContact: boolean;
    private lstContacts: any;
    private isTextSearh: boolean;
    private tagsString: string;
    private countConnet = 0;
    private isDestroy: boolean;
    private position = 'right';
    private emitFocusRoom = 'ROUTE_FOCUS_ROOM';

    constructor(private cdRef: ChangeDetectorRef, private router: Router, private applicationService: ApplicationService, private chatService: ChatService, configService: ConfigService) {
    }
    public ngOnInit() {
        this.registerEmit();
        //   get muc room
        this.isTextSearh = false;
        if (!this.allCustomerInfo) {
            this.allCustomerInfo = [];
        }
        if (!this.lstContacts) {
            this.lstContacts = [];
        }
    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }
    public ngAfterViewInit() {
        // console_bk.log('AFTERINNNNNNNNNNNNNNNN ' + JSON.stringify(this.lstContacts));

    }
    public ngOnChanges(changes: SimpleChanges) {
        // // console_bk.log('Change input');
        if (changes['roomFocus'] && this.roomFocus) {
            let roomTem = this.roomFocus.split('@');
            if (roomTem.length !== 2) {
                return false;
            }
            let roomEnd = roomTem[1];
            this.roomFocus = Base64.decode(roomEnd);
            let openRoom = _.find(this.lstContacts, (contact) => {
                return contact.name === this.roomFocus;
            });
            // console_bk.log('LISTROM ' + JSON.stringify(openRoom));
            if (openRoom) {
                if (!openRoom.is_open) {
                    this.openChat(openRoom);
                }
            } else {
                let body = {
                    app_id: this.appId,
                    appid: this.appId,
                    name: this.roomFocus
                };
                this.getMucRoom(body, 'offline', '');
            }
        }

        if (changes['userinfoChange'] && this.userinfoChange) {
            this.isTextSearh = false;
            this.inputSearch = '';
            this.changeInfoContact();
        }
        if (changes['connectStatus'] && this.connectStatus) {
            if (this.connectStatus === 'connected') {
                this.countConnet = this.countConnet + 1;
                // console_bk.log('SO LAN CONNECT ' + this.countConnet);
                if (this.countConnet > 1) {
                    let bodyFirst = {
                        app_id: this.appId,
                        size_load: 25
                    };
                    this.lstContacts = [];
                    bodyFirst = this.getParamToSearch(bodyFirst);
                    this.getMucRoom(bodyFirst, 'offline', '');
                }
            }
        }
        if (changes['emitFromRoom'] && this.emitFromRoom) {
            // console_bk.log('CONTACT NHAN '  + JSON.stringify(this.emitFromRoom));
            this.isTextSearh = false;
            this.inputSearch = '';
            this.emitFromRoomAction();
        }
        if (changes['rawReceive'] && this.rawReceive) {
            // // console_bk.log('CONTACT NHAN');
            // // console_bk.log('NHAMESSAGE CHATROOM ' + this.rawReceive);
            this.onMessageRawReceive();
        }
        if (changes['sortMucRoomStatus'] && this.sortMucRoomStatus) {
            // console_bk.log('SORT MUC ROOM');
            this.sortMucRoom();
        }

        if (changes['openRoomId'] && this.openRoomId) {
            // console_bk.log('CHANGE OOPEN ROOM ' + this.openRoomId);
            let roomEx = _.find(this.lstContacts, (cont) => {
                return cont.name === this.openRoomId;
            });
            if (roomEx) {
                // console_bk.log('CHANGE OOPEN ROOM ' + JSON.stringify(roomEx));
                this.openChat(roomEx);
                this.callChange();
            } else {
                // console_bk.log('ADD THEM VAO FIRST CHANGE OOPEN ROOM ' + this.openRoomId);
                let bodyRe = {
                    app_id: this.appId,
                    appid: this.appId,
                    name: this.openRoomId
                };
                this.getMucRoomPrivateReply(bodyRe);
            }
        }
        if (changes['searchChatMeChanage'] && this.searchChatMeChanage) {
            // console_bk.log('CHANGESERACH CHAT ' + JSON.stringify(this.searchChatMe));
            this.isTextSearh = false;
            this.inputSearch = '';
            if (this.searchChatMe.allChatMe) {
                this.lstContacts = [];
                let bodyFirst = {
                    partner_id: this.partnerId,
                    limit: 20,
                    content: this.searchChatMe.room
                };
                let bodyString = JSON.stringify(bodyFirst);
                this.chatService.searchRoom(bodyString)
                    .then((integrate) => {
                        if (integrate) {
                            this.lstContacts = [];
                            if (integrate.errorCode === 200) {
                                this.getCustomerInfoFromRoom(integrate.data, 'offline');
                                _.each(integrate.data, (dd) => {
                                    let findInObject = _.find(this.lstContacts, (lst) => {
                                        return lst.name === dd.name;
                                    });
                                    if (!findInObject) {
                                        this.lstContacts.push(dd);
                                    }
                                });
                                this.sortMucRoom();
                            }
                            this.callChange();
                        }
                        this.loadingContact = false;
                    });
            } else {
                let bodyFirst = {
                    app_id: this.appId,
                    size_load: 20
                };
                this.lstContacts = [];
                bodyFirst = this.getParamToSearch(bodyFirst);
                this.getMucRoom(bodyFirst, 'offline', '');
            }
        }
        if (changes['chatFilterChanage'] && this.chatFilterChanage) {
            // this.resetSearch(); 
            this.isTextSearh = false;
            this.inputSearch = '';
            this.chatFilter = this.chatFilterChanage;
            let filterTrue = _.find(this.chatFilter, (cf) => {
                return cf.filter === true;
            });
            if (!filterTrue) {
                return false;
            }
            let bodyFirst = {
                app_id: this.appId,
                size_load: 20
            };
            this.lstContacts = [];
            bodyFirst = this.getParamToSearch(bodyFirst);
            // console_bk.log('FILTERBYFILTERCHANGE ' + JSON.stringify(this.chatFilter));
            this.getMucRoom(bodyFirst, 'offline', '');
        }
        if (changes['listTags'] && this.listTags && !this.tagsString) {
            _.each(this.listTags, (tag) => {
                // console_bk.log('TAGITEM ' + JSON.stringify(tag));
                tag.opacity = 0.3;
            });
            this.callChange();
        }
    }

    public registerEmit() {
        EmitterService.get(this.emitFocusRoom).subscribe((data: any) => {
            // console_bk.log('dataaaa emit ' + JSON.stringify(data));
            let openRoom = _.find(this.lstContacts, (contact) => {
                return contact.name === data.data.p;
            });
            // console_bk.log('LISTROM ' + JSON.stringify(openRoom));
            if (openRoom) {
                if (!openRoom.is_open) {
                    this.openChat(openRoom);
                }
            } else {

            }
        });
    }

    public openFromRoute() {
        if (this.roomFocus) {
            let openRoom = _.find(this.lstContacts, (contact) => {
                return contact.name === this.roomFocus;
            });
            // console_bk.log('LISTROM ' + JSON.stringify(openRoom));
            if (openRoom) {
                if (!openRoom.is_open) {
                    this.openChat(openRoom);
                }
            }
        }
    }
    public changeInfoContact() {

        _.each(this.lstContacts, (contact) => {
            if (contact.creater === this.userinfoChange.customer_username) {
                contact.cus_name = this.userinfoChange.name;
            }
        });
        this.callChange();
    }
    public emitFromRoomAction() {
        if (this.emitFromRoom.type === 'detectphonenumber') {
            let room = this.emitFromRoom.data.room;
            let contact = _.find(this.lstContacts, (con) => {
                return con.name === room;
            });
            if (contact) {
                contact.is_phone_number = 1;
                this.callChange();
            }
        } else if (this.emitFromRoom.type === 'deleteContact') {
            let room = this.emitFromRoom.data.room;
            // console_bk.log('ROMMMMMMMM ' + room);
            this.lstContacts = _.filter(this.lstContacts, (cont) => {
                // console_bk.log('CUSNAME ' + cont.name);
                return cont.name !== room;
            });

            this.callChange();
        }
    }
    public onMessageRawReceive() {
        // console.log('FFFFFFFFFFFFFFFFFFF ' + this.rawReceive);
        let x2js = new X2JS();
        let jsonObj = x2js.xml_str2json(this.rawReceive);
        if (jsonObj.message) {
            let message = jsonObj.message;
            let broadcast = message._broadcast;
            let subtype = message._subtype;
            let msgtype = message._msgtype;
            if (msgtype && msgtype === 'remarketing') {
                return false;
            }

            if (broadcast) {
                this.handleBroadCastMessage(message);
                return false;
            }
            if (message.body) {
                this.handleMessageBody(message, this.rawReceive);
            } else if (message.frontendstructure) {
                this.handleMessageStruct(message, this.rawReceive);
            } else if (message.fbcomment) {
                this.handleMessageComment(message, this.rawReceive);
            } else if (message.broadcast) {
                this.handleBroadcastXmpp(message);
            }

        } else if (jsonObj.iq) {
            // console_bk.log('GOI TIN IQ');
        } else if (jsonObj.presence) {
            // console_bk.log('PRESENCE');
        }

    }

    public handleBroadcastXmpp(message) {
        let objectBroad = JSON.parse(Base64.decode(message.broadcast));
        let from = message._from;
        from = this.chatService.getNodeChat(from);
        if (objectBroad.type === 'forwardChat') {
            if (objectBroad.member === this.usernameChat.username) {
                let roomOb = _.find(this.lstContacts, (cont) => {
                    return cont.name === from;
                });
                if (roomOb) {
                    // hien thi thong bao
                    this.showNotification(message);
                    // console_bk.log('HIEN THI THONG BAO');
                } else {
                    // console_bk.log('HIEN THI THONG BAO them room');
                    let body = {
                        app_id: this.appId,
                        appid: this.appId,
                        name: from
                    };
                    this.getMucRoom(body, 'online', message);
                }
            } else {
                let roomOb = _.find(this.lstContacts, (cont) => {
                    return cont.name === from;
                });
                if (roomOb) {
                    // xoa room
                    this.lstContacts = _.reject(this.lstContacts, (obj) => {
                        return obj.name === from;
                    });
                    /* if (this.openRoomId === from) {

                    } */
                }
            }
        } else if (objectBroad.type === 'collapse') {
            let roomOb = _.find(this.lstContacts, (cont) => {
                return cont.name === from;
            });
            if (roomOb) {
                // console_bk.log('ROOMNAYDANG HOAT DONG NHE ' + JSON.stringify(objectBroad));
                if (objectBroad.data.status === 'show') {
                    roomOb.broadcastStatus = 'show';
                } else {
                    roomOb.broadcastStatus = 'hide';
                }
            }
        } else if (objectBroad.type === 'visible') {
            let roomOb = _.find(this.lstContacts, (cont) => {
                return cont.name === from;
            });
            if (roomOb) {
                // console_bk.log('ROOMNAYDANG HOAT DONG NHE ' + JSON.stringify(objectBroad));
                if (objectBroad.data.status === 'focus') {
                    roomOb.broadcastStatus = 'focus';
                } else {
                    roomOb.broadcastStatus = 'hidden';
                }
            }
        } else if (objectBroad.type === 'typing') {
            let roomOb = _.find(this.lstContacts, (cont) => {
                return cont.name === from;
            });
            if (roomOb) {
                // console_bk.log('ROOMNAYDANG HOAT DONG NHE ' + JSON.stringify(objectBroad));
                if (objectBroad.data.status === 'typing') {
                    roomOb.broadcastStatus = 'typing';
                    roomOb.typingmessage = objectBroad.data.content;
                } else {
                    roomOb.broadcastStatus = 'show';
                }
            }
        }
        this.callChange();

    }
    public handleBroadCastMessage(message) {
        // console_bk.log('MESSAGE BROADCAST ' + JSON.stringify(message));
        let member = message._member;
        let from = this.chatService.getNodeChat(message._from);
        let text = message.broadcast;
        let broadcast = message._broadcast;

        if (broadcast === 'pintags') {
            this.broadcastReceiveTagsEvent(text, from);
        } else if (broadcast === 'detectphonenumber') {
            this.broadcastDetectPhoneEvent(text, from);
        } else if (broadcast === 'broadcastonlineoffline') {
            this.broadcastOnlineOffline(text, from);
        }
    }
    public broadcastReceiveTagsEvent(text, room) {
        let jsonBase = JSON.parse(Base64.decode(text));
        let tags = jsonBase.data.tags;
        let contact = _.find(this.lstContacts, (con) => {
            return con.name === room;
        });
        if (contact) {
            contact.tags = tags;
            this.callChange();
        }
    }
    public broadcastDetectPhoneEvent(text, room) {
        let contact = _.find(this.lstContacts, (con) => {
            return con.name === room;
        });
        if (contact) {
            contact.is_phone_number = 1;
            this.callChange();
        }
    }
    public broadcastOnlineOffline(text, room) {
        let jsonObject = JSON.parse(Base64.decode(text));
        // console_bk.log('JSON>STING broadcastOnlineOffline ' + JSON.stringify(jsonObject) + ' room ' + room);
        let roomFind = _.find(this.lstContacts, (cont) => {
            return cont.name === room;
        });

        if (roomFind) {
            if (jsonObject.data.status === 'online') {
                roomFind.detectOnline = true;
            } else {
                roomFind.detectOnline = false;
            }
        }
    }
    public handleMessageBody(message, raw) {
        let timeSend = Date.now();
        let from = this.chatService.getNodeChat(message._from);
        let member = message._member;
        let contact = _.find(this.lstContacts, (con) => {
            return con.name === from;
        });
        // console_bk.log('CONTACT ' + JSON.stringify(contact));
        if (contact) {
            contact.last_message_content = raw;
            contact.last_message_time = timeSend;
            contact.broadcastStatus = 'show';
            if (this.chatService.checkIfCustomer(member, this.allAgent)) {
                contact.last_message_status = 0;
                this.showNotification(message);
            }
            this.sortMucRoom();
            this.callChange();
        } else {
            let body = {
                app_id: this.appId,
                appid: this.appId,
                name: from
            };
            this.getMucRoom(body, 'online', message);
        }

    }
    public handleMessageStruct(message, raw) {
        let timeSend = Date.now();
        let from = this.chatService.getNodeChat(message._from);
        let member = message._member;
        let contact = _.each(this.lstContacts, (con) => {
            return con.name === from;
        });
        if (contact) {
            contact.last_message_content = raw;
            contact.last_message_time = timeSend;
            contact.broadcastStatus = 'show';
            if (this.chatService.checkIfCustomer(member, this.allAgent)) {
                contact.last_message_status = 0;
                this.showNotification(message);
            }
            this.sortMucRoom();
            this.callChange();
        } else {
            let body = {
                app_id: this.appId,
                appid: this.appId,
                name: from
            };
            this.getMucRoom(body, 'online', message);
        }
    }
    public handleMessageComment(message, raw) {
        let timeSend = Date.now();
        let from = this.chatService.getNodeChat(message._from);
        let member = message._member;
        let contact = _.find(this.lstContacts, (con) => {
            return con.name === from;
        });
        // console_bk.log('CONTACTHEREEE ' + JSON.stringify(contact) + ' FROM ' + from);
        if (contact) {
            contact.last_message_content = raw;
            contact.last_message_time = timeSend;
            if (this.chatService.checkIfCustomer(member, this.allAgent)) {
                contact.last_message_status = 0;
                this.showNotification(message);
            }
            this.sortMucRoom();
            this.callChange();
        } else {
            let body = {
                app_id: this.appId,
                appid: this.appId,
                name: from
            };
            this.getMucRoom(body, 'online', message);
        }
    }

    public getMucRoom(body, type, message) {
        let timeSend = Date.now();
        if (type === 'online') {
            this.loadingContact = false;
        } else {
            this.loadingContact = true;
        }
        this.chatService.getMucRoom(body)
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('MUCROOM ' + JSON.stringify(integrate));
                    if (integrate.errorCode === 200) {
                        this.getCustomerInfoFromRoom(integrate.data, type);
                        _.each(integrate.data, (dd) => {
                            let findInObject = _.find(this.lstContacts, (lst) => {
                                return lst.name === dd.name;
                            });
                            if (!findInObject) {
                                if (type === 'online') {
                                    dd.last_message_time = timeSend;
                                    dd.broadcastStatus = 'show';
                                } else {
                                    dd.last_message_time = timeSend - (integrate.timereceive - dd.last_message_time);
                                }
                                this.checkRoomByFilter(dd, type, message);
                            }
                        });

                    }
                    this.loadingContact = false;
                    this.callChange();
                }
            });
    }
    public checkRoomByFilter(room, type, message) {

        let filterAll = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterAll';
        });
        let filterUnread = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterUnread';
        });
        let filterComment = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterComment';
        });
        let filterChat = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterChat';
        });
        let filterDontUn = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterDontUn';
        });
        let filterForward = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterForward';
        });
        let filterPhone = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterPhone';
        });
        let filterMe = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterMe';
        });
        // console_bk.log('CHATFILTẺR  ' + JSON.stringify(this.chatFilter));
        if (filterUnread.filter) {
            if (room.last_message_status === 0 || room.last_message_status === '0') {
                // return false;
            } else {
                return false;
            }
        }
        if (filterComment.filter) {
            if (room.entry !== 'fbcomment') {
                return false;
            }
        }
        if (filterChat.filter) {
            if (room.entry !== 'groupchat') {
                return false;
            }
        }
        if (filterDontUn.filter) {
            if (room.need_support !== true) {
                return false;
            }
        }
        if (filterPhone.filter) {
            if (room.is_phone_number !== '1') {
                return false;
            }
        }
        if (filterForward.filter) {
            if (room.member_forwarded !== this.usernameChat.username) {
                return false;
            }
        }
        if (filterMe.filter) {
            if (room.supporter !== this.usernameChat.username) {
                return false;
            }
        }
        // console_bk.log('CHATFILTERMUCROOM ' + JSON.stringify(this.chatFilter));
        if (type === 'online') {
            this.showNotification(message);
        }
        this.lstContacts.push(room);
        this.sortMucRoom();
        this.callChange();
    }
    public getMucRoomPrivateReply(body) {
        this.chatService.getMucRoom(body)
            .then((integrate) => {
                if (integrate) {
                    if (integrate.errorCode === 200) {
                        this.getCustomerInfoFromRoom(integrate.data, 'offline');
                        let dd = integrate.data[0];
                        // console_bk.log('HEYYYYYYYYYYY ' + JSON.stringify(dd));
                        let findInObject = _.find(this.lstContacts, (lst) => {
                            return lst.name === dd.name;
                        });
                        if (!findInObject) {
                            this.lstContacts.push(dd);
                            this.openChat(dd);
                        }
                        this.sortMucRoom();
                    }
                    this.callChange();
                }
            });
    }
    public getCustomerInfoFromRoom(roomList, type) {
        this.chatService.getCustomerInfoFromListRoom(roomList)
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('USERSLIST ' + JSON.stringify(integrate));
                    _.each(integrate, (userInfo) => {
                        let contact = _.find(this.lstContacts, (lstContact) => {
                            return lstContact.name === userInfo.name;
                        });
                        if (contact) {
                            //supporter here
                            // console_bk.log('COntact ' + JSON.stringify(contact));
                            let supporter = _.find(this.allAgent, (agent) => {
                                return agent.username_chat === contact.supporter;
                            });
                            contact.supporterinfo = supporter;
                            // console_bk.log('Allagent ' + JSON.stringify(contact.supporterinfo));
                            if (userInfo.user) {
                                if (this.chatService.checkUserIsFacebook(userInfo.name)) {
                                    if (userInfo.user.name) {
                                        contact.cus_name = userInfo.user.name ? userInfo.user.name : 'Facebook User';
                                        contact.cus_avatar = userInfo.user.avatar_url ? userInfo.user.avatar_url : undefined;
                                        contact.type_user = 'facebook';
                                    } else {
                                        let bodyInfo = {
                                            appId: this.appId,
                                            creater: userInfo.creater
                                        };
                                        this.chatService.getCustomerOrUpdateInfo(JSON.stringify(bodyInfo))
                                            .then((infoUser) => {
                                                if (infoUser) {
                                                    contact.cus_name = infoUser.name ? infoUser.name : 'Facebook User';
                                                    contact.cus_avatar = infoUser.avatar_url ? infoUser.avatar_url : undefined;
                                                    contact.type_user = 'facebook';
                                                }
                                            });
                                    }
                                } else {
                                    contact.cus_name = userInfo.user.name ? userInfo.user.name : userInfo.user.time_zone ? userInfo.user.time_zone : 'User';
                                    contact.type_user = 'website';
                                }
                            } else {
                                contact.cus_name = 'User';
                            }

                            if (userInfo.user) {
                                let userHave = _.find(this.allCustomerInfo, (customer) => {
                                    return customer.customer_username === userInfo.user.customer_username;
                                });
                                if (!userHave) {
                                    this.allCustomerInfo.push(userInfo.user);
                                }
                            }
                        }
                    });
                    let emitbody = {
                        type: 'allCustomerInfo',
                        data: this.allCustomerInfo
                    };
                    // console_bk.log('LISTCUSINFO ' + JSON.stringify(this.allCustomerInfo));
                    this.contactClick.emit(emitbody);
                    this.callChange();
                }
            });
    }

    public sortMucRoom() {
        this.lstContacts = _.sortBy(this.lstContacts, (room) => {
            return -room.last_message_time;
        });
        this.openFromRoute();
        this.callChange();
    }

    public getParamToSearch(object: any) {
        if (this.tagsString) {
            object.tags = this.tagsString;
        }
        let filterAll = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterAll';
        });
        let filterUnread = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterUnread';
        });
        let filterComment = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterComment';
        });
        let filterChat = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterChat';
        });
        let filterDontUn = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterDontUn';
        });
        let filterForward = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterForward';
        });
        let filterPhone = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterPhone';
        });
        let filterMe = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterMe';
        });
        if (filterAll.filter) {
            return object;
        }
        if (filterUnread.filter) {
            object.status_read = '0';
        }
        if (filterComment.filter) {
            object.type = 'fbcomment';
        }
        if (filterChat.filter) {
            object.type = 'groupchat';
        }
        if (filterDontUn.filter) {
            object.need_support = true;
        }
        if (filterPhone.filter) {
            object.is_phone_number = '1';
        }
        if (filterForward.filter) {
            object.member_forwarded = this.usernameChat.username;
        }
        if (filterMe.filter) {
            object.supporter = this.usernameChat.username;
        }
        return object;
    };
    public contactScroll(event) {
        let $div = $(event.target);
        let scrollH = $div[0].scrollHeight - $div.scrollTop();
        let fixed = $div.outerHeight();
        scrollH = Math.round(scrollH);
        fixed = Math.round(fixed);
        console.log('SCROLLING ' + fixed + ' ' + scrollH);
        if (scrollH - fixed < 30) {
            console.log('SCroll bottom ');
            if (this.isTextSearh) {
                return;
            }
            let roomLast = _.last(this.lstContacts);
            console.log('SCroll bottom 1');
            if (!roomLast) {
                return false;
            }
            console.log('LOAD');
            let unixtime = roomLast.last_message_time;
            let bodyFirst = {
                app_id: this.appId,
                size_load: 20,
                time: unixtime
            };
            bodyFirst = this.getParamToSearch(bodyFirst);
            this.getMucRoom(bodyFirst, 'offline', '');
        }
    }

    public onKeySearch(event) {
        // this.resetSearch();
        // console_bk.log('ENTERKEY ' + this.inputSearch);
        if (this.inputSearch.trim() === '') {
            this.isTextSearh = false;
            this.loadingContact = false;
            let bodyFirst = {
                app_id: this.appId,
                size_load: 25,
            };
            bodyFirst = this.getParamToSearch(bodyFirst);
            this.getMucRoom(bodyFirst, 'offline', '');
            return;
        }
        this.lstContacts = [];
        this.isTextSearh = true;
        this.loadingContact = true;
        let body = {
            partner_id: this.partnerId,
            content: this.inputSearch,
            limit: 30
        };
        let bodyString = JSON.stringify(body);
        this.chatService.searchRoom(bodyString)
            .then((integrate) => {
                if (integrate) {
                    this.loadingContact = false;
                    this.callChange();
                    this.lstContacts = [];
                    if (integrate.errorCode === 200) {
                        this.getCustomerInfoFromRoom(integrate.data, 'offline');
                        _.each(integrate.data, (dd) => {
                            let findInObject = _.find(this.lstContacts, (lst) => {
                                return lst.name === dd.name;
                            });
                            if (!findInObject) {
                                this.checkRoomByFilter(dd, 'offline', '');
                            }
                        });
                        this.sortMucRoom();
                    }
                    this.callChange();
                }
                this.loadingContact = false;
            });
    }

    public markAllReaded() {
        let parameQuery = {
            app_id: this.appId
        };
        parameQuery = this.getParamToSearch(parameQuery);
        let body = {
            app_id: this.appId,
            filter: parameQuery,
            update: {
                last_message_status: 1
            }
        };
        let bodyString = JSON.stringify(body);
        this.chatService.updateRoomByFilter(bodyString)
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('CAP NHAT THANH CONG DA DOC');
                    _.each(this.lstContacts, (contact) => {
                        contact.last_message_status = 1;
                    });
                    this.callChange();
                }
                this.loadingContact = false;
            });
    }
    public markAllSupported() {
        let parameQuery = {
            app_id: this.appId
        };
        parameQuery = this.getParamToSearch(parameQuery);
        let body = {
            app_id: this.appId,
            filter: parameQuery,
            update: {
                last_message_status: 1,
                need_support: false
            }
        };
        let bodyString = JSON.stringify(body);
        this.chatService.updateRoomByFilter(bodyString)
            .then((integrate) => {
                if (integrate) {
                    // console_bk.log('CAP NHAT THANH CONG DA HO TRO');
                    _.each(this.lstContacts, (contact) => {
                        contact.need_support = false;
                        contact.last_message_status = 1;
                    });
                    this.callChange();
                }
                this.loadingContact = false;
            });
    }

    public searchTags(tag) {
        tag = _.find(this.listTags, (tagLst) => {
            return tagLst.id === tag.id;
        });
        this.lstContacts = [];
        if (tag.opacity === 0.3) {
            _.each(this.listTags, (tagT) => {
                tagT.opacity = 0.3;
            });
            tag.opacity = 1;
            // console_bk.log('TAGSS ' + tag.id);
            this.tagsString = tag.id;
            let bodyFirst = {
                app_id: this.appId,
                size_load: 20,
                tags: this.tagsString
            };

            bodyFirst = this.getParamToSearch(bodyFirst);
            this.getMucRoom(bodyFirst, 'offline', '');
            this.callChange();
        } else {
            this.tagsString = undefined;
            tag.opacity = 0.3;
            let bodyFirst = {
                app_id: this.appId,
                size_load: 20,
            };
            bodyFirst = this.getParamToSearch(bodyFirst);
            this.getMucRoom(bodyFirst, 'offline', '');
            this.callChange();
        }
    }
    public resetSearch() {
        _.each(this.listTags, (tagT) => {
            tagT.opacity = 0.3;
        });

    }

    public openChatRoute(contact) {
        this.roomFocus = contact.name;
        let navi = '@' + Base64.encode(this.roomFocus);
        let navigationExtras: NavigationExtras = {
            queryParams: { p: navi },
            fragment: 'hana'
        };
        let openchat = '/' + this.partnerId + '/messages';
        this.router.navigate([openchat], navigationExtras);
    }

    public openChat(contact) {
        _.each(this.lstContacts, (conta) => {
            conta.is_open = false;
        });
        contact.is_open = true;
        let body = {
            muc_room_id: contact.name,
            last_message_status: 1
        };
        this.chatService.updateMucRoom(JSON.stringify(body))
            .then((integrate) => { });
        let emitbody = {
            type: 'contactClick',
            data: contact
        };
        contact.last_message_status = 1;
        this.contactClick.emit(emitbody);
        this.callChange();
    }

    public showNotification(message) {

        // console_bk.log('NTIFICAT ' + document[this.hidden]);
        let hidden = document[this.hidden];
        if (!hidden) {
            return false;
        }
        let msgNotifi = 'Bạn có tin nhắn';
        if (message.body) {
            let bodyJson = JSON.parse(Base64.decode(message.body));
            if (bodyJson.type === 'text') {
                msgNotifi = bodyJson.body;
            } else if (bodyJson.type === 'images') {
                msgNotifi = 'Bạn được gửi một hình ảnh';
            }
        } else if (message.broadcast) {
            msgNotifi = 'Bạn được yêu cầu trả lời cuộc chat';
        }
        let n = new Notification('Bạn có tin nhắn mới nhé', {
            body: msgNotifi,
            lang: 'vi',
            icon: 'https://chat.hana.ai/assets/img/chat/logo.png'
        });
        $(n).on('click', (e) => {
            window.focus();
            e.target.close();
        });
        setTimeout(n.close.bind(n), 10000);

        this.playSound();
    }

    public playSound() {
        // console_bk.log('playsource ' + this.configGateway.play_sound);
        if (this.configGateway.play_sound !== '1') {
            return false;
        }
        // console_bk.log('playsource ' + this.configGateway.play_sound);
        let audio;
        if (typeof Audio !== 'undefined') {
            // console_bk.log('playsource ' + this.configGateway.play_sound);
            audio = new Audio(this.configGateway.sound_bk_url + '.ogg');
            if (audio.canPlayType('/audio/ogg')) {
                audio.play();
            } else {
                audio = new Audio(this.configGateway.sound_bk_url + '.mp3');
                audio.play();
            }
        }
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
