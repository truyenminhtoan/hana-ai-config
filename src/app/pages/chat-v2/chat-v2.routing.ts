import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ChatV2Component } from './chat-v2.component';

const routes: Routes = [
  { path: '', component: ChatV2Component },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
