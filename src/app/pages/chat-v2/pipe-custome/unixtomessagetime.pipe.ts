import { Pipe, PipeTransform } from '@angular/core';
declare var $: any;
declare var _: any;
import * as moment from 'moment';
declare var Base64: any;

@Pipe({ name: 'unixtomessagetime' })
export class UnixToMessageTimeString implements PipeTransform {

  public transform(from: number, now: number): any {
    // let unixtime = now - from;
    // let time = Date.now() - unixtime;
    return moment(from).format('DD/MM/YYYY HH:mm:ss');
  }

}