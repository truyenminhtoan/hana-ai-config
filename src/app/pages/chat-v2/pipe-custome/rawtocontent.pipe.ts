import { Pipe, PipeTransform } from '@angular/core';
import { EditMenuDialogComponent } from '../../remarketing-v2/components/setting-menus/components/edit-menu/edit-menu.components';
declare var $: any;
declare var _: any;
declare var Base64: any;
declare var X2JS: any;


@Pipe({ name: 'rawtocontent' })
export class RawToContentPipe implements PipeTransform {

  private entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };

  public transform(value: string, length: number): any {
    if (!value) {
      return 'Tin nhắn';
    }
    let x2js = new X2JS();
    let jsonObj;
    try {
      jsonObj = x2js.xml_str2json(value);
    } catch (e) {
      return 'Tin nhắn';
    }
    // console_bk.log('TIN NHĂN ' + JSON.stringify(jsonObj));

    let message = jsonObj.message;
    if (!message) {
      return 'tin nhắn';
    }
    if (message.frontendstructure) {
      let body = message.frontendstructure;
      let textjson;
      try {
        textjson = JSON.parse(Base64.decode(body));
      } catch (e) {
        return 'Tin nhắn';
      }
      _.each(textjson, (attachment, index) => {
        if (attachment.message.text) {
          let text = this.escapeHtmlHana(attachment.message.text);
          return text;
        } else {
          return 'Tin nhắn';
        }
      });
    } else if (message.body) {
      let body = message.body;
      let textjson = JSON.parse(Base64.decode(body));
      if (textjson.type === 'text') {
        let text = this.escapeHtmlHana(textjson.body);
        return text;
      } else if (textjson.type === 'image') {
        return 'Hình ảnh';
      } else if (textjson.type === 'attachment') {
        return 'Tệp';
      }
    } else if (message.fbcomment) {
      let body = message.fbcomment;
      let textjson = JSON.parse(Base64.decode(body));
      let text = this.escapeHtmlHana(textjson.message);
      return text;
    } else {
      return 'Tin nhắn khác';
    }
  }

  public escapeHtmlHana(value) {
    return String(value).replace(/[&<>"'`=\/]/g, (s) => {
      return this.entityMap[s];
    });
  }


}