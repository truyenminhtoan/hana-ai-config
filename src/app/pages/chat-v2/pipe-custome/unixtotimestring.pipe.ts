import { Pipe, PipeTransform } from '@angular/core';
declare var $: any;
declare var _: any;
import * as moment from 'moment';
declare var Base64: any;

@Pipe({ name: 'unixtotimestring' })
export class UnixToTimeString implements PipeTransform {

  public transform(value: string, format: string): any {
    let unixtime = parseInt(value, 10);

    if (moment(unixtime).format('DD/MM/YYYY') === moment(new Date()).format('DD/MM/YYYY')) {
      format = 'hour';
    } else {
      format = 'day';
    }

    if (format === 'hour') {
      return moment(unixtime).format('HH:mm');
    } else if (format === 'day') {
      return moment(unixtime).format('HH:mm DD/MM ');
    } else {
      return moment(unixtime).format('DD/MM/YYYY HH:mm');
    }
  }

}