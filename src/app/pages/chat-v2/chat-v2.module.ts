// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { ChatV2Component } from './chat-v2.component';
import { routing } from './chat-v2.routing';
import { CommonModule } from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgaModule } from '../../themes/nga.module';
import { ApplicationService } from '../../_services/application.service';
import { ColorPickerModule, ColorPickerService } from 'angular2-color-picker';
import { ValidationService } from '../../_services/ValidationService';
import { ConfigService } from '../../_services/config.service';
import { NotificationService } from '../../_services/notification.service';
import { ChatService } from '../../_services/chat.service';
import { ChatFilterComponent } from './chat-filter/chat-filter.component';
import { ChatContactsComponent } from './chat-contacts/chat-contacts.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { RawToContentPipe } from './pipe-custome/rawtocontent.pipe';
import { UnixToTimeString } from './pipe-custome/unixtotimestring.pipe';
import { UnixToMessageTimeString } from './pipe-custome/unixtomessagetime.pipe';
import { InviteHanaDialogComponent } from './chat-room/components/invite-hana-dialog/invite-hana-dialog.component';
import { ConfirmDialogModule, DragDropModule } from 'primeng/primeng';

import { MdProgressSpinnerModule, MdDialogModule, MdAutocompleteModule, MdPaginatorModule, MatChipsModule, MatSlideToggleModule, MatButtonToggleModule, MatPaginatorModule, MatButtonModule, MatDialogModule, MatInputModule, MatAutocompleteModule, MatProgressSpinnerModule, MatIconModule, MatSelectModule, MatDatepickerModule, MdNativeDateModule, MdTooltipModule, MdChipsModule } from '@angular/material';
import { ForwardChatDialogComponent } from './chat-room/components/forward-chat-dialog/forward-chat-dialog.component';
import { QuickReplyDialogComponent } from './chat-room/components/quick-reply-dialog/quick-reply-dialog.component';
import { DeleteCommentDialogComponent } from './chat-room/components/delete-comment-dialog/delete-comment-dialog.component';
import { PrivateReplyDialogComponent } from './chat-room/components/private-reply-dialog/private-reply-dialog.component';
import { ChatCustomerComponent } from './chat-customer/chat-customer.component';
import { AddDemandDialogComponent } from './chat-customer/components/add-demand-dialog/add-demand-dialog.component';
import { TagInputModule } from 'ng2-tag-input';
import { AddReportDialogComponent } from './chat-customer/components/add-report-dialog/add-report-dialog.component';
import { AddTrainingDialogComponent } from './chat-customer/components/add-training-dialog/add-training-dialog.component';
import { IntentService, BlocksService } from '../../_services/index';

@NgModule({
    imports: [
        CommonModule,
        AngularFormsModule,
        ColorPickerModule,
        NgaModule,
        ConfirmDialogModule,
        routing,
        MatDialogModule,
        MdDialogModule,
        TagInputModule,
        MdTooltipModule,
        MdChipsModule,
        DragDropModule
    ],
    declarations: [
        ChatV2Component,
        ChatFilterComponent,
        ChatContactsComponent,
        ChatRoomComponent,
        ChatCustomerComponent,
        InviteHanaDialogComponent,
        ForwardChatDialogComponent,
        QuickReplyDialogComponent,
        AddReportDialogComponent,
        AddDemandDialogComponent,
        AddTrainingDialogComponent,
        DeleteCommentDialogComponent,
        PrivateReplyDialogComponent,
        RawToContentPipe,
        UnixToTimeString,
        UnixToMessageTimeString
    ],
    exports: [
        ChatV2Component,
        ChatFilterComponent,
        ChatContactsComponent,
        ChatRoomComponent,
        ChatCustomerComponent,
        InviteHanaDialogComponent,
        AddReportDialogComponent,
        ForwardChatDialogComponent,
        QuickReplyDialogComponent,
        DeleteCommentDialogComponent,
        AddDemandDialogComponent,
        AddTrainingDialogComponent,
        PrivateReplyDialogComponent
    ],
    providers: [
        ApplicationService,
        ValidationService,
        ColorPickerService,
        ConfigService,
        NotificationService,
        ChatService,
        IntentService,
        BlocksService
    ],
    bootstrap: [
        InviteHanaDialogComponent,
        ForwardChatDialogComponent,
        QuickReplyDialogComponent,
        DeleteCommentDialogComponent,
        PrivateReplyDialogComponent,
        AddDemandDialogComponent,
        AddTrainingDialogComponent,
        AddReportDialogComponent
    ]
})
export class ChatV2Module {

}
