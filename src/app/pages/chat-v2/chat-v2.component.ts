import { Component, AfterViewInit, OnInit, ElementRef, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';
import { ConfigService } from '../../_services/config.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApplicationService } from '../../_services/application.service';
import { NotificationService } from '../../_services/notification.service';
import { ChatService } from '../../_services/chat.service';

declare var swal: any;
declare var $: any;
declare var _: any;
declare var require: any;
declare var window: any;
declare var hanaapi: any;
declare var document: any;
import * as moment from 'moment';
import { EmitterService } from '../../_services/index';
import { setTimeout } from 'timers';

@Component({
  selector: 'chat-v2',
  templateUrl: 'chat-v2.component.html',
  styleUrls: ['./assets/css/material-dashboard.css', 'chat-v2.component.scss', './assets/css/chat.css']
})
export class ChatV2Component implements OnInit, AfterViewInit, OnDestroy {
  // tslint:disable-next-line:max-line-length
  private currentUser: any;
  private partnerId: any;
  private appId: any;
  private backendUserId: any;
  private application: any;
  private configGateway: any;
  private allAgent: any;
  private allCustomerInfo: any;
  private usernameChat: any;
  private chatConfig: any;
  private chatFilter: any;
  private chatFilterChanage: any;
  private listTags = [];
  private subscription: any;
  private isOpenChat: any;
  private chatOpenObject: any;
  private searchChatMe: any;
  private searchChatMeChanage: any;
  private templateReply: any;
  private rawReceive: any;
  private agentActive: any;
  private openRoomId: any;
  private sortMucRoomStatus: boolean;
  private emitFromRoom: any;
  private userinfoChange: any;
  private roleUser: any;
  private hidden: any;
  private visibilityChange: any;
  private isDestroy: boolean;

  private connectStatus: string;
  private imhere: string;
  private that: any;
  private startTimeLogin: number;
  private startFirst: any;

  private sub: any;
  private dateAlertPayment = 7;
  private roomFocus: string;

  private emitTraining: string = 'EMITRAININGCHAT';
  private emitFocusRoom = 'ROUTE_FOCUS_ROOM';

  private jid: any;
  constructor(private ngZone: NgZone, private elementRef: ElementRef, private applicationService: ApplicationService,
    private router: Router, private notificationService: NotificationService, private route: ActivatedRoute, private serviceConfig: ConfigService, private cdRef: ChangeDetectorRef,
    private chatService: ChatService) {
    $('body').addClass('sidebar-mini');
    this.sub = this.route.params.subscribe((params) => {
      this.partnerId = params['id'];
    });
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      // // console_bk.log('APPUSINGNN ' + JSON.stringify(currentUser) + ' apppp ' + this.partnerId);
      let app = _.find(currentUser.apps, (appU) => {
        return parseInt(this.partnerId, 10) === appU.id;
      });
      // // console_bk.log('APPUSINGNN ' + JSON.stringify(app));
      this.currentUser = currentUser;
      currentUser.version = app.version;
      currentUser.partner_id = app.id;
      currentUser.app_using = app;
      if (currentUser.role_id !== 1) {
        let user = app.backendPartnerUsers.filter((item) => item.user_id === currentUser.id)[0];
        if (user) {
          currentUser.role_id = user.role_id;
        }
      }
      this.currentUser = currentUser;
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      if (localStorage.getItem('apphanausing')) {
        // // console_bk.log('ABCDEFFFFFFFFFFFFFFFFFFFFF ');
        let preUsing = JSON.parse(localStorage.getItem('apphanausing'));
        // console_bk.log('ABCDEFFFFFFFFFFFFFFFFFFFFF ' + app.id + ' pre ' + preUsing.id);
        if (app.id === preUsing.id) {
          // console_bk.log('');
        } else {
          localStorage.setItem('apphanausing', JSON.stringify(app));
          this.getToken(app);
          // this.getTryUsing();
        }

      } else {
        this.getToken(app);
        localStorage.setItem('apphanausing', JSON.stringify(app));
      }
      this.getTryUsing();
      // console_bk.log('CURRENTUSER ' + JSON.stringify(this.currentUser.username));
      // console_bk.log('CURRENTUSER ' + JSON.stringify(this.currentUser));

    }
    // subscribe to router event

    this.chatFilter = [
      { key: 'filterAll', show: true, filter: false, alert: false },
      { key: 'filterUnread', show: true, filter: false, alert: false },
      { key: 'filterComment', show: true, filter: false, alert: false },
      { key: 'filterChat', show: true, filter: false, alert: false },
      { key: 'filterDontUn', show: true, filter: false, alert: false },
      { key: 'filterPhone', show: true, filter: false, alert: false },
      { key: 'filterForward', show: true, filter: false, alert: false },
      { key: 'filterMe', show: true, filter: false, alert: false },
    ];
    if (!this.listTags) {
      this.listTags = [];
    }

  }

  public getToken(app) {
    this.applicationService.getTokenByPartnerId(this.currentUser.full_name, this.currentUser.username, app.id, app.version)
      .then((apps) => {
        // console_bk.log('');
      });
  }
  public ngAfterViewInit() {
    /* this.ngZone.run(() => {
      $.getScript('../../../assets/js/pushnotification/main.js');
    }); */
    this.checkLoged();
    $('.navbar-messages').css('border-bottom', '2px solid #e9604a');
    // console_bk.log('');
  }

  public getTryUsing() {
    // // console_bk.log('UERRRRRR ' + JSON.stringify(this.currentUser));

    let integrations = this.currentUser.app_using.integrations;
    if (!integrations) {
      return false;
    }
    let fbInte = _.find(integrations, (inte) => {
      return inte.gatewayId === 1;
    });
    if (fbInte) {
      if (fbInte.isExpire === 0) {
        let body = {
          page_id: fbInte.pageId,
          partner_id: this.currentUser.partner_id
        }
        this.applicationService.getTryUsing(body)
          .then((apps) => {

            // // console_bk.log('paypay ' + JSON.stringify(apps));
            this.currentUser.payment = apps.data;
            this.currentUser.paymentstatus = false;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
            this.checkPayment();
          });
      }
    }
  }

  public checkPayment() {
    let payment = this.currentUser.payment;
    if (payment) {
      /* if (this.currentUser.paymentstatus) {
        return false;
      } else {
        this.currentUser.paymentstatus = true;
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
      } */
      let currentDate = payment.current_day;
      let expireDate = payment.expiry_date;
      let momentCurrent = moment(currentDate).valueOf();
      let momentExpire = moment(expireDate).valueOf();
      let distance = momentExpire - momentCurrent;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      // console.log('PAYMENT '  + days);
      if (days < 0) {
        this.showDeadlineTrial();
      } else if (days <= this.dateAlertPayment) {
        this.showwarningTrial(days);
      }
    }
  }
  public focusIfHaveParam(params) {
    if (params) {
      let room = params.p;
      let body = {
        type: 'param',
        data: params
      };
      EmitterService.get(this.emitFocusRoom).emit(body);
    }
  }

  public showwarningTrial(num) {
    // console_bk.log('xóa id ' + id + ' APP ID ' + appid);
    this.currentUser.deadlined = false;
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
    swal({
      title: 'Bạn còn ' + num + ' ngày dùng thử gói "CAO CẤP"',
      html: '<p>Bạn có thể thanh toán ngay gói "CAO CẤP" bên dưới để tiếp tục sử dụng những tính năng nâng cao (Marketing, đào tạo theo bài post, đào tạo theo hashtag,...) và việc kinh doanh của bạn không bị gián đoạn.</p><p>Lưu ý: Bạn có thể quay lại sử dụng phiên bản miễn phí bất kỳ thời điểm nào bạn muốn.</p>',
      type: 'warning',
      showCancelButton: true,

      // imageUrl: '/assets/icon/hana-icon1.jpg',
      // imageWidth: 200,
      // imageHeight: 200,
      // animation: false,

      confirmButtonText: 'TIẾP TỤC GÓI TIÊU CHUẨN',
      confirmButtonClass: 'btn btn-warning',
      cancelButtonText: 'NÂNG LÊN GÓI CAO CẤP',
      cancelButtonClass: 'btn btn-success',
      buttonsStyling: false
    }).then(() => {
      // setTimeout(() => {
      //   $('#payment-tab').click();
      // }, 1000);
      // this.router.navigate(['/application-hana/application-config-detail/' + this.partnerId]);
    }, (dismiss) => {
      setTimeout(() => {
        $('#payment-tab').click();
      }, 1000);
      this.router.navigate(['/application-hana/application-config-detail/' + this.partnerId]);
    });
  }

  public showDeadlineTrial() {
    console.log('xóa id ' + this.currentUser.role_id  + ' APP ID ' + this.serviceConfig.ROLE_USER.admin);
    this.currentUser.deadlined = true;
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
    swal({
      title: 'Bạn đã hết thời gian dùng thử gói "CAO CẤP"',
      html: '<p>Bạn có thể thanh toán ngay gói "CAO CẤP" bên dưới để tiếp tục sử dụng những tính năng nâng cao (Marketing, đào tạo theo bài post, đào tạo theo hashtag,...) giúp mang lại nhiều lợi ích hơn cho việc kinh doanh của bạn.</p><p>Lưu ý: Bạn có thể quay lại sử dụng phiên bản miễn phí bất kỳ thời điểm nào bạn muốn.</p>',
      type: 'error',

      // imageUrl: '/assets/icon/hana-icon2.jpg',
      // imageWidth: 200,
      // imageHeight: 200,
      // animation: false,

      showCancelButton: true,
      confirmButtonText: 'TIẾP TỤC GÓI TIÊU CHUẨN',
      confirmButtonClass: 'btn btn-warning',
      cancelButtonText: 'NÂNG LÊN GÓI CAO CẤP',
      cancelButtonClass: 'btn btn-success',
      buttonsStyling: false
    }).then(() => {
      //
    }, (dismiss) => {
      // if (dismiss === 'cancel') {
      //   // console_bk.log('giữ nó');
      // }
      setTimeout(() => {
        $('#payment-tab').click();
      }, 1000);
      this.router.navigate(['/application-hana/application-config-detail/' + this.partnerId]);
    });
  }

  public ngOnInit() {


    // this.checkLoged();
    window.my = window.my || {};
    window.my.namespace = window.my.namespace || {};
    window.my.namespace.publicFunc = this.publicFunc.bind(this);
    window.my.namespace.publicFuncObject = this.publicFuncObject.bind(this);
    window.my.namespace.publicFuncAction = this.publicFuncAction.bind(this);
    window.my.namespace.publicFuncChat = this.publicFuncChat.bind(this);
    this.chatConfig = {
      hana_bosh: this.serviceConfig.HOST_CHAT
    };
    this.imhere = 'foreground';

    /* this.usernameChat = {
      user: '7a79f1d8-bcd8-4a2e-b046-dd666ea8295f',
      pass: 'fa1641da-3f61-418f-bacd-cebacf3e1bc0'
    };
    window.hanaapi.start(this.usernameChat); */

  }

  public ngOnDestroy(): void {
    this.isDestroy = true;
    $('body').removeAttr('style');
    $('#myStyle').remove();
    // if (window.converse.connection) {
    //   window.converse.actionglobal.logout();
    // }
    window.my.namespace.publicFunc = null;
    window.my.namespace.publicFuncObject = null;
    window.my.namespace.publicFuncAction = null;
    // window.my.namespace.publicFuncChat = null;
    this.imhere = 'background';
    window.hanaapi.connection.disconnect();
    this.cdRef.detach();
  }

  public checkLoged() {
    if (localStorage.getItem('currentUser') === 'undefined') {
      this.router.navigate(['/login-hana']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    if (this.currentUser.app_using) {
      // // console_bk.log('CURRENTUSER ' + JSON.stringify(this.currentUser.app_using));
      this.partnerId = this.currentUser.app_using.id;
      this.application = this.currentUser.app_using.applications[0];
      this.appId = this.application.app_id;
      this.backendUserId = this.currentUser.id;
      this.getRoleUser(this.currentUser.app_using);
      this.getUsernameChat();
      this.getAllAPI();
      this.checkWindowVisible();

      this.callChange();
    } else {
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    }
  }

  public getRoleUser(appUsing) {
    _.each(appUsing.backendPartnerUsers, (user) => {
      if (this.backendUserId === user.user_id) {
        this.roleUser = user.role_id;
        this.callChange();
        // console_bk.log('ROLEUSER ' + this.roleUser);
      }
    });
  }
  public getAllAPI() {
    this.getIntegrationGateWay();
    this.getAllAgent();
    this.getTags();
    this.getTemplateReply();
    this.getAgentActive();
  }
  public getUsernameChat() {
    let body = {
      partner: this.partnerId,
      backend_user_id: this.backendUserId
    };
    this.chatService.getChatUser(JSON.stringify(body))
      .then((integrate) => {
        if (integrate) {
          // console_bk.log('userchat ' + JSON.stringify(integrate));
          this.usernameChat = integrate.data[0];
          this.startTimeLogin = new Date().getTime();
          window.hanaapi.start(this.usernameChat, this.chatConfig);
          this.route.queryParams
            .filter((params) => params.p)
            .subscribe((params) => {
              this.roomFocus = params.p;
            });
          this.callChange();
        }
      });
  }

  public getTemplateReply() {
    let body = {
      partner_id: this.partnerId
    };
    this.chatService.getTemplateReply(JSON.stringify(body))
      .then((integrate) => {
        if (integrate) {
          this.templateReply = integrate.data;
          this.callChange();
        }
      });
  }
  public getIntegrationGateWay() {
    this.applicationService.getIntegrationByAppId(this.appId, 2)
      .then((integrate) => {
        if (integrate) {
          this.configGateway = integrate;
          this.callChange();
        }
      });
  }
  public getAllAgent() {
    this.chatService.getAllAgent(this.appId)
      .then((integrate) => {
        if (integrate) {
          // console_bk.log('USERSBD ' + JSON.stringify(integrate));
          if (integrate.errorCode === 200) {
            // console_bk.log('USERSBD ' + JSON.stringify(integrate));
            this.allAgent = integrate.data.users;
          }
          this.callChange();
        }
      });
  }
  public getTags() {
    this.applicationService.getAllTagsByPartnerId(this.partnerId)
      .then((tags) => {
        if (tags) {
          // console_bk.log('ALL TGS ' + JSON.stringify(tags));
          this.listTags = tags;
          this.callChange();
        }
      });
  }

  public getAgentActive() {
    this.applicationService.getAgentActiveByPartnerId(this.partnerId)
      .then((agent) => {
        if (agent) {
          // console_bk.log('AGENT ACTIVE ' + JSON.stringify(agent));
          // this.listTags = tags;
          this.agentActive = agent;
          this.callChange();
        }
      });
  }
  public publicFunc(subscription) {
    this.ngZone.run(() => this.privateFunc(subscription));
  }

  public privateFunc(subscription) {
    // console_bk.log('CALLLL OUT SITE ' + JSON.stringify(subscription));
    // console_bk.log('sfd ' + this.valueHeight + ' ' + this.valueWidth);
    let body = {
      push_subscription: subscription,
      partner_id: this.currentUser.app_using.id
    }
    this.applicationService.saveUserNotification(body)
      .then((config) => {
        if (config) { }
      });
  }

  public publicFuncObject(subscription) {
    this.ngZone.run(() => this.privateFuncObject(subscription));
  }

  public privateFuncObject(subscription) {
    // console_bk.log('CALLLL OUT SITE ' + JSON.stringify(subscription));
    if (subscription.status === 403) {
      localStorage.removeItem('currentUser');
      window.location.href = '/';
    }
  }

  public publicFuncAction(subscription) {
    this.ngZone.run(() => this.privateFuncAction(subscription));
  }
  public publicFuncChat(subscription) {
    this.ngZone.run(() => this.privateFuncChat(subscription));
  }

  public privateFuncChat(subscription) {
    // console_bk.log('CHAT MESSAGE ' + JSON.stringify(subscription));
    if (subscription.type === 'message') {
      this.exploreRawMsg(subscription.raw);
    } else if (subscription.type === 'statusconnection') {
      this.exploreStatusMsg(subscription.status);
    } else {
    }
  }

  public exploreRawMsg(raw) {
    // console_bk.log('EXPLORERAW' + raw);
    this.rawReceive = raw;
    this.callChange();
  }
  public exploreStatusMsg(status) {
    if (this.imhere !== 'foreground') {
      return false;
    }
    // console_bk.log('EXPLOREstatus' + status);
    if (status === 'connected') {
      this.jid = hanaapi.user.jid();
      this.connectStatus = 'connected';
      this.callChange();
      if (!this.startFirst) {
        this.startFirst = 1;
        let end = new Date().getTime();
        let range = end - this.startTimeLogin;
        let bodyLog = {
          app_id: this.appId,
          api_name: 'loginXmpp',
          time_request: range
        };
        this.chatService.createLogs(JSON.stringify(bodyLog)).then((integrate) => {
          if (integrate) {
            // console_bk.log('');
          }
        });
      }
      // console_bk.log('EXPLOREstatus' + this.connectStatus);
    } else if (status === 'disconnected') {
      this.callChange();
      // console_bk.log('disconnectedFIRST ' + this.connectStatus);
      if (this.connectStatus !== 'disconnected') {
        if (this.usernameChat) {
          window.hanaapi.start(this.usernameChat, this.chatConfig);
        }
      }
      this.connectStatus = 'disconnected';
    } else {
      this.connectStatus = status;
    }
  }

  public retryConnecting() {
    this.connectStatus = 'connecting';
    window.hanaapi.start(this.usernameChat, this.chatConfig);
    this.callChange();
  }
  public privateFuncAction(subscription) {
    if (subscription.type === 'addtag') {
      $('#createQuickTemplateModel').modal('show');
      // console_bk.log('ACTIONNNNNNNNNNN 1' + subscription.type);
      this.getTags();
      this.subscription = subscription;
    } else if (subscription.type === 'notificationmsg') {
      // console_bk.log('ACTIONNNNNNNNNNN 2' + subscription.type);
      if (subscription.data.type === 'success') {
        this.notificationService.showSuccessTimer(subscription.data.msg, 3000);
      } else {
        this.notificationService.showWarningTimer(subscription.data.msg, 3000);
      }
    } else if (subscription.type === 'showlog') {
      // console_bk.log('ACTIONNNNNNNNNNN 2' + subscription.type);
      let msgId = subscription.data.msgid;
      // // console_bk.log('HIEN THI O DAY NHE chi role postsale thoi ' + msgId );
      // // console_bk.log('HIEN THI O DAY NHE chi role postsale thoi ' + JSON.stringify(this.currentUser) );
      if (this.currentUser.app_using) {
        let backendUsers = this.currentUser.app_using.backendPartnerUsers;
        let idUser = this.currentUser.id;
        let UserRole = backendUsers.find((data: any) => { return idUser === data.user_id });
        let role = UserRole.role_id;
        // console_bk.log('HIEN THI O DAY NHE chi role postsale thoi ' + JSON.stringify(UserRole));
        if ((role === this.serviceConfig.ROLE_USER.syssupport) || (role === this.serviceConfig.ROLE_USER.sysadmin)) {
          window.open(
            this.serviceConfig.HOST_MY + '/chat_backend/getMessageById/' + msgId,
            '_blank'
          );
        }
      }
    }

  }

  public filterChange(chatFilter: any) {
    // console_bk.log('EMITCHANGE CHATV2 ' + JSON.stringify(chatFilter));
    this.chatFilterChanage = chatFilter;
    this.callChange();
    this.chatFilterChanage = undefined;
    this.callChange();
  }

  public contactClick(contact: any) {

    if (contact.type === 'contactClick') {
      this.isOpenChat = true;
      this.chatOpenObject = contact.data;
      this.callChange();
    } else if (contact.type === 'allCustomerInfo') {
      this.allCustomerInfo = contact.data;
      this.callChange();
    }

  }

  public emitChatRoom(emitRoom) {
    // console_bk.log('EMIT ROOM  ' + JSON.stringify(emitRoom));
    if (emitRoom.type === 'searchRoom') {
      emitRoom = emitRoom.data;
      this.searchChatMe = emitRoom;
      this.searchChatMeChanage = true;
      this.callChange();
      this.searchChatMeChanage = undefined;
      this.callChange();
    } else if (emitRoom.type === 'openChat') {
      // console_bk.log('OPENCHATNAY ' + emitRoom.data.room);
      this.openRoomId = emitRoom.data.room;
      this.callChange();
      this.openRoomId = undefined;
      this.callChange();
    } else if (emitRoom.type === 'sortmucroom') {
      this.sortMucRoomStatus = true;
      this.callChange();
      this.sortMucRoomStatus = false;
      this.callChange();
    } else if (emitRoom.type === 'detectphonenumber') {
      this.emitFromRoom = emitRoom;
      this.callChange();
      this.emitFromRoom = undefined;
      this.callChange();
    } else if (emitRoom.type === 'deleteContact') {
      this.emitFromRoom = emitRoom;
      this.callChange();
      this.emitFromRoom = undefined;
      this.callChange();
    }
  }

  public emitChatCustomer(emitRoom) {
    // console_bk.log('EMITUPDAE ' + JSON.stringify(emitRoom));
    if (emitRoom.type === 'updateinfo') {
      this.userinfoChange = emitRoom.data;
      this.callChange();
      this.userinfoChange = undefined;
      this.callChange();
    }
  }

  public checkWindowVisible() {
    // Set the name of the hidden property and the change event for visibility

    if (typeof document.hidden !== 'undefined') { // Opera 12.10 and Firefox 18 and later support 
      this.hidden = 'hidden';
      this.visibilityChange = 'visibilitychange';
    } else if (typeof document.msHidden !== 'undefined') {
      this.hidden = 'msHidden';
      this.visibilityChange = 'msvisibilitychange';
    } else if (typeof document.webkitHidden !== 'undefined') {
      this.hidden = 'webkitHidden';
      this.visibilityChange = 'webkitvisibilitychange';
    }

    // Warn if the browser doesn't support addEventListener or the Page Visibility API
    if (typeof document.addEventListener === 'undefined' || typeof document[this.hidden] === 'undefined') {
      // console_bk.log('This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.');
    } else {
      // Handle page visibility change   
      document.addEventListener(this.visibilityChange, this.handleVisibilityChange, false);

    }
  }

  public handleVisibilityChange() {
    // console_bk.log('ONCHANGE ' + this.hidden);
    if (this.hidden) {
      // console_bk.log('HIDDEND');
    } else {
      // console_bk.log('SHOWW');
    }
  }

  public callChange() {
    if (this.isDestroy) {
      return false;
    }
    this.cdRef.detectChanges();
  }
}
