import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';
import { NotificationService } from '../../../../../_services/notification.service';

declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;

@Component({
    selector: 'add-report-dialog',
    templateUrl: 'add-report-dialog.component.html',
    styleUrls: ['add-report-dialog.component.scss']
})
export class AddReportDialogComponent {
    private message: any;
    private title: string;

    private partnerId: string;
    private appId: string;
    private room: string;
    private customerInfo: any;
    private usernameChat: any;
    private demandName: string;
    private demandDetail: string;
    private chatOpenObject: any;
    private type: string;
    private report: any;

    private saveing: boolean;
    private contentReport: string;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<AddReportDialogComponent>, private chatService: ChatService, private notificationService: NotificationService) {
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.appId = data.appId;
        this.partnerId = data.partnerId;
        this.customerInfo = data.customerInfo;
        this.usernameChat = data.usernameChat;
        this.demandName = data.demandName;
        this.demandDetail = data.demandDetail;
        this.chatOpenObject = data.chatOpenObject;
        this.type = data.type;
        if (this.type === 'delete') {
            this.report = data.report;

        }

        console.log('CUSTOMER INF O ' + JSON.stringify(this.usernameChat));
    }

    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }

    public deleteReport(report) {
        let body;
        body = {
            user_bad_id: this.report.id,
            partner_id: this.partnerId,
        };
        this.chatService.deleteReport(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.notificationService.showSuccessTimer('Thành công', 2000);
                    this.dialogRef.close({
                        type: 'delete'
                    });
                }
            });
    }

    public saveReport() {
        if (!this.contentReport) {
            this.notificationService.showWarningTimer('Vui lòng nhập nội dung report', 3000);
            return;
        }
        let body;
        if (this.type === 'create') {
            body = {
                name: this.customerInfo.name,
                username: this.chatService.getUserIdFacebookFromRoom(this.chatOpenObject.name),
                address: this.customerInfo.address,
                phone: this.customerInfo.phone,
                reason: this.contentReport,
                partner_id: this.partnerId,
                user_id_report: this.usernameChat.backend_user_id,
                username_bad: this.chatService.getUserIdFacebookFromRoom(this.chatOpenObject.name)
            };
            this.chatService.createReport(JSON.stringify(body))
                .then((user) => {
                    if (user) {
                        // console_bk.log('USERRRRR ' + JSON.stringify(user));
                        this.notificationService.showSuccessTimer('Thành công', 2000);
                        this.dialogRef.close({
                            type: 'create'
                        });
                    }
                });
        }
    }

}
