import { Component, Inject, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';
import { NotificationService } from '../../../../../_services/notification.service';
import { Observable } from 'rxjs/Rx';
import { UUID } from 'angular2-uuid';
import { IntentService } from '../../../../../_services/index';
import { BlocksService } from '../../../../../_services/blocks.service';

declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;

@Component({
    selector: 'add-training-dialog',
    templateUrl: 'add-training-dialog.component.html',
    styleUrls: ['add-training-dialog.component.scss']
})
export class AddTrainingDialogComponent {
    private message: any;
    private title: string;

    private partnerId: string;
    private appId: string;
    private room: string;
    private customerInfo: any;
    private usernameChat: any;
    private demandName: string;
    private demandDetail: string;
    private chatOpenObject: any;
    private type: string;
    private report: any;
    private content: any;
    private isDestroy: boolean;
    private agentActive: any;

    private intents: any = [];
    private intentsTemp: any = [];
    public blocks: any = [];

    private saveing: boolean;
    private contentReport: string;
    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<AddTrainingDialogComponent>,
        private cdRef: ChangeDetectorRef,
        private chatService: ChatService, private notificationService: NotificationService,
        private intentService: IntentService, private blocksService: BlocksService) {
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.appId = data.appId;
        this.partnerId = data.partnerId;
        this.customerInfo = data.customerInfo;
        this.usernameChat = data.usernameChat;
        this.demandName = data.demandName;
        this.demandDetail = data.demandDetail;
        this.chatOpenObject = data.chatOpenObject;
        this.content = data.content;
        this.agentActive = data.agentActive;
        this.type = data.type;
        if (this.type === 'delete') {
            this.report = data.report;

        }
        // console_bk.log('CUSTOMER INF OBB ' + JSON.stringify(this.usernameChat));
        this.addNewItem((cb) => {
            this.getListBlock((cb1) => {
                this.onAfterAddUsersay(this.content, 0);
            });
        });

    }
    public ngOnDestroy(): void {
        this.isDestroy = true;
    }

    public close() {
        this.dialogRef.close();
    }

    public getListBlock(callback) {
        let actionOperate: Observable<any>;
        actionOperate = this.blocksService.getListGroupBlocks(this.partnerId);
        actionOperate.subscribe(
            (blocks) => {
                if (!blocks) {
                    this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
                    callback(false);
                    return;
                }
                let block = [];
                blocks.filter((item) => {
                    if (item.blocks) {
                        block = _.union(block, item.blocks);
                    }
                });
                let temp = [];
                block.filter((item) => {
                    temp.push({ value: item.id, display: item.name });
                });
                this.blocks = temp;
                callback(true);
            },
            (err) => {
                callback(false);
                this.notificationService.showDanger('Tải dữ liệu thất bại vui lòng thử lại');
            });
    }

    public onAfterAddUsersay(tag, index) {

        let intentObject = this.intentsTemp[index];
        let usersayInsert: any = { id: UUID.UUID(), intent_id: intentObject.id, content: tag, not_update_mongo: true };
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.addUsersayIntoIntent(usersayInsert);
        actionOperate.subscribe(
            (usay) => {
                if (!intentObject.usersays) {
                    intentObject.usersays = [];
                }
                intentObject.usersays.push(usay);
                let ids = _.pluck(intentObject.usersays, 'id');
                this.updateFirstUsersay(ids);
                this.intents = this.intentsTemp;
                this.callChange();
            },
            (err) => {
                this.notificationService.showDanger('Có lỗi xảy ra khi thêm câu nói khách hàng');
            });
    }

    public updateFirstUsersay(ids) {
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.updateFirstUsersay(ids);
        actionOperate.subscribe(
            (usay) => {
            },
            (err) => {
            });
    }

    public addNewItem(callback) {
        // console_bk.log('AddNew Item');
        let intentNew = { id: UUID.UUID(), name: UUID.UUID(), faq: 1, agent_id: this.agentActive.agent_id };

        let actionOperate: Observable<any>;
        actionOperate = this.intentService.add(intentNew);
        actionOperate.subscribe(
            (intent) => {
                if (!intent) {
                    this.notificationService.showDanger('Thêm thất bại');
                    this.dialogRef.close();
                    return;
                }
                if (intent === 202) {
                    this.notificationService.showWarning('FAQ đã tồn tại');
                    this.dialogRef.close();
                } else {
                    this.intentsTemp.unshift(intent);
                    return callback(true);
                }
            },
            (err) => {
                this.notificationService.showDanger('Thêm thất bại');
            });
    }

    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
