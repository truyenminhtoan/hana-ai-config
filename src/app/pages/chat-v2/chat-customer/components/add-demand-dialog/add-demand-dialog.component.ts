import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ChatService } from '../../../../../_services/chat.service';
import { NotificationService } from '../../../../../_services/notification.service';

declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;

@Component({
    selector: 'add-demand-dialog',
    templateUrl: 'add-demand-dialog.component.html',
    styleUrls: ['add-demand-dialog.component.scss']
})
export class AddDemandDialogComponent {
    private message: any;
    private title: string;

    private partnerId: string;
    private appId: string;
    private room: string;
    private customerInfo: any;
    private usernameChat: any;
    private demandName: string;
    private demandDetail: string;
    private chatOpenObject: any;
    private type: string;
    private demandObject: any;

    private saveing: boolean;

    constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<AddDemandDialogComponent>, private chatService: ChatService, private notificationService: NotificationService) {
        // console_bk.log('THISMODEL ' + JSON.stringify(data));
        this.title = data.title;
        this.appId = data.appId;
        this.partnerId = data.partnerId;
        this.customerInfo = data.customerInfo;
        this.usernameChat = data.usernameChat;
        this.demandName = data.demandName;
        this.demandDetail = data.demandDetail;
        this.chatOpenObject = data.chatOpenObject;
        this.type = data.type;
        if (this.type === 'edit') {
            this.demandObject = data.demand;
            this.demandName = this.demandObject.name;
            this.demandDetail = this.demandObject.description;
        }
    }

    public close() {
        // console_bk.log('CLICK CLOSE ');
        this.dialogRef.close();
    }
    public saveDemand() {
        this.saveing = true;
        if (!this.demandName || this.demandName.trim() === '') {
            this.notificationService.showWarningTimer('Vui lòng nhập tên nhu cầu', 2000);
            $('#demandname').focus();
            this.saveing = false;
            return false;
        }
        if (!this.demandDetail || this.demandDetail.trim() === '') {
            this.notificationService.showWarningTimer('Vui lòng nhập ghi chú', 2000);
            $('#demanddetail').focus();
            this.saveing = false;
            return false;
        }

        if (!this.customerInfo.name || this.customerInfo.name.trim() === '') {
            this.notificationService.showWarningTimer('Vui lòng nhập tên khách hàng', 2000);
            $('#demandusername').focus();
            this.saveing = false;
            return false;
        }

        this.updateCustomerInfo();

    }

    public updateCustomerInfo() {
        let body = {
            name: this.customerInfo.name,
            phone: this.customerInfo.phone,
            email: this.customerInfo.email,
            address: this.customerInfo.address,
            phone_posible: this.customerInfo.phone_posible,
            backend_partner_id: this.partnerId,
            backend_user_created_id: this.usernameChat.backend_user_id,
            note: this.customerInfo.note,
            customer_username: this.chatOpenObject.creater
        };
        this.chatService.updateCustomer(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    if (user.errorCode === 200) {
                        // console_bk.log('USERRRRR ' + JSON.stringify(user));
                        this.customerInfo.id = user.data[0].id;
                        // this.notificationService.showSuccessTimer('Thành công', 2000);
                        this.updateDemand();
                    } else {
                        this.notificationService.showWarningTimer('Thất bại', 2000);
                        this.saveing = false;
                    }
                }
            });
    }

    public updateDemand() {

        let body;
        if (this.type === 'create') {
            body = {
                partner_id: this.partnerId,
                app_id: this.appId,
                backend_partner_id: this.partnerId,
                customer_username: this.chatOpenObject.creater,
                customer_id: this.customerInfo.id,
                name: this.demandName,
                description: this.demandDetail
            };
            this.chatService.createDemand(JSON.stringify(body))
                .then((user) => {
                    if (user) {
                        // console_bk.log('USERRRRR ' + JSON.stringify(user));
                        this.notificationService.showSuccessTimer('Thành công', 2000);
                        this.dialogRef.close({
                            type: 'create',
                            data: {
                                customerInfo: this.customerInfo
                            }
                        });
                    }
                });
        } else {
            body = {
                partner_id: this.partnerId,
                app_id: this.appId,
                backend_partner_id: this.partnerId,
                customer_username: this.chatOpenObject.creater,
                customer_id: this.customerInfo.id,
                name: this.demandName,
                description: this.demandDetail,
                id: this.demandObject.id
            };
            this.chatService.updateDemand(JSON.stringify(body))
                .then((user) => {
                    if (user) {
                        // console_bk.log('USERRRRR ' + JSON.stringify(user));
                        this.notificationService.showSuccessTimer('Thành công', 2000);
                        this.dialogRef.close({
                            type: 'create',
                            data: {
                                customerInfo: this.customerInfo
                            }
                        });
                    }
                });
        }


    }
}
