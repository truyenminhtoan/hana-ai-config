import { ApplicationService } from '../../../_services/application.service';
import { ChatService } from '../../../_services/chat.service';
import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef, Directive, HostListener, sequence
} from '@angular/core';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { NotificationService } from '../../../_services/notification.service';
import { MdDialog } from '@angular/material';
import { AddDemandDialogComponent } from './components/add-demand-dialog/add-demand-dialog.component';
import { AddReportDialogComponent } from './components/add-report-dialog/add-report-dialog.component';

import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs/Observable';
import { IntentService, ConfigService, EmitterService } from '../../../_services/index';
import { AddTrainingDialogComponent } from './components/add-training-dialog/add-training-dialog.component';
declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;

@Component({
    selector: 'chat-customer',
    templateUrl: 'chat-customer.component.html',
    styleUrls: ['chat-customer.component.scss']
})
export class ChatCustomerComponent {

    @Input() appId: string;
    @Input() partnerId: string;
    @Input() allCustomerInfo: any;
    @Input() allAgent: any;
    @Input() isOpenChat: any;
    @Input() usernameChat: any;
    @Input() jid: any;
    @Input() rawReceive: any;
    @Input() agentActive: any;
    @Input() connectStatus: string;
    @Input() chatOpenObject: any;

    @Output() emitChatCustomer = new EventEmitter<any>();

    private navFilter: any;
    private customerInfo: any;
    private listDemand: any;
    private isDestroy: boolean;
    private listHastag: any = [];
    private tags = [];
    private listSequencetag: any = [];
    private sequenceTags = [];
    private listAllGroupOb: any;
    private listUserGroup: any;
    private listAllSequenceOb: any;
    private listUserSequence: any;
    private reportCounter: number;
    private listReport: any;
    private position = 'right';
    private listTrainingString = [];
    private searchKey: any = '';
    private loadingProcess = false;
    private errorMes = false;
    private msgSay = false;
    private intents: any[] = [];
    private currentDrag: string;

    @Input() emitTraining: string;

    constructor(private cdRef: ChangeDetectorRef, private dialog: MdDialog, private applicationService: ApplicationService,
        private chatService: ChatService, private configService: ConfigService, private notificationService: NotificationService, private intentService: IntentService) {
    }
    private ngOnInit() {
        EmitterService.get(this.emitTraining).subscribe((data: any) => {
            this.configService.log(JSON.stringify(data));
            if (data.type === 'training') {
                let msg = data.data.content;
                this.searchKey = msg;
                this.scrollTo('training');
                this.getListIntentByUsersay();
            }
        });
        this.showFilter();
    }

    private showFilter() {
        // console_bk.log('USERNAMECHAT ' + this.usernameChat);

    }

    private ngOnDestroy(): void {
        this.isDestroy = true;
    }

    private ngOnChanges(changes: SimpleChanges) {
        // console_bk.log('Change input');
        if (changes['chatOpenObject'] && this.chatOpenObject) {
            this.getCustomerByUser();
            this.navFilter = [];
            this.listTrainingString = [];
            this.searchKey = '';
            this.intents = [];
            // console_bk.log('CONTACT NHAN ' + JSON.stringify(this.usernameChat));
            if (this.chatService.checkUserIsFacebook(this.chatOpenObject.creater)) {
                if (this.chatOpenObject.entry === 'groupchat') {
                    this.navFilter.push({
                        name: 'cusinfo',
                        focus: true
                    });
                    this.navFilter.push({
                        name: 'addgroup',
                        focus: false
                    });
                    this.navFilter.push({
                        name: 'demand',
                        focus: false
                    });
                    this.navFilter.push({
                        name: 'training',
                        focus: false
                    });
                    this.navFilter.push({
                        name: 'report',
                        focus: false
                    });
                } else {
                    this.navFilter.push({
                        name: 'cusinfo',
                        focus: true
                    });
                    this.navFilter.push({
                        name: 'demand',
                        focus: false
                    });
                    /* this.navFilter.push({
                        name: 'addgroup',
                        focus: false
                    }); */
                    this.navFilter.push({
                        name: 'report',
                        focus: false
                    });
                }

            } else {
                if (this.chatOpenObject.entry === 'groupchat') {
                    this.navFilter.push({
                        name: 'cusinfo',
                        focus: true
                    });
                    this.navFilter.push({
                        name: 'demand',
                        focus: false
                    });
                    this.navFilter.push({
                        name: 'training',
                        focus: false
                    });
                } else {
                    this.navFilter.push({
                        name: 'cusinfo',
                        focus: true
                    });
                    this.navFilter.push({
                        name: 'demand',
                        focus: false
                    });
                }
            }
            this.callChange();
        }
    }

    private getCustomerByUser() {
        let body = {
            partner_id: this.partnerId,
            app_id: this.appId,
            customer_username: this.chatOpenObject.creater
        };
        this.chatService.getCustomerByUser(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.customerInfo = {};
                    let userInfo = user.data;
                    // console_bk.log('USERINFO ' + JSON.stringify(userInfo));
                    this.customerInfo.id = userInfo.id;
                    if (userInfo.id) {
                        this.listDemand = [];
                        this.getDemand();
                    } else {
                        this.listDemand = [];
                    }
                    let filterAddG = _.find(this.navFilter, (nav) => {
                        return nav.name === 'demand';
                    });
                    let filterRep = _.find(this.navFilter, (nav) => {
                        return nav.name === 'report';
                    });
                    if (filterAddG) {
                        this.getAllGroup();
                        this.getAllSequence();
                    }
                    if (filterRep) {
                        this.getReportUser();
                    }
                    this.customerInfo.name = userInfo.name ? userInfo.name : userInfo.time_zone;
                    this.customerInfo.phone = userInfo.phone;
                    this.customerInfo.email = userInfo.email;
                    this.customerInfo.phone_posible = userInfo.phone_posible;
                    this.customerInfo.address = userInfo.address;
                    this.customerInfo.site_url = userInfo.site_url;
                    this.customerInfo.note = userInfo.note;
                    this.callChange();
                }
            });
    }

    private getReportUser() {
        let body = {
            username_bad: this.chatService.getUserIdFacebookFromRoom(this.chatOpenObject.name)
        };
        this.chatService.getCustomerReport(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    if (user && user.length > 0) {
                        this.reportCounter = user.length;
                    } else {
                        this.reportCounter = 0;
                    }
                    this.listReport = user;
                }
            });
    }

    private getAllGroup() {
        this.chatService.getListGroupByAppid(this.appId)
            .then((user) => {
                this.listAllGroupOb = user;
                this.getCustomerGroup();
            });
    }
    private getCustomerGroup() {
        this.chatService.getListGroupByPageCustomer(this.chatService.getPageCustomerFromCreater(this.chatOpenObject.creater))
            .then((user) => {
                this.listUserGroup = user;
                this.listHastag = [];
                this.tags = [];
                _.each(this.listUserGroup, (userG) => {
                    this.tags.push(userG.name);
                });
                _.each(this.listAllGroupOb, (userG) => {
                    this.listHastag.push(userG.name);
                });

                this.callChange();
            });
    }

    private getAllSequence() {
        this.chatService.getListSequenceByAppid(this.appId)
            .then((user) => {
                this.listAllSequenceOb = user;
                this.getCustomerSequence();
            });
    }
    private getCustomerSequence() {
        this.chatService.getListSequenceByPageCustomer(this.chatService.getPageCustomerFromCreater(this.chatOpenObject.creater))
            .then((user) => {
                this.listUserSequence = user;
                this.listSequencetag = [];
                this.sequenceTags = [];
                _.each(this.listUserSequence, (userG) => {
                    this.sequenceTags.push(userG.name);
                });
                _.each(this.listAllSequenceOb, (userG) => {
                    this.listSequencetag.push(userG.name);
                });

                this.callChange();
            });
    }

    private updateCustomerInfo() {
        let body = {
            name: this.customerInfo.name,
            phone: this.customerInfo.phone,
            email: this.customerInfo.email,
            address: this.customerInfo.address,
            phone_posible: this.customerInfo.phone_posible,
            backend_partner_id: this.partnerId,
            backend_user_created_id: this.usernameChat.backend_user_id,
            note: this.customerInfo.note,
            customer_username: this.chatOpenObject.creater
        };
        // console_bk.log('updateCUSTOMERINFO ' + JSON.stringify(body));
        this.chatService.updateCustomer(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    if (user.errorCode === 200) {
                        this.notificationService.showSuccessTimer('Thành công', 2000);
                        let emitbody = {
                            type: 'updateinfo',
                            data: body
                        };
                        this.emitChatCustomer.emit(emitbody);
                    } else {
                        this.notificationService.showWarningTimer('Thất bại', 2000);
                    }
                    this.callChange();
                }
            });
    }

    private getDemand() {
        let body = {
            customer_id: this.customerInfo.id,
            partner_id: this.partnerId,
            app_id: this.appId,
            customer_username: this.chatOpenObject.creater,
            backend_partner_id: this.partnerId
        };
        this.chatService.getDemand(JSON.stringify(body))
            .then((user) => {
                if (user) {

                    this.listDemand = user.data;
                    // console_bk.log('LISTDEMAND ' + JSON.stringify(this.listDemand));
                    this.callChange();
                }
            });
    }

    private dialogDemand(type, demandObject) {
        let dialogRef;
        if (type === 'create') {
            dialogRef = this.dialog.open(AddDemandDialogComponent, {
                width: '50%',
                data: {
                    title: 'Tạo nhu cầu',
                    customerInfo: this.customerInfo,
                    partnerId: this.partnerId,
                    appId: this.appId,
                    type: 'create',
                    usernameChat: this.usernameChat,
                    chatOpenObject: this.chatOpenObject
                }
            });
        } else {
            dialogRef = this.dialog.open(AddDemandDialogComponent, {
                width: '50%',
                data: {
                    title: 'Chỉnh sửa nhu cầu',
                    customerInfo: this.customerInfo,
                    partnerId: this.partnerId,
                    appId: this.appId,
                    type: 'edit',
                    usernameChat: this.usernameChat,
                    chatOpenObject: this.chatOpenObject,
                    demand: demandObject
                }
            });
        }

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (result) {
                    // console_bk.log('GETTTTTTTTTTTTTTTTTTTTTTTTTT ' + JSON.stringify(result));
                    if (result.type === 'create') {
                        this.customerInfo = result.data.customerInfo;
                        // console_bk.log('GETTTTTTTTTTTTTTTTTTTTTTTTTT');
                        this.customerInfo.customer_username = this.chatOpenObject.creater;
                        let emitbody = {
                            type: 'updateinfo',
                            data: this.customerInfo
                        };

                        this.emitChatCustomer.emit(emitbody);
                        this.getDemand();
                        this.callChange();
                    } else {

                    }
                }
            }
        });
    }

    private dialogTraining(message) {
        let dialogRef;
        dialogRef = this.dialog.open(AddTrainingDialogComponent, {
            width: '85%',
            data: {
                title: 'Thêm câu nói khách hàng',
                customerInfo: this.customerInfo,
                partnerId: this.partnerId,
                appId: this.appId,
                type: 'create',
                content: message,
                usernameChat: this.usernameChat,
                agentActive: this.agentActive,
                chatOpenObject: this.chatOpenObject
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (result) {
                    // console_bk.log('GETTTTTTTTTTTTTTTTTTTTTTTTTT ' + JSON.stringify(result));
                }
            }
        });
    }

    private dialogReport(type, reportObject) {
        let dialogRef;
        if (type === 'create') {
            dialogRef = this.dialog.open(AddReportDialogComponent, {
                width: '50%',
                data: {
                    title: 'Tạo báo xấu',
                    customerInfo: this.customerInfo,
                    partnerId: this.partnerId,
                    appId: this.appId,
                    type: 'create',
                    usernameChat: this.usernameChat,
                    chatOpenObject: this.chatOpenObject
                }
            });
        } else {
            dialogRef = this.dialog.open(AddReportDialogComponent, {
                width: '50%',
                data: {
                    title: 'Bạn có muốn xóa báo xấu này?',
                    customerInfo: this.customerInfo,
                    partnerId: this.partnerId,
                    appId: this.appId,
                    type: 'delete',
                    usernameChat: this.usernameChat,
                    chatOpenObject: this.chatOpenObject,
                    report: reportObject
                }
            });
        }

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (result) {
                    this.getReportUser();
                    this.callChange();
                }
            }
        });
    }

    private addToGroup(value) {
        // {"display":"sfsdfdsfdsfád","value":"sfsdfdsfdsfád"}
        // console_bk.log('adddvalue ' + JSON.stringify(value));
        let body = {
            app_id: this.appId,
            name: value.value
        };
        this.chatService.groupFindOrCreate(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    // console_bk.log('USER  + user ' + JSON.stringify(user));
                    // this.listDemand = user;
                    this.groupAddCustomerToGroup(value.value);
                }
            });

    }
    private groupAddCustomerToGroup(nameGr) {
        let pageCustomer = this.chatService.getPageIdFacebook(this.chatOpenObject.creater);

        // console_bk.log('PAGECUSTOMER ' + JSON.stringify(pageCustomer));
        let body = {
            app_id: this.appId,
            page_id: pageCustomer.page,
            customer_id: pageCustomer.customer,
            group: {
                name: nameGr
            }
        };
        this.chatService.groupAddCustomerToGroup(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.notificationService.showSuccessTimer('Thành công', 3000);
                } else {
                    this.notificationService.showWarningTimer('Không thể thêm người dùng này vào nhóm', 3000);
                }
            });

    }

    // sequence
    private addToSequence(value) {
        console.log('SAFFFFFFFFFFF FFFFFF ' + JSON.stringify(this.sequenceTags) );
        let sequencess = _.find(this.listAllSequenceOb, (seq) => {
            return seq.name === value.value;
        });
        if (!sequencess) {
            this.notificationService.showWarningTimer('Vui lòng chọn chuỗi', 3000);
            this.sequenceTags = _.filter(this.sequenceTags, (ss) => {
                return ss !== value.value;
            });
            this.callChange();
            return false;
        }
        // console.log('SAFFFFFFFFFFF FFFFFF ' + JSON.stringify(this.listAllSequenceOb) );
        this.groupAddCustomerToSequence(value.value);
    }
    private groupAddCustomerToSequence(nameGr) {
        let pageCustomer = this.chatService.getPageIdFacebook(this.chatOpenObject.creater);
        let sequencess = _.find(this.listAllSequenceOb, (seq) => {
            return seq.name === nameGr;
        });
        let body = {
            app_id: this.appId,
            campaign_id: sequencess.id,
            page_customer_ids: [
                pageCustomer.page + '_' + pageCustomer.customer
            ]
        };
        this.chatService.sequenceAddCustomerToSequence(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.notificationService.showSuccessTimer('Thành công', 3000);
                } else {
                    this.notificationService.showWarningTimer('Không thể thêm người dùng này vào nhóm', 3000);
                }
            });

    }

    private dragStart(event, car: any) {
        this.configService.log('STARTDRAG ' + car);
        this.currentDrag = car;
    }
    private drop(event, index) {
        this.configService.log('DROPDONE ' + index);
        this.configService.log('DROPDONE search ' + this.currentDrag);
        this.onAfterAddUsersay(this.currentDrag, index);

    }

    private onAfterAddUsersay(tag, index) {
        let intentObject = this.intents[index];
        this.configService.log('OBJECTHIST  ' + JSON.stringify(intentObject));
        let findSame = _.find(intentObject.usersays, (find) => {
            return find.content === tag;
        });
        if (findSame) {
            this.notificationService.showWarningTimer('Bạn đã dạy câu nói này rồi', 3000);
            return false;
        }
        this.listTrainingString = _.filter(this.listTrainingString, (stringTr) => {
            return stringTr !== tag;
        });
        this.configService.log('OBJECTHIST  ' + JSON.stringify(intentObject));
        let usersayInsert: any = { id: UUID.UUID(), intent_id: intentObject.id, content: tag, not_update_mongo: true };
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.addUsersayIntoIntent(usersayInsert);
        actionOperate.subscribe(
            (usay) => {
                intentObject.usersays.push(usay);
                /* if (!this.currentIntent.usersays) {
                    this.currentIntent.usersays = [];
                }
                this.currentIntent.usersays.push(usay);
                let ids = _.pluck(this.currentIntent.usersays, 'id');
                this.updateFirstUsersay(ids);
                this.updateResponse(); */
            },
            (err) => {
                this.notificationService.showDanger('Có lỗi xảy ra khi thêm câu nói khách hàng');
                // console_bk.log(err);
            });
    }

    private backToDontUnderstand(item, index) {
        this.removeUsersay(item.value.id, (cb) => {
            let intent = this.intents[index];
            for (let i = 0; i < intent.usersays.length; i++) {
                if (intent.usersays[i].id === item.value.id) {
                    this.intents[index].usersays.splice(i, 1);
                    break;
                }
            }
        });
    }

    private removeUsersay(Id, callback) {
        this.loadingProcess = true;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.removeUsersayFromIntent(Id);
        actionOperate.subscribe(
            (says) => {
                this.loadingProcess = false;
                callback(true);
            },
            (err) => {
                this.loadingProcess = false;
                callback(false);
            });
    }

    private dragEnd(event) {
        this.configService.log('DRAGEND');
    }

    private valuechangeSearch() {
        // console_bk.log('CHANGEVALUE ' + this.searchKey);
        if (this.searchKey === '') {
            this.getListIntentByUsersay();
        }
    }


    private getListIntentByUsersay() {
        let say = this.searchKey;
        if (say === '') {
            return false;
        }
        this.listTrainingString.push(say);
        this.loadingProcess = true;
        this.intents = [];
        this.errorMes = false;
        let actionOperate: Observable<any>;
        actionOperate = this.intentService.getListIntentUsersay(say, this.agentActive.agent_id);
        actionOperate.subscribe(
            (intents) => {
                if (intents) {
                    this.loadingProcess = false;
                    if (_.isEmpty(intents)) {
                        this.errorMes = true;
                    } else {
                        let intentList = intents;
                        intentList.filter((item) => {
                            if (item.usersays) {
                                item.usersays.filter((saysItem) => {
                                    saysItem.style = 'chip-none';
                                });
                            }
                        });
                        this.intents = intentList;
                    }
                    this.scrollTo('training');
                }
            },
            (err) => {
                this.loadingProcess = false;
                this.errorMes = true;
            });
    }

    private removeOutGroup(value) {
        // console_bk.log('remove from ' + value);
        let pageCustomer = this.chatService.getPageIdFacebook(this.chatOpenObject.creater);
        let body = {
            app_id: this.appId,
            page_id: pageCustomer.page,
            customer_id: pageCustomer.customer,
            group: {
                name: value
            }
        };
        this.chatService.removeGroupCustomer(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.notificationService.showSuccessTimer('Thành công', 3000);
                } else {
                    this.notificationService.showWarningTimer('Không thể thêm người dùng này vào nhóm', 3000);
                }
            });
    }
    private removeOutSequence(value) {
        console.log('remove from ' + value);
        let pageCustomer = this.chatService.getPageIdFacebook(this.chatOpenObject.creater);
        let sequencess = _.find(this.listAllSequenceOb, (seq) => {
            return seq.name === value;
        });
        let body = {
            app_id: this.appId,
            campaign_id: sequencess.id,
            page_customer_ids: [
                pageCustomer.page + '_' + pageCustomer.customer
            ]
        };
        this.chatService.removeSequenceCustomer(JSON.stringify(body))
            .then((user) => {
                if (user) {
                    this.notificationService.showSuccessTimer('Thành công', 3000);
                } else {
                    this.notificationService.showWarningTimer('Không thể thêm người dùng này vào nhóm', 3000);
                }
            });
    }
    private scrollTo(value) {
        this.focusNavbar(value);
        if (value === 'demand') {
            let $container = $('#side-content-customer');
            let $scrollTo = $('#demand');
            $container.animate({
                scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
            });
        }
        if (value === 'cusinfo') {
            let $container = $('#side-content-customer');
            let $scrollTo = $('#cusinfo');
            $container.animate({
                scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
            });
        }
        if (value === 'addgroup') {
            let $container = $('#side-content-customer');
            let $scrollTo = $('#addgroup');
            $container.animate({
                scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
            });
        }
        if (value === 'report') {
            let $container = $('#side-content-customer');
            let $scrollTo = $('#report');
            $container.animate({
                scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
            });
        }
        if (value === 'training') {
            let $container = $('#side-content-customer');
            let $scrollTo = $('#training');
            $container.animate({
                scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
            });
        }
    }

    private focusNavbar(value) {
        _.each(this.navFilter, (nav) => {
            nav.focus = false;
        });
        let navObject = _.find(this.navFilter, (nav) => {
            return nav.name === value;
        });
        navObject.focus = true;

    }
    private callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }

}
