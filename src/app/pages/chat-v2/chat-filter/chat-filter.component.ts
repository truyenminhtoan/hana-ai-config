import {
    Component, OnInit, OnChanges, AfterViewInit, ViewEncapsulation, Input, Output, EventEmitter,
    ChangeDetectorRef, Directive, HostListener
} from '@angular/core';
import { ApplicationService } from '../../../_services/application.service';
import { ChatService } from '../../../_services/chat.service';
import { SimpleChanges } from 'angular2-color-picker/node_modules/@angular/core/src/metadata/lifecycle_hooks';
import { ConfigService } from '../../../_services/config.service';

declare var $: any;
declare var _: any;
declare var X2JS: any;
declare var Base64: any;
@Component({
    selector: 'chat-filter',
    templateUrl: 'chat-filter.component.html',
    styleUrls: ['../assets/css/material-dashboard.css', '../assets/css/chat.css', 'chat-filter.component.scss']
})
export class ChatFilterComponent implements OnInit, AfterViewInit {
    @Input() chatFilter: any;
    @Input() configGateway: any;
    @Input() roleUser: any;
    @Input() rawReceive: any;
    @Input() usernameChat: any;
    @Output() filterChange = new EventEmitter<any>();

    private isDestroy: boolean;
    private keyFirst: string;
    private position = 'right';

    constructor(private configService: ConfigService, private cdRef: ChangeDetectorRef, private applicationService: ApplicationService, private chatService: ChatService) {
    }
    public ngOnInit() {
        // console_bk.log('LOCHATFILTER' + JSON.stringify(this.chatFilter));
        /* this.chatFilter = {
            filterAll: true,
            filterUnread: false,
            filterComment: false,
            filterChat: false,
            filterDontUn: false,
            filterPhone: false,
            filterForward: false,
            filterMe: false
          }; */
        /* this.chatFilter = [
          {key: 'filterAll', show: true, filter: true},
          {key: 'filterUnread', show: true, filter: false},
          {key: 'filterComment', show: true, filter: false},
          {key: 'filterChat', show: true, filter: false},
          {key: 'filterDontUn', show: true, filter: false},
          {key: 'filterPhone', show: true, filter: false},
          {key: 'filterForward', show: true, filter: false},
          {key: 'filterMe', show: true, filter: false},
        ]; */

    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes['configGateway'] && this.configGateway) {
            this.getFilter();
        } else if (changes['rawReceive'] && this.rawReceive) {
            // console_bk.log('RECEIVEFILTER');
            this.onMessageRawReceive();
        }
    }

    public ngOnDestroy(): void {
        this.isDestroy = true;
    }
    public ngAfterViewInit() {
        this.filterChange.emit(this.chatFilter);
        this.callChange();
    }

    public onMessageRawReceive() {
        let x2js = new X2JS();
        let jsonObj = x2js.xml_str2json(this.rawReceive);
        if (jsonObj.message) {
            let message = jsonObj.message;
            let broadcast = message._broadcast;
            let subtype = message._subtype;
            if (message.broadcast) {
                this.handleBroadcastXmpp(message);
            }

        } else if (jsonObj.iq) {
            // console_bk.log('GOI TIN IQ');
        } else if (jsonObj.presence) {
            // console_bk.log('PRESENCE');
        }

    }

    public handleBroadcastXmpp(message) {
        let objectBroad = JSON.parse(Base64.decode(message.broadcast));
        let from = message._from;
        from = this.chatService.getNodeChat(from);
        if (objectBroad.type === 'forwardChat') {
            if (objectBroad.member === this.usernameChat.username) {
                let forward = _.find(this.chatFilter, (fil) => {
                    return fil.key === 'filterForward';
                });
                if (forward) {
                    forward.alert = true;
                }
                this.callChange();
            }
            
        }

    }

    public getFilter() {
        if ((this.roleUser === this.configService.ROLE_USER.admin) || (this.roleUser === this.configService.ROLE_USER.syssupport) || (this.roleUser === this.configService.ROLE_USER.sysadmin)) {
            this.getFirst();
        } else {
            if (this.configGateway.list_filter_chat) {
                let filter = JSON.parse(this.configGateway.list_filter_chat);
                if (filter.length > 0) {
                    _.each(filter, (fil) => {
                        let keyFil = _.find(this.chatFilter, (cf) => {
                            return cf.key === fil.key;
                        });
                        keyFil.show = fil.show;
                    });
                }
            }
            this.getFirst();
        }
    }
    public filterRule(rule) {
        // console_bk.log(rule);
        let filterAll = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterAll';
        });
        let filterUnread = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterUnread';
        });
        let filterComment = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterComment';
        });
        let filterChat = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterChat';
        });
        let filterDontUn = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterDontUn';
        });
        let filterForward = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterForward';
        });
        let filterPhone = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterPhone';
        });
        let filterMe = _.find(this.chatFilter, (chatf) => {
            return chatf.key === 'filterMe';
        });
        if (rule === 'filterAll') {
            _.each(this.chatFilter, (chatf) => {
                if (chatf.key === 'filterAll') {
                    chatf.filter = true;
                } else {
                    chatf.filter = false;
                }
            });
        } else if (rule === 'filterUnread') {
            if (filterUnread.filter) {
                filterUnread.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                filterUnread.filter = true;
            }

        } else if (rule === 'filterComment') {
            if (filterComment.filter) {
                filterComment.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                if (filterChat) {
                    filterChat.filter = false;
                }
                filterComment.filter = true;
            }
        } else if (rule === 'filterChat') {
            if (filterChat.filter) {
                filterChat.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                if (filterComment) {
                    filterComment.filter = false;
                }
                filterChat.filter = true;
            }

        } else if (rule === 'filterDontUn') {
            if (filterDontUn.filter) {
                filterDontUn.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                filterDontUn.filter = true;
            }
        } else if (rule === 'filterPhone') {
            if (filterPhone.filter) {
                filterPhone.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                filterPhone.filter = true;
            }
        } else if (rule === 'filterForward') {
            filterForward.alert = false;
            if (filterForward.filter) {
                filterForward.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                filterForward.filter = true;
            }
        } else if (rule === 'filterMe') {
            if (filterMe.filter) {
                filterMe.filter = false;
            } else {
                if (filterAll) {
                    filterAll.filter = false;
                }
                filterMe.filter = true;
            }
        }
        if ((filterAll.filter === false) && (filterUnread.filter === false) &&
            (filterComment.filter === false) && (filterChat.filter === false) &&
            (filterDontUn.filter === false) && (filterPhone.filter === false) &&
            (filterForward.filter === false) && (filterMe.filter === false)) {
            let filterShow = _.filter(this.chatFilter, (fil) => {
                return fil.show;
            });
            let first = _.first(filterShow);
            first.filter = true;
        }
        // console_bk.log('GETFIRSTTTTT ' + JSON.stringify(this.chatFilter));
        this.filterChange.emit(this.chatFilter);
        this.callChange();
    }

    public getFirst() {
        let filterShow = _.filter(this.chatFilter, (fil) => {
            return fil.show;
        });
        let first = _.first(filterShow);
        this.filterRule(first.key);
    }
    public callChange() {
        if (this.isDestroy) {
            return false;
        }
        this.cdRef.detectChanges();
    }
}
