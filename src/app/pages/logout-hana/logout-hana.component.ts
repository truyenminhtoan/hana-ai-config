import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'logout-hana',
    templateUrl: 'logout-hana.component.html',
    styleUrls: ['logout-hana.component.scss']
})
export class LogoutHanaComponent {
    constructor(private route: ActivatedRoute,  private router: Router) {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('apphanausing');
        localStorage.removeItem('session_time');
        // this.router.navigate(['/login-hana'], { queryParams: { returnUrl: this.router.url } });
        this.router.navigate(['/login-hana']);
    }
}
