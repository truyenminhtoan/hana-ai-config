import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LogoutHanaComponent } from './logout-hana.component';

const routes: Routes = [
  { path: '', component: LogoutHanaComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
