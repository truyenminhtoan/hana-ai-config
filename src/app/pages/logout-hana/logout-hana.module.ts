// This Module's Components
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './logout-hana.routing';
import { UserService } from '../../_services/user.service';
import { NotificationService } from '../../_services/notification.service';
import { LogoutHanaComponent } from './logout-hana.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        CommonModule,
        routing,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        LogoutHanaComponent,
    ],
    providers: [UserService, NotificationService],
    exports: [
        LogoutHanaComponent,
    ]
})
export class LogoutHanaModule {

}

