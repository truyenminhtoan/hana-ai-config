import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const ROUTES: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  // otherwise redirect to home
  { path: '**', redirectTo: 'intents', pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(ROUTES, { useHash: true });
