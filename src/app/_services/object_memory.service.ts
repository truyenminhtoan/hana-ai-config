import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BaService } from './ba.service';

@Injectable()
export class ObjectMemoryService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  // @toantm1
  public getListObjectMemoryByAgent(agentId: string): Observable<any> {
    let method = `/object_memory/get_list_object_memory_by_agent_id?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public getObjectMemoryById(id: string): Observable<any> {
    let method = `/object_memory/get_object_memory_detail_by_id?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public getObjectByParams(nameValue: string, agentId: string): Observable<any> {
    let obj = { name: nameValue, agent_id: agentId, status: 1 };
    let method = `/object_memory/get_object_memory_by_params`;
    return this.HTTP_POST(this.HOST_MY, method, obj);
  }

  // @toantm1
  public add(body: Object): Observable<any> {
    let method = `/object_memory/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public update(body: Object): Observable<any> {
    let method = `/object_memory/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public getListObjectMemoryActionsByActionId(actionId: string): Observable<any> {
    let method = `/object_memory_actions/get_list_object_memory_actions?action_id=${actionId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public addObjectActions(body: Object): Observable<any> {
    let method = `/object_memory_actions/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }
  
  // @toantm1
  public updateObjectActions(body: Object): Observable<any> {
    let method = `/object_memory_actions/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }
}
