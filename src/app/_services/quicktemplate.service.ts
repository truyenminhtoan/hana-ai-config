import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { QuickTemplate } from '../_models/quicktemplate.model';

@Injectable()
export class QuickTemplateService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  public getListQuickPromise(partnerId): Promise<any> {
      let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
      let options = new RequestOptions({ headers: header });
      return this.http.post(this.HOST_MY + '/backend_app_user/getTemplateReply', {partner_id: partnerId}, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    if (error.status === 403) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      window.location.href = '/';
    }
    return Promise.reject(error.message || error);
  }

  public pageSubscribedAppJSON(appid: any, fanpageInfo: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/subscribe_app_with_facebook_fantpage';
    let bodyString = `{"partner_id": ${partnerId},"app_id": "${appid}","gateway_id":1,"page_access_token":"${fanpageInfo.access_token}","page_id":"${fanpageInfo.id}","page_name":"${fanpageInfo.name}","picture":"${fanpageInfo.picture}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  /*public getAllFanpage(accessToken: any): Promise<any> {
   let bodyString = `method=userAccounts&data={"userAccessToken": "${accessToken}"}`;
   let header = new Headers({
   'Content-Type': 'application/x-www-form-urlencoded'
   });
   let options = new RequestOptions({headers: header});
   return this.http.post(this.HOST_FACEBOOK, bodyString, options)
   .toPromise()
   .then((response) => {
   return response.json();
   })
   .catch(this.handleErrorPromise);
   }*/

   public deleteTemplateReply(partnerId: any, id: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/destroyTemplateReply';
    // let bodyString = `{"userAccessToken": "${accessToken}","partner_id":${partnerId}}`;
    // let bodyString = `{"partner_id": ${partnerId},"quick_search":"${quickSearch}","text_search":"${textSearch}","status":1}`;
    let bodyJson = {
      partner_id: partnerId,
      template_reply_id: id
    };
    let bodyString = JSON.stringify(bodyJson);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  public createTemplateReply(partnerId: any, quickSearch: any, textSearch: any, ordernum: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/createTemplateReply';
    // let bodyString = `{"userAccessToken": "${accessToken}","partner_id":${partnerId}}`;
    // let bodyString = `{"partner_id": ${partnerId},"quick_search":"${quickSearch}","text_search":"${textSearch}","status":1}`;
    let bodyJson = {
      partner_id: partnerId,
      quick_search: quickSearch,
      text_search: textSearch,
      order_num: ordernum,
      status: 1
    };
    let bodyString = JSON.stringify(bodyJson);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public editTemplateReply(partnerId: any, quickSearch: any, textSearch: any, ordernum: any, id: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/updateTemplateReply';
    let bodyJson = {
      partner_id: partnerId,
      quick_search: quickSearch,
      text_search: textSearch,
      template_reply_id: id,
      order_num: ordernum,
      status: 1
    };
    let bodyString = JSON.stringify(bodyJson);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh get
  public getAllFanpageIntegrate(): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_all_fanpage_integrate';
    let bodyString = `{}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  

}
