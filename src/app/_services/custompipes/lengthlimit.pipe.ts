import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'lengthlimit'})
export class LengthLimitCapitalizePipe implements PipeTransform {
  public transform(value: string, length: number): any {
    
    if (!value) return value;
    // console_bk.log('limit length ' + length);
    // console_bk.log('limit value ' + value.length );
    // console_bk.log(value.substring(0,  length ) + '...');

    let returnValue = value.length > length ? value.substring(0,  length) + '...' : value;
    return returnValue;
  }
}