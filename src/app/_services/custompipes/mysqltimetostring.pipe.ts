import { ConfigService } from './../config.service';
import { Pipe, PipeTransform } from '@angular/core';
declare var moment: any;
import * as _moment from 'moment-timezone';

@Pipe({ name: 'mysqltimetostring' })
export class MysqlTimeToStringPipe implements PipeTransform {
  private timeZone = 'UTC';
  constructor(private config: ConfigService){
    this.timeZone = this.config.timeZone;
  }
  public transform(value: string, length: number): any {
    if (!value) return value;
    return _moment(value).tz(this.timeZone).format('YYYY-MM-DD HH:mm');
    // return _moment(value).tz('Asia/ho_chi_minh').format('YYYY-MM-DD hh:mm');
  }
}