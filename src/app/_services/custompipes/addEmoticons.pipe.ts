import { Pipe, PipeTransform } from '@angular/core';

declare var $: any;
declare var _: any;

@Pipe({ name: 'addEmoticons' })
export class AddEmoticonsPipe implements PipeTransform {
  public transform(link: string): string {
    return this.emoticon(link);
  }

  private emoticon(plainText): string {
    if (plainText) {
      plainText = plainText.replace(/:\*/g, '<i class="em em-hana-kiss"></i>');
      plainText = plainText.replace(/\-\_\-/g, '<i class="em em-hana-squint"></i>');
      plainText = plainText.replace(/8\)/g, '<i class="em em-hana-glasses"></i>');
      plainText = plainText.replace(/B\)/g, '<i class="em em-hana-glasses"></i>');
      plainText = plainText.replace(/O:\)/g, '<i class="em em-hana-angel"></i>');
      plainText = plainText.replace(/3:\)/g, '<i class="em em-hana-devil"></i>');
      plainText = plainText.replace(/:\)/g, '<i class="em em-hana-smile"></i>');
      plainText = plainText.replace(/:\]/g, '<i class="em em-hana-smile"></i>');
      plainText = plainText.replace(/:\-\)/g, '<i class="em em-hana-smile"></i>');
      plainText = plainText.replace(/=\)/g, '<i class="em em-hana-smile"></i>');
      plainText = plainText.replace(/:\(/g, '<i class="em em-hana-sad"></i>');
      // plainText = plainText.replace(/:\[/g, '<i class="em em-hana-sad"></i>');
      plainText = plainText.replace(/:\-\(/g, '<i class="em em-hana-sad"></i>');
      plainText = plainText.replace(/=\(/g, '<i class="em em-hana-sad"></i>');
      plainText = plainText.replace(/:P/g, '<i class="em em-hana-tongue"></i>');
      plainText = plainText.replace(/:\-P/g, '<i class="em em-hana-tongue"></i>');
      plainText = plainText.replace(/:p/g, '<i class="em em-hana-tongue"></i>');
      plainText = plainText.replace(/:\-p/g, '<i class="em em-hana-tongue"></i>');
      plainText = plainText.replace(/:D/g, '<i class="em em-hana-grin"></i>');
      plainText = plainText.replace(/:\-D/g, '<i class="em em-hana-grin"></i>');
      plainText = plainText.replace(/=D/g, '<i class="em em-hana-grin"></i>');
      plainText = plainText.replace(/:O/g, '<i class="em em-hana-gasp"></i>');
      plainText = plainText.replace(/:\-O/g, '<i class="em em-hana-gasp"></i>');
      plainText = plainText.replace(/:o/g, '<i class="em em-hana-gasp"></i>');
      plainText = plainText.replace(/;\)/g, '<i class="em em-hana-wink"></i>');
      plainText = plainText.replace(/;\-\)/g, '<i class="em em-hana-wink"></i>');
      plainText = plainText.replace(/:\-\//g, '<i class="em em-hana-unsure"></i>');
      // plainText = plainText.replace(/:\//g, '<i class="em em-hana-unsure"></i>');
      plainText = plainText.replace(/:\-\\/g, '<i class="em em-hana-unsure"></i>');
      plainText = plainText.replace(/:\\/g, '<i class="em em-hana-unsure"></i>');
      plainText = plainText.replace(/&lt;3/g, '<i class="em em-hana-heart"></i>');
      plainText = plainText.replace(/\(y\)/g, '<i class="em em-hana-like"></i>');
    } else {
      plainText = '';
    }
    return plainText;
  }
}