import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ComponentInteractionService {
    eventPublisher$: Subject<any> = new Subject();
    eventReceiver$ = this.eventPublisher$.asObservable();
}