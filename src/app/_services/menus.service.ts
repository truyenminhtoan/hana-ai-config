import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';

@Injectable()
export class MenusService extends BaService {
    constructor(public http: Http) {
        super(http);
    }

    public getListMenuByAppId(body: Object): Observable<any> {
        let method = `/messenger_profile/getMenuRootByAppId`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getMessengerProfile(body: Object): Observable<any> {
        let method = `/messenger_profile/getMessengerProfile`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }


    public add(body: Object): Observable<any> {
        let method = `/messenger_profile/createOrUpdateMenuRoot`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public delete(body: Object): Observable<any> {
        let method = `/messenger_profile/deleteMenuByLevelAndId`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getMenuLevel1ByRootId(body: Object): Observable<any> {
        let method = `/messenger_profile/getMenuLevel1ByRootId`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public addMenuLevel1(body: Object): Observable<any> {
        let method = `/messenger_profile/createOrUpdateMenuLevel1`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getMenuLevel2ByLevel1Id(body: Object): Observable<any> {
        let method = `/messenger_profile/getMenuLevel2ByLevel1Id`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public addMenuLevel2(body: Object): Observable<any> {
        let method = `/messenger_profile/createOrUpdateMenuLevel2`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public updateIndexMenu(body: Object): Observable<any> {
        let method = `/messenger_profile/updateIndexMenu`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public updateAllowUserInput(body: Object): Observable<any> {
        let method = `/messenger_profile/updateAllowUserInput`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public applyPersistentMenu(body: Object): Observable<any> {
        let method = `/messenger_profile/applyPersistentMenu`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public createOrUpdateGreetingText(body: Object): Observable<any> {
        let method = `/messenger_profile/createOrUpdateGreetingText`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

}
