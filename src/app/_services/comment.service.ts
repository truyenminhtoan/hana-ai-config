
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BaService } from './ba.service';

@Injectable()
export class CommentsService extends BaService {
    constructor(public http: Http) {
        super(http);
    }

    public addNew(body): Observable<any> {
        let method = `/hashtags/add`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public find(body): Observable<any> {
        let method = `/hashtags/find`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public handleErrorPromise(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    public delete(hastagId, contextId): Observable<any> {
        let body = { id: hastagId , context_id: contextId};
        let method = `/hashtags/delete`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public update(body): Observable<any> {
        let options = {partner_id: body.partner_id, id: body.id, name: body.name};
        let method = `/hashtags/update`;
        return this.HTTP_POST(this.HOST_MY, method, options);
    }

    public getHashTagFallback(body): Observable<any> {
        let method = `/hashtags/getHashTagFallback`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public addListHashtag(body): Observable<any> {
        // {"partner_id":2095, "tags":["xinloi","tentoila"]}
        let options = {partner_id: body.partner_id, tags: body.tags};
        let method = `/hashtags/addListHashtag`;
        return this.HTTP_POST(this.HOST_MY, method, options);
    }

}
