import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';

@Injectable()
export class CampaignService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  public getAllCampaign(appId): Observable<any> {
    let method = `/campaign/getAllCampaign/${appId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getDetailCampaign(campaignId): Observable<any> {
    let method = `/campaign/getDetailCompaign/${campaignId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getListBroadcast(campaignId): Observable<any> {
    let method = `/campaign/getListBroadcast/${campaignId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public addCampaign(body): Observable<any> {
    let method = `/campaign/addCampaign`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public findOrCreateCampaign(body): Observable<any> {
    let method = `/campaign/findOrCreateCampaign`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }
  
  public updateCampaign(body): Observable<any> {
    let method = `/campaign/updateCampaign`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public deleteCampaign(id): Observable<any> {
    let method = `/campaign/deleteCampaign/${id}`;
    return this.HTTP_DELETE(this.HOST_MY, method);
  }

  public addTemplateBroadcast(body): Observable<any> {
    let method = `/campaign/addTemplateBroadcast`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public updateTemplateBroadcast(body): Observable<any> {
    let method = `/campaign/updateTemplateBroadcast`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public deleteTemplateBroadcast(id): Observable<any> {
    let method = `/campaign/deleteCampaignTemplate/${id}`;
    return this.HTTP_DELETE(this.HOST_MY, method);
  }

  public deleteCampaignTemplate(id): Observable<any> {
    let method = `/campaign/deleteCampaignTemplateByCampaign/${id}`;
    return this.HTTP_DELETE(this.HOST_MY, method);
  }

  public addCustomerToSequence(body): Observable<any> {
    let method = `/campaign/addCustomerToSequence`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public removeCustomerToSequence(body): Observable<any> {
    let method = `/campaign/removeCustomerToSequence`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public sortTemplateBroadcast(body): Observable<any> {
    let method = `/campaign/sortTemplateBroadcast`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public createGrowToolGateway(body): Observable<any> {
    let method = `/customer/createGrowToolGateway`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public cancelBroadcast(body): Observable<any> {
      let method = `/campaign/cancelBroadcast`;
      return this.HTTP_POST(this.HOST_MY, method, body);
  }
}
