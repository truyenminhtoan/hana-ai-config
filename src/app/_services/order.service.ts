import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { ActionBasic } from "../_models/actions.model";

@Injectable()
export class OrderService extends BaService {
    constructor(public http: Http) {
        super(http);
    }

    // public getOrderByParam(body): Observable<any> {
    //     let method = `/backend_app_user/getOrderByParam`;
    //     return this.HTTP_POST(this.HOST_MY, method, body);
    // }

    public getOrderByParam(body): Observable<any> {
        let method = `/backend_app_user/getOrderByParamV2`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getCreaterOrder(body): Observable<any> {
        let method = `/backend_app_user/getCreaterOrder`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public destroyListOrder(body): Observable<any> {
        let method = `/backend_app_user/destroy_list_order`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public updateOrder(body): Observable<any> {
        let method = `/backend_app_user/update_order`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

}
