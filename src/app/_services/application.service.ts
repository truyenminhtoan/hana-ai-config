import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { ActionBasic } from '../_models/actions.model';

@Injectable()
export class ApplicationService extends BaService {
  private currentUser;
  constructor(public http: Http) {
    super(http);
  }
  // docaohuynh
  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    if (error.status === 403) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      window.location.href = '/';
    }
    return Promise.reject(error.message || error);
  }
  // docaohuynh
  public createBackendPartner(company: any, userId: any): Promise<any> {
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let url = this.HOST_MY + '/backend_app_user/create_app_v2';
    // if (this.currentUser && this.currentUser.version === 1) {
    //   url = this.HOST_MY + '/backend_app_user/create_app';
    // }
    // let url = this.HOST_MY + '/backend_app_user/create_app';
    let bodyString = `{"company":"${company}","backend_user_id":${userId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status !== 200) {
          return false;
        } else {
          return response.json();
        }

      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public deleteApp(partnerId: any, appId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/destroy_app';
    let bodyString = `{"partner_id":"${partnerId}","app_id":"${appId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status !== 200) {
          return false;
        } else {
          return response.json();
        }

      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getListPartnerByUsername(username: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_app_by_user';
    let bodyString = `{"username":"${username}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    // let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + ''});
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getListUserInvited(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_invite_users';
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    // let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + ''});
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getPartnerDetailById(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_apps';
    console.log('PARTNERIDDDD ' + partnerId);
    let bodyString = `{"partner_id":${partnerId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getHideCommentConfig(appId: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_hide_comment_config';
    let bodyString = `{"app_id":"${appId}","partner_id":${partnerId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    // let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + ''});
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getEmailInviteByAppId(email: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/check_user_is_admin_by_email';
    let emails = JSON.stringify(email);
    let bodyString = `{"partner_id":${partnerId},"email":${emails}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getIntegrationByAppId(appId: any, gatewayId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_integration_gateway';
    let bodyString = `{"app_id":"${appId}","gateway_id":${gatewayId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // toantm
  public getOnlyIntegrationGateway(appId: any, gatewayId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_only_integration_gateway';
    let bodyString = `{"app_id":"${appId}","gateway_id":${gatewayId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public insertEmailInviteByAppId(email: any, partnerId: any, type: any, company: any, appId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/invite_app';
    let emails = JSON.stringify(email);
    let bodyString = `{"partner_id":${partnerId},"email_invites":${emails},"type":"${type}","company":"${company}","app_id":"${appId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status !== 200) {
          return false;
        }
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getTokenByPartnerId(fullName: any, username: any, partnerId: any, version: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_token';
    let bodyString = `{"partner_id":"${partnerId}","username":"${username}", "full_name":"${fullName}", "version": ${version}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public checkSession(): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/check_token';
    let bodyString = `{}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 403) {
          return false;
        } else {
          return response.json();
        }

      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getNotificationConfigByPartnerId(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_config_notifications';
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveNotificationConfigByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_config_notifications';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public createWhitelistDomain(body: any): Promise<any> {
    let url = this.HOST_MY + '/messenger_profile/createWhitelistedDomains';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
          // return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getWhitelistDomain(body: any): Promise<any> {
    let url = this.HOST_MY + '/messenger_profile/getWhitelistedDomains';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
          // return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveEmailReportByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_email_notifications';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveEmailFacebookByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/config_integration_email_expire';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveAgentConfigByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_app_agent';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveCommentConfigByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/set_hide_comment_config';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveTagsByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_all_tags';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveScriptConfigByPartnerId(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_integration_keyvalue';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public increaseTryUsing(body: any): Promise<any> {
    let url = this.HOST_MY + '/payment/update_fb_page_payment';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getTryUsing(body: any): Promise<any> {
    let url = this.HOST_MY + '/payment/get_fb_page_payment';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }
  // docaohuynh
  public getEmailReportByPartnerId(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_email_notifications';
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getEmailFacebookByPartnerId(appId: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_integration_email_expire';
    let bodyString = `{"partner_id":"${partnerId}","appId":"${appId}","gatewayId":1}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getAgentActiveByPartnerId(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_agent_active';
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getAllTagsByPartnerId(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_all_tags';
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public getAllAgentByPartnerId(partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/agent/get_list_agent_by_partner_id_v2';
    if (localStorage.getItem('currentUser')) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser.version === 1) {
        url = this.HOST_MY + '/agent/get_list_agent_by_partner_id';
      }
    }
    let bodyString = `{"partner_id":"${partnerId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh
  public saveUserNotification(body: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/update_user_notification';
    let bodyString = JSON.stringify(body);
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          // return response.json();
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getPageInfo(pageId): Observable<any> {
    let method = `/voucher/getPageInfo/${pageId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public activeVoucher(body): Observable<any> {
    let method = `/voucher/activeVoucher`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }
}
