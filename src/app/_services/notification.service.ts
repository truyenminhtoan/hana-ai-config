import { Injectable } from '@angular/core';
import { Messages } from '../_models/messages.model';
declare var $: any;
declare var swal: any;
@Injectable()
export class NotificationService {

  public showNotification(messages, types, froms, aligns, timers) {
    $.notify({
      icon: 'notifications',
      message: messages

    }, {
        type: types,
        timer: timers,
        delay: 10,
        z_index: 9000,
        placement: {
          from: froms,
          align: aligns
        }
      });
  }
  public showDanger(message: string) {
    this.showNotification(message, 'danger', 'top', 'right', 1500);
  }

  public showSuccess(message: string) {
    this.showNotification(message, 'success', 'top', 'right', 1000);
  }

  public showWarning(message: string) {
    this.showNotification(message, 'warning', 'top', 'right', 1000);
  }

  public showInfo(message: string) {
    this.showNotification(message, 'info', 'top', 'right', 1000);
  }

  public showRose(message: string) {
    this.showNotification(message, 'rose', 'top', 'right', 1000);
  }

  public showPrimary(message: string) {
    this.showNotification(message, 'primary', 'top', 'right', 1000);
  }
  
  public showNoAgentId() {
    this.showWarning("Không nhận dạng được Agent Id");
    setTimeout(function () {
      window.location.href = "/#/pages/agents/agentList";
    }, 2000);
  }

  public showNoPartnerId() {
    this.showWarning("Không nhận dạng được partnerId");
    setTimeout(function () {
      window.location.href = "/#/login-hana";
    }, 2000);
  }

  public showWarningTimer(message: string, time: number) {
    this.showNotification(message, 'warning', 'top', 'center', time);
  }
  
  public showSuccessTimer(message: string, time: number) {
    this.showNotification(message, 'success', 'top', 'center', time);
  }

  public showDangerTimer(message: string, time: number) {
    this.showNotification(message, 'danger', 'top', 'center', time);
  }
  
  public showSwal(message) {
    swal({
      text: message,
      buttonsStyling: false,
      confirmButtonClass: "btn btn-info"
    });
  }
  
  public clearConsole() {
    setTimeout(function () {
     // console.clear();
    }, 1000);
  }
}
