import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { UserModel, AgentModel } from '../_models/user.model';
import { BaService } from './ba.service';

@Injectable()
export class UserService extends BaService {
    constructor(public http: Http) {
        super(http);
    }

    // @toantm1
    public register(body: Object): Observable<UserModel> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/user/add', bodyString, options)
            .map((res) => {
                if (res.status !== 200) {
                    throw new Error('This request has failed ' + res.status);
                } else {
                    return res.json();
                }
            })
            .catch(this.handleError);
    }

    // @toantm1
    public getLogin(body: Object): Observable<UserModel> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/user/login', bodyString, options)
            .map((res) => {
                if (res.status !== 200) {
                    throw new Error('This request has failed ' + res.status);
                } else {
                    let user = res.json();
                    user.is_lock = false;
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    return user;
                }
            })
            .catch(this.handleError);
    }

    // @toantm1
    public getUserByEmail(username: string): Observable<UserModel> {
        let method = `/user/get_user_by_username?username=${username}`;
        return this.HTTP_GET(this.HOST_MY, method);
    }

    // @toantm1
    public loginOrRegisterSocial(body: any): Observable<UserModel> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/user/findOrCreate', bodyString, options)
            .map((res) => {
                if (res.status !== 200) {
                    throw new Error('This request has failed ' + res.status);
                } else {
                    let user = res.json();
                    localStorage.setItem('token', user.access_token);
                    user.is_lock = false;
                    localStorage.setItem('accessToken', body.access_token);
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    // console.log('CUrrent USer ' + JSON.stringify(user) + ' ACCESSTOKEN 0' + body.access_token);
                    return user;
                }
            })
            .catch(this.handleError);
    }

    // @docaohuynh
    //   public loginWithSocial(body: any): Observable<UserModel> {
    //     let bodyString = JSON.stringify(body);
    //     let header = new Headers({ 'Content-Type': 'application/json' });
    //     let options = new RequestOptions({ headers: header });
    //     return this.http.post(this.HOST + '/user/findOrCreate', bodyString, options)
    //       .map((res) => {
    //         if (res.status !== 200) {
    //           throw new Error('This request has failed ' + res.status);
    //         } else {
    //           let user = res.json();
    //           localStorage.setItem('token', user.access_token);
    //           user.is_lock = false;
    //           localStorage.setItem('accessToken', body.access_token);
    //           localStorage.setItem('currentUser', JSON.stringify(user));
    //           console.log('CUrrent USer ' +  JSON.stringify(user) + ' ACCESSTOKEN 0' +body.access_token );
    //           return user;
    //         }
    //       })
    //       .catch(this.handleError);
    //   }

    // @docaohuynh
    public loginWithSocial(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/backend_app_user/register_or_login_user_other', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }

    // @docaohuynh
    public loginWithSUsername(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/backend_app_user/login_with_backend_user', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }

    // @docaohuynh
    public getBackendUserByConfirmCode(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/backend_app_user/get_user_by_code', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }
    // @docaohuynh
    public updateBackendUser(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/backend_app_user/update_user', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response;
            })
            .catch(this.handleErrorPromise);
    }

    // @docaohuynh
    public createBackendUsername(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/backend_app_user/create_backend_user', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }
    // @docaohuynh
    public handleErrorPromise(error: any): Promise<any> {
        console.error('An error occurred', error);
        if (error.status === 403) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('apphanausing');
            localStorage.removeItem('session_time');
            window.location.href = '/';
        }
        return Promise.reject(error.message || error);
    }

    // @docaohuynh affilate user
    public createAffiliateUser(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_AFFILIATE, bodyString, options)
            .toPromise()
            .then((response) => {
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }

    // @toantm1
    public getListAgentByPartnerId(partnerId: string): Observable<AgentModel[]> {
        let method = `/agent/get_list_agent_by_partner_id_v2?partner_id=${partnerId}`;
        return this.HTTP_GET(this.HOST_MY, method);
    }
}
