import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { AgentModel } from "../_models/user.model";

@Injectable()
export class AgentService extends BaService {
  constructor(public http: Http) {
    super(http);
  }
  // @tamnguyen
  public add(body: Object): Observable<any> {
    let method = `/agent/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @tamnguyen
  public getListAgentByPartnerId(partnerId: string): Promise<AgentModel[]> {
    return this.http.get(this.HOST_MY +
        `/agent/get_list_agent_by_partner_id_v2?partner_id=${partnerId}`)
        .toPromise()
        .then((response) => {
          return response.json() as AgentModel[];
        })
        .catch(this.handleErrorPromise);
  }

  // @tamnguyen
  public update(body: Object): Observable<any> {
    let method = `/agent/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @tamnguyen
  public getAgentById(agentId: string): Observable<any> {
    let method = `/agent/get_agent_by_id?id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getAgentInfo(body): Observable<any> {
    let method = `/agent/get_agent_by_id`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  public deleteAgent(id: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/agent/delete`, JSON.stringify({ id: id }))
        .toPromise()
        .then((response) => {
          return response.json();
        })
        .catch(this.handleErrorPromise);
  }

}