import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';

@Injectable()
export class EventsService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  // @toantm1
  public getListEventByAgentId(agentId: string): Observable<any> {
    let method = `/event/get_list_event_by_agent_id?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public add(body: Object): Observable<any> {
    let method = `/event/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public update(body: Object): Observable<any> {
    let method = `/event/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public add_or_update_array(body: Object): Observable<any> {
    let method = `/event/add_or_update_array`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }
}
