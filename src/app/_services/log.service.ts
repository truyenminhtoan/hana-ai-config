import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
@Injectable()
export class Log {

    constructor(private configService: ConfigService) { }
    public info(content) {
        if (this.configService.isProd) {
            return;
        }
        console.log(content);
    }
}
