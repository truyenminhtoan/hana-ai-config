// @docaohuynh
import { Injectable } from '@angular/core';
import { BaService } from './ba.service';
@Injectable()
export class ConfigService {
  public timeZone = 'Asia/Ho_chi_minh';
  // public timeZone = 'UTC';

  // deploy
  public HOST_MY_WEB = 'https://chat.hana.ai';
  // public HOST_MY_WEB = 'https://10min.mideasvn.com:9004';
  public HOST_MY = 'https://10mingateway.mideasvn.com';
  public isProd = true;
  public hostResource = 'https://resources.mideasvn.com';
  public consoleHana = 'https://crm.hana.ai';
  public SESSION_TIME = 480;
  public facebookConfig = {
      client_id: '189605364869646',
      growtool_client_id: '265010030625004',
  };
  public googleConfig = {
      client_id: '960920835497-3ul430b6376a002nvuseui57h7r75g7m.apps.googleusercontent.com',
      client_secret: 'fd118fd148ec997b9db9ece6c00aae8f'
  };
  public HOST_UPLOAD_IMAGE = 'https://upload.mideasvn.com/api/upload';
  public HOST_RESOURCE_CHAT = 'https://resources.mideasvn.com';
  public HOST_STATIC_FILE = 'https://file.mideasvn.com';
  public HOST_CHAT = 'https://chatweb.mideasvn.com/http-bind';
  public HOST_API = 'https://chatapi.mideasvn.com/ReengBackendBiz';
  public develop = 'prod';
  public ROLE_USER = {
      sysadmin: 1,
      syssupport: 2,
      memeber: 3,
      admin: 4
  };

  // hotfix

  // // public HOST_MY_WEB = 'http://localhost:3000';
  // public HOST_MY_WEB = 'https://test.mideasvn.com:9003';
  // public HOST_MY = 'https://test.mideasvn.com:9211';
  // public hostResource = 'https://test.mideasvn.com';
  // // public consoleHana = 'http://localhost:8001';
  // public consoleHana = 'https://test.mideasvn.com:9001';
  // public isProd = false;
  // public SESSION_TIME = 480; // phut 480
  // public facebookConfig = {
  //   client_id: '108207942999174',
  //   // client_id: '265010030625004',
  //   growtool_client_id: '1139298356105259',
  // };
  // public googleConfig = {
  //   client_id: '960920835497-3ul430b6376a002nvuseui57h7r75g7m.apps.googleusercontent.com',
  //   client_secret: 'fd118fd148ec997b9db9ece6c00aae8f'
  // };

  // public HOST_UPLOAD_IMAGE = 'https://test.mideasvn.com:8206/api/upload';
  // public HOST_RESOURCE_CHAT = 'https://test.mideasvn.com:8206';
  // public HOST_STATIC_FILE = 'https://test.mideasvn.com:8000';
  // public HOST_CHAT = 'https://test.mideasvn.com:8282/http-bind';
  // public HOST_API = 'https://test.mideasvn.com:8200/ReengBackendBiz';

  // public develop = 'dev';
  // public ROLE_USER = {
  //   sysadmin: 1,
  //   syssupport: 2,
  //   memeber: 3,
  //   admin: 4
  // };

  public hideTryUsing = ['remarketing', 'recustomer', 'posttraining'];
  public facebookPolicyTitle = 'Hana cảnh báo!';
  public facebookPolicy = 'Tin nhắn re-marketing dùng để chăm sóc khách hàng và nuôi dưỡng khách hàng, kêu gọi khách hàng quay lại fanpage. \nKhông nên gửi nội dung quảng cáo, tiếp thị, khuyến mãi hoặc giảm giá. Nếu không tuân thủ chính sách này của Facebook tài khoản của bạn có thể bị Facebook khoá.';

  public log(msg) {
    if (this.develop === 'dev') {
      console.log(msg);
    }
  }
}