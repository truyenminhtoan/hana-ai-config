import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { ActionBasic } from "../_models/actions.model";

@Injectable()
export class FacebookService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  public getListActionPromise(agentId): Promise<ActionModel[]> {
    // return this.http.get(this.HOST_ACTION +
    //   `method=getListActionGroupByAgentId&data={"agentId": "' + agentId + '"}`)
    //   .toPromise()
    //   .then((response) => {
    //     return response.json() as ActionModel[];
    //   })
    //   .catch(this.handleErrorPromise);
      return this.http.post(this.HOST_MY + '/actions/get_list_action_by_agent_id', {agent_id: agentId})
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    if (error.status === 403) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      window.location.href = '/';
    }
    return Promise.reject(error.message || error);
  }
  /*public checkIntegrationGateway(appid: any): Promise<any> {
   let bodyString = `method=getIntegrationGatewayWithAppIdAndSource&data={"appId":"${appid}","gatewayId":1}`;
   let header = new Headers({
   'Content-Type': 'application/x-www-form-urlencoded'
   });
   let options = new RequestOptions({headers: header});
   return this.http.post(this.HOST_FACEBOOK_SV, bodyString, options)
   .toPromise()
   .then((response) => {
   return response.json();
   })
   .catch(this.handleErrorPromise);
   }*/

  /*public pageUnSubscribedApp(appid: any, fanpageInfo: any): Promise<any> {
   let bodyString = `method=pageUnSubscribedApp&data={"appId": "${appid}","gatewayId":1,"pageAccessToken":"${fanpageInfo.accessToken}","pageId":"${fanpageInfo.pageId}"}`;
   let header = new Headers({
   'Content-Type': 'application/x-www-form-urlencoded'
   });
   let options = new RequestOptions({headers: header});
   return this.http.post(this.HOST_FACEBOOK, bodyString, options)
   .toPromise()
   .then((response) => {
   return response.json();
   })
   .catch(this.handleErrorPromise);
   }*/

  public pageUnSubscribedAppJSON(appid: any, fanpageInfo: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/unsubscribe_app_with_facebook_fantpage';
    let bodyString = `{"partner_id": "${partnerId}","app_id": "${appid}","gateway_id":1,"page_access_token":"${fanpageInfo.accessToken}","page_id":"${fanpageInfo.pageId}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  /* public pageSubscribedApp(appid: any, fanpageInfo: any): Promise<any> {
    let avatar = encodeURIComponent(fanpageInfo.picture);
    let bodyString = `method=pageSubscribedApp&data={"appId": "${appid}","gatewayId":1,"pageAccessToken":"${fanpageInfo.access_token}","
   pageId":"${fanpageInfo.id}","pageName":"${fanpageInfo.name}","picture":"${avatar}"}`;
    let header = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: header });
    return this.http.post(this.HOST_FACEBOOK, bodyString, options)
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  } */

  public pageSubscribedAppJSON(appid: any, fanpageInfo: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/subscribe_app_with_facebook_fantpage';
    let bodyString = `{"partner_id": ${partnerId},"app_id": "${appid}","gateway_id":1,"page_access_token":"${fanpageInfo.access_token}","page_id":"${fanpageInfo.id}","page_name":"${fanpageInfo.name}","picture":"${fanpageInfo.picture}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response;
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  /*public getAllFanpage(accessToken: any): Promise<any> {
   let bodyString = `method=userAccounts&data={"userAccessToken": "${accessToken}"}`;
   let header = new Headers({
   'Content-Type': 'application/x-www-form-urlencoded'
   });
   let options = new RequestOptions({headers: header});
   return this.http.post(this.HOST_FACEBOOK, bodyString, options)
   .toPromise()
   .then((response) => {
   return response.json();
   })
   .catch(this.handleErrorPromise);
   }*/

  public getAllFanpage(accessToken: any, partnerId: any): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_all_fanpage';
    let bodyString = `{"userAccessToken": "${accessToken}","partner_id":${partnerId}}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  // docaohuynh get
  public getAllFanpageIntegrate(): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/get_all_fanpage_integrate';
    let bodyString = `{}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomes(body): Observable<any> {
    let options = { app_id: body.app_id };
    let method = `/inbound_service/get_list_user`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public sendMessage(body): Observable<any> {
    let options: any = {};
    options.app_id = body.app_id;
    options.users = body.users;
    options.message = body.message;
    let method = `/inbound_service/send_makerting_message`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public getSearchType(appId): Observable<any> {
    let method = `/customer/filter/${appId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getListCustomer(body): Observable<any> {
    let method = `/customer/list`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getTotalCustomer(body): Observable<any> {
    let method = `/customer/count`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListGroup(appId): Observable<any> {
    let method = `/customer/groups/listGroup/${appId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public addListCustomerToGroup(body): Observable<any> {
    let method = `/customer/groups/addListCustomerToGroup`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public deleteListCustomerFromGroup(body): Observable<any> {
    let method = `/customer/groups/removeListCustomerFromGroup`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public sendMessageStruct(body): Observable<any> {
    let method = `/customer/sendMessageStruct`;
    return this.HTTP_POST_NOT_RESTRY(this.HOST_MY, method, body);
  }


  public updateGrowPeriodMessageConfig(body): Observable<any> {
        let method = `/integration_config/growPeriodMessageConfig`;
        return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getGrowPeriodMessageConfig(body): Observable<any> {
        let method = `/backend_app_user/get_only_integration_gateway`;
        return this.HTTP_POST(this.HOST_MY, method, body);
  }

}
