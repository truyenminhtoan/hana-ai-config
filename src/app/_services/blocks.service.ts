import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { BlockGroup, Block, Button, Content, QuickReply, Payload, Message, Attachment } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { BaService } from './ba.service';
// import clone from 'clone-deep';
@Injectable()
export class BlocksService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  // toantm
  public getListGroupBlocks(partnerId): Observable<any> {
    let method = `/block/getListGroupBlocks`;
    return this.HTTP_POST(this.HOST_MY, method, {partner_id: partnerId});
  }

  public getDetailListBlocks(blockId): Observable<any> {
    let method = `/block/getListBlocks/${blockId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public addNewGroup(body): Observable<any> {
    let method = `/block/addGroup`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public updateNewGroup(body): Observable<any> {
    let method = `/block/updateGroup`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public addNewBlock(body): Observable<any> {
    let method = `/block/addBlock`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public updateNewBlock(body): Observable<any> {
    let method = `/block/updateBlock`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public deleteNewBlock(blockId): Observable<any> {
    let method = `/block/deleteBlock/${blockId}`;
    return this.HTTP_DELETE(this.HOST_MY, method);
  }

  public deleteNewGroup(groupId): Observable<any> {
    let method = `/block/deleteGroup/${groupId}`;
    return this.HTTP_DELETE(this.HOST_MY, method);
  }

  private handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  public listGroupByApp(appId): Observable<any> {
        let method = `/customer/groups/listGroup/${appId}`;
        return this.HTTP_GET(this.HOST_MY, method);
  }

   public addNewGroupBlock(body): Observable<any> {
        let method = `/customer/groups/findOrCreate`;
        return this.HTTP_PUT(this.HOST_MY, method, body);
   }

    public listSequencesByApp(appId): Observable<any> {
        let method = `/campaign/getListSequence/${appId}`;
        return this.HTTP_GET(this.HOST_MY, method);
    }

    public addNewSequence(body): Observable<any> {
        let method = `/customer/groups/findOrCreate`;
        return this.HTTP_PUT(this.HOST_MY, method, body);
    }

}
