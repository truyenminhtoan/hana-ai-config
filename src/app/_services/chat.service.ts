import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { ActionBasic } from "../_models/actions.model";
import { ConfigService } from './index';

declare var $: any;
declare var _: any;
declare var Base64: any;

@Injectable()
export class ChatService extends BaService {

  constructor(public http: Http, private configService: ConfigService) {
    super(http);
  }

  public getListActionPromise(agentId): Promise<ActionModel[]> {
    return this.http.post(this.HOST_MY + '/actions/get_list_action_by_agent_id', { agent_id: agentId })
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public createLogs(bodyString): Promise<any> {
    let url = this.HOST_MY + '/request-logs/create_log';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    if (error.status === 403) {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('apphanausing');
      localStorage.removeItem('session_time');
      window.location.href = '/';
    }
    return Promise.reject(error.message || error);
  }

  public getAllAgent(appid: any): Promise<any> {
    let start = new Date().getTime();
    let url = this.HOST_MY + '/chat_backend/get_company_user_app';
    let bodyString = `{"app_id": "${appid}"}`;
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        let end = new Date().getTime();
        let range = end - start;
        let bodyLog = {
          app_id: appid,
          api_name: 'chat_backend/get_company_user_app',
          time_request: range
        };
        this.createLogs(JSON.stringify(bodyLog)).then((integrate) => {
          if (integrate) {
            console.log('');
          }
        });
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getMucRoom(bodyString): Promise<any> {
    let start = new Date().getTime();
    let url = this.HOST_MY + '/chat_backend/get_muc_room';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        try {
          console.log('');
          let end = new Date().getTime();
          let range = end - start;
          let bodyLog = {
            app_id: bodyString.app_id,
            api_name: '/chat_backend/get_muc_room',
            time_request: range
          };
          this.createLogs(JSON.stringify(bodyLog)).then((integrate) => {
            if (integrate) {
              console.log('');
            }
          });
        } catch (e) {
          console.log('');
        }
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomerInfoFromListRoom(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/get_customer_info_from_list';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomerOrUpdateInfo(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/get_customer_or_update_info';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getChatUser(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/get_chat_by_param';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public searchRoom(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/getListMucroomByCustomer';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public updateMucRoom(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/update_muc_room';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }


  public updateAllRoomByAppId(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/updateAllMucroomsbyAppId';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public updateRoomByFilter(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/updateMucRoomByFilter';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }


  public updateAllRoomByFilter(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/updateAllMucroomsbyAppId';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getStoreMsg(bodyString): Promise<any> {
    // {"room_name":"web_df7a8b50-4442-11e7-9341-09d921074e88hanahana633c220e5d9b9ca353c7f51745f69920-1510136946525@muc.10min","unix_start":0,"unix_end":0,"size_msg":35,"useragent":"e2c3fed4-86ca-4774-9459-3ce6c42e84cc","sort":0}
    let url = this.HOST_MY + '/chat_backend/find_store_msg';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public addTags(bodyString): Promise<any> {
    // {"room_name":"web_df7a8b50-4442-11e7-9341-09d921074e88hanahana633c220e5d9b9ca353c7f51745f69920-1510136946525@muc.10min","unix_start":0,"unix_end":0,"size_msg":35,"useragent":"e2c3fed4-86ca-4774-9459-3ce6c42e84cc","sort":0}
    let url = this.HOST_MY + '/chat_backend/create_link_tag_room';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public pushToQueue(partnerId, bodyString): Promise<any> {
    // {"room_name":"web_df7a8b50-4442-11e7-9341-09d921074e88hanahana633c220e5d9b9ca353c7f51745f69920-1510136946525@muc.10min","unix_start":0,"unix_end":0,"size_msg":35,"useragent":"e2c3fed4-86ca-4774-9459-3ce6c42e84cc","sort":0}
    let body = {
      partner_id: partnerId,
      data: {
        queue_name: 'xmpp_broadcast',
        routing_key: 'xmpp_broadcast',
        content: Base64.encode(bodyString)
      }
    };
    // console_bk.log('PUSHTƠQUQQU ' + JSON.stringify(body));
    let url = this.HOST_MY + '/chat_backend/push_queue';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, JSON.stringify(body), options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getTemplateReply(bodyString): Promise<any> {
    let url = this.HOST_MY + '/backend_app_user/getTemplateReply';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomes(body): Observable<any> {
    let options = { app_id: body.app_id };
    let method = `/inbound_service/get_list_user`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public getPost(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/get_post';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public fbSendComment(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/fbSendComment';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getPostInfo(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/fbGetPostInfo';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public checkCommentFacebook(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/check_with_comment_id_on_facebook';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public editCommentFacebook(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/edit_comment';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }


  public likeCommentFacebook(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/create_like';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public hideCommentFacebook(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/hide_comment';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public deleteCommentFacebook(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/delete_comment';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public fbSendPrivateReplies(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/fbSendPrivateReplies';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public updateStoreMessage(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/update_store_msg';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public checkRoomExist(bodyString): Promise<any> {
    let url = this.configService.HOST_API + '/webclient/listroom';
    let header = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomerByUser(bodyString): Promise<any> {
    let url = this.HOST_MY + '/hana_core_api/getCustomerByUser';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public updateCustomer(bodyString): Promise<any> {
    let url = this.HOST_MY + '/chat_backend/update_customer';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getDemand(bodyString): Promise<any> {
    let url = this.HOST_MY + '/hana_core_api/getDemand';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getCustomerReport(bodyString): Promise<any> {
    let url = this.HOST_MY + '/report_bad/get_user_bads';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }


  public createReport(bodyString): Promise<any> {
    let url = this.HOST_MY + '/report_bad/create_user_bad';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public deleteReport(bodyString): Promise<any> {
    let url = this.HOST_MY + '/report_bad/destroy_user_bad';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }


  public createDemand(bodyString): Promise<any> {
    let url = this.HOST_MY + '/hana_core_api/createDemand';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public updateDemand(bodyString): Promise<any> {
    let url = this.HOST_MY + '/hana_core_api/updateDemand';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public groupFindOrCreate(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/groups/findOrCreate';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.put(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public groupAddCustomerToGroup(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/groups/addGroupCustomer';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.put(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return {};
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public removeGroupCustomer(bodyString): Promise<any> {
    let url = this.HOST_MY + '/customer/groups/removeGroupCustomer';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({
      headers: header,
      body: bodyString
    });
    return this.http.delete(url, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return {};
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public sequenceAddCustomerToSequence(bodyString): Promise<any> {
    let url = this.HOST_MY + '/campaign/addCustomerToSequenceV1';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    let options = new RequestOptions({ headers: header });
    return this.http.post(url, bodyString, options)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return {};
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public removeSequenceCustomer(bodyString): Promise<any> {
    let url = this.HOST_MY + '/campaign/removeCustomerToSequenceV1';
    let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
    return this.http.post(url, bodyString)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return {};
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getListGroupByAppid(appId): Promise<ActionModel[]> {
    return this.http.get(this.HOST_MY + '/customer/groups/listGroup/' + appId)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getListSequenceByAppid(appId): Promise<ActionModel[]> {
    return this.http.get(this.HOST_MY + '/campaign/getListSequence/' + appId)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getListGroupByPageCustomer(pageCustomer): Promise<ActionModel[]> {
    return this.http.get(this.HOST_MY + '/customer/groups/getListGroupByPageCustomer/' + pageCustomer)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public getListSequenceByPageCustomer(pageCustomer): Promise<ActionModel[]> {
    return this.http.get(this.HOST_MY + '/schedulebroadcast/getListSequenceByPageCustomer/' + pageCustomer)
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public sendMessage(body): Observable<any> {
    let options: any = {};
    options.app_id = body.app_id;
    options.users = body.users;
    options.message = body.message;
    let method = `/inbound_service/send_makerting_message`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public getSearchType(appId): Observable<any> {
    let method = `/customer/filter/${appId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getListCustomer(body): Observable<any> {
    let method = `/customer/list`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getTotalCustomer(body): Observable<any> {
    let method = `/customer/count`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListGroup(appId): Observable<any> {
    let method = `/customer/groups/listGroup/${appId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public addListCustomerToGroup(body): Observable<any> {
    let method = `/customer/groups/addListCustomerToGroup`;
    return this.HTTP_PUT(this.HOST_MY, method, body);
  }

  public deleteListCustomerFromGroup(body): Observable<any> {
    let method = `/customer/groups/removeListCustomerFromGroup`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public sendMessageStruct(body): Observable<any> {
    let method = `/customer/sendMessageStruct`;
    return this.HTTP_POST_NOT_RESTRY(this.HOST_MY, method, body);
  }







  // chat service util
  public checkUserIsFacebook(user: string) {
    if (user.indexOf('facebook_') >= 0) {
      return true;
    } else {
      return false;
    }
  }

  // chat service util
  public getNodeChat(users: string) {
    let usertemp = users.split('@');
    return usertemp[0];
  }

  public checkIfAgent(users: string, allAgent: any) {
    let agent = _.find(allAgent, (ag) => {
      return ag.username_chat === users;
    });
    if (agent) {
      return true;
    } else {
      return false;
    }
  }

  public checkIfHana(users: string) {
    if (users.indexOf('hanaagenthana_') >= 0) {
      return true;
    } else {
      return false;
    }
  }

  public checkFBUser(users: string) {
    if (users.indexOf('fb_user') >= 0) {
      return true;
    } else {
      return false;
    }
  }

  public checkIfCustomer(users: string, allAgent: any) {
    if (this.checkIfAgent(users, allAgent)) {
      return false;
    }
    if (this.checkIfHana(users)) {
      return false;
    }
    if (this.checkFBUser(users)) {
      return false;
    }

    return true;
  }
  public regexReplaceEnter(value: string) {
    let rege = /(\\r\\n)+/g;
    value = value.replace(rege, '\\r\\n');
    let rege1 = /(\\r)+/g;
    value = value.replace(rege1, '\\r');
    rege1 = /(\\n)+/g;
    value = value.replace(rege1, '\\n');
    let rege2 = /( )+/g;
    value = value.replace(rege2, ' ');
    return value;
  }

  public getAvatarFb(username: string) {
    return 'https://graph.facebook.com/' + username + '/picture?width=50&height=50';
  }

  public getAvatarFbFromCreater(username: string) {
    let user = username.split('hanahana')[1];
    let pageid = user.split('-')[0];
    return 'https://graph.facebook.com/' + pageid + '/picture?width=50&height=50';
  }


  public getFbLink(username: string) {
    return 'https://www.facebook.com/' + username;
  }

  // chat service util
  public getPageIdFacebook(user: string) {
    let usernamcustomer = user.split('hanahana')[1];
    console.log('CREATERRR ' + user);
    let pageId = usernamcustomer.split('-')[0];
    let customerId = usernamcustomer.split('-')[1];

    return {
      page: pageId,
      customer: customerId
    };

  }

  public getUserIdFacebookFromRoom(user: string) {
    if (_.isEmpty(user)) {
      return null;
    }
    user = user.substr(0, user.lastIndexOf('-'));
    return user.substr(user.lastIndexOf('-') + 1);

  }

  // chat service util
  public getPageCustomerFromCreater(user: string) {
    let usernamcustomer = user.split('hanahana')[1];
    return usernamcustomer.replace('-', '_');
  }

}
