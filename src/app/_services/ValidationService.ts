import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {StringFormat} from "../themes/validators/string.format";
@Injectable()
export class ValidationService {

  public validateAlphabet(value) {
    if (StringFormat.checkRegexp(value, /^[a-zA-Z0-9_]+$/)) {
      return true;
    }
    return false;
  }
  public validateEmail(value) {
    if (StringFormat.checkRegexp(value, /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/)) {
      return true;
    }
    return false;
  }
  public validateHttpsUrl(value) {
    if (StringFormat.checkRegexp(value, /(https:\/\/.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z‌​]{2,6}\b([-a-zA-Z0-9‌​@:%_\+.~#?&=]*)/)) {

      return true;
    }
    return false;
  }

  public validatePhone(value) {
        if (value.trim().length > 11 || value.trim().length < 10) {
          return false;
        }
        if (StringFormat.checkRegexp(value, /^(\+?(\d{1}|\d{2}|\d{3})[- ]?)?\d{3}[- ]?\d{3}[- ]?\d{4}$/)) {
            return true;
        }
        return false;
    }
}
