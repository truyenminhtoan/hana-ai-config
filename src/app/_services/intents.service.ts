import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { IntentModel } from '../_models/intents.model';
import { IntentDontunderstandModel } from '../_models/intents_dontunderstand';
import { Request, RequestMethod } from '@angular/http';

@Injectable()
export class IntentService extends BaService {

  constructor(public http: Http) {
    super(http);
  }

  public getPromiseIntentByById(id: string): Promise<IntentModel> {
    return this.http.get(this.HOST_MY + `/intent/get_intent_by_id?id=${id}`)
      .toPromise()
      .then((response) => response.json().data as IntentModel)
      .catch(this.handleErrorPromise);
  }

  public getIntentById(id: string): Observable<IntentModel> {
    let method = `/intent/get_intent_by_id?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public updatePromiseIntent(intent: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/intent/update`, JSON.stringify(intent))
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  public update(body: Object): Observable<any> {
    let method = `/intent/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public getListIntentByAgentId(agentId: string): Observable<any> {
    let method = `/event/get_list_event_by_agent_id?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  //Tam
  public getListIntentByAgentId_custom(agentId): Promise<IntentModel[]> {
    return this.http.get(this.HOST_MY +
      `/intent/get_list_intent_by_agent_id?agent_id=${agentId}`)
      .toPromise()
      .then((response) => {
        return response.json() as IntentModel[];
      })
      .catch(this.handleErrorPromise);
  }

  public getListIntentByAgentIdForSearch(agentId): Promise<IntentModel[]> {
    return this.http.get(this.HOST_MY +
      `/intent/get_list_intent_filter?agent_id=${agentId}`)
      .toPromise()
      .then((response) => {
        return response.json() as IntentModel[];
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  // @toantm1
  public getIntentByParams(name: string, agentId: string): Observable<any> {
    let method = `/intent/get_intent_by_params?agent_id=${agentId}&name=${name}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @Tam
  public getIntentByContexName(body: Object): Observable<any> {
    let method = `/intent/get_intent_by_context_name`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public add(body: Object): Observable<any> {
    let method = `/intent/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @T
  public addntentByContextName(body: Object): Observable<any> {
    let method = `/intent/add_intent_by_context_name`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public tagAddFaq(body: Object): Observable<any> {
    let method = `/intent/tagAddFaq`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @toantm1
  public getListIntentFilterByAgentId(agentId: string): Observable<any> {
    let method = `/intent/get_list_intent_filter?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public delete(id): Observable<any> {
    let method = `/intent/delete?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public getListContextByAgentId(agentId: string): Observable<any> {
    let method = `/context/get_list_context_by_agent_id?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm1
  public initIntentView(agentId: string, isSystem: boolean): Observable<any> {
    let method = `/intent/load_data_for_intent?agent_id=${agentId}&is_system=${isSystem}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getDetailIntent(id: string): Observable<IntentModel> {
    let method = `/intent/get_intent_detail_by_id?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public getDetailIntentV2(id: string): Observable<any> {
        let method = `/intent/get_intent_detail_by_id?id=${id}`;
        return this.HTTP_GET(this.HOST_MY, method);
  }

  public getListFAQ(body): Observable<any> {
    let method = `/intent/get_list_faq`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListFAQByContextId(body): Observable<any> {
    let options: any = { partner_id: body.partner_id, context_id: body.context_id };
    let method = `/hashtags/getListFAQByContextId`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListFAQByPostId(body): Observable<any> {
    let options: any = { partner_id: body.partner_id, post_id: body.post_id };
    let method = `/posts/getListFAQByPostId`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListDontunderstand(agentId): Observable<any> {
    let method = `/intent/get_list_usersay_dont_understand?agent_id=${agentId}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  public updateStatusUsersay(body): Observable<any> {
    // status; notYetTrain: chua train; trained: da train; delete: da xoa; ignoreTrain: xoa tam
    let method = `/intent/update_status_usersay_dont_understand`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public updateFAQ(body): Observable<any> {
    let method = `/intent/update_faq`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getValidateFacebookJSON(body): Observable<any> {
    let method = `/json/getValidateFacebookJSON`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getListIntentUsersay(say, agentId): Observable<any> {
    // {"usersay":"muon gi","agent_id":"ecb8c06d-3bde-46c5-a28c-aabbc3a18ae3"}
    let options = { usersay: say, agent_id: agentId };
    let method = `/intent/get_list_intent_usersay`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public addUsersayIntoIntent(body): Observable<any> {
    let options = { id: body.id, intent_id: body.intent_id, content: body.content, not_update_mongo: false };
    if (body.not_update_mongo) {
      options.not_update_mongo = body.not_update_mongo;
    }
    let method = `/intent/add_usersay_into_intent`;
    return this.HTTP_POST(this.HOST_MY, method, options);
  }

  public removeUsersayFromIntent(Id): Observable<any> {
    let method = `/intent/remove_usersay_from_intent`;
    return this.HTTP_POST(this.HOST_MY, method, { id: Id });
  }

  public updateFirstUsersay(_ids): Observable<any> {
    let method = `/intent/updateFirstUsersay`;
    return this.HTTP_POST(this.HOST_MY, method, { ids: _ids });
  }

  public addOrUpdateIntentBlock(body): Observable<any> {
    let method = `/intent/block/addOrUpdateIntentBlock`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public updateUsersay(body): Observable<any> {
    let method = `/intent/updateUsersay`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public sortIntentBlock(body): Observable<any> {
    let method = `/intent/block/sortIntentBlock`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public deleteIntentBlock(_id): Observable<any> {
    let method = `/intent/block/update`;
    return this.HTTP_POST(this.HOST_MY, method, { id: _id, status: -1 });
  }

  public getDontunderstand(agentId: string): Promise<IntentDontunderstandModel[]> {
    return this.http.get(this.HOST_MY + `/intent/get_list_usersay_dont_understand?agent_id=${agentId}&page=0&page_size=10`)
      .toPromise()
      .then((response) => {
        return response.json() as IntentDontunderstandModel[];
      })
      .catch(this.handleErrorPromise);
  }

  public searchDontunderstand(body: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/intent/search`, JSON.stringify(body))
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  public deleteDontunderstand(id: string): Promise<void> {
    var headers = new Headers({ 'Content-Type': 'application/json' });
    const url = this.HOST_MY + `web/dontunderstand/${id}`;

    return this.http.delete(url, { headers: headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  deleteServiceWithId(val: string): Promise<any> {
    const url = this.HOST_MY + `web/dontunderstand`;
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    return this.http
      .delete(url + "/?id=" + val, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError1);
  }

  private extractData(res: Response) {
    console.log('res', res);
    let body = res.json();
    return body || {};
  }

  private handleError1(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  // tslint:disable-next-line:member-ordering
  public deleteIntentFiltertById(id: any): Promise<any> {

    return this.http.post(this.HOST_MY + `/intent/update_status_usersay_dont_understand`, JSON.stringify({ id: id, status: 'deleted' }))
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  public deleteIntent(_id: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/intent/delete`, JSON.stringify({ id: _id }))
      .toPromise()
      .then((response) => {
        return response.json();
      }).catch(this.handleErrorPromise);
  }

  public duplicateIntentById(_id: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/intent/duplicateIntentById`, JSON.stringify({ id: _id }))
      .toPromise()
      .then((response) => {
        return response.json();
      }).catch(this.handleErrorPromise);
  }

  public getListIntentByContentIds(ids: any): Observable<any> {
    let method = `/intent/getListIntentByContentIds`;
    return this.HTTP_POST(this.HOST_MY, method, { ids: ids });
  }

  public addPostContext(body: any): Observable<any> {
    let options = { post_id: body.post_id, partner_id: body.partner_id };
    let method = `/postcontexts/addPostContext`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }
}
