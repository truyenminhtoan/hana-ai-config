// @toantm
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
export class BaService {
    protected ACCESS_TOKEN = '&access_token=d764ee96-21d9-4e0e-9d3e-9272d3cd8c59';

    // // trienkhai
    protected HOST_MY = 'https://10mingateway.mideasvn.com';
    protected HOST_ACTION = 'http://10mingateway.mideasvn.com:8107/services/action';
    protected HOST_AFFILIATE = 'http://trolythongminhhana.com/affiliate.php';

    // hotfix
    // protected HOST_MY = 'https://test.mideasvn.com:9201';
    // protected HOST_AFFILIATE = 'http://trolythongminhhana.com/affiliate.php';
    // protected HOST_ACTION = 'https://test.mideasvn.com:9107/services/action';

    protected token: string;
    protected retryRequest: number = 2; // retry connection
    protected timeOutRequest: number = 1000 * 60 * 2; // 60s * 2

    // ERROR CODE
    protected SUCESS_CODE = 200;
    protected ERROR_COMMON = 201;
    protected ERROR_EXITS = 202;
    protected ERROR_EXITS_2 = 203;
    protected ERROR_NOT_EXITS = 204;

    constructor(public http: Http) {
    }

    protected jwt() {
        let header = new Headers({ authorization: 'Bearer ' + this.getToken() });
        return new RequestOptions({ headers: header });
    }

    protected getToken() {
        try {
            let user = JSON.parse(localStorage.getItem('currentUser'));
            return user.token;
        } catch (error) {
            return null;
        }
    }

    protected HTTP_GET(host, method): Observable<any> {
        return this.http.get(host + method, this.jwt())
            .timeout(this.timeOutRequest)
            .map((res) => {
                if (res.status === this.SUCESS_CODE) {
                    return res.json();
                }
                if (res.status === this.ERROR_COMMON) {
                    return '';
                }
                if (res.status === this.ERROR_NOT_EXITS) {
                    return this.ERROR_NOT_EXITS;
                }
                if (res.status === this.ERROR_EXITS) {
                    return this.ERROR_EXITS;
                }
                if (res.status === this.ERROR_EXITS_2) {
                    return this.ERROR_EXITS_2;
                }
                throw new Error('This request has failed ' + res.status);
            })
            .retry(this.retryRequest)
            .catch(this.handleError);
    }

    protected HTTP_POST(host, method, body): Observable<any> {
        let bodyString = JSON.stringify(body);
        // tslint:disable-next-line:max-line-length
        let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
        let options = new RequestOptions({ headers: header });
        return this.http.post(host + method, bodyString, options)
            .timeout(this.timeOutRequest)
            .map((res) => {
                if (res.status === this.SUCESS_CODE) {
                    return res.json();
                }
                if (res.status === this.ERROR_COMMON) {
                    return '';
                }
                if (res.status === this.ERROR_NOT_EXITS) {
                    return this.ERROR_NOT_EXITS;
                }
                if (res.status === this.ERROR_EXITS) {
                    return this.ERROR_EXITS;
                }
                if (res.status === this.ERROR_EXITS_2) {
                    return this.ERROR_EXITS_2;
                }
                throw new Error('This request has failed ' + res.status);
            })
            .retry(this.retryRequest)
            .catch(this.handleError);
    }

    protected HTTP_POST_NOT_RESTRY(host, method, body): Observable<any> {
        let bodyString = JSON.stringify(body);
        // tslint:disable-next-line:max-line-length
        let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
        let options = new RequestOptions({ headers: header });
        return this.http.post(host + method, bodyString, options)
            .timeout(this.timeOutRequest * 2)
            .map((res) => {
                if (res.status === this.SUCESS_CODE) {
                    return res.json();
                }
                if (res.status === this.ERROR_COMMON) {
                    return '';
                }
                if (res.status === this.ERROR_NOT_EXITS) {
                    return this.ERROR_NOT_EXITS;
                }
                if (res.status === this.ERROR_EXITS) {
                    return this.ERROR_EXITS;
                }
                if (res.status === this.ERROR_EXITS_2) {
                    return this.ERROR_EXITS_2;
                }
                throw new Error('This request has failed ' + res.status);
            })
            .retry(0)
            .catch(this.handleError);
    }

    protected HTTP_PUT(host, method, body): Observable<any> {
        let bodyString = JSON.stringify(body);
        // tslint:disable-next-line:max-line-length
        let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
        let options = new RequestOptions({ headers: header });
        return this.http.put(host + method, bodyString, options)
            .timeout(this.timeOutRequest)
            .map((res) => {
                if (res.status === this.SUCESS_CODE) {
                    return res.json();
                }
                if (res.status === this.ERROR_COMMON) {
                    return '';
                }
                if (res.status === this.ERROR_NOT_EXITS) {
                    return this.ERROR_NOT_EXITS;
                }
                if (res.status === this.ERROR_EXITS) {
                    return this.ERROR_EXITS;
                }
                if (res.status === this.ERROR_EXITS_2) {
                    return this.ERROR_EXITS_2;
                }
                throw new Error('This request has failed ' + res.status);
            })
            .retry(this.retryRequest)
            .catch(this.handleError);
    }

    protected HTTP_DELETE(host, method): Observable<any> {
        // tslint:disable-next-line:max-line-length
        let header = new Headers({ 'Content-Type': 'application/json', 'authorization': 'Bearer ' + this.getToken() });
        let options = new RequestOptions({ headers: header });
        return this.http.delete(host + method, options)
            .timeout(this.timeOutRequest)
            .map((res) => {
                if (res.status === this.SUCESS_CODE) {
                    return res.json();
                }
                if (res.status === this.ERROR_COMMON) {
                    return '';
                }
                if (res.status === this.ERROR_NOT_EXITS) {
                    return this.ERROR_NOT_EXITS;
                }
                if (res.status === this.ERROR_EXITS) {
                    return this.ERROR_EXITS;
                }
                throw new Error('This request has failed ' + res.status);
            })
            .retry(this.retryRequest)
            .catch(this.handleError);
    }

    protected handleError(error: Response) {
        console.log('handleError:', JSON.stringify(error));
        if (error.status === 403) {
            // localStorage.removeItem('currentUser');
            return;
        }
        return Observable.throw(error || 'Server error');
    }
}