import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { BaService } from './ba.service';
import { EntitieModel } from '../_models/entities.model';

@Injectable()
export class EntityService extends BaService {
  constructor(public http: Http) {
    super(http);
  }
  public getListEntityPromise(agentId, isSystem): Promise<EntitieModel[]> {
    return this.http.get(this.HOST_MY +
      `/entity/get_entity_list_by_agent_id?agent_id=${agentId}&is_system=${isSystem}&status=1`)
      .toPromise()
      .then((response) => {
        return response.json() as EntitieModel[];
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  // @toantm
  public getListEntityModel(agentId: string, isSystem: string): Observable<any> {
    let method = `/entity/get_entity_list_by_agent_id?agent_id=${agentId}&is_system=${isSystem}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm
  public addEntity(body: Object): Observable<any> {
    let method = `/entity/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  // @tam
  public deleteEntity(id: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/entity/delete`, JSON.stringify({id: id}))
        .toPromise()
        .then((response) => {
          return response.json();
        })
        .catch(this.handleErrorPromise);
  }
  
  // @huynh them moi entity
  public addEntityPromise(entity: any): Promise<any> {
    let header = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: header }); // Create a request option
    return this.http.post(this.HOST_MY + `/entity/add`, JSON.stringify(entity), options)
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  // @tam lay entity by id
  public getEntityById(id: string): Observable<EntitieModel> {
    let method = `/entity/get_entity_detail_by_id?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }
  
  // @tam update entity
  public update(body: Object): Observable<any> {
    let method = `/entity/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public getEntityByName(name: any): Promise<any> {
    let header = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: header }); // Create a request option
    return this.http.post(this.HOST_MY + `/get_entity_by_params`, JSON.stringify({'name': name}), options)
        .toPromise()
        .then((response) => {
          return response.json();
        })
        .catch(this.handleErrorPromise);
  }

}
