import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CategoryModel, ProductsModel, ProductCriteria } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { BaService } from './ba.service';

@Injectable()
export class ProductsService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  // @huynh lay danh sach product by partner Id
  public getListProductByPartnerId(partnerId: string): Promise<ProductsModel[]> {
    // tslint:disable-next-line:max-line-length
    return this.http.get(this.HOST_MY + `/product_api/get_product_by_partner_id?partner_id=${partnerId}`)
      .toPromise()
      .then((response) => response.json().data as ProductsModel[])
      .catch(this.handleErrorPromise);
  }

  public getListProductByAgentId(agentId: string): Promise<ProductsModel[]> {
    // tslint:disable-next-line:max-line-length
    return this.http.get(this.HOST_MY + `/product_api/get_product_by_agent_id?agent_id=${agentId}`)
        .toPromise()
        .then((response) => response.json().data as ProductsModel[])
        .catch(this.handleErrorPromise);
  }

  // @toantm
  public getProductById(id: string): Observable<ProductsModel> {
    let method = `/product_api/get_product_by_id?id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @toantm
  public getProductsByPartnerId(id: string): Observable<any> {
    let method = `/product_api/get_product_by_partner_id?partner_id=${id}`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // @huynh lay product by id
  public getPromiseProductById(id: string): Promise<ProductsModel> {
    return this.http.get(this.HOST_MY + `/product_api/get_product_by_id?id=${id}`)
      .toPromise()
      .then((response) => response.json().data[0] as ProductsModel)
      .catch(this.handleErrorPromise);
  }
  // @huynh update product criteria
  public updatePromiseProductCriterias(criterias: any): Promise<ProductCriteria[]> {
    // tslint:disable-next-line:max-line-length
    return this.http.post(this.HOST_MY + `/product_api/add_or_upadte_list_product_criteria`, JSON.stringify(criterias))
      .toPromise()
      .then((response) => {
        return response.json().data as ProductCriteria[];
      })
      .catch(this.handleErrorPromise);
  }
  // @huynh cap nhat product
  public updatePromiseProduct(product: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/product_api/update_product`, JSON.stringify(product))
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  public deleteProductById(idProduct: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/product_api/delete_product`, JSON.stringify({ id: idProduct }))
      .toPromise()
      .then((response) => {
        return response.json();
      })
      .catch(this.handleErrorPromise);
  }

  // @huynh them moi product
  public createPromiseProduct(product: any): Promise<ProductsModel> {
    return this.http.post(this.HOST_MY + `/product_api/add_product`, JSON.stringify(product))
      .toPromise()
      .then((response) => {
        return response.json().data as ProductsModel;
      })
      .catch(this.handleErrorPromise);
  }
  // @huynh lay product criteria
  public getPromiseProductCriterias(productId: string): Promise<ProductCriteria[]> {
    return this.http.get(this.HOST_MY +
      `/product_api/get_product_criteria_by_param?product_id=${productId}`)
      .toPromise()
      .then((response) => response.json().data as ProductCriteria[])
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    //console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  public deleteProduct(productId: string): Observable<any> {
    let method = `/product_api/delete_product`;
    return this.HTTP_POST(this.HOST_MY, method, productId);
  }

  // @tam
  public addListsProductItem(body: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/product_api/create_product_item`, body)
        .toPromise()
        .then((response) => {
          try {
            return response.json();
          }
          catch(err) {
            return {
              "errorCode":201
            }
          }
        })
        .catch(this.handleErrorPromise);
  }
// @tam
  public getListsProductItem(product_id: any): Promise<any> {
    return this.http.post(this.HOST_MY + `/product_api/get_product_item`, JSON.stringify({product_id: product_id}))
        .toPromise()
        .then((response) => {
          console.log('response', response);
          return response.json();
        })
        .catch(this.handleErrorPromise);
  }

  public deleteProductItem(product_item_id: string): Promise<any> {
    return this.http.get(this.HOST_MY +
        `/product_api/delete_product_data/${product_item_id}`)
        .toPromise()
        .then((response) => response.json())
        .catch(this.handleErrorPromise);
  }

  public destroyProductItem(product_id: string): Promise<any> {
    return this.http.post(this.HOST_MY + `/product_api/destroy_product_item`, JSON.stringify({product_id: product_id}))
        .toPromise()
        .then((response) => {
          console.log('response', response);
          return response.json();
        })
        .catch(this.handleErrorPromise);
  }

  public updatePromiseProductItem(product_item: any): Promise<any> {
        return this.http.post(this.HOST_MY + `/product_api/update_product_item`, JSON.stringify(product_item))
            .toPromise()
            .then((response) => {
                return response.json();
            })
            .catch(this.handleErrorPromise);
  }

}
