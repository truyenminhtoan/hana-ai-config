import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { UserModel, AgentModel } from '../_models/user.model';
import { BaService } from './ba.service';

@Injectable()
export class CrmCustomerService extends BaService {
    constructor(public http: Http) {
        super(http);
    }
    // @docaohuynh
    public handleErrorPromise(error: any): Promise<any> {
        console.error('An error occurred', error);
        if (error.status === 403) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('apphanausing');
            localStorage.removeItem('session_time');
            window.location.href = '/';
        }
        return Promise.reject(error.message || error);
    }
    // @docaohuynh
    public getAllCustomer(body: any): Promise<any> {
        let bodyString = JSON.stringify(body);
        let header = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: header });
        return this.http.post(this.HOST_MY + '/hana_core_api/getAllCustomer', bodyString, options)
            .toPromise()
            .then((response) => {
                if (response.status !== this.SUCESS_CODE) {
                    return false;
                }
                return response.json();
            })
            .catch(this.handleErrorPromise);
    }


}
