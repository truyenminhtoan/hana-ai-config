import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';

@Injectable()
export class TaskSearchService extends BaService {
    constructor(public http: Http) {
        super(http);
    }

    public handleErrorPromise(error: any): Promise<any> {
        console.error('An error occurred', error);
        if (error.status === 403) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('apphanausing');
            localStorage.removeItem('session_time');
            window.location.href = '/';
        }
        return Promise.reject(error.message || error);
    }

    public getDemandNew(body: any): Observable<any> {
        let method = `/hana_core_api/get_demand_new`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getSourceCustomer(body: any): Observable<any> {
        let method = `/hana_core_api/get_source_customer`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getCompanyeCustomer(body: any): Observable<any> {
        let method = `/hana_core_api/get_company_customer`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public createDemand(body: any): Observable<any> {
        let method = `/hana_core_api/create_demand`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getDemandDetail(body: any): Observable<any> {
        let method = `/hana_core_api/get_demand_by_id`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public updateDemand(body: any): Observable<any> {
        let method = `/hana_core_api/update_demand`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public deleteCustomer(body: any): Observable<any> {
        let method = `/hana_core_api/destroy_customer`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public createCompanyCustomer(body: any): Observable<any> {
        let method = `/hana_core_api/create_company_customer`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public getDemandCategory(body: any): Observable<any> {
        let method = `/hana_core_api/get_demand_category`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

    public deleteDemand(body: any): Observable<any> {
        let method = `/hana_core_api/destroy_demand`;
        return this.HTTP_POST(this.HOST_MY, method, body);
    }

}
