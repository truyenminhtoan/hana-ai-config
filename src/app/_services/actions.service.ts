import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActionModel } from '../_models/index';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaService } from './ba.service';
import { ActionBasic } from "../_models/actions.model";

@Injectable()
export class ActionsService extends BaService {
  constructor(public http: Http) {
    super(http);
  }

  public getListActionPromise(agentId): Promise<ActionModel[]> {
    return this.http.post(this.HOST_MY + '/actions/get_list_action_by_agent_id', { agent_id: agentId })
      .toPromise()
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          return false;
        }
      })
      .catch(this.handleErrorPromise);
  }

  public handleErrorPromise(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  public getListActionByAgentId(agentId: String): Observable<any> {
    let method = `/actions/get_list_action_by_agent_id`;
    return this.HTTP_POST(this.HOST_MY, method, { agent_id: agentId });
  }

  public getListSearchType(): Observable<any> {
    let method = `/actions/get_action_search_type`;
    return this.HTTP_GET(this.HOST_MY, method);
  }

  // Load thông tin hành động phải bao gồm hành động của hệ thống nhưng loại trừ loại 'is_abstract' = 1 đi và cả những action của agent đó có trạng thái = 1
  public getListActionBasicNotAbstractByAgentId(agentId: String): Observable<any> {
    let method = `/actions/getListActionBasicNotAbstractByAgentId`;
    return this.HTTP_POST(this.HOST_MY, method, {agent_id: agentId});
  }

  public getListActionBasicNotAbstract(): Observable<any> {
    let method = `/actions/getListActionBasicNotAbstract`;
    return this.HTTP_POST(this.HOST_MY, method, {});
  }

  public getListActionTemplate(): Observable<any> {
    let method = `/actions/get_list_actions_abstract`;
    return this.HTTP_POST(this.HOST_MY, method, {});
  }

  public getActionById(id: string): Observable<any> {
    let method = `/actions/get_action_by_id`;
    return this.HTTP_POST(this.HOST_MY, method, { action_id: id });
  }

  public getActionByName(name: string): Observable<any> {
    let method = `/actions/get_action_by_name`;
    return this.HTTP_POST(this.HOST_MY, method, { action_name: name });
  }

  public add(body: Object): Observable<any> {
    let method = `/actions/add`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public update(body: Object): Observable<any> {
    let method = `/actions/update`;
    return this.HTTP_POST(this.HOST_MY, method, body);
  }

  public delete(id: string): Observable<any> {
    let method = `/actions/delete`;
    return this.HTTP_POST(this.HOST_MY, method, {action_id: id});
  }


// ole


  // public getListActionPromise(agentId): Promise<ActionModel[]> {
  //   return this.http.get(this.HOST_ACTION +
  //       `method=getListActionGroupByAgentId&data={"agentId": "' + agentId + '"}`)
  //       .toPromise()
  //       .then((response) => {
  //         return response.json() as ActionModel[];
  //       })
  //       .catch(this.handleErrorPromise);
  // }

  // public getListActionByAgentId(agentId: String): Observable<any> {
  //   let bodyString = 'method=getListActionGroupByAgentId&data={"agentId": "' + agentId + '"}';
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response.actions;
  //       })
  //       .catch(this.handleError);
  // }

  // public getListSearchType(): Observable<any> {
  //   let bodyString = 'method=actionSearchTypeActive';
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response;
  //       })
  //       .catch(this.handleError);
  // }

  // // Load thông tin hành động phải bao gồm hành động của hệ thống nhưng loại trừ loại 'is_abstract' = 1 đi và cả những action của agent đó có trạng thái = 1
  // public getListActionBasicNotAbstractByAgentId(agentId: String): Observable<any> {
  //   let bodyString = 'method=getListActionBasicNotAbstractByAgentId&data={"agentId": "' + agentId + '"}';
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response.actions;
  //       })
  //       .catch(this.handleError);
  // }

  // public getListActionBasicNotAbstract(): Observable<any> {
  //   let bodyString = 'method=getListActionBasicNotAbstract';
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response.actions;
  //       })
  //       .catch(this.handleError);
  // }

  // public getActionById(id: string): Observable<any> {
  //   let bodyString = `method=getActionById&data={"actionId": "${id}"}`;
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response.action;
  //       })
  //       .catch(this.handleError);
  // }

  // public getActionByName(name: string): Observable<any> {
  //   let bodyString = `method=getActionByName&data={"actionName": "${name}"}`;
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST, bodyString, options)
  //       .map((res: Response) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         }
  //         return res.json().response.action;
  //       })
  //       .catch(this.handleError);
  // }

  // public add(body: Object): Observable<any> {
  //   let bodyString = 'method=createAction&data=' + JSON.stringify(body);
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         } else {
  //           return res.json().response;
  //         }
  //       })
  //       .catch(this.handleError);
  // }

  // public update(body: Object): Observable<any> {
  //   let bodyString = 'method=updateAction&data=' + JSON.stringify(body);
  //   let header = new Headers({
  //     'Content-Type':
  //         'application/x-www-form-urlencoded'
  //   });
  //   let options = new RequestOptions({ headers: header });
  //   return this.http.post(this.HOST_ACTION, bodyString, options)
  //       .map((res) => {
  //         if (res.json().code !== 200) {
  //           throw new Error('This request has failed ' + res.json().code);
  //         } else {
  //           return res.json();
  //         }
  //       })
  //       .catch(this.handleError);
  // }

  // public delete(id: string): Observable<any> {
  //   {
  //     let bodyString = `method=deleteActionById&data={"actionId": "${id}"}`;
  //     let header = new Headers({
  //       'Content-Type': 'application/x-www-form-urlencoded'
  //     });
  //     let options = new RequestOptions({ headers: header });
  //     return this.http.post(this.HOST_ACTION, bodyString, options)
  //         .map((res) => {
  //           if (res.json().code !== 200) {
  //             throw new Error('This request has failed ' + res.json().code);
  //           } else {
  //             return res.json();
  //           }
  //         })
  //         .catch(this.handleError);
  //   }
  // }
}



